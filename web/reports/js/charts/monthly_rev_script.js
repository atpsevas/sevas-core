$(function () {

    if (typeof location.origin === 'undefined')
        location.origin = location.protocol + '//' + location.host;
    var monthlyRevenueDataURL = location.origin + '/sevas/MonthlyRevenueChart';
    console.log(monthlyRevenueDataURL);

    // Load revenue chart
    monthlyRevenueChart(monthlyRevenueDataURL);

    // The tooltip shown over the chart
    var tt = $('<div class="ex-tooltip">').appendTo('body'), topOffset = -32;

    // Initialize Data
    var data = {
        "xScale": "ordinal",
        "yScale": "linear",
        "main": [{
            className: ".stats",
            "data": []
        }]
    };


    // Option Declaration for XChart Instance
    // Option Declaration for XChart Instance
    var opts = {
        paddingLeft: 50,
        paddingTop: 20,
        paddingRight: 10,
        axisPaddingLeft: 25,
        ymin: 0,
        xmin: 1,
        dataFormatX: function (x) {
            // This turns converts the timestamps coming from
            // ajax.php into a proper JavaScript Date object
            return (x);
        },
        tickFormatX: function (x) {
            // Provide formatting for the x-axis tick labels.
            // This uses sugar's format method of the date object.
            return getMonthName(x);
        },
        tickFormatY: function (x) {
            // Provide formatting for the Y-axis tick labels.
            // This uses sugar's format method of the date object.
            return x.format('{000},{000}');
        },
        "mouseover": function (d, i) {
            var pos = $(this).offset();
            tt.text(getMonthName(d.x) + ', Rev: ' + d.y)
                .css({
                    top: topOffset + pos.top,
                    left: pos.left
                }).show();
        },
        "mouseout": function (x) {
            tt.hide();
        }
    };

    getMonthName = function (month_num) {
        var d = new Date();
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        var n = month[month_num - 1];
        return n;
    }

    // Create a new xChart instance, passing the type
    // of chart a data set and the options object
    var chart = new xChart('line-dotted', data, '#monthly_chart_rev', opts);

    setLinearChartData = function (chart_data) {
        chart.setData({
            "xScale": "ordinal",
            "yScale": "linear",
            "main": [{
                className: ".stats",
                data: chart_data
            }]
        });
    };


    //daily Revenue Chart..
    function monthlyRevenueChart(dataURL) {
        // If no data is passed (the chart was cleared)
        // Otherwise, issue an AJAX request
        $.post(dataURL, function (data) {
            if ((data.indexOf("No record found") > -1)
                || (data.indexOf("Date must be selected.") > -1)) {
                $('#msg').append('<span style="color:red;"> Monthly - ' + data + '</span>');
                $('#placeholder_monthly_rev').hide();
                chart.setData({
                    "xScale": "time",
                    "yScale": "linear",
                    "main": [{
                        className: ".stats",
                        data: []
                    }]
                });
            } else {

                // Add onload 
                var set = [];
                $.each(data, function () {
                    set.push({
                        x: parseInt(this.label),
                        y: parseInt(this.value, 10)
                    });
                });
                setLinearChartData(set);
                console.log('Dataset', set);
                $('#placeholder_monthly_rev').show();

            }
        }, 'json');
    }
});