<%@page import="java.sql.Connection" %>
<%@page import="java.sql.PreparedStatement" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.text.DecimalFormat" %>
<%

    out.println("<table width='100%' class='fancy'>");
    out.println("<tr>");
    out.println("<th>Service Name</th>");
    out.println("<th>Rev Target Year</th>");
    out.println("<th>Rev Target Month</th>");
    out.println("<th>Rev Target</th>");
    out.println("<th>Update</th>");
    out.println("</tr>");
    Connection conn = null;
    try {

        Long serviceID = Long.parseLong(request.getParameter("service"));
        conn = (Connection) getServletContext().
                getAttribute("database.connection");

        PreparedStatement st = conn.prepareStatement(
                getServletContext().getInitParameter("monthlyRevTargetByService"));
        st.setLong(1, serviceID);

        sevasLogger.info(getServletContext().getInitParameter("monthlyRevTargetByService"));
        sevasLogger.info(serviceID);
        ResultSet rs = st.executeQuery();
        long totalTarget = 0;
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        while (rs.next()) {
            out.println("<tr>");
            out.println("<td>" + rs.getString("a.service_name") + "</td>");
            out.println("<td>" + rs.getString("b.the_year") + "</td>");
            out.println("<td>" + rs.getString("b.the_month") + "</td>");
            out.println("<td>" + formatter.format(rs.getLong("b.target_revenue")) + "</td>");
            out.println("<td><a href=>Edit</a><a href=>Delete</a></td>");
            totalTarget += rs.getLong("b.target_revenue");
            out.println("</tr>");
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    out.println("</table>");
%>
