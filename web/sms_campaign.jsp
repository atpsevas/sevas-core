<%@page import="org.esme.dto.Service" %>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>::....</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--FoR Date Picker -->
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--FoR Date Picker -->
    <link href="css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.17.custom.js" type="text/javascript"></script>
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        function smsCampaignToggle(sel_box, tagToShow, tagToHide) {
            var state = document.getElementById(sel_box);
            var status = state.value;
            if (status == 'yes') {
                document.getElementById(tagToShow).style.display = 'block';
                document.getElementById(tagToHide).style.display = 'none';
            } else {
                document.getElementById(tagToShow).style.display = 'none';
                document.getElementById(tagToHide).style.display = 'block';
            }
        }
    </script>
    <script type="text/javascript">
        $(function () {
            $(".datepickr").datepicker();
        });
    </script>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li class="on"><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Campaign Scheduler</h2>
                    <form method="post" enctype="multipart/form-data" action="./broadcaster.do">
                        <c:set var="message" value="${requestScope.message}"/>
                        <c:choose>
                            <c:when test="${fn:contains(message, 'successfully')}">
                                <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                            </c:when>
                            <c:otherwise>
                                <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                            </c:otherwise>
                        </c:choose>
                        <div>
                            <%
                                ArrayList<Service> serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                            %>

                            <p>
                                <label for="subject">Campaign Service:</label><br/>
                                <select name="service" style="max-width: 180px;">
                                    <option value="all" selected="selected">Select Service</option>
                                    <%
                                        for (Service service : serviceCache) {
                                    %>
                                    <option value="<%=service.getServiceID()%>"><%=service.getServiceName().toUpperCase()%>
                                    </option>
                                    <%}%>

                                </select>
                            </p>
                            <p>
                                <label for="subject">Campaign Sender ID:</label><br/>
                                <input style="max-width: 160px;" type="text" name="senderid"/>
                            </p>
                            <p>
                                <label for="subject">Campaign Message:</label><br/>
                                <textarea style="max-width: 300px;" name="message" id="message" cols="20"
                                          rows="5"></textarea><br/>
                            </p>
                            <p>
                                <label for="subject">Campaign Target Subscribers:</label><br/>
                                <input style=" width: 160px;" type="file" name="attachment" id="attachment"
                                       onchange="document.getElementById('moreUploadsLink').style.display = 'block';"/>
                            </p>
                            <p id="modelP">
                                <label for="message">Schedule?</label><br/>
                                <select style=" width: 170px;" name="scdm" id="model"
                                        onchange="javascript:smsCampaignToggle('model', 'date', 'sendNow');">
                                    <option value="no">No</option>
                                    <option value="yes">Yes</option>
                                </select>
                            </p>
                            <p style="display: none" id="date">
                                <label>Campaign Date & Time</label><br/>
                                <input value="" style=" width: 75px;" type="text" name="date" class="datepickr"
                                       readonly/>
                                <select name="stob" id="aubject">
                                    <app:timeOfBlast/><br/>
                                </select>
                                <input style="background-color: #099; font-weight: bolder; color: #fff; max-width: 100px;"
                                       type="submit" value="SCHEDULE"/>
                            </p>
                            <p id="sendNow">
                                <input style="background-color: #099; font-weight: bolder; color: #fff; max-width: 100px;"
                                       type="submit" value="SEND NOW"/>
                            </p>
                        </div>
                    </form>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <ul class="links">
                                <li><a href="sms_campaign_mgr.jsp">Manage Existing Campaign</a></li>
                                <!--<li><a href="new_sub_service.jsp">New Subscription Service</a></li>
                                <li><a href="manage_sub_services.jsp">Manage Services</a></li>
                                <li><a href="manual_sub.jsp">Manual Subscription</a></li>
                                <li><a href="bulk_subscription.jsp">Bulk Subscription</a></li>-->
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
