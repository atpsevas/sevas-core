<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@include file="partials/header.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">
<head>
    <title>::...:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javaScript">
        // Nannette Thacker http://www.shiningstar.net
        function confirmSubmit() {
            var agree = confirm("Do you really want to delete this service?");
            if (agree)
                return true;
            else
                return false;
        }

        // -->
    </script>
</head>

<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li class="on"><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Manage Services</h2>
                    <c:set var="message" value="${requestScope.message}"/>
                    <c:choose>
                        <c:when test="${fn:contains(message, 'successfully')}">
                            <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:when>
                        <c:otherwise>
                            <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:otherwise>
                    </c:choose>

                    <hr/>
                    <label style="font-weight: bold; font-size: 14px;">No of Services: <%=totalService%>
                    </label>
                    <table width="100%" class="fancy">
                        <tr>
                            <th>ID</th>
                            <th>Date Created</th>
                            <th>Service Name</th>
                            <th>MO Short Code</th>
                            <th>MT Short Code</th>
                            <th>Billing Short Code</th>
                            <th>Main Keyword</th>
                            <th>Price</th>
                            <th>Manage Service</th>
                        </tr>

                        <%
                            for (Service service : serviceCache) {
                        %>
                        <tr>
                            <td><%=service.getServiceID()%>
                            </td>
                            <td><%=service.getDateCreated().substring(0, 10)%>
                            </td>
                            <td><%= service.getServiceName().toUpperCase()%>
                            </td>
                            <td><%= service.getServiceShortcode()%>
                            </td>
                            <td><%= service.getServiceMTShortCode()%>
                            </td>
                            <td><%= service.getServiceBillingShortCode()%>
                            </td>
                            <td><%= service.getServiceKeyword()%>
                            </td>
                            <td><%= service.getServicePrice()%>
                            </td>
                            <td>
                                <a href="sub_service_bill_update.jsp?id=<%=service.getServiceID()%>">Update Billing</a>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                    </table>
                    <div>
                    </div>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <ul class="links">
                                <li><a href="new_sub_service.jsp">New Subscription Service</a></li>
                                <li><a href="manage_sub_services.jsp">Manage Services</a></li>
                                <li><a href="manage_services_billing.jsp">Manage Services Billing</a></li>
                                <li><a href="bulk_subscription.jsp">Bulk Subscription</a></li>
                                <li><a href="sms_campaign.jsp">Campaign</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
        <app:footer/>
    </div>
</div>
</body>
</html>
