<%@page import="org.esme.dao.models.Network" %>
<%@page import="org.esme.dao.models.ShortCode" %>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>

<%@include file="partials/header.jsp" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>::..:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--[if IE]>
    <link href="css/ie-transparency.css" rel="stylesheet" type="text/css"/><![endif]-->
    <script type="text/javascript">
        function toggleAdd(sel_box, tagToShow) {
            var state = document.getElementById(sel_box);
            var status = state.value;
            if (status == 'preload') {
                document.getElementById(tagToShow).style.display = 'block';
            } else {
                document.getElementById(tagToShow).style.display = 'none';
            }
        }
        ;
    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#fb1hide").click(function () {
                $("#fb1").hide(1000);
            });

            $("#fb1show").click(function () {
                $("#fb1").show(1000);
            });
        });
    </script>
    <script type="text/javascript">
        function crossSellToggle(sel_box, tagToShow) {
            var state = document.getElementById(sel_box);
            var status = state.value;
            if (status == 'yes') {
                document.getElementById(tagToShow).style.display = 'block';
                document.getElementById(tagToHide).style.display = 'none';
            } else {
                document.getElementById(tagToShow).style.display = 'none';
                document.getElementById(tagToHide).style.display = 'block';
            }
        }
    </script>
</head>

<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li class="on"><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Update Service Billing Configuration</h2>
                    <c:set var="module" value="./updatesubservice_bill.do"/>
                    <p>
                    <hr/>
                    <label style="font-weight: bold; font-size: 14px;">Billing Configuration</label>
                    </p>
                    <form method="post" action="${module}">
                        <fieldset>
                            <legend>Service</legend>
                            <c:set var="message" value="${requestScope.message}"/>
                            <c:choose>
                                <c:when test="${fn:contains(message, 'successfully')}">
                                    <label style="color: green; font-weight: bold;"><c:out value="${message}"/> Click <a
                                            href="new_sub_service.jsp" style="text-decoration: none;">here </a> to
                                        create a new service.</label>
                                </c:when>
                                <c:otherwise>
                                    <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                                </c:otherwise>
                            </c:choose>
                            <input type="hidden" name="id" value="${param.id}"/>

                            <%
                                ArrayList<ShortCode> shortCodeCache = (ArrayList<ShortCode>) getServletContext().getAttribute("shortCodesCache");
                                ArrayList<Network> networks = (ArrayList<Network>) getServletContext().getAttribute("networksCache");

                                Service theService = null;
                                for (Service serv : serviceCache) {
                                    if (serv.getServiceID() == Long.parseLong(request.getParameter("id"))) {
                                        theService = serv;
                                        break;
                                    }
                                }
                            %>
                            <p>
                                <label for="subject">Service Name: (e.g Jokes)</label>
                                <input type="text" name="sn" value="<%=theService.getServiceName()%>"
                                       title="Name of your service e.g Jokes" readonly/>
                            </p>
                            <hr/>
                            <p>
                            <hr/>
                            <label style="font-weight: bold; font-size: 14px;">Main Billing</label>
                            </p>
                            <p class="theleft">
                                <label for="subject">Billing Type</label>
                                <select name="sst">
                                    <option selected
                                            value='<%=theService.getServiceSubType()%>'><%=theService.getServiceSubType()%>
                                    </option>
                                    }
                                    }%>
                                    <option value="UCIP">AIRTEL UCIP</option>
                                    <option value="EDB">ETISALAT Direct Billing</option>
                                    <option value="PSA">GLO PSA</option>
                                    <option value="MO">MO</option>
                                    <option value="MT">MT</option>
                                </select>
                            </p>
                            <p class="theright">
                                <label for="subject">Billing Network</label>
                                <select name="bill_net">
                                    <option selected
                                            value="<%=theService.getServiceBillingNetwork()%>"><%=theService.getServiceBillingNetwork()%>
                                    </option>
                                    }
                                    }%>
                                    <%
                                        for (Network net : networks) {
                                    %>
                                    <option value="<%=net.getNetworkID()%>"><%=net.getNetworkID()%>
                                    </option>
                                    <%}%>
                                </select>
                            </p>
                            <p class="theleft">
                                <label for="subject">Billing Short Code</label>
                                <select name="wc_ssc">
                                    <option selected
                                            value='<%=theService.getServiceBillingShortCode()%>'><%=theService.getServiceBillingShortCode()%>
                                    </option>
                                    }
                                    }%>
                                    <%
                                        for (ShortCode shortCode : shortCodeCache) {
                                    %>
                                    <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                    </option>
                                    <%}%>
                                </select>
                            </p>
                            <p class="theright">
                                <label for="subject">Price</label>
                                <select name="price">
                                    <option selected
                                            value='<%=theService.getServicePrice()%>'><%=theService.getServicePrice()%>
                                    </option>
                                    <option value="0">N0</option>
                                    <option value="5">N5</option>
                                    <option value="10">N10</option>
                                    <option value="15">N15</option>
                                    <option value="20">N20</option>
                                    <option value="25">N25</option>
                                    <option value="30">N30</option>
                                    <option value="50">N50</option>
                                    <option value="75">N75</option>
                                    <option value="100">N100</option>
                                    <option value="150">N150</option>
                                </select>
                            </p>
                            <p class="theleft">
                                <label for="subject">Expiry Period</label>
                                <select name="sp" id="aubject">
                                    <option selected
                                            value='<%=theService.getServicePeriod()%>'><%=theService.getServicePeriod()%>
                                    </option>
                                    <app:expiryPeriod/>
                                </select>
                            </p>
                            <p class="theright">
                                <label for="message">Renewal Message: </label><br/>
                                <textarea name="rm" cols="40"
                                          rows="5"><%=theService.getServiceRenewalMessage()%></textarea>
                            </p>
                            <p class="theleft">
                                <label for="subject">Send Notification</label>
                                <select name="notify" id="aubject">
                                    <option selected
                                            value='<%=theService.getMainbillingnotifySMSSetting()%>'><%=theService.getMainbillingnotifySMSSetting()%>
                                    </option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </p>
                            <p class="theright">
                                <label for="subject">Enable Instant Billing Retry: </label>
                                <select name="irb">
                                    <option selected
                                            value='<%=theService.getInstantBillingRetry()%>'><%=theService.getInstantBillingRetry()%>
                                    </option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </p>
                            <p class="theleft">
                                <label for="subject">Billing Time: </label>
                                <select name="sbt" id="aubject"
                                        title="At what time of the day should the system try to bill subscribers">
                                    <option selected
                                            value='<%=theService.getBillingTime()%>'><%=theService.getBillingTime()%>
                                    </option>
                                    <option value="0">0:00</option>
                                    <option value="1">1:00</option>
                                    <option value="2">2:00</option>
                                    <option value="3">3:00</option>
                                    <option value="4">4:00</option>
                                    <option value="5">5:00</option>
                                    <option value="6">6:00</option>
                                    <option value="7">7:00</option>
                                    <option value="8">8:00</option>
                                    <option value="9">9:00</option>
                                    <option value="10">10:00</option>
                                    <option value="11">11:00</option>
                                    <option value="12">12:00</option>
                                    <option value="13">13:00</option>
                                    <option value="14">14:00</option>
                                    <option value="15">15:00</option>
                                    <option value="16">16:00</option>
                                    <option value="17">17:00</option>
                                    <option value="18">18:00</option>
                                    <option value="19">19:00</option>
                                    <option value="20">20:00</option>
                                    <option value="21">21:00</option>
                                    <option value="22">22:00</option>
                                    <option value="23">23:00</option>
                                </select>
                            </p>
                            <p class="theright">
                                <label for="subject">Billing Retry Intervals: </label>
                                <select name="sbi" id="aubject">
                                    <option selected
                                            value='<%=theService.getBillingRetryIntervals()%>'><%=theService.getBillingRetryIntervals()%>
                                    </option>
                                    <app:billingIntervals/>
                                </select>
                            </p>
                            <p>
                            <hr/>
                            </p>
                            <p class="theleft">
                                <label style="font-weight: bold; font-size: 14px;">Fallback 1 Billing</label>
                                <select name="fb_ssc">
                                    <option selected
                                            value='<%=theService.getFallbackShortCode()%>'><%=theService.getFallbackShortCode()%>
                                    </option>
                                    <%
                                        for (ShortCode shortCode : shortCodeCache) {
                                    %>
                                    <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                    </option>
                                    <%}%>
                                </select>
                            </p>
                            <p class="theright">
                                <label for="subject">Fallback 1 Price</label>
                                <select name="fb_price">
                                    <option selected
                                            value='<%=theService.getFallbackPrice()%>'><%=theService.getFallbackPrice()%>
                                    </option>
                                    <app:priceTag/>
                                </select>
                            </p>
                            <p class="theleft">
                                <label for="subject">Fallback 1 Expiry Period</label>
                                <select name="fsp">
                                    <option selected
                                            value='<%=theService.getFallbackPeriod()%>'><%=theService.getFallbackPeriod()%>
                                    </option>
                                    <app:expiryPeriod/>
                                </select>
                            </p>
                            <p class="theright">
                                <label for="message">Fallback 1 Renewal Message: </label><br/>
                                <textarea name="frm" cols="40"
                                          rows="5"><%=theService.getServiceFallbackRenewalMessage()%></textarea>
                            </p>
                            <p class="theleft">
                                <label for="subject">Send Notification</label>
                                <select name="fb_notify" id="aubject">
                                    <option selected
                                            value='<%=theService.getFallBack1billingnotifySMSSetting()%>'><%=theService.getFallBack1billingnotifySMSSetting()%>
                                    </option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </p>
                            <p>
                            <hr/>
                            </p>
                            <p class="theleft">
                                <label style="font-weight: bold; font-size: 14px;">Fallback 2 Billing</label>
                                <select name="fb_ssc2">
                                    <option selected
                                            value='<%=theService.getFallbackShortCode2()%>'><%=theService.getFallbackShortCode2()%>
                                    </option>
                                    <%
                                        for (ShortCode shortCode : shortCodeCache) {
                                    %>
                                    <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                    </option>
                                    <%}%>
                                </select>
                            </p>
                            <p class="theright">
                                <label for="subject">Fallback 2 Price</label>
                                <select name="fb_price2">
                                    <option selected
                                            value='<%=theService.getFallbackPrice2()%>'><%=theService.getFallbackPrice2()%>
                                    </option>
                                    <app:priceTag/>
                                </select>
                            </p>
                            <p class="theleft">
                                <label for="subject">Fallback 2 Expiry Period</label>
                                <select name="fsp2">
                                    <option selected
                                            value='<%=theService.getFallbackPeriod2()%>'><%=theService.getFallbackPeriod2()%>
                                    </option>
                                    <app:expiryPeriod/>
                                </select>
                            </p>
                            <p class="theright">
                                <label for="message">Fallback 2 Renewal Message: </label><br/>
                                <textarea name="frm2" cols="40"
                                          rows="5"><%=theService.getServiceFallbackRenewalMessage2()%></textarea>
                            </p>
                            <p class="theleft">
                                <label for="subject">Send Notification</label>
                                <select name="fb2_notify" id="aubject">
                                    <option selected
                                            value='<%=theService.getFallBack2billingnotifySMSSetting()%>'><%=theService.getFallBack2billingnotifySMSSetting()%>
                                    </option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </p>
                            <p>
                            <hr/>
                            </p>
                            <p class="theleft">
                                <label style="font-weight: bold; font-size: 14px;">Fallback 3 Billing</label>
                                <select name="fb_ssc3">
                                    <option selected
                                            value='<%=theService.getFallbackShortCode3()%>'><%=theService.getFallbackShortCode3()%>
                                    </option>
                                    <%
                                        for (ShortCode shortCode : shortCodeCache) {
                                    %>
                                    <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                    </option>
                                    <%}%>
                                </select>
                            </p>
                            <p class="theright">
                                <label for="subject">Fallback 3 Price</label>
                                <select name="fb_price3">
                                    <option selected
                                            value='<%=theService.getFallbackPrice3()%>'><%=theService.getFallbackPrice3()%>
                                    </option>
                                    <app:priceTag/>
                                </select>
                            </p>
                            <p class="theleft">
                                <label for="subject">Fallback 3 Expiry Period</label>
                                <select name="fsp3" id="aubject">
                                    <option selected
                                            value='<%=theService.getFallbackPeriod3()%>'><%=theService.getFallbackPeriod3()%>
                                    </option>
                                    <app:expiryPeriod/>
                                </select>
                            </p>
                            <p class="theright">
                                <label for="message">Fallback 3 Renewal Message: </label><br/>
                                <textarea name="frm3" cols="40"
                                          rows="5"><%=theService.getServiceFallbackRenewalMessage3()%></textarea>
                            </p>
                            <p class="theleft">
                                <label for="subject">Send Notification</label>
                                <select name="fb3_notify" id="aubject">
                                    <option selected
                                            value='<%=theService.getFallBack3billingnotifySMSSetting()%>'><%=theService.getFallBack3billingnotifySMSSetting()%>
                                    </option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </p>
                            <p>
                            <hr/>
                            </p>
                            <p class="theleft">
                                <label style="font-weight: bold; font-size: 14px;">Fallback 4 Billing</label>
                                <select name="fb_ssc4">
                                    <option selected
                                            value='<%=theService.getFallbackShortCode4()%>'><%=theService.getFallbackShortCode4()%>
                                    </option>
                                    <%
                                        for (ShortCode shortCode : shortCodeCache) {
                                    %>
                                    <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                    </option>
                                    <%}%>
                                </select>
                            </p>
                            <p class="theright">
                                <label for="subject">Fallback 4 Price</label>
                                <select name="fb_price4">
                                    <option selected
                                            value='<%=theService.getFallbackPrice4()%>'><%=theService.getFallbackPrice4()%>
                                    </option>
                                    <app:priceTag/>
                                </select>
                            </p>
                            <p class="theleft">
                                <label for="subject">Fallback 4 Expiry Period</label>
                                <select name="fsp4" id="aubject">
                                    <option selected
                                            value='<%=theService.getFallbackPeriod4()%>'><%=theService.getFallbackPeriod4()%>
                                    </option>
                                    <app:expiryPeriod/>
                                </select>
                            </p>
                            <p class="theright">
                                <label for="message">Fallback 4 Renewal Message: </label><br/>
                                <textarea name="frm4" cols="40"
                                          rows="5"><%=theService.getServiceFallbackRenewalMessage4()%></textarea>
                            </p>
                            <p class="theleft">
                                <label for="subject">Send Notification</label>
                                <select name="fb4_notify" id="aubject">
                                    <option selected
                                            value='<%=theService.getFallBack4billingnotifySMSSetting()%>'><%=theService.getFallBack4billingnotifySMSSetting()%>
                                    </option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </p>
                            <p class="theleft">
                                <input type="submit" value="Update" class="btn"/>
                            </p>
                            <hr/>
                        </fieldset>
                    </form>
                    </table>
                    <div>
                    </div>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <ul class="links">
                                <li><a href="manage_sub_services.jsp">Manage Services</a></li>
                                <li><a href="manage_services_billing.jsp">Manage Services Billing</a></li>
                                <li><a href="manual_sub.jsp">Manual Subscription</a></li>
                                <li><a href="bulk_subscription.jsp">Bulk Subscription</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
        <app:footer/>
    </div>
</div>
</body>
</html>
