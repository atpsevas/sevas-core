<%@page import="org.esme.dao.models.Users" %>
<%@page import="java.sql.Connection" %>
<%@page import="java.sql.PreparedStatement" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.text.DecimalFormat" %>
<%@page import="java.util.ArrayList" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@include file="partials/header.jsp" %>


<%
    HttpSession sess = request.getSession(true);
    String user = (String) sess.getAttribute("userName");
    long userID = 0;
    String userRole = (String) sess.getAttribute("UserRole");

    ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
    for (Users us : usersCache) {
        if (us.getUserName().equals(user)) {
            userID = us.getId();
            break;
        }
    }

%>

<h3>Services Report Update</h3>
<!--Filter-->
<form action="" method="post">

    <!--<select name="service" 
            style=" width: 200px; height: 30px; font-size: 16px;  
            font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; 
            letter-spacing: -1px;" onchange="showReport(this.value)">
        <option>Select A Service</option>
    <%       
    out.println("I CALLED");
        for (Service service : serviceCache) {
            if (userRole.equals("Admin")) {
    %>
    <option value="<%=service.getServiceID()%>"><%=service.getServiceName()%></option>
    <%} else {
        if (userID == service.getServiceOwner()) {
    %>
    <option value="<%=service.getServiceID()%>"><%=service.getServiceName()%></option>

    <%}
            }
        }%>
</select>-->
</form>
<!--End of Filter-->
<b>
    <div id="showRep">
            <%

            Connection conn = null;
            PreparedStatement st = null;
            ResultSet rs;
            DecimalFormat formatter = new DecimalFormat("###,###,###");

            try {

                conn = (Connection) getServletContext().
                        getAttribute("database.connection");

                if (userRole.equals("Admin")) {
                    /*st = conn.prepareStatement((String) getServletContext().getInitParameter("dailyPaidContentRevenue"));*/
                    rs = (ResultSet) getServletContext().getAttribute("dailyPaidContent");
                } else {
                    st = conn.prepareStatement(
                            " select a.service_name as theService, date(b.sent_time) as theDate,"
                            + "sum((CASE WHEN (b.src_module = 'DailyPaidContent' ) THEN 1 ELSE 0 END)) AS totalContent,"
                            + "sum((CASE WHEN (b.src_module = 'DailyPaidContent'  and (b.delivery_status = 'SUBMITTED' or b.delivery_status = 'DELIVERED')) THEN 1 ELSE 0 END)) AS totalDel,"
                            + "sum((CASE WHEN (b.src_module = 'DailyPaidContent'  and (b.delivery_status = 'FAILED' or b.delivery_status = 'REJECTED')) THEN 1 ELSE 0 END)) AS failed,"
                            + "sum((CASE WHEN (b.src_module = 'DailyPaidContent' ) THEN 1 ELSE 0 END) * a.service_price) AS posRev,"
                            + "sum((CASE WHEN (b.src_module = 'DailyPaidContent'  and (b.delivery_status = 'SUBMITTED' or b.delivery_status = 'DELIVERED')) "
                            + "THEN 1 ELSE 0 END)* a.service_price) AS accRev,"
                            + "sum((CASE WHEN (b.src_module = 'DailyPaidContent'  and (b.delivery_status = 'FAILED' or b.delivery_status = 'REJECTED')) "
                            + "THEN 1 ELSE 0 END) * a.service_price) AS lossRev  FROM subscription_service a, delivered_outgoing_messages b "
                            + "where extract(month from b.sent_time) = extract (month from now()) "
                            + "and extract(year from b.sent_time) = extract (year from now()) "
                            + "and a.service_period = 0  and a.service_owner = " + userID + " and a.id = b.serv_id"
                            + " group by theService,theDate order by theService,theDate Desc");
                     rs = st.executeQuery();      
                }

                
                long totalContent = 0;
                long totalDelivered = 0;
                long totalFailed = 0;
                long possibleRevenue = 0;
                long totalAccruedRevenue = 0;
                long totallossRevenue = 0;
                String nextService = "";

                while (rs.next()) {

                    if (nextService.equalsIgnoreCase(rs.getString("theService").trim())) {
                    } else {

                        out.println("<table width='100%' class='fancy'>");
                        out.println("<tr colspan='9'>" + rs.getString("theService").toUpperCase() + "</tr>");
                        out.println("<tr>");
                        out.println("<th>Date</th>");
                        out.println("<th>Total Content</th>");
                        out.println("<th>Delivered</th>");
                        out.println("<th>Failed</th>");
                        out.println("<th>Possible Rev</th>");
                        out.println("<th>Accrued Rev</th>");
                        out.println("<th>Lost Rev</th>");
                        out.println("<th>Succ Rate</th>");
                        out.println("</tr>");
                    }

                    out.println("<tr>");
                    out.println("<td>" + rs.getString("theDate") + "</td>");
                    totalContent = totalContent + rs.getLong("totalContent");
                    out.println("<td>" + formatter.format(rs.getLong("totalContent")) + "</td>");
                    totalDelivered = totalDelivered + rs.getLong("totalDel");
                    out.println("<td>" + formatter.format(rs.getLong("totalDel")) + "</td>");
                    totalFailed = totalFailed + rs.getLong("failed");
                    out.println("<td>" + formatter.format(rs.getLong("failed")) + "</td>");
                    possibleRevenue = possibleRevenue + rs.getLong("posRev");
                    out.println("<td>" + formatter.format(rs.getLong("posRev")) + "</td>");
                    totalAccruedRevenue = totalAccruedRevenue + rs.getLong("accRev");
                    out.println("<td>" + formatter.format(rs.getLong("accRev")) + "</td>");
                    totallossRevenue = totallossRevenue + rs.getLong("lossRev");
                    out.println("<td>" + formatter.format(rs.getLong("lossRev")) + "</td>");
                    long percentage = 0;
                    try {
                        percentage = (rs.getLong("accRev") * 100) / rs.getLong("posRev");
                    } catch (Exception e) {
                    }
                    out.println("<td>" + percentage + "%</td>");
                    out.println("</tr>");
                    nextService = rs.getString("theService").trim();
                }

                out.println("</table>");
                out.println("<u><b> Report Summary</b></u>");
                out.println("<table width='100%' class='fancy'>");
                out.println("<tr>");
                out.println("<th>Report Date</th>");
                out.println("<th>Tot Content</th>");
                out.println("<th>Tot Delvd</th>");
                out.println("<th>Tot Failed</th>");
                out.println("<th>Possible Rev</th>");
                out.println("<th>Acc Rev</th>");
                out.println("<th>Lost Rev</th>");
                out.println("<th>Succ Rate</th>");
                out.println("</tr>");
                out.println("<tr style='font-weight:bold;'>");
                out.println("<td>Month till Date</td>");
                out.println("<td>" + formatter.format(totalContent) + "</td>");
                out.println("<td>" + formatter.format(totalDelivered) + "</td>");
                out.println("<td>" + formatter.format(totalFailed) + "</td>");
                out.println("<td>" + formatter.format(possibleRevenue) + "</td>");
                out.println("<td>" + formatter.format(totalAccruedRevenue) + "</td>");
                out.println("<td>" + formatter.format(totallossRevenue) + "</td>");
                long totalPercentage = 0;
                try {
                    totalPercentage = (totalAccruedRevenue * 100) / possibleRevenue;
                } catch (Exception e) {
                }
                out.println("<td>" + totalPercentage + "%</td>");
                out.println("</tr>");
                out.println("</table>");
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.println("</table>");
        %>
</b>
</div>

