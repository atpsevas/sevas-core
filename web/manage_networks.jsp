<%@page import="org.esme.dao.models.Network" %>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">
<head>
    <title>::...:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        // Nannette Thacker http://www.shiningstar.net
        function confirmSubmit() {
            var agree = confirm("Do you really want to delete this network?");
            if (agree)
                return true;
            else
                return false;
        }

        // -->
    </script>
</head>

<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li class="on"><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Manage Users</h2>
                    <c:set var="message" value="${requestScope.message}"/>
                    <c:choose>
                        <c:when test="${fn:contains(message, 'successfully')}">
                            <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:when>
                        <c:otherwise>
                            <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:otherwise>
                    </c:choose>
                    <%
                        ArrayList<Network> networkCache = (ArrayList<Network>) getServletContext().getAttribute("networksCache");

                    %>
                    <label style="font-weight: bold; font-size: 14px;">Total Networks:  <%=networkCache.size()%>
                    </label>
                    <hr/>
                    <table width="100%" class="fancy">
                        <tr>
                            <th>Network</th>
                            <th>Prefixes</th>
                            <th>Manage</th>
                        </tr>

                        <%
                            for (Network network : networkCache) {

                        %>
                        <tr>
                            <td><%=network.getNetworkID()%>
                            </td>
                            <td><%=network.getPrefixes()%>
                            </td>
                            <td>
                                <a onclick="return confirmSubmit()"
                                   href="./updatenetworks.do?action=delete&networkID=<%=network.getNetworkID()%>">Delete</a>
                                <a href="new_network.jsp?networkID=<%=network.getNetworkID()%>">View/Edit</a>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                    </table>
                </div>
                <div id="sub">
                    <h2>Manage Networks</h2>
                    <ul class="links">
                        <li><a href="new_network.jsp">Create New Network</a></li>
                        <li><a href="manage_networks.jsp">Manage Existing Networks</a></li>
                        <li><a href="./logout">[Logout]</a></li>
                    </ul>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
