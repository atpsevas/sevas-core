<%@page import="org.esme.dao.models.SubscriptionLogs" %>
<%@page import="org.esme.dao.models.Users" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page import="java.sql.Connection" %>
<%@page import="java.sql.PreparedStatement" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.util.ArrayList" %>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">
<head>
    <title>:::..:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--[if IE]>
    <link href="css/ie-transparency.css" rel="stylesheet" type="text/css"/><![endif]-->
    <!--FoR Date Picker -->
    <link href="css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.17.custom.js" type="text/javascript"></script>
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="js/reportSupport.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $(".datepickr").datepicker();
        });

    </script>
    <script>
        var auto_refresh = setInterval(
            function () {
                $('#main').fadeOut('slow').load('log_helper.jsp').fadeIn("slow");
            }, 20000);
    </script>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Live Traffic</h2>
                    <%
                        HttpSession sess = request.getSession(true);
                        String user = (String) sess.getAttribute("userName");
                        long userID = 0;
                        String userRole = (String) sess.getAttribute("UserRole");
                        ArrayList<SubscriptionLogs> logs = null;
                        SubscriptionLogs log = null;
                        Connection conn = (Connection) getServletContext().
                                getAttribute("database.connection");

                        ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
                        for (Users us : usersCache) {
                            if (us.getUserName().equals(user)) {
                                userID = us.getId();
                            }
                        }

                        if (userRole.equals("Admin")) {
                            logs = new SubscriptionLogs().queryLogs(conn, 0);
                        } else {
                            logs = new ArrayList<SubscriptionLogs>();
                            ResultSet rsPagination = null;
                            PreparedStatement psPagination = null;

                            try {

                                String network = "";
                                String sqlPagination = "";
                                boolean queryByNetwork = false;

                                if (userRole.equalsIgnoreCase("mtn")) {
                                    network = "mtn";
                                    queryByNetwork = true;
                                } else if (userRole.equalsIgnoreCase("airtel")) {
                                    network = "airtel";
                                    queryByNetwork = true;
                                } else if (userRole.equalsIgnoreCase("glo")) {
                                    network = "glo";
                                    queryByNetwork = true;
                                } else if (userRole.equalsIgnoreCase("etisalat")) {
                                    network = "etisalat";
                                    queryByNetwork = true;
                                }

                                if (!queryByNetwork) {
                                    sqlPagination = "SELECT a.service_name, b.sub_request_time,sub_request_text,"
                                            + " sub_network,sub_shortcode,sub_msisdn, "
                                            + " sub_response_text, b.sub_expiry_date FROM subscription_service a, "
                                            + " subscription b where date(b.sub_request_time) = date(CURRENT_TIMESTAMP) "
                                            + " and extract(month from b.sub_request_time) = extract (month from now()) "
                                            + " and extract(year from b.sub_request_time) = extract (year from now()) "
                                            + " and a.service_owner = " + userID + " "
                                            + " and a.id = b.sub_service_id ORDER BY sub_request_time desc ";
                                } else {
                                    sqlPagination = "SELECT a.service_name, b.sub_request_time,sub_request_text,"
                                            + " sub_network,sub_shortcode,sub_msisdn, "
                                            + " sub_response_text, b.sub_expiry_date FROM subscription_service a, "
                                            + " subscription b where date(b.sub_request_time) = date(CURRENT_TIMESTAMP) "
                                            + " and extract(month from b.sub_request_time) = extract (month from now()) "
                                            + " and extract(year from b.sub_request_time) = extract (year from now()) "
                                            + " and a.service_network ilike '%" + network + "% "
                                            + " and a.id = b.sub_service_id ORDER BY sub_request_time desc ";
                                }

                                psPagination = conn.prepareStatement(sqlPagination);
                                rsPagination = psPagination.executeQuery();

                                while (rsPagination.next()) {
                                    log = new SubscriptionLogs();
                                    log.setRequestTime(rsPagination.getString("sub_request_time"));
                                    log.setService(rsPagination.getString("service_name"));
                                    log.setRequestText(rsPagination.getString("sub_request_text"));
                                    log.setNetwork(rsPagination.getString("sub_network"));
                                    log.setShortCode(rsPagination.getString("sub_shortcode"));
                                    log.setMsisdn(rsPagination.getString("sub_msisdn"));
                                    log.setResponseText(rsPagination.getString("sub_response_text"));
                                    log.setActiveTill(rsPagination.getString("sub_expiry_date"));
                                    logs.add(log);
                                }

                            } catch (SQLException ex) {
                                ex.printStackTrace();
                            } finally {
                                try {
                                    psPagination.close();
                                    rsPagination.close();
                                } catch (SQLException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    %>

                    <b>Total Result  <%=logs.size()%>
                    </b>
                    <hr/>
                    <form name="frm">
                        <table width='100%' class="fancy">
                            <tr>
                                <th>Request Time</th>
                                <th>Network</th>
                                <th>Short Code</th>
                                <th>Service</th>
                                <th>MSISDN</th>
                                <th>Request Text</th>
                                <th>Response Text</th>
                                <th>Active Till</th>
                            </tr>
                            <%
                                try {
                                    for (SubscriptionLogs tlog : logs) {
                            %>
                            <tr>
                                <td><%=tlog.getRequestTime().substring(0, tlog.getRequestTime().lastIndexOf("."))%>
                                </td>
                                <td><%=tlog.getNetwork()%>
                                </td>
                                <td><%=tlog.getShortCode()%>
                                </td>
                                <td><%=tlog.getService()%>
                                </td>
                                <td><%=tlog.getMsisdn()%>
                                </td>
                                <td><%=tlog.getRequestText()%>
                                </td>
                                <td><%=tlog.getResponseText()%>
                                </td>
                                <td><%=tlog.getActiveTill()%>
                                </td>
                            </tr>
                            <%
                                    }
                                } catch (Exception e) {
                                }
                            %>
                        </table>
                    </form>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>iReport</h2>
                            <ul class="links">
                                <!--<li><a href="#" onclick="showUser('new_revenue_target.jsp', this.value)">Set Monthly Target Revenue</a></li>
                                <li><a href="#" onclick="showUser('update_revenue_target.jsp', this.value)">Update Monthly Target Revenue</a></li>
                                <li><a href="revTarget.csv" >Download Rev Target Template</a></li>-->
                                <li><a href="iReport.do">iReport</a></li>
                                <!--<li><a href="logs.jsp" >Live Traffic</a></li>
                                        <li><a href="./LogsCSVDownload.csv" >Download Today's Traffic Logs</a></li>
                                        <li><a href="#" onclick="showUser('report_mgr.jsp', this.value)">Subscription Report</a></li>
                                        <c:choose>
                                            <c:when test="${sessionScope.UserRole ==  'Admin'}">
                                            <li><a href="#" onclick="showUser('report_rev_mgr.jsp', this.value)">Services Revenue Report</a></li>
                                            </c:when>
                                        </c:choose>
                                    <li><a href="#" onclick="showUser('content_report_mgr.jsp', this.value)">Content Delivery Report</a></li>-->
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                            <h2>Filter Report</h2>
                            <form action="./LogsCSVDownload.csv">
                                <p>
                                    <label>Start Date</label>
                                    <input value="Click here to set date"
                                           style=" width: 180px; height: 30px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;"
                                           type="text" id="start" name="start" class="datepickr"></input><br/>
                                    <label>End Date</label>
                                    <input value="Click here to set date"
                                           style=" width: 180px; height: 30px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;"
                                           type="text" id="end" name="end" class="datepickr" readonly></input>
                                    <input type="submit" value="Download"
                                           style="background-color: #099; font-weight: bolder; color: #fff;"/>
                                </p>
                            </form>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
