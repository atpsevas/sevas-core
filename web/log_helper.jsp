<%@page import="org.esme.dao.models.SubscriptionLogs" %>
<%@page import="org.esme.dao.models.Users" %>
<%@page import="java.sql.Connection" %>
<%@page import="java.sql.PreparedStatement" %>
<%@page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.util.ArrayList" %>

<h2>Live Traffic</h2>
<%
    HttpSession sess = request.getSession(true);
    String user = (String) sess.getAttribute("userName");
    long userID = 0;
    String userRole = (String) sess.getAttribute("UserRole");
    ArrayList<SubscriptionLogs> logs = null;
    SubscriptionLogs log = null;
    Connection conn = (Connection) getServletContext().
            getAttribute("database.connection");

    ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
    for (Users us : usersCache) {
        if (us.getUserName().equals(user)) {
            userID = us.getId();
        }
    }

    if (userRole.equals("Admin")) {
        logs = new SubscriptionLogs().queryLogs(conn, 0);
    } else {
        logs = new ArrayList<SubscriptionLogs>();
        ResultSet rsPagination = null;
        PreparedStatement psPagination = null;

        try {

            String sqlPagination = "SELECT a.service_name, b.sub_request_time,sub_request_text,"
                    + " sub_network,sub_shortcode,sub_msisdn, "
                    + " sub_response_text, b.sub_expiry_date FROM subscription_service a, "
                    + " subscription b where date(b.sub_request_time) = date(CURRENT_TIMESTAMP) "
                    + " and extract(month from b.sub_request_time) = extract (month from now()) "
                    + " and extract(year from b.sub_request_time) = extract (year from now()) "
                    + " and a.service_owner = " + userID + " "
                    + " and a.id = b.sub_service_id ORDER BY sub_request_time desc ";

            psPagination = conn.prepareStatement(sqlPagination);
            rsPagination = psPagination.executeQuery();

            while (rsPagination.next()) {
                log = new SubscriptionLogs();
                log.setRequestTime(rsPagination.getString("sub_request_time"));
                log.setService(rsPagination.getString("service_name"));
                log.setRequestText(rsPagination.getString("sub_request_text"));
                log.setNetwork(rsPagination.getString("sub_network"));
                log.setShortCode(rsPagination.getString("sub_shortcode"));
                log.setMsisdn(rsPagination.getString("sub_msisdn"));
                log.setResponseText(rsPagination.getString("sub_response_text"));
                log.setActiveTill(rsPagination.getString("sub_expiry_date"));
                logs.add(log);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                psPagination.close();
                rsPagination.close();
            } catch (SQLException ex) {
            }
        }
    }
%>
<b>Total Result  <%=logs.size()%>
</b>
<hr/>
<form name="frm">
    <table width='100%' class="fancy">
        <tr>
            <th>Request Time</th>
            <th>Network</th>
            <th>Short Code</th>
            <th>Service</th>
            <th>MSISDN</th>
            <th>Request Text</th>
            <th>Response Text</th>
            <th>Active Till</th>
        </tr>
        <%
            try {
                for (SubscriptionLogs tlog : logs) {
        %>
        <tr>
            <td><%=tlog.getRequestTime().substring(0, tlog.getRequestTime().lastIndexOf("."))%>
            </td>
            <td><%=tlog.getNetwork()%>
            </td>
            <td><%=tlog.getShortCode()%>
            </td>
            <td><%=tlog.getService()%>
            </td>
            <td><%=tlog.getMsisdn()%>
            </td>
            <td><%=tlog.getRequestText()%>
            </td>
            <td><%=tlog.getResponseText()%>
            </td>
            <td><%=tlog.getActiveTill()%>
            </td>
        </tr>
        <%
                }
            } catch (Exception e) {
            }
        %>
    </table>
</form>