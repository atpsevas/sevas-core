<%@page import="java.sql.Connection" %>
<%@page import="java.sql.PreparedStatement" %>
<%@page import="java.sql.ResultSet" %>
<%

    String start = "";
    String end = "";


    out.println("<table width='100%' class='fancy'>");
    out.println("<tr>");
    //out.println("<th>Day</th>");
    out.println("<th>Report Date</th>");
    out.println("<th>New Act Sub</th>");
    out.println("<th>Tot Act Sub</th>");
    out.println("<th>New Unsub</th>");
    out.println("<th>Tot Unsub</th>");
    out.println("<th>New Charged</th>");
    out.println("<th>Tot Charged</th>");
    out.println("<th>Repeat Charged</th>");
    out.println("<th>New Charged (N100)</th>");
    out.println("<th>New Charged (N50)</th>");
    out.println("</tr>");
    Connection conn = null;
    try {
        start = request.getParameter("start");
        end = request.getParameter("end");
        if (start.contains("Click") || start.equalsIgnoreCase("") || end.contains("Click") || end.equalsIgnoreCase("")) {
            out.println("<div style='border: 1px solid #BE7;background-color: #F8FFF0;padding: 10px;font-family: serif;font-size: 13px;font-weight: 400;font-style: normal;color: red;'>Invalid date selected.</div>");
            return;
        } else {
            out.println("<div style='border: 1px solid #BE7;background-color: #F8FFF0;padding: 10px;font-family: serif;font-size: 13px;font-weight: 400;font-style: normal;color: Green;'>Report between " + start + " and " + end + " </div>");
        }

        conn = (Connection) getServletContext().
                getAttribute("database.connection");
        PreparedStatement st = conn.prepareStatement(
                "select date(rev_date) as theDate, "
                        + "new_active_sub,total_active_sub, new_unsub,"
                        + "total_unsub,new_charged,total_charged,repeat_charged,"
                        + "new_main_charged,new_fallback_charged "
                        + "from rev_report_by_shortcode "
                        + " where date(rev_date) between '" + start + "' and '" + end + "' "
                        + "order by rev_date desc");
        ResultSet rs = st.executeQuery();

        while (rs.next()) {
            out.println("<tr>");
            //out.println("<td>" + scReport.getReportDay() + "</td>");
            out.println("<td>" + rs.getString("theDate") + "</td>");
            out.println("<td>" + rs.getString("new_active_sub") + "</td>");
            out.println("<td>" + rs.getString("total_active_sub") + "</td>");
            out.println("<td>" + rs.getString("new_unsub") + "</td>");
            out.println("<td>" + rs.getString("total_unsub") + "</td>");
            out.println("<td>" + rs.getString("new_charged") + "</td>");
            out.println("<td>" + rs.getString("total_charged") + "</td>");
            out.println("<td>" + rs.getString("repeat_charged") + "</td>");
            out.println("<td>" + rs.getString("new_main_charged") + "</td>");
            out.println("<td>" + rs.getString("new_fallback_charged") + "</td>");
            out.println("</tr>");
        }
    } catch (Exception e) {
        out.println("<label style='color:red; font-weight:bold;'>Error occured while querying report. Please try again or contact the administrator</label>");
        e.printStackTrace();
    }
    out.println("</table>");
%>
