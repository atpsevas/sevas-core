<%@page import="org.esme.dao.models.Content" %>
<%@page import="org.esme.dao.models.SequentialContent" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="org.esme.dao.models.Users" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@ page import="java.util.ArrayList" %>

<%@include file="partials/header.jsp" %>

<%

    HttpSession sess = request.getSession(true);
    String user = (String) sess.getAttribute("userName");
    long userID = 0;
    String userRole = (String) sess.getAttribute("UserRole");
    long contentServiceID = Long.parseLong(request.getParameter("service"));

    ArrayList<Content> content = null;
    ArrayList<SequentialContent> seqContent = null;

    boolean itSequential = false;

    for (Service nextService : serviceCache) {
        if (nextService.getServiceID() == contentServiceID
                && nextService.getServiceContentDeliveryMethod().equalsIgnoreCase("sequential")) {
            seqContent = (ArrayList<SequentialContent>) getServletContext().getAttribute("seqContentCache");
            itSequential = true;
            break;
        } else if (nextService.getServiceID() == contentServiceID) {
            content = (ArrayList<Content>) getServletContext().getAttribute("contentCache");
            break;
        }
    }

    ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
    for (Users us : usersCache) {
        if (us.getUserName().equals(user)) {
            userID = us.getId();
            break;
        }
    }

    out.println("<form method='post' action='./updatecontent.do'>");
    out.println("<input type='submit'  onclick = 'return confirmSubmit()' style=' margin-left: 773px; background-color: #099; font-weight: bolder; color: #fff; max-width: 100px;' value='Delete'/>");
    out.println("<table width='100%' class='fancy'>");
    out.println("<tr>");
    out.println("<td><input type=checkbox name='checkall' value='' onClick='toggle(this)'> </td>");
    out.println("<th>ID</th>");
    out.println("<th>Date - Time Uploaded</th>");
    out.println("<th>Content Owner</th>");
    out.println("<th>Service Name</th>");
    out.println("<th>Content</th>");
    out.println("<th>Scheduled</th>");
    out.println("<th>Manage</th>");
    //out.println("<th>Delete</th>");
    out.println("</tr>");

    if (itSequential) {
        for (SequentialContent con : seqContent) {
            if (userRole.equalsIgnoreCase("Admin") && con.getContentService() == contentServiceID) {
                out.println("<tr>");
                out.println("<td><input type='checkbox' name='check' value=" + con.getContent_id() + "> </td>");
                out.println("<td>" + con.getContent_id() + "</td>");
                out.println("<td>" + con.getDateUploaded() + "</td>");
                out.println("<td>" + con.getContentOwner() + "</td>");
                out.println("<td>" + con.getContentServiceName() + "</td>");
                out.println("<td>" + con.getContent() + "</td>");
                out.println("<td>" + con.getContentBlastDay() + "</td>");
                out.println("<td><a href='update_content.jsp?id=" + con.getContent_id() + "&cs=" + con.getContentService() + "&con=" + con.getContent() + "&dob=" + con.getContentBlastDay() + "'>View/Edit</a></td>");
                out.println("</tr>");

            } else {

                if (con.getContentOwner() == userID && con.getContentService() == contentServiceID) {

                    out.println("<tr>");
                    out.println("<td><input type='checkbox' name='check' value=" + con.getContent_id() + "> </td>");
                    out.println("<td>" + con.getContent_id() + "</td>");
                    out.println("<td>" + con.getDateUploaded() + "</td>");
                    out.println("<td>" + con.getContentOwner() + "</td>");
                    out.println("<td>" + con.getContentServiceName() + "</td>");
                    out.println("<td>" + con.getContent() + "</td>");
                    out.println("<td>" + con.getContentBlastDay() + "</td>");
                    out.println("<td><a href='update_content.jsp?id=" + con.getContent_id() + "&cs=" + con.getContentService() + "&con=" + con.getContent() + "&dob=" + con.getContentBlastDay() + "'>View/Edit</a></td>");
                    out.println("</tr>");
                }
            }
        }
    } else {
        for (Content con : content) {
            if (userRole.equalsIgnoreCase("Admin") && con.getContentService() == contentServiceID) {
                out.println("<tr>");
                out.println("<td><input type='checkbox' name='check' value=" + con.getContent_id() + "> </td>");
                out.println("<td>" + con.getContent_id() + "</td>");
                out.println("<td>" + con.getDateUploaded() + "</td>");
                out.println("<td>" + con.getContentOwner() + "</td>");
                out.println("<td>" + con.getContentServiceName() + "</td>");
                if (con.getContent().endsWith(".mp3")) {
                    out.println("<td><audio autoplay='autoplay' controls='controls'><source src='" + con.getContent() + "' /> " + con.getContent() + "</audio> </td>");
                } else {
                    out.println("<td>" + con.getContent() + "</td>");
                }
                out.println("<td>" + con.getConentDOB() + "</td>");
                out.println("<td><a href='update_content.jsp?id=" + con.getContent_id() + "&cs=" + con.getContentService() + "&con=" + con.getContent() + "&dob=" + con.getConentDOB() + "&servName=" + con.getContentServiceName() + "'>View/Edit</a></td>");
                out.println("</tr>");
            } else {
                if (con.getContentOwner() == userID && con.getContentService() == contentServiceID) {
                    out.println("<tr>");
                    out.println("<td><input type='checkbox' name='check' value=" + con.getContent_id() + "> </td>");
                    out.println("<td>" + con.getContent_id() + "</td>");
                    out.println("<td>" + con.getDateUploaded() + "</td>");
                    out.println("<td>" + con.getContentOwner() + "</td>");
                    out.println("<td>" + con.getContentServiceName() + "</td>");
                    if (con.getContent().endsWith(".mp3")) {
                        out.println("<td><audio autoplay='autoplay' controls='controls'><source src='" + con.getContent() + "' /> " + con.getContent() + "</audio> </td>");
                    } else {
                        out.println("<td>" + con.getContent() + "</td>");
                    }
                    out.println("<td>" + con.getConentDOB() + "</td>");
                    out.println("<td><a href='update_content.jsp?id=" + con.getContent_id() + "&cs=" + con.getContentService() + "&con=" + con.getContent() + "&dob=" + con.getConentDOB() + "&servName=" + con.getContentServiceName() + "'>View/Edit</a></td>");
                    out.println("</tr>");
                }
            }
        }
    }
    out.println("</table>");
    out.println("</form>");
%>

