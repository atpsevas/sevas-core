<%@page import="org.esme.dao.models.Users" %>
<%@page import="java.sql.Connection" %>
<%@page import="java.sql.PreparedStatement" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.text.DecimalFormat" %>
<%@ page import="java.util.ArrayList" %>
<%

    out.println("<table width='100%' class='fancy'>");
    out.println("<tr>");
    out.println("<th>Report Date</th>");
    out.println("<th>New Sub</th>");
    out.println("<th>New Sub Main Charge</th>");
    out.println("<th>Sub Fallback Charge</th>");
    out.println("<th>New Sub Tot Rev</th>");
    out.println("<th>Total Renewed</th>");
    out.println("<th>Renewal Main Charge</th>");
    out.println("<th>Renewal Fallback Charge</th>");
    out.println("<th>Renewal Total Rev</th>");
    out.println("</tr>");
    Connection conn = null;

    HttpSession sess = request.getSession(true);
    String user = (String) sess.getAttribute("userName");
    long userID = 0;
    String userRole = (String) sess.getAttribute("UserRole");

    ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
    for (Users us : usersCache) {
        if (us.getUserName().equals(user)) {
            userID = us.getId();
            break;
        }
    }

    try {

        Long serviceID = Long.parseLong(request.getParameter("service"));
        conn = (Connection) getServletContext().
                getAttribute("database.connection");

        PreparedStatement st = null;
        if (userRole.equals("Admin")) {
            st = conn.prepareStatement(
                    (String) getServletContext().getInitParameter("dailyServiceRevenue"));
            st.setLong(1, serviceID);
        } else {
            st = conn.prepareStatement(
                    (String) getServletContext().getInitParameter("dailyServiceRevenue_User"));
            st.setLong(1, userID);
            st.setLong(2, serviceID);
        }

        ResultSet rs = st.executeQuery();
        long totalNewSub = 0;
        long totalNewSubMainCharge = 0;
        long totalNewSubFallBackCharge = 0;
        long totalNewSubCharged = 0;
        long totalRenew = 0;
        long totalRenSubMainCharge = 0;
        long totalRenSubFallBackCharge = 0;
        long totalRenSubCharged = 0;
        long newSubTotalRev = 0;
        DecimalFormat formatter = new DecimalFormat("###,###,###");

        while (rs.next()) {

            newSubTotalRev = 0;
            out.println("<tr>");
            out.println("<td>" + rs.getString("theDate") + "</td>");
            out.println("<td>" + formatter.format(rs.getLong("new_sub")) + "</td>");
            totalNewSub += rs.getLong("new_sub");
            out.println("<td>" + formatter.format(rs.getLong("new_sub_main_charge")) + "</td>");
            totalNewSubMainCharge += rs.getLong("new_sub_main_charge");
            out.println("<td>" + formatter.format(rs.getLong("new_sub_fallback_charge")) + "</td>");
            totalNewSubFallBackCharge += rs.getLong("new_sub_fallback_charge");
            newSubTotalRev = rs.getLong("new_sub_main_charge") + rs.getLong("new_sub_fallback_charge");
            out.println("<td>" + formatter.format(newSubTotalRev) + "</td>");
            totalNewSubCharged += newSubTotalRev;
            out.println("<td>" + formatter.format(rs.getLong("renewed")) + "</td>");
            totalRenew = +rs.getLong("renewed");
            out.println("<td>" + formatter.format(rs.getLong("renew_main_charge")) + "</td>");
            totalRenSubMainCharge += rs.getLong("renew_main_charge");
            out.println("<td>" + formatter.format(rs.getLong("renew_fallback_charge")) + "</td>");
            totalRenSubFallBackCharge += rs.getLong("renew_fallback_charge");
            newSubTotalRev = rs.getLong("renew_main_charge") + rs.getLong("renew_fallback_charge");
            out.println("<td>" + formatter.format(newSubTotalRev) + "</td>");
            totalRenSubCharged += newSubTotalRev;
            out.println("</tr>");
        }

        out.println("<tr style='font-weight:bold;'>");
        out.println("<td>Total</td>");
        out.println("<td>" + formatter.format(totalNewSub) + "</td>");
        out.println("<td>" + formatter.format(totalNewSubMainCharge) + "</td>");
        out.println("<td>" + formatter.format(totalNewSubFallBackCharge) + "</td>");
        out.println("<td>" + formatter.format(totalNewSubCharged) + "</td>");
        out.println("<td>" + formatter.format(totalRenew) + "</td>");
        out.println("<td>" + formatter.format(totalRenSubMainCharge) + "</td>");
        out.println("<td>" + formatter.format(totalRenSubFallBackCharge) + "</td>");
        out.println("<td>" + formatter.format(totalRenSubCharged) + "</td>");
        out.println("</tr>");

    } catch (Exception e) {
        e.printStackTrace();
    }
    out.println("</table>");
%>
