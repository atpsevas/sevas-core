<%@page import="java.lang.management.ManagementFactory" %>
<%@page import="java.lang.management.RuntimeMXBean" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>::...:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--[if IE]>
    <link href="css/ie-transparency.css" rel="stylesheet" type="text/css"/><![endif]-->
</head>

<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="on"><a href="start.jsp">Home</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Main</h2>
                    <p>
                        This platform is a ready-to-launch mobile revenue generating platform,
                        which allows Mobile operators and content provider to easily design,
                        upload and distribute various contents for mobile subscribers.
                    </p>

                    <p>Operators and content providers can launch mobile value added services with minimal or NO
                        technical/marketing resources.</p>
                    <p>The platform is a very stable platform with high uptime, built on proven web and mobile
                        technologies.</p>
                </div>
                <div id="sub">
                    <%
                        RuntimeMXBean rb = ManagementFactory.getRuntimeMXBean();
                        long uptime = rb.getUptime();
                        String hms = String.format("%02dH,%02dM,%02dS", TimeUnit.MILLISECONDS.toHours(uptime),
                                TimeUnit.MILLISECONDS.toMinutes(uptime) % TimeUnit.HOURS.toMinutes(1),
                                TimeUnit.MILLISECONDS.toSeconds(uptime) % TimeUnit.MINUTES.toSeconds(1));
                    %>
                    <label style="text-transform: uppercase; font-size: 14px;font-weight: bold; color: maroon;">UPTIME: <%=hms%>
                    </label>
                    <h2>Welcome, <c:out value="${sessionScope.userName}"/></h2>
                    <ul class="links">

                        <c:choose>
                            <c:when test="${sessionScope.UserRole ==  'Admin'}">
                                <li><a href="new_account.jsp">New Account</a></li>
                                <li><a href="new_network.jsp">Network</a></li>
                                <li><a href="shortcode.jsp">Short Code</a></li>
                                <li><a href="services.jsp">Services</a></li>
                                <li><a href="sms_campaign.jsp">Campaign</a></li>
                                <li><a href="contents.jsp">Content</a></li>
                                <li><a href="iReport.do">Report</a></li>
                                <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                                <li><a href="gateway.jsp">SMSC</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </c:when>
                            <c:when test="${sessionScope.UserRole ==  'Content'}">
                                <li class="first"><a href="start.jsp">Home</a></li>
                                <li class="on"><a href="contents.jsp">Content</a></li>
                                <li><a href="iReport.do">iReport</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </c:when>
                            <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                                <li><a href="manage_blacklist.jsp">Manage Blacklisted MSISDN</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </c:when>
                            <c:otherwise>
                                <li><a href="./logout">[Logout]</a></li>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
