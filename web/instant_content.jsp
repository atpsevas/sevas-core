<%@page import="org.esme.dao.models.Users" %>
<%@page import="javax.servlet.http.HttpSession" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="java.util.ArrayList" %>

<%@include file="partials/header.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <title>::..:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--FoR Date Picker -->
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--FoR Date Picker -->
    <link href="css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.17.custom.js" type="text/javascript"></script>
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="js/contentSupport.js" type="text/javascript"></script>

    <script>
        $(document).ready(function () {

            //Stops the submit request
            $("#myAjaxRequestForm").submit(function (e) {
                e.preventDefault();
            });

            //checks for the button click event
            $("#submit").click(function (e) {

                //get the form data and then serialize that
                dataString = $("#myAjaxRequestForm").serialize();

                //get the form data using another method
                var serviceID = $("#subject").val();

                var theContent = $("#message").val();

                dataString = "serv=" + serviceID + "&sd=" + theContent;

                //make the AJAX request, dataType is set to json
                //meaning we are expecting JSON data in response from the server
                $.ajax({
                    type: "POST",
                    url: "instantcontentupload.do",
                    data: dataString,
                    dataType: "text",
                    //if received a response from the server
                    success: function () {
                        $('#succ').text(
                            'Content processed');
                        alert('Content Processed');


                    },
                    //If there was no resonse from the server
                    error: function () {
                        $('#fail').text(
                            'System error.');
                        alert('System Error');
                    }
                });

                $('#succ').text(
                    'Content successfully sent for processing');
                $("#message").val() = "";
            });

        });
    </script>
    <script type="text/javascript">
        $(function () {
            $(".datepickr").datepicker();
        });

    </script>
    <script>
        function toggleCMD(sel_box, tagToHide) {
            var state = document.getElementById(sel_box);
            var status = state.value;
            if (status == 'sequential') {
                document.getElementById(tagToHide).style.display = 'none';
            } else {
                document.getElementById(tagToHide).style.display = 'block';
            }
        }
    </script>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'mtn'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'etisalat'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'glo'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'airtel'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Send Instant Content</h2>
                    <div id="succ" style="color:green;font-weight: bold;"></div>
                    <div id="fail" style="color: red;font-weight: bold;"></div>
                    <div id="msg">Please select a service and enter the content below.</div>
                    <form method="post" id="myAjaxRequestForm">
                        <c:set var="message" value="${requestScope.message}"/>
                        <c:choose>
                            <c:when test="${fn:contains(message, 'successfully')}">
                                <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                            </c:when>
                            <c:otherwise>
                                <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                            </c:otherwise>
                        </c:choose>
                        <%
                            HttpSession sess = request.getSession(true);
                            String user = (String) sess.getAttribute("userName");
                            long userID = 0;
                            String userRole = (String) sess.getAttribute("UserRole");
                            ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
                            for (Users us : usersCache) {
                                if (us.getUserName().equals(user)) {
                                    userID = us.getId();
                                }
                            }

                        %>
                        <p>
                            <option value="">SELECT SERVICE</option>
                            <select style=" width: 200px; height: 40px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;"
                                    name="serv" id="subject">
                                <% for (Service theService : serviceCache) {
                                    if (userRole.equalsIgnoreCase("Admin")) {

                                %>
                                <option value="<%=theService.getServiceID()%>"><%=theService.getServiceName().toUpperCase()%>
                                </option>
                                <%
                                } else if (userRole.contains("mtn")) {
                                    if (theService.getServiceNetwork().contains("mtn")) {
                                %>
                                <option value="<%=theService.getServiceID()%>"><%=theService.getServiceName().toUpperCase()%>
                                </option>
                                <%
                                    }
                                } else if (userRole.contains("etisalat")) {
                                    if (theService.getServiceNetwork().contains("etisalat")) {
                                %>
                                <option value="<%=theService.getServiceID()%>"><%=theService.getServiceName().toUpperCase()%>
                                </option>
                                <%
                                    }
                                } else if (userRole.contains("airtel")) {
                                    if (theService.getServiceNetwork().contains("airtel")) {
                                %>
                                <option value="<%=theService.getServiceID()%>"><%=theService.getServiceName().toUpperCase()%>
                                </option>
                                <%
                                    }
                                } else if (userRole.contains("glo")) {
                                    if (theService.getServiceNetwork().contains("glo")) {
                                %>
                                <option value="<%=theService.getServiceID()%>"><%=theService.getServiceName().toUpperCase()%>
                                </option>
                                <%
                                    }
                                } else {
                                    if (theService.getServiceOwnerID() == userID) {
                                %>
                                <option value="<%=theService.getServiceID()%>"><%=theService.getServiceName().toUpperCase()%>
                                </option>
                                <%
                                            }
                                        }
                                    }
                                %>

                            </select>
                        </p>
                        <p>
                            <label></label><br/>
                            <input value="Click here to set date"
                                   style=" width: 180px; height: 30px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px; display: none;"
                                   type="text" name="date" id="date" class="datepickr" readonly/>
                        </p>
                        <p>
                            <label for="message">Content: </label><br/>
                            <textarea cols="22" name="sd" id="message" rows="5"
                                      onKeyDown="limitText(this.form.sd, this.form.countdown, 160);"
                                      onKeyUp="limitText(this.form.sd, this.form.countdown, 160);"></textarea>
                        </p>
                        <p>
                            You have <input style="width:3%; background: #fff; border: 0px; border-color: #fff" readonly
                                            type="text" name="countdown" size="3" value="160"/>Characters remaining<br/>
                            <input style="background-color: #099; font-weight: bolder; color: #fff; max-width: 100px;"
                                   id="submit" type="submit" value="UPLOAD"/>
                        </p>
                    </form>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <ul class="links">
                                <li><a href="new_content.jsp">New Content</a></li>
                                <li><a href="new_bulk_content.jsp">Upload Bulk Content</a></li>
                                <li><a href="content_mgr.jsp">Manage Existing Content</a></li>
                                <li><a href="Content_Template.csv">Download Bulk Content Template</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
