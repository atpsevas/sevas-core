<%@page import="org.esme.dto.Service" %>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">
<head>
    <title>:::...:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h3>B List Billing</h3>
                    <c:set var="message" value="${requestScope.message}"/>
                    <c:choose>
                        <c:when test="${fn:contains(message, 'succes')}">
                            <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:when>
                        <c:otherwise>
                            <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:otherwise>
                    </c:choose>
                    <!--End of Filter-->
                    <div id="txtHint">
                        <b>
                            <p>
                                <label for="message">Bulk Blacklist: </label><br/>
                            <form action="./BlacklistSubscribersBilling" method="post">
                                <select style="width:25%;" name="serviceID" id="subject">
                                    <%
                                        ArrayList<Service> serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                                        for (Service service : serviceCache) {
                                    %>
                                    <option value="<%=service.getServiceID()%>"><%=service.getServiceName().toUpperCase()%>
                                    </option>
                                    <%
                                        }
                                    %>
                                </select>
                                <input style="width:25%;" type="file" name="conte_file" id="content_file"/><br/>
                                <input style="background-color: #000;  font-weight: bolder; height: 35px; color: #fff; "
                                       type="submit" value="SUBMIT"/>
                            </form>
                            </p>
                        </b>
                    </div>
                </div>
                <div id="sub">
                    <h2>Features</h2>
                    <li><a href="./logout">[Logout]</a></li>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
