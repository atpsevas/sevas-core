<%@page import="org.esme.dao.models.*" %>
<%@page import="org.esme.dto.Service" %>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>::..:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--[if IE]>
    <link href="css/ie-transparency.css" rel="stylesheet" type="text/css"/><![endif]-->
    <script type="text/javascript">
        function toggleAdd(sel_box, tagToShow) {
            var state = document.getElementById(sel_box);
            var status = state.value;
            if (status == 'preload') {
                document.getElementById(tagToShow).style.display = 'block';
            } else {
                document.getElementById(tagToShow).style.display = 'none';
            }
        }
        ;
    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#fb1hide").click(function () {
                $("#fb1").hide(1000);
            });

            $("#fb1show").click(function () {
                $("#fb1").show(1000);
            });
        });
    </script>
    <script type="text/javascript">
        function crossSellToggle(sel_box, tagToShow) {
            var state = document.getElementById(sel_box);
            var status = state.value;
            if (status == 'yes') {
                document.getElementById(tagToShow).style.display = 'block';
                document.getElementById(tagToHide).style.display = 'none';
            } else {
                document.getElementById(tagToShow).style.display = 'none';
                document.getElementById(tagToHide).style.display = 'block';
            }
        }
    </script>
    <script>
        function generateUnsubKeyword() {
            keyword = document.getElementById("skw").value;
            document.getElementById("suk").value = "STOP " + last;
        }
    </script>
</head>

<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li class="on"><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Create New Subscription Service</h2>
                    <c:choose>
                        <c:when test="${param.id ==  null}">
                            <c:set var="module" value="./css.do"/>
                            <p>Please enter new subscription service details below. You may hover on each field for
                                description.</p>
                        </c:when>
                        <c:otherwise>
                            <c:set var="module" value="./updatesubservice.do?action=update"/>
                            <p>Update your service details below. You may hover on each field for description.</p>
                        </c:otherwise>
                    </c:choose>
                    <%
                        ArrayList<ShortCode> shortCodeCache = (ArrayList<ShortCode>) getServletContext().getAttribute("shortCodesCache");
                        ArrayList<Network> networks = (ArrayList<Network>) getServletContext().getAttribute("networksCache");
                        ArrayList<ServiceCategory> ServiceCategory = (ArrayList<ServiceCategory>) getServletContext().getAttribute("categoryCache");
                        ArrayList<ServiceBundle> serviceBundle = (ArrayList<ServiceBundle>) getServletContext().getAttribute("bundleCache");
                        ArrayList<Service> services = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                    %>
                    <form method="post" action="${module}">
                        <fieldset>
                            <legend>Service</legend>
                            <c:set var="message" value="${requestScope.message}"/>
                            <c:choose>
                                <c:when test="${fn:contains(message, 'successfully')}">
                                    <label style="color: green; font-weight: bold;"><c:out value="${message}"/> Click <a
                                            href="new_sub_service.jsp" style="text-decoration: none;">here </a> to
                                        create a new service.</label>
                                </c:when>
                                <c:otherwise>
                                    <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                                </c:otherwise>
                            </c:choose>
                            <c:choose>
                            <c:when test="${param.id == null}">
                                <hr/>
                                <label style="font-weight: bold; font-size: 14px;">Subscription Details</label>
                                <p>
                                    <label for="subject">Service Category:</label>
                                    <select name="sc">
                                        <option></option>
                                        <%
                                            for (ServiceCategory cat : ServiceCategory) {
                                        %>
                                        <option value="<%=cat.getCategoryID()%>"><%=cat.getCategoryName()%>
                                        </option>
                                        <%}%>
                                    </select>
                                </p>
                                <p>
                                    <label for="subject">Service Status </label>
                                    <select name="status">
                                        <option value="active">Active</option>
                                        <option value="inactive">Inactive</option>
                                        <option selected value="uat">UAT</option>
                                        <option value="suspended">Suspension</option>
                                    </select>
                                </p>
                                <!--Add to Bundle-->
                                <p>
                                    <label for="promo_label">Add to Parent Service?</label>
                                    <select id="addBundle" name="addBundle"
                                            onchange="javascript:crossSellToggle('addBundle', 'bundle');">
                                        <option selected value="no">No</option>
                                        <option value="yes">Yes</option>
                                    </select>
                                </p>
                                <div id="bundle" style="display: none">
                                    <p>
                                        <option value="">SELECT SERVICE</option>
                                        <select style=" width: 200px; height: 40px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;"
                                                name="parentServID">
                                            <%
                                                ArrayList<Service> serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                                                for (Service theService : serviceCache) {
                                            %>
                                            <option value="<%=theService.getServiceID()%>"><%=theService.getServiceName().toUpperCase()%>
                                            </option>
                                            <%
                                                }

                                            %>

                                        </select>
                                    </p>
                                    <p>
                                        <label for="promo_label">Auto Subscribe to Parent Service?</label>
                                        <select name="autoSubToParentServ">
                                            <option selected value="no">No</option>
                                            <option value="yes">Yes</option>
                                        </select>
                                    </p>
                                </div>
                                <!--Add to Bundle End-->
                                <p>
                                    <label for="subject">Service Name:</label>
                                    <input type="text" name="sn" title="Name of your service e.g Jokes"/>
                                </p>
                                <p>
                                <hr/>
                                <label style="font-weight: bold; font-size: 14px;">Network and Short Code
                                    Configuration</label>
                                </p>
                                <p class="theleft">
                                    <label for="subject">Network</label>
                                    <select name="net">
                                        <% for (Network net : networks) {
                                        %>
                                        <option value="<%=net.getNetworkID()%>"><%=net.getNetworkID()%>
                                        </option>
                                        <%}%>
                                    </select>
                                </p>
                                <p class="theright">
                                    <label for="subject">MO Short Code</label>
                                    <select name="ssc">
                                        <%
                                            for (ShortCode shortCode : shortCodeCache) {
                                        %>
                                        <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                        </option>
                                        <%}%>
                                    </select>
                                </p>
                                <p class="theleft">
                                    <label for="subject">Welcome Short Code</label>
                                    <select name="wsc">
                                        <%
                                            for (ShortCode shortCode : shortCodeCache) {
                                        %>
                                        <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                        </option>
                                        <%}%>
                                    </select>
                                </p>
                                <p class="theright">
                                    <label for="subject">MT Short Code</label>
                                    <select name="mt_ssc">
                                        <%
                                            for (ShortCode shortCode : shortCodeCache) {
                                        %>
                                        <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                        </option>
                                        <%}%>
                                    </select>
                                </p>
                                <p>
                                <hr/>
                                <label style="font-weight: bold; font-size: 14px;">Promo</label>
                                </p>
                                <hr/>
                                <p>
                                    <label for="promo_label">Enable Free Promo</label>
                                    <select name="promo">
                                        <option selected value="no">No</option>
                                        <option value="yes">Yes</option>
                                    </select>
                                </p>
                                <p class="theright">
                                    <label for="subject">Promo Period</label>
                                    <select name="promoPeriod">
                                        <app:expiryPeriod/>
                                    </select>
                                </p>
                                <p>
                                <hr/>
                                <label style="font-weight: bold; font-size: 14px;">Billing Configuration</label>
                                </p>
                                <p class="theleft">
                                    <label for="subject">Billing Type</label>
                                    <select name="sst">
                                        <option value="UCIP">AIRTEL UCIP</option>
                                        <option value="EDB">ETISALAT Direct Billing</option>
                                        <option value="PSA">GLO PSA</option>
                                        <option value="MO">MO</option>
                                        <option value="MT">MT</option>
                                    </select>
                                </p>
                                <p class="theright">
                                    <label for="subject">Billing Network</label>
                                    <select name="bill_net">
                                        <%
                                            for (Network net : networks) {
                                        %>
                                        <option value="<%=net.getNetworkID()%>"><%=net.getNetworkID()%>
                                        </option>
                                        <%}%>
                                    </select>
                                </p>
                                <div>
                                    <p class="theleft">
                                        <label for="subject">Billing Short Code</label>
                                        <select name="wc_ssc">
                                            <%
                                                for (ShortCode shortCode : shortCodeCache) {
                                            %>
                                            <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                            </option>
                                            <%}%>
                                        </select></p>
                                    <p class="theright">
                                        <label for="subject">Price</label>
                                        <select name="price">
                                            <option value="0">N0</option>
                                            <option value="5">N5</option>
                                            <option value="10">N10</option>
                                            <option value="15">N15</option>
                                            <option value="20">N20</option>
                                            <option value="25">N25</option>
                                            <option value="30">N30</option>
                                            <option value="50">N50</option>
                                            <option value="75">N75</option>
                                            <option value="100">N100</option>
                                        </select>
                                    </p>
                                    <p class="theleft">
                                        <label for="subject">Expiry Period</label>
                                        <select name="sp" id="aubject">
                                            <app:expiryPeriod/>
                                        </select>
                                    </p>
                                    <p class="theright">
                                        <label for="message">Renewal Message: </label><br/>
                                        <textarea name="rm" cols="40" rows="5"></textarea>
                                    </p>
                                    <p class="theleft">
                                        <label for="subject">Send Notification</label>
                                        <select name="notify" id="aubject">
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                    </p>
                                    <p class="theright">
                                        <label for="subject">Enable Instant Billing Retry: </label>
                                        <select name="irb">
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                    </p>
                                    <p class="theleft">
                                        <label for="subject">Billing Time: </label>
                                        <select name="sbt">
                                            <app:billingTime/>
                                        </select>
                                    </p>
                                    <p class="theright">
                                        <label for="subject">Billing Retry Intervals: </label>
                                        <select name="sbi" id="aubject">
                                            <app:billingIntervals/>
                                        </select>
                                    </p>
                                    <p class="theleft">
                                        <span class="fake-link" id="fb1show">More Billing Options</span>
                                    </p>
                                </div>
                                <p>
                                <hr/>
                                </p>
                                <div style="display: none" id="fb1">
                                    <p class="theleft">
                                        <label for="subject">Fallback 1 Short Code</label>
                                        <select name="fb_ssc">
                                            <%
                                                for (ShortCode shortCode : shortCodeCache) {
                                            %>
                                            <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                            </option>
                                            <%}%>
                                        </select>
                                    </p>
                                    <p class="theright">
                                        <label for="subject">Fallback 1 Price</label>
                                        <select name="fb_price">
                                            <app:priceTag/>
                                        </select>
                                    </p>
                                    <p class="theleft">
                                        <label for="subject">Fallback 1 Expiry Period</label>
                                        <select name="fsp">
                                            <app:expiryPeriod/>
                                        </select>
                                    </p>
                                    <p class="theright">
                                        <label for="message">Fallback 1 Renewal Message: </label><br/>
                                        <textarea name="frm" cols="40" rows="5"></textarea>
                                    </p>
                                    <p class="theleft">
                                        <label for="subject">Send Notification</label>
                                        <select name="fb1_notify" id="aubject">
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                    </p>
                                    <p>
                                    <hr/>
                                    </p>
                                    <p class="theleft">
                                        <label for="subject">Fallback 2 Short Code</label>
                                        <select name="fb_ssc2">
                                            <%
                                                for (ShortCode shortCode : shortCodeCache) {
                                            %>
                                            <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                            </option>
                                            <%}%>
                                        </select>
                                    </p>
                                    <p class="theright">
                                        <label for="subject">Fallback 2 Price</label>
                                        <select name="fb_price2">
                                            <app:priceTag/>
                                        </select>
                                    </p>
                                    <p class="theleft">
                                        <label for="subject">Fallback 2 Expiry Period</label>
                                        <select name="fsp2">
                                            <app:expiryPeriod/>
                                        </select>
                                    </p>
                                    <p class="theright">
                                        <label for="message">Fallback 2 Renewal Message: </label><br/>
                                        <textarea name="frm2" cols="40" rows="5"></textarea>
                                    </p>
                                    <p class="theleft">
                                        <label for="subject">Send Notification</label>
                                        <select name="fb2_notify" id="aubject">
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                    </p>
                                    <p>
                                    <hr/>
                                    </p>
                                    <p class="theleft">
                                        <label for="subject">Fallback 3 Short Code</label>
                                        <select name="fb_ssc3">
                                            <%
                                                for (ShortCode shortCode : shortCodeCache) {
                                            %>
                                            <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                            </option>
                                            <%}%>
                                        </select>
                                    </p>
                                    <p class="theright">
                                        <label for="subject">Fallback 3 Price</label>
                                        <select name="fb_price3">
                                            <app:priceTag/>
                                        </select>
                                    </p>
                                    <p class="theleft">
                                        <label for="subject">Fallback 3 Expiry Period</label>
                                        <select name="fsp3" id="aubject">
                                            <app:expiryPeriod/>
                                        </select></p>
                                    <p class="theright">
                                        <label for="message">Fallback 3 Renewal Message: </label><br/>
                                        <textarea name="frm3" cols="40" rows="5"></textarea>
                                    </p>
                                    <p class="theleft">
                                        <label for="subject">Send Notification</label>
                                        <select name="fb3_notify" id="aubject">
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                    </p>
                                    <p>
                                    <hr/>
                                    </p>
                                    <p class="theleft">
                                        <label for="subject">Fallback 4 Short Code</label>
                                        <select name="fb_ssc4">
                                            <%
                                                for (ShortCode shortCode : shortCodeCache) {
                                            %>
                                            <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                            </option>
                                            <%}%>
                                        </select>
                                    </p>
                                    <p class="theright">
                                        <label for="subject">Fallback 4 Price</label>
                                        <select name="fb_price4">
                                            <app:priceTag/>
                                        </select>
                                    </p>
                                    <p class="theleft">
                                        <label for="subject">Fallback 4 Expiry Period</label>
                                        <select name="fsp4" id="aubject">
                                            <app:expiryPeriod/>
                                        </select></p>
                                    <p class="theright">
                                        <label for="message">Fallback 4 Renewal Message: </label><br/>
                                        <textarea name="frm4" cols="40" rows="5"></textarea>
                                    </p>
                                    <p class="theleft">
                                        <label for="subject">Send Notification</label>
                                        <select name="fb4_notify" id="aubject">
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                    </p>
                                    <p class="theright">
                                        <span class="fake-link" id="fb1hide">Hide Billing Options</span>
                                    </p>
                                </div>
                                <p>
                                <hr/>
                                <label style="font-weight: bold; font-size: 14px;">Service Manager Configuration</label>
                                </p>
                                <p>
                                    <label for="subject">Service Managers:</label>
                                    <select name="so" id="subject">
                                        <%
                                            ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
                                            for (Users us : usersCache) {
                                        %>
                                        <option value="<%=us.getId()%>"><%=us.getUserName().toUpperCase()%>
                                        </option>
                                        <%}%>
                                    </select>
                                </p>
                                <hr/>
                                <label style="font-weight: bold; font-size: 14px;">Service Content Delivery</label>
                                <p class="theleft">
                                    <label for="subject"> Content Type </label>
                                    <select name="content_type">
                                        <option value="sms">SMS</option>
                                        <option value="voice">Voice</option>
                                    </select>
                                </p>
                                <p>
                                    <label for="subject">Content Sender ID:</label>
                                    <input type="text" name="csID" title="Name of your service e.g Jokes"/>
                                </p>
                                <p class="theleft">
                                    <label for="subject">Delivery Method: </label>
                                    <select name="cdm">
                                        <option value="byDate">By Date</option>
                                        <option value="random">Random</option>
                                        <option value="sequential">Sequential</option>
                                    </select>
                                </p>
                                <p class="theright">
                                    <label for="subject">Content Delivery Time:</label>
                                    <select name="stob" id="aubject"
                                            title="At what time of the day should the system send the content of this service to subscriber?">
                                        <app:timeOfBlast/>
                                    </select>
                                </p>
                                <p>
                                <hr/>
                                <label style="font-weight: bold; font-size: 14px;">Service Subscription Keywords</label>
                                </p>
                                <p>
                                    <label for="subject">Subscription Keyword: </label>
                                    <input id="skw" type="text" name="skw" value="" onkeyup="generateUnsubKeyword();"/>
                                </p>
                                <p>
                                    <label for="subject">Alternate Keywords </label>
                                    <input type="text" name="sakw" value=""/>
                                </p>
                                <p>
                                    <label for="subject">Un subscription Keyword</label>
                                    <input id="suk" type="text" name="suk" value=""/>
                                </p>
                                <p>
                                <hr/>
                                <label style="font-weight: bold; font-size: 14px;">Service Description and
                                    Messages</label>
                                </p>
                                <p class="theleft">
                                    <label for="message">Help Message: </label><br/>
                                    <textarea name="shm" cols="40" rows="5"></textarea>
                                </p>
                                <p class="theright">
                                    <label for="message">Subscription Message: </label><br/>
                                    <textarea name="ssm" cols="40" rows="5"></textarea>
                                </p>
                                <p class="theleft">
                                    <label for="message">Un subscription Message: </label><br/>
                                    <textarea name="sum" cols="40" rows="5"></textarea>
                                </p>
                                <p class="theright">
                                    <label for="message">Reminder Message: </label><br/>
                                    <textarea name="srm" cols="40" rows="5"></textarea>
                                </p>
                                <p class="theleft">
                                    <label for="subject">Reminder Period: </label>
                                    <select name="srp">
                                        <app:reminderPeriod/>
                                    </select>
                                </p>
                                <p class="theright">
                                    <label for="subject">Subscription Reminder Time</label>
                                    <select name="srt" id="aubject"
                                            title="When should the subscriber be reminded to resubscribe?">
                                        <app:reminderTime/>
                                    </select>
                                </p>
                                <p>
                                    <label for="promo_label">Require Double Confirmation?</label>
                                    <select id="dbCon" name="dbCon"
                                            onchange="javascript:crossSellToggle('dbCon', 'doubleConfirm');">
                                        <option value=""></option>
                                        <option value="no">No</option>
                                        <option value="yes">Yes</option>
                                    </select>
                                </p>
                                <div style="display: none" id="doubleConfirm">
                                    <p>
                                        <label> Confirmation Keyword</label>
                                        <input type="text" name="dck" title="Name of your service e.g Jokes"/>
                                    </p>
                                    <p>
                                        <label>Confirmation Message</label>
                                        <textarea name="dcm" cols="40" rows="5"></textarea>
                                    </p>
                                </div>
                                <label style="font-weight: bold; font-size: 14px;">Cross Selling</label>
                                <p>
                                    <label for="promo_label">Cross Sell Another Service?</label>
                                    <select id="cselling" name="cselling"
                                            onchange="javascript:crossSellToggle('cselling', 'cross');">
                                        <option selected value="no">No</option>
                                        <option value="yes">Yes</option>
                                    </select>
                                </p>
                                <div style="display: none" id="cross">
                                    <label>Cross Short Network, Short Code and Message</label><br/>
                                    <p>
                                        <label for="subject">Cross Sell Network</label>
                                        <select name="cross_net">
                                            <%
                                                for (Network net : networks) {
                                            %>
                                            <option value="<%=net.getNetworkID()%>"><%=net.getNetworkID()%>
                                            </option>
                                            <%}%>
                                        </select>
                                    </p>
                                    <p>
                                        <label for="subject">Cross Sell Short Code</label>
                                        <select name="cross_sc">
                                            <%
                                                for (ShortCode shortCode : shortCodeCache) {
                                            %>
                                            <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                            </option>
                                            <%}%>
                                        </select>
                                    </p>
                                    <p>
                                        <label for="cross message">Cross Sell Message: </label><br/>
                                        <textarea name="cross_msg" cols="40" rows="5"></textarea>
                                    </p>
                                </div>
                                <!--Route to third party-->
                                <label style="font-weight: bold; font-size: 14px;">Route to External API</label>
                                <p>
                                    <label for="promo_label">Route?</label>
                                    <select id="route" name="route"
                                            onchange="javascript:crossSellToggle('route', 'theroute');">
                                        <option value=""></option>
                                        <option value="no">No</option>
                                        <option value="yes">Yes</option>
                                    </select>
                                </p>

                                <div style="display: none" id="theroute">
                                    <label>External API and parameters</label><br/>
                                    <p>
                                        <label for="promo_label">Route by Short Code</label>
                                        <select id="route" name="route_by_sc">
                                            <option value=""></option>
                                            <option value="no">No</option>
                                            <option value="yes">Yes</option>
                                        </select>
                                    </p>
                                    <p>
                                        <label for="subject">URL (http://ip:port/path?)</label>
                                        <input type="text" name="routeurl" style="width: 600px;"/>
                                    </p>
                                    <p>
                                        <label for="subject">Short Code Parameter</label>
                                        <input type="text" name="routeshortcode" style="width: 200px;"/>
                                    </p>
                                    <p>
                                        <label for="subject">MSISDN Parameter</label>
                                        <input type="text" name="routemsisdn" style="width: 200px;"/>
                                    </p>
                                    <p>
                                        <label for="subject">Message Parameter</label>
                                        <input type="text" name="routemessage" style="width: 200px;"/>
                                    </p>
                                </div>
                                <!--Route to third party-->
                                <p>
                                    <input type="submit" value="Create" class="btn"/>
                                </p>
                                <hr/>
                            </c:when>
                            <c:otherwise>
                            <%
                                Service theService = null, serviceParent = null;
                                for (Service serv : services) {
                                    if (serv.getServiceID() == Long.parseLong(request.getParameter("id"))) {
                                        theService = serv;
                                        for (Service service : services) {
                                            if (service.getServiceID() == theService.getParentServiceID()) {
                                                serviceParent = service;
                                            }
                                        }
                                        break;
                                    }
                                }
                            %>
                            <input type="hidden" name="id" value="<%=theService.getServiceID()%>"/>
                            <p>
                                <label for="subject">Service Category:</label>
                                <select name="sc">
                                    <%
                                        for (ServiceCategory cat : ServiceCategory) {
                                            if (cat.getCategoryID() == Long.parseLong(theService.getServiceCategory())) {%>
                                    <option selected value="<%=cat.getCategoryID()%>"><%=cat.getCategoryName()%>
                                    </option>
                                    <%
                                                break;
                                            }
                                        }
                                    %>
                                    <%
                                        for (ServiceCategory cat : ServiceCategory) {
                                    %>
                                    <option selected value="<%=cat.getCategoryID()%>"><%=cat.getCategoryName()%>
                                    </option>
                                    <%
                                        }
                                    %>
                                </select>
                            <p>
                                <label for="subject">Service Status</label>
                                <input type="text" name="status" value="<%=theService.getServiceStatus()%>"
                                       title="Service Status"/>
                            </p>
                            </p>

                            <p>
                                <label for="promo_label">Edit to Parent Service?</label>
                                <select id="addBundle" name="addBundle">
                                    <option selected
                                            value="<%=theService.getAutoSubToParentService()%>"><%=theService.getAutoSubToParentService()%>
                                    </option>
                                    <option value="no">No</option>
                                    <option value="yes">Yes</option>
                                </select>
                            </p>
                            <div id="bundle">
                                <p>
                                    <option value="">SELECT SERVICE</option>
                                    <select style=" width: 200px; height: 40px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;"
                                            name="parentServID">
                                        <%try {%>
                                        <option selected
                                                value="<%=serviceParent.getServiceID()%>"><%=serviceParent.getServiceName()%>
                                        </option>
                                        <% } catch (Exception e) {
                                        }
                                        %>
                                        <%
                                            //serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                                            for (Service service : services) {
                                        %>
                                        <option value="<%=service.getServiceID()%>"><%=service.getServiceName().toUpperCase()%>
                                        </option>
                                        <%
                                            }

                                        %>

                                    </select>
                                </p>
                                <p>
                                    <label for="promo_label">Auto Subscribe to Parent Service?</label>
                                    <select name="autoSubToParentServ">
                                        <option selected
                                                value="<%=theService.getAutoSubToParentService()%>"><%=theService.getAutoSubToParentService()%>
                                        </option>
                                        <option value="no">No</option>
                                        <option value="yes">Yes</option>
                                    </select>
                                </p>
                            </div>

                            <p>
                                <label for="subject">Service Name: (e.g Jokes)</label>
                                <input type="text" name="sn" value="<%=theService.getServiceName()%>"/>
                            </p>
                            <p>
                                <label for="subject">Network</label>
                                <input type="text" name="net" value="<%=theService.getServiceNetwork()%>"/>
                            </p>
                            <p>
                                <label for="subject">MO Short Code: (e.g 39090) </label>
                                <input type="text" name="ssc" value="<%=theService.getServiceShortCode()%>"/>
                            </p>
                            <p>
                                <label for="subject">Welcome Short Code: (e.g 39090) </label>
                                <input type="text" name="wsc"
                                       value="<%=theService.getServiceWelcomeMessageShortCode()%>"/>
                            </p>
                            <p>
                                <label for="subject">MT Short Code: (e.g 30090) </label>
                                <input type="text" name="mt_ssc" value="<%=theService.getServiceMTShortCode()%>"/>
                            </p>
                            <p>
                                <label for="promo_label">Run Free Promo for New Sub</label>
                                <select name="promo" id="promo1"
                                        onchange="javascript:crossSellToggle('promo1', 'promo2');">
                                    <option selected
                                            value="<%=theService.getServicePromo()%>"><%=theService.getServicePromo()%>
                                    </option>
                                    <option value="no">No</option>
                                    <option value="yes">Yes</option>
                                </select>
                            </p>
                            <div style="display: none" id="promo2">
                                <p>
                                    <label for="subject">Promo Period</label>
                                    <select name="promoPeriod" id="aubject">
                                        <option selected
                                                value="<%=theService.getServicePromoPeriod()%>"><%=theService.getServicePromoPeriod()%>
                                        </option>
                                        <app:expiryPeriod/>
                                    </select>
                                </p>
                            </div>
                            <p>
                                <label for="message">Description: </label>
                                <textarea name="sd" id="message" cols="40" rows="5"><c:out
                                        value="<%=theService.getServiceDescription()%>"/></textarea>
                            </p>
                            <p>
                                <label for="subject">Subscription Keyword: </label>
                                <input type="text" name="skw" value="<%=theService.getServiceKeyword()%>"
                                       title="Your service keyword e.g Joke"/>
                            </p>
                            <p>
                                <label for="subject">Allowable Keywords </label>
                                <input type="text" name="sakw" value="<%=theService.getServiceAllowableKeyword()%>"
                                       title="Your service allowable keywords seperated
                                                       by comma e.g Jokes,joke,laff"/>
                            </p>
                            <p>
                                <label for="subject">Un subscribe Keyword</label>
                                <input type="text" name="suk" value="<%=theService.getServiceUnSubscribeKeyword()%>"
                                       title="Your service un-subscription keyword e.g STOP,1,2"/>
                            </p>

                            <p>
                                <label for="subject">Service Managers:</label>
                                <select name="so" id="subject">
                                    <%
                                        ArrayList<Users> usersCache2 = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
                                        for (Users us : usersCache2) {
                                            if (us.getId() == theService.getServiceOwnerID()) {%>
                                    <option selected value="<%=us.getId()%>"><%=us.getUserName().toUpperCase()%>
                                    </option>
                                    <% }
                                    %>
                                    <option value="<%=us.getId()%>"><%=us.getUserName().toUpperCase()%>
                                    </option>
                                    <%}%>
                                </select>
                            </p>
                            <p>
                                <label for="subject">Content Delivery Time</label>
                                <select name="stob" id="aubject"
                                        title="At what time of the day should the system send the content of this service to subscriber?">
                                    <option selected value="<%=theService.getTimeOfBlast()%>">
                                        <%=theService.getTimeOfBlast()%>
                                    </option>
                                    <app:timeOfBlast/>
                                </select>
                            </p>
                            <hr/>
                            <label style="font-weight: bold; font-size: 14px;">Service Content Delivery</label>

                            <%
                                String[] arrCDM = theService.getServiceContentDeliveryMethod().split(",");

                                boolean selection = false;

                                if (arrCDM.length == 1 || arrCDM[1].equalsIgnoreCase("sms")) {
                                    selection = true;
                                }
                            %>
                            <p class="theleft">
                                <label for="subject"> Content Type </label>
                                <select name="content_type">
                                    <option <%=(selection) ? "selected" : ""%> value="sms">SMS</option>
                                    <option <%=(!selection) ? "selected" : ""%> value="voice">Voice</option>
                                </select>
                            </p>
                            <p>
                                <label for="subject">Content Sender ID:</label>
                                <input type="text" value="<%=theService.getServiceContentSenderID()%>" name="csID"
                                       title="Name of your service e.g Jokes"/>
                            </p>
                            <p class="theright">
                                <label for="subject">Delivery Method: </label>
                                <select name="cdm">
                                    <option <%=arrCDM[0] == "byDate" ? "selected" : ""%> value="byDate">By Date</option>
                                    <option <%=arrCDM[0] == "random" ? "selected" : ""%> value="random">Random</option>
                                    <option <%=arrCDM[0] == "sequential" ? "selected" : ""%> value="sequential">
                                        Sequential
                                    </option>
                                </select>
                            </p>
                            <p>
                                <label for="message">Subscription Message: </label><br/>
                                <textarea name="ssm" cols="40" rows="5"><c:out
                                        value="<%=theService.getServiceSubscriptionMessage()%>"/></textarea>
                            </p>
                            <p>
                                <label for="message">Subscription Renewal Message: </label><br/>
                                <textarea name="rm" cols="40" rows="5"><c:out
                                        value="<%=theService.getServiceRenewalMessage()%>"/></textarea>
                            </p>
                            <p>
                                <label for="message">Un-subscription Message: </label><br/>
                                <textarea name="sum" cols="40" rows="5"><c:out
                                        value="<%=theService.getServiceUnSubscriptionMessage()%>"/></textarea>
                            </p>
                            <p>
                                <label for="message">Reminder Message: </label><br/>
                                <textarea name="srm" cols="40" rows="5"><c:out
                                        value="<%=theService.getServiceReminderMessage()%>"/></textarea>
                            </p>
                            <p>
                                <label for="subject">Reminder Period: </label>
                                <select name="srp">
                                    <option selected
                                            value="<%=theService.getServiceReminderPeriod()%>"><%=theService.getServiceReminderPeriod()%>
                                    </option>
                                    <app:reminderPeriod/>
                                </select>
                            </p>
                            <p>
                                <label for="subject">Subscription Reminder Time</label>
                                <select name="srt">
                                    <option selected
                                            value="<%=theService.getServiceReminderTime()%>"><%=theService.getServiceReminderTime()%>
                                    </option>
                                    <app:reminderTime/>
                                </select>
                            </p>
                            <p>
                                <label for="promo_label">Double Confirmation?</label>
                                <select id="dbCon2" name="dbCon"
                                        onchange="javascript:crossSellToggle('dbCon2', 'doubleConfirm2');">
                                    <option selected
                                            value="<%=theService.getServiceDoubleConfirmation()%>"><%=theService.getServiceDoubleConfirmation()%>
                                    </option>
                                    <option value="no">No</option>
                                    <option value="yes">Yes</option>
                                </select>
                            </p>
                            <div style="display: none" id="doubleConfirm2">
                                <p>
                                    <label>Confirmation Keyword</label>
                                    <input type="text" name="dck"
                                           value="<%=theService.getServiceDoubleConfirmationKeyword()%>"/>
                                </p>
                                <p>
                                    <label>Confirmation Message</label>
                                    <textarea name="dcm" cols="40" rows="5"><c:out
                                            value="<%=theService.getServiceDoubleConfirmationMessage()%>"/></textarea>
                                </p>
                            </div>
                            <p>
                                <label for="message">Help Message: </label><br/>
                                <textarea name="shm" cols="40" rows="5"><c:out
                                        value="<%=theService.getServiceHelpMessage()%>"/></textarea>
                            </p>
                            <hr/>
                            <label style="font-weight: bold; font-size: 14px;">Cross Selling</label>
                            <p>
                                <label for="promo_label">Cross Sell Another Service?</label>
                                <select id="cselling" name="cross_sell"
                                        onchange="javascript:crossSellToggle('cselling', 'cross');">
                                    <option selected
                                            value="<%=theService.getServiceCrossSell()%>"><%=theService.getServiceCrossSell()%>
                                    </option>
                                    <option value="no">No</option>
                                    <option value="yes">Yes</option>
                                </select>
                            </p>
                            <div style="display: none" id="cross">
                                <label>Cross Sell Network, Short Code and Message</label><br/>
                                <p>
                                    <label for="subject">Cross Sell Network</label>
                                    <select id="cselling" name="cross_net">
                                        <option selected
                                                value="<%=theService.getServiceCrossNetwork()%>"><%=theService.getServiceCrossNetwork()%>
                                        </option>
                                        <%
                                            for (Network net : networks) {
                                        %>
                                        <option value="<%=net.getNetworkID()%>"><%=net.getNetworkID()%>
                                        </option>
                                        <%}%>
                                    </select>
                                </p>
                                <p>
                                    <label for="subject">Cross Sell Short Code</label>
                                    <select name="cross_sc">
                                        <option selected
                                                value="<%=theService.getServiceCrossSellShortCode()%>"><%=theService.getServiceCrossSellShortCode()%>
                                        </option>
                                        <%
                                            for (ShortCode shortCode : shortCodeCache) {
                                        %>
                                        <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                        </option>
                                        <%}%>
                                    </select>
                                </p>
                                <p>
                                    <label for="cross message">Cross Sell Message: </label><br/>
                                    <textarea name="cross_msg" cols="40" rows="5"><c:out
                                            value="<%=theService.getServiceCrossMessage()%>"/></textarea>
                                </p>
                            </div>
                            <!--Route to third party-->
                            <label style="font-weight: bold; font-size: 14px;">Route to External API</label>
                            <p>
                                <label for="promo_label">Route?</label>
                                <select id="route" name="route"
                                        onchange="javascript:crossSellToggle('route', 'theroute');">
                                    <option selected
                                            value="<%=theService.getRouteToExternalAPI()%>"><%=theService.getRouteToExternalAPI()%>
                                    </option>
                                    <option value="no">No</option>
                                    <option value="yes">Yes</option>
                                </select>
                            </p>

                            <div style="display: none" id="theroute">
                                <label>External API and parameters</label><br/>
                                <p>
                                    <label for="promo_label">Route by Short Code</label>
                                    <select id="route" name="route_by_sc">
                                        <option value="<%=theService.getRouteToExternalAPByShortCode()%>"></option>
                                        <option value="no">No</option>
                                        <option value="yes">Yes</option>
                                    </select>
                                </p>
                                <p>
                                    <label for="subject">URL</label>
                                    <input type="text" name="routeurl" value="<%=theService.getExternalAPIURL()%>"
                                           style="width: 600px;"/>
                                </p>
                                <p>
                                    <label for="subject">Short Code Parameter</label>
                                    <input type="text" name="routeshortcode"
                                           value="<%=theService.getExternalAPIShortCodeParam()%>"
                                           style="width: 200px;"/>
                                </p>
                                <p>
                                    <label for="subject">MSISDN Parameter</label>
                                    <input type="text" name="routemsisdn"
                                           value="<%=theService.getExternalAPIMSISDNParam()%>" style="width: 200px;"/>
                                </p>
                                <p>
                                    <label for="subject">Message Parameter</label>
                                    <input type="text" name="routemessage"
                                           value="<%=theService.getExternalAPIMessageParam()%>" style="width: 200px;"/>
                                </p>
                            </div>
                            <!--Route to third party-->
                            <p>
                                <input type="submit" value="Update" class="btn"/>
                            </p>
                        </fieldset>
                    </form>
                    </c:otherwise>
                    </c:choose>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <ul class="links">
                                <li><a href="new_serv_category.jsp">New Service Category</a></li>
                                <li><a href="new_serv_bundle.jsp">New Service Bundle</a></li>
                                <li><a href="manage_serv_category.jsp">Manage Services Category</a></li>
                                <li><a href="manage_serv_bundle.jsp">Manage Services Bundle</a></li>
                                <li><a href="manage_sub_services.jsp">Manage Services</a></li>
                                <li><a href="manage_services_billing.jsp">Manage Services Billing</a></li>
                                <li><a href="manual_sub.jsp">Manual Subscription</a></li>
                                <li><a href="bulk_subscription.jsp">Bulk Subscription</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/></div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
