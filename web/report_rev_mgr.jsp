<%@page import="org.esme.dao.models.Users" %>
<%@page import="org.esme.dto.Service" %>
<%@page import="java.sql.Connection" %>
<%@page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<%@page import="java.text.DecimalFormat" %>
<%@page import="java.util.ArrayList" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<h3>Services Revenue Report Update</h3>
<c:set var="message" value="${requestScope.message}"/>
<c:choose>
    <c:when test="${fn:contains(message, 'succes')}">
        <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
    </c:when>
    <c:otherwise>
        <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
    </c:otherwise>
</c:choose>

<!--Filter-->
<form action="" method="post">
    <select name="service" style=" width: 200px; height: 30px; font-size: 16px;  
            font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; 
            letter-spacing: -1px;" onchange="showRevReport(this.value)">
        <option>Select A Service</option>
        <%
            ArrayList<Service> serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            for (Service service : serviceCache) {
        %>
        <option value="<%=service.getServiceID()%>"><%=service.getServiceName()%>
        </option>
        <%}%>
    </select>
</form>
<!--End of Filter-->

<div id="showRep">
    <%

        HttpSession sess = request.getSession(true);
        String user = (String) sess.getAttribute("userName");
        long userID = 0;
        String userRole = (String) sess.getAttribute("UserRole");

        ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
        for (Users us : usersCache) {
            if (us.getUserName().equals(user)) {
                userID = us.getId();
                break;
            }
        }

        Connection conn = null;
        try {

            conn = (Connection) getServletContext().
                    getAttribute("database.connection");

            PreparedStatement st = null;
            ResultSet rs = null;

            if (userRole.equals("Admin")) {
                rs = (ResultSet) getServletContext().getAttribute("dailyServiceRevenue_All");
            } else {
                st = conn.prepareStatement(
                        (String) getServletContext().getInitParameter("dailyServiceRevenue_All_User"));
                st.setLong(1, userID);
                rs = st.executeQuery();
            }

            long totalNewSub = 0;
            long totalNewSubMainCharge = 0;
            long totalNewSubFallBackCharge = 0;
            long totalNewSubFallBackCharge2 = 0;
            long totalNewSubCharged = 0;
            long totalRenew = 0;
            long totalRenSubMainCharge = 0;
            long totalRenSubFallBackCharge = 0;
            long totalRenSubFallBackCharge2 = 0;
            long totalRenSubCharged = 0;
            long newSubTotalRev = 0;
            long renTotalRev = 0;
            DecimalFormat formatter = new DecimalFormat("###,###,###");

            String nextService = "";
            while (rs.next()) {

                if (nextService.equalsIgnoreCase(rs.getString("theService").trim())) {

                } else {

                    out.println("<table width='100%' class='fancy'>");
                    out.println("<tr colspan='9'>" + rs.getString("theService").toUpperCase() + "</tr>");
                    out.println("<tr>");
                    out.println("<th>Report Date</th>");
                    out.println("<th>New Sub</th>");
                    out.println("<th>New Sub Main Rev</th>");
                    out.println("<th>New Sub FB Rev</th>");
                    out.println("<th>New Sub FB 2 Rev</th>");
                    out.println("<th>New Sub Tot Rev</th>");

                    out.println("<th>Renewal</th>");
                    out.println("<th>Ren Main Rev</th>");
                    out.println("<th>Ren FB Rev</th>");
                    out.println("<th>Ren FB 2 Rev</th>");
                    out.println("<th>Ren Tot Rev</th>");
                    out.println("</tr>");

                }

                out.println("<tr>");
                out.println("<td>" + rs.getString("theDate") + "</td>");
                out.println("<td>" + formatter.format(rs.getLong("new_sub")) + "</td>");
                totalNewSub += rs.getLong("new_sub");
                out.println("<td>" + formatter.format(rs.getLong("new_sub_main_charge")) + "</td>");

                totalNewSubMainCharge += rs.getLong("new_sub_main_charge");

                /*if (rs.getString("welcome_shortcode").equalsIgnoreCase(rs.getString("fallback_shortcode"))) {
                    out.println("<td>0</td>");
                    newSubTotalRev = rs.getLong("new_sub_main_charge");
                } else {*/
                out.println("<td>" + formatter.format(rs.getLong("new_sub_fallback_charge")) + "</td>");
                out.println("<td>" + formatter.format(rs.getLong("new_sub_fallback_charge_2")) + "</td>");
                totalNewSubFallBackCharge += rs.getLong("new_sub_fallback_charge");
                totalNewSubFallBackCharge2 += rs.getLong("new_sub_fallback_charge_2");
                newSubTotalRev = rs.getLong("new_sub_main_charge") + rs.getLong("new_sub_fallback_charge") + rs.getLong("new_sub_fallback_charge_2");
                //}

                out.println("<td>" + formatter.format(newSubTotalRev) + "</td>");
                totalNewSubCharged += newSubTotalRev;

                out.println("<td>" + formatter.format(rs.getLong("renewed")) + "</td>");
                totalRenew = +rs.getLong("renewed");
                totalRenSubMainCharge += rs.getLong("renew_main_charge");

                out.println("<td>" + formatter.format(rs.getLong("renew_main_charge")) + "</td>");
                /*if (rs.getString("welcome_shortcode").equalsIgnoreCase(rs.getString("fallback_shortcode"))) {
                    out.println("<td>0</td>");
                } else {*/
                out.println("<td>" + formatter.format(rs.getLong("renew_fallback_charge")) + "</td>");
                out.println("<td>" + formatter.format(rs.getLong("renew_fallback_charge_2")) + "</td>");
                totalRenSubFallBackCharge += rs.getLong("renew_fallback_charge");
                totalRenSubFallBackCharge2 += rs.getLong("renew_fallback_charge_2");
                //}

                /*if (rs.getString("welcome_shortcode").equalsIgnoreCase(rs.getString("fallback_shortcode"))) {
                    renTotalRev = rs.getLong("renew_main_charge");
                } else {*/
                renTotalRev = rs.getLong("renew_main_charge") + rs.getLong("renew_fallback_charge") + rs.getLong("renew_fallback_charge_2");
                //}
                out.println("<td>" + formatter.format(renTotalRev) + "</td>");
                totalRenSubCharged += renTotalRev;
                out.println("</tr>");

                nextService = rs.getString("theService").trim();
                newSubTotalRev = 0;
                renTotalRev = 0;
            }

            out.println("</table>");
            out.println("<u><b> Revenue Summary</b></u>");
            out.println("<table width='100%' class='fancy'>");
            out.println("<tr>");
            out.println("<th>Report Date</th>");
            out.println("<th>New Sub</th>");
            out.println("<th>New Main Rev</th>");
            out.println("<th>New FB Rev</th>");
            out.println("<th>New FB 2 Rev</th>");
            out.println("<th>New Sub Tot Rev</th>");
            out.println("<th>Renewal</th>");
            out.println("<th>Ren Main Rev</th>");
            out.println("<th>Ren FB Rev</th>");
            out.println("<th>Ren FB 2 Rev</th>");
            out.println("<th>Renewal Tot Rev</th>");
            out.println("</tr>");
            out.println("<tr style='font-weight:bold;'>");
            out.println("<td>Total</td>");
            out.println("<td>" + formatter.format(totalNewSub) + "</td>");
            out.println("<td>" + formatter.format(totalNewSubMainCharge) + "</td>");
            out.println("<td>" + formatter.format(totalNewSubFallBackCharge) + "</td>");
            out.println("<td>" + formatter.format(totalNewSubFallBackCharge2) + "</td>");
            out.println("<td>" + formatter.format(totalNewSubCharged) + "</td>");
            out.println("<td>" + formatter.format(totalRenew) + "</td>");
            out.println("<td>" + formatter.format(totalRenSubMainCharge) + "</td>");
            out.println("<td>" + formatter.format(totalRenSubFallBackCharge) + "</td>");
            out.println("<td>" + formatter.format(totalRenSubFallBackCharge2) + "</td>");
            out.println("<td>" + formatter.format(totalRenSubCharged) + "</td>");
            out.println("</tr>");
            out.println("</table>");

        } catch (Exception e) {
            e.printStackTrace();
        }
        out.println("</table>");
    %>

</div>

