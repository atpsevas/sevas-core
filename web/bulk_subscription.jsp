<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@include file="partials/header.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>::..:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        function loadSubmit() {
            ProgressImage = document.getElementById('progress_image');
            document.getElementById("progress").style.visibility = "visible";
            setTimeout("ProgressImage.src = ProgressImage.src", 1000);
            return true;
        }
    </script>
</head>


<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li class="on"><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <p><label style="color: red; font-weight: bold;">For high performance, you are advise to NOT upload
                        more than 50,000 numbers at a time.</label></p>
                    <h2>Subscriber Numbers</h2>
                    <p style="visibility:hidden;" id="progress"/>
                    <img id="progress_image" style="padding-left:5px;padding-top:5px;" src="img/loading.gif"
                         alt="Processing? Please wait"/><br/>
                    Processing your request. Please wait...
                    <form method="post" action="./bulksubscription.do">
                        <c:set var="message" value="${requestScope.message}"/>
                        <c:choose>
                            <c:when test="${fn:contains(message, 'successfully')}">
                                <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                            </c:when>
                            <c:otherwise>
                                <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                            </c:otherwise>
                        </c:choose>
                        <p>
                            <label for="subject">Short Code:</label><br/>
                            <select name="sc" size="1">
                                <option value="" selected="sel">Select Short Code</option>
                                <c:forEach items="${applicationScope.shortCodesCache}" var="ShortCode">
                                    <option value="<c:out value="${ShortCode.shortCodeID}"/>"><c:out
                                            value="${ShortCode.shortCodeID}"/></option>
                                </c:forEach>
                            </select>
                        </p>
                        <p>
                            <label for="subject">Service:</label><br/>
                            <select name="serv_id" size="1">
                                <option value="" selected="sel">Select Service</option>

                                <%--<c:forEach items="${applicationScope.serviceCache}" var="Service" >--%>
                                <%--<option value="<c:out value="${Service.serviceName}"/>"><c:out value="${Service.serviceName}"/></option>--%>
                                <%--</c:forEach>--%>

                                <%
                                    for (Service service : serviceCache) {
                                %>
                                <option value="<%=service.getServiceID()%>"><%= service.getServiceName().toUpperCase()%>
                                </option>
                                <% } %>
                            </select>
                        </p>
                        <p><label for="message">Numbers </label><br/>
                            <textarea style="width:220px; text-align: left;" name="msisdns" id="message" cols="40"
                                      rows="10"><c:out value="${param.sd}"/></textarea><br/>
                            <input style="background-color: #099; font-weight: bolder; color: #fff; max-width: 100px;"
                                   type="submit" value="SUBSCRIBE" onclick="return loadSubmit()"/>
                        </p>
                    </form>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <ul class="links">
                                <li><a href="new_sub_service.jsp">New Subscription Service</a></li>
                                <li><a href="manage_sub_services.jsp">Manage Services</a></li>
                                <li><a href="manual_sub.jsp">Manual Subscription</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
