<%@page import="org.esme.dao.models.Users" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="org.esme.dto.Service" %>
<%@ page import="javax.servlet.http.HttpSession" %>
<%@ page import="java.util.ArrayList" %>

<div id="main">
    <h2>Update Revenue Target</h2>
    <p>Please select a service to view the target revenue. You may click <a href="revTarget.csv">here</a> to download
        revenue target template</p>
    <c:set var="message" value="${requestScope.message}"/>
    <c:choose>
        <c:when test="${fn:contains(message, 'succes')}">
            <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
        </c:when>
        <c:otherwise>
            <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
        </c:otherwise>
    </c:choose>
    <!--Filter-->
    <%
        HttpSession sess = request.getSession(true);
        String user = (String) sess.getAttribute("userName");
        long userID = 0;
        String userRole = (String) sess.getAttribute("UserRole");
        ArrayList<Service> serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
        ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
        for (Users us : usersCache) {
            if (us.getUserName().equals(user)) {
                userID = us.getId();
            }
        }

    %>
    <select name="service" style=" width: 200px; height: 30px; font-size: 16px;  
            font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; 
            letter-spacing: -1px;" onchange="showRevenueTarget(this.value)">
        <option value="">SELECT SERVICE</option>
        <% for (Service theService : serviceCache) {

            if (userRole.equalsIgnoreCase("Admin")) {
        %>
        <option value="<%=theService.getServiceID()%>"><%=theService.getServiceName().toUpperCase()%>
        </option>
        <%

        } else {
            if (theService.getServiceOwner() == userID) {
        %>
        <option value="<%=theService.getServiceID()%>"><%=theService.getServiceName().toUpperCase()%>
        </option>
        <%
                    }
                }
            }
        %>
    </select>
    <!--End of Filter-->
    <div id="txtHint"><b>Revenue Target will be listed here.</b></div>
</div>