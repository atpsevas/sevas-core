<%@page import="org.esme.dao.models.Users" %>
<%@page import="org.esme.dto.Service" %>
<%@page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.util.ArrayList" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%
    HttpSession sess = request.getSession(true);
    String user = (String) sess.getAttribute("userName");
    long userID = 0;
    String userRole = (String) sess.getAttribute("UserRole");

    ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
    for (Users us : usersCache) {
        if (us.getUserName().equals(user)) {
            userID = us.getId();
            break;
        }
    }

%>

<h3>Services Report Update</h3>
<!--Filter-->
<form action="" method="post">

    <select name="service"
            style=" width: 200px; height: 30px; font-size: 16px;  
            font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; 
            letter-spacing: -1px;" onchange="showReport(this.value)">
        <option>Select A Service</option>
        <% ArrayList<Service> serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            for (Service service : serviceCache) {
                if (userRole.equals("Admin")) {
        %>
        <option value="<%=service.getServiceID()%>"><%=service.getServiceName()%>
        </option>
        <%
        } else {
            if (userID == service.getServiceOwner()) {
        %>
        <option value="<%=service.getServiceID()%>"><%=service.getServiceName()%>
        </option>

        <%
                    }
                }
            }
        %>
    </select>
</form>
<!--End of Filter-->
<b>
    <div id="showRep">
            <%

            Connection conn = null;
            PreparedStatement st = null;
            try {

                conn = (Connection) getServletContext().
                        getAttribute("database.connection");

                if (userRole.equals("Admin")) {
                    st = conn.prepareStatement(
                            (String) getServletContext().getInitParameter("dailyServiceReport_All"));
                } else {

                    st = conn.prepareStatement(
                            "select a.service_name as theService, date(b.sub_request_time) as theDate,"
                            + " count(CASE WHEN (date(b.sub_request_time) = "
                            + " date(CURRENT_TIMESTAMP)) THEN 1 ELSE 0 END) AS newSub,"
                            + " sum((CASE WHEN (b.sub_status =  'active') THEN 1 ELSE 0 END)) AS active_sub,"
                            + " sum((CASE WHEN (b.sub_status =  'inactive') THEN 1 ELSE 0 END)) AS inactive_sub,"
                            + " sum(CASE WHEN (to_date(b.sub_expiry_date, 'YYYY-MM-DD') = "
                            + " date(CURRENT_TIMESTAMP) and a.service_period != 0 ) THEN 1 ELSE 0 END ) AS expired,"
                            + " count(sub_msisdn) AS total_sub FROM subscription_service a, subscription b "
                            + " where a.id = b.sub_service_id and a.service_owner = " + userID + " "
                            + "and extract(month from b.sub_request_time) = extract (month from now())  "
                            + "and extract(year from b.sub_request_time) = extract (year from now())"
                            + " group by theService,theDate order by theService");

                }

                ResultSet rs = st.executeQuery();
                long totalActiveSub = 0;
                long totalInactiveSub = 0;
                long totalExpired = 0;
                long totalSub = 0;
                long totalNewSub = 0;

                String nextService = "";

                while (rs.next()) {

                    if (nextService.equalsIgnoreCase(rs.getString("theService").trim())) {

                    } else {

                        out.println("<table width='100%' class='fancy'>");
                        out.println("<tr colspan='9'>" + rs.getString("theService").toUpperCase() + "</tr>");
                        out.println("<tr>");
                        out.println("<th>Report Date</th>");
                        out.println("<th>New Sub</th>");
                        out.println("<th>Act Sub</th>");
                        out.println("<th>InAct Sub</th>");
                        out.println("<th>Expired</th>");
                        out.println("<th>Total Sub</th>");
                        out.println("</tr>");

                    }

                    out.println("<tr>");
                    out.println("<td>" + rs.getString("theDate") + "</td>");
                    out.println("<td>" + rs.getString("newSub") + "</td>");
                    out.println("<td>" + rs.getLong("active_sub") + "</td>");
                    out.println("<td>" + rs.getLong("inactive_sub") + "</td>");
                    out.println("<td>" + rs.getLong("expired") + "</td>");
                    out.println("<td>" + rs.getLong("total_sub") + "</td>");
                    out.println("</tr>");
                    
                    nextService = rs.getString("theService").trim();
                    
                    totalNewSub += rs.getLong("newSub");
                    totalActiveSub += rs.getLong("active_sub");
                    totalExpired += rs.getLong("expired");
                    totalSub += rs.getLong("total_sub");
                    totalInactiveSub += rs.getLong("inactive_sub");

                }

                out.println("</table>");
                out.println("<u><b> Report Summary</b></u>");
                out.println("<table width='100%' class='fancy'>");
                out.println("<tr>");
                out.println("<th>Report Date</th>");
                out.println("<th>New Sub</th>");
                out.println("<th>Active Sub</th>");
                out.println("<th>InActive Sub</th>");
                out.println("<th>Expired</th>");
                out.println("<th>Total Sub</th>");
                out.println("</tr>");
                out.println("<tr style='font-weight:bold;'>");
                out.println("<td>Total</td>");
                out.println("<td>" + totalNewSub + "</td>");
                out.println("<td>" + totalActiveSub + "</td>");
                out.println("<td>" + totalInactiveSub + "</td>");
                out.println("<td>" + totalExpired + "</td>");
                out.println("<td>" + totalSub + "</td>");
                out.println("</tr>");
                out.println("</table>");

            } catch (Exception e) {
                e.printStackTrace();
            }
            out.println("</table>");
        %>
</b>
</div>

