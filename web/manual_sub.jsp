<%@page import="org.esme.dao.models.ShortCode" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="java.util.ArrayList" %>

<%@include file="partials/header.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>::..:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--FoR Date Picker -->
    <!--FoR Date Picker -->
    <link href="css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.17.custom.js" type="text/javascript"></script>
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="js/dyninfo.js" type="text/javascript"></script>
    <script type="text/javascript">
        function loadSubmit() {
            ProgressImage = document.getElementById('progress_image');
            document.getElementById("progress").style.visibility = "visible";
            setTimeout("ProgressImage.src = ProgressImage.src", 1000);
            return true;
        }
    </script>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li class="on"><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Web Subscription</h2>
                    <p style="visibility:hidden;" id="progress"/>
                    <img id="progress_image" style="padding-left:5px;padding-top:5px;" src="img/loading.gif"
                         alt="Processing? Please wait"/><br/>
                    Processing your request. Please wait...
                    <form method="post" action="./subengine?smscID=web">
                        <c:set var="message" value="${requestScope.message}"/>
                        <c:choose>
                            <c:when test="${fn:contains(message, 'successfully')}">
                                <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                            </c:when>
                            <c:otherwise>
                                <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                            </c:otherwise>
                        </c:choose>
                        <%
                            ArrayList<ShortCode> shortCodeCache = (ArrayList<ShortCode>) getServletContext().getAttribute("shortCodesCache");
                        %>
                        <p>
                            <label>Short Code</label><br/>
                            <select name="shortcode" size="1">
                                <option value="" selected="sel">Select Short Code</option>
                                <%
                                    for (ShortCode shortCode : shortCodeCache) {
                                %>
                                <option value="<%=shortCode.getShortCodeID()%>"><%=shortCode.getShortCodeID()%>
                                </option>
                                <%}%>
                            </select>
                        </p>
                        <p>
                            <label for="subject">Service:</label><br/>
                            <select name="req_text">
                                <option value="all" selected="selected">Select Service</option>
                                <%
                                    for (Service service : serviceCache) {
                                %>
                                <option value="<%=service.getServiceKeyword()%>"><%=service.getServiceName().toUpperCase()%>
                                </option>
                                <%}%>

                            </select>
                        </p>
                        <p>
                            <label for="message">Numbers </label><br/>
                            <input type="text" style="width:200px; text-align: left;" name="msisdn"/><br/>
                        </p>
                        <p>
                            <input style="background-color: #099; font-weight: bolder; color: #fff; max-width: 100px;"
                                   type="submit" value="SUBSCRIBE" onclick="return loadSubmit()"/>
                        </p>
                    </form>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <ul class="links">
                                <li><a href="new_sub_service.jsp">New Subscription Service</a></li>
                                <li><a href="manage_sub_services.jsp">Manage Services</a></li>
                                <li><a href="bulk_subscription.jsp">Bulk Subscription</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
