<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>:::..:::</title>
    <!--[if IE]>
    <link href="css/ie-transparency.css" rel="stylesheet" type="text/css"/><![endif]-->
    <link href="<%=request.getContextPath()%>/css/style.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath()%>/reports/css/charts/chart.css"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath()%>/reports/css/charts/xcharts.min.css"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath()%>/reports/css/charts/bootstrap.min.css"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath()%>/reports/css/daterangepicker.css"/>
    <script type='text/javascript'
            src="<%=request.getContextPath()%>/reports/js/jquery-1.9.1.min.js"></script>
    <script type='text/javascript'
            src="<%=request.getContextPath()%>/reports/js/jquery-migrate-1.2.1.js"></script>
    <script type='text/javascript'
            src="<%=request.getContextPath()%>/reports/js/jquery-ui-1.10.3-custom.min.js"></script>
    <script type='text/javascript'
            src='<%=request.getContextPath()%>/reports/js/charts/d3.min.js'></script>
    <script type='text/javascript'
            src='<%=request.getContextPath()%>/reports/js/charts/sugar.min.js'></script>
    <script type='text/javascript'
            src='<%=request.getContextPath()%>/reports/js/charts/xcharts.js'></script>
    <script type='text/javascript'
            src='<%=request.getContextPath()%>/reports/js/charts/daily_sub_script.js'></script>
    <script type='text/javascript'
            src='<%=request.getContextPath()%>/reports/js/charts/daily_rev_script.js'></script>
    <script type='text/javascript'
            src='<%=request.getContextPath()%>/reports/js/charts/monthly_rev_script.js'></script>
    <script type='text/javascript'
            src='<%=request.getContextPath()%>/reports/js/daterangepicker.js'></script>

    <script src="js/reportSupport.js" type="text/javascript"></script>
    <script>
        function filterReport(start, end, url, str) {
            var startDate = document.getElementById(start).value;
            var endDate = document.getElementById(end).value;
            if (str == "") {
                document.getElementById("txtHint").innerHTML = "No data retrieved";
                return;
            }

            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", url + "?start=" + startDate + "&end=" + endDate, true);
            xmlhttp.send();
        }
    </script>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Revenue Summary</h2>
                    <c:choose>
                        <c:when test="${fn:contains(message, 'successfully')}">
                            <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:when>
                        <c:otherwise>
                            <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:otherwise>
                    </c:choose>
                    <div id="txtHint">
                        <div style="margin-top :10px; ">
                            <table style="margin-top:1px" cellspacing="20" cellpadding="1">
                                <tr style="font-size: 16PX; font-family:Copperplate Gothic Light;">
                                    <td style="padding-right: 25px">
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            INCEPTION
                                        </label>
                                        <c:out value="${param.inceptionTillDateRev}"/><br/>
                                    </td>
                                    <td style="padding-right: 25px">
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            TODAY
                                        </label>
                                        <c:out value="${param.todayRev}"/><br/>
                                    </td>
                                    <td style="padding-right: 25px">
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            DAILY AVE.
                                        </label>
                                        <c:out value="${param.dailyAverageRev}"/><br/>
                                    </td>
                                    <td style="padding-right: 25px">
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            EXTRAPOLATED
                                        </label>
                                        <c:out value="${param.extraPolatedRev}"/>
                                        <br/>
                                    </td>
                                    <td style="padding-right: 25px">
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            CHURN
                                        </label>
                                        <c:out value="${param.dailyChurn}"/>%<br/>
                                    </td>
                                    <td style="padding-right: 25px">
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            TARGET
                                        </label>
                                        <c:out value="${param.monthTargetRev}"/><br/>
                                    </td>
                                    <td style="padding-right: 25px">
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            ACTUAL
                                        </label>
                                        N<c:out value="${param.actualRev}"/><br/>
                                    </td>
                                    <td style="padding-right: 25px">
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            VARIANCE
                                        </label>
                                        <c:out value="${param.revenueVariance}"/>%<br/>
                                    </td>
                                </tr>
                            </table>

                            <div id="msg"></div>

                            <hr style="background-color: orangered;"/>
                            <div class="chart_container">

                                <label class="chart_title">
                                    Daily Revenue Report
                                </label>
                                <div class="chart" id="placeholder_rev">
                                    <figure id="chart_rev"></figure>
                                </div>

                            </div>

                            <hr style="background-color: orangered;"/>
                            <div class="chart_container">
                                <label class="chart_title">
                                    Monthly Revenue Report
                                </label>
                                <div class="chart" id="placeholder_monthly_rev">
                                    <figure id="monthly_chart_rev"></figure>
                                </div>
                            </div>

                            <hr style="background-color: orangered;"/>
                            <div class="chart_container">
                                <label class="chart_title">
                                    Daily Acquisition Report
                                </label>
                                <div class="chart" id="placeholder_sub">
                                    <figure id="chart"></figure>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>iReport</h2>
                            <ul class="links">
                                <li><a href="#" onclick="showUser('new_revenue_target.jsp', this.value)">Set Monthly
                                    Target Revenue</a></li>
                                <li><a href="#" onclick="showUser('update_revenue_target.jsp', this.value)">Update
                                    Monthly Target Revenue</a></li>
                                <li><a href="revTarget.csv">Download Rev Target Template</a></li>
                                <li><a href="iReport.do">Revenue Dashboard</a></li>
                                <li><a href="logs.jsp">Live Traffic</a></li>
                                <li><a href="#" onclick="showUser('report_rev_mgr.jsp', this.value)">Services Revenue
                                    Report</a></li>
                                <li><a href="#" onclick="showUser('report_mgr.jsp', this.value)">Daily Subscription
                                    Report</a></li>
                                <li><a href="#" onclick="showUser('sub_summary.jsp', this.value)">Subscription Report
                                    Summary</a></li>
                                <li><a href="./LogsCSVDownload.csv">Download Today's Traffic Logs</a></li>
                                <li><a href="#" onclick="showUser('daily_paid_content_report.jsp', this.value)">Daily
                                    Paid Content Report</a></li>
                                <li><a href="#" onclick="showUser('content_report_mgr.jsp', this.value)">Content
                                    Delivery Report</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
