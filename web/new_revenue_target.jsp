<%@page import="org.esme.dao.models.Users" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="org.esme.dto.Service" %>
<%@ page import="javax.servlet.http.HttpSession" %>
<%@ page import="java.util.ArrayList" %>

<div id="main">
    <h2>Set Revenue Target</h2>
    <p>Please select a service and upload your target revenue file below. You may click <a href="">here</a> to download
        revenue target template</p>
    <form method="post" enctype="multipart/form-data" action="./revenuetargetupload.do">
        <c:set var="message" value="${requestScope.message}"/>
        <c:choose>
            <c:when test="${fn:contains(message, 'succes')}">
                <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
            </c:when>
            <c:otherwise>
                <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
            </c:otherwise>
        </c:choose>
        <p><label for="subject">Service:</label><br/>
            <select style="width:25%;" name="service" id="subject">
                <option value="0">SELECT A SERVICE</option>
                <%
                    HttpSession sess = request.getSession(true);
                    String user = (String) sess.getAttribute("userName");
                    long userID = 0;
                    String userRole = (String) sess.getAttribute("UserRole");
                    ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
                    for (Users us : usersCache) {
                        if (us.getUserName().equals(user)) {
                            userID = us.getId();
                        }
                    }
                    ArrayList<Service> serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                    for (Service service : serviceCache) {
                        if (userRole.equals("Admin")) {
                %>
                <option value="<%=service.getServiceID()%>"><%=service.getServiceName().toUpperCase()%>
                </option>
                <%
                } else {
                    if (service.getServiceOwner() == userID) {
                %>
                <option value="<%=service.getServiceID()%>"><%=service.getServiceName().toUpperCase()%>
                </option>
                <%
                        }
                    }

                %>
                <%}%>
            </select>
        </p>
        <p><label for="message">Revenue File </label><br/>
            <input style="width:25%;" type="file" name="rev_file" id="content_file"/><br/>
            <input style="background-color: #099; font-weight: bolder; color: #fff; max-width: 100px;" type="submit"
                   value="Upload"/>
        </p>
    </form>
