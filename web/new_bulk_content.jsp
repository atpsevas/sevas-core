<%@page import="org.esme.dao.models.Users" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="org.esme.dto.Service" %>
<%@ page import="javax.servlet.http.HttpSession" %>
<%@ page import="java.util.ArrayList" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>::..::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="js/datetimepicker_css.js"></script>
    <script type="text/javascript">
        function toggleAdd(sel_box, tagToShow) {
            var state = document.getElementById(sel_box);
            var status = state.value;
            if (status == 'schedule') {
                document.getElementById(tagToShow).style.display = 'block';
            } else {
                document.getElementById(tagToShow).style.display = 'none';
            }
        };
    </script>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'mtn'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'etisalat'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'glo'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'airtel'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Upload New Bulk Content</h2>
                    <p>Please select a service and upload your file below.</p>
                    <form method="post" enctype="multipart/form-data" action="./bulkcontentupload.do">
                        <c:set var="message" value="${requestScope.message}"/>
                        <c:choose>
                            <c:when test="${fn:contains(message, 'succes')}">
                                <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                            </c:when>
                            <c:otherwise>
                                <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                            </c:otherwise>
                        </c:choose>
                        <p><label for="subject">Service:</label><br/>
                            <select style="width:25%;" name="service" id="subject">
                                <option value="">SELECT SERVICE</option>
                                <%
                                    HttpSession sess = request.getSession(true);
                                    String user = (String) sess.getAttribute("userName");
                                    long userID = 0;
                                    String userRole = (String) sess.getAttribute("UserRole");
                                    ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
                                    for (Users us : usersCache) {
                                        if (us.getUserName().equals(user)) {
                                            userID = us.getId();
                                        }
                                    }
                                    ArrayList<Service> serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                                    for (Service service : serviceCache) {
                                        if (userRole.equals("Admin")) {
                                %>
                                <option value="<%=service.getServiceID()%>"><%=service.getServiceName().toUpperCase()%>
                                </option>
                                <%
                                } else {
                                    if (service.getServiceOwner() == userID) {
                                %>
                                <option value="<%=service.getServiceID()%>"><%=service.getServiceName().toUpperCase()%>
                                </option>
                                <%
                                        }
                                    }

                                %>
                                <%}%>
                            </select>
                        </p>
                        <p>
                            <label for="message">Content: </label><br/>
                            <input style="width:25%;" type="file" name="conte_file" id="content_file"/><br/>
                            <input style="background-color: #099; font-weight: bolder; color: #fff; max-width: 100px;"
                                   type="submit" value="Upload"/>
                        </p>
                    </form>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <ul class="links">
                                <li><a href="new_content.jsp">Upload New Content</a></li>
                                <li><a href="content_mgr.jsp">Manage Existing Content</a></li>
                                <li><a href="Content_Template.csv">Download Bulk Content Template</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/></div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
