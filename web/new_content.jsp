<%@page import="org.esme.dao.models.Users" %>
<%@page import="javax.servlet.http.HttpSession" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="java.util.ArrayList" %>

<%@include file="partials/header.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>::..:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--FoR Date Picker -->
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--FoR Date Picker -->
    <link href="css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.17.custom.js" type="text/javascript"></script>
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="js/contentSupport.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $(".datepickr").datepicker();
        });

    </script>

    <script type="text/javascript">
        function toggleContentType(serviceField, multimedia, message) {
            var state = document.getElementById(serviceField);
            var status = state.value;
            if (status.includes('voice') |
                status.includes('video') |
                status.includes('image')) {
                document.getElementById(multimedia).style.display = 'block';
                document.getElementById(message).style.display = 'none';
            } else if (status.includes('sms')) {
                document.getElementById(message).style.display = 'block';
                document.getElementById(multimedia).style.display = 'none';
            }
        }
    </script>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Upload New Content</h2>
                    <p>Please select a service and enter the content below.</p>

                    <form method="post" enctype="multipart/form-data" action="./newcontentupload.do">

                        <c:set var="message" value="${requestScope.message}"/>
                        <c:choose>
                            <c:when test="${fn:contains(message, 'successfully')}">
                                <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                            </c:when>
                            <c:otherwise>
                                <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                            </c:otherwise>
                        </c:choose>

                        <%
                            HttpSession sess = request.getSession(true);
                            String user = (String) sess.getAttribute("userName");
                            long userID = 0;
                            String userRole = (String) sess.getAttribute("UserRole");
                            ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
                            for (Users us : usersCache) {
                                if (us.getUserName().equals(user)) {
                                    userID = us.getId();
                                }
                            }

                        %>
                        <p>
                            <label for="subject">Service:</label><br/>
                            <select style=" width: 200px; height: 40px; font-size: 16px;
                                            font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; 
                                            letter-spacing: -1px;" name="serv" id="subject">
                                <option value="">SELECT SERVICE</option>
                                <%
                                    for (Service theService : serviceCache) {
                                        if (userRole.equalsIgnoreCase("Admin")) {
                                %>
                                <option value="<%=theService.getServiceID()%>"><%=theService.getServiceName().toUpperCase()%>
                                </option>
                                <%
                                } else if (theService.getServiceOwner() == userID) {
                                %>
                                <option value="<%=theService.getServiceID()%>"><%=theService.getServiceName().toUpperCase()%>
                                </option>
                                <%
                                        }
                                    }
                                %>
                            </select>
                        </p>
                        <p>
                            <label>Select Date</label><br/>
                            <input value="" style=" width: 180px; height: 30px;
                                           font-size: 16px;  font-family: Candara, Trebuchet, 
                                           Verdana, Arial, Helvetica, sans-serif; 
                                           letter-spacing: -1px;" type="text" name="date" id="date" class="datepickr"
                                   readonly/>
                        </p>
                        <p>
                            <label for="message">Content: </label><br/>
                            <select id="contentType" name="contentType"
                                    onchange="toggleContentType('contentType',
                                                            'multimedia', 'message');">
                                <option value="" selected>Select Content Type</option>
                                <option value="sms">Text</option>
                                <option value="voice">Voice</option>
                                <option value="video">Video</option>
                                <option value="image">Image</option>
                            </select>
                        </p>
                        <p>
                            <input type="file" style="display: none; width: 160px;" id="multimedia" name="multimedia"/>
                        </p>
                        <p>
                                    <textarea style="display: none;" cols="25" name="sd" id="message" rows="5"
                                              onKeyDown="limitText(this.form.sd, this.form.countdown, 320);"
                                              onKeyUp="limitText(this.form.sd, this.form.countdown, 320);"></textarea>
                        </p>
                        <p>
                            You have <input style="width:3%; background: #fff; border: 0px; border-color: #fff" readonly
                                            type="text" name="countdown" size="3" value="320"/>Characters remaining<br/>
                            <input style="background-color: #099; font-weight: bolder; color: #fff; max-width: 100px;"
                                   type="submit" value="UPLOAD"/>
                        </p>
                    </form>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <ul class="links">
                                <li><a href="instant_content.jsp">Instant Content</a></li>
                                <li><a href="new_bulk_content.jsp">Upload Bulk Content</a></li>
                                <li><a href="mm_webcontent_upload.jsp">Mobile Market Content</a></li>
                                <li><a href="content_mgr.jsp">Manage Existing Content</a></li>
                                <li><a href="Content_Template.csv">Download Bulk Content Template</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
