<%@page import="java.sql.Connection" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">
<head>
    <title>:::...:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css"/>
    <script>
        $(function () {
            $("#datepicker").datepicker();
        });
    </script>
    <script>
        $(function () {
            $("#datepicker2").datepicker();
        });
    </script>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h3>Subscriber Transaction History</h3>
                    <c:set var="message" value="${requestScope.message}"/>
                    <c:choose>
                        <c:when test="${fn:contains(message, 'succes')}">
                            <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:when>
                        <c:otherwise>
                            <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:otherwise>
                    </c:choose>
                    <!--Filter-->
                    <form action="" method="post">
                        MISDN <input type="text" id="msisdn" name="msisdn"
                                     style=" width: 150px; height: 30px; font-size: 16px;  font-family: Candara;"
                                     value=AppBroker.CountryCodePrefix_PROP/>
                        Start Date: <input name=sDate" type="text" id="datepicker"
                                           style=" width: 100px; height: 30px; font-size: 16px;  font-family: Candara;"/>
                        End Date: <input name="eDate" type="text" id="datepicker2"
                                         style=" width: 100px; height: 30px; font-size: 16px;  font-family: Candara;"/>
                        <input style="background-color: #000; font-weight: bolder; height: 35px; color: #fff; "
                               type="submit" value="SEARCH" onclick="showUserActivities()"/>
                    </form>
                    <!--End of Filter-->
                    <div id="txtHint">
                        <b>
                            <%
                                String msisdn = null;
                                Statement st = null;
                                ResultSet rs = null;
                                Connection conn = null;
                                String sDate = null;
                                String eDate = null;
                                int index = 0;

                                try {
                                    msisdn = request.getParameter("msisdn");
                                    //Set date attribute for othe pages to use..

                                } catch (Exception e) {
                                    out.println("<div style='color:red'> Wrong MSISDN format. </div>");
                                }

                                try {
                                    if (msisdn != null) {
                                        getServletContext().setAttribute("msisdn", msisdn);

                                        conn = (Connection) getServletContext().
                                                getAttribute("database.connection");
                                        st = conn.createStatement();
                                        if (sDate == null || eDate == null) {
                                            rs = st.executeQuery(
                                                    "select a.sub_request_time,a.sub_status, a.sub_source, a.sub_request_text, a.sub_msisdn,a.last_billed_date, "
                                                            + "a.sub_expiry_date,a.un_sub_date, a.un_sub_source, b.service_price, "
                                                            + "a.sub_status from subscription a, subscription_service b "
                                                            + "where a.sub_msisdn ilike '%" + msisdn.substring(msisdn.length() - 10) + "%' "
                                                            + "and a.sub_service_id = b.id order by a.sub_request_time desc");
                                        } else {

                                            getServletContext().setAttribute("sDate", sDate);
                                            getServletContext().setAttribute("eDate", eDate);
                                            rs = st.executeQuery(
                                                    "select a.sub_request_time, a.sub_request_text, a.sub_status, a.sub_source,"
                                                            + "a.sub_msisdn,a.last_billed_date, "
                                                            + "a.sub_expiry_date,a.un_sub_date, a.un_sub_source, b.service_price, "
                                                            + "a.sub_status from subscription a, subscription_service b "
                                                            + "where a.sub_msisdn like '%" + msisdn.substring(msisdn.length() - 10) + "%' "
                                                            + "and (to_date(to_char(a.sub_request_time, 'MM DD YYYY'), 'MM DD YYYY') "
                                                            + "between '" + sDate + "' and '" + eDate + "') "
                                                            + "and a.sub_service_id = b.id order by a.sub_request_time desc");
                                        }
                                        out.println("<b>" + msisdn + "</b>");
                                        out.println("<hr/>");
                                        out.println("<form method='post' action='./deactivate.do'>");
                                        out.println("<input type='text' name='msisdn' style = 'visibility:hidden;' value=" + msisdn + "/>");
                                        out.println("<input type='text' name='network' style = 'visibility:hidden;' value='mtn'/>");
                                        out.println("<input  style='background-color: #000; font-weight: bolder; color: #fff; max-width: 100px;'  type='submit'  value='DEACTIVATE'/>");
                                        out.println("<table width='100%' class='fancy'>");
                                        out.println("<tr>");
                                        out.println("<th>Subscription Mode</th>");
                                        out.println("<th>Subscription Time</th>");
                                        out.println("<th>Keyword</th>");
                                        out.println("<th>Last Charged</th>");
                                        out.println("<th>Service Charged</th>");
                                        out.println("<th>Next Charged</th>");
                                        out.println("<th>Status</th>");
                                        out.println("<th>UnSub Mode</th>");
                                        out.println("<th>UnSub Time</th>");
                                        out.println("</tr>");
                                        while (rs.next()) {

                                            index++;
                                            String status = rs.getString("sub_status");
                                            String nextCharge = rs.getString("sub_expiry_date");

                                            try {
                                                if (rs.getString("sub_status").equalsIgnoreCase("inactive")) {
                                                    try {
                                                        if (rs.getString("un_sub_source").equalsIgnoreCase("sms")) {
                                                            status = "UNSUBSCRIBED";
                                                        } else {
                                                            status = "DEACTIVATED";
                                                        }
                                                    } catch (Exception w) {

                                                    }
                                                    nextCharge = "";
                                                }
                                            } catch (Exception e) {
                                            }

                                            out.println("<tr>");
                                            out.println("<td>SMS</td>");
                                            out.println("<td>" + rs.getString("sub_request_time").substring(0, 19) + "</td>");
                                            out.println("<td>" + rs.getString("sub_request_text").toUpperCase() + "</td>");
                                            out.println("<td>" + rs.getString("last_billed_date").substring(0, 19) + "</td>");
                                            out.println("<td>" + rs.getString("service_price") + "</td>");
                                            out.println("<td>" + nextCharge + "</td>");
                                            out.println("<td>" + status.toUpperCase() + "</td>");
                                            try {
                                                if (rs.getString("un_sub_source").equalsIgnoreCase("") || rs.getString("un_sub_source") == null) {
                                                    out.println("<td></td>");
                                                }
                                                out.println("<td>" + rs.getString("un_sub_source").toUpperCase() + "</td>");
                                            } catch (Exception e) {
                                                out.println("<td></td>");
                                            }
                                            String unSubDate = "";
                                            try {
                                                unSubDate = rs.getString("un_sub_date").substring(0, 19);
                                                if (unSubDate.equalsIgnoreCase("") || unSubDate == null) {
                                                    status = "";
                                                }
                                            } catch (Exception e) {

                                            }
                                            out.println("<td>" + unSubDate + "</td>");
                                            out.println("</tr>");
                                        }
                                        if (index < 1) {
                                            out.println("<div style='color:red'>" + msisdn + " is currently not subscribed to any service</div>");
                                        }
                                        out.println("</table>");
                                        out.println("</form>");
                                    } else {
                                        out.println("<div style='color:red'>Enter new MSISDN</div>");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            %>
                        </b>
                    </div>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <ul class="links">
                                <li><a href="sms_history.jsp">Messaging History</a></li>
                                <li><a href="dnd.jsp">Do Not Disturb</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
