<%@page import="java.sql.Connection" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">
<head>
    <title>:::...:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <%
                        String theMSISDN = (String) getServletContext().getAttribute("msisdn");

                    %>
                    <h3><%=theMSISDN%> Messaging History</h3>
                    <c:set var="message" value="${requestScope.message}"/>
                    <c:choose>
                        <c:when test="${fn:contains(message, 'succes')}">
                            <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:when>
                        <c:otherwise>
                            <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:otherwise>
                    </c:choose>
                    <form action="deactivate.jsp" method="post">
                        MISDN <input type="text" id="msisdn" name="msisdn"
                                     style=" width: 150px; height: 30px; font-size: 16px;  font-family: Candara;"
                                     value=AppBroker.CountryCodePrefix_PROP/>
                        Start Date: <input name=sDate" type="text" id="datepicker"
                                           style=" width: 100px; height: 30px; font-size: 16px;  font-family: Candara;"/>
                        End Date: <input name="eDate" type="text" id="datepicker2"
                                         style=" width: 100px; height: 30px; font-size: 16px;  font-family: Candara;"/>
                        <input style="background-color: #000; font-weight: bolder; height: 35px; color: #fff; "
                               type="submit" value="SEARCH" onclick="showUserActivities()"/>
                    </form>
                    <div id="txtHint">
                        <b>
                            <%
                                String msisdn = null;
                                String sDate = null;
                                String eDate = null;
                                Statement st = null;
                                ResultSet rs = null;
                                Connection conn = null;
                                int index = 0;
                                try {
                                    msisdn = (String) getServletContext().getAttribute("msisdn");
                                } catch (Exception e) {
                                    out.println("<div style='color:red'> Wrong MSISDN format. </div>");
                                }
                                try {
                                    if (msisdn != null) {
                                        conn = (Connection) getServletContext().
                                                getAttribute("database.connection");
                                        st = conn.createStatement();
                                        if (sDate == null || eDate == null) {
                                            rs = st.executeQuery("select serv_name, sender, message, "
                                                    + "sent_time, deliver_time, "
                                                    + "delivery_status from outgoing_messages  "
                                                    + "where recipient ilike '%" + msisdn.substring(msisdn.length() - 10) + "%' order by sent_time desc");
                                        } else {
                                            sDate = (String) getServletContext().getAttribute("sDate");
                                            eDate = (String) getServletContext().getAttribute("eDate");
                                            rs = st.executeQuery("select serv_name, sender, message, "
                                                    + "sent_time, deliver_time, "
                                                    + "delivery_status from outgoing_messages  "
                                                    + "where recipient ilike '%" + msisdn.substring(msisdn.length() - 10) + "%' "
                                                    + "and (to_date(to_char(sent_time, 'MM DD YYYY'), 'MM DD YYYY') between '" + sDate + "' and '" + eDate + "') order by sent_time desc");
                                        }
                                        out.println("<hr/>");
                                        out.println("<form method='post' action='./deactivate.do'>");
                                        out.println("<input type='text' name='msisdn' style = 'visibility:hidden;' value=" + msisdn + "/>");
                                        out.println("<input  style='background-color: #000; font-weight: bolder; color: #fff; max-width: 100px;'  type='submit'  value='DEACTIVATE'/>");
                                        out.println("<table width='100%' class='fancy'>");
                                        out.println("<tr>");
                                        out.println("<th>Event Type</th>");
                                        out.println("<th>Messaging Code</th>");
                                        out.println("<th>Text</th>");
                                        out.println("<th>Sent Time</th>");
                                        out.println("<th>Deliver Time</th>");
                                        out.println("<th>Delivery Status</th>");
                                        out.println("</tr>");

                                        while (rs.next()) {
                                            out.println("<tr>");
                                            out.println("<td>" + rs.getString("serv_name") + "</td>");
                                            out.println("<td>" + rs.getString("sender") + "</td>");
                                            out.println("<td>" + rs.getString("message") + "</td>");
                                            out.println("<td>" + rs.getString("sent_time").substring(0, 23) + "</td>");
                                            out.println("<td>" + rs.getString("deliver_time").substring(0, 23) + "</td>");
                                            out.println("<td>" + rs.getString("delivery_status") + "</td>");
                                            out.println("</tr>");
                                        }
                                        out.println("</table>");
                                        out.println("</form>");
                                    }
                                } catch (Exception e) {
                                }
                            %>
                        </b>
                    </div>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <!--<h2>Features</h2>
                            <ul class="links">
                            <li><a href="deactivate.jsp">Billing History</a></li>
                            <li><a href="dnd.jsp">Blacklist</a></li>
                            <li><a href="dnd_bulk.jsp">Bulk Blacklist</a></li>
                            <li><a href="./logout">[Logout]</a></li>
                            </ul>-->
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
