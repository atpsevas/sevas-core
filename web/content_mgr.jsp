<%@page import="org.esme.dao.models.Users" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="javax.servlet.http.HttpSession" %>
<%@ page import="java.util.ArrayList" %>

<%@include file="partials/header.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">
<head>
    <title>:::...:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--FoR Date Picker -->
    <link href="css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.17.custom.js" type="text/javascript"></script>
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="js/contentSupport.js" type="text/javascript"></script>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h3>Content Manager</h3>
                    <c:set var="message" value="${requestScope.message}"/>
                    <c:choose>
                        <c:when test="${fn:contains(message, 'succes')}">
                            <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:when>
                        <c:otherwise>
                            <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:otherwise>
                    </c:choose>
                    <!--Filter-->
                    <%
                        HttpSession sess = request.getSession(true);
                        String user = (String) sess.getAttribute("userName");
                        long userID = 0;
                        String userRole = (String) sess.getAttribute("UserRole");
                        ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
                        for (Users us : usersCache) {
                            if (us.getUserName().equals(user)) {
                                userID = us.getId();
                            }
                        }

                    %>
                    <select name="service"
                            style=" width: 200px; height: 30px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;"
                            onchange="showUser(this.value)">
                        <option value="">SELECT SERVICE</option>
                        <%
                            for (Service theService : serviceCache) {

                                if (userRole.equalsIgnoreCase("Admin")) {
                        %>
                        <option value="<%=theService.getServiceID()%>"><%=theService.getServiceName().toUpperCase()%>
                        </option>
                        <%

                        } else {
                            if (theService.getServiceOwner() == userID) {
                        %>
                        <option value="<%=theService.getServiceID()%>"><%=theService.getServiceName().toUpperCase()%>
                        </option>
                        <%
                                    }
                                }
                            }
                        %>
                    </select>
                    <!--End of Filter-->
                    <div id="txtHint"><b>Content will be listed here.</b></div>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <ul class="links">
                                <li><a href="instant_content.jsp">Instant Content</a></li>
                                <li><a href="new_bulk_content.jsp">Upload Bulk Content</a></li>
                                <li><a href="mm_webcontent_upload.jsp">Mobile Market Content</a></li>
                                <li><a href="content_mgr.jsp">Manage Existing Content</a></li>
                                <li><a href="Content_Template.csv">Download Bulk Content Template</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
