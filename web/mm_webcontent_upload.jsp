<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>::..::::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--FoR Date Picker -->
    <link href="css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.17.custom.js" type="text/javascript"></script>
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="js/contentSupport.js" type="text/javascript"></script>
    <!--[if IE]>
    <link href="css/ie-transparency.css" rel="stylesheet" type="text/css"/><![endif]-->
</head>

<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <li class="first"><a href="start.jsp">Home</a></li>
            <li class="on"><a href="new_account.jsp">New Account</a></li>
            <li><a href="new_network.jsp">Network</a></li>
            <li><a href="shortcode.jsp">Short Code</a></li>
            <li><a href="services.jsp">Services</a></li>
            <li><a href="sms_campaign.jsp">Campaign</a></li>
            <li><a href="contents.jsp">Content</a></li>
            <li><a href="iReport.do">iReport</a></li>
            <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
            <li><a href="gateway.jsp">SMSC</a></li>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Upload new item</h2>
                    <form method="post" action="SMSItemsListingWebUploader.do">
                        <fieldset>
                            <legend>User</legend>
                            <c:set var="message" value="${requestScope.message}"/>
                            <c:choose>
                                <c:when test="${fn:contains(message, 'successfully')}">
                                    <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                                </c:when>
                                <c:otherwise>
                                    <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                                </c:otherwise>
                            </c:choose>
                            <p>
                                <label for="subject">Seller Name: </label>
                                <input type="text" name="sn" value=""
                                       style=" width: 400px; height: 20px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;"/>
                            </p>
                            <p>
                                <label for="subject">Seller Location: </label>
                                <input type="text" name="sl"
                                       style=" width: 400px; height: 20px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;"/>
                            </p>
                            <p>
                                <label for="subject">Seller Email: </label>
                                <input type="text" name="se" value=""
                                       style=" width: 200px; height: 20px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;"/>
                            </p>
                            <p>
                                <label for="subject">Seller Phone: </label>
                                <input type="text" name="sp"
                                       style=" width: 200px; height: 20px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;"/>
                            </p>
                            <p>
                                <label for="subject">Item Category: </label>
                                <select name="ic" id="subject"
                                        style=" width: 200px; height: 40px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;">
                                    <option value="Accessories">Accessories</option>
                                    <option value="Animals and Pets">Animals and Pets</option>
                                    <option value="Books and Media">Books and Media</option
                                    <option value="Fashion Items">Fashion Items</option>
                                    <option value="Computers">Computers</option>
                                    <option value="Electronics">Electronics</option>
                                    <option value="Phones">Phones</option
                                    <option value="Vehicles">Vehicles</option>
                                    <option value="Houses">Houses</option>
                                    <option value="Softwares">Softwares</option>
                                    <option value="Furniture">Furniture</option>
                                </select>
                            </p>
                            <p>
                                <label for="subject">Item Title: </label>
                                <input type="text" name="it"
                                       style=" width: 400px; height: 20px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;"
                                       onKeyDown="limitText(this.form.it, this.form.countdown1, 50);"
                                       onKeyUp="limitText(this.form.it, this.form.countdown1, 50);"/><br/>
                                You have <input style="width:3%; background: #fff; border: 0px; border-color: #fff"
                                                readonly type="text" name="countdown1" size="3" value="50"/>Characters
                                remaining<br/>
                            </p>
                            <p>
                                <label for="message">Item Description: </label><br/>
                                <textarea cols="22" name="id" id="message" rows="5"
                                          onKeyDown="limitText(this.form.id, this.form.countdown, 90);"
                                          onKeyUp="limitText(this.form.id, this.form.countdown, 90);"></textarea><br/>
                            </p>
                            <p>
                                You have <input style="width:3%; background: #fff; border: 0px; border-color: #fff"
                                                readonly type="text" name="countdown" size="3" value="90"/>Characters
                                remaining<br/>
                                <input style="background-color: #099; font-weight: bolder; color: #fff; max-width: 100px;"
                                       type="submit" value="UPLOAD"/>
                            </p>
                        </fieldset>
                    </form>
                </div>
                <div id="sub">
                    <h2>Features</h2>
                    <ul class="links">
                        <li><a href="instant_content.jsp">Instant Content</a></li>
                        <li><a href="new_bulk_content.jsp">Upload Bulk Content</a></li>
                        <li><a href="content_mgr.jsp">Manage Existing Content</a></li>
                        <li><a href="Content_Template.csv">Download Bulk Content Template</a></li>
                        <li><a href="./logout">[Logout]</a></li>
                    </ul>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
