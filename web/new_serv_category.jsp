<%@page import="org.esme.dao.models.ServiceCategory" %>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>::..::::</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="imagetoolbar" content="no"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <meta name="copyright" content=""/>
    <meta name="revisit-after" content="3 days"/>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--[if IE]>
    <link href="css/ie-transparency.css" rel="stylesheet" type="text/css"/><![endif]-->
</head>

<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li class="on"><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Add Category</h2>
                    <%
                        ArrayList<ServiceCategory> categoryCache = (ArrayList<ServiceCategory>) getServletContext().getAttribute("categoryCache");
                        ServiceCategory category = null;
                    %>
                    <c:choose>
                        <c:when test="${param.categoryID ==  null}">
                            <c:set var="module" value="./newcategory.do"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="module" value="./updatecategory.do?action=update"/>
                        </c:otherwise>
                    </c:choose>
                    <form method="post" action="${module}">
                        <fieldset>
                            <legend>User</legend>
                            <c:set var="message" value="${requestScope.message}"/>
                            <c:choose>
                                <c:when test="${fn:contains(message, 'successfully')}">
                                    <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                                </c:when>
                                <c:otherwise>
                                    <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                                </c:otherwise>
                            </c:choose>
                            <c:choose>
                                <c:when test="${param.categoryID ==  null}">
                                    <p>Please enter category details below.</p>

                                    <p>
                                        <label for="subject">Category Name: </label>
                                        <input type="text" name="catName" value=""/>
                                    </p>
                                    <p>
                                        <label for="subject">Category Keyword</label>
                                        <input type="text" name="catKeyword" value=""/>
                                    </p>
                                    <p>
                                        <input type="submit" value="CREATE" class="btn"/>
                                    </p>
                                </c:when>
                                <c:otherwise>
                                    <p>Update category details below.</p>
                                    <%
                                        for (ServiceCategory cat : categoryCache) {
                                            if (cat.getCategoryID() == Long.parseLong(request.getParameter("categoryID"))) {
                                                category = cat;
                                            }
                                        }
                                    %>
                                    <p>
                                        <input type="hidden" name="categoryID" value="<%=category.getCategoryID()%>"/>
                                        <label for="subject">Category Name: </label>
                                        <input disabled type="text" name="catName"
                                               value="<%=category.getCategoryName()%>"/>
                                    </p>
                                    <p>
                                        <label for="subject">Category Keyword</label>
                                        <input type="text" name="catKeyword"
                                               value="<%=category.getCategoryKeyword()%>"/>
                                    </p>
                                    <p>
                                        <input type="submit" value="UPDATE" class="btn"/>
                                    </p>
                                </c:otherwise>
                            </c:choose>
                        </fieldset>
                    </form>
                </div>
                <div id="sub">
                    <h2>Features</h2>
                    <ul class="links">
                        <li><a href="manage_serv_category.jsp">Manage Service Category</a></li>
                        <li><a href="new_serv_bundle.jsp">New Service Bundle</a></li>
                        <li><a href="new_sub_service.jsp">New Subscription Service</a></li>
                        <li><a href="manage_sub_services.jsp">Manage Services</a></li>
                        <li><a href="manage_services_billing.jsp">Manage Services Billing</a></li>
                        <li><a href="manual_sub.jsp">Manual Subscription</a></li>
                        <li><a href="bulk_subscription.jsp">Bulk Subscription</a></li>
                        <li><a href="./logout">[Logout]</a></li>
                    </ul>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
