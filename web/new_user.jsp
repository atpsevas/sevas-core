<%@page import="org.esme.dao.models.UserRole" %>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>::..::::</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="imagetoolbar" content="no"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <meta name="copyright" content=""/>
    <meta name="revisit-after" content="3 days"/>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--[if IE]>
    <link href="css/ie-transparency.css" rel="stylesheet" type="text/css"/><![endif]-->
</head>

<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Create Content Administrator</h2>
                    <c:choose>
                        <c:when test="${param.username ==  null}">
                            <c:set var="module" value="./cnu.do"/>
                            <p>Please enter new content administrator details below.</p>
                        </c:when>
                        <c:otherwise>
                            <c:set var="module" value="./updateusers.do?action=update"/>
                            <p>Update content user details below.</p>
                        </c:otherwise>
                    </c:choose>
                    <form method="post" action="${module}">
                        <fieldset>
                            <legend>User</legend>
                            <c:set var="message" value="${requestScope.message}"/>
                            <c:choose>
                                <c:when test="${fn:contains(message, 'successfully')}">
                                    <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                                </c:when>
                                <c:otherwise>
                                    <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                                </c:otherwise>
                            </c:choose>
                            <c:choose>
                                <c:when test="${param.username ==  null}">
                                    <p>
                                        <label for="subject">User ID: </label>
                                        <input type="text" name="username" value=""/>
                                    </p>
                                    <p>
                                        <label for="subject">Password: </label>
                                        <input type="password" name="password"/>
                                    </p>
                                    <p>
                                        <label for="subject">Role: </label>
                                        <select name="role" id="subject">
                                            <option value="Blacklister">Blacklister</option>
                                            <option value="Content">Content Administrator</option>
                                            <option value="User">Content/Log Administrator</option>
                                            <option value="Deactivate">Deactivate</option>
                                            <option value="Logviewer">Log Viewer</option>
                                            <option value="Admin">Portal Administrator</option>
                                            <option value="SendSMS">Send SMS</option>
                                            <option value="mtn">MTN</option>
                                        </select><br/>
                                        <input type="submit" value="CREATE" class="btn"/>
                                    </p>
                                </c:when>
                                <c:otherwise>
                                    <%
                                        ArrayList<UserRole> userRoleCache = (ArrayList<UserRole>) getServletContext().getAttribute("userRoleCache");
                                        String userName = "";
                                        String userRoleName = "";
                                        for (UserRole uRole : userRoleCache) {
                                            if (uRole.getUserName().equalsIgnoreCase(request.getParameter("username"))) {
                                                userName = uRole.getUserName();
                                                userRoleName = uRole.getRoleName();
                                            }
                                        }

                                    %>
                                    <p>
                                        <label for="subject">User ID: </label>
                                        <input type="text" name="username" value="<%=userName%>" readonly/>
                                    </p>
                                    <p>
                                        <label for="subject">Password: </label>
                                        <input type="password" name="password"/><br/>
                                        <label>&nbsp;</label> <input type="submit" value="CREATE" class="btn"/><br/>
                                        <input type="submit" value="UPDATE" class="btn"/>
                                    </p>
                                    <p>
                                        <label for="subject">Role: </label>
                                        <select name="role" id="subject">
                                            <option selected value="<%=userRoleName%>"><%=userRoleName%>
                                            </option>
                                            <option value="Blacklister">Blacklister</option>
                                            <option value="Content">Content Administrator</option>
                                            <option value="User">Content/Log Administrator</option>
                                            <option value="Deactivate">Deactivate</option>
                                            <option value="Logviewer">Log Viewer</option>
                                            <option value="Admin">Portal Administrator</option>
                                            <option value="SendSMS">Send SMS</option>
                                        </select><br/>
                                        <input type="submit" value="CREATE" class="btn"/>
                                    </p>
                                </c:otherwise>
                            </c:choose>
                        </fieldset>
                    </form>
                </div>
                <div id="sub">
                    <h2>Manage User Accounts</h2>
                    <ul class="links">
                        <li><a href="new_user.jsp">Create New User</a></li>
                        <li><a href="manage_users.jsp">Manage Existing Users</a></li>
                        <li><a href="./logout">[Logout]</a></li>
                    </ul>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
