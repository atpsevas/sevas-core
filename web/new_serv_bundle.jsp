<%@page import="org.esme.dao.models.ServiceBundle" %>
<%@page import="org.esme.dto.Service" %>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>::..::::</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="imagetoolbar" content="no"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <meta name="copyright" content=""/>
    <meta name="revisit-after" content="3 days"/>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--[if IE]>
    <link href="css/ie-transparency.css" rel="stylesheet" type="text/css"/><![endif]-->
</head>

<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li class="on"><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <%
                        ArrayList<ServiceBundle> bundleCache = (ArrayList<ServiceBundle>) getServletContext().getAttribute("bundleCache");
                        ServiceBundle bundle = null;
                    %>
                    <h2>Add Bundle</h2>
                    <c:choose>
                        <c:when test="${param.bundleID ==  null}">
                            <c:set var="module" value="./newbundle.do"/>
                            <p>Please enter bundle details below.</p>
                        </c:when>
                        <c:otherwise>
                            <c:set var="module" value="./updateservbundle.do?action=update"/>
                            <p>Update bundle details below.</p>
                        </c:otherwise>
                    </c:choose>
                    <form method="post" action="${module}">
                        <fieldset>
                            <legend>Add Bundle</legend>

                            <c:set var="message" value="${requestScope.message}"/>
                            <c:choose>
                                <c:when test="${fn:contains(message, 'successfully')}">
                                    <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                                </c:when>
                                <c:otherwise>
                                    <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                                </c:otherwise>
                            </c:choose>

                            <c:choose>
                                <c:when test="${param.bundleID ==  null}">
                                    <p>Please enter bundle details below.</p>

                                    <p>
                                        <label for="subject">Bundle Name: </label>
                                        <input type="text" name="bundleName" value=""/>
                                    </p>
                                    <p>
                                        <label for="subject">Bundle Keyword</label>
                                        <input type="text" name="bundleKeyword" value=""/>
                                    </p>
                                    <p>
                                        <label for="subject">Bundle Price</label>
                                        <select name="bundlePrice">
                                            <option value="0">N0</option>
                                            <option value="5">N5</option>
                                            <option value="10">N10</option>
                                            <option value="15">N15</option>
                                            <option value="20">N20</option>
                                            <option value="25">N25</option>
                                            <option value="30">N30</option>
                                            <option value="50">N50</option>
                                            <option value="75">N75</option>
                                            <option value="100">N100</option>
                                        </select>
                                    </p>
                                    <p>
                                        <label for="subject">Bundle Subscription Message</label>
                                        <textarea name="bundleSubMessage" cols="40" rows="5"></textarea>
                                    </p>
                                    <p>
                                        <label for="subject">Bundle UnSubscription Message</label>
                                        <textarea name="bundleUnsubMessage" cols="40" rows="5"></textarea>
                                    </p>
                                    <p>
                                        <label for="subject">Add Default Service</label>
                                        <option value="">SELECT SERVICE</option>
                                        <select style=" width: 200px; height: 40px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;"
                                                name="serv" id="subject">
                                            <%
                                                ArrayList<Service> serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                                                for (Service theService : serviceCache) {
                                            %>
                                            <option value="<%=theService.getServiceID()%>"><%=theService.getServiceName().toUpperCase()%>
                                            </option>
                                            <%
                                                }

                                            %>

                                        </select>
                                    </p>
                                    <p>

                                        <input type="submit" value="CREATE" class="btn"/>
                                    </p>
                                </c:when>
                                <c:otherwise>
                                    <p>Update bundle details below.</p>
                                    <% for (ServiceBundle bun : bundleCache) {
                                        if (bun.getBundleID() == Long.parseLong(request.getParameter("bundleID"))) {
                                            bundle = bun;
                                        }
                                    }
                                    %>
                                    <input type="hidden" name="bundleID" value="<%=bundle.getBundleID()%>"/>
                                    <p>
                                        <label for="subject">Bundle Name: </label>
                                        <input disabled type="text" name="bundleName"
                                               value="<%=bundle.getBundleName()%>"/>
                                    </p>
                                    <p>
                                        <label for="subject">Bundle Keyword</label>
                                        <input type="text" name="bundleKeyword" value="<%=bundle.getBundleKeyword()%>"/>
                                    </p>
                                    <p>
                                        <label for="subject">Bundle Price</label>
                                        <select name="bundlePrice">
                                            <option selected
                                                    value="<%=bundle.getBundlePrice()%>"><%=bundle.getBundlePrice()%>
                                            </option>
                                            <option value="0">N0</option>
                                            <option value="5">N5</option>
                                            <option value="10">N10</option>
                                            <option value="20">N20</option>
                                            <option value="30">N30</option>
                                            <option value="50">N50</option>
                                            <option value="100">N100</option>
                                        </select>
                                    </p>
                                    <p>
                                        <label for="subject">Bundle Subscription Message</label>
                                        <textarea name="bundleSubMessage" cols="40"
                                                  rows="5"><%=bundle.getBundleSubscriptionMessage()%></textarea>
                                    </p>
                                    <p>
                                        <label for="subject">Bundle UnSubscription Message</label>
                                        <textarea name="bundleUnsubMessage" cols="40"
                                                  rows="5"><%=bundle.getBundleUnsubscriptionMessage()%></textarea>
                                    </p>
                                    <p>
                                        <input type="submit" value="UPDATE" class="btn"/>
                                    </p>
                                </c:otherwise>
                            </c:choose>
                        </fieldset>
                    </form>
                </div>
                <div id="sub">
                    <h2>Features</h2>
                    <ul class="links">
                        <li><a href="new_serv_category.jsp">New Service Category</a></li>
                        <li><a href="new_serv_bundle.jsp">New Service Bundle</a></li>
                        <li><a href="manage_serv_category.jsp">Manage Services Category</a></li>
                        <li><a href="manage_serv_bundle.jsp">Manage Services Bundle</a></li>
                        <li><a href="manage_sub_services.jsp">Manage Services</a></li>
                        <li><a href="manage_services_billing.jsp">Manage Services Billing</a></li>
                        <li><a href="manual_sub.jsp">Manual Subscription</a></li>
                        <li><a href="bulk_subscription.jsp">Bulk Subscription</a></li>
                        <li><a href="./logout">[Logout]</a></li>
                    </ul>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
