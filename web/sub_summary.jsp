<%@page import="org.esme.dao.models.Users" %>
<%@page import="org.esme.dto.Service" %>
<%@page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.util.ArrayList" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%
    HttpSession sess = request.getSession(true);
    String user = (String) sess.getAttribute("userName");
    long userID = 0;
    String userRole = (String) sess.getAttribute("UserRole");

    ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
    for (Users us : usersCache) {
        if (us.getUserName().equals(user)) {
            userID = us.getId();
            break;
        }
    }

%>

<h3>Services Report Update</h3>
<!--Filter-->
<form action="" method="post">

    <select name="service"
            style=" width: 200px; height: 30px; font-size: 16px;  
            font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; 
            letter-spacing: -1px;" onchange="showReport(this.value)">
        <option>Select A Service</option>
        <% ArrayList<Service> serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            for (Service service : serviceCache) {
                if (userRole.equals("Admin")) {
        %>
        <option value="<%=service.getServiceID()%>"><%=service.getServiceName()%>
        </option>
        <%
        } else {
            if (userID == service.getServiceOwner()) {
        %>
        <option value="<%=service.getServiceID()%>"><%=service.getServiceName()%>
        </option>

        <%
                    }
                }
            }
        %>
    </select>
</form>
<!--End of Filter-->
<b>
    <div id="showRep">
            <%

            Connection conn = null;
            PreparedStatement st = null;
            try {

                conn = (Connection) getServletContext().
                        getAttribute("database.connection");

                if (userRole.equals("Admin")) {
                    st = conn.prepareStatement(
                            (String) getServletContext().getInitParameter("dailyServiceReport_All_Summary"));
                } else {

                    st = conn.prepareStatement("select a.service_name as theService,"
                            + "sum((CASE WHEN (b.sub_status =  'active') THEN 1 ELSE 0 END)) AS active_sub,"
                            + "sum((CASE WHEN (b.sub_status =  'inactive') THEN 1 ELSE 0 END)) AS inactive_sub,"
                            + "count(sub_msisdn) AS total_sub FROM subscription_service a, subscription b "
                            + "where a.id = b.sub_service_id and a.service_owner = " + userID + ""
                            + "group by theService order by theService");
                }

                ResultSet rs = st.executeQuery();
                out.println("<table width='100%' class='fancy'>");
                out.println("<tr>");
                out.println("<th>Service Name</th>");
                out.println("<th>Active Sub</th>");
                out.println("<th>Inactive Sub</th>");
                out.println("<th>Total Sub</th>");
                out.println("</tr>");
                while (rs.next()) {
                    out.println("<tr>");
                    out.println("<td>" + rs.getString("theService") + "</td>");
                    out.println("<td>" + rs.getString("active_sub") + "</td>");
                    out.println("<td>" + rs.getLong("inactive_sub") + "</td>");
                    out.println("<td>" + rs.getLong("total_sub") + "</td>");
                    out.println("</tr>");

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            out.println("</table>");
        %>
</b>
</div>

