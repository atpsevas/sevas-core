<%@page import="org.esme.dao.models.Users" %>
<%@page import="java.sql.Connection" %>
<%@page import="java.sql.PreparedStatement" %>
<%@page import="java.sql.ResultSet" %>
<%@ page import="java.util.ArrayList" %>
<%

    out.println("<table width='100%' class='fancy'>");
    out.println("<tr>");
    out.println("<th>Report Date</th>");
    out.println("<th>Total Active Sub</th>");
    out.println("<th>Main Sub</th>");
    out.println("<th>Main Charged</th>");
    out.println("<th>Fallback Sub</th>");
    out.println("<th>Fallback Charged</th>");
    out.println("<th>Repeat Sub</th>");
    out.println("<th>Unsub</th>");
    out.println("<th>Expired</th>");
    out.println("<th>Blacklisted</th>");
    out.println("</tr>");

    HttpSession sess = request.getSession(true);
    String user = (String) sess.getAttribute("userName");
    long userID = 0;
    String userRole = (String) sess.getAttribute("UserRole");
    ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
    for (Users us : usersCache) {
        if (us.getUserName().equals(user)) {
            userID = us.getId();
        }
    }
    Connection conn = null;
    try {

        Long serviceID = Long.parseLong(request.getParameter("service"));
        conn = (Connection) getServletContext().
                getAttribute("database.connection");
        PreparedStatement st = null;
        if (userRole.equals("Admin")) {
            st = conn.prepareStatement(
                    (String) getServletContext().getInitParameter("dailyServiceSubscription"));
        } else {
            st = conn.prepareStatement(
                    "select date(sub_request_time) as theDate, "
                            + "count(CASE WHEN (b.sub_status = 'active' "
                            + "and to_date(b.sub_expiry_date, 'YYYY-MM-DD') > date(CURRENT_TIMESTAMP)) THEN 1 ELSE 0 END) "
                            + "AS total_active_sub, sum(CASE WHEN (b.sub_status = 'active' and b.sub_shortcode = a.welcome_shortcode ) THEN 1 ELSE 0 END) "
                            + "AS new_main_sub, sum(((CASE WHEN b.sub_status = 'active' and "
                            + "b.sub_shortcode = a.welcome_shortcode and date(b.sub_request_time) = "
                            + "date(CURRENT_TIMESTAMP) THEN 1 ELSE 0 END)) * a.service_price) AS new_main_charged,"
                            + "sum(CASE WHEN b.sub_status = 'active' and b.sub_shortcode = "
                            + "a.fallback_shortcode THEN 1 ELSE 0 END) AS new_fallback_sub, "
                            + "sum((CASE WHEN b.sub_status = 'active' and b.sub_shortcode = "
                            + "a.fallback_shortcode THEN 1 ELSE 0 END)* a.fallback_price) AS new_fallback_charged, "
                            + "sum(CASE WHEN to_date(b.sub_expiry_date, 'YYYY-MM-DD') - b.sub_request_time::date >  "
                            + "a.service_period THEN 1 ELSE 0 END) AS repeat_sub, "
                            + "sum(CASE WHEN b.sub_status = 'inactive' THEN 1 ELSE 0 END) AS new_unsub, "
                            + "sum(CASE WHEN to_date(b.sub_expiry_date, 'YYYY-MM-DD') "
                            + "<= date(CURRENT_TIMESTAMP)  THEN 1 ELSE 0 END) AS new_expired, "
                            + "sum(CASE WHEN b.blacklisted = 'blacklisted'  THEN 1 ELSE 0 END) AS new_blacklisted "
                            + "FROM subscription_service a, subscription b "
                            + "where extract(month from sub_request_time) = extract (month from now()) "
                            + "and extract(year from sub_request_time) = extract (year from now()) "
                            + "and a.id = b.sub_service_id and a.service_owner = " + userID + " group by theDate order by theDate Desc");
        }

        st.setLong(1, serviceID);
        ResultSet rs = st.executeQuery();
        long totalActive = 0;
        while (rs.next()) {
            totalActive = +rs.getLong("total_active_sub");
            out.println("<tr>");
            out.println("<td>" + rs.getString("theDate") + "</td>");
            out.println("<td>" + totalActive + "</td>");
            out.println("<td>" + rs.getLong("new_main_sub") + "</td>");
            out.println("<td>" + rs.getLong("new_main_charged") + "</td>");
            out.println("<td>" + rs.getLong("new_fallback_sub") + "</td>");
            out.println("<td>" + rs.getLong("new_fallback_charged") + "</td>");
            out.println("<td>" + rs.getLong("repeat_sub") + "</td>");
            out.println("<td>" + rs.getLong("new_unsub") + "</td>");
            out.println("<td>" + rs.getLong("new_expired") + "</td>");
            out.println("<td>" + rs.getLong("new_blacklisted") + "</td>");
            out.println("</tr>");
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    out.println("</table>");
%>
