<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>:::..:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--[if IE]>
    <link href="css/ie-transparency.css" rel="stylesheet" type="text/css"/><![endif]-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="js/reportSupport.js" type="text/javascript"></script>
    <script>
        function filterReport(start, end, url, str) {
            var startDate = document.getElementById(start).value;
            var endDate = document.getElementById(end).value;
            if (str == "") {
                document.getElementById("txtHint").innerHTML = "No data retrieved";
                return;
            }

            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", url + "?start=" + startDate + "&end=" + endDate, true);
            xmlhttp.send();
        }
    </script>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>iReport</h2>
                    <c:choose>
                        <c:when test="${fn:contains(message, 'successfully')}">
                            <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:when>
                        <c:otherwise>
                            <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:otherwise>
                    </c:choose>
                    <div id="txtHint">
                        <div>
                            <form action="" method="post">
                                <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">Start
                                    Date:</label> <input name=sDate" type="text" id="datepicker"
                                                         style=" width: 100px; height: 30px; font-size: 16px;  font-family: Candara;"/>
                                <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">End
                                    Date:</label><input name="eDate" type="text" id="datepicker2"
                                                        style=" width: 100px; height: 30px; font-size: 16px;  font-family: Candara;"/>
                                <input style="background-color: #000; font-weight: bolder; height: 45px; color: #fff; "
                                       type="submit" value="APPLY" onclick="showUserActivities()"/>
                            </form>
                        </div>
                        <hr style="margin-top: 1px; "/>
                        <div style="margin-top :10px; ">
                            <table style="margin-top:1px" cellspacing="20" cellpadding="1">
                                <tr style="font-size: 14PX; font-family:Copperplate Gothic Light;">
                                    <td>
                                        N<c:out value="${param.inceptionTillDateRev}"/><br/>
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            INCEPTION TILL DATE REVENUE
                                        </label>
                                    </td>
                                    <td>
                                        N<c:out value="${param.todayRev}"/><br/>
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            TODAY'S REVENUE
                                        </label>
                                    </td>
                                    <td>
                                        N<c:out value="${param.dailyAverageRev}"/><br/>
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            AVE. DAILY REVENUE
                                        </label>
                                    </td>
                                    <td>
                                        N<c:out value="${param.extraPolatedRev}"/>
                                        <br/>
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            EXTRAPOLATED REV.
                                        </label>
                                    </td>
                                    <td>
                                        <c:out value="${param.dailyChurn}"/>%<br/>
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            DAILY CHURN
                                        </label>
                                    </td>
                                    <td>
                                        N<c:out value="${param.monthTargetRev}"/><br/>
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            TARGET REVENUE
                                        </label>
                                    </td>
                                    <td>
                                        N<c:out value="${param.actualRev}"/><br/>
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            ACTUAL REVENUE
                                        </label>
                                    </td>
                                    <td>
                                        <c:out value="${param.revenueVariance}"/>%<br/>
                                        <label style=" margin-top:1px; font-size: 10px;font-weight: bold; color: graytext;">
                                            REVENUE VARIANCE
                                        </label>
                                    </td>
                                </tr>
                            </table>
                            <hr style="margin-top: 1px; "/>
                            <div id="data-chart">

                            </div>
                            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                            <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                            <script type="text/javascript" src="js/visualization-chart-script.js"></script>
                        </div>

                    </div>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>iReport</h2>
                            <ul class="links">
                                <li><a href="#" onclick="showUser('new_revenue_target.jsp', this.value)">Set Monthly
                                    Target Revenue</a></li>
                                <li><a href="#" onclick="showUser('update_revenue_target.jsp', this.value)">Update
                                    Monthly Target Revenue</a></li>
                                <li><a href="revTarget.csv">Download Rev Target Template</a></li>
                                <li><a href="iReport.do">View Revenue Dashboard</a></li>
                                <li><a href="logs.jsp">Live Traffic</a></li>
                                <li><a href="#" onclick="showUser('report_rev_mgr.jsp', this.value)">Services Revenue
                                    Report</a></li>
                                <li><a href="#" onclick="showUser('report_mgr.jsp', this.value)">Daily Subscription
                                    Report</a></li>
                                <li><a href="#" onclick="showUser('sub_summary.jsp', this.value)">Subscription Report
                                    Summary</a></li>
                                <li><a href="./LogsCSVDownload.csv">Download Today's Traffic Logs</a></li>
                                <li><a href="#" onclick="showUser('daily_paid_content_report.jsp', this.value)">Daily
                                    Paid Content Report</a></li>
                                <li><a href="#" onclick="showUser('content_report_mgr.jsp', this.value)">Content
                                    Delivery Report</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
