<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">
<head>
    <title>:::...:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#fb1hide").click(function () {
                $("#fb1").hide(1000);
            });

            $("#fb1show").click(function () {
                $("#fb1").show(1000);
            });
        });
    </script>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <%
                        String msisdn = (String) getServletContext().getAttribute("msisdn");

                    %>
                    <h3>Do Not Disturb <%=msisdn%>
                    </h3>
                    <c:set var="message" value="${requestScope.message}"/>
                    <c:choose>
                        <c:when test="${fn:contains(message, 'succes')}">
                            <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:when>
                        <c:otherwise>
                            <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:otherwise>
                    </c:choose>
                    <!--End of Filter-->
                    <b>
                        <!--Filter-->
                        <p>
                            <label for="message">Single Blacklist: </label><br/>
                        <form action="./dnd" method="post">
                            <input type="text" id="msisdn" name="msisdn"
                                   style=" width: 200px; height: 30px; font-size: 16px;   font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;"
                                   value="<%=msisdn%>"/><br/>
                            <input style="background-color: #000;  font-weight: bolder; height: 35px; color: #fff; "
                                   type="submit" value="SUBMIT" onclick="showUserActivities()"/>
                        </form>
                        </p>
                    </b>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <ul class="links">
                                <li><a href="deactivate.jsp">Billing History</a></li>
                                <li><a href="sms_history.jsp">Messaging History</a></li>
                                <li><a href="dnd_bulk.jsp">Bulk Blacklist</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
