<%@page import="org.esme.dao.models.SMSCampaign" %>
<%@page import="org.esme.dao.models.Users" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="java.util.ArrayList" %>

<%@include file="partials/header.jsp" %>

<%

    HttpSession sess = request.getSession(true);
    String user = (String) sess.getAttribute("userName");
    String compaignOwner = null;
    long userID = 0;
    String userRole = (String) sess.getAttribute("UserRole");
    long campaignServiceID = Long.parseLong(request.getParameter("service"));
    String serviceName = null;
    ArrayList<SMSCampaign> campaign = (ArrayList<SMSCampaign>) getServletContext().getAttribute("smsCampaignCache");
    ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
    for (Users us : usersCache) {
        if (us.getUserName().equals(user)) {
            userID = us.getId();
            break;
        }
    }

    out.println("<form method='post' action='./deleteSMSCampaign.do'>");
    out.println("<input type='submit'  onclick = 'return confirmSubmit()' style=' margin-left: 773px; background-color: #099; font-weight: bolder; color: #fff; max-width: 100px;' value='Delete'/>");
    out.println("<table width='100%' class='fancy'>");
    out.println("<tr>");
    out.println("<td><input type=checkbox name='checkall' value='' onClick='toggle(this)'> </td>");
    out.println("<th>Date - Time Scheduled</th>");
    out.println("<th>Service</th>");
    out.println("<th>Sender ID</th>");
    out.println("<th style='width:143px;'>Message</th>");
    out.println("<th>Campaign Owner</th>");
    out.println("<th>Campaign Target</th>");
    out.println("<th>Campaign Sent</th>");
    out.println("<th>Set Date</th>");
    out.println("<th>Set Time</th>");
    out.println("<th>Campaign Start</th>");
    out.println("<th>Campaign End</th>");
    out.println("<th>Status</th>");
    out.println("</tr>");

    for (SMSCampaign con : campaign) {

        if (userRole.equalsIgnoreCase("Admin") && con.getCampaignServiceID() == campaignServiceID) {
            out.println("<tr>");
            out.println("<td><input type='checkbox' name='check' value=" + con.getCampaignID() + "> </td>");
            try {
                out.println("<td>" + con.getCampaignUploadDate().substring(0, 19) + "</td>");
            } catch (Exception e) {
                out.println("<td>Not Available</td>");
            }
            for (Service service : serviceCache) {
                if (service.getId() == campaignServiceID) {
                    serviceName = service.getServiceName();
                    break;
                }
            }
            out.println("<td>" + serviceName + "</td>");
            out.println("<td>" + con.getCampaignSenderID() + "</td>");
            out.println("<td style='width:143px;'>" + con.getCampaignContent() + "</td>");
            for (Users us : usersCache) {
                if (us.getId() == con.getCampaignOnwerID()) {
                    compaignOwner = us.getUserName();
                    break;
                }
            }
            out.println("<td>" + compaignOwner + "</td>");
            out.println("<td>" + con.getCampaignTotalTarget() + "</td>");
            out.println("<td>" + con.getCampaignTotalSent() + "</td>");
            out.println("<td>" + con.getCampaignSentDate().substring(0, 10) + "</td>");
            out.println("<td>" + con.getCampaignSentTime() + "</td>");
            try {
                out.println("<td>" + con.getCampaignActualStartTime().substring(0, 19) + "</td>");
            } catch (Exception e) {
                out.println("<td>Not Available</td>");
            }
            try {
                out.println("<td>" + con.getCampaignActualEndTime().substring(0, 19) + "</td>");
            } catch (Exception e) {
                out.println("<td>Not Available</td>");
            }
            out.println("<td>" + con.getCampaignStatus() + "</td>");
            out.println("</tr>");
        } else {
            if (con.getCampaignOnwerID() == userID && con.getCampaignServiceID() == campaignServiceID) {
                out.println("<tr>");
                out.println("<td><input type='checkbox' name='check' value=" + con.getCampaignID() + "> </td>");
                out.println("<td>" + con.getCampaignID() + "</td>");
                try {
                    out.println("<td>" + con.getCampaignUploadDate().substring(0, 19) + "</td>");
                } catch (Exception e) {
                    out.println("<td>Not Available</td>");
                }
                for (Service service : serviceCache) {
                    if (service.getId() == campaignServiceID) {
                        serviceName = service.getServiceName();
                        break;
                    }
                }
                out.println("<td>" + serviceName + "</td>");
                out.println("<td>" + con.getCampaignSenderID() + "</td>");
                out.println("<td style='width:143px;'>" + con.getCampaignContent() + "</td>");
                out.println("<td>" + user + "</td>");
                out.println("<td>" + con.getCampaignTotalTarget() + "</td>");
                out.println("<td>" + con.getCampaignTotalSent() + "</td>");
                out.println("<td>" + con.getCampaignSentDate() + "</td>");
                out.println("<td>" + con.getCampaignSentTime() + "</td>");
                try {
                    out.println("<td>" + con.getCampaignActualStartTime().substring(0, 19) + "</td>");
                } catch (Exception e) {
                    out.println("<td>Not Available</td>");
                }
                try {
                    out.println("<td>" + con.getCampaignActualEndTime().substring(0, 19) + "</td>");
                } catch (Exception e) {
                    out.println("<td>Not Available</td>");
                }
                out.println("<td>" + con.getCampaignStatus() + "</td>");
                out.println("</tr>");
            }
        }
    }
    out.println("</table>");
    out.println("</form>");
%>
