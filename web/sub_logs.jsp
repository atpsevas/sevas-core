<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">
<head>
    <title>:::...:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--FoR Date Picker -->
    <link href="css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.17.custom.js" type="text/javascript"></script>
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="js/dyninfo.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $(".datepickr").datepicker();
        });
    </script>
</head>

<body>
<%!
    public int nullIntconv(String str) {
        int conv = 0;
        if (str == null) {
            str = "0";
        } else if ((str.trim()).equals("null")) {
            str = "0";
        } else if (str.equals("")) {
            str = "0";
        }
        try {
            conv = Integer.parseInt(str);
        } catch (Exception e) {
        }
        return conv;
    }
%>

<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <%

                Connection conn = null;
                Class.forName("org.postgresql.Driver").newInstance();
                conn = DriverManager.getConnection("jdbc:postgresql://198.57.181.127:5432/sevas", "postgres", "fileopen");

                ResultSet rsPagination = null;
                ResultSet rsRowCnt = null;

                PreparedStatement psPagination = null;
                PreparedStatement psRowCnt = null;

                int iShowRows = 15;  // Number of records show on per page
                int iTotalSearchRecords = 10;  // Number of pages index shown

                int iTotalRows = nullIntconv(request.getParameter("iTotalRows"));
                int iTotalPages = nullIntconv(request.getParameter("iTotalPages"));
                int iPageNo = nullIntconv(request.getParameter("iPageNo"));
                int cPageNo = nullIntconv(request.getParameter("cPageNo"));

                int iStartResultNo = 0;
                int iEndResultNo = 0;

                if (iPageNo == 0) {
                    iPageNo = 0;
                } else {
                    iPageNo = Math.abs((iPageNo - 1) * iShowRows);
                }


                String sqlPagination = "SELECT sub_request_time, sub_msisdn,sub_request_text,"
                        + "sub_network,sub_shortcode,sub_status, sub_expiry_date "
                        + " from subscription order by sub_request_time desc offset " + iPageNo + " limit " + iShowRows + "";
                sevasLogger.info(sqlPagination);

                psPagination = conn.prepareStatement(sqlPagination);
                rsPagination = psPagination.executeQuery();

                //// this will count total number of rows
                String sqlRowCnt = "SELECT count(*) as cnt from subscription  ";
                psRowCnt = conn.prepareStatement(sqlRowCnt);
                rsRowCnt = psRowCnt.executeQuery();

                if (rsRowCnt.next()) {
                    iTotalRows = rsRowCnt.getInt("cnt");
                }
            %>
            <div>
                <div id="wrapper">
                    <h3>Filter and Download Mo Report</h3>
                    <c:set var="message" value="${requestScope.message}"/>
                    <c:choose>
                        <c:when test="${fn:contains(message, 'succes')}">
                            <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:when>
                        <c:otherwise>
                            <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:otherwise>
                    </c:choose>
                    <!--Filter-->
                    <form method="post" action="./mo_csv.do">
                        <app:filter_form/>
                    </form>
                    <!--End of Filter-->
                    <form name="frm" method="post">
                        <table>
                            <input type="hidden" name="iPageNo" value="<%=iPageNo%>"/>
                            <input type="hidden" name="cPageNo" value="<%=cPageNo%>"/>
                            <input type="hidden" name="iShowRows" value="<%=iShowRows%>"/>
                            <table width="100%" class="fancy">
                                <tr>
                                    <th>No.</th>
                                    <th>Request Time</th>
                                    <th>Network</th>
                                    <th>Short Code</th>
                                    <th>MSISDN</th>
                                    <th>Keyword</th>
                                    <th>Status</th>
                                    <th>Expiry Date</th>
                                </tr>

                                <%
                                    int index = 0;
                                    while (rsPagination.next()) {
                                        index++;
                                %>
                                <tr>
                                    <td><%=index%>
                                    </td>
                                    <td><%=rsPagination.getString("sub_request_time").substring(0, 19)%>
                                    </td>
                                    <td><%=rsPagination.getString("sub_network")%>
                                    </td>
                                    <td><%=rsPagination.getString("sub_shortcode")%>
                                    </td>
                                    <td><%=rsPagination.getString("sub_msisdn")%>
                                    </td>
                                    <td><%=rsPagination.getString("sub_request_text")%>
                                    </td>
                                    <td><%=rsPagination.getString("sub_status")%>
                                    </td>
                                    <td><%=rsPagination.getString("sub_expiry_date")%>
                                    </td>
                                </tr>

                                <%
                                    }
                                %>
                            </table>
                                <%
                                                //// calculate next record start record  and end record
                                                try {
                                                    if (iTotalRows < (iPageNo + iShowRows)) {
                                                        iEndResultNo = iTotalRows;
                                                    } else {
                                                        iEndResultNo = (iPageNo + iShowRows);
                                                    }

                                                    iStartResultNo = (iPageNo + 1);
                                                    iTotalPages = ((int) (Math.ceil((double) iTotalRows / iShowRows)));

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                    %>
                            <div>
                                        <span id="Pages">
                                            <%

                                                int i = 0;
                                                int cPage = 0;
                                                if (iTotalRows != 0) {
                                                    cPage = ((int) (Math.ceil((double) iEndResultNo / (iTotalSearchRecords * iShowRows))));

                                                    int prePageNo = (cPage * iTotalSearchRecords) - ((iTotalSearchRecords - 1) + iTotalSearchRecords);
                                                    if ((cPage * iTotalSearchRecords) - (iTotalSearchRecords) > 0) {
                                            %>
                                            <a href="sub_logs.jsp?iPageNo=<%=prePageNo%>&cPageNo=<%=prePageNo%>"
                                               style='font-size: 10px; background-color:#fff; color: #003366'>Prev</a>
                                            <%
                                                }

                                                for (i = ((cPage * iTotalSearchRecords) - (iTotalSearchRecords - 1)); i <= (cPage * iTotalSearchRecords); i++) {
                                                    if (i == ((iPageNo / iShowRows) + 1)) {
                                            %>
                                            <a href="sub_logs.jsp?iPageNo=<%=i%>"
                                               style='font-size: 10px; background-color:#fff; color: #003366'><b><%=i%></b></a>
                                            <%
                                            } else if (i <= iTotalPages) {
                                            %>
                                            <a href="sub_logs.jsp?iPageNo=<%=i%>"
                                               style='font-size: 10px; background-color:#fff; color: #003366'><%=i%></a>
                                            <%
                                                    }
                                                }
                                                if (iTotalPages > iTotalSearchRecords && i < iTotalPages) {
                                            %>
                                            <a href="sub_logs.jsp?iPageNo=<%=i%>&cPageNo=<%=i%>"
                                               style='font-size: 10px; background-color:#fff; color: #003366'>Next</a>
                                            <%
                                                    }
                                                }


                                            %>
                                            <i>Rows <%=iStartResultNo%> - <%=iEndResultNo%>   Total Result  <%=iTotalRows%> </i>
                                        </span>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
<%
    try {
        if (psPagination != null) {
            psPagination.close();
        }
        if (rsPagination != null) {
            rsPagination.close();
        }

        if (psRowCnt != null) {
            psRowCnt.close();
        }
        if (rsRowCnt != null) {
            rsRowCnt.close();
        }

        if (conn != null) {

        }
    } catch (Exception e) {
        e.printStackTrace();
    }
%>