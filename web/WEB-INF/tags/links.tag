<%-- 
    Document   : links
    Created on : 17-Dec-2011, 01:27:53
    Author     : TTMO
--%>

<%@tag description="put the tag description here" pageEncoding="windows-1252" %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="message" %>

<%-- any content can be specified here e.g.: --%>
<h2>Welcome, <c:out value="${sessionScope.userName}"/></h2>
<ul class="links">
    <li><a href="#">Services</a></li>
    <li><a href="#">Content</a></li>
    <li><a href="#">Logs</a></li>
    <li><a href="./logout">[Logout]</a></li>
</ul>