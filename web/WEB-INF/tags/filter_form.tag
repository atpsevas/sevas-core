<%-- 
    Document   : filter_form
    Created on : Jun 3, 2012, 1:01:00 PM
    Author     : salbert
--%>

<jsp:useBean id="portalDAOOG" class="org.esme.dao.models.PortalDAO" scope="request"/>
<%@tag description="put the tag description here" pageEncoding="windows-1252" %>
<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="message" %>

<table>
    <tr>
        <td>
            <label for="subject">Network: </label><br/>
            <select name="network"
                    style='font-size: 1em;
                    color: #333;
                    border: 1px solid #DDD;
                    background-color: #EEE;
                    width: 120px;
                    font-family: Tahoma, Arial, Helvetica;
                    height: 34px'>
                <option value="airtel">Airtel</option>
                <!--<option value="glo">Glo</option>
                <option value="mtn">MTN</option>-->
                <option value="etisalat">Etisalat</option>
            </select>
        </td>
        <td>
            <label for="subject">Short Code: </label><br/>
            <select name="sc" size="1"
                    style='font-size: 1em;
                    color: #333;
                    border: 1px solid #DDD;
                    background-color: #EEE;
                    width: 80px;
                    font-family: Tahoma, Arial, Helvetica;
                    height: 34px'>
                <%
                    java.util.ArrayList services = null;
                    services = portalDAOOG.uniqueServices();
                    for (int idx = 0; idx < portalDAOOG.uniqueShortCodes().size(); idx++) {
                %>
                <option value="<%=portalDAOOG.uniqueShortCodes().get(idx).toString().trim()%>">
                    <%=portalDAOOG.uniqueShortCodes().get(idx).toString().trim()%>
                </option>
                <%}%>
            </select>

        </td>
        <td>
            <p><label for="subject">Service:</label>
                <select name="serv">
                    <option value="all" selected="selected">Select Service</option>
                    <%
                        java.util.Iterator iters = services.iterator();
                        while (iters.hasNext()) {
                            String serv = iters.next().toString().replace(" ", "%20").trim();
                    %>
                    <option value="<%=serv%>"><%=serv.replace("%20", " ")%>
                    </option>
                    <%}%>
                </select>
        </td>
        <td>
            <label for="subject">Start Date: </label><br/>
            <input id="start" class="datepickr" name="start" value=""
                   style='font-size: 1em;
                   color: #333;
                   border: 1px solid #DDD;
                   background-color: #EEE;
                   width: 120px;
                   font-family: Tahoma, Arial, Helvetica;
                   height: 34px'/>
        <td/>
        <td>
            <label for="subject">End Date: </label><br/>
            <input id="end" class="datepickr" name="end" value=""
                   style='font-size: 1em;
                   color: #333;
                   border: 1px solid #DDD;
                   background-color: #EEE;
                   width: 120px;
                   font-family: Tahoma, Arial, Helvetica;
                   height: 34px'/>
        </td>
        <td>
            <label for="subject">Report Type </label><br/>
            <select name="reportType"
                    style='font-size: 1em;
                    color: #333;
                    border: 1px solid #DDD;
                    background-color: #EEE;
                    width: 120px;
                    font-family: Tahoma, Arial, Helvetica;
                    height: 34px'>
                <option>Report Type</option>
                <option value="count">Hits</option>
                <option value="logs">Logs</option>
            </select>
        </td>
        <td>
            <label for="subject">Download: </label><br/>
            <input type="submit" value="Download" class="btn"/>
        </td>
    </tr>
</table>