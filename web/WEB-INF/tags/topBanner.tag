<%-- 
    Document   : topBanner
    Created on : Feb 9, 2013, 5:47:41 PM
    Author     : salbert
--%>

<%@tag description="put the tag description here" pageEncoding="windows-1252" %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="message" %>

<%-- any content can be specified here e.g.: --%>
<div id="slider-wrapper">
    <div id="slider" class="nivoSlider">
        <img src="img/services.jpg" alt=""/>
        <img src="img/content2.jpg" alt=""/>
        <img src="img/report.jpg" alt=""/>
    </div>
</div>