<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="org.esme.xmlreader.SMSC" %>
<%@ page import="org.esme.xmlreader.SMSCStats" %>
<%@ page import="java.util.Iterator" %>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">
<head>
    <title>:::..::::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <meta http-equiv="refresh" content="300"/>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li class="on"><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>SMSCs Real-time Statistics</h2>
                    <hr/>
                    <label style="font-weight: bold; color: green; font-size: 14px;"><%= new SMSCStats().getSMSGatewayStatus()%>
                    </label>&nbsp;
                    <div id="ref">
                        <table width="100%" class="fancy">
                            <tr>
                                <th>ID</th>
                                <th>STATUS</th>
                                <th>RECEIVED</th>
                                <th>SENT</th>
                                <th>DLR</th>
                                <th>FAILED</th>
                                <th>QUEUE</th>
                                <th>MANAGE</th>
                            </tr>
                            <%
                                Iterator iter = new SMSCStats().readSMSCXML().iterator();
                                while (iter.hasNext()) {
                                    SMSC nextSMSC = (SMSC) iter.next();
                            %>
                            <tr style="font-weight: bolder; font-size: larger; font-variant:small-caps">
                                <td><%=nextSMSC.getId()%>
                                </td>

                                <%
                                    if (nextSMSC.getStatus().toUpperCase().contains("ONLINE")) {
                                %>
                                <td style="background-color: green; color: white;">
                                    <%=nextSMSC.getStatus()%>
                                </td>
                                <%} else {%>
                                <td style="background-color: red; color: white;">
                                    <%=nextSMSC.getStatus()%>
                                </td>
                                <%}%>
                                <td><%=nextSMSC.getReceived()%>
                                </td>
                                <td><%=nextSMSC.getSent()%>
                                </td>
                                <td><%=nextSMSC.getSentSMSDLR()%>
                                </td>
                                <td><%=nextSMSC.getFailed()%>
                                </td>
                                <td><%=nextSMSC.getQueued()%>
                                </td>
                                <td style="font-weight: bold;">
                                    <a href="./kanneladmin.do?command=start-smsc&smsc=<%=nextSMSC.getId()%>">Start</a>
                                    <a href="./kanneladmin.do?command=stop-smsc&smsc=<%=nextSMSC.getId()%>">Shut
                                        Down</a>
                                </td>
                            </tr>
                            <%}%>
                        </table>
                    </div>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <c:set var="message" value="${requestScope.message}"/>
                            <label style="color: green; font-weight: bold;">
                                <c:out value="${message}"/>
                            </label>
                            <ul class="links">
                                <li><a href="./kanneladmin.do?command=suspend">Suspend Gateway</a></li>
                                <li><a href="./kanneladmin.do?command=resume">Resume Gateway</a></li>
                                <li><a href="./kanneladmin.do?command=shutdown">Shut Down Gateway</a></li>
                                <li><a href="./kanneladmin.do?command=restart">Restart Gateway</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
        <app:footer/>
    </div>
</div>
</body>
</html>
