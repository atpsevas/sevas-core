<%@page import="org.esme.dao.models.ContentLogs" %>
<%@page import="org.esme.dao.models.Users" %>
<%@page import="java.sql.Connection" %>
<%@page import="java.sql.PreparedStatement" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.SQLException" %>
<%@page import="java.util.ArrayList" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@include file="partials/header.jsp" %>

<h3>Daily Content Delivery Report</h3>
<b>
    <div id="showRep">
            <%
            
            HttpSession sess = request.getSession(true);
            String user = (String) sess.getAttribute("userName");
            long userID = 0;
            String userRole = (String) sess.getAttribute("UserRole");
            ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
            for (Users us : usersCache) {
                if (us.getUserName().equals(user)) {
                    userID = us.getId();
                }
            }
            
            
            Connection conn = null;
            try {
                
                conn = (Connection) getServletContext().
                        getAttribute("database.connection");
                ArrayList<ContentLogs> logs = (ArrayList<ContentLogs>) 
                        getServletContext().getAttribute("monthTillDateContentLogs");
                
                long totalSent = 0;
                long totalSubmitted = 0;
                long totalDelivered = 0;
                long totalFailed = 0;
                long totalBuffered = 0;
                long totalRejected = 0;
                long totalNoDLR = 0;
                String ReportDate = "";
                
                String nextService = "";
                
                if (userRole.equalsIgnoreCase("Admin")) {
                    for (ContentLogs log : logs) {
                        if (nextService.equalsIgnoreCase(log.getService().trim())) {
                        } else {
                            out.println("<table width='100%' class='fancy'>");
                            out.println("<tr colspan='9'>" + log.getService().toUpperCase() + "</tr>");
                            out.println("<tr>");
                            out.println("<th>Delivery Date</th>");
                            out.println("<th>Total Sent</th>");
                            out.println("<th>Submitted</th>");
                            out.println("<th>Delivered</th>");
                            out.println("<th>Failed</th>");
                            out.println("<th>Bufferred</th>");
                            out.println("<th>Rejected</th>");
                            out.println("<th>Unknow Status</th>");
                            out.println("</tr>");
                        }
                        
                        ReportDate = log.getTheDate();
                        out.println("<tr>");
                        out.println("<td>" + ReportDate + "</td>");
                        out.println("<td>" + log.getTotalSent() + "</td>");
                        totalSent += log.getTotalSent();
                        out.println("<td>" + log.getSubmitted() + "</td>");
                        totalSubmitted += log.getSubmitted();
                        out.println("<td>" + log.getDelivered() + "</td>");
                        totalDelivered += log.getDelivered();
                        out.println("<td>" + log.getFailed() + "</td>");
                        totalFailed += log.getFailed();
                        out.println("<td>" + log.getBuffered() + "</td>");
                        totalBuffered += log.getBuffered();
                        out.println("<td>" + log.getRejected() + "</td>");
                        totalRejected += log.getRejected();
                        out.println("<td>" + log.getUnKnownStatus() + "</td>");
                        totalNoDLR += log.getUnKnownStatus();
                        out.println("</tr>");
                        nextService = log.getService().trim();
                    }
                } else {
                    try {
                        
                        String sqlPagination = " select a.service_name as theService, "
                                + "date(b.sent_time) as theDate,count(b.recipient) AS total_sent,"
                                + "sum((CASE WHEN (b.delivery_status =  'SUBMITTED') THEN 1 ELSE 0 END)) AS submitted,"
                                + "sum((CASE WHEN (b.delivery_status =  'DELIVERED') THEN 1 ELSE 0 END)) AS delivered,"
                                + "sum((CASE WHEN (b.delivery_status =  'FAILED') THEN 1 ELSE 0 END)) AS failed,"
                                + " sum((CASE WHEN (b.delivery_status =  'BUFFERED') THEN 1 ELSE 0 END)) AS buffered,"
                                + "sum((CASE WHEN (b.delivery_status =  'REJECTED') THEN 1 ELSE 0 END)) AS rejected,"
                                + " sum((CASE WHEN (b.delivery_status =  'NO DLR') THEN 1 ELSE 0 END)) AS no_dlr "
                                + "FROM subscription_service a, delivered_outgoing_messages b "
                                + "where extract(month from b.sent_time) = extract (month from now()) "
                                + "and extract(year from b.sent_time) = extract (year from now()) "
                                + "and (b.src_module = 'DailyAlert' or b.src_module = 'ContentUpload' "
                                + "or b.src_module = 'InstantContent' or b.src_module = 'DailyPaidContent') "
                                + "and a.service_name = b.serv_name and a.service_owner = " + userID + " "
                                + "group by theService,theDate order by theService,theDate Desc";
                        
                        PreparedStatement psPagination = conn.prepareStatement(sqlPagination);
                        ResultSet rs = psPagination.executeQuery();
                        
                        while (rs.next()) {
                            if (nextService.equalsIgnoreCase(rs.getString("theService"))) {
                            } else {
                                out.println("<table width='100%' class='fancy'>");
                                out.println("<tr colspan='9'>" + rs.getString("theService").toUpperCase() + "</tr>");
                                out.println("<tr>");
                                out.println("<th>Delivery Date</th>");
                                out.println("<th>Total Sent</th>");
                                out.println("<th>Submitted</th>");
                                out.println("<th>Delivered</th>");
                                out.println("<th>Failed</th>");
                                out.println("<th>Bufferred</th>");
                                out.println("<th>Rejected</th>");
                                out.println("<th>Unknow Status</th>");
                                out.println("</tr>");
                            }
                            ReportDate = rs.getString("theDate") ;
                            out.println("<tr>");
                            out.println("<td>" + rs.getString("theDate") + "</td>");
                            out.println("<td>" + rs.getString("total_sent") + "</td>");
                            totalSent += rs.getLong("total_sent");
                            out.println("<td>" + rs.getString("submitted") + "</td>");
                            totalSubmitted += rs.getLong("submitted");
                            out.println("<td>" + rs.getString("delivered") + "</td>");
                            totalDelivered += rs.getLong("delivered");
                            out.println("<td>" + rs.getString("failed") + "</td>");
                            totalFailed += rs.getLong("failed");
                            out.println("<td>" + rs.getString("buffered") + "</td>");
                            totalBuffered += rs.getLong("buffered");
                            out.println("<td>" + rs.getString("rejected") + "</td>");
                            totalRejected += rs.getLong("rejected");
                            out.println("<td>" + rs.getString("no_dlr") + "</td>");
                            totalNoDLR += rs.getLong("no_dlr");;
                            out.println("</tr>");
                            nextService = rs.getString("theService").trim();
                        }
                        
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
                
                out.println("</table>");
                out.println("<u><b> Content Report Summary</b></u>");
                out.println("<table width='100%' class='fancy'>");
                out.println("<tr>");
                out.println("<th>DELIVERY AS AT</th>");
                out.println("<th>Total Sent</th>");
                out.println("<th>Submitted</th>");
                out.println("<th>Delivered</th>");
                out.println("<th>Failed</th>");
                out.println("<th>Bufferred</th>");
                out.println("<th>Rejected</th>");
                out.println("<th>Unknown Status</th>");
                out.println("</tr>");
                out.println("<tr style='font-weight:bold;'>");
                out.println("<td>" + ReportDate + "</td>");
                out.println("<td>" + totalSent + "</td>");
                out.println("<td>" + totalSubmitted + "</td>");
                out.println("<td>" + totalDelivered + "</td>");
                out.println("<td>" + totalFailed + "</td>");
                out.println("<td>" + totalBuffered + "</td>");
                out.println("<td>" + totalRejected + "</td>");
                out.println("<td>" + totalNoDLR + "</td>");
                out.println("</tr>");
                out.println("</table>");
                
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.println("</table>");
        %>
</b>
</div>

