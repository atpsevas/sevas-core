<%@page import="java.sql.Connection" %>
<%@page import="java.sql.PreparedStatement" %>
<%@page import="java.sql.ResultSet" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">
<head>
    <title>:::...:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--FoR Date Picker -->
    <link href="css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.17.custom.js" type="text/javascript"></script>
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="js/contentSupport.js" type="text/javascript"></script>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li class="on"><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'mtn'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'etisalat'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'glo'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'airtel'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h3>Search MSISDN</h3>
                    <c:set var="message" value="${requestScope.message}"/>
                    <c:choose>
                        <c:when test="${fn:contains(message, 'succes')}">
                            <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:when>
                        <c:otherwise>
                            <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                        </c:otherwise>
                    </c:choose>
                    <!--Filter-->
                    <form action="" method="post">
                        <input type="text" id="blackie"
                               style="width: 200px; height: 30px; font-size: 16px;  font-family: Candara, Trebuchet, Verdana, Arial, Helvetica, sans-serif; letter-spacing: -1px;"/>
                        <input type="submit" class="btn" value="Search"/>
                    </form>

                    <%
                        String subscriber = null;
                        try {
                            subscriber = request.getParameter("msisdn");
                        } catch (Exception e) {
                        }
                        Connection conn = null;
                        try {
                            if (subscriber != null) {
                                int index = 0;
                                out.println("<table width='100%' class='fancy'>");
                                out.println("<tr>");
                                out.println("<th>ID</th>");
                                out.println("<th>Service Name</th>");
                                out.println("<th>MO Short Code</th>");
                                out.println("<th>MT Short Code</th>");
                                out.println("<th>Billing Short Code</th>");
                                out.println("<th>Date Subscribed</th>");
                                out.println("<th>Expiry Date</th>");
                                out.println("<th>Status</th>");
                                out.println("<th>Manage</th>");
                                out.println("</tr>");
                                String msisdn = subscriber.substring(subscriber.length() - 9);
                                conn = (Connection) getServletContext().
                                        getAttribute("database.connection");
                                PreparedStatement st = conn.prepareStatement(
                                        "select a.service_name, a.service_shortcode, "
                                                + "a.service_mt_shortcode, a.welcome_shortcode, "
                                                + "b.id, b.sub_request_time, b.sub_expiry_date,"
                                                + "b.sub_status from subscription_service a, subscription b "
                                                + "where b.sub_msisdn ilike '%" + msisdn + "' and a.id = b.sub_service_id");
                                ResultSet rs = st.executeQuery();

                                while (rs.next()) {
                                    index++;
                                    out.println("<tr>");
                                    out.println("<td>" + index + "</td>");
                                    out.println("<td>" + rs.getString("service_name") + "</td>");
                                    out.println("<td>" + rs.getString("service_shortcode") + "</td>");
                                    out.println("<td>" + rs.getString("service_mt_shortcode") + "</td>");
                                    out.println("<td>" + rs.getString("welcome_shortcode") + "</td>");
                                    out.println("<td>" + rs.getString("sub_request_time") + "</td>");
                                    out.println("<td>" + rs.getString("sub_expiry_date") + "</td>");
                                    out.println("<td>" + rs.getString("sub_status") + "</td>");
                                    out.println("<td><a href='doSingleBlacklist.do?id=" + rs.getString("id") + " '>Blacklist</a></td>");
                                    out.println("</tr>");
                                }
                                out.println("</table>");
                            }
                        } catch (Exception e) {
                        }
                    %>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <ul class="links">
                                <li><a href="blacklist_mgr.jsp">Blacklist MSISDN</a></li>
                                <li><a href="manage_blacklist.jsp">Manage Blacklisted MSISDN</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
