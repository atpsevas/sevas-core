<%-- 
    Document   : footer
    Created on : 14-Dec-2011, 00:37:52
    Author     : TTMO
--%>

<%@tag description="put the tag description here" pageEncoding="windows-1252" %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="message" %>

<%-- any content can be specified here e.g.: --%>
<div id="footer">
    <p class="left">&copy;<br/><a style="color: #fff" href="http://www.advancedtechnologypark.com">Developed by:
        Advanced Technology Park</a></p>
</div>