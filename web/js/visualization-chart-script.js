var TUTORIAL_SAVVY = {
    /*return google visualization data*/
    getvisualizationData: function (jsonData) {

        var point1, point2, point3, dataArray = [],
            data = new google.visualization.DataTable();

        data.addColumn('string', 'Name');

        data.addColumn('number', 'Main Revenue');

        data.addColumn({
            type: 'string',
            role: 'tooltip',
            'p': {
                'html': true
            }
        });

        data.addColumn('number', 'Fallback Revenue');

        data.addColumn({
            type: 'string',
            role: 'tooltip',
            'p': {
                'html': true
            }
        });

        data.addColumn('number', 'Total Revenue');

        data.addColumn({
            type: 'string',
            role: 'tooltip',
            'p': {
                'html': true
            }
        });

        /* for loop code for changing inputdata to 'data' of type google.visualization.DataTable*/
        $.each(jsonData, function (i, obj) {

            point1 = "Main Charged Rev : " + obj.mainCharged + "";

            point2 = "Fallback Charged Rev: " + obj.fallBackCharged + "";

            point3 = "Total Revenue: " + obj.totalRevenue + "";

            dataArray.push([obj.theMonth,
                obj.mainCharged, TUTORIAL_SAVVY.returnTooltip(point1, point2, point3),
                obj.fallBackCharged, TUTORIAL_SAVVY.returnTooltip(point1, point2, point3),
                obj.totalRevenue, TUTORIAL_SAVVY.returnTooltip(point1, point2, point3)]);
        });

        data.addRows(dataArray);

        return data;
    },
    /*return options for bar chart: these options are for various configuration of chart*/


    getOptionForBarchart: function () {

        var options = {
            animation: {
                duration: 2000,
                easing: 'out'
            },
            legend: {
                position: 'right',
                //alignment: 'end',
            },
            textStyle: {
                color: '#b3b8bc',
                fontName: 'roboto',
                fontSize: 12
            },
            tooltip: {
                isHtml: true,
                showColorCode: true,
                isStacked: true
            },
            hAxis: {
                baselineColor: '#ccc'
            },
            vAxis: {
                baselineColor: '#ccc',
                gridlineColor: '#fff'
            },
            title: 'Monthly Main, Fallback, and Total Revenue for the year',
            curveType: 'function',
            pointSize: 10,
            isStacked: true,
            height: 350
        };
        return options;
    },
    /*Draws a Bar chart*/
    drawBarChart: function (inputdata) {
        var barOptions = TUTORIAL_SAVVY.getOptionForBarchart(),
            data = TUTORIAL_SAVVY.getvisualizationData(inputdata),
            chart = new google.visualization.BarChart(document.getElementById('data-chart'));
        chart.draw(data, barOptions);
        /*for redrawing the bar chart on window resize*/
        $(window).resize(function () {
            chart.draw(data, barOptions);
        });
    },
    /* Returns a custom HTML tooltip for Visualization chart*/
    returnTooltip: function (dataPoint1, dataPoint2, dataPoint3) {
        return "<div style='height:30px;width:150px;font:12px,roboto;padding:15px 5px 5px 5px;border-radius:3px;'>" +
            "<span style='color:#68130E;font:12px,roboto;padding-right:20px;'>" + dataPoint1 + "</span>" +
            "<span style='color:#c65533;font:12px,roboto;padding-right:20px;'>" + dataPoint2 + "</span>\n\
                <span style='color:#c66633;font:12px,roboto;'>" + dataPoint3 + "</span></div>";
    },

    /*Makes ajax call to servlet and download data 
     * Spexweb "http://vas-etisalat.nuobjects.com:8585/home/sevas/ServiceRevenueChart.do
     * */

    getStudentData: function () {
        var url = $(location).attr('host');
        $.ajax({
            url: "http://" + url + "/sevas/ServiceRevenueChart.do",
            dataType: "JSON",
            success: function (data) {
                TUTORIAL_SAVVY.drawBarChart(data);
            }
        });
    }
};

google.load("visualization", "1", {
    packages: ["corechart"]
});

$(document).ready(function () {

    TUTORIAL_SAVVY.getStudentData();
});