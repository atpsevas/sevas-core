function showUser(str) {
    if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    }

    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET", "manage_content.jsp?service=" + str, true);
    xmlhttp.send();
}

function showSMSCampaign(str) {
    if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    }

    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET", "manage_sms_campaign.jsp?service=" + str, true);
    xmlhttp.send();
}

function confirmSubmit() {
    var agree = confirm("Do you really want to delete this content?");
    if (agree)
        return true;
    else
        return false;
}

function toggle(source) {
    checkboxes = document.getElementsByName('check');
    for (var i in checkboxes)
        checkboxes[i].checked = source.checked;
}

function toggleAdd(sel_box, tagToShow) {
    var state = document.getElementById(sel_box);
    var status = state.value;
    if (status == 'yes') {
        document.getElementById(tagToShow).style.display = 'block';
    } else {
        document.getElementById(tagToShow).style.display = 'none';
    }
}

function limitText(limitField, limitCount, limitNum) {
    if (limitField.value.length > limitNum) {
        limitField.value = limitField.value.substring(0, limitNum);
    } else {
        limitCount.value = limitNum - limitField.value.length;
    }
}
