jQuery(document).ready(function () {
    //ShowSubmitButton();
    // when any option from make list is selected
    jQuery("select[name='sc']").change(function () {
        // get the selected option value of make
        var optionValue = $("select[name='sc']").val();
        jQuery.ajax({
            type: "POST",
            url: "loadservice.do",
            data: ({
                sc: optionValue,
                status: 1
            }),
            cache: false,
            async: false,
            beforeSend: function () {
                jQuery("#ajaxLoader").show();
            },
            complete: function () {
                jQuery("#ajaxLoader").hide();
            },
            success: function (html) {
                $("#models").empty();
                $("#models").append(html);
                $("#models").fadeIn("fast");
                //Calling other functions here to make them available
                //loadYear();
                //loadServiceComponents ();
                //ShowImage();
            }
        });
    });

});

function loadYear() {
    // when any option from model list is selected
    $("select#model").change(function () {
        // get the selected option value of model
        var optionValue = jQuery("select#model").val();
        jQuery.ajax({
            type: "POST",
            url: "caryear.do",
            data: ({
                model: optionValue,
                status: 1
            }),
            beforeSend: function () {
                jQuery("#ajaxLoader").show();
            },
            complete: function () {
                jQuery("#ajaxLoader").hide();
            },
            success: function (response) {
                jQuery("#years").html(response);
                jQuery("#years").show();
            }
        });
    });
}


function loadServiceComponents() {
    // when any option from model list is selected
    $("select#serv_repair").change(function () {
        // get the selected option value of model
        var optionValue = jQuery("select#serv_repair").val();
        var make = jQuery("select#fm-state").val();
        var model = jQuery("select#model").val();
        var year = jQuery("select#year").val();
        jQuery.ajax({
            type: "POST",
            url: "component.do",
            data: ({
                servid: optionValue,
                make: make,
                model: model,
                year: year,
                status: 1
            }),
            beforeSend: function () {
                jQuery("#ajaxLoader").show();
            },
            complete: function () {
                jQuery("#ajaxLoader").hide();
            },
            success: function (response) {
                jQuery("#components").html(response);
                jQuery("#components").show();
            }
        });
    });
}


function ShowSubmitButton() {
    //Hide it
    $('#submit').hide();
    // alert('ShowSubmitButton');
    $(function () {
        $('#serv_component').click(function () {
            //Show it
            $('#submit').show();
        });
    });
}

function ShowImage() {
    $('#fm-state').change(function () {
        //alert('Called');
        if ($(this).val() == 'HONDA') {
            $('#toyotaimage').hide();
            $('#defaultimage').hide();
            $('#hondaimage').show();
        } else if ($(this).val() == 'TOYOTA') {
            $('#hondaimage').hide();
            $('#defaultimage').hide();
            $('#toyotaimage').show();
        } else {
            $('#toyotaimage').hide();
            $('#hondaimage').hide();
            $('#defaultimage').show();
        }
    });
}