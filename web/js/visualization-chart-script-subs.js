var TUTORIAL_SAVVY = {
    /*return google visualization data*/
    getvisualizationData: function (jsonData) {

        var point1, point2, point3, point4, dataArray = [],
            data = new google.visualization.DataTable();

        data.addColumn('string', 'Name');

        data.addColumn('number', 'New Subs');

        data.addColumn({
            type: 'string',
            role: 'tooltip',
            'p': {
                'html': true
            }
        });

        data.addColumn('number', 'New Active');

        data.addColumn({
            type: 'string',
            role: 'tooltip',
            'p': {
                'html': true
            }
        });

        data.addColumn('number', 'New InActive');

        data.addColumn({
            type: 'string',
            role: 'tooltip',
            'p': {
                'html': true
            }
        });

        data.addColumn('number', 'New Expired');

        data.addColumn({
            type: 'string',
            role: 'tooltip',
            'p': {
                'html': true
            }
        });

        /* for loop code for changing inputdata to 'data' of type google.visualization.DataTable*/
        $.each(jsonData, function (i, obj) {

            point1 = "New Subscription : " + obj.newSub + "";

            point2 = "New Active Subscription: " + obj.newActiveSub, +"";

            point3 = "New InActive Subscription: " + obj.newInActiveSub + "";

            point4 = "New Expired Subscription: " + obj.newExpired + "";

            dataArray.push([obj.subDate,
                obj.newSub, TUTORIAL_SAVVY.returnTooltip(point1, point2, point3, point4),
                obj.newActiveSub, TUTORIAL_SAVVY.returnTooltip(point1, point2, point3, point4),
                obj.newInActiveSub, TUTORIAL_SAVVY.returnTooltip(point1, point2, point3, point4),
                obj.newExpired, TUTORIAL_SAVVY.returnTooltip(point1, point2, point3, point4)]);
        });

        data.addRows(dataArray);

        return data;
    },
    /*return options for bar chart: these options are for various configuration of chart*/


    getOptionForBarchart: function () {

        var options = {
            animation: {
                duration: 2000,
                easing: 'out'
            },
            legend: {
                position: 'right',
                //alignment: 'end',
            },
            textStyle: {
                color: '#b3b8bc',
                fontName: 'roboto',
                fontSize: 12
            },
            tooltip: {
                isHtml: true,
                showColorCode: true,
                isStacked: true
            },
            hAxis: {
                baselineColor: '#ccc'
            },
            vAxis: {
                baselineColor: '#ccc',
                gridlineColor: '#fff'
            },
            title: 'New Subs, New Active Sub, New Inactive Sub and New Expired',
            curveType: 'function',
            pointSize: 10,
            isStacked: true,
            height: 350
        };
        return options;
    },
    /*Draws a Bar chart*/
    drawBarChart: function (inputdata) {
        var barOptions = TUTORIAL_SAVVY.getOptionForBarchart(),
            data = TUTORIAL_SAVVY.getvisualizationData(inputdata),
            chart = new google.visualization.LineChart(document.getElementById('data-chart'));
        chart.draw(data, barOptions);
        /*for redrawing the bar chart on window resize*/
        $(window).resize(function () {
            chart.draw(data, barOptions);
        });
    },
    /* Returns a custom HTML tooltip for Visualization chart*/
    returnTooltip: function (dataPoint1, dataPoint2, dataPoint3, dataPoint4) {
        return "<div style='height:30px;width:150px;font:12px,roboto;padding:15px 5px 5px 5px;border-radius:3px;'>" +
            "<span style='color:#68130E;font:12px,roboto;padding-right:15;'>" + dataPoint1 + "</span>" +
            "<span style='color:#c65533;font:12px,roboto;padding-right:15;'>" + dataPoint2 + "</span>\n\
                <span style='color:#c66633;font:12px,roboto;padding-right:15;'>" + dataPoint3 + "</span>\n\
                <span style='color:#c66633;font:12px,roboto;'>" + dataPoint4 + "</span></div>";
    },
    getDailySubsriptionReport: function () {
        var url = $(location).attr('host');
        $.ajax({
            url: "http://" + url + "/sevas/SubscriptionReportChart.do",
            dataType: "JSON",
            success: function (data) {
                TUTORIAL_SAVVY.drawBarChart(data);
            }
        });
    }
};

google.load("visualization", "1", {
    packages: ["corechart"]
});

$(document).ready(function () {

    TUTORIAL_SAVVY.getDailySubsriptionReport();
});