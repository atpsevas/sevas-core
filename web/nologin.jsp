<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>:::..:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--[if IE]>
    <link href="css/ie-transparency.css" rel="stylesheet" type="text/css"/><![endif]-->
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <li class="first on"><a href="start.jsp">Home</a></li>
            <li><a href="new_account.jsp">New Account</a></li>
            <li><a href="new_network.jsp">Network</a></li>
            <li><a href="shortcode.jsp">Short Code</a></li>
            <li><a href="services.jsp">Services</a></li>
            <li><a href="sms_campaign.jsp">Campaign</a></li>
            <li><a href="contents.jsp">Content</a></li>
            <li><a href="iReport.do">iReport</a></li>
            <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
            <li><a href="gateway.jsp">SMSC</a></li>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <!-- <h2>Home</h2>
                     <p>
                         Secured Enterprise VAS platform is a ready-to-launch mobile revenue generating platform,
                         which allows Mobile operators and content provider to easily design,
                         upload and distribute various contents for mobile subscribers.
                     </p>
                      <p>Operators and content providers can launch mobile value added services with minimal or NO technical/marketing resources.</p>
                     <p>The Secured Enterprise VAS platform is a very stable platform with high uptime, built on proven web and mobile technologies.
                    -->
                </div>
                <div id="sub">
                    <h2>Login</h2>
                    <app:login_form/>
                    <p style="color: red;">Access denied. Please log in with appropriate username and password.</p>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
