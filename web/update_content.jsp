<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="app" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>:::..:::</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <!--FoR Date Picker -->
    <script type="text/javascript" src="datePicker/htmlDatePicker.js" type="text/javascript"></script>
    <link href="datePicker/htmlDatePicker.css" rel="stylesheet"/>
    <script type="text/javascript">
        function limitText(limitField, limitCount, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
</head>
<body>
<ul class="hide">
    <li><a href="#body">Skip to content</a></li>
</ul>
<div id="container">
    <app:header/>
    <div id="body">
        <ul id="nav">
            <c:choose>
                <c:when test="${sessionScope.UserRole ==  'Admin'}">
                    <li class="first on"><a href="start.jsp">Home</a></li>
                    <li><a href="new_account.jsp">New Account</a></li>
                    <li><a href="new_network.jsp">Network</a></li>
                    <li><a href="shortcode.jsp">Short Code</a></li>
                    <li><a href="services.jsp">Services</a></li>
                    <li><a href="sms_campaign.jsp">Campaign</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                    <li><a href="gateway.jsp">SMSC</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Blacklister'}">
                    <li><a href="start.jsp">Home</a></li>
                    <li><a href="blacklist_msisdn.jsp">Blacklist</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'User'}">
                    <li><a href="start.jsp">Home</a></li>
                    <li><a href="contents.jsp">Content</a></li>
                    <li><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Logviewer'}">
                    <li><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="iReport.do">iReport</a></li>
                </c:when>
                <c:when test="${sessionScope.UserRole ==  'Content'}">
                    <li><a href="start.jsp">Home</a></li>
                    <li class="on"><a href="contents.jsp">Content</a></li>
                </c:when>
                <c:otherwise>
                    <li class="first"><a href="start.jsp">Home</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div id="content">
            <div>
                <div id="main">
                    <h2>Update Content</h2>
                    <p>Edit content below.</p>
                    <form method="post" action="./updatecontent.do?action=update">
                        <fieldset>
                            <legend>New Content Update</legend>
                            <c:set var="message" value="${requestScope.message}"/>
                            <c:choose>
                                <c:when test="${fn:contains(message, 'successfully')}">
                                    <label style="color: green; font-weight: bold;"><c:out value="${message}"/></label>
                                </c:when>
                                <c:otherwise>
                                    <label style="color: red; font-weight: bold;"><c:out value="${message}"/></label>
                                </c:otherwise>
                            </c:choose>
                            <p>
                                <label for="subject">Service:</label>
                                <input type="text" name="id" value="${param.id}" hidden/>
                                <input type="text" name="cs" value="${param.cs}" hidden/>
                                <input type="text" name="" value="${param.servName}" readonly/>
                            </p>
                            <p>
                                <label>Date to Blast:</label>
                                <input value="${param.dob}"
                                       style="width: 25%;" type="text" name="date" id="date"
                                       readonly onClick="GetDate(this);"/>
                            </p>
                            <p>
                                <label for="message">Content: </label>
                                <textarea style="text-align: left;" name="con" id="message" cols="40" rows="10"
                                          onKeyDown="limitText(this.form.con, this.form.countdown, 160);"
                                          onKeyUp="limitText(this.form.con, this.form.countdown, 160);"><c:out
                                        value="${param.con}"/></textarea>
                            </p>
                            <p>
                                You have <input style="width:3%; background: #fff; border: 0px; border-color: #fff"
                                                readonly type="text" name="countdown" size="3" value="160"/>Characters
                                remaining<br/>
                                <input style="background-color: #099; font-weight: bolder; color: #fff; max-width: 100px;"
                                       type="submit" value="UPLOAD"/>
                            </p>
                        </fieldset>
                    </form>
                </div>
                <div id="sub">
                    <c:choose>
                        <c:when test="${sessionScope.userName !=  null}">
                            <h2>Features</h2>
                            <ul class="links">
                                <li><a href="new_content.jsp">Upload New Content</a></li>
                                <li><a href="new_bulk_content.jsp">Upload Bulk Content</a></li>
                                <li><a href="manage_content.jsp">Manage Existing Content</a></li>
                                <li><a href="./logout">[Logout]</a></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <h2>Login</h2>
                            <app:login_form/>
                        </c:otherwise>
                    </c:choose>
                    <app:help/>
                </div>
            </div>
        </div>
    </div>
    <app:footer/>
</div>
</body>
</html>
