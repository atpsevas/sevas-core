/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.webservices.etisalat;

/**
 * @author saheedalbert
 * <p>
 * <p>
 * String rabbitPayLoad = "{\n" + "msisdn: '"+subscriber+"'," +
 * "billingTransactionID: '"+subscriber.substring(1)+service.getServiceID()+"',"
 * + "serviceShortCode: '"+serviceName+"'," + "serviceId:
 * '"+service.getServiceID()+"'," + "serviceName: '"+serviceName+"'," +
 * "billingDescription: ''," + "billingEventType: ''," + "servicePrice:
 * '"+service.getServicePrice()+"00'," + "serviceKeyword:
 * '"+service.getServiceKeyword()+"'," + "servicePeriod:
 * '"+service.getServicePeriod()+"'," + "callback_url: '"+encodedCallBackURL+"'"
 * + "}";
 */
public class RabbitPayLoadJsonData {

    private String msisdn;
    private String billingTransactionID;
    private String serviceShortCode;
    private String serviceName;
    private long serviceId;
    private String billingDescription;
    private String billingEventType;
    private String servicePrice;
    private String serviceKeyword;
    private String servicePeriod;
    private String callback_url;
    private boolean isOnUAT;


    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getBillingTransactionID() {
        return billingTransactionID;
    }

    public void setBillingTransactionID(String billingTransactionID) {
        this.billingTransactionID = billingTransactionID;
    }

    public String getServiceShortCode() {
        return serviceShortCode;
    }

    public void setServiceShortCode(String serviceShortCode) {
        this.serviceShortCode = serviceShortCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public long getServiceId() {
        return serviceId;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }

    public String getBillingDescription() {
        return billingDescription;
    }

    public void setBillingDescription(String billingDescription) {
        this.billingDescription = billingDescription;
    }

    public String getBillingEventType() {
        return billingEventType;
    }

    public void setBillingEventType(String billingEventType) {
        this.billingEventType = billingEventType;
    }

    public String getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(String servicePrice) {
        this.servicePrice = servicePrice + "00";
    }

    public String getServiceKeyword() {
        return serviceKeyword;
    }

    public void setServiceKeyword(String serviceKeyword) {
        this.serviceKeyword = serviceKeyword;
    }

    public String getServicePeriod() {
        return servicePeriod;
    }

    public void setServicePeriod(String servicePeriod) {
        this.servicePeriod = servicePeriod;
    }

    public String getCallback_url() {
        return callback_url;
    }

    public void setCallback_url(String callback_url) {
        this.callback_url = callback_url;
    }

    public boolean isIsOnUAT() {
        return isOnUAT;
    }

    public void setIsOnUAT(boolean isOnUAT) {
        this.isOnUAT = isOnUAT;
    }

}
