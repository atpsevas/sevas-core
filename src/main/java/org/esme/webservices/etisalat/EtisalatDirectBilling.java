package org.esme.webservices.etisalat;

import com.google.gson.Gson;
import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.RabbitMQPublisher;
import org.esme.dto.Service;
import org.esme.utils.parser.StringParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeoutException;

/**
 * @author saheedalbert
 */
public class EtisalatDirectBilling implements org.esme.utils.gateway.GatewayInterface {

    static Properties prop;

    static EtisalatDirectBilling etisalatDirectBilling;

    //Long.toString(data.length())
    @SuppressWarnings({"CallToPrintStackTrace", "deprecation"})
    public static synchronized EtisalatDirectBilling getInstance() {

        try {

            if (etisalatDirectBilling == null) {
                etisalatDirectBilling = new EtisalatDirectBilling();
            }
            prop = AppBroker.getPropertyFileHandle();
            //service = serv;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return etisalatDirectBilling;
    }

    @Override
    public long getQueueSize(String queue) {
        return 0;
    }

    public boolean isQueueEmpty() {

        String etisalatDirectBillingQueueURL = prop.getProperty("etisalatDBQueueURL");
        long queue = 0;
        URL website;

        try {
            website = new URL(etisalatDirectBillingQueueURL);
            URLConnection connection = website.openConnection();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            connection.getInputStream()));

            StringBuilder response = new StringBuilder();
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            try {
                queue = Long.parseLong(response.toString().trim());
            } catch (Exception e) {
                e.printStackTrace();
            }

            in.close();

        } catch (Exception urlEx) {
            urlEx.printStackTrace();
        }

        org.esme.broker.AppBroker.sevasLogger.info("EDB  queue :" + queue);
        if (queue < 1) {
            return true;
        }

        return false;
    }

    @SuppressWarnings({"deprecation", "CallToPrintStackTrace"})
    public void makeEtisalatDirectBillingBillingRequest(
            Service service, String billingSrcModule, String billingCode,
            String billingEventType, String billingContent,
            String subscriber) {

        etisalatDirectBilling = getInstance();

        //BufferedReader in = null;
        String serviceName = billingCode;

        RabbitMQPublisher publisher;
        String QUEUE_NAME, FAST_QUEUE_NAME;
        RabbitPayLoadJsonData rabbitPayLoadJsonData;
        Gson gson;

        try {

            long billingTransactionID = generateMessageID();
            String servicePrice = service.getServicePrice() + "00";
            String theContent = billingContent;

            if (theContent.equalsIgnoreCase("")) {
                theContent = "No Content";
            }

            String callBackURL = prop.getProperty("etisalatDBResponseCallbackURL");

            String callBackURLParam = "serviceID=" + String.valueOf(service.getServiceID())
                    + "&subscriber=" + subscriber + "&billingCode=" + billingCode
                    + "&billingSrcModule=" + billingSrcModule + "&transactionID="
                    + prop.getProperty("vendorServiceID").substring(0, 5).toUpperCase() + billingTransactionID
                    + "&amount=" + servicePrice + "&content=" + theContent + "&transactionResponseCode=%d";

            String encodedCallBackURL = callBackURL + URLEncoder.encode(callBackURLParam, "UTF-8");

            if (billingCode.isEmpty() || billingCode.equalsIgnoreCase("nocode")) {
                serviceName = service.getServiceName();
            }

            org.esme.broker.AppBroker.sevasLogger.info(service.getServiceName());
            org.esme.broker.AppBroker.sevasLogger.info(service.getServiceShortCode());

            List<String> uatServicesID = new StringParser().customParser(prop.getProperty("on_uat"), ",");
            rabbitPayLoadJsonData = new RabbitPayLoadJsonData();
            rabbitPayLoadJsonData.setMsisdn(subscriber);
            rabbitPayLoadJsonData.setBillingTransactionID(subscriber.substring(1) + service.getServiceID());
            rabbitPayLoadJsonData.setServiceShortCode(service.getServiceShortCode());
            rabbitPayLoadJsonData.setServiceName(serviceName);
            rabbitPayLoadJsonData.setServiceId(service.getServiceID());
            rabbitPayLoadJsonData.setBillingDescription("");
            rabbitPayLoadJsonData.setBillingEventType(billingEventType);
            rabbitPayLoadJsonData.setServicePrice(service.getServicePrice());
            rabbitPayLoadJsonData.setServiceKeyword(service.getServiceKeyword());
            rabbitPayLoadJsonData.setServicePeriod(service.getServicePeriod());
            rabbitPayLoadJsonData.setCallback_url(encodedCallBackURL);

            if (uatServicesID.contains(String.valueOf(service.getServiceID()))) {
                rabbitPayLoadJsonData.setIsOnUAT(true);
            } else {
                rabbitPayLoadJsonData.setIsOnUAT(false);
            }

            //Convert to JSON...
            gson = new Gson();
            String rabbitPayLoad = gson.toJson(rabbitPayLoadJsonData);

            org.esme.broker.AppBroker.sevasLogger.info(rabbitPayLoad);

            //  Initialize Etisalat Direct Billing Rabbit Queue Name
            QUEUE_NAME = "edb_queue";
            FAST_QUEUE_NAME = "fast_edb";

            try {
                FAST_QUEUE_NAME = prop.getProperty("etisalatDBFastQueueName");
                QUEUE_NAME = prop.getProperty("etisalatDBQueueName");
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            publisher = RabbitMQPublisher.getInstance();

            try {
                if (rabbitPayLoadJsonData.isIsOnUAT()) {
                    org.esme.broker.AppBroker.sevasLogger.info("FAST EDB QUEUE - " + FAST_QUEUE_NAME);
                    publisher.publishDLRToRabbit(rabbitPayLoad, FAST_QUEUE_NAME);
                } else {
                    org.esme.broker.AppBroker.sevasLogger.info("EDB QUEUE - " + QUEUE_NAME);
                    publisher.publishDLRToRabbit(rabbitPayLoad, QUEUE_NAME);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (IOException | TimeoutException ex) {
            ex.printStackTrace();
        }
    }

    public long generateMessageID() {
        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(900000);
        return n;
    }

    @Override
    public boolean isQueueEmpty(String queue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
