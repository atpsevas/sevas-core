package org.esme.webservices.airtel;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.esme.broker.AppBroker;
import org.esme.dto.Service;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.PreparedStatement;
import java.util.Properties;
import java.util.Random;

/**
 * @author saheedalbert
 */
public class AirtelUCIP {

    static Properties prop;
    //static Service service;
    //static Connection dbConnection;
    static String subscriber;
    static String billingDescription;
    static String billingSrcModule;
    //Billing thread number...
    //private static final int MAXTHREAD = 5;
    private static AirtelUCIP airtelIBMSOAPFilterWebservice;
    URL url;
    OutputStreamWriter out;
    //Databse connection...
    PreparedStatement preparedStatement = null;
    DocumentBuilderFactory dbFactory;
    DocumentBuilder dBuilder;
    Document doc;
    HttpClient client;
    HttpState state;
    // Set credentials on the client
    Credentials credentials;
    // Prepare HTTP post
    PostMethod method;
    // Request content will be retrieved directly
    // from the input stream
    RequestEntity entity;
    String transactionResponseCode = "3";
    String billingTransactionID;
    String chargingRequest;

    //Long.toString(data.length())
    @SuppressWarnings({"CallToPrintStackTrace", "deprecation"})
    public static synchronized AirtelUCIP getInstance() {

        prop = AppBroker.getPropertyFileHandle();

        try {

            if (airtelIBMSOAPFilterWebservice == null) {
                airtelIBMSOAPFilterWebservice = new AirtelUCIP();
            }

            //service = serv;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return airtelIBMSOAPFilterWebservice;
    }


    @SuppressWarnings({"deprecation", "CallToPrintStackTrace"})
    public void makeAirtelChargingRequest(
            Service service, String billingSrcModule, String billingCode,
            String billingEventType, String billingContent,
            String subscriber) {

        BufferedReader in = null;

        try {

            long transactionID = generateMessageID();
            String servicePrice = service.getServicePrice();
            Random random = new Random();
            //billingTransactionID = random.nextInt(6) + "_" + random.nextInt(6);
            String theContent = billingContent;

            if (theContent.equalsIgnoreCase("")) {
                theContent = "No Content";
            }

            //String callBackURL = "http://localhost:8585/sevas/AirtelIMBUCIPResponseCallBack?";
            String callBackURL = prop.getProperty("airtelUCIPResponseCallbackURL");
            String callBackURLParam = "serviceID=" + String.valueOf(service.getServiceID())
                    + "&subscriber=" + subscriber + "&billingCode=" + billingCode
                    + "&billingSrcModule=" + billingSrcModule + "&transactionID=" + transactionID
                    + "&amount=" + servicePrice + "&content=" + theContent + "&transactionResponseCode=%d";

            String encodedCallBackURL = callBackURL + URLEncoder.encode(callBackURLParam, "UTF-8");
            //org.esme.broker.AppBroker.sevasLogger.info("CALLBACK URL " + callBackURL);
            org.esme.broker.AppBroker.sevasLogger.info("ENCODED CALLBACK URL " + encodedCallBackURL);

            String billingURLParams = "msisdn=" + subscriber + "&serviceId=" + String.valueOf(service.getServiceID()) + "&serviceName=" + service.getServiceName() + "&billingDescription=" + billingContent + "&billingEventType=" + billingEventType + "&servicePrice=" + servicePrice + "&serviceShortCode=" + service.getServiceShortCode() + "&serviceKeyword=" + service.getServiceKeyword() + "&transactionID=" + transactionID + "&servicePeriod=" + service.getServicePeriod() + "&cb_url=" + encodedCallBackURL;
            //String theURL = "http://localhost/~xumeapp/airtel_ucip_http_client.php?";

            String theURL = prop.getProperty("airtelUCIPBillingWrapperURL");

            String encodedBillingURL = theURL + billingURLParams.replace(" ", "+");
            //URLEncoder.encode(billingURLParams, "UTF-8");
            org.esme.broker.AppBroker.sevasLogger.error("URL " + encodedBillingURL);

            URL billingURL = new URL(encodedBillingURL);

            URLConnection yc = billingURL.openConnection();
            in = new BufferedReader(new InputStreamReader(
                    yc.getInputStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                org.esme.broker.AppBroker.sevasLogger.info(inputLine);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
            }
        }

    }

    //The difference, measured in seconds, between the
    //current time and midnight, January 1, 1970 UTC. 
    public long getTimeDiff() {
        long timeSeconds = System.currentTimeMillis() / 1000;
        return timeSeconds;
    }

    public long generateMessageID() {
        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(900000);
        return n;
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace"})
    public String encodeParams(String param) {
        String encParam = "";
        try {
            encParam = URLEncoder.encode(param, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return encParam;
    }

}
