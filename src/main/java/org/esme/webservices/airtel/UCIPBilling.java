package org.esme.webservices.airtel;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.esme.utils.pingenerator.RandomPasswordGenerator;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author salbert
 */
public class UCIPBilling {

    private static UCIPBilling ucipBilling;

    public static synchronized UCIPBilling getInstance() {
        try {
            if (ucipBilling == null) {
                ucipBilling = new UCIPBilling();
            }
        } catch (Exception ex) {
        }
        return ucipBilling;
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public String initiateBilling(String msisdn, String amount) throws URISyntaxException,
            org.apache.commons.httpclient.HttpException, IOException {

        byte[] responseBody;
        List<NameValuePair> sendSMSParams = new ArrayList<NameValuePair>();
        sendSMSParams.add(new BasicNameValuePair("msisdn", msisdn));
        sendSMSParams.add(new BasicNameValuePair("amount", amount));
        sendSMSParams.add(new BasicNameValuePair("promoid", "qmobile"));
        sendSMSParams.add(new BasicNameValuePair("ucip_auth", "cW1vYmlsZTpxbW9iaWxl"));
        URI uri = URIUtils.createURI("http", "localhost", 8090, "/ucip/charge.php",
                URLEncodedUtils.format(sendSMSParams, "UTF-8"), null);

        // Create an instance of HttpClient.
        HttpClient client = new HttpClient();

        // Create a method instance and replace all possible poison.
        GetMethod method = new GetMethod(uri.toString());

        // Provide custom retry handler is necessary
        method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                new DefaultHttpMethodRetryHandler(3, false));

        // Execute the method.
        int statusCode = client.executeMethod(method);
        if (statusCode != HttpStatus.SC_OK) {
            org.esme.broker.AppBroker.sevasLogger.error("Method failed: " + method.getStatusLine());
        }
        responseBody = method.getResponseBody();
        // Deal with the response.
        // Use caution: ensure correct character encoding and is not binary data
        String theRes = new String(responseBody);
        String ucipReturnedCode = "000";
        //Work on the UCIP return XML to reteive the actual response...
        try {
            DocumentBuilder builderFactory = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(theRes.trim()));
            Document xmlDocument = builderFactory.parse(src);
            XPath xPath = XPathFactory.newInstance().newXPath();
            String expression = "/methodResponse/params/param/value/struct/member/value/i4";
            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
            for (int i = 0; i < nodeList.getLength(); i++) {
                ucipReturnedCode = nodeList.item(i).getFirstChild().getNodeValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //org.esme.broker.AppBroker.sevasLogger.info(theRes.trim());
        return ucipReturnedCode;
    }

    public void logFailedRequestToDLRDB(String serviceType,
                                        String send, String rec,
                                        String message, String serviceName,
                                        String srcMod, String smsc, String serviceID, String dlr, String network) {
        try {

            List<NameValuePair> sendSMSParams = new ArrayList<NameValuePair>();
            sendSMSParams.add(new BasicNameValuePair("sub_type", serviceType));
            sendSMSParams.add(new BasicNameValuePair("msg_id", generateMessageID()));
            sendSMSParams.add(new BasicNameValuePair("sc", send));
            sendSMSParams.add(new BasicNameValuePair("msisdn", rec));
            sendSMSParams.add(new BasicNameValuePair("msg", message));
            sendSMSParams.add(new BasicNameValuePair("serv_name", serviceName));
            sendSMSParams.add(new BasicNameValuePair("serv_id", serviceID));
            sendSMSParams.add(new BasicNameValuePair("src_module", srcMod));
            sendSMSParams.add(new BasicNameValuePair("dlr", dlr));
            sendSMSParams.add(new BasicNameValuePair("smsc", smsc));
            URI uri = URIUtils.createURI("http", "localhost", 80, "/qm/dlr",
                    URLEncodedUtils.format(sendSMSParams, "UTF-8"), null);

            // Create an instance of HttpClient.
            HttpClient client = new HttpClient();

            // Create a method instance and replace all possible poison.
            GetMethod method = new GetMethod(uri.toString());

            // Provide custom retry handler is necessary
            method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                    new DefaultHttpMethodRetryHandler(3, false));
            client.executeMethod(method);
            //If calling the URL throws any exception...We will do manual DB insert...
        } catch (Exception ex) {
            try {
                Connection conn = DriverManager.getConnection("");

                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery("select count(*) as theCount from outgoing_messages");
                long nextID = 0;
                while (rs.next()) {
                    nextID = rs.getLong("theCount") + 1;
                }
                PreparedStatement pstmt = conn.prepareStatement(
                        "insert into outgoing_messages"
                                + "(message_id,sender,recipient,"
                                + "network,message,delivery_status,serv_name,"
                                + "src_module,smsc,serv_id,service_sub_type,id) "
                                + "values(?,?,?,?,?,?,?,?,?,?,?,?)");
                pstmt.setString(1, generateMessageID());
                pstmt.setString(2, send);
                pstmt.setString(3, rec);
                pstmt.setString(4, network);
                pstmt.setString(5, message);
                pstmt.setString(6, dlr);
                pstmt.setString(7, serviceName);
                pstmt.setString(8, srcMod);
                pstmt.setString(9, smsc);
                pstmt.setLong(10, Long.parseLong(serviceID));
                pstmt.setString(11, serviceType);
                pstmt.setLong(12, nextID);
                pstmt.executeUpdate();

            } catch (SQLException ex1) {
            }
        }

    }

    public String generateMessageID() {
        return RandomPasswordGenerator.randomPasswordGenerator();
    }
//    @SuppressWarnings("CallToThreadDumpStack")
//    public static void main(String arg[]) {
//        try {
//            org.esme.broker.AppBroker.sevasLogger.info(new UCIPBilling().initiateBilling("263118", "1"));
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
