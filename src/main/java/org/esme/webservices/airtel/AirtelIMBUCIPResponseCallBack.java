package org.esme.webservices.airtel;

import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.HelperBroker;
import org.esme.controllers.helpers.LogMTBillingResponse;
import org.esme.controllers.helpers.UpdateSubscription;
import org.esme.dto.Service;
import org.esme.gateway.kannel.WEB2SMS;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

@SuppressWarnings("serial")
public class AirtelIMBUCIPResponseCallBack extends AppBroker {

    long serviceID;
    String billingCode;
    String billingModule;
    long amount;
    String subscriber;
    String content;
    long transactionResponseCode = 5;
    String instantContent;
    long transactionID;
    String transactionResponseCodeString;

    LogMTBillingResponse logMTBillingResponse;

    PreparedStatement ps;

    boolean isSubscriberBilled = false;
    String params;

    HelperBroker hb;

    ArrayList<Service> serviceCache;
    WEB2SMS webSMS;

    Properties prop;

    @SuppressWarnings({"unchecked", "UseSpecificCatch", "CallToPrintStackTrace"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String transactionDescription = "FAILED";

        Service appBillingService = null;
        serviceID = Long.parseLong(removePoison(request.getParameter("serviceID")));
        transactionID = Long.parseLong(removePoison(request.getParameter("transactionID")));
        billingCode = removePoison(request.getParameter("billingCode"));
        billingModule = removePoison(request.getParameter("billingSrcModule"));
        content = removePoison(request.getParameter("content"));
        amount = Long.parseLong(removePoison(request.getParameter("amount")));
        subscriber = removePoison(request.getParameter("subscriber"));
        transactionResponseCode = Long.parseLong(removePoison(request.getParameter("transactionResponseCode")));

        org.esme.broker.AppBroker.sevasLogger.info(serviceID);
        org.esme.broker.AppBroker.sevasLogger.info(transactionID);
        org.esme.broker.AppBroker.sevasLogger.info(billingCode);
        org.esme.broker.AppBroker.sevasLogger.info(billingModule);
        org.esme.broker.AppBroker.sevasLogger.info(content);
        org.esme.broker.AppBroker.sevasLogger.info(amount);
        org.esme.broker.AppBroker.sevasLogger.info(subscriber);
        org.esme.broker.AppBroker.sevasLogger.info(transactionResponseCode);

        hb = new HelperBroker();

        try {
            serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
        } catch (Exception e) {
        }

        if (serviceCache == null || serviceCache.size() < 1) {
            updateServicesCache();
            serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
        }

        //Log the transaction...
        try {
            for (Service theService : serviceCache) {

                if (theService.getServiceID() == serviceID) {

                    appBillingService = theService;

                    if (transactionResponseCode == 0) {

                        transactionDescription = "SUCCESS";
                        transactionResponseCodeString = "0";
                        isSubscriberBilled = true;

                        org.esme.broker.AppBroker.sevasLogger.error("Response " + transactionDescription);

                        //Instant content charging...
                        if (billingModule.trim().equalsIgnoreCase("DailyPaidContent")) {
                            try {
                                //Send instant content.
                                org.esme.broker.AppBroker.sevasLogger.error("Sending content to  " + subscriber);
                                sendInstantContent(theService);
                            } catch (Exception E) {
                                E.printStackTrace();
                            }

                        } else {

                            if (billingModule.trim().equalsIgnoreCase("SubscriptionRenewal")
                                    || billingModule.trim().contains("SubscriptionRenewalRetry")
                                    || billingModule.trim().equalsIgnoreCase("NewSubscription")) {

                                try {
                                    org.esme.broker.AppBroker.sevasLogger.info("Logging transactions status...");
                                    customBillingLogCondition(appBillingService, subscriber,
                                            content, billingModule, isSubscriberBilled);
                                } catch (Exception E) {
                                    E.printStackTrace();
                                }

                                //Send billing notification...
                                try {
                                    sendBillingNotification(theService, subscriber, billingModule);
                                } catch (Exception E) {
                                    E.printStackTrace();
                                }
                                //update subscription...
                                try {
                                    //Send to help class for process...
                                    new UpdateSubscription().
                                            updateUserSubscription(getDBConnection(),
                                                    theService, theService.getServiceShortCode(), subscriber,
                                                    billingModule, "success");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                //If new subscription...Let him get content....
                                if (billingModule.trim().equalsIgnoreCase("NewSubscription")) {
                                    if (getCurrentHour() >= Long.parseLong(theService.getTimeOfBlast())) {
                                        sendFirstMTContent(theService, subscriber);
                                    }
                                }
                            }
                        }
                    } else {
                        transactionDescription = "FAILED";
                        transactionResponseCodeString = "1";
                        isSubscriberBilled = false;
                        //Log Failed..
                        new UpdateSubscription().
                                updateUserSubscription(getDBConnection(),
                                        theService, theService.getServiceShortCode(), subscriber,
                                        billingModule, "failed");

                    }
                    break;
                }
            }

            org.esme.broker.AppBroker.sevasLogger.info("Transaction status..." + transactionDescription);
            org.esme.broker.AppBroker.sevasLogger.info("Logging billing transaction...");

            chargingTransactionUCIPLog(appBillingService,
                    removePoison(request.getParameter("transactionID")),
                    subscriber, String.valueOf(amount),
                    "", billingCode, "",
                    "", "", transactionResponseCodeString,
                    transactionDescription,
                    billingModule, getDBConnection());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    //Log AIRTEL billing transaction...
    @SuppressWarnings({"UseSpecificCatch", "CallToPrintStackTrace"})
    public void chargingTransactionUCIPLog(Service service,
                                           String transactionID,
                                           String msisdn, String amount,
                                           String token, String vendorCode, String vendorUserName,
                                           String vendorPassword, String hashKey, String billingResponsCode,
                                           String billingResponseDecription,
                                           String billingSourceModule, Connection dbConnection) {

        try {

            PreparedStatement preparedStatement = dbConnection.prepareStatement(
                    "insert into airtel_ucip_billing_logs "
                            + "(billing_service_id,billing_transaction_id,"
                            + "billing_msisdn,billing_amount,billing_token,"
                            + "billing_vendor_code,billing_vendor_username,"
                            + "billing_vendor_password,"
                            + "billing_response_code, "
                            + "billing_response_description,src_module) "
                            + "values(?,?,?,?,?,?,?,?,?,?,?)");

            preparedStatement.setLong(1, service.getServiceID());
            preparedStatement.setString(2, transactionID);
            preparedStatement.setString(3, msisdn);
            preparedStatement.setLong(4, Long.parseLong(amount));
            preparedStatement.setString(5, token);
            preparedStatement.setString(6, vendorCode);
            preparedStatement.setString(7, vendorUserName);
            preparedStatement.setString(8, vendorPassword);
            preparedStatement.setString(9, billingResponsCode);
            preparedStatement.setString(10, billingResponseDecription);
            preparedStatement.setString(11, billingSourceModule);
            preparedStatement.executeUpdate();

        } catch (Exception E) {
            E.printStackTrace();
        }
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public void sendBillingNotification(Service service, String msisdn, String srcModule) {
        webSMS = WEB2SMS.getInstance();

        String message;

        //SubscriptionRenewal
        if (srcModule.equalsIgnoreCase("SubscriptionRenewal") || srcModule.equalsIgnoreCase("NewSubscription")) {
            message = service.getServiceRenewalMessage();
        } else {
            message = service.getServiceFallbackRenewalMessage();
        }

        try {
//
//            long epochTime = System.currentTimeMillis() / 1000;
//            long pid = 64;
//
//            ps = postgresDBCconnection.prepareStatement(
//                    "INSERT INTO send_sms (momt, sender, receiver, "
//                    + "msgdata, smsc_id, sms_type, boxc_id,"
//                    + "dlr_mask,dlr_url,time,pid) "
//                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?)");
//
//            org.esme.broker.AppBroker.sevasLogger.error("Seding..." + message + " to " + msisdn);
//            params = encodeParams(service.getServiceBillingNetwork())
//                    + "&sub_type=" + encodeParams(service.getServiceSubType())
//                    + "&msg_id=" + encodeParams(generateMessageID())
//                    + "&sc=" + encodeParams(service.getServiceShortCode())
//                    + "&msisdn=" + encodeParams(msisdn)
//                    + "&msg=" + encodeParams(formatText(message))
//                    + "&serv_id=" + service.getServiceID()
//                    + "&serv_name=" + encodeParams(service.getServiceName())
//                    + "&src_module=" + encodeParams(srcModule)
//                    + "&dlr_mask=" + encodeParams(DLR_MASK) + "&dlr=%d";
//
//            ps.setString(1, service.getServiceSubType());
//            ps.setString(2, service.getServiceShortCode());
//            ps.setString(3, msisdn);
//            ps.setString(4, formatText(message));
//            ps.setString(5, service.getServiceNetwork());
//            ps.setLong(6, 2);
//            ps.setString(7, "thesmsbox");
//            ps.setLong(8, Long.parseLong(DLR_MASK));
//            ps.setString(9, DLRURL + params);
//            ps.setLong(10, epochTime + 3600);
//            ps.setLong(11, pid);
//            ps.executeUpdate();

            webSMS.SMSBOX(
                    service.getServiceSubType(),
                    service.getServiceShortCode(),
                    msisdn, formatText(message),
                    service.getServiceName(),
                    service.getServiceID(), srcModule, service.getServiceNetwork(), DLR_MASK);

            org.esme.broker.AppBroker.sevasLogger.info(service.getServiceShortCode() + " "
                    + msisdn + " " + formatText(message) + " "
                    + service.getServiceNetwork());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public void sendInstantContent(Service theService) {

        prop = AppBroker.getPropertyFileHandle();
        try {

            try {

                DLRURL = prop.getProperty("dlr_url");
                DLR_MASK = prop.getProperty("dlrMask");

            } catch (Exception e) {
                org.esme.broker.AppBroker.sevasLogger.error("ERROR: Could not load app property file.");
                e.printStackTrace();
            }
            long epochTime = System.currentTimeMillis() / 1000;
            //long pid = 64;

            ps = getDBConnection().prepareStatement(
                    "INSERT INTO send_sms (momt, sender, receiver, "
                            + "msgdata, smsc_id, sms_type, boxc_id,"
                            + "dlr_mask,dlr_url,time) "
                            + "VALUES (?,?,?,?,?,?,?,?,?,?)");

            org.esme.broker.AppBroker.sevasLogger.error("Seding..." + content + " to " + subscriber);
            params = encodeParams(theService.getServiceBillingNetwork())
                    + "&sub_type=" + encodeParams(theService.getServiceSubType())
                    + "&msg_id=" + encodeParams(generateMessageID())
                    + "&sc=" + encodeParams(theService.getServiceMTShortCode())
                    + "&msisdn=" + encodeParams(subscriber)
                    + "&msg=" + encodeParams(formatText(content))
                    + "&serv_id=" + theService.getServiceID()
                    + "&serv_name=" + encodeParams(theService.getServiceName())
                    + "&src_module=" + encodeParams(billingModule)
                    + "&dlr_mask=31&dlr=%d";

            ps.setString(1, "MT");
            ps.setString(2, theService.getServiceMTShortCode());
            ps.setString(3, subscriber);
            ps.setString(4, formatText(content));
            ps.setString(5, theService.getServiceNetwork());
            ps.setLong(6, 2);
            ps.setString(7, prop.getProperty("contentSMSBoxID"));
            ps.setLong(8, 31);
            ps.setString(9, DLRURL + params);
            ps.setLong(10, epochTime + 3600);
            ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    //Log every request.....
    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace"})
    public int subscribeUser(long id, long servID, String subMSISDN, long subServicePeriod,
                             String reqText, String resText, String subNetwork,
                             long subServiceOwner, String tob, String iob,
                             String subShortCode, String status, String subExpiryDate,
                             String subContentDeliveryMethod, String subscriptionSource, String lastBilledCode) {
        int update = 0;

        try {

            ps = getDBConnection().prepareStatement(
                    "insert into subscription("
                            + "id,sub_service_id,sub_msisdn,sub_service_period,sub_request_text,"
                            + "sub_response_text,sub_network,sub_service_owner_id,"
                            + "sub_tob,sub_iob,sub_shortcode,sub_status,"
                            + "sub_expiry_date,sub_content_delivery_method,sub_source,last_billed_code)"
                            + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            ps.setLong(1, id);
            ps.setLong(2, servID);
            ps.setString(3, subMSISDN);
            ps.setLong(4, subServicePeriod);
            ps.setString(5, reqText);
            ps.setString(6, resText);
            ps.setString(7, subNetwork);
            ps.setLong(8, subServiceOwner);
            ps.setString(9, tob);
            ps.setString(10, iob);
            ps.setString(11, subShortCode);
            ps.setString(12, status);
            ps.setString(13, subExpiryDate);
            ps.setString(14, subContentDeliveryMethod);
            ps.setString(15, subscriptionSource);
            ps.setString(16, lastBilledCode);
            update = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return update;
    }
}
