package org.esme.webservices.mtn;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.esme.broker.AppBroker;
import org.esme.dto.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

@SuppressWarnings("serial")
public class SDPWebUnSubscription extends AppBroker {

    Properties prop;
    int dlr = 0;
    PreparedStatement pstmt;
    ArrayList<Service> serviceCache;

    String sender;
    String text;

    String productID;
    String recipient;

    @SuppressWarnings({"CallToPrintStackTrace", "empty-statement"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        prop = AppBroker.getPropertyFileHandle();

        try {
            productID = removePoison(request.getParameter("productID"));
        } catch (Exception e) {
        }

        try {
            recipient = removePoison(request.getParameter("subscriber"));
        } catch (Exception e) {
        }

        String unSubscriptionRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:loc=\"http://www.org.esme.csapi.org/schema/parlayx/subscribe/manage/v1_0/local\">\n"
                + "    <soapenv:Header>\n"
                + "        <tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com.cn/schema/common/v2_1\">\n"
                + "            <tns:spId>" + prop.getProperty("spID") + "</tns:spId> \n"
                + "            <tns:spPassword>" + DigestUtils.md5Hex(prop.getProperty("spID") + prop.getProperty("spPassword") + formatDateToString3(new Date())) + "</tns:spPassword> \n"
                + "            <tns:timeStamp>" + formatDateToString3(new Date()) + "</tns:timeStamp>\n"
                + "        </tns:RequestSOAPHeader>\n"
                + "    </soapenv:Header>\n"
                + "    <soapenv:Body>\n"
                + "        <loc:unSubscribeProductRequest>\n"
                + "            <loc:unSubscribeProductReq>\n"
                + "                <userID>\n"
                + "                    <ID>" + recipient + "</ID>\n"
                + "                    <type>0</type>\n"
                + "                </userID>\n"
                + "                <subInfo>\n"
                + "                    <productID>1" + productID + "</productID> \n"
                + "                    <operCode>zh</operCode> \n"
                + "                    <isAutoExtend>0</isAutoExtend> \n"
                + "                    <channelID>1</channelID> \n"
                + "                    <extensionInfo>\n"
                + "                        <namedParameters>\n"
                + "                            <key>SubType</key>\n"
                + "                            <value>0</value>\n"
                + "                        </namedParameters>\n"
                + "                    </extensionInfo>\n"
                + "                </subInfo>\n"
                + "            </loc:unSubscribeProductReq>\n"
                + "        </loc:unSubscribeProductRequest> \n"
                + "    </soapenv:Body>\n"
                + "</soapenv:Envelope>";

        org.esme.broker.AppBroker.sevasLogger.info(unSubscriptionRequest);

        HttpClient client = new HttpClient();
        HttpState state = client.getState();

        // Prepare HTTP post
        //Test Server
        PostMethod method = new PostMethod(prop.getProperty("sdpWebUnSubscribeSURL"));
        method.setDoAuthentication(true);

        // Request content will be retrieved directly
        // from the input stream
        RequestEntity entity;
        entity = new StringRequestEntity(unSubscriptionRequest, "text/xml", "ISO-8859-1");
        method.setRequestEntity(entity);
        method.setRequestHeader("SOAPAction", "");

        String responseBody = null;
        // Execute request
        try {
            int result = client.executeMethod(method);
            // Display status code
            org.esme.broker.AppBroker.sevasLogger.info("Response status code: " + result);
            // Display response
            responseBody = method.getResponseBodyAsString();
            out.print(responseBody);
        } catch (IOException ex) {
        } finally {
            // Release current connection to the connection pool once you are done
            method.releaseConnection();
        }

        org.esme.broker.AppBroker.sevasLogger.info(responseBody);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
