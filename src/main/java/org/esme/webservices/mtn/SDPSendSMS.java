package org.esme.webservices.mtn;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.esme.broker.AppBroker;
import org.esme.dto.Service;
import org.esme.utils.pingenerator.RandomPinGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

@SuppressWarnings("serial")
public class SDPSendSMS extends AppBroker {

    Properties prop;

    String subServiceTimeOfBlast, serviceMTShortCode,
            subContentDeliveryMethod, servName, subType,
            messageID, recNetwork, delivertStatus, srcModule, smsc;

    int dlr = 0;
    PreparedStatement pstmt;
    ArrayList<Service> serviceCache;

    String sender;
    String recipient;
    String text;
    String serviceID;

    @SuppressWarnings({"CallToPrintStackTrace", "empty-statement"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        org.esme.broker.AppBroker.sevasLogger.error("We called SDPSendSMS");

        prop = AppBroker.getPropertyFileHandle();

        try {
            srcModule = removePoison(request.getParameter("srcMod"));
            subType = removePoison(request.getParameter("subType"));
            servName = removePoison(request.getParameter("servName"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        sender = removePoison(request.getParameter("from"));
        recipient = removePoison(request.getParameter("to"));
        text = removePoison(request.getParameter("text"));
        serviceID = request.getParameter("servID");


        @SuppressWarnings("LocalVariableHidesMemberVariable")
        String messageID = "0" + RandomPinGenerator.randomPinGenerator(9);

        String sdpSendSMSRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                + "xmlns:v2=\"http://www.huawei.com.cn/schema/common/v2_1\" "
                + "xmlns:loc=\"http://www.org.esme.csapi.org/schema/parlayx/sms/send/v2_2/local\">"
                + "<soapenv:Header>"
                + "<v2:RequestSOAPHeader>"
                + "<v2:spId>" + prop.getProperty("spID") + "</v2:spId>"
                + "<v2:spPassword>" + DigestUtils.md5Hex(prop.getProperty("spID") + prop.getProperty("spPassword") + formatDateToString3(new Date())) + "</v2:spPassword>"
                + "<v2:serviceId>" + serviceID + "</v2:serviceId>"
                + "<v2:timeStamp>" + formatDateToString3(new Date()) + "</v2:timeStamp>"
                + "<v2:OA>tel:" + recipient + "</v2:OA>"
                + "<v2:FA>tel:" + recipient + "</v2:FA>"
                + "</v2:RequestSOAPHeader>"
                + "</soapenv:Header>"
                + "<soapenv:Body>"
                + "<loc:sendSms>"
                + "<loc:addresses>tel:" + recipient + "</loc:addresses>"
                + "<loc:senderName>" + sender + "</loc:senderName>"
                + "<loc:message>" + text + "</loc:message>"
                + "<loc:receiptRequest>"
                + "<endpoint>" + prop.getProperty("sdpDLRURL") + messageID + "</endpoint>"
                + "<interfaceName>DLR_URL</interfaceName>"
                + "<correlator>" + messageID + "</correlator>"
                + "</loc:receiptRequest>"
                + "</loc:sendSms>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        org.esme.broker.AppBroker.sevasLogger.info(sdpSendSMSRequest);
        // Get file to be posted
        // File input = new File("./REQUEST.xml");
        HttpClient client = new HttpClient();
        HttpState state = client.getState();
        // Prepare HTTP post
        //Test Server
        // PostMethod method = new PostMethod("http://41.206.4.219:8310/SendSmsService/services/SendSms");
        PostMethod method = new PostMethod(prop.getProperty("sdpSendSMSURL"));
        method.setDoAuthentication(true);

        // Request content will be retrieved directly
        // from the input stream
        RequestEntity entity;
        entity = new StringRequestEntity(sdpSendSMSRequest, "text/xml", "ISO-8859-1");
        method.setRequestEntity(entity);
        method.setRequestHeader("SOAPAction", "");

        String responseBody = null;
        // Execute request
        try {
            int result = client.executeMethod(method);
            // Display status code
            org.esme.broker.AppBroker.sevasLogger.info("Response status code: " + result);
            // Display response
            responseBody = method.getResponseBodyAsString();
            delivertStatus = "SUBMITTED";
            out.print(responseBody);
        } catch (IOException ex) {
            delivertStatus = "FAILED";

        } finally {
            // Release current postgresDBCconnection to the postgresDBCconnection pool once you are done
            method.releaseConnection();
        }

        //Log outgoing messagess...
        PreparedStatement delpstmt;
        //if uccessful, insert the recent attempt..
        try {

            if (postgresDBCconnection == null || postgresDBCconnection.isClosed()) {
                postgresDBCconnection = getDBConnection();
            }

            delpstmt = postgresDBCconnection.prepareStatement(
                    "insert into delivered_outgoing_messages"
                            + "(message_id,sender,recipient,"
                            + "network,message,delivery_status,serv_name,"
                            + "src_module,smsc,serv_id,service_sub_type) "
                            + "values(?,?,?,?,?,?,?,?,?,?,?)");

            delpstmt.setString(1, messageID);
            delpstmt.setString(2, sender);
            delpstmt.setString(3, recipient);
            delpstmt.setString(4, "mtn");
            delpstmt.setString(5, text.replaceAll("'", "''"));
            delpstmt.setString(6, delivertStatus);
            delpstmt.setString(7, servName);
            delpstmt.setString(8, srcModule);
            delpstmt.setString(9, "mtn");
            delpstmt.setLong(10, Long.parseLong(serviceID));
            delpstmt.setString(11, subType);
            delpstmt.executeUpdate();

        } catch (Exception e) {
            try {
                if (srcModule.trim().equalsIgnoreCase("SubscriptionRenewal")) {
                    PrintWriter pw = new PrintWriter(new BufferedWriter(
                            new FileWriter("/home/sevas/sevas_un_logged_other_msg.txt", true)));
                    pw.println("insert into delivered_outgoing_messages"
                            + "(message_id,sender,recipient,"
                            + "network,message,delivery_status,serv_name,"
                            + "src_module,smsc,serv_id,service_sub_type) "
                            + "values('"
                            + messageID + "','" + sender + "','"
                            + recipient + "',"
                            + "'" + recNetwork + "','" + text.replaceAll("'", "''") + "','"
                            + delivertStatus + "',"
                            + "'" + servName + "','" + srcModule + "','" + smsc + "',"
                            + "" + serviceID + ",'" + subType + "');");
                    pw.close();
                }
            } catch (IOException ex) {
                //oh noes!
            }
            e.printStackTrace();
        }
        org.esme.broker.AppBroker.sevasLogger.info(responseBody);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
