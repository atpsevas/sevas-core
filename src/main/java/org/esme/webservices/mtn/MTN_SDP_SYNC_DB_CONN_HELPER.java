/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.webservices.mtn;

import org.esme.broker.AppBroker;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author saheedalbert
 */
public class MTN_SDP_SYNC_DB_CONN_HELPER {

    static MTN_SDP_SYNC_DB_CONN_HELPER mtnSDPSyncDBConnHelper;
    private static Properties prop;
    private Connection connection;

    //Instance of WEB2SMS  can only be created by call this method...

    @SuppressWarnings("CallToPrintStackTrace")
    public static synchronized MTN_SDP_SYNC_DB_CONN_HELPER getInstance() {
        try {
            if (mtnSDPSyncDBConnHelper == null) {
                mtnSDPSyncDBConnHelper = new MTN_SDP_SYNC_DB_CONN_HELPER();
            }
            prop = AppBroker.getPropertyFileHandle();

        } catch (Exception ex) {
        }
        return mtnSDPSyncDBConnHelper;
    }

    Connection getJNDIConnection() {
        //String DATASOURCE_CONTEXT = "java:comp/env";
        try {
            Context initialContext = (Context) new InitialContext().lookup("java:comp/env");
            if (initialContext == null) {
                org.esme.broker.AppBroker.sevasLogger.info("JNDI problem. Cannot get InitialContext.");
            }
            DataSource datasource = (DataSource) initialContext.lookup("jdbc/sevas");
            ;
            if (datasource != null) {
                this.connection = datasource.getConnection();
            } else {
                org.esme.broker.AppBroker.sevasLogger.info("Failed to lookup datasource.");
            }
        } catch (NamingException | SQLException ex) {
            try {
                org.esme.broker.AppBroker.sevasLogger.info("Cannot get connection from pool " + ex);
                org.esme.broker.AppBroker.sevasLogger.info("Attempting direct conection...");
                try {

                    Class.forName("org.postgresql.Driver");
                } catch (ClassNotFoundException e) {
                    org.esme.broker.AppBroker.sevasLogger.info("Where is your PostgreSQL JDBC Driver? "
                            + "Include in your library path!");
                    e.printStackTrace();
                }

                org.esme.broker.AppBroker.sevasLogger.info("PostgreSQL JDBC Driver Registered!");

                this.connection = DriverManager.getConnection(
                        prop.getProperty("dbURL"), prop.getProperty("dbUserName"),
                        prop.getProperty("dbPassword"));
                org.esme.broker.AppBroker.sevasLogger.info("Direct conection established...");
                org.esme.broker.AppBroker.sevasLogger.info(prop.getProperty("dbURL") + prop.getProperty("dbUserName") + prop.getProperty("dbPassword"));
            } catch (Exception ex1) {
                org.esme.broker.AppBroker.sevasLogger.info("direct conection also failed...App exit");
            }
        }
        return this.connection;
    }
}
