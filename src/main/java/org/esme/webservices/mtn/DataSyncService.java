package org.esme.webservices.mtn;

import org.esme.broker.AppBroker;
import org.esme.controllers.MTBillingResponseCallback;
import org.esme.controllers.UnSubEngine;
import org.esme.controllers.helpers.LogMTBillingResponse;
import org.esme.dto.Service;
import org.esme.utils.pingenerator.RandomPasswordGenerator;
import org.esme.utils.pingenerator.RandomPinGenerator;

import javax.jws.WebService;
import javax.xml.ws.Holder;
import java.io.*;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebService(serviceName = "DataSyncService", portName = "DataSync",
        endpointInterface = "org.esme.webservices.mtn.DataSync", targetNamespace = "http://www.csapi.org/wsdl/parlayx/remote/sync/v1_0/interface")
@SuppressWarnings("serial")

public class DataSyncService implements DataSync {

    PreparedStatement ps;
    BufferedReader reader;
    String allMSISDN;
    ArrayList<Service> servicesCache;
    Service theService;
    PreparedStatement sub = null;
    Properties prop;
    String spIdentity;
    String subMSISDN;
    String servID;
    String prodID;
    String sdpStatus;
    String subStatus;
    int subRequestType;
    String updateDescription;
    String updateTime;
    String expiryTime;
    String effectiveTime;
    String cycleStartTime = null;
    String cycleEndTime = null;
    long durationOfGrace = 0L;
    long durationOfSuspend = 0L;
    String expiryYear;
    String expiryMonth;
    String expiryDay;
    Service dService;

    //private static  AppLogger appLogger = null;
    //private static SendSMS sendSMS;
    Connection conn = null;
    PrintWriter pw = null;
    File dataFile;

    LogMTBillingResponse logMTBillingResponse;

    MTN_SDP_SYNC_DB_CONN_HELPER mtnSDPConnHelper;

    public int syncSubscriptionData(String msisdn, String serviceId,
                                    String productId, int updateType, ProductDetail productDetail) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public int changeMSISDN(String msisdn, String newMSISDN, String timeStamp) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @SuppressWarnings({"UseSpecificCatch", "CallToPrintStackTrace", "fallthrough"})
    public void syncOrderRelation(UserID userID, String spID, String productID, String serviceID,
                                  String serviceList, int updateType, String updateT, String updateDesc,
                                  String effectiveT, String expiryT, Holder<NamedParameterList> extensionInfo,
                                  Holder<Integer> result, Holder<String> resultDescription) {

        SyncOrderRelationResponse rsp = new SyncOrderRelationResponse();
        SyncOrderRelationResponse param = new SyncOrderRelationResponse();

        mtnSDPConnHelper = MTN_SDP_SYNC_DB_CONN_HELPER.getInstance();

        try {

            prop = AppBroker.getPropertyFileHandle();

            try {
                spIdentity = spID;
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                subMSISDN = userID.getID();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                prodID = productID;
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                subRequestType = updateType;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                updateDescription = updateDesc;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                updateTime = updateT;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                expiryTime = expiryT;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                effectiveTime = effectiveT;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                cycleStartTime = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                cycleEndTime = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                durationOfGrace = 0L;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                durationOfSuspend = 0L;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                expiryYear = expiryT.substring(0, 4);
                expiryMonth = expiryT.substring(4, 6);
                expiryDay = expiryT.substring(6, 8);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                org.esme.broker.AppBroker.sevasLogger.info("=====================receive syncOrderRelation request==================================");
                org.esme.broker.AppBroker.sevasLogger.info("Request Date : " + formatDateToString(new Date()));
                org.esme.broker.AppBroker.sevasLogger.info("Subscriber : " + userID.getID());
                org.esme.broker.AppBroker.sevasLogger.info("MTN Product ID | Service Keyword : " + productID);
                org.esme.broker.AppBroker.sevasLogger.info("MTN Service ID : " + serviceID);
                org.esme.broker.AppBroker.sevasLogger.info("Update Type : " + updateType);
                org.esme.broker.AppBroker.sevasLogger.info("EffectiveTime : " + effectiveT);
                org.esme.broker.AppBroker.sevasLogger.info("ExpiryTime : " + expiryT);

                // TODO: Fix Incompatible type for Sync Order payload
                NamedParameterList extensionInfoList = extensionInfo.value;
                if (null != extensionInfoList) {
                    for (NamedParameter extensionInfo2 : extensionInfoList.getItem()) {
                        //org.esme.broker.AppBroker.sevasLogger.info("extensionKey " + extensionInfo2.getKey());
                        //org.esme.broker.AppBroker.sevasLogger.info("extensionValue " + extensionInfo2.getValue());
                        try {
                            if (extensionInfo2.getKey().equalsIgnoreCase("cycleStartTime")) {
                                cycleStartTime = extensionInfo2.getValue();
                            } else if (extensionInfo2.getKey().equalsIgnoreCase("cycleEndTime")) {
                                cycleEndTime = extensionInfo2.getValue();
                            } else if (extensionInfo2.getKey().equalsIgnoreCase("durationOfGracePeriod")) {
                                durationOfGrace = Long.parseLong(extensionInfo2.getValue());
                            } else if (extensionInfo2.getKey().equalsIgnoreCase("durationOfSuspendPeriod")) {
                                durationOfSuspend = Long.parseLong(extensionInfo2.getValue());
                            } else if (extensionInfo2.getKey().equalsIgnoreCase("status")) {
                                sdpStatus = extensionInfo2.getValue();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }


            //sendSMS = SendSMS.getInstance();
            dService = getService(Long.parseLong(prodID));

            switch (subRequestType) {
                case 1:
                    // New Subscription - Subscribe User
                    try {
                        long subID = RandomPinGenerator.randomPinGenerator(9);

                        // Subscribe User
                        subscribeUser(subID, dService, "active");

                        // Send to AMPQ to send content
                        (new MTBillingResponseCallback()).publishToAMPQ(subMSISDN, dService.getServiceID(), dService.getServiceShortCode(), dService.getServiceNetwork(), "NewSubscription", 8, AppBroker.rabbitQueueName_PROP);

                    } catch (Exception e) {
                        e.printStackTrace();
                        param.setResult(2500);
                        param.setResultDescription("Unable to the process request.");
//            return rsp;
                    }
                    break;

                case 2:
                    // Subscription Deactivation
                    try {

                        (new UnSubEngine()).deactivateServiceSpecifySource(dService.getServiceID(), subMSISDN, "sdp");

                    } catch (Exception e) {
                        param.setResult(2500);
                        param.setResultDescription("Unable to process the request.");
//            return rsp;
                    }
                    break;

                case 3:
                    // Subscription Renewal

                    try {
                        // Ensuring Service Period is configured for Service
                        (new MTBillingResponseCallback()).publishToAMPQ(subMSISDN, dService.getServiceID(), dService.getServiceShortCode(), dService.getServiceNetwork(), "SubscriptionRenewal", 8, AppBroker.rabbitQueueName_PROP);
                    } catch (Exception e) {
                        param.setResult(2500);
                        param.setResultDescription("Unable to process the request.");
//            return rsp;
                    }
                    break;
            }

            org.esme.broker.AppBroker.sevasLogger.info(subMSISDN + "|" + productID + "|" + sdpStatus + "|" + effectiveTime + "|" + expiryTime);
            param.setResult(0);
            param.setResultDescription("success");
            rsp = param;

        } catch (Exception e) {

        }
//    return rsp;
    }

    public void syncMSISDNChange(
            String msisdn, String newMSISDN, Holder<NamedParameterList> extensionInfo,
            Holder<Integer> result, Holder<String> resultDescription) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void syncSubscriptionActive(
            UserID userID, String spID, String productID, String serviceID,
            Holder<NamedParameterList> extensionInfo, Holder<Integer> result,
            Holder<String> resultDescription) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public long substractDate(String dateA, String dateB) {
        long result = 0L;
        try {
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
            long d1 = formater.parse(dateA).getTime();
            long d2 = formater.parse(dateB).getTime();
            result = Math.abs((d1 - d2) / 86400000L);
        } catch (ParseException ex) {
            Logger.getLogger(DataSyncService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public Date formatStringToDate(String theString) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateInString = theString;
        Date date = null;
        try {
            date = formatter.parse(dateInString);
            org.esme.broker.AppBroker.sevasLogger.info(date);
            org.esme.broker.AppBroker.sevasLogger.info(formatter.format(date));
        } catch (ParseException e) {
        }
        return date;
    }

    @SuppressWarnings("deprecation")
    boolean isAfterToday(Date d) {
        Date today = new Date();
        today.setHours(0);
        today.setMinutes(0);
        today.setSeconds(0);
        return d.after(today);
    }


    public int subscribeUser(long id, Service dService, String status) {

        int update = 0;
        try {

            org.esme.broker.AppBroker.sevasLogger.info("Subscribing the user...");
            String query
                    = "insert into subscription(id,sub_service_id,sub_msisdn,"
                    + "sub_service_period,sub_request_text,sub_response_text,"
                    + "sub_network,sub_service_owner_id,sub_tob,sub_shortcode,"
                    + "sub_status,sub_expiry_date,sub_content_delivery_method,"
                    + "sub_source,last_billed_code)"
                    + "values("
                    + "" + id + "," + dService.getServiceID() + ",'" + subMSISDN + "',"
                    + "" + Long.parseLong(dService.getServicePeriod()) + ","
                    + "'" + dService.getServiceKeyword() + "',"
                    + "'" + dService.getServiceSubscriptionMessage() + "',"
                    + "'" + dService.getServiceNetwork() + "',"
                    + "" + dService.getServiceOwner() + ","
                    + "'" + dService.getTimeOfBlast() + "',"
                    + "'" + dService.getServiceShortCode() + "',"
                    + "'" + status + "',"
                    + "'" + addDate(Long.parseLong(dService.getServicePeriod())) + "',"
                    + "'" + dService.getServiceContentDeliveryMethod() + "','sdp',"
                    + "'" + dService.getServiceShortCode() + "')";

            try {

                if (conn == null || conn.isClosed()) {
                    conn = mtnSDPConnHelper.getJNDIConnection();
                }

                ps = conn.prepareCall(query);
                update = ps.executeUpdate();

                if (update > 0) {
                    //Send first content
                    if (getCurrentHour() > Integer.parseInt(dService.getTimeOfBlast())) {
                        theFirstMTContent(dService, subMSISDN);
                    }
                }

            } catch (SQLException | NumberFormatException e) {
                try {

                    dataFile = new File("/home/sevas/sqlFile.sql");
                    if (!dataFile.exists()) {
                        dataFile.createNewFile();
                    }
                    pw = new PrintWriter(new FileWriter(dataFile, true), true);
                    pw.write(query + ";\n");
                    pw.flush();

                } catch (Exception ec) {
                    ec.printStackTrace();

                } finally {
                    pw.close();
                }
                e.printStackTrace();
            }
            //Log billing/renewal report....
            //Log it....
            org.esme.broker.AppBroker.sevasLogger.info("Logging to delivered table....");

            logMTBillingResponse = new LogMTBillingResponse();
            logMTBillingResponse.setMessageID(generateMessageID());
            logMTBillingResponse.setSenderID(dService.getServiceShortCode());
            logMTBillingResponse.setRecipientMSISDN(subMSISDN);
            logMTBillingResponse.setSmsc("MTN");
            logMTBillingResponse.setMessage(dService.getServiceSubscriptionMessage());
            logMTBillingResponse.setDelivertStatus("SUBMITTED");
            logMTBillingResponse.setServName(dService.getServiceName());
            logMTBillingResponse.setSrcModule("NewSubscription");
            logMTBillingResponse.setSmsc("MTN");
            logMTBillingResponse.setServiceID(dService.getServiceID());
            logMTBillingResponse.setSubscriptionBillingType("MTN");
            logMTBillingResponse.billing_outgoing_messages(logMTBillingResponse);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return update;
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public void extendSubscription(Service dService, String subscriber,
                                   String subStatus) {
        try {
            String msisdn = subscriber.substring(subscriber.length() - 9);
            try {

                pw = null;
                try {

                    if (conn == null || conn.isClosed()) {
                        conn = mtnSDPConnHelper.getJNDIConnection();
                    }

                    ps = conn.prepareCall("update subscription set sub_expiry_date = '"
                            + expiryYear + "-" + expiryMonth + "-"
                            + expiryDay + "',  " + "sub_status = '" + subStatus + "', "
                            + "last_billed_code = '" + dService.getServiceShortCode() + "', "
                            + "last_billed_date = CURRENT_TIMESTAMP "
                            + "where sub_msisdn ilike '%"
                            + msisdn + "' " + "and sub_service_id  =  " + dService.getServiceID());
                    int extend = ps.executeUpdate();

                    //The dude is  not in the db...we will insert it..
                    if (extend < 1) {
                        subscribeUser(dService.getServiceID(), dService, "active");
                    }

                } catch (Exception ex1) {
                    dataFile = new File("/home/sevas/sqlFile.sql");
                    if (!dataFile.exists()) {
                        dataFile.createNewFile();
                    }
                    pw = new PrintWriter(new FileWriter(dataFile, true), true);
                    pw.write(
                            "update subscription set sub_expiry_date = '"
                                    + expiryYear + "-" + expiryMonth + "-"
                                    + expiryDay + "',  " + "sub_status = '" + subStatus + "', "
                                    + "last_billed_code = '" + dService.getServiceShortCode() + "', "
                                    + "last_billed_date = CURRENT_TIMESTAMP "
                                    + "where sub_msisdn ilike '%"
                                    + msisdn + "' " + "and sub_service_id  =  "
                                    + dService.getServiceID() + ";\n");
                    ex1.printStackTrace();
                    pw.flush();
                } finally {
                    pw.close();
                }

                org.esme.broker.AppBroker.sevasLogger.info("Subscription Entended");

            } catch (Exception e) {

            }

            //Log billing/renewal report....
            //Log it....
            org.esme.broker.AppBroker.sevasLogger.info("Logging to delivered table....");
            logMTBillingResponse = new LogMTBillingResponse();
            logMTBillingResponse.setMessageID(generateMessageID());
            logMTBillingResponse.setSenderID(dService.getServiceShortCode());
            logMTBillingResponse.setRecipientMSISDN(subMSISDN);
            logMTBillingResponse.setSmsc("MTN");
            logMTBillingResponse.setMessage(dService.getServiceRenewalMessage());
            logMTBillingResponse.setDelivertStatus("SUBMITTED");
            logMTBillingResponse.setServName(dService.getServiceName());
            logMTBillingResponse.setSrcModule("SubscriptionRenewal");
            logMTBillingResponse.setSmsc("MTN");
            logMTBillingResponse.setServiceID(dService.getServiceID());
            logMTBillingResponse.setSubscriptionBillingType("MTN");
            logMTBillingResponse.billing_outgoing_messages(logMTBillingResponse);

            //Send first content
            if (getCurrentHour() > Integer.parseInt(dService.getTimeOfBlast())) {
                theFirstMTContent(getService(dService.getServiceID()), subscriber);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings({"unchecked", "CallToPrintStackTrace", "unchecked", "unchecked", "UseSpecificCatch"})
    public void theFirstMTContent(Service service, String recipientMSISDN) {
        org.esme.broker.AppBroker.sevasLogger.info("Send first content...FAKE");
    }

    //
    @SuppressWarnings("CallToPrintStackTrace")
    public String formatText(String text) {
        String formattedContent;
        String myString = text.trim();
        byte[] ptext;
        try {
            ptext = myString.getBytes("ISO-8859-1");
            formattedContent = new String(ptext, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            return text;
        }
        return formattedContent;
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public String addDate(long NoOfDays) {
        int castedNumber = (int) NoOfDays;
        Date date = new Date();
        String dt = formatDateToString(date);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(dt));
            c.add(Calendar.DATE, castedNumber);
            dt = sdf.format(c.getTime());
        } catch (ParseException ex) {
        }
        return dt;
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public int getCurrentHour() {
        Date date = new Date();   // given date
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(date);   // assigns calendar to given date
        calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
        calendar.get(Calendar.HOUR);        // gets hour in 12h format
        calendar.get(Calendar.MONTH);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public String generateMessageID() {
        return RandomPasswordGenerator.randomPasswordGenerator();
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public String formatDateToString(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public Service getService(long serviceID) {
        org.esme.broker.AppBroker.sevasLogger.info("Querying for SDP Services...");
        Service service = null;
        try {

            if (conn == null || conn.isClosed()) {
                conn = mtnSDPConnHelper.getJNDIConnection();
            }

            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "select * from subscription_service "
                            + "where service_keyword  ilike '%" + serviceID + "%' or id = " + serviceID + " ");

            while (resultSet.next()) {

                org.esme.broker.AppBroker.sevasLogger.info("Populating services cahche...");

                service = new Service();
                service.setServiceID(resultSet.getLong("id"));
                service.setDateCreated(resultSet.getString("date_created"));
                service.setServiceOwner(resultSet.getInt("service_owner"));
                service.setServiceName(resultSet.getString("service_name"));
                service.setServiceShortcode(resultSet.getString("service_shortcode"));
                service.setServiceMTShortCode(resultSet.getString("service_mt_shortcode"));
                service.setServiceBillingShortCode(resultSet.getString("welcome_shortcode"));
                service.setServiceKeyword(resultSet.getString("service_keyword"));
                service.setServiceAllowableKeyword(resultSet.getString("service_allowable_keyword"));
                service.setServiceUnSubscribeKeyword(resultSet.getString("service_unsubcribe_keyword"));
                service.setServicePrice(resultSet.getString("service_price"));
                service.setServiceCategory(resultSet.getString("service_category"));
                service.setServiceCategoryKeyword(resultSet.getString("service_category_keyword"));
                service.setServiceDescription(resultSet.getString("service_description"));
                service.setServicePeriod(resultSet.getString("service_period"));
                service.setFallbackPeriod(resultSet.getLong("service_fallback_period"));
                service.setTimeOfBlast(resultSet.getString("service_tob"));
                service.setServiceSubscriptionMessage(resultSet.getString("service_subscription_msg"));
                service.setServiceUnSubScriptionMessage(resultSet.getString("service_unsubscription_msg"));
                service.setServiceReminderMessage(resultSet.getString("service_reminder_msg"));
                service.setServiceReminderPeriod(resultSet.getString("service_reminder_period"));
                service.setFallbackShortCode(resultSet.getString("fallback_shortcode"));
                service.setFallbackPrice(resultSet.getLong("fallback_price"));
                service.setServiceContentDeliveryMethod(resultSet.getString("service_content_delivery_method"));
                service.setBillingRetryIntervals(resultSet.getLong("service_billing_retry_intervals"));
                service.setBillingTime(resultSet.getLong("service_renewal_time_of_the_day"));
                service.setServiceRenewalMessage(resultSet.getString("service_renewal_message"));
                service.setServiceFallbackRenewalMessage(resultSet.getString("service_fallback_renewal_message"));
                service.setServiceHelpMessage(resultSet.getString("service_help_message"));
                service.setServiceReminderTime(resultSet.getLong("service_reminder_time"));
                service.setServiceSubType(resultSet.getString("service_sub_type"));
                service.setServiceNetwork(resultSet.getString("service_network"));
                service.setServiceWelcomeMessageShortcode(resultSet.getString("service_wc_msg_shortcode"));
                service.setSeriviceBillingNetwork(resultSet.getString("billing_network"));
                service.setServiceStatus(resultSet.getString("service_status"));
                service.setServicePromo(resultSet.getString("service_promo"));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return service;
    }

}
