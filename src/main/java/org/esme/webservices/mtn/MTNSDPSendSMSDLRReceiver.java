package org.esme.webservices.mtn;

import org.esme.broker.AppBroker;
import org.esme.dto.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.PreparedStatement;
import java.util.ArrayList;


public class MTNSDPSendSMSDLRReceiver extends AppBroker {
    private static final long serialVersionUID = 1L;

    int dlr = 0;
    String deliveryStatus;
    PreparedStatement pstmt;
    ArrayList<Service> serviceCache;
    BufferedReader br;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            try {

                br = new BufferedReader(request.getReader());
                String dlrXMLString;

                @SuppressWarnings("StringBufferMayBeStringBuilder")
                StringBuffer workBuffer = new StringBuffer();

                while ((dlrXMLString = br.readLine()) != null) {
                    workBuffer.append(dlrXMLString);
                    if (dlrXMLString.startsWith("<deliveryStatus>")) {
                        //Get the actual delivery status string...XML parsing is better option thouhg..
                        deliveryStatus = dlrXMLString.replace("<deliveryStatus>", "").replace("</deliveryStatus>", "");
                    }
                }

                dlrXMLString = workBuffer.toString();
                org.esme.broker.AppBroker.sevasLogger.info("TransportHTTPServlet - Got XML: " + dlrXMLString);

            } catch (Exception e) {
                dlr = 1;
            }

            @SuppressWarnings("StringBufferMayBeStringBuilder")
            StringBuffer sb = new StringBuffer();
            //sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
            sb.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:loc=\"http://www.org.esme.csapi.org/schema/parlayx/sms/notification/v2_2/local\">\n");
            sb.append("<soapenv:Header/>\n");
            sb.append("    <soapenv:Body>\n");
            sb.append("        <loc:notifySmsDeliveryReceiptResponse xmlns=\"http://www.org.esme.csapi.org/schema/parlayx/sms/notification/v2_2/local/\"/>\n");
            sb.append("    </soapenv:Body>\n");
            sb.append("</soapenv:Envelope>\n");
            out.println(sb.toString());
            out.flush();

            org.esme.broker.AppBroker.sevasLogger.info("Our Response: " + sb.toString());

            //Update the outgoing messages table...
            String query = "update delivered_outgoing_messages set delivery_status = '" + deliveryStatus + "' "
                    + "where message_id = '" + request.getParameter("") + "' "
                    + "and date(sent_time) = date(now())";
            PreparedStatement delpstmt;

            //if uccessful, insert the recent attempt..
            try {

                if (postgresDBCconnection == null || postgresDBCconnection.isClosed()) {
                    postgresDBCconnection = getDBConnection();
                }

                delpstmt = postgresDBCconnection.prepareStatement(query);
                delpstmt.executeUpdate();

            } catch (Exception e) {
                try {
                    PrintWriter pw = new PrintWriter(new BufferedWriter(
                            new FileWriter("/home/sevas/sevas_un_logged_other_msg.txt", true)));
                    pw.println(query);
                    pw.close();
                } catch (IOException ex) {
                    //oh noes!
                }
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
