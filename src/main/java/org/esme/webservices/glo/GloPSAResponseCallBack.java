package org.esme.webservices.glo;

import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.HelperBroker;
import org.esme.controllers.helpers.LogMTBillingResponse;
import org.esme.controllers.helpers.UpdateSubscription;
import org.esme.dto.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class GloPSAResponseCallBack extends AppBroker {

    long serviceID;
    String billingCode;
    String billingModule;
    long amount;
    String subscriber;
    String content;
    String transactionResponseCode;
    String instantContent;
    long transactionID;
    LogMTBillingResponse logMTBillingResponse;

    Service appBillingService;

    PreparedStatement ps;

    boolean isSubscriberBilled = false;
    String params;

    HelperBroker hb;

    ArrayList<Service> serviceCache;

    @SuppressWarnings({"unchecked", "UseSpecificCatch", "CallToPrintStackTrace"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String transactionDescription = "FAILED";

        serviceID = Long.parseLong(removePoison(request.getParameter("serviceID")));
        transactionID = Long.parseLong(removePoison(request.getParameter("transactionID")));
        billingCode = removePoison(request.getParameter("billingCode"));
        billingModule = removePoison(request.getParameter("billingSrcModule"));
        content = removePoison(request.getParameter("content"));
        amount = Long.parseLong(removePoison(request.getParameter("amount")));
        subscriber = removePoison(request.getParameter("subscriber"));
        transactionResponseCode = removePoison(request.getParameter("transactionResponseCode"));
        hb = new HelperBroker();

        try {
            serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
        } catch (Exception e) {
        }

        if (serviceCache == null || serviceCache.size() < 1) {
            updateServicesCache();
            serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
        }

        //Log the transaction...
        try {
            for (Service theService : serviceCache) {

                if (theService.getServiceID() == serviceID) {

                    appBillingService = theService;

                    if (transactionResponseCode.equalsIgnoreCase("0")) {

                        transactionDescription = "SUCCESS";
                        isSubscriberBilled = true;

                        //Instant content charging...
                        if (theService.getServicePeriod().equals("0") || billingModule.trim().equalsIgnoreCase("DailyPaidContent")) {
                            try {
                                //Send instant content.
                                sendInstantContent(theService);
                            } catch (SQLException E) {

                            }
                        } else {

                            if (billingModule.trim().equalsIgnoreCase("SubscriptionRenewal")
                                    || billingModule.trim().contains("SubscriptionRenewalRetry")
                                    || billingModule.trim().equalsIgnoreCase("NewSubscription")) {
                                isSubscriberBilled = true;

                                try {
                                    org.esme.broker.AppBroker.sevasLogger.info("Logging transactions status...");
                                    customBillingLogCondition(appBillingService, subscriber,
                                            content, billingModule, isSubscriberBilled);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                //Send billing notification...
                                try {
                                    sendBillingNotofication(theService, subscriber, billingModule);
                                } catch (Exception E) {
                                    E.printStackTrace();
                                }

                                //Log billing report
                                org.esme.broker.AppBroker.sevasLogger.info("PSA Billing Status received " + transactionResponseCode);
                                //update subscription...
                                try {
                                    //Send to help class for process...
                                    new UpdateSubscription().
                                            updateUserSubscription(getDBConnection(),
                                                    theService, theService.getServiceShortCode(), subscriber,
                                                    billingModule, "success");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                //If new subscription...Let him get content....
                                if (billingModule.trim().equalsIgnoreCase("NewSubscription")) {
                                    if (getCurrentHour() >= Long.parseLong(theService.getTimeOfBlast())) {
                                        sendFirstMTContent(theService, subscriber);
                                    }
                                }
                            }
                        }
                    } else {
                        new UpdateSubscription().
                                updateUserSubscription(getDBConnection(),
                                        theService, theService.getServiceShortCode(), subscriber,
                                        billingModule, "failed");
                    }
                    break;
                }
            }

            org.esme.broker.AppBroker.sevasLogger.info("Logging transaction...");
            chargingTransactionPSALog(
                    serviceID, transactionID, subscriber, amount, prop.getProperty("psaToken"),
                    billingCode, prop.getProperty("psaVendorUserName"),
                    prop.getProperty("psaPassword"), prop.getProperty("psaToken"),
                    transactionResponseCode, transactionDescription,
                    billingModule, getDBConnection());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    //Log glo psa billing transaction....
    @SuppressWarnings({"UseSpecificCatch", "CallToPrintStackTrace"})
    public void chargingTransactionPSALog(long service,
                                          long transactionID,
                                          String msisdn, long amount,
                                          String token, String vendorCode, String vendorUserName,
                                          String vendorPassword, String hashKey, String billingResponsCode,
                                          String billingResponseDecription,
                                          String billingSourceModule, Connection dbConnection) {

        try {

            PreparedStatement preparedStatement = dbConnection.prepareStatement(
                    "insert into glo_psa_billing_logs "
                            + "(billing_service_id,billing_transaction_id,"
                            + "billing_msisdn,billing_amount,billing_token,"
                            + "billing_vendor_code,billing_vendor_username,"
                            + "billing_vendor_password,"
                            + "billing_response_code, "
                            + "billing_response_description,src_module) "
                            + "values(?,?,?,?,?,?,?,?,?,?,?)");

            preparedStatement.setLong(1, service);
            preparedStatement.setString(2, Long.toString(transactionID));
            preparedStatement.setString(3, msisdn);
            preparedStatement.setLong(4, amount);
            preparedStatement.setString(5, token);
            preparedStatement.setString(6, vendorCode);
            preparedStatement.setString(7, vendorUserName);
            preparedStatement.setString(8, vendorPassword);
            preparedStatement.setString(9, billingResponsCode);
            preparedStatement.setString(10, billingResponseDecription);
            preparedStatement.setString(11, billingSourceModule);
            preparedStatement.executeUpdate();
            org.esme.broker.AppBroker.sevasLogger.info("Logged...");

        } catch (Exception E) {
            E.printStackTrace();
        }
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public void sendBillingNotofication(Service service, String msisdn, String srcModule) {
        prop = AppBroker.getPropertyFileHandle();

        DLRURL = prop.getProperty("dlr_url");
        DLR_MASK = prop.getProperty("dlrMask");

        String message;

        //SubscriptionRenewal
        if (srcModule.equalsIgnoreCase("SubscriptionRenewal") || srcModule.equalsIgnoreCase("NewSubscription")) {
            message = service.getServiceRenewalMessage();
        } else {
            message = service.getServiceFallbackRenewalMessage();
        }

        try {

            long epochTime = System.currentTimeMillis() / 1000;
            long pid = 64;

            ps = postgresDBCconnection.prepareStatement(
                    "INSERT INTO send_sms (momt, sender, receiver, "
                            + "msgdata, smsc_id, sms_type, boxc_id,"
                            + "dlr_mask,dlr_url,time,pid) "
                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?)");

            org.esme.broker.AppBroker.sevasLogger.error("Seding..." + message + " to " + msisdn);
            params = encodeParams(service.getServiceBillingNetwork())
                    + "&sub_type=" + encodeParams(service.getServiceSubType())
                    + "&msg_id=" + encodeParams(generateMessageID())
                    + "&sc=" + encodeParams(service.getServiceShortCode())
                    + "&msisdn=" + encodeParams(msisdn)
                    + "&msg=" + encodeParams(formatText(message))
                    + "&serv_id=" + service.getServiceID()
                    + "&serv_name=" + encodeParams(service.getServiceName())
                    + "&src_module=" + encodeParams(srcModule)
                    + "&dlr_mask=" + encodeParams(DLR_MASK) + "&dlr=%d";

            ps.setString(1, service.getServiceSubType());
            ps.setString(2, service.getServiceShortCode());
            ps.setString(3, msisdn);
            ps.setString(4, formatText(message));
            ps.setString(5, service.getServiceNetwork());
            ps.setLong(6, 2);
            ps.setString(7, prop.getProperty("contentSMSBoxID"));
            ps.setLong(8, Long.parseLong(DLR_MASK));
            ps.setString(9, DLRURL + params);
            ps.setLong(10, epochTime + 3600);
            ps.setLong(11, pid);
            ps.executeUpdate();

            org.esme.broker.AppBroker.sevasLogger.info(service.getServiceShortCode() + " "
                    + msisdn + " " + formatText(message) + " "
                    + service.getServiceNetwork());

        } catch (SQLException | NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public void sendInstantContent(Service theService) throws SQLException {

        prop = AppBroker.getPropertyFileHandle();

        DLRURL = prop.getProperty("dlr_url");
        DLR_MASK = prop.getProperty("dlrMask");
        long epochTime = System.currentTimeMillis() / 1000;
        long pid = 64;

        ps = getDBConnection().prepareStatement(
                "INSERT INTO send_sms (momt, sender, receiver, "
                        + "msgdata, smsc_id, sms_type, boxc_id,"
                        + "dlr_mask,dlr_url,time,pid) "
                        + "VALUES (?,?,?,?,?,?,?,?,?,?,?)");

        org.esme.broker.AppBroker.sevasLogger.error("Seding..." + this.content + " to " + subscriber);
        params = encodeParams(theService.getServiceBillingNetwork())
                + "&sub_type=" + encodeParams(theService.getServiceSubType())
                + "&msg_id=" + encodeParams(generateMessageID())
                + "&sc=" + encodeParams(theService.getServiceMTShortCode())
                + "&msisdn=" + encodeParams(subscriber)
                + "&msg=" + encodeParams(formatText(content))
                + "&serv_id=" + theService.getServiceID()
                + "&serv_name=" + encodeParams(theService.getServiceName())
                + "&src_module=" + encodeParams(billingModule)
                + "&dlr_mask=" + encodeParams(DLR_MASK) + "&dlr=%d";

        ps.setString(1, theService.getServiceSubType());
        ps.setString(2, theService.getServiceMTShortCode());
        ps.setString(3, subscriber);
        ps.setString(4, formatText(content));
        ps.setString(5, theService.getServiceNetwork());
        ps.setLong(6, 2);
        ps.setString(7, prop.getProperty("contentSMSBoxID"));
        ps.setLong(8, Long.parseLong(DLR_MASK));
        ps.setString(9, DLRURL + params);
        ps.setLong(10, epochTime + 3600);
        ps.setLong(11, pid);
        ps.executeUpdate();
    }

    //Log every request.....
    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace"})
    public int subscribeUser(long id, long servID, String subMSISDN, long subServicePeriod,
                             String reqText, String resText, String subNetwork,
                             long subServiceOwner, String tob, String iob,
                             String subShortCode, String status, String subExpiryDate,
                             String subContentDeliveryMethod, String subscriptionSource, String lastBilledCode) {
        int update = 0;

        try {

            ps = getDBConnection().prepareStatement(
                    "insert into subscription("
                            + "id,sub_service_id,sub_msisdn,sub_service_period,sub_request_text,"
                            + "sub_response_text,sub_network,sub_service_owner_id,"
                            + "sub_tob,sub_iob,sub_shortcode,sub_status,"
                            + "sub_expiry_date,sub_content_delivery_method,sub_source,last_billed_code)"
                            + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            ps.setLong(1, id);
            ps.setLong(2, servID);
            ps.setString(3, subMSISDN);
            ps.setLong(4, subServicePeriod);
            ps.setString(5, reqText);
            ps.setString(6, resText);
            ps.setString(7, subNetwork);
            ps.setLong(8, subServiceOwner);
            ps.setString(9, tob);
            ps.setString(10, iob);
            ps.setString(11, subShortCode);
            ps.setString(12, status);
            ps.setString(13, subExpiryDate);
            ps.setString(14, subContentDeliveryMethod);
            ps.setString(15, subscriptionSource);
            ps.setString(16, lastBilledCode);
            update = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return update;
    }
}
