package org.esme.webservices.glo;

import org.esme.broker.AppBroker;
import org.esme.dto.Service;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Properties;
import java.util.Random;

public class GloPSA {

    static Properties prop;
    static HttpURLConnection httpConnection;
    private static GloPSA gloXMLSOAPFilterWebservice;
    URL url;
    OutputStreamWriter out;
    //Databse connection...
    PreparedStatement preparedStatement = null;
    DocumentBuilderFactory dbFactory;
    DocumentBuilder dBuilder;
    Document doc;

    //Long.toString(data.length())
    @SuppressWarnings("CallToPrintStackTrace")
    public static synchronized GloPSA getInstance() throws Exception {
        try {
            if (gloXMLSOAPFilterWebservice == null) {
                gloXMLSOAPFilterWebservice = new GloPSA();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        prop = AppBroker.getPropertyFileHandle();


        return gloXMLSOAPFilterWebservice;
    }

    @SuppressWarnings({"deprecation", "CallToPrintStackTrace", "UseSpecificCatch"})

    public void makeGloChargingRquest(Service service,
                                      long amount, String billingSrcModule, String billingCode,
                                      String subscriber, Connection dbConnection, String content) throws Exception {

        long transactionID = generateMessageID();
        String theContent = content;
        if (theContent.equalsIgnoreCase("")) {
            theContent = "No Content";
        }

        ///String callBackURL = "http://localhost:8585/sevas/"

        String callBackURL = prop.getProperty("gloPSABillingResponseCallbackURL")
                + "GloPSAResponseCallBack?serviceID=" + String.valueOf(service.getServiceID())
                + "&subscriber=" + subscriber + "&billingCode=" + billingCode
                + "&billingSrcModule=" + billingSrcModule + "&transactionID=" + transactionID
                + "&amount=" + Long.toString(amount) + "&content=" + theContent + "&transactionResponseCode=%d";

        String encodedCallBackURL = URLEncoder.encode(callBackURL, "UTF-8");
        org.esme.broker.AppBroker.sevasLogger.info("CALLBACK URL " + callBackURL);
        org.esme.broker.AppBroker.sevasLogger.info("ENCODED CALLBACK URL " + encodedCallBackURL);

        String params
                = "transactionID=" + Long.toString(transactionID)
                + "&msisdn=" + subscriber + "&rate_code=" + billingCode + "&cb_url=" + encodedCallBackURL;

        String theURL = prop.getProperty("gloPSABillingWrapperURL") + params;

        //String theURL = "http://starfish.atp-sevas.com/glo_psa_http_client.php?" + params;
        URL urldemo = new URL(theURL);

        org.esme.broker.AppBroker.sevasLogger.error("URL " + theURL);

        URLConnection yc = urldemo.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                yc.getInputStream()));

        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            org.esme.broker.AppBroker.sevasLogger.info(inputLine);
        }
        in.close();
    }

    //The difference, measured in seconds, between the
    //current time and midnight, January 1, 1970 UTC. 
    public long getTimeDiff() {
        long timeSeconds = System.currentTimeMillis() / 1000;
        return timeSeconds;
    }

    public long generateMessageID() {
        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(900000);
        return n;
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace"})
    public String encodeParams(String param) {
        String encParam = "";
        try {
            encParam = URLEncoder.encode(param, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return encParam;
    }
}
