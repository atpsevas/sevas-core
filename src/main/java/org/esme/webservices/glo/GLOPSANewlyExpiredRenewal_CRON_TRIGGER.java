package org.esme.webservices.glo;

import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.BillingBroadCaster;
import org.esme.dto.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class GLOPSANewlyExpiredRenewal_CRON_TRIGGER extends AppBroker {

    ArrayList<Service> serviceCache;

    //Billing Helper...
    BillingBroadCaster billingBroadCaster;
    String billingTracking;

    @SuppressWarnings({"unchecked", "CallToPrintStackTrace"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        org.esme.broker.AppBroker.sevasLogger.info("Billing process callled....");

        try {
            response.setContentType("text/html;charset=UTF-8");

            try {
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            } catch (Exception e) {
            }

            if (serviceCache == null || serviceCache.size() < 1) {
                updateServicesCache();
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            }

            for (Service nextService : serviceCache) {

                if (nextService.getServiceSubType().equalsIgnoreCase("psa")) {

                    org.esme.broker.AppBroker.sevasLogger.info("NAME OF SERVCE " + nextService.getServiceName() + " ID OF SERVICE " + nextService.getServiceID());
                    billingBroadCaster = BillingBroadCaster.getInstance(getDBConnection());
                    billingBroadCaster.doGLOPSABilling(nextService, "SubscriptionRenewal");
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
