package org.esme.logger;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * @author saheedalbert
 * @author tofunmi
 */
public class AppLogger {

    @SuppressWarnings("NonConstantLogger")
    static Logger appLogger;

    //Instance of AppLogger  can only be created by call this method...
    @SuppressWarnings({"UseSpecificCatch", "CallToPrintStackTrace"})
    public static synchronized Logger getInstance(String logFilePath) {

        try {
            if (appLogger == null) {
                PropertyConfigurator.configure(logFilePath);
                appLogger = Logger.getLogger(AppLogger.class);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return appLogger;
    }
}
