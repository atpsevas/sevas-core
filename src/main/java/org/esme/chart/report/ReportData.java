/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.chart.report;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author saheedalbert
 */
public class ReportData {

    Connection dbConn;
    ResultSet rs;
    Statement st;
    List<ChartModel> CHART_MODELS;

    public ReportData(Connection conn) {
        this.dbConn = conn;
    }

    // static {
    //}
    public List<ChartModel> getDailyNewSubscriptionData() {

        try {

            CHART_MODELS = new ArrayList<ChartModel>();
            st = this.dbConn.createStatement();
            rs = st.executeQuery(
                    "select date(sub_request_time) as theDate, "
                            + "count(*) as theCount from subscription "
                            + "where extract(month from sub_request_time) = extract (month from now()) \n"
                            + " and extract(year from sub_request_time) = extract (year from now()) "
                            + "and sub_service_id > 0 "
                            + "group by theDate order by theDate asc ");

            while (rs.next()) {
                Long value = rs.getLong("theCount");
                CHART_MODELS.add(new ChartModel(rs.getString("theDate"), value));
            }

        } catch (SQLException ex) {
            Logger.getLogger(ReportData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return CHART_MODELS;
    }

    public List<ChartModel> getDailyRevenueData() {

        try {

            CHART_MODELS = new ArrayList<ChartModel>();
            st = this.dbConn.createStatement();
            rs = st.executeQuery(
                    "select date(b.sent_time) as theDate,\n"
                            + "sum((CASE WHEN ((b.sender = a.welcome_shortcode) and (b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal'  or b.src_module ilike '%SubscriptionRenewalRetry%' )) THEN 1 ELSE 0 END) * a.service_price) AS main_charge,\n"
                            + "sum((CASE WHEN (b.sender = a.fallback_shortcode) and (b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal' or b.src_module ilike '%SubscriptionRenewalRetry%' ) THEN 1 ELSE 0 END) * a.fallback_price) AS fallback_charge,\n"
                            + "sum((CASE WHEN (b.sender = a.fallback2_shortcode) and (b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal' or b.src_module ilike '%SubscriptionRenewalRetry2%' ) THEN 1 ELSE 0 END) * a.fallback2_price::bigint) AS fallback_charge_2, \n"
                            + "sum((CASE WHEN (b.src_module = 'DailyPaidContent') THEN 1 ELSE 0 END) * a.service_price) AS daily_paid_content \n"
                            + "FROM subscription_service a, delivered_outgoing_messages b \n"
                            + "where extract(month from b.sent_time) = extract (month from now()) and extract(year from b.sent_time) = extract (year from now())\n"
                            + "and a.id = b.serv_id  group by theDate order by theDate asc");
            while (rs.next()) {
                Long value = rs.getLong("main_charge") + rs.getLong("fallback_charge") + rs.getLong("fallback_charge_2") + rs.getLong("daily_paid_content");
                CHART_MODELS.add(new ChartModel(rs.getString("theDate"), value));
            }

        } catch (SQLException ex) {
            Logger.getLogger(ReportData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return CHART_MODELS;
    }


    public List<ChartModel> getMonthlyRevenueData() {

        try {

            CHART_MODELS = new ArrayList<ChartModel>();
            st = this.dbConn.createStatement();
            rs = st.executeQuery(
                    "select extract(month from b.sent_time) as theMonth,\n"
                            + "sum((CASE WHEN ((b.sender = a.welcome_shortcode) and (b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal'  or b.src_module ilike '%SubscriptionRenewalRetry%' )) THEN 1 ELSE 0 END) * a.service_price) AS main_charge,\n"
                            + "sum((CASE WHEN (b.sender = a.fallback_shortcode) and (b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal' or b.src_module ilike '%SubscriptionRenewalRetry%' ) THEN 1 ELSE 0 END) * a.fallback_price) AS fallback_charge,\n"
                            + "sum((CASE WHEN (b.sender = a.fallback2_shortcode) and (b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal' or b.src_module ilike '%SubscriptionRenewalRetry2%' ) THEN 1 ELSE 0 END) * a.fallback2_price::bigint) AS fallback_charge_2, \n"
                            + "sum((CASE WHEN (b.src_module = 'DailyPaidContent') THEN 1 ELSE 0 END) * a.service_price) AS daily_paid_content \n"
                            + "FROM subscription_service a, delivered_outgoing_messages b \n"
                            + "where extract(year from b.sent_time) = extract (year from now())\n"
                            + "and a.id = b.serv_id  group by theMonth order by theMonth asc");
            while (rs.next()) {
                Long value = rs.getLong("main_charge") + rs.getLong("fallback_charge") + rs.getLong("fallback_charge_2") + rs.getLong("daily_paid_content");
                CHART_MODELS.add(new ChartModel(rs.getString("theMonth"), value));
            }

        } catch (SQLException ex) {
            Logger.getLogger(ReportData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return CHART_MODELS;
    }

    public List<ChartModel> getCustomDailyRevenueData(String startDate, String endDate) {

        try {
            CHART_MODELS = new ArrayList<ChartModel>();
            st = this.dbConn.createStatement();
            rs = st.executeQuery(
                    "select date(b.sent_time) as theDate,\n"
                            + "sum((CASE WHEN ((b.sender = a.welcome_shortcode) and (b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal'  or b.src_module ilike '%SubscriptionRenewalRetry%' )) THEN 1 ELSE 0 END) * a.service_price) AS main_charge,\n"
                            + "sum((CASE WHEN (b.sender = a.fallback_shortcode) and (b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal' or b.src_module ilike '%SubscriptionRenewalRetry%' ) THEN 1 ELSE 0 END) * a.fallback_price) AS fallback_charge,\n"
                            + "sum((CASE WHEN (b.sender = a.fallback2_shortcode) and (b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal' or b.src_module ilike '%SubscriptionRenewalRetry2%' ) THEN 1 ELSE 0 END) * a.fallback2_price::bigint) AS fallback_charge_2 \n"
                            + "FROM subscription_service a, delivered_outgoing_messages b \n"
                            + "where date(b.sent_time) between '" + startDate + "' and '" + endDate + "' and a.id = b.serv_id  group by theDate order by theDate asc");
            while (rs.next()) {
                Long value = rs.getLong("main_charge") + rs.getLong("fallback_charge") + rs.getLong("fallback_charge_2");
                CHART_MODELS.add(new ChartModel(rs.getString("theDate"), value));
            }

        } catch (SQLException ex) {
            Logger.getLogger(ReportData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return CHART_MODELS;
    }

}
