/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.chart.report;

import com.google.gson.Gson;
import org.esme.broker.AppBroker;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author saheedalbert
 */
public class MonthlyRevenueChart extends AppBroker {

    private static final long serialVersionUID = 1L;

    List<ChartModel> chartModels;
    ReportData xChartData;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            PrintWriter out = response.getWriter();

            xChartData = new ReportData(getDBConnection());
            //get data for XChart

            Gson gson = new Gson();
            String json = "";

            //get start date from request parameter
//            String sDate = request.getParameter("start");
            //get end date from request parameter
//            String eDate = request.getParameter("end");
            chartModels = xChartData.getMonthlyRevenueData();

//            if (sDate != null && eDate != null) {
//                org.esme.broker.AppBroker.sevasLogger.info("Start Date : " + sDate);
//                org.esme.broker.AppBroker.sevasLogger.info("End Date : " + eDate);
//                chartModels = xChartData.getCustomDailyRevenueData(sDate, eDate);
//            } else {
//                chartModels = xChartData.getMonthlyRevenueData();
//            }
            //Actual data should come from database
            //select data from database based on start date and end date
            //here I am neither going to fetch data from database nor fetch data based on date range
            //you need to manipulate those things from database
            List<ChartDto> chartDtos = new ArrayList<ChartDto>();

            for (ChartModel chartModel : chartModels) {
                ChartDto chartDto = new ChartDto(chartModel.getDate(), chartModel.getVisits());
                chartDtos.add(chartDto);
            }

            if (!chartDtos.isEmpty()) {
                //convert list of pojo model to json for plotting on graph
                json = gson.toJson(chartDtos);
            } else {
                json = "No record found";
            }

            org.esme.broker.AppBroker.sevasLogger.info(json);
            //write to the response
            response.getWriter().write(json);
        } catch (Exception E) {
            E.printStackTrace();
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
