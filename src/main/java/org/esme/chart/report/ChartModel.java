/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.chart.report;

/**
 * @author saheedalbert
 */
public class ChartModel {

    private String date;
    private Long count;

    public ChartModel(String date, Long visits) {
        this.date = date;
        this.count = visits;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getVisits() {
        return count;
    }

    public void setVisits(Long visits) {
        this.count = visits;
    }

}
