/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.dto;

/**
 * @author root
 */
public class ExtRoute {
    long id;
    String accessId;
    String type;
    String routeBaseURL;
    String shortcodeParam;
    String msisdnParam;
    String messageParam;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getRouteBaseURL() {
        return routeBaseURL;
    }

    public void setRouteBaseURL(String routeBaseURL) {
        this.routeBaseURL = routeBaseURL;
    }

    public String getShortcodeParam() {
        return shortcodeParam;
    }

    public void setShortcodeParam(String shortcodeParam) {
        this.shortcodeParam = shortcodeParam;
    }

    public String getMsisdnParam() {
        return msisdnParam;
    }

    public void setMsisdnParam(String msisdnParam) {
        this.msisdnParam = msisdnParam;
    }

    public String getMessageParam() {
        return messageParam;
    }

    public void setMessageParam(String messageParam) {
        this.messageParam = messageParam;
    }

}
