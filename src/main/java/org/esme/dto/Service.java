/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.dto;

import org.esme.dao.models.ServiceDao;

import java.util.ArrayList;

/**
 * @author TTMO
 */
public class Service {

    long id;
    String serviceDateCreated;
    long serviceOwnerID;
    String serviceSubType;
    String serviceName;
    String serviceShortCode;
    String serviceMTShortcode;
    String servicebillingShortcode;
    String serviceWelcomeMessageShortCode;
    String serviceKeyword;
    String serviceAllowableKeyword;
    String serviceCategory;
    String serviceCategoryKeyword;
    String serviceDescription;
    String servicePrice;
    String serviceSubscriptionMessage;
    String serviceUnsubscriptionMessage;
    String serviceReminderMessage;
    String serviceUnsubcribeKeyword;
    String servicePeriod;
    String autoSubToParentService;
    long parentServiceID;
    String serviceReminderPeriod;
    String timeOfBlast;
    String fallbackShortCode;
    String serviceContentDeliveryMethod;
    String serviceRenewalMessage;
    String serviceFallbackRenewalMessage;
    String serviceHelpMessage;
    String serviceNetwork;
    String seriviceBillingNetwork;
    String servicePromo;
    String servicePromoPeriod;
    String serviceStatus;
    String serviceCrossSell;
    String serviceCrossNetwork;
    String serviceCrossSellShortCode;
    String serviceCrossMessage;
    long billingRetryIntervals;
    long billingTime;
    long serviceSubscriptionReminderTime;
    long fallbackPeriod;
    long serviceReminderTime;
    String fallbackShortCode2;
    String fallbackPrice2;
    long fallbackPeriod2;
    String serviceFallbackRenewalMessage2;
    String fallbackShortCode3;
    String fallbackPrice3;
    long fallbackPeriod3;
    String serviceFallbackRenewalMessage3;
    String fallbackShortCode4;
    String fallbackPrice4;
    long fallbackPeriod4;
    String serviceFallbackRenewalMessage4;
    String MainbillingnotifySMSSetting;
    String fallBack1billingnotifySMSSetting;
    String fallBack2billingnotifySMSSetting;
    String fallBack3billingnotifySMSSetting;
    String fallBack4billingnotifySMSSetting;
    String instantBillingRetry;
    long fallbackPrice;
    private String serviceDoubleConfirmation;
    private String serviceDoubleConfirmationKeyword;
    private String serviceDoubleConfirmationMessage;
    private String serviceContentSenderID;
    //Routing..
    private String routeToExternalAPI;
    private String routeToExternalAPByShortCode;
    private String externalAPIURL;
    private String externalAPIShortCodeParam;
    private String externalAPIMSISDNParam;
    private String externalAPIMessageParam;

    // TODO: Remove completely
    public static Service getService(long serviceID) {
        return ServiceDao.getService(serviceID);

    }

    public static ArrayList<Service> getAllServices() {
        return ServiceDao.getAllServices();
    }

    public long getFallbackPrice() {
        return fallbackPrice;
    }

    public void setFallbackPrice(long fallbackPrice) {
        this.fallbackPrice = fallbackPrice;
    }

    public String getServiceDoubleConfirmationKeyword() {
        return serviceDoubleConfirmationKeyword;
    }

    public void setServiceDoubleConfirmationKeyword(String serviceDoubleConfirmationKeyword) {
        this.serviceDoubleConfirmationKeyword = serviceDoubleConfirmationKeyword;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getServiceSubType() {
        return serviceSubType;
    }

    public void setServiceSubType(String serviceSubType) {
        this.serviceSubType = serviceSubType;
    }

    public String getAutoSubToParentService() {
        return autoSubToParentService;
    }

    public void setAutoSubToParentService(String autoSubToParentService) {
        this.autoSubToParentService = autoSubToParentService;
    }

    public long getParentServiceID() {
        return parentServiceID;
    }

    public void setParentServiceID(long parentServiceID) {
        this.parentServiceID = parentServiceID;
    }

    public String getServiceCrossSell() {
        return serviceCrossSell;
    }

    public void setServiceCrossSell(String serviCrossSell) {
        this.serviceCrossSell = serviCrossSell;
    }

    public String getServiceCrossNetwork() {
        return serviceCrossNetwork;
    }

    public void setServiceCrossNetwork(String serviceCrossNetwork) {
        this.serviceCrossNetwork = serviceCrossNetwork;
    }

    public String getServiceCrossSellShortCode() {
        return serviceCrossSellShortCode;
    }

    public void setServiceCrossSellShortCode(String serviceCorssSellShortCode) {
        this.serviceCrossSellShortCode = serviceCorssSellShortCode;
    }

    public String getServiceCrossMessage() {
        return serviceCrossMessage;
    }

    public void setServiceCrossMessage(String serviceCrossMessage) {
        this.serviceCrossMessage = serviceCrossMessage;
    }

    public String getServicePromoPeriod() {
        return servicePromoPeriod;
    }

    public void setServicePromoPeriod(String servicePromoPeriod) {
        this.servicePromoPeriod = servicePromoPeriod;
    }

    public long getServiceOwnerID() {
        return serviceOwnerID;
    }

    public void setServiceOwnerID(long serviceOwnerID) {
        this.serviceOwnerID = serviceOwnerID;
    }

    public String getFallbackShortCode4() {
        return fallbackShortCode4;
    }

    public void setFallbackShortCode4(String fallbackShortCode4) {
        this.fallbackShortCode4 = fallbackShortCode4;
    }

    public String getFallbackPrice4() {
        return fallbackPrice4;
    }

    public void setFallbackPrice4(String fallbackPrice4) {
        this.fallbackPrice4 = fallbackPrice4;
    }

    public long getFallbackPeriod4() {
        return fallbackPeriod4;
    }

    public void setFallbackPeriod4(long fallbackPeriod4) {
        this.fallbackPeriod4 = fallbackPeriod4;
    }

    public String getServiceFallbackRenewalMessage4() {
        return serviceFallbackRenewalMessage4;
    }

    public void setServiceFallbackRenewalMessage4(String serviceFallbackRenewalMessage4) {
        this.serviceFallbackRenewalMessage4 = serviceFallbackRenewalMessage4;
    }

    public String getMainbillingnotifySMSSetting() {
        return MainbillingnotifySMSSetting;
    }

    public void setMainbillingnotifySMSSetting(String MainbillingnotifySMSSetting) {
        this.MainbillingnotifySMSSetting = MainbillingnotifySMSSetting;
    }

    public String getFallBack1billingnotifySMSSetting() {
        return fallBack1billingnotifySMSSetting;
    }

    public void setFallBack1billingnotifySMSSetting(String fallBack1billingnotifySMSSetting) {
        this.fallBack1billingnotifySMSSetting = fallBack1billingnotifySMSSetting;
    }

    public String getFallBack2billingnotifySMSSetting() {
        return fallBack2billingnotifySMSSetting;
    }

    public void setFallBack2billingnotifySMSSetting(String fallBack2billingnotifySMSSetting) {
        this.fallBack2billingnotifySMSSetting = fallBack2billingnotifySMSSetting;
    }

    public String getFallBack3billingnotifySMSSetting() {
        return fallBack3billingnotifySMSSetting;
    }

    public void setFallBack3billingnotifySMSSetting(String fallBack3billingnotifySMSSetting) {
        this.fallBack3billingnotifySMSSetting = fallBack3billingnotifySMSSetting;
    }

    public String getFallBack4billingnotifySMSSetting() {
        return fallBack4billingnotifySMSSetting;
    }

    public void setFallBack4billingnotifySMSSetting(String fallBack4billingnotifySMSSetting) {
        this.fallBack4billingnotifySMSSetting = fallBack4billingnotifySMSSetting;
    }

    public String getServiceShortCode() {
        return serviceShortCode;
    }

    public void setServiceShortCode(String serviceShortCode) {
        this.serviceShortCode = serviceShortCode;
    }

    public String getServiceMTShortcode() {
        return serviceMTShortcode;
    }

    public void setServiceMTShortcode(String serviceMTShortcode) {
        this.serviceMTShortcode = serviceMTShortcode;
    }

    public String getServicebillingShortcode() {
        return servicebillingShortcode;
    }

    public void setServicebillingShortcode(String servicebillingShortcode) {
        this.servicebillingShortcode = servicebillingShortcode;
    }

    public String getServiceUnsubscriptionMessage() {
        return serviceUnsubscriptionMessage;
    }

    public void setServiceUnsubscriptionMessage(String serviceUnsubscriptionMessage) {
        this.serviceUnsubscriptionMessage = serviceUnsubscriptionMessage;
    }

    public String getServiceUnsubcribeKeyword() {
        return serviceUnsubcribeKeyword;
    }

    public void setServiceUnsubcribeKeyword(String serviceUnsubcribeKeyword) {
        this.serviceUnsubcribeKeyword = serviceUnsubcribeKeyword;
    }

    public String getInstantBillingRetry() {
        return instantBillingRetry;
    }

    public void setInstantBillingRetry(String instantRetry) {
        this.instantBillingRetry = instantRetry;
    }

    public String getServiceBillingNetwork() {
        return seriviceBillingNetwork;
    }

    public void setSeriviceBillingNetwork(String seriviceBillingNetwork) {
        this.seriviceBillingNetwork = seriviceBillingNetwork;
    }

    public String getServicePromo() {
        return servicePromo;
    }

    public void setServicePromo(String servicePromo) {
        this.servicePromo = servicePromo;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getServiceWelcomeMessageShortCode() {
        return serviceWelcomeMessageShortCode;
    }

    public void setServiceWelcomeMessageShortCode(String serviceWelcomeMessageShortCode) {
        this.serviceWelcomeMessageShortCode = serviceWelcomeMessageShortCode;
    }

    public String getServiceNetwork() {
        return serviceNetwork;
    }

    public void setServiceNetwork(String serviceNetwork) {
        this.serviceNetwork = serviceNetwork;
    }

    public String getServiceHelpMessage() {
        return serviceHelpMessage;
    }

    public void setServiceHelpMessage(String serviceHelpMessage) {
        this.serviceHelpMessage = serviceHelpMessage;
    }

    public String getServiceFallbackRenewalMessage() {
        return serviceFallbackRenewalMessage;
    }

    public void setServiceFallbackRenewalMessage(String serviceFallbackRenewalMessage) {
        this.serviceFallbackRenewalMessage = serviceFallbackRenewalMessage;
    }

    public String getServiceRenewalMessage() {
        return serviceRenewalMessage;
    }

    public void setServiceRenewalMessage(String serviceRenewalMessage) {
        this.serviceRenewalMessage = serviceRenewalMessage;
    }

    public long getServiceReminderTime() {
        return serviceReminderTime;
    }

    public void setServiceReminderTime(long serviceReminderTime) {
        this.serviceReminderTime = serviceReminderTime;
    }

    public long getFallbackPeriod() {
        return fallbackPeriod;
    }

    public void setFallbackPeriod(long fallbackPeriod) {
        this.fallbackPeriod = fallbackPeriod;
    }

    public long getServiceSubscriptionReminderTime() {
        return serviceSubscriptionReminderTime;
    }

    public void setServiceSubscriptionReminderTime(long serviceSubscriptionReminderTime) {
        this.serviceSubscriptionReminderTime = serviceSubscriptionReminderTime;
    }

    public long getBillingTime() {
        return billingTime;
    }

    public void setBillingTime(long billingTime) {
        this.billingTime = billingTime;
    }

    public long getBillingRetryIntervals() {
        return billingRetryIntervals;
    }

    public void setBillingRetryIntervals(long billingRetryIntervals) {
        this.billingRetryIntervals = billingRetryIntervals;
    }

    public String getServiceContentDeliveryMethod() {
        return serviceContentDeliveryMethod;
    }

    public void setServiceContentDeliveryMethod(String serviceContentDeliveryMethod) {
        this.serviceContentDeliveryMethod = serviceContentDeliveryMethod;
    }

    public String getServiceDateCreated() {
        return serviceDateCreated;
    }

    public void setServiceDateCreated(String date_created) {
        this.serviceDateCreated = date_created;
    }

    public String getFallbackShortCode() {
        return fallbackShortCode;
    }

    public void setFallbackShortCode(String fallbackShortCode) {
        this.fallbackShortCode = fallbackShortCode;
    }

    public String getServiceCategoryKeyword() {
        return serviceCategoryKeyword;
    }

    public void setServiceCategoryKeyword(String serviceCategoryKeyword) {
        this.serviceCategoryKeyword = serviceCategoryKeyword;
    }

    public String getServiceBillingShortCode() {
        return servicebillingShortcode;
    }

    public void setServiceBillingShortCode(String service_wc_shortcode) {
        this.servicebillingShortcode = service_wc_shortcode;
    }

    public String getTimeOfBlast() {
        return timeOfBlast;
    }

    public void setTimeOfBlast(String timeOfBlast) {
        this.timeOfBlast = timeOfBlast;
    }

    public String getServiceMTShortCode() {
        return serviceMTShortcode;
    }

    public void setServiceMTShortCode(String service_mt_shortcode) {
        this.serviceMTShortcode = service_mt_shortcode;
    }

    public String getServiceSubscriptionMessage() {
        return serviceSubscriptionMessage;
    }

    public void setServiceSubscriptionMessage(String service_subscription_msg) {
        this.serviceSubscriptionMessage = service_subscription_msg;
    }

    public String getServiceUnSubscriptionMessage() {
        return serviceUnsubscriptionMessage;
    }

    public void setServiceUnSubScriptionMessage(String service_unsubscription_msg) {
        this.serviceUnsubscriptionMessage = service_unsubscription_msg;
    }

    public String getServicePeriod() {
        return servicePeriod;
    }

    public void setServicePeriod(String service_period) {
        this.servicePeriod = service_period;
    }

    public String getServiceReminderMessage() {
        return serviceReminderMessage;
    }

    public void setServiceReminderMessage(String service_reminder_msg) {
        this.serviceReminderMessage = service_reminder_msg;
    }

    public String getServiceUnSubscribeKeyword() {
        return serviceUnsubcribeKeyword;
    }

    public void setServiceUnSubscribeKeyword(String service_unsubcribe_keyword) {
        this.serviceUnsubcribeKeyword = service_unsubcribe_keyword;
    }

    public String getServiceReminderPeriod() {
        return serviceReminderPeriod;
    }

    public void setServiceReminderPeriod(String service_reminder_period) {
        this.serviceReminderPeriod = service_reminder_period;
    }

    public String getDateCreated() {
        return serviceDateCreated;
    }

    public void setDateCreated(String date_created) {
        this.serviceDateCreated = date_created;
    }

    public long getServiceID() {
        return id;
    }

    public void setServiceID(long id) {
        this.id = id;
    }

    public String getServiceAllowableKeyword() {
        return serviceAllowableKeyword;
    }

    public void setServiceAllowableKeyword(String service_allowable_keyword) {
        this.serviceAllowableKeyword = service_allowable_keyword;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String service_category) {
        this.serviceCategory = service_category;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String service_description) {
        this.serviceDescription = service_description;
    }

    public String getServiceKeyword() {
        return serviceKeyword;
    }

    public void setServiceKeyword(String service_keyword) {
        this.serviceKeyword = service_keyword;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String service_name) {
        this.serviceName = service_name;
    }

    public long getServiceOwner() {
        return serviceOwnerID;
    }

    public void setServiceOwner(long service_owner) {
        this.serviceOwnerID = service_owner;
    }

    public String getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(String service_price) {
        this.servicePrice = service_price;
    }

    public String getServiceShortcode() {
        return serviceShortCode;
    }

    public void setServiceShortcode(String service_shortcode) {
        this.serviceShortCode = service_shortcode;
    }

    public String getServiceWelcomeMessageShortcode() {
        return serviceWelcomeMessageShortCode;
    }

    public void setServiceWelcomeMessageShortcode(String serviceWelcomeSC) {
        this.serviceWelcomeMessageShortCode = serviceWelcomeSC;
    }

    public String getFallbackShortCode2() {
        return fallbackShortCode2;
    }

    public void setFallbackShortCode2(String fallbackShortCode2) {
        this.fallbackShortCode2 = fallbackShortCode2;
    }

    public String getFallbackPrice2() {
        return fallbackPrice2;
    }

    public void setFallbackPrice2(String fallbackPrice2) {
        this.fallbackPrice2 = fallbackPrice2;
    }

    public long getFallbackPeriod2() {
        return fallbackPeriod2;
    }

    public void setFallbackPeriod2(long fallbackPeriod2) {
        this.fallbackPeriod2 = fallbackPeriod2;
    }

    public String getServiceFallbackRenewalMessage2() {
        return serviceFallbackRenewalMessage2;
    }

    public void setServiceFallbackRenewalMessage2(String serviceFallbackRenewalMessage2) {
        this.serviceFallbackRenewalMessage2 = serviceFallbackRenewalMessage2;
    }

    public String getFallbackShortCode3() {
        return fallbackShortCode3;
    }

    public void setFallbackShortCode3(String fallbackShortCode3) {
        this.fallbackShortCode3 = fallbackShortCode3;
    }

    public String getFallbackPrice3() {
        return fallbackPrice3;
    }

    public void setFallbackPrice3(String fallbackPrice3) {
        this.fallbackPrice3 = fallbackPrice3;
    }

    public long getFallbackPeriod3() {
        return fallbackPeriod3;
    }

    public void setFallbackPeriod3(long fallbackPeriod3) {
        this.fallbackPeriod3 = fallbackPeriod3;
    }

    public String getServiceFallbackRenewalMessage3() {
        return serviceFallbackRenewalMessage3;
    }

    public void setServiceFallbackRenewalMessage3(String serviceFallbackRenewalMessage3) {
        this.serviceFallbackRenewalMessage3 = serviceFallbackRenewalMessage3;
    }

    public String getRouteToExternalAPI() {
        return routeToExternalAPI;
    }

    public void setRouteToExternalAPI(String routeToExternalAPI) {
        this.routeToExternalAPI = routeToExternalAPI;
    }

    public String getRouteToExternalAPByShortCode() {
        return routeToExternalAPByShortCode;
    }

    public void setRouteToExternalAPByShortCode(String routeToExternalAPByShortCode) {
        this.routeToExternalAPByShortCode = routeToExternalAPByShortCode;
    }

    public String getExternalAPIURL() {
        return externalAPIURL;
    }

    public void setExternalAPIURL(String externalAPIURL) {
        this.externalAPIURL = externalAPIURL;
    }

    public String getExternalAPIShortCodeParam() {
        return externalAPIShortCodeParam;
    }

    public void setExternalAPIShortCodeParam(String externalAPIShortCodeParam) {
        this.externalAPIShortCodeParam = externalAPIShortCodeParam;
    }

    public String getExternalAPIMSISDNParam() {
        return externalAPIMSISDNParam;
    }

    public void setExternalAPIMSISDNParam(String externalAPIMSISDNParam) {
        this.externalAPIMSISDNParam = externalAPIMSISDNParam;
    }

    public String getExternalAPIMessageParam() {
        return externalAPIMessageParam;
    }

    public void setExternalAPIMessageParam(String externalAPIMessageParam) {
        this.externalAPIMessageParam = externalAPIMessageParam;
    }

    public String getServiceDoubleConfirmation() {
        return serviceDoubleConfirmation;
    }

    public void setServiceDoubleConfirmation(String serviceDoubleConfirmation) {
        this.serviceDoubleConfirmation = serviceDoubleConfirmation;
    }

    public String getServiceDoubleConfirmationMessage() {
        return serviceDoubleConfirmationMessage;
    }

    public void setServiceDoubleConfirmationMessage(String serviceDoubleConfirmationMessage) {
        this.serviceDoubleConfirmationMessage = serviceDoubleConfirmationMessage;
    }

    public String getServiceContentSenderID() {
        return serviceContentSenderID;
    }

    public void setServiceContentSenderID(String serviceContentSenderID) {
        this.serviceContentSenderID = serviceContentSenderID;
    }

}
