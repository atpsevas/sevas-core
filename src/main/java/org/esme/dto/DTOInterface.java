package org.esme.dto;

import java.util.ArrayList;

// TODO: Abstract Data
public interface DTOInterface {

    public DTOInterface getOne();

    public ArrayList<DTOInterface> getAll();

    // TODO: Determine count dynamics
    public long count();

    // Define custom query to retrieve DTOInterface from JDBC
    public DTOInterface get(String query);

    // TODO: Determine return value of Insert, Delete and Update from JDBC
    public void save();

    public void delete();

}
