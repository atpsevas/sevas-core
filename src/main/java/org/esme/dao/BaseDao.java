/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.dao;

import java.sql.ResultSet;

/**
 * @author TTMO
 */
public interface BaseDao {

    void save(BaseDao object);

    Object getOne();

    Object getAll();

    Object extractObjectFromResultSet(ResultSet resultSet, Object object);

}
