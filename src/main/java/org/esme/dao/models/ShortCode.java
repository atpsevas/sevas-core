
package org.esme.dao.models;

public class ShortCode {

    private long id;
    private String shortCodeID;
    private String dateCreated;

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public long getID() {
        return id;
    }

    public void setID(long shortCodeID) {
        this.id = shortCodeID;
    }

    public String getShortCodeID() {
        return shortCodeID;
    }

    public void setShortCodeID(String shortCodeNumber) {
        this.shortCodeID = shortCodeNumber;
    }
}
