/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.dao.models;

/**
 * @author salbert
 */
public class InActiveSubscription {

    private long subscriberServiceID;
    private long subscriberServiceOwnerID;
    private long subscriberServicePeriod;
    private String subscriberServiceTimeOfBlast;
    private String subscriberServiceRequestTime;
    private String subscriberServiceName;
    private String subscriberMSISDN;
    private String subscriberNetwork;
    private String subscriberServiceMOShortCode;
    private String subscriberServiceMTShortCode;
    private String subscriberWelcomeShortCode;
    private String subscriberServiceExpiryDate;
    private String subscriberServiceUnsubscribeKeyword;
    private String subscriberUnsubMessage;
    private String subscriberReminderMessage;
    private String subscriberServiceCategoryKeuword;

    public String getSubscriberReminderMessage() {
        return subscriberReminderMessage;
    }

    public void setSubscriberReminderMessage(String subscriberReminderMessage) {
        this.subscriberReminderMessage = subscriberReminderMessage;
    }

    public String getSubscriberUnsubMessage() {
        return subscriberUnsubMessage;
    }

    public void setSubscriberUnsubMessage(String subscriberUnsubMessage) {
        this.subscriberUnsubMessage = subscriberUnsubMessage;
    }

    public String getSubscriberServiceCategoryKeuword() {
        return subscriberServiceCategoryKeuword;
    }

    public void setSubscriberServiceCategoryKeyword(String subscriberServiceCategoryKeuword) {
        this.subscriberServiceCategoryKeuword = subscriberServiceCategoryKeuword;
    }

    public String getSubscriberServiceUnsubscribeKeyword() {
        return subscriberServiceUnsubscribeKeyword;
    }

    public void setSubscriberServiceUnsubscribeKeyword(String subscriberServiceUnsubscribeKeyword) {
        this.subscriberServiceUnsubscribeKeyword = subscriberServiceUnsubscribeKeyword;
    }


    public String getSubscriberMSISDN() {
        return subscriberMSISDN;
    }

    public void setSubscriberMSISDN(String subscriberMSISDN) {
        this.subscriberMSISDN = subscriberMSISDN;
    }

    public String getSubscriberService() {
        return subscriberServiceName;
    }

    public void setSubscriberService(String subscriberService) {
        this.subscriberServiceName = subscriberService;
    }

    public String getSubscriberServiceExpiryDate() {
        return subscriberServiceExpiryDate;
    }

    public void setSubscriberServiceExpiryDate(String subscriberServiceExpiryDate) {
        this.subscriberServiceExpiryDate = subscriberServiceExpiryDate;
    }

    public long getSubscriberServiceID() {
        return subscriberServiceID;
    }

    public void setSubscriberServiceID(long subscriberServiceID) {
        this.subscriberServiceID = subscriberServiceID;
    }

    public String getSubscriberServiceMOShortCode() {
        return subscriberServiceMOShortCode;
    }

    public void setSubscriberServiceMOShortCode(String subscriberServiceMOShortCode) {
        this.subscriberServiceMOShortCode = subscriberServiceMOShortCode;
    }

    public String getSubscriberServiceMTShortCode() {
        return subscriberServiceMTShortCode;
    }

    public void setSubscriberServiceMTShortCode(String subscriberServiceMTShortCode) {
        this.subscriberServiceMTShortCode = subscriberServiceMTShortCode;
    }

    public String getSubscriberNetwork() {
        return subscriberNetwork;
    }

    public void setSubscriberNetwork(String subscriberNetwork) {
        this.subscriberNetwork = subscriberNetwork;
    }

    public String getSubscriberServiceName() {
        return subscriberServiceName;
    }

    public void setSubscriberServiceName(String subscriberServiceName) {
        this.subscriberServiceName = subscriberServiceName;
    }

    public long getSubscriberServiceOwnerID() {
        return subscriberServiceOwnerID;
    }

    public void setSubscriberServiceOwnerID(long subscriberServiceOwnerID) {
        this.subscriberServiceOwnerID = subscriberServiceOwnerID;
    }

    public long getSubscriberServicePeriod() {
        return subscriberServicePeriod;
    }

    public void setSubscriberServicePeriod(long subscriberServicePeriod) {
        this.subscriberServicePeriod = subscriberServicePeriod;
    }

    public String getSubscriberServiceRequestTime() {
        return subscriberServiceRequestTime;
    }

    public void setSubscriberServiceRequestTime(String subscriberServiceRequestTime) {
        this.subscriberServiceRequestTime = subscriberServiceRequestTime;
    }

    public String getSubscriberServiceTimeOfBlast() {
        return subscriberServiceTimeOfBlast;
    }

    public void setSubscriberServiceTimeOfBlast(String subscriberServiceTimeOfBlast) {
        this.subscriberServiceTimeOfBlast = subscriberServiceTimeOfBlast;
    }

    public String getSubscriberWelcomeShortCode() {
        return subscriberWelcomeShortCode;
    }

    public void setSubscriberWelcomeShortCode(String subscriberWelcomeShortCode) {
        this.subscriberWelcomeShortCode = subscriberWelcomeShortCode;
    }
}
