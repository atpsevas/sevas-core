/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.esme.dao.models;

/**
 * @author saheedalbert
 */
public class MonthlyRevPerformance {

    private String theMonth;
    private long totalRevenue;
    private long mainCharged;
    private long fallBackCharged;

    public long getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(long totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public String getTheMonth() {
        return theMonth;
    }

    public void setTheMonth(String serviceName) {
        this.theMonth = serviceName;
    }

    public long getMainCharged() {
        return mainCharged;
    }

    public void setMainCharged(long mainCharged) {
        this.mainCharged = mainCharged;
    }

    public long getFallBackCharged() {
        return fallBackCharged;
    }

    public void setFallBackCharged(long fallBackCharged) {
        this.fallBackCharged = fallBackCharged;
    }
}
