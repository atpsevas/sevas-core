/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.dao.models;

/**
 * @author salbert
 */
public class ActiveSubscriptionQueryReport {

    private String date;
    private String serviceName;
    private String servicePrice;
    private String serviceShortCode;
    private String serviceFallbackShortCode;
    private String serviceFallbackPrice;
    private long totalSubscription;
    private long revenue;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getRevenue() {
        return revenue;
    }

    public void setRevenue(long revenue) {
        this.revenue = revenue;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(String servicePrice) {
        this.servicePrice = servicePrice;
    }

    public long getTotalSubscription() {
        return totalSubscription;
    }

    public void setTotalSubscription(long totalSubscription) {
        this.totalSubscription = totalSubscription;
    }

    public String getServiceFallbackPrice() {
        return serviceFallbackPrice;
    }

    public void setServiceFallbackPrice(String serviceFallbackPrice) {
        this.serviceFallbackPrice = serviceFallbackPrice;
    }

    public String getServiceFallbackShortCode() {
        return serviceFallbackShortCode;
    }

    public void setServiceFallbackShortCode(String serviceFallbackShortCode) {
        this.serviceFallbackShortCode = serviceFallbackShortCode;
    }

    public String getServiceShortCode() {
        return serviceShortCode;
    }

    public void setServiceShortCode(String serviceShortCode) {
        this.serviceShortCode = serviceShortCode;
    }
}
