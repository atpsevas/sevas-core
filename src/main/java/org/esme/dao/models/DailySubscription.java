
package org.esme.dao.models;

/**
 * @author saheedalbert
 */
public class DailySubscription extends Subscription {

    private long newSub;
    private long newActiveSub;
    private long newInActiveSub;
    private String subDate;

    public String getSubDate() {
        return subDate;
    }

    public void setSubDate(String subDate) {
        this.subDate = subDate;
    }

    public long getNewSub() {
        return newSub;
    }

    public void setNewSub(long newSub) {
        this.newSub = newSub;
    }

    public long getNewActiveSub() {
        return newActiveSub;
    }

    public void setNewActiveSub(long newActiveSub) {
        this.newActiveSub = newActiveSub;
    }

    public long getNewInActiveSub() {
        return newInActiveSub;
    }

    public void setNewInActiveSub(long newInActiveSub) {
        this.newInActiveSub = newInActiveSub;
    }
}
