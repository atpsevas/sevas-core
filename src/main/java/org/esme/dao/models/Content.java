/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.dao.models;

import org.esme.broker.AppBroker;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.esme.broker.AppBroker.sevasLogger;

/**
 * @author TTMO
 */
public class Content {

    long content_id;
    String dateUploaded;
    long contentOwner;
    long contentServiceID;
    String contentServiceName;
    String content;
    String conentDOB;
    String contentType;

    public static ArrayList<Content> getAllTodayContent() {
        ArrayList<Content> todaysContent = new ArrayList<Content>();
        ;

        try {

            Statement statement = null;
            ResultSet resultSet = null;

            statement = AppBroker.getDBConnection(AppBroker.getPropertyFileHandle()).createStatement();
            String query = "select distinct on(a.content_service_id) a.*, b.service_keyword from content a, "
                    + "subscription_service b where b.id = a.content_service_id and a.content_date_of_blast::date = date(now())";

            sevasLogger.info(query);

            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {

                Content _content = new Content();

                _content.setContent_id(resultSet.getInt("content_id"));
                _content.setContentServiceID(resultSet.getLong("content_service_id"));
                _content.setConentDOB(resultSet.getString("content_date_of_blast"));
                _content.setContent(resultSet.getString("content"));
                _content.setContentType(resultSet.getString("content_type"));
                _content.setContentServiceName(resultSet.getString("service_keyword"));
                _content.setContentOwner(1);
                _content.setDateUploaded(resultSet.getString("content_upload_date"));
                _content.setContentService(resultSet.getLong("content_service_id"));

                todaysContent.add(_content);

            }
        } catch (SQLException ex) {
            Logger.getLogger(Content.class.getName()).log(Level.SEVERE, null, ex);
        }

        return todaysContent;

    }

    public static Content todayContent(Long servId) {

        Content _content = new Content();

        sevasLogger.info("Attempting to retrieve content");

        try {

            Statement statement = null;
            ResultSet resultSet = null;

            statement = AppBroker.getDBConnection(AppBroker.getPropertyFileHandle()).createStatement();
            String query = "select a.*, b.service_keyword from content a, subscription_service b "
                    + "where b.id = a.content_service_id and a.content_date_of_blast::date = date(now()) and content_service_id =" + servId;
            resultSet = statement.executeQuery(query);

            sevasLogger.info(query);

            while (resultSet.next()) {

                _content.setContent_id(resultSet.getInt("content_id"));
                _content.setContentServiceID(resultSet.getLong("content_service_id"));
                _content.setConentDOB(resultSet.getString("content_date_of_blast"));
                _content.setContent(resultSet.getString("content"));
                _content.setContentType(resultSet.getString("content_type"));
                _content.setContentServiceName(resultSet.getString("service_keyword"));
                _content.setContentOwner(1);
                _content.setDateUploaded(resultSet.getString("content_upload_date"));
                _content.setContentService(resultSet.getLong("content_service_id"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(Content.class.getName()).log(Level.SEVERE, null, ex);
        }

        return _content;

    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public long getContentServiceID() {
        return contentServiceID;
    }

    public void setContentServiceID(long contentServiceID) {
        this.contentServiceID = contentServiceID;
    }

    public String getContentServiceName() {
        return contentServiceName;
    }

    public void setContentServiceName(String contentServiceName) {
        this.contentServiceName = contentServiceName;
    }

    public String getConentDOB() {
        return conentDOB;
    }

    public void setConentDOB(String conentDOB) {
        this.conentDOB = conentDOB;
    }

    public long getContent_id() {
        return content_id;
    }

    public void setContent_id(long conten_id) {
        this.content_id = conten_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getContentOwner() {
        return contentOwner;
    }

    public void setContentOwner(long contentOwner) {
        this.contentOwner = contentOwner;
    }

    public long getContentService() {
        return contentServiceID;
    }

    public void setContentService(long contentService) {
        this.contentServiceID = contentService;
    }

    public String getDateUploaded() {
        return dateUploaded;
    }

    public void setDateUploaded(String dateUploaded) {
        this.dateUploaded = dateUploaded;
    }

}
