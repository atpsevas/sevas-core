package org.esme.dao.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SubscriptionLogs {

    SubscriptionLogs log;
    ArrayList<SubscriptionLogs> logs;
    private String requestTime;
    private String network;
    private String shortCode;
    private String service;
    private String msisdn;
    private String requestText;
    private String responseText;
    private String activeTill;

    @SuppressWarnings("CallToThreadDumpStack")
    public ArrayList<SubscriptionLogs> queryLogs(Connection DBConnection, long userID) {
        logs = new ArrayList<SubscriptionLogs>();
        ResultSet rsPagination = null;
        PreparedStatement psPagination = null;
        String sqlPagination;
        try {
            if (userID == 0) {
                sqlPagination = "SELECT a.service_name, b.sub_request_time,sub_request_text,"
                        + "sub_network,sub_shortcode,sub_msisdn, "
                        + "sub_response_text, b.sub_expiry_date FROM subscription_service a, "
                        + "subscription b where date(b.sub_request_time) = date(CURRENT_TIMESTAMP) "
                        + "and extract(month from b.sub_request_time) = extract (month from now()) "
                        + "and extract(year from b.sub_request_time) = extract (year from now()) "
                        + "and a.id = b.sub_service_id ORDER BY sub_request_time desc ";
            } else {
                sqlPagination = "SELECT a.service_name, b.sub_request_time,sub_request_text,"
                        + "sub_network,sub_shortcode,sub_msisdn, "
                        + "sub_response_text, b.sub_expiry_date FROM subscription_service a, "
                        + "subscription b where date(b.sub_request_time) = date(CURRENT_TIMESTAMP) "
                        + "and extract(month from b.sub_request_time) = extract (month from now()) "
                        + "and extract(year from b.sub_request_time) = extract (year from now()) "
                        + "and a.id = b.sub_service_id and a.service_owner = " + userID + " ORDER BY sub_request_time desc ";
            }
            psPagination = DBConnection.prepareStatement(sqlPagination);
            rsPagination = psPagination.executeQuery();

            while (rsPagination.next()) {
                log = new SubscriptionLogs();
                log.setRequestTime(rsPagination.getString("sub_request_time"));
                log.setService(rsPagination.getString("service_name"));
                log.setRequestText(rsPagination.getString("sub_request_text"));
                log.setNetwork(rsPagination.getString("sub_network"));
                log.setShortCode(rsPagination.getString("sub_shortcode"));
                log.setMsisdn(rsPagination.getString("sub_msisdn"));
                log.setResponseText(rsPagination.getString("sub_response_text"));
                log.setActiveTill(rsPagination.getString("sub_expiry_date"));
                logs.add(log);
            }

        } catch (SQLException ex) {

        } finally {
            try {
                psPagination.close();
                rsPagination.close();
            } catch (SQLException ex) {
            }
        }
        return logs;
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public ArrayList<SubscriptionLogs> customQueryLogs(
            Connection DBConnection, String startDate,
            String endDate, long userID) {

        logs = new ArrayList<SubscriptionLogs>();
        ResultSet rsPagination = null;
        PreparedStatement psPagination = null;

        try {
            String sqlPagination;
            if (userID == 0) {
                sqlPagination = "SELECT a.service_name, b.sub_request_time,sub_request_text,"
                        + "sub_network,sub_shortcode,sub_msisdn, sub_response_text, b.sub_expiry_date "
                        + "FROM subscription_service a, subscription b "
                        + "where date(b.sub_request_time) between '" + startDate + "' and '" + endDate + "' "
                        + "AND a.id = b.sub_service_id ORDER BY sub_request_time desc ";
            } else {
                sqlPagination = "SELECT a.service_name, b.sub_request_time,sub_request_text,"
                        + "sub_network,sub_shortcode,sub_msisdn, sub_response_text, b.sub_expiry_date "
                        + "FROM subscription_service a, subscription b "
                        + "where date(b.sub_request_time) between '" + startDate + "' and '" + endDate + "' "
                        + "AND a.id = b.sub_service_id AND a.service_owner =" + userID + " ORDER BY sub_request_time desc ";
            }

            psPagination = DBConnection.prepareStatement(sqlPagination);
            rsPagination = psPagination.executeQuery();

            while (rsPagination.next()) {
                log = new SubscriptionLogs();
                log.setRequestTime(rsPagination.getString("sub_request_time"));
                log.setService(rsPagination.getString("service_name"));
                log.setRequestText(rsPagination.getString("sub_request_text"));
                log.setNetwork(rsPagination.getString("sub_network"));
                log.setShortCode(rsPagination.getString("sub_shortcode"));
                log.setMsisdn(rsPagination.getString("sub_msisdn"));
                log.setResponseText(rsPagination.getString("sub_response_text"));
                log.setActiveTill(rsPagination.getString("sub_expiry_date"));
                logs.add(log);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                psPagination.close();
                rsPagination.close();
            } catch (SQLException ex) {
            }
        }
        return logs;
    }

    public String getActiveTill() {
        return activeTill;
    }

    public void setActiveTill(String activeTill) {
        this.activeTill = activeTill;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getRequestText() {
        return requestText;
    }

    public void setRequestText(String requestText) {
        this.requestText = requestText;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }
}
