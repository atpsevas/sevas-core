package org.esme.dao.models;

public class SMSCampaign {

    private long campaignID;
    private String campaignUploadDate;
    private String campaignSenderID;
    private String campaignContent;
    private String campaignTargetFileDir;
    private String campaignSentDate;
    private long campaignSentTime;
    private long campaignOnwerID;
    private long campaignTotalTarget;
    private long campaignTotalSent;
    private String campaignActualStartTime;
    private String campaignActualEndTime;
    private String campaignStatus;
    private long campaignServiceID;

    public String getCampaignActualEndTime() {
        return campaignActualEndTime;
    }

    public void setCampaignActualEndTime(String campaignActualEndTime) {
        this.campaignActualEndTime = campaignActualEndTime;
    }

    public String getCampaignActualStartTime() {
        return campaignActualStartTime;
    }

    public void setCampaignActualStartTime(String campaignActualStartTime) {
        this.campaignActualStartTime = campaignActualStartTime;
    }

    public String getCampaignContent() {
        return campaignContent;
    }

    public void setCampaignContent(String campaignContent) {
        this.campaignContent = campaignContent;
    }

    public long getCampaignID() {
        return campaignID;
    }

    public void setCampaignID(long campaignID) {
        this.campaignID = campaignID;
    }

    public long getCampaignOnwerID() {
        return campaignOnwerID;
    }

    public void setCampaignOnwerID(long campaignOnwerID) {
        this.campaignOnwerID = campaignOnwerID;
    }

    public String getCampaignSenderID() {
        return campaignSenderID;
    }

    public void setCampaignSenderID(String campaignSenderID) {
        this.campaignSenderID = campaignSenderID;
    }

    public String getCampaignSentDate() {
        return campaignSentDate;
    }

    public void setCampaignSentDate(String campaignSentDate) {
        this.campaignSentDate = campaignSentDate;
    }

    public long getCampaignSentTime() {
        return campaignSentTime;
    }

    public void setCampaignSentTime(long campaignSentTime) {
        this.campaignSentTime = campaignSentTime;
    }

    public long getCampaignServiceID() {
        return campaignServiceID;
    }

    public void setCampaignServiceID(long campaignServiceID) {
        this.campaignServiceID = campaignServiceID;
    }

    public String getCampaignStatus() {
        return campaignStatus;
    }

    public void setCampaignStatus(String campaignStatus) {
        this.campaignStatus = campaignStatus;
    }

    public String getCampaignTargetFileDir() {
        return campaignTargetFileDir;
    }

    public void setCampaignTargetFileDir(String campaignTargetFileDir) {
        this.campaignTargetFileDir = campaignTargetFileDir;
    }

    public long getCampaignTotalSent() {
        return campaignTotalSent;
    }

    public void setCampaignTotalSent(long campaignTotalSent) {
        this.campaignTotalSent = campaignTotalSent;
    }

    public long getCampaignTotalTarget() {
        return campaignTotalTarget;
    }

    public void setCampaignTotalTarget(long campaignTotalTarget) {
        this.campaignTotalTarget = campaignTotalTarget;
    }

    public String getCampaignUploadDate() {
        return campaignUploadDate;
    }

    public void setCampaignUploadDate(String campaignUploadDate) {
        this.campaignUploadDate = campaignUploadDate;
    }
}
