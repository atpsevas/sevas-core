/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.dao.models;

/**
 * @author salbert
 */
public class SequentialContentLog {

    private String subscriber;
    private String contentStartDate;
    private String lastContentDate;
    private long lastContentDay;
    private long contentServiceID;

    public long getContentServiceID() {
        return contentServiceID;
    }

    public void setContentServiceID(long contentServiceID) {
        this.contentServiceID = contentServiceID;
    }

    public String getContentStartDate() {
        return contentStartDate;
    }

    public void setContentStartDate(String contentStartDate) {
        this.contentStartDate = contentStartDate;
    }

    public String getLastContentDate() {
        return lastContentDate;
    }

    public void setLastContentDate(String lastContentDate) {
        this.lastContentDate = lastContentDate;
    }

    public long getLastContentDay() {
        return lastContentDay;
    }

    public void setLastContentDay(long lastContentDay) {
        this.lastContentDay = lastContentDay;
    }

    public String getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(String subscriber) {
        this.subscriber = subscriber;
    }

}
