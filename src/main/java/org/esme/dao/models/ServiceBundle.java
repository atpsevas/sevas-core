/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.esme.dao.models;

/**
 * @author saheedalbert
 */
public class ServiceBundle {

    private long bundleID;
    private String bundleName;
    private String bundleKeyword;
    private long bundlePrice;
    private String bundleDateCreated;
    private String bundleSubscriptionMessage;
    private String bundleUnsubscriptionMessage;

    public long getBundleID() {
        return bundleID;
    }

    public void setBundleID(long bundleID) {
        this.bundleID = bundleID;
    }

    public String getBundleName() {
        return bundleName;
    }

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    public String getBundleKeyword() {
        return bundleKeyword;
    }

    public void setBundleKeyword(String bundleKeyword) {
        this.bundleKeyword = bundleKeyword;
    }

    public long getBundlePrice() {
        return bundlePrice;
    }

    public void setBundlePrice(long bundlePrice) {
        this.bundlePrice = bundlePrice;
    }

    public String getBundleDateCreated() {
        return bundleDateCreated;
    }

    public void setBundleDateCreated(String bundleDateCreated) {
        this.bundleDateCreated = bundleDateCreated;
    }

    public String getBundleSubscriptionMessage() {
        return bundleSubscriptionMessage;
    }

    public void setBundleSubscriptionMessage(String bundleSubscriptionMessage) {
        this.bundleSubscriptionMessage = bundleSubscriptionMessage;
    }

    public String getBundleUnsubscriptionMessage() {
        return bundleUnsubscriptionMessage;
    }

    public void setBundleUnsubscriptionMessage(String bundleUnsubscriptionMessage) {
        this.bundleUnsubscriptionMessage = bundleUnsubscriptionMessage;
    }

}
