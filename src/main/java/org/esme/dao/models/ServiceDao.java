/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.dao.models;

import org.apache.log4j.Logger;
import org.esme.broker.AppBroker;
import org.esme.dto.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author TTMO
 */
public class ServiceDao {

    private static final Logger logger = Logger.getLogger(ServiceDao.class);


    public static Service getService(long serviceID) {
        Service service = null;

        try {
            Statement statement = (new AppBroker()).getDBConnection().createStatement();

            ResultSet resultSet = statement.executeQuery(
                    "select * from subscription_service "
                            + "where service_keyword  ilike '%" + serviceID + "%' or id = " + serviceID + " ");

            while (resultSet.next()) {
                service = new Service();
                service = extractServiceFromResultSet(resultSet);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        logger.info("Service is" + service);

        return service;

    }

    public static ArrayList<Service> getAllServices() {
        Statement statement = null;
        ResultSet resultSet = null;
        Service service;
        ArrayList<Service> allServices = new ArrayList<Service>();

        try {
//            java.sql.Connection dbConnection = DriverManager.getConnection(AppBroker.getPropertyFileHandle().getProperty("dbURL"), AppBroker.getPropertyFileHandle().getProperty("dbUserName"), AppBroker.getPropertyFileHandle().getProperty("dbPassword"));
//            statement = dbConnection.createStatement();

            statement = AppBroker.getDBConnection(AppBroker.getPropertyFileHandle()).createStatement();
            resultSet = statement.executeQuery("select * from subscription_service");

            //We place a lock on the content cache untill it finishes updating....
            //update previously stored job contents from the container...
            while (resultSet.next()) {
                service = extractServiceFromResultSet(resultSet);
                logger.info("Adding Service: " + service);
                allServices.add(service);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.error("Unable to retrieve all Services");
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }

        }
        return allServices;
    }

    private static Service extractServiceFromResultSet(ResultSet resultSet) throws SQLException {
        Service service = new Service();

        service.setServiceID(resultSet.getLong("id"));
        service.setDateCreated(resultSet.getString("date_created"));
        service.setServiceOwner(resultSet.getInt("service_owner"));
        service.setServiceName(resultSet.getString("service_name"));
        service.setServiceShortcode(resultSet.getString("service_shortcode"));
        service.setServiceMTShortCode(resultSet.getString("service_mt_shortcode"));
        service.setServiceBillingShortCode(resultSet.getString("welcome_shortcode"));
        service.setServiceKeyword(resultSet.getString("service_keyword"));
        service.setServiceAllowableKeyword(resultSet.getString("service_allowable_keyword"));
        service.setServiceUnSubscribeKeyword(resultSet.getString("service_unsubcribe_keyword"));
        service.setServicePrice(resultSet.getString("service_price"));
        service.setServiceCategory(resultSet.getString("service_category"));
        service.setServiceCategoryKeyword(resultSet.getString("service_category_keyword"));
        service.setServiceDescription(resultSet.getString("service_description"));
        service.setServicePeriod(resultSet.getString("service_period"));
        service.setFallbackPeriod(resultSet.getLong("service_fallback_period"));
        service.setTimeOfBlast(resultSet.getString("service_tob"));
        service.setServiceSubscriptionMessage(resultSet.getString("service_subscription_msg"));
        service.setServiceUnSubScriptionMessage(resultSet.getString("service_unsubscription_msg"));
        service.setServiceReminderMessage(resultSet.getString("service_reminder_msg"));
        service.setServiceReminderPeriod(resultSet.getString("service_reminder_period"));
        service.setFallbackShortCode(resultSet.getString("fallback_shortcode"));
        service.setFallbackPrice(resultSet.getLong("fallback_price"));
        service.setServiceContentDeliveryMethod(resultSet.getString("service_content_delivery_method"));
        service.setBillingRetryIntervals(resultSet.getLong("service_billing_retry_intervals"));
        service.setBillingTime(resultSet.getLong("service_renewal_time_of_the_day"));
        service.setServiceRenewalMessage(resultSet.getString("service_renewal_message"));
        service.setServiceFallbackRenewalMessage(resultSet.getString("service_fallback_renewal_message"));
        service.setServiceHelpMessage(resultSet.getString("service_help_message"));
        service.setServiceReminderTime(resultSet.getLong("service_reminder_time"));
        service.setServiceSubType(resultSet.getString("service_sub_type"));
        service.setServiceNetwork(resultSet.getString("service_network"));
        service.setServiceWelcomeMessageShortcode(resultSet.getString("service_wc_msg_shortcode"));
        service.setSeriviceBillingNetwork(resultSet.getString("billing_network"));
        service.setServiceStatus(resultSet.getString("service_status"));
        service.setServicePromo(resultSet.getString("service_promo"));
        service.setServicePromoPeriod(resultSet.getString("service_promo_period"));
        service.setServiceCrossSell(resultSet.getString("service_cross_sell"));
        service.setServiceCrossNetwork(resultSet.getString("service_cross_sell_network"));
        service.setServiceCrossMessage(resultSet.getString("service_cross_sell_message"));
        service.setServiceCrossSellShortCode(resultSet.getString("service_cross_sell_shortcode"));

        service.setFallbackShortCode2(resultSet.getString("fallback2_shortcode"));
        service.setFallbackPrice2(resultSet.getString("fallback2_price"));
        service.setFallbackPeriod2(resultSet.getLong("fallback2_period"));
        service.setServiceFallbackRenewalMessage2(resultSet.getString("fallback2_message"));

        service.setFallbackShortCode3(resultSet.getString("fallback3_shortcode"));
        service.setFallbackPrice3(resultSet.getString("fallback3_price"));
        service.setFallbackPeriod3(resultSet.getLong("fallback3_period"));
        service.setServiceFallbackRenewalMessage3(resultSet.getString("fallback3_message"));

        service.setFallbackShortCode4(resultSet.getString("fallback4_shortcode"));
        service.setFallbackPrice4(resultSet.getString("fallback4_price"));
        service.setFallbackPeriod4(resultSet.getLong("fallback4_period"));
        service.setServiceFallbackRenewalMessage4(resultSet.getString("fallback4_message"));

        service.setMainbillingnotifySMSSetting(resultSet.getString("main_billing_notify_settings"));
        service.setFallBack1billingnotifySMSSetting(resultSet.getString("fallback1_billing_notify_settings"));
        service.setFallBack2billingnotifySMSSetting(resultSet.getString("fallback2_billing_notify_settings"));
        service.setFallBack3billingnotifySMSSetting(resultSet.getString("fallback3_billing_notify_settings"));
        service.setFallBack4billingnotifySMSSetting(resultSet.getString("fallback4_billing_notify_settings"));

        service.setInstantBillingRetry(resultSet.getString("instant_billing_retry"));

        //Routing
        service.setRouteToExternalAPI(resultSet.getString("route_to_external_api"));
        service.setRouteToExternalAPByShortCode(resultSet.getString("route_by_shortcode"));

        service.setExternalAPIURL(resultSet.getString("external_api_url"));
        service.setExternalAPIShortCodeParam(resultSet.getString("external_api_sc_param"));
        service.setExternalAPIMSISDNParam(resultSet.getString("external_api_msisdn_param"));
        service.setExternalAPIMessageParam(resultSet.getString("external_api_msg_param"));

        service.setAutoSubToParentService(resultSet.getString("auto_sub_to_parent_serv"));
        service.setParentServiceID(resultSet.getLong("service_parent_id"));

        service.setServiceDoubleConfirmation(resultSet.getString("service_double_confirm"));
        service.setServiceDoubleConfirmationKeyword(resultSet.getString("service_double_confirm_keyword"));
        service.setServiceDoubleConfirmationMessage(resultSet.getString("service_double_confirm_message"));
        service.setServiceContentSenderID(resultSet.getString("service_content_sender_id"));

        return service;
    }

}
