/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.dao.models;

import org.esme.broker.AppBroker;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author TTMO
 */
public class Subscription {

    String subscriptionID;
    String subscriptionReqTime;
    String subscriptionServiceName;
    String subscriptionServiceShortCode;
    String subscriptionMSISDN;
    String subscriptionServiceNetwork;
    String subscriptionType;
    String subscriptionPeriod;
    String subscriptonMainBillingCode;
    String subscriptonFallbackBillingCode;
    String subscriptionStatus;
    String subscriptonLastCharged;
    String subscriptionNextCharged;
    String subscriptionSource;
    String unSubSource;
    String subscriptionExpiryDate;
    String subscriptionAmount;
    String unSubDate;
    String subRequestText;
    String blacklisted;
    String blacklistedDate;
    String serviceID;

    public Subscription() {
    }

    public static ArrayList<String> getAllUniqueMsisdn() {
        ArrayList<String> allMsisdn = new ArrayList<String>();

        try {
            Connection dbConnection = (new AppBroker()).getDBConnection();
            Statement theStatement = dbConnection.createStatement();

            // Get Active Unique subscription MSISDN
            ResultSet resultSet = theStatement.executeQuery("select distinct(sub_msisdn) from subscription "
                    + "where sub_status='active' and blacklisted <> 'blacklisted'");

            // Iterating through result
            while (resultSet.next()) {
                allMsisdn.add(resultSet.getString("sub_msisdn"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Subscription.class.getName()).log(Level.SEVERE, null, ex);
        }

        return allMsisdn;

    }

    public String getUnSubDate() {
        return unSubDate;
    }

    public void setUnSubDate(String unSubDate) {
        this.unSubDate = unSubDate;
    }

    public String getSubRequestText() {
        return subRequestText;
    }

    public void setSubRequestText(String subRequestText) {
        this.subRequestText = subRequestText;
    }

    public String getBlacklisted() {
        return blacklisted;
    }

    public void setBlacklisted(String blacklisted) {
        this.blacklisted = blacklisted;
    }

    public String getBlacklistedDate() {
        return blacklistedDate;
    }

    public void setBlacklistedDate(String blacklistedDate) {
        this.blacklistedDate = blacklistedDate;
    }

    public String getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(String subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    public String getSubscriptonLastCharged() {
        return subscriptonLastCharged;
    }

    public void setSubscriptonLastCharged(String subscriptonLastCharged) {
        this.subscriptonLastCharged = subscriptonLastCharged;
    }

    public String getSubscriptionNextCharged() {
        return subscriptionNextCharged;
    }

    public void setSubscriptionNextCharged(String subscriptionNextCharged) {
        this.subscriptionNextCharged = subscriptionNextCharged;
    }

    public String getSubscriptionSource() {
        return subscriptionSource;
    }

    public void setSubscriptionSource(String subscriptionSource) {
        this.subscriptionSource = subscriptionSource;
    }

    public String getSubscriptionExpiryDate() {
        return subscriptionExpiryDate;
    }

    public void setSubscriptionExpiryDate(String subscriptionExpiryDate) {
        this.subscriptionExpiryDate = subscriptionExpiryDate;
    }

    public String getSubscriptionAmount() {
        return subscriptionAmount;
    }

    public void setSubscriptionAmount(String subscriptionAmount) {
        this.subscriptionAmount = subscriptionAmount;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getSubscriptionID() {
        return subscriptionID;
    }

    public void setSubscriptionID(String subscriptionID) {
        this.subscriptionID = subscriptionID;
    }

    public String getSubscriptionMSISDN() {
        return subscriptionMSISDN;
    }

    public void setSubscriptionMSISDN(String subscriptionMSISDN) {
        this.subscriptionMSISDN = subscriptionMSISDN;
    }

    public String getSubscriptionServiceNetwork() {
        return subscriptionServiceNetwork;
    }

    public void setSubscriptionServiceNetwork(String subscriptionServiceNetwork) {
        this.subscriptionServiceNetwork = subscriptionServiceNetwork;
    }

    public String getSubscriptionServiceName() {
        return subscriptionServiceName;
    }

    public void setSubscriptionServiceName(String subscriptionServiceName) {
        this.subscriptionServiceName = subscriptionServiceName;
    }

    public String getSubscriptionServiceShortCode() {
        return subscriptionServiceShortCode;
    }

    public void setSubscriptionServiceShortCode(String subscriptionServiceShortCode) {
        this.subscriptionServiceShortCode = subscriptionServiceShortCode;
    }

    public String getSubscriptionReqTime() {
        return subscriptionReqTime;
    }

    public void setSubscriptionReqTime(String subscriptionReqTime) {
        this.subscriptionReqTime = subscriptionReqTime;
    }

    public String getUnSubSource() {
        return unSubSource;
    }

    public void setUnSubSource(String unSubSource) {
        this.unSubSource = unSubSource;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public String getSubscriptionPeriod() {
        return subscriptionPeriod;
    }

    public void setSubscriptionPeriod(String subscriptionPeriod) {
        this.subscriptionPeriod = subscriptionPeriod;
    }

    public String getSubscriptonMainBillingCode() {
        return subscriptonMainBillingCode;
    }

    public void setSubscriptonMainBillingCode(String subscriptonMainBillingCode) {
        this.subscriptonMainBillingCode = subscriptonMainBillingCode;
    }

    public String getSubscriptonFallbackBillingCode() {
        return subscriptonFallbackBillingCode;
    }

    public void setSubscriptonFallbackBillingCode(String subscriptonFallbackBillingCode) {
        this.subscriptonFallbackBillingCode = subscriptonFallbackBillingCode;
    }
}
