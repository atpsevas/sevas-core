/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.esme.dao.models;

/**
 * @author salbert
 */
public class ExpiredSubscription {

    private long subscriptionserviceID;
    private String subscriptionServiceName;
    private long subscriptionServicePeriod;
    private String subscriptionMSISDN;
    private String subscriptionWelcomeShortCode;

    public String getSubscriptionWelcomeShortCode() {
        return subscriptionWelcomeShortCode;
    }

    public void setSubscriptionWelcomeShortCode(String subscriptionWelcomeShortCode) {
        this.subscriptionWelcomeShortCode = subscriptionWelcomeShortCode;
    }


    public String getSubscriptionMSISDN() {
        return subscriptionMSISDN;
    }

    public void setSubscriptionMSISDN(String subscriptionMSISDN) {
        this.subscriptionMSISDN = subscriptionMSISDN;
    }

    public String getSubscriptionServiceName() {
        return subscriptionServiceName;
    }

    public void setSubscriptionServiceName(String subscriptionServiceName) {
        this.subscriptionServiceName = subscriptionServiceName;
    }

    public long getSubscriptionServicePeriod() {
        return subscriptionServicePeriod;
    }

    public void setSubscriptionServicePeriod(long subscriptionServicePeriod) {
        this.subscriptionServicePeriod = subscriptionServicePeriod;
    }

    public long getSubscriptionserviceID() {
        return subscriptionserviceID;
    }

    public void setSubscriptionserviceID(long subscriptionserviceID) {
        this.subscriptionserviceID = subscriptionserviceID;
    }

}
