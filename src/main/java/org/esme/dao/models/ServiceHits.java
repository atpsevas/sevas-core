/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.esme.dao.models;

/**
 * @author salbert
 */
public class ServiceHits {

    private String servName;
    private long hits;

    public long getHits() {
        return hits;
    }

    public void setHits(long hits) {
        this.hits = hits;
    }

    public String getServName() {
        return servName;
    }

    public void setServName(String servName) {
        this.servName = servName;
    }


}
