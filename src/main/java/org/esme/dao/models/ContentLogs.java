/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.dao.models;

import java.util.ArrayList;


public class ContentLogs {

    ArrayList<ContentLogs> logs;
    ContentLogs log;
    private String service;
    private String theDate;
    private long totalSent;
    private long submitted;
    private long delivered;
    private long failed;
    private long buffered;
    private long rejected;
    private long unKnownStatus;

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getTheDate() {
        return theDate;
    }

    public void setTheDate(String theDate) {
        this.theDate = theDate;
    }

    public long getBuffered() {
        return buffered;
    }

    public void setBuffered(long buffered) {
        this.buffered = buffered;
    }

    public long getDelivered() {
        return delivered;
    }

    public void setDelivered(long delivered) {
        this.delivered = delivered;
    }

    public long getFailed() {
        return failed;
    }

    public void setFailed(long failed) {
        this.failed = failed;
    }

    public long getRejected() {
        return rejected;
    }

    public void setRejected(long rejected) {
        this.rejected = rejected;
    }

    public long getSubmitted() {
        return submitted;
    }

    public void setSubmitted(long submitted) {
        this.submitted = submitted;
    }

    public long getTotalSent() {
        return totalSent;
    }

    public void setTotalSent(long totalSent) {
        this.totalSent = totalSent;
    }

    public long getUnKnownStatus() {
        return unKnownStatus;
    }

    public void setUnKnownStatus(long unKnownStatus) {
        this.unKnownStatus = unKnownStatus;
    }

}
