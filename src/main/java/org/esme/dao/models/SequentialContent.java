/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.dao.models;

/**
 * @author TTMO
 */
public class SequentialContent {

    private long content_id;
    private String dateUploaded;
    private long contentOwner;
    private long contentServiceID;
    private String contentServiceName;
    private String content;
    private long contentBlastDay;

    public long getContentServiceID() {
        return contentServiceID;
    }

    public void setContentServiceID(long contentServiceID) {
        this.contentServiceID = contentServiceID;
    }

    public String getContentServiceName() {
        return contentServiceName;
    }

    public void setContentServiceName(String contentServiceName) {
        this.contentServiceName = contentServiceName;
    }

    public long getContentBlastDay() {
        return contentBlastDay;
    }

    public void setContentBlastDay(long contentBlastDay) {
        this.contentBlastDay = contentBlastDay;
    }

    public long getContent_id() {
        return content_id;
    }

    public void setContent_id(long conten_id) {
        this.content_id = conten_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getContentOwner() {
        return contentOwner;
    }

    public void setContentOwner(long contentOwner) {
        this.contentOwner = contentOwner;
    }

    public long getContentService() {
        return contentServiceID;
    }

    public void setContentService(long contentService) {
        this.contentServiceID = contentService;
    }

    public String getDateUploaded() {
        return dateUploaded;
    }

    public void setDateUploaded(String dateUploaded) {
        this.dateUploaded = dateUploaded;
    }
}
