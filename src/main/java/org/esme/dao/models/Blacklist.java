/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.dao.models;

import org.esme.controllers.SubEngine;

/**
 * @author salbert
 */
public class Blacklist {

    String blacklistedMSISDN;
    String blacklistDate;
    String blacklistedNetwork;

    /**
     * Check if a particular MSISDN is blacklisted
     *
     * @param sub_msisdn
     * @param network
     * @return
     */

    public static boolean isBlacklisted(String subscriber, String network) {
        // TODO

        return false;
    }

    /**
     * Check if a particular MSISDN is blacklisted
     *
     * @param sub_msisdn
     * @return
     */

    public static boolean isBlacklisted(String subscriber) {
        return (new SubEngine()).isBlacklisted(subscriber);
    }

    public String getBlacklistDate() {
        return blacklistDate;
    }

    public void setBlacklistDate(String blacklistDate) {
        this.blacklistDate = blacklistDate;
    }

    public String getBlacklistedMSISDN() {
        return blacklistedMSISDN;
    }

    public void setBlacklistedMSISDN(String blacklistedMSISDN) {
        this.blacklistedMSISDN = blacklistedMSISDN;
    }

    public String getBlacklistedNetwork() {
        return blacklistedNetwork;
    }

    public void setBlacklistedNetwork(String blacklistedNetwork) {
        this.blacklistedNetwork = blacklistedNetwork;
    }
//    static public boolean isBlacklisted(String sub_msisdn) {
//
//        // Search Element of DNC file
//        try {
//            ArrayList<String> doNotChargeNumbers = (ArrayList<String>) AppBroker.context.getAttribute("doNotChargeNumbersCache");
//            sevasLogger.info(AppBroker.context.getAttribute("doNotChargeNumbersCache"));
//         
//
//            if (doNotChargeNumbers.contains(sub_msisdn)) {
//                sevasLogger.info("Found in DNC list " + sub_msisdn);
//                return true;
//            }
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//        try {
//            // Search Element of External Blacklist
//            HashMap blacklisted = reverse((HashMap) AppBroker.context.getAttribute("blackListedNumbersCache"));
//
//            if (blacklisted.containsKey(sub_msisdn)) {
//                sevasLogger.info("Found in Blacklist Portal list " + sub_msisdn);
//
//                return true;
//            }
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//        try {
//            // Search elements of Blacklisted from Portal
//            sevasLogger.info("Searching blacklisted from portal for " + sub_msisdn);
//
//            ArrayList<Blacklist> blacklistFromPortal = (ArrayList<Blacklist>) AppBroker.context.getAttribute("activeBlacklistCache");
//
//            List<Blacklist> foundObjs = blacklistFromPortal.stream()
//                    .filter(Blacklist
//                            -> Blacklist.blacklistedMSISDN.equalsIgnoreCase(sub_msisdn))
//                    .collect(Collectors.toList());
//
//            if (null != foundObjs) {
//                foundObjs.forEach(System.out::println);
//                sevasLogger.info("Found in subscription blacklist list " + sub_msisdn);
//
//                return true;
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//        return true;
//    }


//    public Blacklist(String msisdn, String date, String network) {
//        this.blacklistedMSISDN = msisdn;
//        this.blacklistDate = date;
//        this.blacklistedNetwork = network;
//    }
}
