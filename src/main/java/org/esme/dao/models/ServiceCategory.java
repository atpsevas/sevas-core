
package org.esme.dao.models;

/**
 * @author salbert
 */
public class ServiceCategory {

    private long categoryID;
    private String categoryName;
    private String categoryKeyword;
    private String categoryDateCreated;

    public String getCategoryDateCreated() {
        return categoryDateCreated;
    }

    public void setCategoryDateCreated(String categoryDateCreated) {
        this.categoryDateCreated = categoryDateCreated;
    }

    public String getCategoryKeyword() {
        return categoryKeyword;
    }

    public void setCategoryKeyword(String categoryKeyword) {
        this.categoryKeyword = categoryKeyword;
    }

    public long getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(long categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
