/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.esme.dao.models;

/**
 * @author TTMO
 */
public class Subscribers {
    private int subServiceID;
    private String subServiceShortCode;

    public int getSubServiceID() {
        return subServiceID;
    }

    public void setSubServiceID(int subServiceID) {
        this.subServiceID = subServiceID;
    }

    public String getSubServiceShortCode() {
        return subServiceShortCode;
    }

    public void setSubServiceShortCode(String subServiceShortCode) {
        this.subServiceShortCode = subServiceShortCode;
    }

}
