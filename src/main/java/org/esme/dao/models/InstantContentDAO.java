/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.dao.models;

/**
 * @author saheedalbert
 */
public class InstantContentDAO {

    private String serviceType;
    private String srcModule;
    private String senderID;
    private String recipientID;
    private String content;
    private String serviceName;
    private long serviceID;
    private String network;
    private String dlrMask;

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getSrcModule() {
        return srcModule;
    }

    public void setSrcModule(String srcModule) {
        this.srcModule = srcModule;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getRecipientID() {
        return recipientID;
    }

    public void setRecipientID(String recipientID) {
        this.recipientID = recipientID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public long getServiceID() {
        return serviceID;
    }

    public void setServiceID(long serviceID) {
        this.serviceID = serviceID;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getDlrMask() {
        return dlrMask;
    }

    public void setDlrMask(String dlrMask) {
        this.dlrMask = dlrMask;
    }

}
