/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.dao.models;

/**
 * @author tofunmi
 */
public class MTBillingPayload {

    private String serviceSubType;
    private String billingCode;
    private String msisdn;
    private String message;
    private String smsc;
    private long smsType;
    private String billingSMSBoxID;
    private long milliTime;
    private long pid;
    private long serviceId;
    private long dlrMask;
    private String dlrCallbackUrl;
    private boolean isOnUAT;
    private String srcModule;

    public String getSrcModule() {
        return srcModule;
    }

    public void setSrcModule(String srcModule) {
        this.srcModule = srcModule;
    }

    public boolean isIsOnUAT() {
        return isOnUAT;
    }

    public void setIsOnUAT(boolean isOnUAT) {
        this.isOnUAT = isOnUAT;
    }

    public String getServiceSubType() {
        return serviceSubType;
    }

    public void setServiceSubType(String serviceSubType) {
        this.serviceSubType = serviceSubType;
    }

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSmsc() {
        return smsc;
    }

    public void setSmsc(String smsc) {
        this.smsc = smsc;
    }

    public long getSmsType() {
        return smsType;
    }

    public void setSmsType(long smsType) {
        this.smsType = smsType;
    }

    public String getBillingSMSBoxID() {
        return billingSMSBoxID;
    }

    public void setBillingSMSBoxID(String billingSMSBoxID) {
        this.billingSMSBoxID = billingSMSBoxID;
    }

    public long getMilliTime() {
        return milliTime;
    }

    public void setMilliTime(long milliTime) {
        this.milliTime = milliTime;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public long getServiceId() {
        return serviceId;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }

    public long getDlrMask() {
        return dlrMask;
    }

    public void setDlrMask(long dlrMask) {
        this.dlrMask = dlrMask;
    }

    public String getDlrCallbackUrl() {
        return dlrCallbackUrl;
    }

    public void setDlrCallbackUrl(String dlrCallbackUrl) {
        this.dlrCallbackUrl = dlrCallbackUrl;
    }

}
