/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.esme.dao.models;

/**
 * @author TTMO
 */
public class Subscriber {

    private long subServiceID;
    private String subServiceShortCode;

    public long getSubServiceID() {
        return subServiceID;
    }

    public void setSubServiceID(long subServiceID) {
        this.subServiceID = subServiceID;
    }

    public String getSubServiceShortCode() {
        return subServiceShortCode;
    }

    public void setSubServiceShortCode(String subServiceShortCode) {
        this.subServiceShortCode = subServiceShortCode;
    }

}
