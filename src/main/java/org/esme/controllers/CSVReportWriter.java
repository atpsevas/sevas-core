package org.esme.controllers;

import com.csvreader.CsvWriter;
import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.ReportMailer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("serial")
public class CSVReportWriter extends AppBroker {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            doMonthTillDateReport();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @SuppressWarnings({"CallToPrintStackTrace", "UseSpecificCatch"})
    public void doMonthTillDateReport() {

        String outputFile = getServletContext().getInitParameter("dailyReportFilePath") + formatDateToString(new Date()) + ".csv";
        // before we open the file check to see if it already exists
        try {

            // use FileWriter constructor that specifies open for appending
            CsvWriter csvOutput = new CsvWriter(new FileWriter(outputFile), ',');
            // if the file didn't already exist then we need to write out the header line
            org.esme.broker.AppBroker.sevasLogger.info("Creating folder....");
            csvOutput.write(" ");
            csvOutput.write("Nigeria");
            csvOutput.write("MTD");

            //if (!alreadyExists) {
            //This will print from day 1 of the month till last date.
            //Rev in Nigeria.... Daily rev
            //Sub revenue..... Revenue for that day
            //Resubmit revenue..... Old subscribers, renewals
            ResultSet dailyTotalRevenueDate = getData(getServletContext().getInitParameter("PSADailydailyTotalRevenue"));
            while (dailyTotalRevenueDate.next()) {
                csvOutput.write("Day " + dailyTotalRevenueDate.getString("theDate") + "");
                //previousDayOfTheMonth++;
            }

            csvOutput.endRecord();

            // write out a few records
            csvOutput.write("Revenue");
            csvOutput.write("Rev In Nigeria");
            //Month Till Date Total
            ResultSet monthTillDateTotalRevenue = getData(getServletContext().getInitParameter("PSAMonthTillDateTotalRevenue"));
            while (monthTillDateTotalRevenue.next()) {
                try {
                    csvOutput.write(formatNumber(monthTillDateTotalRevenue.getString("monthTillDateTotalRevenue")));
                } catch (Exception e) {

                }
            }

            //Daily Revenue 
            ResultSet dailyTotalRevenue = getData(getServletContext().getInitParameter("PSADailydailyTotalRevenue"));
            while (dailyTotalRevenue.next()) {
                try {
                    csvOutput.write(formatNumber(dailyTotalRevenue.getString("dailyTotalRevenue")));
                } catch (Exception e) {

                }
            }
            csvOutput.endRecord();

            //New Subscription Revenue...
            csvOutput.write("");
            csvOutput.write("Sub Revenue");

            //New Subscription Total Revenue...
            ResultSet dailyNewSubTotalRevenue = getData(getServletContext().getInitParameter("PSANewSubTotalRevenue"));
            while (dailyNewSubTotalRevenue.next()) {
                try {
                    csvOutput.write(formatNumber(dailyNewSubTotalRevenue.getString("newSubTotalRevenue")));
                } catch (Exception e) {

                }
            }
            //New Subscription Daily Revenue...
            ResultSet dailyNewSubRevenue = getData(getServletContext().getInitParameter("PSANewSubDailyRevenue"));
            while (dailyNewSubRevenue.next()) {
                csvOutput.write(formatNumber(dailyNewSubRevenue.getString("dailyNewSubRevenue")));
            }
            csvOutput.endRecord();

            //Renewal Revenue
            csvOutput.write("");
            csvOutput.write("ReSub Revenue");
            //Total Renewal Revenue
            ResultSet totalRenewalRevenue = getData(getServletContext().getInitParameter("PSARenewedSubTotalRevenue"));
            while (totalRenewalRevenue.next()) {
                try {
                    csvOutput.write(formatNumber(totalRenewalRevenue.getString("renewSubTotalRevenue")));
                } catch (Exception e) {

                }
            }

            //Daily Renewal Revenue
            ResultSet dailyRenewalRevenue = getData(getServletContext().getInitParameter("PSArenewSubDailyRevenue"));
            while (dailyRenewalRevenue.next()) {
                try {
                    csvOutput.write(formatNumber(dailyRenewalRevenue.getString("dailyrenewSubRevenue")));
                } catch (Exception e) {

                }
            }
            csvOutput.endRecord();

            //Leave two rows blank....
            csvOutput.write("");
            csvOutput.endRecord();
            csvOutput.write("");
            csvOutput.endRecord();

            //Daily Subscription..
            csvOutput.write("Addition");
            csvOutput.write("Gross Add");
            ResultSet grossTotalAdd = getData(getServletContext().getInitParameter("PSAGrossTotalAdd"));
            while (grossTotalAdd.next()) {
                try {
                    csvOutput.write(formatNumber(grossTotalAdd.getString("grossTotalAddCount")));
                } catch (Exception e) {

                }
            }

            ResultSet grossAdd = getData(getServletContext().getInitParameter("PSAGrossAdd"));
            while (grossAdd.next()) {
                try {
                    csvOutput.write(formatNumber(grossAdd.getString("grossAddCount")));
                } catch (Exception e) {

                }
            }
            csvOutput.endRecord();

            csvOutput.write("");
            csvOutput.write("Paid Gross Adds");
            //csvOutput.write("0");
            ResultSet totalPaidGrossAdd = getData(getServletContext().getInitParameter("PSATotalPaidGrossAdd"));
            while (totalPaidGrossAdd.next()) {
                try {
                    csvOutput.write(formatNumber(totalPaidGrossAdd.getString("totalPaidGrossAdd")));
                } catch (Exception e) {

                }
            }

            ResultSet paidGrossAdd = getData(getServletContext().getInitParameter("PSAPaidGrossAdd"));
            while (paidGrossAdd.next()) {
                try {
                    csvOutput.write(formatNumber(paidGrossAdd.getString("paidGrossAdd")));
                } catch (Exception e) {

                }
            }
            csvOutput.endRecord();

            csvOutput.write("");
            csvOutput.write("Net Gross Adds");
            csvOutput.write(formatNumber("0"));
//            for (int totalRevenueIndex = 0; totalRevenueIndex < renewSubRevenue.length; totalRevenueIndex++) {
//                csvOutput.write(formatNumber(renewSubRevenue[totalRevenueIndex]));
//            }
            csvOutput.endRecord();

            //BASE
            csvOutput.write("");
            csvOutput.endRecord();
            csvOutput.write("");
            csvOutput.endRecord();
            csvOutput.write("Base");
            csvOutput.write("Active Base");
            ResultSet totalActiveBase = getData(getServletContext().getInitParameter("PSATotalActiveBase"));
            while (totalActiveBase.next()) {
                try {
                    csvOutput.write(formatNumber(totalActiveBase.getString("PSATotalActiveBase")));
                } catch (Exception e) {

                }
            }
            csvOutput.endRecord();

            csvOutput.write("");
            csvOutput.write("Grace Base");
            csvOutput.write(formatNumber("0"));
//            for (int totalRevenueIndex = 0; totalRevenueIndex < newSubRevenue.length; totalRevenueIndex++) {
//                csvOutput.write(formatNumber(newSubRevenue[totalRevenueIndex]));
//            }
            csvOutput.endRecord();

            csvOutput.write("");
            csvOutput.write("Paid Active Base");
            csvOutput.write(formatNumber("0"));
//            for (int totalRevenueIndex = 0; totalRevenueIndex < renewSubRevenue.length; totalRevenueIndex++) {
//                csvOutput.write(formatNumber(renewSubRevenue[totalRevenueIndex]));
//            }
            csvOutput.endRecord();

            //Churn
            csvOutput.write("");
            csvOutput.endRecord();
            csvOutput.write("");
            csvOutput.endRecord();
            csvOutput.write("Churn");
            csvOutput.write("Paid Total Churn");
            csvOutput.write(formatNumber("0"));
//            for (int totalRevenueIndex = 0; totalRevenueIndex < totalRevenue.length; totalRevenueIndex++) {
//                csvOutput.write(formatNumber(totalRevenue[totalRevenueIndex]));
//            }
            csvOutput.endRecord();
            csvOutput.write("");
            csvOutput.write("Paid Voluntary Churn");
            csvOutput.write(formatNumber("0"));
//            for (int totalRevenueIndex = 0; totalRevenueIndex < totalRevenue.length; totalRevenueIndex++) {
//                csvOutput.write(formatNumber(totalRevenue[totalRevenueIndex]));
//            }
            csvOutput.endRecord();

            csvOutput.write("");
            csvOutput.write("Paid Involuntary Churn ");
            csvOutput.write(formatNumber("0"));
//            for (int totalRevenueIndex = 0; totalRevenueIndex < totalRevenue.length; totalRevenueIndex++) {
//                csvOutput.write(formatNumber(totalRevenue[totalRevenueIndex]));
//            }
            csvOutput.endRecord();

            csvOutput.write("");
            csvOutput.write("Total Churn ");
            csvOutput.write(formatNumber("0"));
//            for (int totalRevenueIndex = 0; totalRevenueIndex < totalRevenue.length; totalRevenueIndex++) {
//                csvOutput.write(formatNumber(totalRevenue[totalRevenueIndex]));
//            }
            csvOutput.endRecord();

            csvOutput.write("");
            csvOutput.write("Total voluntary Churn ");
            csvOutput.write(formatNumber("0"));
//            for (int totalRevenueIndex = 0; totalRevenueIndex < totalRevenue.length; totalRevenueIndex++) {
//                csvOutput.write(formatNumber(totalRevenue[totalRevenueIndex]));
//            }
            csvOutput.endRecord();

            csvOutput.write("");
            csvOutput.write("Total Involuntary Churn ");
            csvOutput.write(formatNumber("0"));
//            for (int totalRevenueIndex = 0; totalRevenueIndex < totalRevenue.length; totalRevenueIndex++) {
//                csvOutput.write(formatNumber(totalRevenue[totalRevenueIndex]));
//            }
            csvOutput.endRecord();
            csvOutput.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Mail Report
        ReportMailer rm = new ReportMailer(
                getServletContext().getInitParameter("reportMailAddresses"),
                "Month Till Date Revenue Report", "Month Till Date Report", outputFile);
        rm.sendReport();
    }

    public int getDayOfTheMonth() {
        Calendar cal = Calendar.getInstance();
        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        return dayOfMonth;
    }

    public String formatNumber(String theNumber) {
        NumberFormat myFormat = NumberFormat.getInstance();
        myFormat.setGroupingUsed(true);
        return myFormat.format(Long.parseLong(theNumber));
    }

    public ResultSet getData(String query) {
        @SuppressWarnings("UnusedAssignment")
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException ex) {
        }
        return resultSet;
    }

}
