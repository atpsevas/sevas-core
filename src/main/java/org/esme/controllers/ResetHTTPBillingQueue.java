/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author saheedalbert
 */
@SuppressWarnings("serial")
public class ResetHTTPBillingQueue extends HttpServlet {

    String b;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            b = request.getParameter("billingToTrack");
            //Confirm if billing is ongoin before starting another instance..
            if (b.equalsIgnoreCase("gloBillingTracker")) {
                getServletContext().setAttribute("gloBillingTracker", "inactive");
            } else if (b.equalsIgnoreCase("etiBillingTracker")) {
                getServletContext().setAttribute("etiBillingTracker", "inactive");
            }

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
