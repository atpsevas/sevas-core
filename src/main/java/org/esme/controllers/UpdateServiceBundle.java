package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.Network;
import org.esme.dao.models.ServiceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class UpdateServiceBundle extends AppBroker {

    PreparedStatement pstmt;
    Network theNetwork;
    ArrayList<Network> networkCache;

    @SuppressWarnings({"CallToThreadDumpStack", "UseSpecificCatch", "CallToPrintStackTrace"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String action = removePoison(request.getParameter("action"));
        long bundleID = Long.parseLong(removePoison(request.getParameter("bundleID")));
        String bundleName = removePoison(request.getParameter("bundleName"));

        if (action.equalsIgnoreCase("delete")) {
            try {

                pstmt = getDBConnection().prepareStatement(
                        "delete from service_bundle where id ilike ? ");
                pstmt.setLong(1, bundleID);
                int update = pstmt.executeUpdate();
                if (update > 0) {
                    //update bundle cache
                    updateBundle();
                    request.setAttribute(
                            "message",
                            bundleID + " successfully deleted.");
                    request.getRequestDispatcher("new_serv_bundle.jsp?bundleID=" + bundleID).forward(request, response);
                } else {
                    request.setAttribute(
                            "message",
                            "No bundle  was deleted.");
                    request.getRequestDispatcher(
                            "new_serv_bundle.jsp").forward(request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
                request.setAttribute(
                        "message",
                        "An error occured while trying to delete " + bundleName);
                request.getRequestDispatcher("new_serv_bundle.jsp?bundleID=" + bundleID).forward(request, response);
            } finally {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                }
            }

        } else if (action.equalsIgnoreCase("update")) {

            String bundleKeyword = removePoison(request.getParameter("bundleKeyword"));
            String bundleSubscriptionMessage = removePoison(request.getParameter("bundleSubMessage"));
            String bundleUnSubscriptionMessage = removePoison(request.getParameter("bundleUnsubMessage"));
            long bundlePrice = Long.parseLong(removePoison(request.getParameter("bundlePrice")));

            try {

                pstmt = getDBConnection().prepareStatement(
                        "update service_bundle set bundle_keyword = ?, "
                                + "bundle_sub_message = ?, "
                                + "bundle_unsub_message = ?, bundle_price = ?"
                                + "where id = ?");

                pstmt.setString(1, bundleKeyword);
                pstmt.setString(2, bundleSubscriptionMessage);
                pstmt.setString(3, bundleUnSubscriptionMessage);
                pstmt.setLong(4, bundlePrice);
                pstmt.setLong(5, bundleID);

                int update = pstmt.executeUpdate();
                if (update > 0) {
                    //Update bundle
                    updateBundle();
                    request.setAttribute(
                            "message",
                            "Bundle successfully updated.");
                    request.getRequestDispatcher(
                            "new_serv_bundle.jsp?bundleID=" + bundleID).forward(request, response);
                } else {
                    request.setAttribute("message", "No bundle was updated.");
                    request.getRequestDispatcher(
                            "new_serv_bundle.jsp?bundleID=" + bundleID).forward(request, response);
                }
            } catch (SQLException e) {
                e.printStackTrace();
                request.setAttribute(
                        "message", "SEVERE: DB eror occured while trying to update " + bundleName);
                request.getRequestDispatcher(
                        "new_serv_bundle.jsp?bundleID=" + bundleID).forward(request, response);
            } finally {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                }
            }
        }

    }

    @SuppressWarnings({"CallToThreadDumpStack", "Convert2Diamond", "null", "CallToPrintStackTrace"})
    public void updateBundle() {

        org.esme.broker.AppBroker.sevasLogger.info("Updating Bundle..");

        ArrayList<ServiceBundle> serviceBundleCache = new ArrayList<ServiceBundle>();
        Statement statement = null;
        ResultSet resultSet = null;

        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(getServletContext().getInitParameter("bundleQuery"));
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {

                //update previously stored job contents from the container...
                while (resultSet.next()) {

                    ServiceBundle serviceBundle = new ServiceBundle();
                    serviceBundle.setBundleID(resultSet.getLong("id"));
                    serviceBundle.setBundleName(resultSet.getString("bundle_name"));
                    serviceBundle.setBundleKeyword(resultSet.getString("bundle_keyword"));
                    serviceBundle.setBundlePrice(resultSet.getLong("bundle_price"));
                    serviceBundle.setBundleDateCreated(resultSet.getString("date_created"));
                    serviceBundleCache.add(serviceBundle);
                }
                //Create cache of all users
                org.esme.broker.AppBroker.sevasLogger.info("Total Bundle " + serviceBundleCache.size());
                getServletContext().setAttribute("bundleCache", serviceBundleCache);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
