package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.ServiceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class CreateNewBundle extends AppBroker {

    String serviceBundleName, serviceBundleKeyword,
            serviceBundleSubscriptionMessage, serviceBundleUnSubscriptionMessage;
    long serviceBundlePrice;

    PreparedStatement pstm;
    Statement st;
    ResultSet rs;

    @SuppressWarnings("CallToPrintStackTrace")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        serviceBundleName = removePoison(request.getParameter("bundleName"));
        serviceBundleKeyword = removePoison(request.getParameter("bundleKeyword"));
        serviceBundlePrice = Long.parseLong(removePoison(request.getParameter("bundlePrice")));
        serviceBundleSubscriptionMessage = removePoison(request.getParameter("bundleSubMessage"));
        serviceBundleUnSubscriptionMessage = removePoison(request.getParameter("bundleUnsubMessage"));

        try {

            st = getDBConnection().createStatement();
            rs = st.executeQuery("select bundle_name from service_bundle "
                    + "where bundle_name ilike '" + serviceBundleName + "' ");

            while (rs.next()) {
                request.setAttribute("message", "Bundle named " + serviceBundleName + " already exist.");
                request.getRequestDispatcher("new_serv_bundle.jsp").forward(request, response);
                return;
            }

            pstm = getDBConnection().prepareStatement(
                    "insert into service_bundle("
                            + "bundle_name, bundle_keyword, bundle_price, "
                            + "bundle_sub_message, bundle_unsub_message) "
                            + "values(?,?,?,?,?)");

            pstm.setString(1, serviceBundleName);
            pstm.setString(2, serviceBundleKeyword);
            pstm.setLong(3, serviceBundlePrice);
            pstm.setString(4, serviceBundleSubscriptionMessage);
            pstm.setString(5, serviceBundleUnSubscriptionMessage);

            int update = pstm.executeUpdate();

            if (update > 0) {
                updateBundle();
                request.setAttribute("message", serviceBundleName + " successfully created");
                request.getRequestDispatcher("new_serv_bundle.jsp").forward(request, response);
                return;
            }
            request.setAttribute("message", "Failed to create " + serviceBundleName + ". Please try again.");
            request.getRequestDispatcher("new_serv_bundle.jsp").forward(request, response);
        } catch (SQLException ex) {
            ex.printStackTrace();
            request.setAttribute("message", "error occured while creating " + serviceBundleName + ". Please try again.");
            request.getRequestDispatcher("new_serv_bundle.jsp").forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    @SuppressWarnings({"CallToThreadDumpStack", "Convert2Diamond", "null", "CallToPrintStackTrace"})
    public void updateBundle() {

        org.esme.broker.AppBroker.sevasLogger.info("Updating Bundle..");
        ArrayList<ServiceBundle> serviceBundleCache = new ArrayList<ServiceBundle>();
        @SuppressWarnings("UnusedAssignment")
        Statement statement = null;
        @SuppressWarnings("UnusedAssignment")
        ResultSet resultSet = null;

        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(getServletContext().getInitParameter("bundleQuery"));
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {

                //update previously stored job contents from the container...
                while (resultSet.next()) {

                    ServiceBundle serviceBundle = new ServiceBundle();
                    serviceBundle.setBundleID(resultSet.getLong("id"));
                    serviceBundle.setBundleName(resultSet.getString("bundle_name"));
                    serviceBundle.setBundleKeyword(resultSet.getString("bundle_keyword"));
                    serviceBundle.setBundlePrice(resultSet.getLong("bundle_price"));
                    serviceBundle.setBundleDateCreated(resultSet.getString("date_created"));
                    serviceBundleCache.add(serviceBundle);
                }
                //Create cache of all users
                org.esme.broker.AppBroker.sevasLogger.info("Total Bundle " + serviceBundleCache.size());
                getServletContext().setAttribute("bundleCache", serviceBundleCache);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
