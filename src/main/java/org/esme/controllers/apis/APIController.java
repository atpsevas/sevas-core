/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers.apis;

import com.google.gson.Gson;
import org.esme.broker.AppBroker;
import org.esme.dao.models.Content;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * @author tm30
 */
public class APIController extends AppBroker {

    PrintWriter out;
    Gson gson;
    ArrayList<Content> allContent;

    @SuppressWarnings("UseSpecificCatch")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        sevasLogger.info("Handling API request");
        response.setContentType("application/json");
        out = response.getWriter();

        String serviceId = null;
        String apiResponsePayload = "";
        gson = new Gson();

        out.println(apiResponsePayload);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
