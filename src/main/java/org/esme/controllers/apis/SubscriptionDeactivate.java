package org.esme.controllers.apis;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import org.esme.broker.AppBroker;
import org.esme.controllers.apis.models.ActionResponse;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("serial")
public class SubscriptionDeactivate extends AppBroker {

    PrintWriter out;

    String json;
    XStream xstream = null;

    public static boolean isNumeric(String msisdn) {

        boolean isValid = false;
        long NumberLenght = 0;
        String number = msisdn.replace("+", "");

        if (number.startsWith(AppBroker.CountryCodePrefix_PROP)) {
            NumberLenght = 12;
        } else if (number.startsWith("0")) {
            NumberLenght = 11;
        }

        /*Number: A numeric value will have following format:
         ^[-+]?: Starts with an optional "+" or "-" sign.
         [0-9]*: May have one or more digits.
         \\.? : May contain an optional "." (decimal point) character.
         [0-9]+$ : ends with numeric digit.
         */
        //Initialize reg ex for numeric data.
        String expression = "^[-+]?[0-9]*\\.?[0-9]+$";
        CharSequence inputStr = number;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches() && ((NumberLenght == 12) || (NumberLenght == 11))) {
            isValid = true;
        }

//        //it can't star with 00
//        if (number.startsWith("00") || (number.startsWith(AppBroker.CountryCodePrefix_PROP) && number.substring(3).startsWith("00"))) {
//            isValid = false;
//        }
        return isValid;
    }

    @SuppressWarnings("UseSpecificCatch")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ActionResponse actionResponse = null;

        response.setContentType("application/json");

        String formatType = null;

        try {
            formatType = removePoison(request.getParameter("formatType"));
        } catch (Exception e) {
            formatType = "json";
        }

        if (formatType.equalsIgnoreCase("xml")) {
            response.setContentType("application/xml");
        }

        actionResponse = new ActionResponse();

        String msisdn = "";
        PreparedStatement ps;

        String serviceID = null;
        ResultSet rs;
        int update = 0;

        try {
            msisdn = removePoison(request.getParameter("msisdn"));
        } catch (NullPointerException e) {
            actionResponse.setActionResponseCode("4");
            actionResponse.setActionResponseDescription("msisdn not set");
            printResponse(formatType, actionResponse, response);
            return;
        }

        if (!isNumeric(msisdn)) {
            actionResponse.setActionResponseCode("6");
            actionResponse.setActionResponseDescription("Invalid MSISDN.");
            printResponse(formatType, actionResponse, response);
            return;
        }

        try {
            serviceID = removePoison(request.getParameter("serviceID"));
        } catch (NullPointerException e) {
            actionResponse.setActionResponseCode("3");
            actionResponse.setActionResponseDescription("service id not set");
            printResponse(formatType, actionResponse, response);
            return;
        }

        String theNumber = msisdn.substring(msisdn.length() - 9);
        Statement st;

        try {
            st = getDBConnection().createStatement();

            rs = st.executeQuery("select sub_status from subscription where sub_msisdn ilike '%" + theNumber + "' and sub_status = 'active' ");
            boolean numberFound = false;

            while (rs.next()) {
                numberFound = true;
                if (serviceID.equalsIgnoreCase("all")) {
                    ps = getDBConnection().prepareStatement(
                            "update subscription set blacklisted = 'blacklisted', "
                                    + "blacklisted_date = CURRENT_TIMESTAMP, un_sub_date = CURRENT_TIMESTAMP, un_sub_source = 'WEB', sub_status = 'inactive' "
                                    + "where sub_msisdn like '%" + theNumber + "' ");
                    update = ps.executeUpdate();
                } else {
                    ps = getDBConnection().prepareStatement(
                            "update subscription set blacklisted = 'blacklisted', "
                                    + "blacklisted_date = CURRENT_TIMESTAMP, un_sub_date = CURRENT_TIMESTAMP, un_sub_source = 'WEB', sub_status = 'inactive' "
                                    + "where sub_msisdn like '%" + theNumber + "' and sub_service_id = " + serviceID + "  ");
                    update = ps.executeUpdate();
                }
            }

            if (!numberFound) {
                actionResponse.setActionResponseCode("2");
                actionResponse.setActionResponseDescription("msidn does not exist");
                printResponse(formatType, actionResponse, response);
                return;
            }

            if (update > 0) {
                actionResponse.setActionResponseCode("0");
                actionResponse.setActionResponseDescription("OK");
                printResponse(formatType, actionResponse, response);
            } else {
                actionResponse.setActionResponseCode("1");
                actionResponse.setActionResponseDescription("Failed");
                printResponse(formatType, actionResponse, response);
            }

        } catch (Exception ex) {
            actionResponse.setActionResponseCode("5");
            actionResponse.setActionResponseDescription("internal server error");
            printResponse(formatType, actionResponse, response);
        }
    }

    public void printResponse(
            String formatType,
            ActionResponse partnerSendSMSResponse,
            HttpServletResponse response) {

        try {
            out = response.getWriter();
        } catch (IOException ex) {
        }

        if (formatType.equalsIgnoreCase("xml")) {
            xstream = new XStream();
            xstream.alias("partnerSendSMSResponse", ActionResponse.class);
            String xml = xstream.toXML(partnerSendSMSResponse);
            out.println(xml);

        } else {
            json = new Gson().toJson(partnerSendSMSResponse);
            out.println(json);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
