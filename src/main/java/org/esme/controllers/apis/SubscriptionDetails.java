/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers.apis;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import org.apache.log4j.Logger;
import org.esme.controllers.apis.models.ActionResponse;
import org.esme.controllers.apis.models.SubscriptionList;
import org.esme.dao.models.Subscription;
import org.esme.dto.Service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author saheedalbert
 * @author tofunmi
 */
public class SubscriptionDetails {

    private static final Logger logger = Logger.getLogger(SubscriptionDetails.class);

    public static String getSubscriptionDetails(
            String subscriber, Service service,
            Connection dbConnection, String responseFormat, String network) {

        ArrayList<Subscription> jsonSubscriptionList = null;

        Subscription subscripton;
        Statement statement;
        ResultSet resultSet;
        String subMSISDN;
        XStream xstream = null;
        SubscriptionList xmlSubscriptionList = null;
        //ActionResponse actionResponse = null;
        ActionResponse actionResponse = null;

        String responseData = null;
        try {

            subMSISDN = subscriber.substring(subscriber.length() - 10);
            statement = dbConnection.createStatement();

            String query = "select a.id, b.service_sub_type, a.sub_service_id, a.un_sub_date,"
                    + "b.fallback_shortcode,b.service_period,b.welcome_shortcode, "
                    + "a.sub_request_text, a.blacklisted,a.sub_status,a.last_billed_date, \n"
                    + "            a.blacklisted_date, a.sub_request_time, a.un_sub_date,\n"
                    + "            a.sub_source, a.un_sub_source, a.last_billed_code,\n"
                    + "            a.sub_expiry_date, a.sub_service_period,b.service_price,\n"
                    + "            a.sub_network, b.service_owner, b.service_name, \n"
                    + "            b.service_shortcode, b.service_reminder_msg, \n"
                    + "            b.service_sub_type from subscription a, subscription_service b \n"
                    + "            where a.sub_network ilike '%" + network + "'\n"
                    + "            and a.sub_msisdn ilike '%" + subMSISDN + "'\n"
                    + "            and (a.sub_service_id = " + service.getServiceID() + " "
                    + "    or a.sub_request_text = '" + service.getServiceKeyword() + "' ) "
                    + "and a.sub_status = 'active' and  a.sub_service_id = b.id limit 1";


            logger.info("Get Active subscription query " + query);
            resultSet = statement.executeQuery(query);

            boolean numberFound = false;
            jsonSubscriptionList = new ArrayList<>();
            xmlSubscriptionList = new SubscriptionList();

            while (resultSet.next()) {

                numberFound = true;
                subscripton = new Subscription();
                subscripton.setSubscriptionID(resultSet.getString("id"));
                subscripton.setServiceID(resultSet.getString("sub_service_id"));
                subscripton.setSubscriptionServiceName(resultSet.getString("service_name"));
                subscripton.setSubscriptionReqTime(resultSet.getString("sub_request_time"));
                subscripton.setSubRequestText(resultSet.getString("sub_request_text"));
                subscripton.setSubscriptionServiceShortCode(resultSet.getString("service_shortcode"));
                subscripton.setSubscriptionServiceNetwork(resultSet.getString("sub_network"));
                subscripton.setSubscriptionStatus(resultSet.getString("sub_status"));
                subscripton.setSubscriptonLastCharged(resultSet.getString("last_billed_date"));
                subscripton.setSubscriptionNextCharged(resultSet.getString("sub_expiry_date"));
                subscripton.setSubscriptionSource(resultSet.getString("sub_source"));
                subscripton.setSubscriptionExpiryDate(resultSet.getString("sub_expiry_date"));
                subscripton.setSubscriptionAmount(resultSet.getString("service_price"));
                subscripton.setUnSubSource(resultSet.getString("un_sub_source"));
                subscripton.setBlacklisted(resultSet.getString("blacklisted"));
                subscripton.setBlacklistedDate(resultSet.getString("blacklisted_date"));
                subscripton.setSubscriptionType(resultSet.getString("service_sub_type"));
                subscripton.setSubscriptonFallbackBillingCode(resultSet.getString("fallback_shortcode"));
                subscripton.setSubscriptionPeriod(resultSet.getString("service_period"));
                subscripton.setSubscriptonMainBillingCode(resultSet.getString("welcome_shortcode"));
                subscripton.setUnSubDate(resultSet.getString("un_sub_date"));

                if (responseFormat.equalsIgnoreCase("xml")) {
                    xmlSubscriptionList.add(subscripton);
                } else {
                    jsonSubscriptionList.add(subscripton);
                }

            }

            actionResponse = new ActionResponse();

            if (!numberFound) {

                actionResponse.setActionResponseCode("2");
                actionResponse.setActionResponseDescription("msidn does not exist");
                //printResponse(formatType, actionResponse, response);
                if (responseFormat.equalsIgnoreCase("xml")) {
                    responseData = xstream.toXML(actionResponse);
                } else {
                    responseData = new Gson().toJson(actionResponse);
                }

            } else {

                if (responseFormat.equalsIgnoreCase("xml")) {
                    responseData = xstream.toXML(xmlSubscriptionList);
                } else {
                    responseData = new Gson().toJson(jsonSubscriptionList);
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            actionResponse.setActionResponseCode("5");
            actionResponse.setActionResponseDescription("internal server error.");
            //printResponse(formatType, actionResponse, response);
            if (responseFormat.equalsIgnoreCase("xml")) {
                responseData = xstream.toXML(actionResponse);
            } else {
                responseData = new Gson().toJson(actionResponse);
            }
        }
        return responseData;
    }

    public static String getSubscriptionDetails(
            String subscriber, Service service,
            Connection dbConnection, String responseFormat) {

        ArrayList<Subscription> jsonSubscriptionList = null;

        Subscription subscripton;
        Statement statement;
        ResultSet resultSet;
        String subMSISDN;
        XStream xstream = null;
        SubscriptionList xmlSubscriptionList = null;
        //ActionResponse actionResponse = null;
        ActionResponse actionResponse = null;

        String responseData = null;
        try {

            subMSISDN = subscriber.substring(subscriber.length() - 10);
            statement = dbConnection.createStatement();

            resultSet = statement.executeQuery(
                    "select a.id, b.service_sub_type, a.sub_service_id, a.un_sub_date,"
                            + "b.fallback_shortcode,b.service_period,b.welcome_shortcode, "
                            + "a.sub_request_text, a.blacklisted,a.sub_status,a.last_billed_date, \n"
                            + "            a.blacklisted_date, a.sub_request_time, a.un_sub_date,\n"
                            + "            a.sub_source, a.un_sub_source, a.last_billed_code,\n"
                            + "            a.sub_expiry_date, a.sub_service_period,b.service_price,\n"
                            + "            a.sub_network, b.service_owner, b.service_name, \n"
                            + "            b.service_shortcode, b.service_reminder_msg, \n"
                            + "            b.service_sub_type from subscription a, subscription_service b \n"
                            + "            where a.sub_msisdn ilike '%" + subMSISDN + "'\n"
                            + "            and (a.sub_service_id = " + service.getServiceID() + " "
                            + "    or a.sub_request_text = '" + service.getServiceKeyword() + "' ) "
                            + "and a.sub_status = 'active' and  a.sub_service_id = b.id limit 1");


            boolean numberFound = false;
            jsonSubscriptionList = new ArrayList<>();
            xmlSubscriptionList = new SubscriptionList();

            while (resultSet.next()) {

                numberFound = true;
                subscripton = new Subscription();
                subscripton.setSubscriptionID(resultSet.getString("id"));
                subscripton.setServiceID(resultSet.getString("sub_service_id"));
                subscripton.setSubscriptionServiceName(resultSet.getString("service_name"));
                subscripton.setSubscriptionReqTime(resultSet.getString("sub_request_time"));
                subscripton.setSubRequestText(resultSet.getString("sub_request_text"));
                subscripton.setSubscriptionServiceShortCode(resultSet.getString("service_shortcode"));
                subscripton.setSubscriptionServiceNetwork(resultSet.getString("sub_network"));
                subscripton.setSubscriptionStatus(resultSet.getString("sub_status"));
                subscripton.setSubscriptonLastCharged(resultSet.getString("last_billed_date"));
                subscripton.setSubscriptionNextCharged(resultSet.getString("sub_expiry_date"));
                subscripton.setSubscriptionSource(resultSet.getString("sub_source"));
                subscripton.setSubscriptionExpiryDate(resultSet.getString("sub_expiry_date"));
                subscripton.setSubscriptionAmount(resultSet.getString("service_price"));
                subscripton.setUnSubSource(resultSet.getString("un_sub_source"));
                subscripton.setBlacklisted(resultSet.getString("blacklisted"));
                subscripton.setBlacklistedDate(resultSet.getString("blacklisted_date"));
                subscripton.setSubscriptionType(resultSet.getString("service_sub_type"));
                subscripton.setSubscriptonFallbackBillingCode(resultSet.getString("fallback_shortcode"));
                subscripton.setSubscriptionPeriod(resultSet.getString("service_period"));
                subscripton.setSubscriptonMainBillingCode(resultSet.getString("welcome_shortcode"));
                subscripton.setUnSubDate(resultSet.getString("un_sub_date"));

                if (responseFormat.equalsIgnoreCase("xml")) {
                    xmlSubscriptionList.add(subscripton);
                } else {
                    jsonSubscriptionList.add(subscripton);
                }

            }

            actionResponse = new ActionResponse();

            if (!numberFound) {

                actionResponse.setActionResponseCode("2");
                actionResponse.setActionResponseDescription("msidn does not exist");
                //printResponse(formatType, actionResponse, response);
                if (responseFormat.equalsIgnoreCase("xml")) {
                    responseData = xstream.toXML(actionResponse);
                } else {
                    responseData = new Gson().toJson(actionResponse);
                }

            } else {

                if (responseFormat.equalsIgnoreCase("xml")) {
                    responseData = xstream.toXML(xmlSubscriptionList);
                } else {
                    responseData = new Gson().toJson(jsonSubscriptionList);
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            actionResponse.setActionResponseCode("5");
            actionResponse.setActionResponseDescription("internal server error.");
            //printResponse(formatType, actionResponse, response);
            if (responseFormat.equalsIgnoreCase("xml")) {
                responseData = xstream.toXML(actionResponse);
            } else {
                responseData = new Gson().toJson(actionResponse);
            }
        }
        return responseData;
    }

}
