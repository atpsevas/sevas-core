package org.esme.controllers.apis;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import org.esme.broker.AppBroker;
import org.esme.controllers.apis.models.ActionResponse;
import org.esme.dao.models.Users;
import org.esme.dto.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author saheedalbert
 */
@SuppressWarnings("serial")
public class PartnerSendSMSAPI extends AppBroker {

    Users users;
    ArrayList<Users> usersCache;
    ArrayList<Service> serviceCache;
    PrintWriter out;
    String json;
    XStream xstream = null;
    ActionResponse partnerSendSMSResponse = null;
    private String partnerUserID;
    private String partnerPassword;
    private String partnerServiceName;
    private String partnerServiceNetwork;
    private long partnerServiceID;
    private String partnerServiceShortCode;
    private String partnerMessage;
    private String partnerRecipient;
    private ArrayList<Users> allUsers;

    public static boolean isNumeric(String msisdn) {

        boolean isValid = false;
        long NumberLenght = 0;
        String number = msisdn.replace("+", "");

        if (number.startsWith(AppBroker.CountryCodePrefix_PROP)) {
            NumberLenght = 13;
        } else if (number.startsWith("0")) {
            NumberLenght = 11;
        }

        /*Number: A numeric value will have following format:
         ^[-+]?: Starts with an optional "+" or "-" sign.
         [0-9]*: May have one or more digits.
         \\.? : May contain an optional "." (decimal point) character.
         [0-9]+$ : ends with numeric digit.
         */
        //Initialize reg ex for numeric data.
        String expression = "^[-+]?[0-9]*\\.?[0-9]+$";
        CharSequence inputStr = number;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches() && ((NumberLenght == 13) || (NumberLenght == 11))) {
            isValid = true;
        }

        //it can't star with 00
        if (number.startsWith("00") || (number.startsWith(AppBroker.CountryCodePrefix_PROP) && number.substring(3).startsWith("00"))) {
            isValid = false;
        }

        return isValid;
    }

    @SuppressWarnings({"UseSpecificCatch", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");

        String formatType = null;

        try {

            try {
                formatType = removePoison(request.getParameter("formatType"));
            } catch (Exception e) {
                formatType = "json";
            }

            if (formatType.equalsIgnoreCase("xml")) {
                response.setContentType("application/xml");
            }

            partnerSendSMSResponse = new ActionResponse();

            //out = response.getWriter();
            try {
                partnerUserID = removePoison(request.getParameter("puid"));
                partnerPassword = removePoison(request.getParameter("ppwd"));
            } catch (NullPointerException e) {
                partnerSendSMSResponse.setActionResponseCode("1");
                partnerSendSMSResponse.setActionResponseDescription("user name or password not set");
                printResponse(formatType, partnerSendSMSResponse, response);
                return;
            }

            allUsers = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
            if (allUsers.size() < 1) {
                updateUsers();
                allUsers = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
            }

            //Validate user ID....
            boolean isUserExist = false;
            for (Users user : allUsers) {
                if (user.getUserName().equals(partnerUserID) && user.getPassword().equals(partnerPassword)) {
                    isUserExist = true;
                    break;
                }
            }

            if (!isUserExist) {
                partnerSendSMSResponse.setActionResponseCode("2");
                partnerSendSMSResponse.setActionResponseDescription("invalid user id or password");
                printResponse(formatType, partnerSendSMSResponse, response);
                return;
            }

            //Check if user exist....
            //org.esme.broker.AppBroker.sevasLogger.info("USER NAME " + user.getUserName() + " USER PASSWORD " + user.getPassword());
            //if (user.getUserName().equals(partnerUserID) && user.getPassword().equals(partnerPassword)) {
            org.esme.broker.AppBroker.sevasLogger.info("ENTERED USER NAME " + partnerUserID + " ENTERED USER PASSWORD " + partnerPassword);

            partnerServiceID = Long.parseLong(removePoison(request.getParameter("psid").trim()));
            partnerServiceShortCode = removePoison(request.getParameter("pssc").trim());

            try {

                //Access cached services...
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");

                //Check services archive availabbility
                if (serviceCache.isEmpty()) {
                    updateServicesCache();
                    serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                }

                boolean serviceExist = false;
                for (Service service : serviceCache) {

                    if (service.getServiceID() == partnerServiceID) {

                        serviceExist = true;
                        //Validate Sender ID
                        if (partnerServiceShortCode.equalsIgnoreCase(service.getServiceMTShortCode())
                                || partnerServiceShortCode.equalsIgnoreCase(service.getServiceShortCode())
                                || partnerServiceShortCode.equalsIgnoreCase(service.getServiceBillingShortCode())) {
                        } else {
                            partnerSendSMSResponse.setActionResponseCode("3");
                            partnerSendSMSResponse.setActionResponseDescription("unknow sender ID");
                            printResponse(formatType, partnerSendSMSResponse, response);
                            return;
                        }

                        //Get Service Name
                        partnerServiceName = service.getServiceName();

                        //Get Serviec Neywork
                        if (partnerServiceShortCode.equalsIgnoreCase(service.getServiceBillingShortCode())) {
                            partnerServiceNetwork = service.getServiceBillingNetwork();
                        } else {
                            partnerServiceNetwork = service.getServiceNetwork();
                        }
                        break;
                    }
                }

                //Service does not exist...
                if (!serviceExist) {
                    partnerSendSMSResponse.setActionResponseCode("4");
                    partnerSendSMSResponse.setActionResponseDescription("invalid service ID. ");
                    printResponse(formatType, partnerSendSMSResponse, response);
                    return;
                }

                partnerMessage = removePoison(request.getParameter("pm"));
                partnerRecipient = removePoison(request.getParameter("pr"));

                if (!isNumeric(partnerRecipient)) {
                    partnerSendSMSResponse.setActionResponseCode("5");
                    partnerSendSMSResponse.setActionResponseDescription("Invalid MSISDN.");
                    printResponse(formatType, partnerSendSMSResponse, response);
                    return;
                }
                //srcModule = removePoison(request.getParameter("sm"));

            } catch (NullPointerException e) {
                partnerSendSMSResponse.setActionResponseCode("6");
                partnerSendSMSResponse.setActionResponseDescription("required parameter(s) missing");
                printResponse(formatType, partnerSendSMSResponse, response);
                return;
            }

            try {

                getSendSMS().SMSBOX(
                        "MT", partnerServiceShortCode, partnerRecipient,
                        formatText(partnerMessage),
                        partnerServiceName,
                        partnerServiceID,
                        "SendSMS", partnerServiceNetwork,
                        getServletContext().getInitParameter("dlrMask"));

                partnerSendSMSResponse.setActionResponseCode("0");
                partnerSendSMSResponse.setActionResponseDescription("ok");
                printResponse(formatType, partnerSendSMSResponse, response);

            } catch (Exception e) {
                partnerSendSMSResponse.setActionResponseCode("7");
                partnerSendSMSResponse.setActionResponseDescription("could not send message");
                printResponse(formatType, partnerSendSMSResponse, response);
            }

        } catch (Exception e) {
            partnerSendSMSResponse.setActionResponseCode("8");
            partnerSendSMSResponse.setActionResponseDescription("internal server error");
            printResponse(formatType, partnerSendSMSResponse, response);
        }

    }

    public void printResponse(
            String formatType,
            ActionResponse partnerSendSMSResponse,
            HttpServletResponse response) {
        try {
            out = response.getWriter();
        } catch (IOException ex) {
        }

        if (formatType.equalsIgnoreCase("xml")) {
            xstream = new XStream();
            xstream.alias("partnerSendSMSResponse", ActionResponse.class);
            String xml = xstream.toXML(partnerSendSMSResponse);
            out.println(xml);

        } else {
            json = new Gson().toJson(partnerSendSMSResponse);
            out.println(json);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "Convert2Diamond"})
    public void updateUsers() {
        org.esme.broker.AppBroker.sevasLogger.info("Updating users...");
        usersCache = new ArrayList<Users>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(getServletContext().getInitParameter("usersQuery"));
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                //update previously stored job contents from the container...
                while (resultSet.next()) {
                    users = new Users();
                    users.setId(resultSet.getInt("id"));
                    users.setUserName(resultSet.getString("username"));
                    users.setPassword(resultSet.getString("password"));
                    usersCache.add(users);
                }
                //Create cache of all users
                org.esme.broker.AppBroker.sevasLogger.info("Total Users " + usersCache.size());
                getServletContext().setAttribute("usersCache", usersCache);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
