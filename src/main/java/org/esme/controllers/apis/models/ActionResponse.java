/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers.apis.models;

/**
 * @author saheedalbert
 */
public class ActionResponse {

    private String actionResponseCode;
    private String actionResponseDescription;

    public String getActionResponseCode() {
        return actionResponseCode;
    }

    public void setActionResponseCode(String sendSMSResponseCode) {
        this.actionResponseCode = sendSMSResponseCode;
    }

    public String getActionResponseDescription() {
        return actionResponseDescription;
    }

    public void setActionResponseDescription(String sendSMSResponseDescription) {
        this.actionResponseDescription = sendSMSResponseDescription;
    }

}
