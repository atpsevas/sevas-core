/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers.apis.models;

/**
 * @author saheedalbert
 */
public class ExternalReportData {

    long monthTargetRev;
    private long inceptionTillDateRev;
    private long extraPolatedRev;
    private long todayRev;
    private long dailyAverageRev;
    private long actualRev;
    private String platformURL;

    public String getPlatformURL() {
        return platformURL;
    }

    public void setPlatformURL(String platformURL) {
        this.platformURL = platformURL;
    }


    public long getInceptionTillDateRev() {
        return inceptionTillDateRev;
    }

    public void setInceptionTillDateRev(long inceptionTillDateRev) {
        this.inceptionTillDateRev = inceptionTillDateRev;
    }

    public long getExtraPolatedRev() {
        return extraPolatedRev;
    }

    public void setExtraPolatedRev(long extraPolatedRev) {
        this.extraPolatedRev = extraPolatedRev;
    }

    public long getTodayRev() {
        return todayRev;
    }

    public void setTodayRev(long todayRev) {
        this.todayRev = todayRev;
    }

    public long getDailyAverageRev() {
        return dailyAverageRev;
    }

    public void setDailyAverageRev(long dailyAverageRev) {
        this.dailyAverageRev = dailyAverageRev;
    }

    public long getMonthTargetRev() {
        return monthTargetRev;
    }

    public void setMonthTargetRev(long monthTargetRev) {
        this.monthTargetRev = monthTargetRev;
    }

    public long getActualRev() {
        return actualRev;
    }

    public void setActualRev(long actualRev) {
        this.actualRev = actualRev;
    }
}
