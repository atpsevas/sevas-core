/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers.apis.models;

import org.esme.dao.models.Subscription;

import java.util.ArrayList;
import java.util.List;

/**
 * @author saheedalbert
 */
public class SubscriptionList {

    private List<Subscription> list;

    public SubscriptionList() {
        list = new ArrayList<Subscription>();
    }

    public void add(Subscription p) {
        list.add(p);
    }

}
