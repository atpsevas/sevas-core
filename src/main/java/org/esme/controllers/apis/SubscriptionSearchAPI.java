package org.esme.controllers.apis;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import org.esme.broker.AppBroker;
import org.esme.controllers.apis.models.ActionResponse;
import org.esme.controllers.apis.models.SubscriptionList;
import org.esme.dao.models.Subscription;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SubscriptionSearchAPI extends AppBroker {

    private static final long serialVersionUID = 1L;

    PrintWriter out;

    String json;
    XStream xstream = null;

    public static boolean isNumeric(String msisdn) {

        boolean isValid = false;
        long NumberLenght = 0;
        String number = msisdn.replace("+", "");

        if (number.startsWith(AppBroker.CountryCodePrefix_PROP)) {
            NumberLenght = 13;
        } else if (number.startsWith("0")) {
            NumberLenght = 11;
        }

        /*Number: A numeric value will have following format:
         ^[-+]?: Starts with an optional "+" or "-" sign.
         [0-9]*: May have one or more digits.
         \\.? : May contain an optional "." (decimal point) character.
         [0-9]+$ : ends with numeric digit.
         */
        //Initialize reg ex for numeric data.
        String expression = "^[-+]?[0-9]*\\.?[0-9]+$";
        CharSequence inputStr = number;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches() && ((NumberLenght == 13) || (NumberLenght == 11))) {
            isValid = true;
        }

//        //it can't star with 00
//        if (number.startsWith("00") || (number.startsWith(AppBroker.CountryCodePrefix_PROP) && number.substring(3).startsWith("00"))) {
//            isValid = false;
//        }
        return isValid;
    }

    @SuppressWarnings({"CallToPrintStackTrace", "unchecked", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            out = response.getWriter();
        } catch (IOException ex) {
        }

        Subscription subscripton;
        Statement statement;
        ResultSet resultSet;
        String subMSISDN;
        String formatType;
        SubscriptionList xmlSubscriptionList = null;
        //ActionResponse actionResponse = null;
        ActionResponse actionResponse = null;

        ArrayList<Subscription> jsonSubscriptionList = null;

        try {
            formatType = removePoison(request.getParameter("formatType"));
        } catch (Exception e) {
            formatType = "json";
        }

        if (formatType.equalsIgnoreCase("xml")) {

            response.setContentType("application/xml");
            xstream = new XStream();
            xstream.alias("subscription", Subscription.class);
            xstream.alias("subscriptionList", SubscriptionList.class);
            xstream.addImplicitCollection(SubscriptionList.class, "list");
            xmlSubscriptionList = new SubscriptionList();

        } else {
            response.setContentType("application/json");
            jsonSubscriptionList = new ArrayList<>();
        }

        actionResponse = new ActionResponse();

        try {

            subMSISDN = removePoison(request.getParameter("msisdn").substring(request.getParameter("msisdn").length() - 9));
            statement = getDBConnection().createStatement();

            resultSet = statement.executeQuery(
                    "select a.id, b.service_sub_type, a.sub_service_id, a.un_sub_date,"
                            + "b.fallback_shortcode,b.service_period,b.welcome_shortcode, "
                            + "a.sub_request_text, a.blacklisted,a.sub_status,a.last_billed_date, \n"
                            + "            a.blacklisted_date, a.sub_request_time, a.un_sub_date,\n"
                            + "            a.sub_source, a.un_sub_source, a.last_billed_code,\n"
                            + "            a.sub_expiry_date, a.sub_service_period,b.service_price,\n"
                            + "            a.sub_network, b.service_owner, b.service_name, \n"
                            + "            b.service_shortcode, b.service_reminder_msg, \n"
                            + "            b.service_sub_type from subscription a, subscription_service b \n"
                            + "            where a.sub_msisdn ilike '%" + subMSISDN + "'\n"
                            + "            and a.sub_service_id = b.id");

            boolean numberFound = false;

            while (resultSet.next()) {

                numberFound = true;
                subscripton = new Subscription();
                subscripton.setSubscriptionID(resultSet.getString("id"));
                subscripton.setServiceID(resultSet.getString("sub_service_id"));
                subscripton.setSubscriptionServiceName(resultSet.getString("service_name"));
                subscripton.setSubscriptionReqTime(resultSet.getString("sub_request_time"));
                subscripton.setSubRequestText(resultSet.getString("sub_request_text"));
                subscripton.setSubscriptionServiceShortCode(resultSet.getString("service_shortcode"));
                subscripton.setSubscriptionServiceNetwork(resultSet.getString("sub_network"));
                subscripton.setSubscriptionStatus(resultSet.getString("sub_status"));
                subscripton.setSubscriptonLastCharged(resultSet.getString("last_billed_date"));
                subscripton.setSubscriptionNextCharged(resultSet.getString("sub_expiry_date"));
                subscripton.setSubscriptionSource(resultSet.getString("sub_source"));
                subscripton.setSubscriptionExpiryDate(resultSet.getString("sub_expiry_date"));
                subscripton.setSubscriptionAmount(resultSet.getString("service_price"));
                subscripton.setUnSubSource(resultSet.getString("un_sub_source"));
                subscripton.setBlacklisted(resultSet.getString("blacklisted"));
                subscripton.setBlacklistedDate(resultSet.getString("blacklisted_date"));
                subscripton.setSubscriptionType(resultSet.getString("service_sub_type"));
                subscripton.setSubscriptonFallbackBillingCode(resultSet.getString("fallback_shortcode"));
                subscripton.setSubscriptionPeriod(resultSet.getString("service_period"));
                subscripton.setSubscriptonMainBillingCode(resultSet.getString("welcome_shortcode"));
                subscripton.setUnSubDate(resultSet.getString("un_sub_date"));

                if (formatType.equalsIgnoreCase("xml")) {
                    xmlSubscriptionList.add(subscripton);
                } else {
                    jsonSubscriptionList.add(subscripton);
                }

            }

            if (!numberFound) {
                actionResponse.setActionResponseCode("2");
                actionResponse.setActionResponseDescription("msidn does not exist");
                //printResponse(formatType, actionResponse, response);

                if (formatType.equalsIgnoreCase("xml")) {
                    String xml = xstream.toXML(actionResponse);
                    out.println(xml);
                } else {
                    json = new Gson().toJson(actionResponse);
                    out.println(json);
                }
                return;
            }

            if (formatType.equalsIgnoreCase("xml")) {
                String xml = xstream.toXML(xmlSubscriptionList);
                out.println(xml);
            } else {
                json = new Gson().toJson(jsonSubscriptionList);
                out.println(json);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            actionResponse.setActionResponseCode("5");
            actionResponse.setActionResponseDescription("internal server error.");
            //printResponse(formatType, actionResponse, response);
            if (formatType.equalsIgnoreCase("xml")) {
                String xml = xstream.toXML(actionResponse);
                out.println(xml);
            } else {
                json = new Gson().toJson(actionResponse);
                out.println(json);
            }
        }

    }

    public void printResponse(
            String formatType,
            ActionResponse aResponse,
            HttpServletResponse response) {
        try {
            out = response.getWriter();
        } catch (IOException ex) {
        }

        if (formatType.equalsIgnoreCase("xml")) {
            xstream = new XStream();
            xstream.alias("ActionResponse", ActionResponse.class);
            String xml = xstream.toXML(aResponse);
            out.println(xml);

        } else {
            json = new Gson().toJson(aResponse);
            out.println(json);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
