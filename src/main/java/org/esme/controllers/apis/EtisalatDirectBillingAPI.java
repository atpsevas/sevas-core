package org.esme.controllers.apis;

import org.esme.broker.AppBroker;
import org.esme.dto.Service;
import org.esme.webservices.etisalat.EtisalatDirectBilling;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class EtisalatDirectBillingAPI extends AppBroker {

    static EtisalatDirectBilling etisalatDirectBilling;
    ArrayList<Service> serviceCache;
    String billingContent;
    String subscriber;
    String sourceModule;
    long serviceID;

    @SuppressWarnings({"unchecked", "unchecked", "CallToPrintStackTrace", "unchecked", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        try {
            subscriber = removePoison(request.getParameter("subscriber"));
        } catch (Exception e) {
            out.print("1: MSISDN REQUIRED");
            return;
        }

        try {
            billingContent = removePoison(request.getParameter("content"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            sourceModule = removePoison(request.getParameter("src_module"));
        } catch (Exception e) {
            e.printStackTrace();
            out.print("1: SOURCE MODULE REQUIRED");
            return;
        }

        try {
            serviceID = Long.parseLong(removePoison(request.getParameter("service_id")));
        } catch (Exception e) {
            out.print("1: SERVICE ID REQUIRED");
            return;
        }

        serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
        etisalatDirectBilling = EtisalatDirectBilling.getInstance();
        for (Service service : serviceCache) {
            if (service.getServiceID() == serviceID) {
                etisalatDirectBilling.makeEtisalatDirectBillingBillingRequest(
                        service, sourceModule, service.getServiceBillingShortCode(),
                        "NILL", billingContent, subscriber);
                out.print("0: OK");
                return;
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
