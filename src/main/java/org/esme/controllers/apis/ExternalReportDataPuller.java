package org.esme.controllers.apis;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import org.esme.broker.AppBroker;
import org.esme.controllers.apis.models.ExternalReportData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Calendar;

@SuppressWarnings("serial")
public class ExternalReportDataPuller extends AppBroker {

    long monthTargetRev;
    ExternalReportData externalReportData = null;
    PreparedStatement statement;
    ResultSet resultSet;
    PrintWriter out;
    String json;
    XStream xstream = null;
    private long inceptionTillDateRev;
    private long extraPolatedRev;
    private long todayRev;
    private long dailyAverageRev;
    private long actualRev;
    private String platformName;

    static public String customFormat(long value) {
        DecimalFormat myFormatter = new DecimalFormat("###,###");
        return myFormatter.format(value);
    }

    @SuppressWarnings({"CallToPrintStackTrace", "UseSpecificCatch"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try {
            out = response.getWriter();
        } catch (IOException ex) {
        }

        String formatType = "json";

        try {
            formatType = removePoison(request.getParameter("formatType"));
        } catch (Exception e) {
            formatType = "json";
        }

        if (formatType.equalsIgnoreCase("xml")) {
            response.setContentType("application/xml");
            xstream = new XStream();
            xstream.alias("externalReportData", ExternalReportData.class);
        } else {
            response.setContentType("application/json");
        }

        Calendar cal = Calendar.getInstance();
        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        int totalDayofMonth = cal.getMaximum(Calendar.DAY_OF_MONTH);
        org.esme.broker.AppBroker.sevasLogger.info("DAYS IN MONTH" + totalDayofMonth);

        try {

            try {
                monthTargetRev = Long.parseLong(monthlyRevTarget_PROP);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                inceptionTillDateRev = getRevenueData("incepTillDateRev");
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                actualRev = getRevenueData("monthTillDateRev");
            } catch (Exception e) {

            }

            try {
                dailyAverageRev = actualRev / dayOfMonth;
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                todayRev = getRevenueData("todayRev");
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                extraPolatedRev = dailyAverageRev * totalDayofMonth;
            } catch (Exception e) {
                e.printStackTrace();
            }

            //build the data....
            externalReportData = new ExternalReportData();
            externalReportData.setPlatformURL(
                    request.getScheme() + "://" +
                            request.getServerName() +
                            request.getContextPath());
            externalReportData.setInceptionTillDateRev(inceptionTillDateRev);
            externalReportData.setTodayRev(todayRev);
            externalReportData.setDailyAverageRev(dailyAverageRev);
            externalReportData.setExtraPolatedRev(extraPolatedRev);
            externalReportData.setTodayRev(todayRev);
            externalReportData.setActualRev(actualRev);

            //Output data...
            if (formatType.equalsIgnoreCase("xml")) {
                String xml = xstream.toXML(externalReportData);
                out.println(xml);
            } else {
                json = new Gson().toJson(externalReportData);
                out.println(json);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    public long getRevenueData(String label) throws SQLException {
        Long revData;
        try {
            revData = (Long) getServletContext().getAttribute(label + "DATA");
        } catch (Exception ex) {
            org.esme.broker.AppBroker.sevasLogger.info(label + "DATA not set in Context");
            // Get query from context
            String query = (String) getServletContext().getAttribute(label);
            revData = (Long) getPeriodicRev(query);

            getServletContext().setAttribute(label + "DATA", revData);
        }
        org.esme.broker.AppBroker.sevasLogger.info("Label: " + revData);
        return revData;
    }

    public long getRevTarget(String query) throws SQLException {

        statement = getDBConnection().prepareStatement(query);
        resultSet = statement.executeQuery();
        long returnedRevenue = 0;
        while (resultSet.next()) {
            returnedRevenue = resultSet.getLong("theTarget");
        }
        return returnedRevenue;
    }

    public long getPeriodicRev(String query) throws SQLException {
        statement = getDBConnection().prepareStatement(query);
        resultSet = statement.executeQuery();
        long returnedRevenue = 0;
        while (resultSet.next()) {
            returnedRevenue = resultSet.getLong("main_charge") + resultSet.getLong("fallback_charge") + resultSet.getLong("fallback_charge_2");
        }
        return returnedRevenue;
    }

    public double getChurnRate(String query) throws SQLException {

        statement = getDBConnection().prepareStatement(query);
        resultSet = statement.executeQuery();
        double churn = 0;
        long totalNewSub = 0;
        long totalInactiveSub = 0;

        while (resultSet.next()) {
            totalNewSub = resultSet.getLong("total");
            totalInactiveSub = resultSet.getLong("inactive");
        }

        try {
            churn = (totalInactiveSub / totalNewSub) * totalNewSub;//(resultSet.getLong("total") - resultSet.getLong("active")) / resultSet.getLong("total");
        } catch (Exception e) {

        }
        DecimalFormat df = new DecimalFormat("#.#");
        df.format(churn);
        return Double.parseDouble(df.format(churn));
    }

}
