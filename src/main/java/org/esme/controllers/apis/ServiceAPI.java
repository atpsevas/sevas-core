/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers.apis;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import org.esme.broker.AppBroker;
import org.esme.dto.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * @author tm30
 */
public class ServiceAPI extends AppBroker {

    PrintWriter out;

    String json;
    XStream xstream = null;

    @SuppressWarnings("UseSpecificCatch")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        sevasLogger.info("Retrieving service");

        response.setContentType("application/json");
        out = response.getWriter();

        String serviceId = null;
        String requestedServicePayload = "";
        Gson gson = new Gson();

        try {
            serviceId = removePoison(request.getParameter("serviceId"));
        } catch (Exception ex) {
        }

        try {
            // Show all services

            ArrayList<Service> serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");

            if (serviceId == null) {
                requestedServicePayload = gson.toJson(serviceCache);
            } else {
                Service requestedService = Service.getService(Long.valueOf(serviceId));
                requestedServicePayload = gson.toJson(requestedService);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        out.println(requestedServicePayload);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
