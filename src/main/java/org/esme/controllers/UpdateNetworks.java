package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.Network;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class UpdateNetworks extends AppBroker {

    PreparedStatement pstmt;
    Network theNetwork;
    ArrayList<Network> networkCache;

    @SuppressWarnings("CallToThreadDumpStack")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();


        String action = removePoison(request.getParameter("action"));
        String networkID = request.getParameter("networkID");
        if (action.equalsIgnoreCase("delete")) {
            try {

                pstmt = getDBConnection().prepareStatement(
                        "delete from network where network_id ilike ? ");
                pstmt.setString(1, networkID);
                int update = pstmt.executeUpdate();
                if (update > 0) {
                    //update network cache..
                    updateNetwork();
                    request.setAttribute(
                            "message",
                            networkID + " successfully deleted.");
                    request.getRequestDispatcher("manage_networks.jsp").forward(request, response);
                } else {
                    request.setAttribute(
                            "message",
                            "No network  was deleted.");
                    request.getRequestDispatcher(
                            "manage_networks.jsp").forward(request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
                request.setAttribute(
                        "message",
                        "An error occured while trying to delete " + networkID);
                request.getRequestDispatcher("new_network.jsp").forward(request, response);
            } finally {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                }
            }

        } else if (action.equalsIgnoreCase("update")) {
            String prefixes = request.getParameter("prefixes");
            try {

                pstmt = getDBConnection().prepareStatement(
                        "update network set network_prefixes = ? where lower(network_id) = ?");
                pstmt.setString(1, prefixes);
                pstmt.setString(2, networkID.toLowerCase());
                int update = pstmt.executeUpdate();
                if (update > 0) {
                    //Update network cache..
                    updateNetwork();
                    request.setAttribute(
                            "message",
                            "Network successfully updated.");
                    request.getRequestDispatcher(
                            "new_network.jsp?networkID=" + networkID).forward(request, response);
                } else {
                    request.setAttribute("message", "No network was updated.");
                    request.getRequestDispatcher(
                            "new_network.jsp?networkID=" + networkID).forward(request, response);
                }
            } catch (SQLException e) {
                e.printStackTrace();
                request.setAttribute(
                        "message", "SEVERE: DB eror occured while trying to update " + networkID);
                request.getRequestDispatcher(
                        "new_network.jsp?networkID=" + networkID).forward(request, response);
            } finally {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                }
            }
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
