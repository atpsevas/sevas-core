package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.Users;
import org.esme.dto.Service;
import org.esme.utils.pingenerator.RandomPinGenerator;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class CreateNewService extends AppBroker {

    PreparedStatement pstmt;
    Service service;
    ServletContext context = null;
    ArrayList<Service> servicesCache;
    ArrayList<Users> userCache;
    long billingRetryIntervals, billingTime, fallbackExpiryPeriod, parentServiceID,
            serviceReminderTime, serviceOwner, serviceCategory, serviceBundleID;
    String fallbackShortCode2;
    String fallbackPrice2;
    long fallbackPeriod2;
    String serviceFallbackRenewalMessage2;
    String fallbackShortCode3;
    String fallbackPrice3;
    long fallbackPeriod3;
    String serviceFallbackRenewalMessage3;
    String instantBillingRetry;
    String fallbackShortCode4;
    String fallbackPrice4;
    long fallbackPeriod4;
    String serviceFallbackRenewalMessage4;
    String MainbillingnotifySMSSetting;
    String fallBack1billingnotifySMSSetting;
    String fallBack2billingnotifySMSSetting;
    String fallBack3billingnotifySMSSetting;
    String fallBack4billingnotifySMSSetting;
    //Service Parameters
    private String serviceSubType, serviceName, serviceMOShortCode, serviceMTShortCode,
            serviceSubKeyword, serviceUnSubKeyword, price,
            servicePeriod, welcomeShortCode, billingShortCode, fallbackShortCode, fallbackPrice,
            serviceTimeOfBlast, serviceSubMsg, serviceUnsubMsg, serviceRemMsg,
            serviceRemPeriod, serviceAllowableKeywords,
            subType, serviceContentDeliveryMethod,
            serviceRenewalMessage, serviceDescription,
            serviceHelpMessage, fallbackRenewalMessage, billingNetwork,
            serviceStatus, serviceNetwork, servicePromo, servicePromoPeriod, autoSubToParentService,
            serviceCrossSell, serviceCrossNetwork, serviceCrossSellShortCode,
            serviceCrossMessage, serviceConfirmation, serviceConfirmationKeyword,
            serviceConfirmationMessage, serviceContentSenderID;
    //routing...
    private String routeToExternalAPI;
    private String routeToExternalAPByShortCode;
    private String externalAPIURL;
    private String externalAPIShortCodeParam;
    private String externalAPIMSISDNParam;
    private String externalAPIMessageParam;

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "UseSpecificCatch", "unchecked", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        org.esme.broker.AppBroker.sevasLogger.error("PROCESS WAS CALLED");

        response.setContentType("text/html;charset=UTF-8");
        servicesCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
        userCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");

        try {
            try {
                serviceSubType = removePoison(request.getParameter("sst").trim());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceName = removePoison(request.getParameter("sn").trim());
            } catch (Exception e) {
                request.setAttribute("message", "*Service name cannot be empty.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                serviceNetwork = removePoison(request.getParameter("net").trim());
            } catch (Exception e) {
                request.setAttribute("message", "*Please select service network.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                billingNetwork = removePoison(request.getParameter("bill_net").trim());
            } catch (Exception e) {
                request.setAttribute("message", "*Please select billing network.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                serviceMOShortCode = removePoison(request.getParameter("ssc").trim());
            } catch (Exception e) {
                request.setAttribute("message", "*Service short code cannot be empty.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                serviceMTShortCode = removePoison(request.getParameter("mt_ssc"));
            } catch (Exception e) {
                request.setAttribute("message", "*Service MT short code cannot be empty.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                welcomeShortCode = removePoison(request.getParameter("wsc"));
            } catch (Exception e) {
                request.setAttribute("message", "*Service welcome message short code cannot be empty.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                billingShortCode = removePoison(request.getParameter("wc_ssc"));
            } catch (Exception e) {
                request.setAttribute("message", "*Service billing short code cannot be empty.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                billingRetryIntervals = Long.parseLong(removePoison(request.getParameter("sbi")));
            } catch (Exception e) {
                request.setAttribute("message", "*Service billing retry intervals cannot be empty.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                billingTime = Long.parseLong(removePoison(request.getParameter("sbt")));
            } catch (Exception e) {
                request.setAttribute("message", "*Service billing retry intervals cannot be empty.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                serviceReminderTime = Long.parseLong(removePoison(request.getParameter("srt")));
            } catch (Exception e) {
                request.setAttribute("message", "*Service subscription reminder time cannot be empty.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                serviceSubKeyword = removePoison(request.getParameter("skw"));
            } catch (Exception e) {
                request.setAttribute("message", "*Service keyword cannot be empty.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                serviceSubMsg = removePoison(request.getParameter("ssm").trim());
            } catch (Exception e) {
                request.setAttribute("message", "*Service subscription message cannot be empty.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                serviceUnsubMsg = removePoison(request.getParameter("sum").trim());
            } catch (Exception e) {
                request.setAttribute("message", "*Service unsubscription message cannot be empty.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                fallbackExpiryPeriod = Integer.parseInt(removePoison(request.getParameter("fsp").trim()));
            } catch (Exception e) {
                request.setAttribute("message", "*Service unsubscription message cannot be empty.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                fallbackRenewalMessage = removePoison(request.getParameter("frm").trim());
            } catch (Exception e) {
                request.setAttribute("message", "*Service fallback subscription message cannot be empty.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }

            try {
                serviceHelpMessage = removePoison(request.getParameter("shm").trim());
            } catch (Exception e) {
                request.setAttribute("message", "*Service help message cannot be empty.");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                return;
            }
            try {
                serviceStatus = removePoison(request.getParameter("status").trim());
            } catch (Exception e) {
                e.printStackTrace();
            }

            //Check if the service already exist
            for (Service theService : servicesCache) {
                if (theService.getServiceName().equalsIgnoreCase(serviceName)
                        && theService.getServiceShortcode().equalsIgnoreCase(serviceMOShortCode)) {
                    request.setAttribute("message", "'" + serviceName
                            + " already exist on '" + serviceMOShortCode + "'. ");
                    request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                    return;
                }
            }

//            try {
//                serviceContentDeliveryMethod = removePoison(request.getParameter("cdm").trim());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            try {
                serviceContentDeliveryMethod = removePoison(request.getParameter("cdm").trim());

                // Adding content type functionality
                String content_type = removePoison(request.getParameter("content_type").trim());
                if (content_type.equalsIgnoreCase("voice")) {
                    serviceContentDeliveryMethod = serviceContentDeliveryMethod + ",voice";
                } else {
                    serviceContentDeliveryMethod = serviceContentDeliveryMethod + ",sms";
                }
            } catch (NullPointerException e) {
            }

            try {
                serviceRenewalMessage = removePoison(request.getParameter("rm").trim());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceAllowableKeywords = removePoison(request.getParameter("sakw"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceUnSubKeyword = removePoison(request.getParameter("suk"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceCategory = Long.parseLong(removePoison(request.getParameter("sc")));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                price = removePoison(request.getParameter("price"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                servicePeriod = removePoison(request.getParameter("sp"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                subType = removePoison(request.getParameter("stype"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceTimeOfBlast = removePoison(request.getParameter("stob"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                serviceRemMsg = removePoison(request.getParameter("srm").trim());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceRemPeriod = removePoison(request.getParameter("srp"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceOwner = Long.parseLong(request.getParameter("so"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                fallbackShortCode = removePoison(request.getParameter("fb_ssc"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                fallbackPrice = removePoison(request.getParameter("fb_price"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                servicePromo = removePoison(request.getParameter("promo"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                servicePromoPeriod = removePoison(request.getParameter("promoPeriod"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceCrossSell = removePoison(request.getParameter("cross_sell"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceCrossNetwork = removePoison(request.getParameter("cross_net"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceCrossSellShortCode = removePoison(request.getParameter("cross_sc"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceCrossMessage = removePoison(request.getParameter("cross_msg"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                fallbackShortCode2 = removePoison(request.getParameter("fb_ssc2"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                fallbackPrice2 = removePoison(request.getParameter("fb_price2"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                fallbackPeriod2 = Long.parseLong(removePoison(request.getParameter("fsp2")));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                serviceFallbackRenewalMessage2 = removePoison(request.getParameter("frm2"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                fallbackShortCode3 = removePoison(request.getParameter("fb_ssc3"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                fallbackPrice3 = removePoison(request.getParameter("fb_price3"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                fallbackPeriod3 = Long.parseLong(removePoison(request.getParameter("fsp3")));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                serviceFallbackRenewalMessage3 = removePoison(request.getParameter("frm3"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                fallbackShortCode4 = removePoison(request.getParameter("fb_ssc4"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                fallbackPrice4 = removePoison(request.getParameter("fb_price4"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                fallbackPeriod4 = Long.parseLong(removePoison(request.getParameter("fsp4")));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                serviceFallbackRenewalMessage4 = removePoison(request.getParameter("frm4"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                MainbillingnotifySMSSetting = removePoison(request.getParameter("notify"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                fallBack1billingnotifySMSSetting = removePoison(request.getParameter("fb1_notify"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                fallBack2billingnotifySMSSetting = removePoison(request.getParameter("fb2_notify"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                fallBack3billingnotifySMSSetting = removePoison(request.getParameter("fb3_notify"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                fallBack4billingnotifySMSSetting = removePoison(request.getParameter("fb4_notify"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                instantBillingRetry = removePoison(request.getParameter("ibr"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                routeToExternalAPI = removePoison(request.getParameter("route"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                routeToExternalAPByShortCode = removePoison(request.getParameter("route_by_sc"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                externalAPIURL = removePoison(request.getParameter("routeurl"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                externalAPIShortCodeParam = removePoison(request.getParameter("routeshortcode"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                externalAPIMSISDNParam = removePoison(request.getParameter("routemsisdn"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                externalAPIMessageParam = removePoison(request.getParameter("routemessage"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                parentServiceID = Long.parseLong(removePoison(request.getParameter("parentServID")));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                autoSubToParentService = removePoison(request.getParameter("autoSubToParentServ"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceConfirmation = removePoison(request.getParameter("dbCon"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceConfirmationKeyword = removePoison(request.getParameter("dck"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceConfirmationMessage = removePoison(request.getParameter("dcm"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                serviceContentSenderID = removePoison(request.getParameter("csID"));
            } catch (Exception e) {
                e.printStackTrace();
            }


            //Generate an ID for the service.
            int id = RandomPinGenerator.randomPinGenerator(4);

            try {

                pstmt = getDBConnection().prepareStatement(
                        "insert into subscription_service"
                                + " (id, service_owner,service_name,service_shortcode,service_mt_shortcode,"
                                + "service_keyword,service_allowable_keyword,"
                                + "service_unsubcribe_keyword,service_category,"
                                + "service_price,service_period,"
                                + "service_tob,service_subscription_msg,service_unsubscription_msg,"
                                + "service_reminder_msg,service_reminder_period,"
                                + "service_description,sub_type,welcome_shortcode,"
                                + "fallback_shortcode, "
                                + "fallback_price,service_content_delivery_method,"
                                + "service_billing_retry_intervals,"
                                + "service_renewal_time_of_the_day,service_renewal_message,"
                                + "service_fallback_period,service_fallback_renewal_message,"
                                + "service_help_message,service_reminder_time,service_sub_type,"
                                + "service_network,service_wc_msg_shortcode,"
                                + "billing_network,service_status,service_promo,service_cross_sell"
                                + ",service_cross_sell_network,service_cross_sell_message,"
                                + "service_cross_sell_shortcode,fallback2_shortcode"
                                + ",fallback2_price,fallback2_period,fallback2_message,"
                                + "fallback3_shortcode,fallback3_price,fallback3_period,"
                                + "fallback3_message,instant_billing_retry,fallback4_shortcode,"
                                + "fallback4_price,fallback4_period,fallback4_message"
                                + ",main_billing_notify_settings,fallback1_billing_notify_settings,"
                                + "fallback2_billing_notify_settings,fallback3_billing_notify_settings,"
                                + "fallback4_billing_notify_settings,route_to_external_api,"
                                + "route_by_shortcode,external_api_url,external_api_sc_param,"
                                + "external_api_msisdn_param,external_api_msg_param,"
                                + "auto_sub_to_parent_serv,service_parent_id,"
                                + "service_double_confirm, service_double_confirm_keyword,"
                                + "service_double_confirm_message,service_promo_period,service_content_sender_id)"
                                + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
                                + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
                                + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

                pstmt.setInt(1, id);
                pstmt.setLong(2, serviceOwner);
                pstmt.setString(3, serviceName);
                pstmt.setString(4, serviceMOShortCode);
                pstmt.setString(5, serviceMTShortCode);
                pstmt.setString(6, serviceSubKeyword);
                pstmt.setString(7, serviceAllowableKeywords);
                pstmt.setString(8, serviceUnSubKeyword);
                pstmt.setLong(9, serviceCategory);
                pstmt.setLong(10, Long.parseLong(price));
                pstmt.setLong(11, Long.parseLong(servicePeriod));
                pstmt.setLong(12, Long.parseLong(serviceTimeOfBlast));
                pstmt.setString(13, serviceSubMsg);
                pstmt.setString(14, serviceUnsubMsg);
                pstmt.setString(15, serviceRemMsg);
                pstmt.setLong(16, Long.parseLong(serviceRemPeriod));
                pstmt.setString(17, serviceDescription);
                pstmt.setString(18, subType);
                pstmt.setString(19, billingShortCode);
                pstmt.setString(20, fallbackShortCode);
                pstmt.setLong(21, Long.parseLong(fallbackPrice));
                pstmt.setString(22, serviceContentDeliveryMethod);
                pstmt.setLong(23, billingRetryIntervals);
                pstmt.setLong(24, billingTime);
                pstmt.setString(25, serviceRenewalMessage);
                pstmt.setLong(26, fallbackExpiryPeriod);
                pstmt.setString(27, fallbackRenewalMessage);
                pstmt.setString(28, serviceHelpMessage);
                pstmt.setLong(29, serviceReminderTime);
                pstmt.setString(30, serviceSubType);
                pstmt.setString(31, serviceNetwork);
                pstmt.setString(32, welcomeShortCode);
                pstmt.setString(33, billingNetwork);
                pstmt.setString(34, serviceStatus);

                pstmt.setString(35, servicePromo);
                pstmt.setString(36, serviceCrossSell);
                pstmt.setString(37, serviceCrossNetwork);
                pstmt.setString(38, serviceCrossMessage);
                pstmt.setString(39, serviceCrossSellShortCode);

                pstmt.setString(40, fallbackShortCode2);
                pstmt.setString(41, fallbackPrice2);
                pstmt.setLong(42, fallbackPeriod2);
                pstmt.setString(43, serviceFallbackRenewalMessage2);

                pstmt.setString(44, fallbackShortCode3);
                pstmt.setString(45, fallbackPrice3);
                pstmt.setLong(46, fallbackPeriod3);
                pstmt.setString(47, serviceFallbackRenewalMessage3);

                pstmt.setString(48, instantBillingRetry);

                pstmt.setString(49, fallbackShortCode4);
                pstmt.setString(50, fallbackPrice4);
                pstmt.setLong(51, fallbackPeriod4);
                pstmt.setString(52, serviceFallbackRenewalMessage4);

                pstmt.setString(53, MainbillingnotifySMSSetting);
                pstmt.setString(54, fallBack1billingnotifySMSSetting);
                pstmt.setString(55, fallBack2billingnotifySMSSetting);
                pstmt.setString(56, fallBack3billingnotifySMSSetting);
                pstmt.setString(57, fallBack4billingnotifySMSSetting);

                //routing.
                pstmt.setString(58, routeToExternalAPI);
                pstmt.setString(59, routeToExternalAPByShortCode);
                pstmt.setString(60, externalAPIURL);
                pstmt.setString(61, externalAPIShortCodeParam);
                pstmt.setString(62, externalAPIMSISDNParam);
                pstmt.setString(63, externalAPIMessageParam);
                pstmt.setString(64, autoSubToParentService);
                pstmt.setLong(65, parentServiceID);

                pstmt.setString(66, serviceConfirmation);
                pstmt.setString(67, serviceConfirmationKeyword);
                pstmt.setString(68, serviceConfirmationMessage);
                pstmt.setString(69, servicePromoPeriod);
                pstmt.setString(70, serviceContentSenderID);

                int update = pstmt.executeUpdate();

                if (update < 1) {
                    request.setAttribute("message", "Failed to create service. Please try again");
                    request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                    return;
                }

            } catch (SQLException e) {
                request.setAttribute("message", "An error occurred while creating yourservice. Please try again later");
                request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
                e.printStackTrace();
                return;
            }
            //Update service cache...
            updateServicesCache();
            request.setAttribute("message", "Service successfully created.");
            request.getRequestDispatcher("new_sub_service.jsp?id=" + id + "").
                    forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
