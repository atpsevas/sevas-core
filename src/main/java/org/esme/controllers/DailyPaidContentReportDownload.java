package org.esme.controllers;

import com.csvreader.CsvWriter;
import org.esme.broker.AppBroker;
import org.esme.dao.models.Users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("serial")
public class DailyPaidContentReportDownload extends AppBroker {

    DecimalFormat formatter = new DecimalFormat("###,###,###");
    String query;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession sess = request.getSession(true);
            String user = (String) sess.getAttribute("userName");
            long userID = 0;
            String userRole = (String) sess.getAttribute("UserRole");

            @SuppressWarnings("unchecked")
            ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
            for (Users us : usersCache) {
                if (us.getUserName().equals(user)) {
                    userID = us.getId();
                    break;
                }
            }

            if (userRole.equals("Admin")) {
                query = getServletContext().getInitParameter("dailyPaidContentRevenue");
            } else {
                query
                        = " select a.service_name as theService, date(b.sent_time) as theDate,"
                        + "sum((CASE WHEN (b.src_module = 'DailyPaidContent' ) THEN 1 ELSE 0 END)) AS totalContent,"
                        + "sum((CASE WHEN (b.src_module = 'DailyPaidContent'  and b.delivery_status = 'SUBMITTED') THEN 1 ELSE 0 END)) AS totalDel,"
                        + "sum((CASE WHEN (b.src_module = 'DailyPaidContent'  and b.delivery_status != 'SUBMITTED') THEN 1 ELSE 0 END)) AS totalFailed,"
                        + "sum((CASE WHEN (b.src_module = 'DailyPaidContent' ) THEN 1 ELSE 0 END) * a.service_price) AS possibleRev,"
                        + "sum((CASE WHEN (b.src_module = 'DailyPaidContent'  and b.delivery_status = 'SUBMITTED') "
                        + "THEN 1 ELSE 0 END)* a.service_price) AS accruedRev,"
                        + "sum((CASE WHEN (b.src_module = 'DailyPaidContent'  and b.delivery_status != 'SUBMITTED') "
                        + "THEN 1 ELSE 0 END) * a.service_price) AS lossRev "
                        + "FROM subscription_service a, outgoing_messages b where extract(month from b.sent_time) = extract (month from now()) "
                        + "and extract(year from b.sent_time) = extract (year from now()) and a.service_owner = " + userID + " and a.id = b.serv_id"
                        + "group by theService,theDate order by theService,theDate Desc";
            }

            downloadReport(query);

//            long totalContent = 0;
//            long totalDelivered = 0;
//            long totalFailed = 0;
//            long possibleRevenue = 0;
//            long totalAccruedRevenue = 0;
//            long totallossRevenue = 0;
//            String nextService = "";
        } catch (Exception e) {
            e.printStackTrace();
        }
        // out.println("</table>");
        //doMonthTillDateReport();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @SuppressWarnings({"CallToPrintStackTrace", "UseSpecificCatch"})
    public void downloadReport(String query) {

        String outputFile = "/home/sevas/reports/";
        // before we open the file check to see if it already exists
        try {

            // use FileWriter constructor that specifies open for appending
            CsvWriter csvOutput = new CsvWriter(new FileWriter(outputFile + formatDateToString2(new Date()) + ".csv"), ',');
            // if the file didn't already exist then we need to write out the header line
            org.esme.broker.AppBroker.sevasLogger.info("Creating folder....");
            csvOutput.write(" ");
            csvOutput.write("Nigeria");
            csvOutput.endRecord();

            // write out headers.....
            csvOutput.write("Date");
            csvOutput.write("Total Content");
            csvOutput.write("Delivered");
            csvOutput.write("Failed");
            csvOutput.write("Possible Revenue");
            csvOutput.write("Accrued Revenue");
            csvOutput.write("Loss Revenue");
            csvOutput.endRecord();

            //Daily Revenue 
            ResultSet dailyTotalRevenue = getData(query);
            while (dailyTotalRevenue.next()) {
                //reset index
                int index = 0;
                while (index < 6) {
                    index++;
                    csvOutput.write(formatNumber(dailyTotalRevenue.getString(index)));
                    csvOutput.endRecord();
                }
            }
            csvOutput.endRecord();
            csvOutput.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getDayOfTheMonth() {
        Calendar cal = Calendar.getInstance();
        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        return dayOfMonth;
    }

    public String formatNumber(String theNumber) {
        NumberFormat myFormat = NumberFormat.getInstance();
        myFormat.setGroupingUsed(true);
        return myFormat.format(Long.parseLong(theNumber));
    }

    public ResultSet getData(String query) {
        @SuppressWarnings("UnusedAssignment")
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException ex) {
        }
        return resultSet;
    }

}
