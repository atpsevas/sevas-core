package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.ServiceCategory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class CreateCategory extends AppBroker {

    String serviceCategoryName, serviceCategoryKeyword;

    PreparedStatement pstm;
    Statement st;
    ResultSet rs;

    @SuppressWarnings("CallToPrintStackTrace")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            serviceCategoryName = removePoison(request.getParameter("catName"));
            serviceCategoryKeyword = removePoison(request.getParameter("catKeyword"));
            try {

                st = getDBConnection().createStatement();
                rs = st.executeQuery("select category_name from service_category "
                        + "where category_name ilike '" + serviceCategoryName + "' ");

                while (rs.next()) {
                    request.setAttribute("message", "Category named " + serviceCategoryName + " already exist.");
                    request.getRequestDispatcher("new_serv_category.jsp").forward(request, response);
                    return;
                }

                pstm = getDBConnection().prepareStatement(
                        "insert into service_category("
                                + "category_name, category_keyword) values(?,?)");
                pstm.setString(1, serviceCategoryName);
                pstm.setString(2, serviceCategoryKeyword);
                int update = pstm.executeUpdate();

                if (update > 0) {
                    //Refresh category archive
                    updateCategory();
                    request.setAttribute("message", serviceCategoryName + " successfully created");
                    request.getRequestDispatcher("new_serv_category.jsp").forward(request, response);
                    return;
                }
                request.setAttribute("message", "Failed to create " + serviceCategoryName + ". Please try again.");
                request.getRequestDispatcher("new_serv_category.jsp").forward(request, response);
            } catch (SQLException ex) {
                ex.printStackTrace();
                request.setAttribute("message", "error occured while creating " + serviceCategoryName + ". Please try again.");
                request.getRequestDispatcher("new_serv_category.jsp").forward(request, response);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    @SuppressWarnings({"CallToThreadDumpStack", "Convert2Diamond", "null"})
    public void updateCategory() {

        org.esme.broker.AppBroker.sevasLogger.info("Updating Category..");
        ArrayList<ServiceCategory> serviceCategoryCache = new ArrayList<ServiceCategory>();
        Statement statement = null;
        ResultSet resultSet = null;

        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(getServletContext().getInitParameter("categoryQuery"));
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {

                //update previously stored job contents from the container...
                while (resultSet.next()) {

                    ServiceCategory serviceCategory = new ServiceCategory();
                    serviceCategory.setCategoryID(resultSet.getLong("id"));
                    serviceCategory.setCategoryName(resultSet.getString("category_name"));
                    serviceCategory.setCategoryKeyword(resultSet.getString("category_keyword"));
                    serviceCategory.setCategoryDateCreated(resultSet.getString("date_created"));
                    serviceCategoryCache.add(serviceCategory);
                }
                //Create cache of all users
                org.esme.broker.AppBroker.sevasLogger.info("Total Category " + serviceCategoryCache.size());
                getServletContext().setAttribute("categoryCache", serviceCategoryCache);
            }

        } catch (Exception ex) {
            org.esme.broker.AppBroker.sevasLogger.info(ex.getMessage());
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }

}
