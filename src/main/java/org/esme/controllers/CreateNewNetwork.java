package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.Network;
import org.esme.utils.pingenerator.RandomPinGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class CreateNewNetwork extends AppBroker {

    PreparedStatement pstmt;
    ArrayList<Network> networkCache;
    Network theNetwork;
    private String networkID;
    private String networkPrefixes;

    @SuppressWarnings("CallToThreadDumpStack")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try {
            networkID = removePoison(request.getParameter("networkID"));
        } catch (NullPointerException e) {
            request.setAttribute("message",
                    "*Network ID cannot be empty.");
            request.getRequestDispatcher("new_network.jsp").forward(request, response);
            return;
        }

        //Check if the service already exist
        networkCache = (ArrayList<Network>) getServletContext().getAttribute("networksCache");
        for (Network network : networkCache) {
            if (network.getNetworkID().equalsIgnoreCase(networkID)) {
                request.setAttribute(
                        "message", networkID + " has already been created.");
                request.getRequestDispatcher("new_network.jsp").forward(request, response);
                return;
            }
        }

        try {
            networkPrefixes = removePoison(request.getParameter("prefixes"));
        } catch (NullPointerException e) {
            request.setAttribute("message",
                    "Network prefixes cannot be empty.");
            request.getRequestDispatcher("new_network.jsp").forward(request, response);
            return;
        }


        try {

            long userID = RandomPinGenerator.randomPinGenerator(4);

            pstmt = getDBConnection().prepareStatement(
                    "insert into network(id, network_id, network_prefixes) values(?,?,?)");
            pstmt.setLong(1, userID);
            pstmt.setString(2, networkID);
            pstmt.setString(3, networkPrefixes);
            int update = pstmt.executeUpdate();

            if (update > 0) {
                updateNetwork();
                request.setAttribute(
                        "message",
                        networkID + " successfully created.");
                request.getRequestDispatcher("new_network.jsp").forward(request, response);
            } else {
                request.setAttribute(
                        "message",
                        "Sorry, an error occured while creating " + networkID + ". Please try again.");
                request.getRequestDispatcher("new_network.jsp").forward(request, response);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            request.setAttribute(
                    "message",
                    "*Sorry, an error occured while creating your account. Try again.");
            request.getRequestDispatcher("new_network.jsp").forward(request, response);
        } finally {
            try {
                pstmt.close();
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
