package org.esme.controllers;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.http.HttpStatus;
import org.esme.broker.AppBroker;
import org.esme.dao.models.ActiveSubscription;
import org.esme.dao.models.Subscription;
import org.esme.dto.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

@SuppressWarnings("serial")
public class UnSubEngine extends AppBroker {

    String requestShortCode,
            serviceMTShortCode,
            msisdn, req_Text,
            requestSource,
            availableServices, smsc;

    PreparedStatement pstmt;

    Connection dbConnection;

    ArrayList<ActiveSubscription> activeSubscriptionCache;
    Subscription subscription;
    ActiveSubscription activeSubscription;
    PreparedStatement st;

    ArrayList<Service> serviceCache;
    Service theService;


    String unSubPrefix = "You have been unsubscribed.";

    String services = "";
    String unSubKeyword = "";
    String network = null;
    String subServiceCode = "";

    Properties theProp = AppBroker.getPropertyFileHandle();

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        org.esme.broker.AppBroker.sevasLogger.info("Unsub process request called...");

        //He text the actual unscubscribe keyword...
        //Check services archive availabbility
        try {
            serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
        } catch (Exception e) {
        }

        if (serviceCache == null || serviceCache.size() < 1) {
            updateServicesCache();
            serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
        }

        try {

            try {

                req_Text = removePoison(request.getParameter("req_text").trim());
                requestShortCode = removePoison(request.getParameter("shortcode"));
                msisdn = removePoison(request.getParameter("msisdn").replace("+", ""));
                msisdn = AppBroker.CountryCodePrefix_PROP + msisdn.substring(msisdn.length() - 10);
                smsc = removePoison(request.getParameter("smscID").replace("\\s+", ""));

            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            prop = AppBroker.getPropertyFileHandle();

//            org.esme.broker.AppBroker.sevasLogger.info("DEBUG " + req_Text.replaceAll("\\s",""));
            // Check if subscriber is requesting for STOP
            //For others, we want to give the list of services he subscribed to...
            org.esme.broker.AppBroker.sevasLogger.info("He sent..." + req_Text);
            if (req_Text.replaceAll("\\s+", "").equalsIgnoreCase("stop") || req_Text.replaceAll("\\s+", "").equalsIgnoreCase("unsub")
                    || req_Text.replaceAll("\\s+", "").equalsIgnoreCase("cancel")) {

                prop = AppBroker.getPropertyFileHandle();

                String unSubMessage = getUnsubscribeMessage(requestShortCode, msisdn, smsc);
                sevasLogger.info(unSubMessage);

                if (network.toLowerCase().contains("etisalat")) {
                    network = theProp.getProperty("etiContentBind");
                }

                if (!unSubMessage.isEmpty()) {
                    getSendSMS().SMSBOX_NODLR(subServiceCode, msisdn,
                            unSubMessage, network);
                }

//                getUserServicesUnSubKeyword(requestShortCode, msisdn);
                return;
            }

            // Check if user is requesting to Stop All services
            if ((req_Text.replaceAll("\\s", "").equalsIgnoreCase("stopall")
                    || req_Text.replaceAll("\\s+", "").equalsIgnoreCase("unsuball")
                    || req_Text.replaceAll("\\s+", "").equalsIgnoreCase("cancelall"))) {

                org.esme.broker.AppBroker.sevasLogger.info("Subscribers wants to stop all services.." + req_Text);

                int update = 0;

                ArrayList<Service> activeServices = getSubscriberServicePerShortcode(msisdn, requestShortCode, smsc);

                // Deactivating Active services for msisdn
                for (Service service : activeServices) {

                    org.esme.broker.AppBroker.sevasLogger.info("Deactivating for " + service.getServiceName());
                    //Take out stop....
                    int status = deActivateService(service.getServiceID(), msisdn);
                    if (status > 0) {
                        update++;
                    }
                }

                if (update > 0) {
                    getSendSMS().SMSBOX_NODLR(requestShortCode, msisdn,
                            "You have been unsubscribed from all services on " + requestShortCode, smsc);
                    return;
                }

            }

            //He text the actual unscubscribe keyword...
            //Check services archive availabbility
            serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            //}

            org.esme.broker.AppBroker.sevasLogger.info("He sent the actual keyword..." + req_Text);
            String notificationMessage;

            for (Service service : serviceCache) {

                org.esme.broker.AppBroker.sevasLogger.info("Actual Unsub Keyword..." + service.getServiceUnSubscribeKeyword().trim());
                org.esme.broker.AppBroker.sevasLogger.info("SERVICE KEYWORD..." + service.getServiceKeyword());

                //Take out stop....
                if (req_Text.trim().replace(" ", "").toLowerCase().equalsIgnoreCase(service.getServiceUnSubscribeKeyword().replace(" ", "").toLowerCase().trim())
                        && service.getServiceNetwork().toLowerCase().contains(smsc.toLowerCase().substring(0, 3))) {

                    org.esme.broker.AppBroker.sevasLogger.info("Service ID " + service.getServiceID() + " Service Network " + service.getServiceNetwork());

                    // Check if susbcriber has an active subscription for service
                    boolean isServiceActive = isSubscriberActiveForService(msisdn, service.getServiceShortCode(), service.getServiceID(), smsc);

                    // There is an active susbcription for service
                    if (isServiceActive) {

                        int update = deActivateService(service.getServiceID(), msisdn);

                        String serviceNetwork = service.getServiceNetwork();

                        if (serviceNetwork.toLowerCase().contains("etisalat")) {
                            serviceNetwork = theProp.getProperty("etiContentBind");
                        }

                        notificationMessage = service.getServiceUnSubscriptionMessage();

                        // Route Request to Registered Listeners
                        routeToAnotherParty(service, msisdn, req_Text);

                    } else {
                        org.esme.broker.AppBroker.sevasLogger.info("INFO: " + msisdn + " Not subscribed to any service ");
                        notificationMessage = "You are currently not subscribed to " + service.getServiceName() + ". "
                                + "Text HELP for help.";

                    }

                    //Send unsubscription message...
                    try {

                        getSendSMS().SMSBOX_NODLR(
                                service.getServiceShortCode(),
                                msisdn,
                                formatText(notificationMessage),
                                service.getServiceNetwork());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                }
            }

        } catch (Exception e) {
        }
    }

    private void routeToAnotherParty(Service service, String msisdn, String req_text) {
        //informing third party if routing service...
        try {

            org.esme.broker.AppBroker.sevasLogger.info("ROUTE " + service.getRouteToExternalAPI());
            //Service if found...Check if we routing to third party..
            if (service.getRouteToExternalAPI().equalsIgnoreCase("yes")) {
                try {

                    HttpClient client;
                    GetMethod method;
                    byte[] responseBody;

                    String theURL = service.getExternalAPIURL();
                    String myParam = URIUtil.encodeQuery(service.getExternalAPIShortCodeParam() + "=" + requestShortCode
                            + "&" + service.getExternalAPIMSISDNParam() + "=" + msisdn
                            + "&" + service.getExternalAPIMessageParam() + "=" + req_Text);

                    String url = theURL + myParam;

                    org.esme.broker.AppBroker.sevasLogger.info(url);
                    client = new HttpClient();
                    method = new GetMethod(url);

                    // Execute the method.
                    int statusCode = client.executeMethod(method);
                    if (statusCode != HttpStatus.SC_OK) {
                        org.esme.broker.AppBroker.sevasLogger.info("Method failed: " + method.getStatusLine());
                    }
                    responseBody = method.getResponseBody();
                    // Use caution: ensure correct character encoding and is not binary data
                    String theRes = new String(responseBody);
                    org.esme.broker.AppBroker.sevasLogger.info(theRes);
                    //}

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {

        }

    }

    private boolean isSubscriberActiveForService(String msisdn, String serviceShortCode, long serviceID, String network) {

        boolean isActive = false;
        try {
            ArrayList<Service> activeServices = getSubscriberServicePerShortcode(msisdn, serviceShortCode, network);

            sevasLogger.info("Active Services Per Shortcode for " + msisdn + " " + activeServices);

            // Check if there are no active services for susbcriber
            if (activeServices.isEmpty()) isActive = false;

            // Iterate through activeServices to check if service with serviceID exist
            for (Service _service : activeServices) {

                sevasLogger.info("Checking ServiceID " + _service.getServiceID());
                sevasLogger.info("Requested ServiceID " + serviceID);

                if (_service.getServiceID() == serviceID) {
                    sevasLogger.info(msisdn + " has an active subscription on " + _service.getServiceName());
                    isActive = true;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return isActive;

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    /*
    @desc gets all active services for subscriber corresponding to request shortcode and network
     */
    public ArrayList<Service> getSubscriberServicePerShortcode(String subMSISDN, String reqShortcode, String reqNetwork) {

        if (dbConnection == null) {
            dbConnection = getDBConnection();
        }

        String _network;
        String billing_network;

        reqNetwork = reqNetwork.replace("\\s+", "");

        ArrayList<Service> userActiveServices = new ArrayList();

        PreparedStatement selectPstmt;
        String query = "select distinct on (a.id) a.id, a.service_name,a.billing_network, a.service_shortcode, a.service_network,"
                + "a.service_unsubcribe_keyword, a.service_unsubscription_msg from subscription_service a , subscription b "
                + "where b.sub_msisdn ilike '%" + subMSISDN.substring(subMSISDN.length() - 10)
                + "%' and b.sub_status = 'active' and a.id = b.sub_service_id and a.service_mt_shortcode = '" + reqShortcode.toString() + "'";
        org.esme.broker.AppBroker.sevasLogger.info(query);

        try {
            selectPstmt = dbConnection.prepareStatement(query);
            ResultSet thRS = selectPstmt.executeQuery();

            while (thRS.next()) {

                _network = thRS.getString("service_network");
                billing_network = thRS.getString("billing_network");

                if (reqNetwork.toLowerCase().contains("etisalat")) {
                    _network = theProp.getProperty("etiContentBind");
                }

                org.esme.broker.AppBroker.sevasLogger.info("Service ID from Database" + thRS.getLong("id"));

                sevasLogger.info("Request Network - " + reqNetwork);
                sevasLogger.info("Service Network - " + reqNetwork);

                //  Match Request source against Service/Billing Network and fetch only Services on that network
                if (reqNetwork.equalsIgnoreCase(_network)
                        || reqNetwork.equalsIgnoreCase(billing_network)) {

                    for (Service service : serviceCache) {

                        sevasLogger.info(service.getServiceID());

                        if (service.getServiceID() == thRS.getLong("id")) {
                            sevasLogger.info("There is a match" + service.getServiceID());
                            userActiveServices.add(service);
                        }
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        sevasLogger.info(userActiveServices);
        return userActiveServices;
    }

    public String getUnsubscribeMessage(String shortCode,
                                        String subMSISDN, String reqNetwork) throws SQLException {

        String serviceUnSubMessage = "";
        Long subServiceID = null;

        ArrayList<Service> activeServices;

//        try {
        activeServices = getSubscriberServicePerShortcode(msisdn, requestShortCode, smsc);

        if (activeServices.isEmpty()) {

            subServiceCode = requestShortCode;

            org.esme.broker.AppBroker.sevasLogger.info("INFO: " + subMSISDN + " Not subscribed to any service ");
            serviceUnSubMessage = "You are currently not subscribed to any service on " + shortCode + ". "
                    + "Text HELP for help.";

        }

        if (activeServices.size() == 1) {

            subServiceID = activeServices.get(0).getServiceID();
            subServiceCode = activeServices.get(0).getServiceShortCode();

            org.esme.broker.AppBroker.sevasLogger.info("INFO: " + subMSISDN + " subscribed to only " + subServiceID + "..Deactivating..");

            int update = deActivateService(subServiceID, subMSISDN);
            if (update > 0) {
                serviceUnSubMessage = activeServices.get(0).getServiceUnSubscriptionMessage();
            }

        }

        if (activeServices.size() > 1) {

            // Deactivating Active services for msisdn
            for (Service service : activeServices) {

                org.esme.broker.AppBroker.sevasLogger.info("Getting Keyword for " + service.getServiceName());
                String serviceName = service.getServiceName();

                // Use Description as service Name for Etisalat Direct Billing
                if (service.getServiceSubType().equalsIgnoreCase("edb")) {
                    serviceName = service.getServiceDescription();
                }

                serviceUnSubMessage += " " + service.getServiceUnSubscribeKeyword().toUpperCase()
                        + " for " + serviceName.toUpperCase() + "\n";

                org.esme.broker.AppBroker.sevasLogger.info("INFO: " + subMSISDN + " - " + serviceName.toUpperCase());
                org.esme.broker.AppBroker.sevasLogger.info("INFO: " + subMSISDN + " - " + service.getServiceUnSubscribeKeyword().toUpperCase());

            }
            //to unsubscriber from ,
            org.esme.broker.AppBroker.sevasLogger.info("INFO: " + subMSISDN + "subscribed to multiple services..Sending UNSUB procedure ");

            subServiceCode = activeServices.get(0).getServiceShortCode();

            serviceUnSubMessage = formatText("To unsubscribe text \n" + serviceUnSubMessage + " "
                    + "or STOP ALL to unsubscribe from all the services.");

        }
        return serviceUnSubMessage;

    }

    //Deactivate Service
    @SuppressWarnings("CallToPrintStackTrace")
    public int deActivateService(long serviceID, String subscriber) {
        int update = 0;
        try {

            String sub = subscriber.substring(subscriber.length() - 10);

            if (dbConnection == null) {
                dbConnection = getDBConnection();
            }

            String deactivateQuery = "update subscription set sub_status = 'inactive', "
                    + "un_sub_date = CURRENT_TIMESTAMP, "
                    + "un_sub_source = 'SMS' where sub_msisdn ilike '%" + sub + "' "
                    + "and sub_service_id = " + serviceID + " and sub_status = 'active'";

            org.esme.broker.AppBroker.sevasLogger.info(deactivateQuery);

            PreparedStatement p = dbConnection.prepareStatement(deactivateQuery);
            update = p.executeUpdate();

        } catch (SQLException ex) {
//            org.esme.broker.AppBroker.sevasLogger.info("Unable to Deactivate ");
            ex.printStackTrace();
        }
        return update;
    }

    public int deactivateServiceSpecifySource(long serviceID, String subscriber, String unsubSource) {
        int update = 0;
        try {

            String sub = subscriber.substring(subscriber.length() - 10);

            if (dbConnection == null) {
                dbConnection = getDBConnection();
            }

            String deactivateQuery = "update subscription set sub_status = 'inactive', "
                    + "un_sub_date = CURRENT_TIMESTAMP, "
                    + "un_sub_source = " + unsubSource + " where sub_msisdn ilike '%" + sub + "' "
                    + "and sub_service_id = " + serviceID + " and sub_status = 'active'";

            org.esme.broker.AppBroker.sevasLogger.info(deactivateQuery);

            PreparedStatement p = dbConnection.prepareStatement(deactivateQuery);
            update = p.executeUpdate();

        } catch (SQLException ex) {
//            org.esme.broker.AppBroker.sevasLogger.info("Unable to Deactivate ");
            ex.printStackTrace();
        }
        return update;
    }

}
