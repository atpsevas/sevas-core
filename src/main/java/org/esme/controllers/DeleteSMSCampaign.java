package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.SMSCampaign;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DeleteSMSCampaign extends AppBroker {

    PreparedStatement pstmt;
    SMSCampaign campaign;
    ArrayList<SMSCampaign> campaignCache;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            String[] delete = request.getParameterValues("check");

            //For content deletion...
            if (delete.length > 0) {
                int counter = 0;
                for (int index = 0; index < delete.length; index++) {
                    //Update the database
                    try {

                        pstmt = getDBConnection().prepareStatement(
                                "delete from sms_campaign where campaign_id = ? ");
                        pstmt.setLong(1, Integer.parseInt(delete[index]));
                        int update = pstmt.executeUpdate();
                        if (update > 0) {
                            //We refereh content cache here
                        }
                    } catch (Exception e) {
                    }
                    counter++;
                }
                updateSMSCampaignCache();
                request.setAttribute(
                        "message",
                        counter + " campaign successfully deleted.");
                request.getRequestDispatcher(
                        "sms_campaign_mgr.jsp").forward(request, response);

            }
        } catch (Exception e) {
        } finally {
            try {
                pstmt.close();
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    public void updateSMSCampaignCache() {
        org.esme.broker.AppBroker.sevasLogger.info("Updating Campaign...");
        campaignCache = new ArrayList<SMSCampaign>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {


            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(getServletContext().getInitParameter("smsCampaignReportQuery"));

            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                while (resultSet.next()) {
                    campaign = new SMSCampaign();
                    campaign.setCampaignID(resultSet.getLong("campaign_id"));
                    campaign.setCampaignUploadDate(resultSet.getString("campaign_upload_date"));
                    campaign.setCampaignSenderID(resultSet.getString("campaign_sender_id"));
                    campaign.setCampaignContent(resultSet.getString("campaign_content"));
                    campaign.setCampaignTargetFileDir(resultSet.getString("campaign_target_file_dir"));
                    campaign.setCampaignSentDate(resultSet.getString("campaign_sent_date"));
                    campaign.setCampaignSentTime(resultSet.getLong("campaign_sent_time"));
                    campaign.setCampaignOnwerID(resultSet.getLong("campaign_owner"));
                    campaign.setCampaignTotalTarget(resultSet.getLong("campaign_total_target_recipients"));
                    campaign.setCampaignTotalSent(resultSet.getLong("campaign_total_sent"));
                    campaign.setCampaignActualStartTime(resultSet.getString("campaign_actual_start_time"));
                    campaign.setCampaignActualEndTime(resultSet.getString("campaign_actual_end_time"));
                    campaign.setCampaignStatus(resultSet.getString("campaign_status"));
                    campaign.setCampaignServiceID(resultSet.getLong("campaign_service_id"));
                    campaignCache.add(campaign);
                }
                //Create cache of Campaign
                getServletContext().setAttribute("totalSMSCampaign", campaignCache.size());
                getServletContext().setAttribute("smsCampaignCache", campaignCache);
            }

        } catch (Exception ex) {
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }
}
