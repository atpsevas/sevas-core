package org.esme.controllers;

import com.csvreader.CsvReader;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.esme.broker.AppBroker;
import org.esme.dao.models.Content;
import org.esme.dao.models.Users;
import org.esme.dto.Service;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@SuppressWarnings("serial")
public class BulkContentUpload extends AppBroker {

    PreparedStatement pstmt;
    File uploadFile = null;
    long serviceID;
    ArrayList<Service> servicesCache;
    ArrayList<Content> contentCache;
    Content content;
    ArrayList<Users> userCache;
    String user;
    String userRole;


    @SuppressWarnings({"CallToThreadDumpStack", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        ServletContext ctx = getServletContext();
        HttpSession session = request.getSession(true);

        user = (String) session.getAttribute("userName");
        userRole = (String) session.getAttribute("UserRole");

        //Get cached services, users, and contents
        servicesCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
        userCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
        contentCache = (ArrayList<Content>) getServletContext().getAttribute("contentCache");

        //get the file using apache commons.
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setRepository(new File(getServletContext().getRealPath("/")));
        ServletFileUpload upload = new ServletFileUpload(factory);
        List<FileItem> items;

        try {
            items = upload.parseRequest(request);
        } catch (FileUploadException ex) {
            ex.printStackTrace();
            request.setAttribute("message", "ERROR:"
                    + "Unable to upload the file specified! Check and try again.");
            request.getRequestDispatcher("new_bulk_content.jsp").forward(request, response);
            return;
        }

        // Process the uploaded items
        Iterator iter = items.iterator();
        while (iter.hasNext()) {
            FileItem item = (FileItem) iter.next();
            if (item.isFormField()) {
                //processFormField(item);
                if (item.getFieldName().equalsIgnoreCase("service")) {
                    //Acquire service ID
                    serviceID = Long.parseLong(item.getString());
                }
            } else {
                //processUploadedFile(item);
                String filename = item.getName();
                File f = new File(filename);
                filename = f.getName();
                // get rid of spaces in filename.
                filename = filename.replace(" ", "_");

                if (!(filename.endsWith(".csv"))) {
                    request.setAttribute("message", "ERROR:"
                            + "You have selected an invalid file type!");
                    request.getRequestDispatcher("new_bulk_content.jsp").forward(request, response);
                    return;
                } else {
                    //Saving the File in the application root folder
                    uploadFile = new File(ctx.getInitParameter("BulkContentDir") + filename);
                    try {
                        item.write((uploadFile));
                    } catch (Exception ex) {
                        request.setAttribute("message", ""
                                + "SEVERE: Error while uploading your file. Please try again");
                        request.getRequestDispatcher("new_bulk_content.jsp").forward(request, response);
                        ex.printStackTrace();
                        return;
                    }
                }
            }
        }

        //Reading the content of the file into the DB
        int successfulUploadCount = 0;
        int failedContentCount = 0;

        try {

            CsvReader reader = new CsvReader(uploadFile.getAbsolutePath());
            reader.readHeaders();
            while (reader.readRecord()) {
                try {

                    //Manually format content date
                    String theDate = reader.get(1);
                    String year = theDate.substring(0, 4);
                    String month = theDate.substring(4, 6);
                    String date = theDate.substring(6, 8);


                    pstmt = getDBConnection().prepareStatement(
                            "insert into content"
                                    + " (content_provider_id,content_service_id,content,"
                                    + "content_date_of_blast)"
                                    + "values(?,?,?,?)");

                    pstmt.setLong(1, getUserID(user));
                    pstmt.setLong(2, serviceID);
                    pstmt.setString(3, formatText(reader.get(3)));
                    pstmt.setString(4, year + "-" + month + "-" + date);
                    pstmt.executeUpdate();

                } catch (Exception e) {
                    e.printStackTrace();
                    failedContentCount++;
                }
                successfulUploadCount++;
            }

            //Update content cache...
            if (successfulUploadCount > 0) {
                updateContentCache();
            }
            request.setAttribute("message", successfulUploadCount + " contents were successfully uploaded. "
                    + failedContentCount + " failed");
            request.getRequestDispatcher("new_bulk_content.jsp").forward(request, response);
        } catch (FileNotFoundException e) {
            request.setAttribute("message", "Oops!"
                    + "File specified could not be located. Please try again later.");
            request.getRequestDispatcher("new_bulk_content.jsp").forward(request, response);
            e.printStackTrace();
        } finally {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }

    //Get user ID
    private long getUserID(String userName) {
        long userID = 0;
        for (Users theUser : userCache) {
            if (theUser.getUserName().equalsIgnoreCase(userName)) {
                userID = theUser.getId();
            }
        }
        return userID;
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "null"})
    public void updateContentCache() {

        org.esme.broker.AppBroker.sevasLogger.info("Updating content archive...");
        ArrayList<Content> lcontentCache = new ArrayList<Content>();
        Statement statement = null;
        ResultSet resultSet = null;
        Content lcontent;
        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(
                    getServletContext().getInitParameter("contentQuery"));
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                while (resultSet.next()) {
                    try {
                        lcontent = new Content();
                        lcontent.setContent_id(resultSet.getInt("content_id"));
                        lcontent.setContentServiceName(resultSet.getString("service_name"));
                        lcontent.setContent(resultSet.getString("content"));
                        lcontent.setDateUploaded(resultSet.getString("content_upload_date"));
                        lcontent.setContentOwner(resultSet.getInt("content_provider_id"));
                        lcontent.setContentService(resultSet.getLong("content_service_id"));
                        lcontent.setConentDOB(resultSet.getString("content_date_of_blast"));
                        lcontentCache.add(lcontent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //Create cache of content with all
                getServletContext().setAttribute("totalContents ", lcontentCache.size());
                getServletContext().setAttribute("contentCache", lcontentCache);
            }

        } catch (Exception ex) {
            org.esme.broker.AppBroker.sevasLogger.info("Error while updating active content cache.");
            ex.printStackTrace();

        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }
        //Close DB Objects
    }
}
