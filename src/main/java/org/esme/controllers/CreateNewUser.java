package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.Users;
import org.esme.utils.pingenerator.RandomPinGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CreateNewUser extends AppBroker {

    PreparedStatement pstmt;
    Users user;
    ArrayList<Users> usersCache;
    //User Details
    private String userName;
    private String password;
    private String role;

    @SuppressWarnings("CallToThreadDumpStack")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try {
            userName = removePoison(request.getParameter("username"));
        } catch (NullPointerException e) {
            request.setAttribute("message",
                    "*User name cannot be empty.");
            request.getRequestDispatcher("new_user.jsp").forward(request, response);
            return;
        }

        //Check if the service already exist
        usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
        for (Users theUser : usersCache) {
            if (theUser.getUserName().equals(userName)) {
                request.setAttribute(
                        "message", "*This account has already been created.");
                request.getRequestDispatcher("new_user.jsp").forward(request, response);
                return;
            }
        }

        try {
            password = removePoison(request.getParameter("password"));
        } catch (NullPointerException e) {
            request.setAttribute("message",
                    "*Password cannot be empty.");
            request.getRequestDispatcher("new_user.jsp").forward(request, response);
            return;
        }

        try {
            role = request.getParameter("role").trim();
        } catch (NullPointerException e) {
            request.setAttribute(
                    "message",
                    "*You must select a role for this user.");
            request.getRequestDispatcher("new_user.jsp").forward(request, response);
            return;
        }

        try {

            long userID = RandomPinGenerator.randomPinGenerator(4);

            pstmt = getDBConnection().prepareStatement(
                    "insert into users(id, username, password) values(?,?,?)");
            pstmt.setLong(1, userID);
            pstmt.setString(2, userName);
            pstmt.setString(3, password);
            int update = pstmt.executeUpdate();

            if (update > 0) {
                PreparedStatement rolepstmt = getDBConnection().prepareStatement(
                        "insert into roles(username,role) values(?,?)");
                rolepstmt.setString(1, userName);
                rolepstmt.setString(2, role);
                rolepstmt.executeUpdate();
                //update users acache..
                updateUsers();
                org.esme.broker.AppBroker.sevasLogger.error("IT UPDATE USERS ARCHIVE");
                request.setAttribute(
                        "message",
                        "Account successfully created. Enter a new account details.");
                request.getRequestDispatcher("new_user.jsp").forward(request, response);

            } else {
                request.setAttribute(
                        "message",
                        "Sorry, an error occured while creating your account. Try again.");
                request.getRequestDispatcher("new_user.jsp").forward(request, response);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            request.setAttribute(
                    "message",
                    "*Sorry, an error occured while creating your account. Try again.");
            request.getRequestDispatcher("new_user.jsp").forward(request, response);
        } finally {
            try {
                pstmt.close();
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void updateUsers() {
        usersCache = new ArrayList<Users>();
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(getServletContext().getInitParameter("usersQuery"));
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                //update previously stored job contents from the container...
                while (resultSet.next()) {
                    user = new Users();
                    user.setId(resultSet.getInt("id"));
                    user.setUserName(resultSet.getString("username"));
                    user.setPassword(resultSet.getString("password"));
                    usersCache.add(user);
                }
                //Create cache of all users
                getServletContext().setAttribute("usersCache", usersCache);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }

        }
    }
}
