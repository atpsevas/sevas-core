package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.Subscription;
import org.esme.dto.Service;
import org.esme.utils.pingenerator.RandomPinGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("serial")
public class BulkSubscription extends AppBroker {


    PreparedStatement pstmt;
    BufferedReader reader;
    Subscription subscription;

    @SuppressWarnings("CallToThreadDumpStack")
    protected void processRequest(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException {

        String serviceMOShortCode, serviceMTShortCode, subMSISDN,
                subServiceTimeOfBlast, subServiceIntervalOfBlast,
                service, subNetwork, billingShortCode,
                serviceName, subRequestText, subResponseText, firstContent, serviceNetwork,
                subExpiryDate, contentDeliveryMethod, subType;
        long subServicePeriod, subServiceID, subServiceOwnerID;

        String allMSISDN;
        String service_id;

        String msisdn;

        response.setContentType("text/html;charset=UTF-8");
        try {
            try {
                //Params
                allMSISDN = removePoison(request.getParameter("msisdns"));
                serviceMOShortCode = removePoison(request.getParameter("sc"));
//                service = removePoison(request.getParameter("serv").replace("%20", " "));

                service_id = removePoison(request.getParameter("serv_id"));


            } catch (NullPointerException e) {
                e.printStackTrace();
                request.setAttribute("message", "Missing a required parameter. Please select short code and a service");
                request.getRequestDispatcher("bulk_subscription.jsp").forward(request, response);
                return;
            }

            //Search for the service in the archive....
            int checker = 0;
//            serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");

            Service theService = Service.getService(Long.valueOf(service_id));

            //Confirm if service was found...
            if (theService == null) {
                request.setAttribute("message", "The service (" + service_id + ") "
                        + "you have selected could not be found on " + serviceMOShortCode + "."
                        + " Please check the service and try again.");
                request.getRequestDispatcher("bulk_subscription.jsp").forward(request, response);
                return;
            }

            subServiceID = theService.getServiceID();
            subRequestText = theService.getServiceKeyword();
            serviceNetwork = theService.getServiceNetwork();
            subResponseText = theService.getServiceSubscriptionMessage();
            subServicePeriod = Integer.parseInt(theService.getServicePeriod());
            subServiceOwnerID = theService.getServiceOwner();
            subServiceTimeOfBlast = theService.getTimeOfBlast();
            serviceMTShortCode = theService.getServiceMTShortCode();
            subExpiryDate = addDate(0);
            contentDeliveryMethod = theService.getServiceContentDeliveryMethod();

            pstmt = getDBConnection().prepareStatement(
                    "insert into subscription("
                            + "id,sub_service_id,sub_msisdn,sub_service_period,sub_request_text,"
                            + "sub_response_text,sub_network,sub_service_owner_id,"
                            + "sub_tob,sub_iob,sub_shortcode,sub_status,"
                            + "sub_expiry_date,sub_content_delivery_method,sub_source)"
                            + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            );

            //Generate subscription ID....
            long counter = 0;
            //Iterate the numbers and insert in the DB...
            reader = new BufferedReader(new StringReader(allMSISDN));
            while ((msisdn = reader.readLine()) != null) {
                try {
                    if (msisdn.equals("") || msisdn == null) {
                    } else {
                        //Subscribe user...insert into the DB...
                        SubEngine.subscribeUserAccess(RandomPinGenerator.randomPinGenerator(9), subServiceID, msisdn, subServicePeriod,
                                subRequestText, subResponseText, serviceNetwork, subServiceOwnerID, subServiceTimeOfBlast, "", serviceMTShortCode,
                                "active", subExpiryDate, contentDeliveryMethod, "bulk_sub", pstmt);

                        counter = counter + 1;
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (counter > 0) {
                request.setAttribute(
                        "message", counter + " numbers was successfully subscribed");
                request.getRequestDispatcher(
                        "bulk_subscription.jsp").forward(request, response);
            } else {
                request.setAttribute(
                        "message", "No number was subscribed. "
                                + "Please try again or contact the portal administrator");
                request.getRequestDispatcher(
                        "bulk_subscription.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                pstmt.close();
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public String addDate(int NoOfDays) {
        Date date = new Date();
        String dt = formatDateToString(date);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(dt));
            c.add(Calendar.DATE, NoOfDays);
            dt = sdf.format(c.getTime());
        } catch (ParseException ex) {
        }
        return dt;
    }
}