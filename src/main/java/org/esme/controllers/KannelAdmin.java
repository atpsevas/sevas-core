package org.esme.controllers;

import org.esme.broker.AppBroker;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author salbert
 */
public class KannelAdmin extends AppBroker {

    String smsc;
    String command;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        smsc = request.getParameter("smsc");
        command = request.getParameter("command");
        try {
            String status = manageSMSCs(command, smsc);
            if (status != null) {
                request.setAttribute("message", status);
                request.getRequestDispatcher("gateway.jsp").forward(request, response);
            } else {
                checkGatewayStatus();
                request.setAttribute("message", "Requested operation failed. Please try again.");
                request.getRequestDispatcher("gateway.jsp").forward(request, response);
            }
        } catch (Exception e) {
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public String manageSMSCs(String command, String smsc) {
        // Send a GET request to the subscription servlet
        String result = null;
        try {
            //Send data
            String urlStr = "http://" + getAppProperty("smsGatewayIP")
                    + ":" + getServletContext().getInitParameter("adminPort") + "/" + command + "?password=" + getServletContext().getInitParameter("smsGatewayAdminPassword") + "&smsc=" + smsc;
            org.esme.broker.AppBroker.sevasLogger.info(urlStr);
            URL url = new URL(urlStr);
            URI uri = new URI(
                    url.getProtocol(),
                    url.getUserInfo(),
                    url.getHost(),
                    url.getPort(),
                    url.getPath(),
                    url.getQuery(),
                    url.getRef());

            url = uri.toURL();
            URLConnection conn = url.openConnection();
            conn.connect();
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            @SuppressWarnings("StringBufferMayBeStringBuilder")
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            result = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void checkGatewayStatus() {
        // Send a GET request to the subscription servlet
        String result;
        File responseFile = new File("./SMSGateway_Report.xml");
        try {
            // Construct data
            @SuppressWarnings("StringBufferMayBeStringBuilder")
            // Send data
                    String urlStr = "http://" + getAppProperty("smsGatewayIP")
                    + ":" + getAppProperty("adminPort") + "/status.xml";
            URL url = new URL(urlStr);
            URI uri = new URI(
                    url.getProtocol(),
                    url.getUserInfo(),
                    url.getHost(),
                    url.getPort(),
                    url.getPath(),
                    url.getQuery(),
                    url.getRef());

            url = uri.toURL();
            URLConnection conn = url.openConnection();
            conn.connect();
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            @SuppressWarnings("StringBufferMayBeStringBuilder")
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            result = sb.toString();
            //write the reponse to a file
            BufferedWriter out = new BufferedWriter(new FileWriter(responseFile));
            out.write(result);
            out.close();

        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
}
