package org.esme.controllers;

import org.apache.catalina.realm.GenericPrincipal;
import org.esme.broker.AppBroker;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Principal;

public class Login extends AppBroker {

    private static final long serialVersionUID = 1L;

    @SuppressWarnings("CallToThreadDumpStack")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        boolean nologin = true;
        HttpSession session = request.getSession(true);
        String userID = request.getUserPrincipal().getName();

        //Verifying caller's identity
        if (request.isUserInRole("Blacklister")) {

            //Get the user name using Java Principal object...
            final Principal userPrincipal = request.getUserPrincipal();

            GenericPrincipal genericPrincipal = (GenericPrincipal) userPrincipal;

            //Get the user role using Java GenericPrincipal object...Which will return array of available roles in the Realm...Web Descriptor
            final String[] roles = genericPrincipal.getRoles();

            for (String role : roles) {
                if (role.equalsIgnoreCase("Blacklister")) {
                    session.setAttribute("UserRole", role);
                }
            }
            session.setAttribute("userName", userID);
            String mainURL = "./main.jsp";
            String encodeURL = response.encodeRedirectURL(mainURL);
            response.sendRedirect(encodeURL);

        } else if (request.isUserInRole("Logviewer")) {

            final Principal userPrincipal = request.getUserPrincipal();
            GenericPrincipal genericPrincipal = (GenericPrincipal) userPrincipal;
            final String[] roles = genericPrincipal.getRoles();

            for (String role : roles) {
                if (role.equalsIgnoreCase("Logviewer")) {
                    session.setAttribute("UserRole", role);
                }
            }

            session.setAttribute("userName", userID);
            String mainURL = "./main.jsp";
            String encodeURL = response.encodeRedirectURL(mainURL);
            response.sendRedirect(encodeURL);

        } else if (request.isUserInRole("Admin")) {

            final Principal userPrincipal = request.getUserPrincipal();
            GenericPrincipal genericPrincipal = (GenericPrincipal) userPrincipal;
            final String[] roles = genericPrincipal.getRoles();
            for (String role : roles) {
                if (role.equalsIgnoreCase("Admin")) {
                    session.setAttribute("UserRole", role);
                }
            }

            session.setAttribute("userName", userID);
            String mainURL = "./main.jsp";
            String encodeURL = response.encodeRedirectURL(mainURL);
            response.sendRedirect(encodeURL);

        } else if (request.isUserInRole("User")) {

            final Principal userPrincipal = request.getUserPrincipal();
            GenericPrincipal genericPrincipal = (GenericPrincipal) userPrincipal;
            final String[] roles = genericPrincipal.getRoles();
            for (String role : roles) {
                if (role.equalsIgnoreCase("User")) {
                    session.setAttribute("UserRole", role);
                }
            }

            session.setAttribute("userName", userID);
            String mainURL = "./main.jsp";
            String encodeURL = response.encodeRedirectURL(mainURL);
            response.sendRedirect(encodeURL);

        } else if (request.isUserInRole("Content")) {

            final Principal userPrincipal = request.getUserPrincipal();
            GenericPrincipal genericPrincipal = (GenericPrincipal) userPrincipal;
            final String[] roles = genericPrincipal.getRoles();
            for (String role : roles) {
                if (role.equalsIgnoreCase("Content")) {
                    session.setAttribute("UserRole", role);
                }
            }

            session.setAttribute("userName", userID);
            String mainURL = "./main.jsp";
            String encodeURL = response.encodeRedirectURL(mainURL);
            response.sendRedirect(encodeURL);

        } else if (request.isUserInRole("Deactivate")) {

            final Principal userPrincipal = request.getUserPrincipal();
            GenericPrincipal genericPrincipal = (GenericPrincipal) userPrincipal;
            final String[] roles = genericPrincipal.getRoles();
            for (String role : roles) {
                if (role.equalsIgnoreCase("Deactivate")) {
                    session.setAttribute("UserRole", role);
                }
            }

            session.setAttribute("userName", userID);
            String mainURL = "./deactivate.jsp";
            String encodeURL = response.encodeRedirectURL(mainURL);
            response.sendRedirect(encodeURL);

        } else if (request.isUserInRole("mtn")) {

            final Principal userPrincipal = request.getUserPrincipal();
            GenericPrincipal genericPrincipal = (GenericPrincipal) userPrincipal;
            final String[] roles = genericPrincipal.getRoles();
            for (String role : roles) {
                if (role.equalsIgnoreCase("mtn")) {
                    session.setAttribute("UserRole", role);
                }
            }

            session.setAttribute("userName", userID);
            String mainURL = "./deactivate_mtn.jsp";
            String encodeURL = response.encodeRedirectURL(mainURL);
            response.sendRedirect(encodeURL);

        } else {

            session.setAttribute("nologin", nologin);
            String mainURL = "./nologin.jsp";
            String encodeURL = response.encodeRedirectURL(mainURL);
            response.sendRedirect(encodeURL);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
