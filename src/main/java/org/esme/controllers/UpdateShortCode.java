package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.ShortCode;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UpdateShortCode extends AppBroker {

    PreparedStatement pstmt;
    ShortCode theNetwork;
    ArrayList<ShortCode> shortCodeCache;

    @SuppressWarnings("CallToThreadDumpStack")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String id = request.getParameter("id");
        try {

            pstmt = getDBConnection().prepareStatement(
                    "delete from service_shortcode where id = ? ");
            pstmt.setInt(1, Integer.parseInt(id));
            int update = pstmt.executeUpdate();
            if (update > 0) {
                //update network cache..
                updateShortCodesCache();
                request.setAttribute(
                        "message",
                        id + " successfully deleted.");
                request.getRequestDispatcher("manage_shortcodes.jsp").forward(request, response);
            } else {
                request.setAttribute(
                        "message",
                        "No shortcode  was deleted.");
                request.getRequestDispatcher(
                        "manage_shortcodes.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute(
                    "message",
                    "An error occured while trying to delete " + id);
            request.getRequestDispatcher("manage_shortcodes.jsp").forward(request, response);
        } finally {
            try {
                pstmt.close();
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void updateShortCodesCache() {
        org.esme.broker.AppBroker.sevasLogger.info("Refreshing Short Codes Cache.....");
        ShortCode shortCodes;
        ArrayList<ShortCode> shortCodesCache = new ArrayList<ShortCode>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(getServletContext().getInitParameter("shortCodeQuery"));
            //We place a lock on the  cache untill it finishes updating....
            synchronized (this) {
                while (resultSet.next()) {
                    shortCodes = new ShortCode();
                    shortCodes.setID(resultSet.getLong("id"));
                    shortCodes.setShortCodeID(resultSet.getString("shortcode_id"));
                    shortCodesCache.add(shortCodes);
                }
                //Create cache of services with all
                getServletContext().setAttribute("shortCodesCache", shortCodesCache);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }
}
