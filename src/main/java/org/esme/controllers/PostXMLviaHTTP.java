package org.esme.controllers;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.esme.broker.AppBroker;
import org.esme.dto.Service;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class PostXMLviaHTTP extends AppBroker {

    String serviceShortCode, subMSISDN,
            subRequestText, subNetwork,
            subResponseText;
    ServletContext ctx;
    ArrayList<Service> serviceCache;

    @SuppressWarnings("CallToThreadDumpStack")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try {
            //Collect the parameters coming from the SMS Gateway
            serviceShortCode = removePoison(request.getParameter("sc"));
            subMSISDN = request.getParameter("msisdn").replace("+", "");
            subRequestText = removePoison(request.getParameter("req_text"));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        String strURL = "http://goldmine.sloocetech.net:8084/spg-war/access";

        String xml = "<?xml version=\"1.0\" ?>"
                + "<SPGMO version=\"1.0\">"
                + "<PartnerId>87634</PartnerId>"
                + "<PartnerPwd>987qxs765fgj321pi_</PartnerPwd>"
                + "<ServiceId>1</ServiceId>"
                + "<SubscriberId>2348034678549</SubscriberId>"
                + "<MessageId>0</MessageId>"
                + "<Locale>en_US</Locale>"
                + "<InterfaceId>1</InterfaceId>"
                + "<RouteInfo>mtn-33560</RouteInfo>"
                + "<Command>MSG</Command>"
                + "<Content>STOP</Content>"
                + "</SPGMO>";

        PostMethod post = new PostMethod(strURL);
        try {
            StringRequestEntity requestEntity = new StringRequestEntity(xml);
            post.setRequestEntity(requestEntity);
            post.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
            HttpClient httpclient = new HttpClient();
            int result = httpclient.executeMethod(post);
            org.esme.broker.AppBroker.sevasLogger.info("Response status code: " + result);
            org.esme.broker.AppBroker.sevasLogger.info("Response body: ");
            org.esme.broker.AppBroker.sevasLogger.info(post.getResponseBodyAsString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            post.releaseConnection();
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
