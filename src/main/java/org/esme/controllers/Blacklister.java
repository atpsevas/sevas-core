package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.Blacklist;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

@SuppressWarnings("serial")
public class Blacklister extends AppBroker {

    Blacklist blacklist;
    ArrayList<Blacklist> activeBlacklistCache;
    String allMSISDN;
    String msisdn;
    String network;
    String requestText;
    PreparedStatement pstmt;
    BufferedReader reader;

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "UseSpecificCatch"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            network = request.getParameter("network");
            allMSISDN = request.getParameter("msisdns");
            requestText = request.getParameter("reqText");
        } catch (Exception e) {
            e.printStackTrace();
            ;
        }

        try {


//            if (requestText.equalsIgnoreCase("whitelist")) {
//                String theNumber = msisdn.substring(msisdn.length() - 9);
//                try {
//
//                    pstmt = connection.prepareStatement(
//                            "update subscription set blacklisted = ? , sub_status = ? "
//                            + "where sub_msisdn like '%" + theNumber + "' ");
//                    pstmt.setString(1, "");
//                    pstmt.setString(2, "active");
//                    int wl = pstmt.executeUpdate();
//
//                    if (wl > 0) {
//                        request.setAttribute(
//                                "message", theNumber + " was successfully whitelisted");
//                        request.getRequestDispatcher("manage_blacklist.jsp").forward(request, response);
//                        updateBlacklist();
//                        return;
//                    } else {
//                        request.setAttribute(
//                                "message", "" + theNumber + " could not be whitelisted. Please try again later.");
//                        request.getRequestDispatcher("manage_blacklist.jsp").forward(request, response);
//                    }
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
//                return;
//            }


            try {
                allMSISDN = request.getParameter("msisdns");
            } catch (NullPointerException e) {
                request.setAttribute(
                        "message",
                        "Please enter the number to blacklist.");
                request.getRequestDispatcher(
                        "blacklist_msisdn.jsp").forward(request, response);
                e.printStackTrace();
                return;
            }

            try {

                reader = new BufferedReader(new StringReader(allMSISDN));
                Timestamp ts;
                int theUpdate = 0;
                try {
                    while ((msisdn = reader.readLine()) != null) {
                        int deleteUpdate;
                        String theNumber = msisdn.substring(msisdn.length() - 9);
                        try {

                            pstmt = getDBConnection().prepareStatement(
                                    "update subscription set blacklisted = ? , "
                                            + "blacklisted_date = ?, sub_status = ? "
                                            + "where sub_msisdn like '%" + theNumber + "' "
                                            + "and trim(sub_network) ilike '%" + network.trim() + "%' ");

                            ts = new Timestamp(new Date().getTime());
                            pstmt.setString(1, "blacklisted");
                            pstmt.setTimestamp(2, ts);
                            pstmt.setString(3, "inactive");
                            deleteUpdate = pstmt.executeUpdate();
                            if (deleteUpdate < 1) {
                                blackListSubscriber(
                                        0, msisdn, 0, "blacklist",
                                        "blacklist", network, 0, "0", "0",
                                        "unknown", "inactive", formatDateToString(new Date()), "unknown");
                                theUpdate++;
                            } else {
                                theUpdate++;
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                    updateBlacklist();
                } catch (Exception e) {
                    e.printStackTrace();
                    request.setAttribute(
                            "message",
                            "Unknown error occurred while processing your request. "
                                    + "Please contact the portal administrator..");
                    request.getRequestDispatcher(
                            "blacklist_msisdn.jsp").forward(request, response);
                    return;
                }

                if (theUpdate > 0) {
                    request.setAttribute(
                            "message", theUpdate + "numbers were successfully blacklisted");
                    request.getRequestDispatcher("blacklist_msisdn.jsp").forward(request, response);
                } else {
                    request.setAttribute(
                            "message", "No number was blacklisted. "
                                    + "Please try again or contact the portal administrator");
                    request.getRequestDispatcher(
                            "blacklist_msisdn.jsp").forward(request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
                request.setAttribute(
                        "message", "Error occured while sending your request.");
                request.getRequestDispatcher(
                        "blacklist_msisdn.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public int blackListSubscriber(long servID, String subMSISDN, long subServicePeriod,
                                   String reqText, String resText, String subNetwork,
                                   long subServiceOwner, String tob, String iob,
                                   String subShortCode, String status, String subExpiryTime, String sub_content_delivery_method) {
        int update = 0;

        try {
            pstmt = getDBConnection().prepareStatement(
                    "insert into subscription("
                            + "sub_service_id,sub_msisdn,sub_service_period,sub_request_text,"
                            + "sub_response_text,sub_network,sub_service_owner_id,"
                            + "sub_tob,sub_iob,sub_shortcode,sub_status,sub_expiry_date,"
                            + "sub_request_time,sub_content_delivery_method,blacklisted,blacklisted_date)"
                            + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            pstmt.setLong(1, servID);
            pstmt.setString(2, subMSISDN);
            pstmt.setLong(3, subServicePeriod);
            pstmt.setString(4, reqText);
            pstmt.setString(5, resText);
            pstmt.setString(6, subNetwork);
            pstmt.setLong(7, subServiceOwner);
            pstmt.setString(8, tob);
            pstmt.setString(9, iob);
            pstmt.setString(10, subShortCode);
            pstmt.setString(11, status);
            pstmt.setString(12, subExpiryTime);
            Timestamp ts = new Timestamp(new Date().getTime());
            pstmt.setTimestamp(13, ts);
            pstmt.setString(14, sub_content_delivery_method);
            pstmt.setString(15, "blacklisted");
            pstmt.setTimestamp(16, ts);
            update = pstmt.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return update;
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "Convert2Diamond"})
    public void updateBlacklist() {
        org.esme.broker.AppBroker.sevasLogger.info("Updating Blacklist cache...");
        activeBlacklistCache = new ArrayList<org.esme.dao.models.Blacklist>();
        Statement statement;
        ResultSet dataResultSet;

        try {
            statement = getDBConnection().createStatement();
            dataResultSet = statement.executeQuery(
                    getServletContext().getInitParameter("blacklist"));

            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                while (dataResultSet.next()) {
                    blacklist = new org.esme.dao.models.Blacklist();
                    blacklist.setBlacklistedMSISDN(dataResultSet.getString("sub_msisdn"));
                    blacklist.setBlacklistDate(dataResultSet.getString("blacklisted_date"));
                    blacklist.setBlacklistedNetwork(dataResultSet.getString("sub_network"));

                    activeBlacklistCache.add(blacklist);
                }
                getServletContext().setAttribute("activeBlacklistCacheCount", activeBlacklistCache.size());
                getServletContext().setAttribute("activeBlacklistCache", activeBlacklistCache);
                org.esme.broker.AppBroker.sevasLogger.info("TOTAL Blacklist " + activeBlacklistCache.size());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
