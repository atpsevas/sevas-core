/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers.feedreaders;

/**
 * @author saheedalbert
 */
public class ReadTest {

    public static void main(String[] args) {
        RSSFeedParser parser = new RSSFeedParser("http://www.thisdaylive.com/go/search/?search=News&contenttype=article&sort=date&output=rss");
        ThisDayNewsFeed feed = parser.readFeed();
        org.esme.broker.AppBroker.sevasLogger.info(feed);
        for (FeedMessage message : feed.getMessages()) {
            org.esme.broker.AppBroker.sevasLogger.info(message);

        }

    }
}
