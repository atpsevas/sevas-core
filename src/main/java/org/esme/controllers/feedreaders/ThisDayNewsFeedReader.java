/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers.feedreaders;

import org.esme.broker.AppBroker;
import org.esme.dto.Service;
import org.esme.webservices.glo.GloPSA;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;

/**
 * @author saheedalbert
 */
public class ThisDayNewsFeedReader extends AppBroker {

    private static final long serialVersionUID = 1L;

    @SuppressWarnings({"null", "CallToPrintStackTrace"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String serviceID = request.getParameter("serviceID");
            ArrayList<Service> serviceCache = null;

            //Glo PSA BILLING...
            GloPSA gloPSA = null;
            String gloPSAbillingResponse;

            String srcModule = "DailyAlert";

            RSSFeedParser parser = new RSSFeedParser("http://www.thisdaylive.com/go/search/?search=news&contenttype=article&sort=date&output=rss");
            ThisDayNewsFeed feed = parser.readFeed();
            //org.esme.broker.AppBroker.sevasLogger.info(feed);
            Random random = new Random();
            if (feed.getMessages().size() > 0) {

                int index = random.nextInt(feed.getMessages().size());

                FeedMessage message = feed.getMessages().get(index);

                Statement statement = null;
                ResultSet dataResultSet = null;
                try {

                    try {
                        serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                    } catch (Exception e) {
                    }

                    if (serviceCache == null || serviceCache.size() < 1) {
                        updateServicesCache();
                        serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                    }

                    for (Service nextService : serviceCache) {

                        if (nextService.getServiceID() == Long.parseLong(serviceID)) {

                            statement = getDBConnection().createStatement();

                            dataResultSet = statement.executeQuery(
                                    "select distinct on (a.sub_msisdn) a.id, "
                                            + "a.sub_msisdn, a.sub_network, "
                                            + "b.service_name, b.service_mt_shortcode, "
                                            + "b.service_mt_shortcode,"
                                            + "b.service_sub_type "
                                            + "from subscription a, subscription_service b "
                                            + "where a.sub_status ilike 'active' "
                                            + "and (to_date(a.sub_expiry_date, 'YYYY-MM-DD') >= date(CURRENT_TIMESTAMP) or b.service_period = 0) "
                                            + "and a.sub_service_id = b.id and a.sub_service_id = " + serviceID + " ");

                            while (dataResultSet.next()) {
                                boolean subscriberBilled = false;

                                if (Long.parseLong(nextService.getServicePeriod()) > 0
                                        && (Long.parseLong(nextService.getTimeOfBlast()) == getCurrentHour())) {
                                    subscriberBilled = true;
                                    getSendSMS().SMSBOX(dataResultSet.getString("service_sub_type"),
                                            nextService.getServiceMTShortCode(),
                                            dataResultSet.getString("sub_msisdn"),
                                            formatText(message.toString()),
                                            dataResultSet.getString("service_name"),
                                            dataResultSet.getLong("id"),
                                            srcModule,
                                            dataResultSet.getString("sub_network"),
                                            getServletContext().getInitParameter("dlrMask"));
                                } else {

                                    long amount = Long.parseLong(nextService.getServicePrice());

                                    try {

                                        gloPSA.makeGloChargingRquest(
                                                nextService, amount,
                                                srcModule, nextService.getServiceMTShortCode(), dataResultSet.getString("sub_msisdn"),
                                                getDBConnection(), message.toString());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    //                               Log custom billing status
                                    customBillingLogCondition(nextService,
                                            dataResultSet.getString("sub_msisdn"),
                                            formatText(message.toString()),
                                            srcModule, subscriberBilled);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
