package org.esme.controllers.feedreaders;

import java.util.ArrayList;

/**
 * @author saheedalbert
 */
public class ThisDayNewsFeedList {

    ArrayList<ThisDayNewsFeed> thisDayNewsFeeds;

    public ThisDayNewsFeedList(ArrayList<ThisDayNewsFeed> theFeeds) {
        this.thisDayNewsFeeds = theFeeds;
    }

}
