package org.esme.controllers;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.esme.broker.AppBroker;
import org.esme.controllers.apis.SubscriptionDetails;
import org.esme.controllers.helpers.InstantContentBroadCaster;
import org.esme.controllers.helpers.RabbitMQPublisher;
import org.esme.controllers.helpers.UpdateSubscription;
import org.esme.dao.models.ActiveSubscription;
import org.esme.dto.Service;
import org.esme.gateway.kannel.WEB2SMS;
import org.esme.utils.parser.ApproxStringMatchingUsingLevenshteinDistance;
import org.esme.utils.pingenerator.RandomPinGenerator;
import org.esme.webservices.etisalat.EtisalatDirectBilling;
import org.esme.webservices.glo.GloPSA;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeoutException;

@SuppressWarnings("serial")
public class SubEngine extends AppBroker {

    private static final Logger logger = Logger.getLogger(SubEngine.class);
    static Connection dbConnection;
    String src_module, BASE_URL;
    long subServicePeriod, subServiceID, subServiceOwnerID;
    PreparedStatement pstmt;
    BufferedReader reader;
    String subscriptionSource = "sms";
    String allMSISDN;
    //    ArrayList<Service> servicesCache;
    Service premiumService;
    ActiveSubscription activeSubscription;
    ArrayList<ActiveSubscription> activeSsubscriptionCache;

    ArrayList<Service> servicesCache;

    //Direct Billing
    EtisalatDirectBilling etisalatDirectBilling;

    //Glo PSA BILLING...
    GloPSA gloPSA;
    String gloPSAbillingResponse;

    String vendorUser;
    String vendorPassword;
    String vendorCode;
    String transactionID;

    WEB2SMS web2SMS = WEB2SMS.getInstance();

    InstantContentBroadCaster instantContentBroadCaster;

    static public int subscribeUserAccess(long id, long servID, String subMSISDN, long subServicePeriod,
                                          String reqText, String resText, String subNetwork,
                                          long subServiceOwner, String tob, String iob,
                                          String subShortCode, String status, String subExpiryDate,
                                          String subContentDeliveryMethod, String subscriptionSource, PreparedStatement _pstmt) throws SQLException {
        int update = 0;

        if (dbConnection == null) {
            dbConnection = getDBConnection();
        }

        logger.info("Creating Subcsription for " + subMSISDN);

        _pstmt.setLong(1, id);
        _pstmt.setLong(2, servID);
        _pstmt.setString(3, subMSISDN);
        _pstmt.setLong(4, subServicePeriod);
        _pstmt.setString(5, reqText);
        _pstmt.setString(6, resText);
        _pstmt.setString(7, subNetwork);
        _pstmt.setLong(8, subServiceOwner);
        _pstmt.setString(9, tob);
        _pstmt.setString(10, iob);
        _pstmt.setString(11, subShortCode);
        _pstmt.setString(12, status);
        _pstmt.setString(13, subExpiryDate);
        _pstmt.setString(14, subContentDeliveryMethod);
        _pstmt.setString(15, subscriptionSource);

        update = _pstmt.executeUpdate();
        return update;

    }

    //PrintWriter out;
//    HTTPRouter httpRouter;
    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace",
            "UseSpecificCatch", "UnusedAssignment", "unchecked", "unchecked",
            "unchecked", "unchecked", "unchecked", "unchecked", "deprecation", "unchecked", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("PROCESS REQUST CALLED");
        //response.setContentType("text/html;charset=UTF-8");

        String serviceMOShortCode = null, subMSISDN,
                subServiceTimeOfBlast = null, subServiceIntervalOfBlast = null,
                subRequestText = null, serviceNetwork = null,
                welcomeMessageShortCode = null, smsc = null,
                subResponseText = null,
                subExpiryDate = null,
                subContentDeliveryMethod = null, servicePromo;
        PrintWriter out = null;

        try {
            out = response.getWriter();
        } catch (IOException ex) {
        }

        try {

            try {
                //Collect the parameters coming from the SMS Gateway
                serviceMOShortCode = removePoison(request.getParameter("shortcode").replace("+", ""));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                smsc = removePoison(request.getParameter("smscID"));
                logger.info("SMSC " + smsc);
            } catch (NullPointerException e) {
            }

            try {

                subMSISDN = removePoison(request.getParameter("msisdn").replace("+", ""));
                subMSISDN = AppBroker.CountryCodePrefix_PROP + subMSISDN.substring(subMSISDN.length() - 10);
                logger.info("MSISDN " + subMSISDN);

            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            try {
                subRequestText = removePoison(request.getParameter("req_text").toLowerCase());
                logger.info("REQUEST TEXT " + subRequestText);

            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                subscriptionSource = removePoison(request.getParameter("sub_source").toLowerCase());
                logger.info("REQUEST SRC " + subscriptionSource);
            } catch (NullPointerException e) {
                subscriptionSource = "sms";
                e.printStackTrace();
            }

            // Check if MSISDN is blacklisted
            if (isBlacklisted(subMSISDN)) {
                logger.info(subMSISDN + " is blacklisted");
                return;
            }

            //For MTN SDP-SYNC ORDER REQUEST
            if (smsc.equalsIgnoreCase("mtn")) {

            }

            //STATUS CHECK.
            if (subRequestText.trim().toLowerCase().equalsIgnoreCase("STATUS")) {
                String status = checkSubStatus(subMSISDN, serviceMOShortCode);
                //send status..
                web2SMS.SMSBOX_NODLR(serviceMOShortCode, subMSISDN,
                        status, smsc);
                return;
            }

            //do not treat empty request...
            if (subRequestText.trim().toLowerCase().equalsIgnoreCase(" ")
                    || subRequestText.length() < 1
                    || subRequestText == null) {
                return;
            }

            //Check services archive availabbility
            //retrieve the service properties...
            if (servicesCache == null || servicesCache.size() < 1) {
                updateServicesCache();
                servicesCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            }

            // Handle Web Subscription
            if (subscriptionSource.equalsIgnoreCase("mobi_eti")
                    || subscriptionSource.equalsIgnoreCase("ivr_eti")) {

                // Check If there is an active subscription
                // Skip confirmation for Web subscription...
                // Get servicesCache from AppBroker
                for (Service _service : servicesCache) {

                    //Check if subscription exist...
                    if (subRequestText.trim().equalsIgnoreCase(_service.getServiceKeyword().trim())) {

                        logger.info("User New Sub, There is a match - " + _service.getServiceName());

                        String returnData = SubscriptionDetails.getSubscriptionDetails(
                                subMSISDN, _service, getDBConnection(), "json", smsc);

                        if (returnData.contains("not exist")) {
                            // Attempt to Subscribe msisdn
                            //Don't send welcome subscription
                            // Subscribe User
                            logger.info("Trying to Subscribe " + subMSISDN + " to " + _service.getServiceName());
                            logger.info("Subscription Source - " + subscriptionSource);
                            logger.info("Network - " + smsc);

                            routeToAnotherParty(_service, subMSISDN, subRequestText);

                            subExpiryDate = addDate(-1);

                            //For Etisalat web subscription...
                            //There's promo....
                            if (_service.getServicePromo().equalsIgnoreCase("yes")) {
                                subExpiryDate = addDate(Long.parseLong(_service.getServicePromoPeriod()) - 1);
                            }

                            long update = subscribeUser(RandomPinGenerator.randomPinGenerator(9), _service.getServiceID(), subMSISDN,
                                    Integer.parseInt(_service.getServicePeriod()), subRequestText,
                                    _service.getServiceSubscriptionMessage(), smsc, _service.getServiceOwner(),
                                    _service.getTimeOfBlast(), subServiceIntervalOfBlast,
                                    serviceMOShortCode, "active", subExpiryDate,
                                    _service.getServiceContentDeliveryMethod(), subscriptionSource);


                            serviceNetwork = smsc;

                            if (update > 0) {

                                updateSubscription(subMSISDN, _service);

                                //Get subscription detail to the calling API....
                                returnData = SubscriptionDetails.
                                        getSubscriptionDetails(subMSISDN, _service, getDBConnection(), "json", smsc);
                            }
                        }

                        out.print(returnData);
                        returnData = "";
                        return;
                    }
                }

            } else {

                for (Service service : servicesCache) {

                    //Check if subscription exist...
                    if (subRequestText.trim().equalsIgnoreCase(service.getServiceKeyword().trim())
                            || subRequestText.equalsIgnoreCase(service.getServiceDoubleConfirmationKeyword())) {

                        String returnData = SubscriptionDetails.getSubscriptionDetails(
                                subMSISDN, service, getDBConnection(), "json");
                        if (returnData.contains("not exist")) {

                        } else {

                            web2SMS.SMSBOX_NODLR(
                                    service.getServiceShortCode(),
                                    subMSISDN, "Hello, you are already subscribed to this service.",
                                    service.getServiceNetwork());
                            return;
                        }
                    }

                    logger.info("We entered service archive...");
                    if (subRequestText.equalsIgnoreCase(service.getServiceDoubleConfirmationKeyword())) {

                        logger.info("Short code..." + serviceMOShortCode + "Confirmation keyword..."
                                + service.getServiceDoubleConfirmationKeyword());
                        //It's a confirmation keyword...now check if he's sent main keyword for the service
                        //String subscriber, String serviceMOShortCode,String subRequestText, Service service
                        if (processedConfirmationKeyword(subMSISDN, serviceMOShortCode, subRequestText.trim(), service)) {
                            logger.info("It's a confirmation keyword...");
                            // Route if defined
                            routeToAnotherParty(service, subMSISDN, subRequestText);
                            return;
                        }
                    }


                }
            }

            //For integrat mtn renewals...They want to use MO to indicate renewal from SDP.
            if (subRequestText.toLowerCase().contains("renew")) {
                String serviceMainKey = subRequestText.substring(5);
                for (Service service : servicesCache) {
                    if (serviceMainKey.trim().equalsIgnoreCase(service.getServiceKeyword().trim())) {

                        (new MTBillingResponseCallback()).publishToAMPQ(subMSISDN, service.getServiceID(), serviceMOShortCode, smsc, "SubscriptionRenewal", 8, rabbitQueueName_PROP);

//                        try {
//                            rabbitPublisher(subRequestText, subMSISDN, serviceMOShortCode,
//                                    smsc, "SRCMSubscriptionRenewal", service, rabbitQueueName_PROP);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }

                        return;
                    }
                }
            }

            //Check if servic exist...We use keyword and short code to confirm....
            boolean found = false;
            String serviceKewords = null;
            String allowableKeywords = null;
            boolean isAllowableKeyword = false;

            logger.info("Process Request Called and We are about to process...");
            logger.info("SC: " + serviceMOShortCode + " KW: " + subRequestText);

            for (Service service : servicesCache) {

                serviceKewords = service.getServiceKeyword();

                allowableKeywords = service.getServiceAllowableKeyword();

                // Check for null
                if (allowableKeywords != null) {
                    allowableKeywords = allowableKeywords.replace(",", " ");
                    isAllowableKeyword = isKeywordExist(allowableKeywords, subRequestText);

                    logger.info("SC: " + serviceMOShortCode + " KW: " + allowableKeywords);
                }

                logger.info("SC: " + serviceMOShortCode + " KW: " + serviceKewords);

                //Take the first 3 letters of both inbound SMSC and Service Network
                String moNetwork = smsc.substring(0, Math.min(smsc.length(), 3));

                String theServiceNetwork = service.getServiceNetwork().substring(
                        0, Math.min(service.getServiceNetwork().length(), 3));

                if ((moNetwork.toLowerCase().equalsIgnoreCase(theServiceNetwork.toLowerCase()))
                        && (serviceMOShortCode.equalsIgnoreCase(service.getServiceShortcode()))
                        && (isKeywordExist(serviceKewords, subRequestText) || isAllowableKeyword)) {

                    logger.info("He's trying to subscribe to " + service.getServiceName());

                    premiumService = service;

                    subServiceID = service.getServiceID();
                    subServicePeriod = Integer.parseInt(service.getServicePeriod());
                    subResponseText = service.getServiceSubscriptionMessage();
                    welcomeMessageShortCode = service.getServiceWelcomeMessageShortcode();
                    subServiceOwnerID = service.getServiceOwner();

                    serviceNetwork = smsc;

                    if (serviceNetwork.equalsIgnoreCase("etisalat") || serviceNetwork.equalsIgnoreCase("etisalat_bc")) {
                        serviceNetwork = prop.getProperty("etiContentBind");
                    }

                    servicePromo = service.getServicePromo();
                    subExpiryDate = addDate(-1);

                    //For Etisalat web subscription...
                    //There's promo....
                    if (servicePromo.equalsIgnoreCase("yes")) {
                        subExpiryDate = addDate(Long.parseLong(service.getServicePromoPeriod()) - 1);
                    }

                    // REDUNDANT: Getting Ready to send content
                    subServiceTimeOfBlast = service.getTimeOfBlast();
                    subContentDeliveryMethod = service.getServiceContentDeliveryMethod();

                    found = true;
                    break;

                }
            }

            //If service is found....
            if (found) {

                try {
                    prop = AppBroker.getPropertyFileHandle();

                } catch (Exception e) {
                    logger.info("ERROR: Could not load app property file.");
                    e.printStackTrace();
                }

                String contentBind = null;

                if (serviceNetwork.toLowerCase().contains("etisalat")) {
                    contentBind = prop.getProperty("etiContentBind");
                } else {
                    contentBind = serviceNetwork;
                }

                //Generate subscription ID....
                long subID = RandomPinGenerator.randomPinGenerator(9);
                int update = 0;

                // Instantiate the Subscriber Status to inactvie
                String theStatus = "inactive";

                //subscribe the user
                try {

                    // Disable Double confirmation based on settings and for all subscriber from the web.
                    // Set subscription to active
                    // Subscribe if user request subscription with allowable keyword
                    if (premiumService.getServiceDoubleConfirmation().equalsIgnoreCase("no")
                            || isAllowableKeyword) {
                        theStatus = "active";
                    }

                    // Subscribe User
                    update = subscribeUser(subID, subServiceID, subMSISDN,
                            subServicePeriod, subRequestText,
                            subResponseText, serviceNetwork, subServiceOwnerID,
                            subServiceTimeOfBlast, subServiceIntervalOfBlast,
                            serviceMOShortCode, theStatus, subExpiryDate,
                            subContentDeliveryMethod, subscriptionSource);

                    //In integrat mtn...Send content
                    if (premiumService.getServiceNetwork().trim().equalsIgnoreCase("integrat_mtn")) {

                        (new MTBillingResponseCallback()).publishToAMPQ(subMSISDN, subServiceID, premiumService.getServiceMTShortCode(), smsc, "NewSubscription", 8, rabbitQueueName_PROP);

                        //For billing report and first content
//                        rabbitPublisher(subRequestText, subMSISDN, serviceMOShortCode, smsc,
//                                "SRCMNewSubscription", premiumService, rabbitQueueName_PROP);
                        //sendFirstMTContent(premiumService, subMSISDN);
                        return;
                    }

                    // Special Scenario for IVR subscriber
                    if (subscriptionSource.equalsIgnoreCase("ivr_eti")
                            && premiumService.getServicePromo().equalsIgnoreCase("no")) {

                        String welcomeMessage = formatText(premiumService.getServiceDoubleConfirmationMessage());

                        if (welcomeMessage.length() < 5) {
                            welcomeMessage = formatText(premiumService.getServiceSubscriptionMessage());
                        }

                        logger.info("Subscription Source: IVR - Sending subscription notification to " + subMSISDN);
                        web2SMS.SMSBOX(premiumService.getServiceSubType(), premiumService.getServiceBillingShortCode(),
                                subMSISDN, welcomeMessage,
                                premiumService.getServiceName(), premiumService.getServiceID(),
                                "NewSubscription", premiumService.getServiceBillingNetwork(), DLR_MASK);
                        return;

                    }

                    // Send Subscription Notification to Subscribers from IVR_ETI marketing platform
                    if (subscriptionSource.equalsIgnoreCase("ivr_eti")) {

                        String welcomeMessage = formatText(premiumService.getServiceDoubleConfirmationMessage());
                        if (welcomeMessage.length() < 5) {
                            welcomeMessage = formatText(premiumService.getServiceSubscriptionMessage());
                        }

                        //Deal with no promo -  promo period determines message sent...
                        if (premiumService.getServicePromo().equalsIgnoreCase("no")) {

                            web2SMS.SMSBOX(premiumService.getServiceSubType(),
                                    premiumService.getServiceBillingShortCode(),
                                    subMSISDN, welcomeMessage,
                                    premiumService.getServiceName(), premiumService.getServiceID(),
                                    "NewSubscription", premiumService.getServiceBillingNetwork(),
                                    getServletContext().getInitParameter("dlrMask"));

                        } else {

                            logger.info("Subscription Source: IVR - Sending subscription notification to " + subMSISDN);
                            if (Long.parseLong(premiumService.getServicePromoPeriod()) == 0) {
                                web2SMS.SMSBOX(premiumService.getServiceSubType(),
                                        premiumService.getServiceBillingShortCode(),
                                        subMSISDN, formatText(prop.getProperty("chargeNotification_"
                                                + premiumService.getServiceID())),
                                        premiumService.getServiceName(), premiumService.getServiceID(),
                                        "NewSubscription", premiumService.getServiceBillingNetwork(),
                                        getServletContext().getInitParameter("dlrMask"));
                            } else {

                                web2SMS.SMSBOX(premiumService.getServiceSubType(), premiumService.getServiceShortCode(),
                                        subMSISDN, welcomeMessage,
                                        premiumService.getServiceName(), premiumService.getServiceID(),
                                        "NewSubscription", etiContentBind_PROP, DLR_MASK);
                            }
                        }
                        return;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }

                if (update > 0) {

                    //Send subscription notification...
                    logger.info("Sending subscription notification to... " + subMSISDN);
                    try {

//                        String theMSISDN = "0" + subMSISDN.substring(subMSISDN.length() - 10);
                        String theMSISDN = subMSISDN;
                        logger.info(premiumService.getServiceSubscriptionMessage());

//                        web2SMS.SMSBOX_NODLR(welcomeMessageShortCode, subMSISDN,
//                                formatText(premiumService.getServiceSubscriptionMessage().replace("MSISDN", theMSISDN)), contentBind);
                        String msg = premiumService.getServiceSubscriptionMessage();
                        String servName = premiumService.getServiceName();
                        long servId = premiumService.getId();

                        // If Subscription is active, attempt to send first message
                        //by setting srcModule to NewSubscription and expecting a DLR
                        if (theStatus.equalsIgnoreCase("active")) {
                            web2SMS.SMSBOX("MT", welcomeMessageShortCode,
                                    theMSISDN, msg, servName, servId,
                                    "NewSubscription", contentBind, "24");
                        } else {
                            // Not expecting content if Subscription requires confirmation
                            web2SMS.SMSBOX_NODLR(welcomeMessageShortCode, theMSISDN, msg, contentBind);
                        }

                        logger.info("Sent subscription notification to... " + subMSISDN);

                    } catch (Exception e) {
                        logger.info("Unable to send subscription notification to... " + subMSISDN);
                        e.printStackTrace();
                    }
                    //For web sub...
                    if (subscriptionSource.equalsIgnoreCase("web")) {
                        request.setAttribute("message", premiumService.getServiceSubscriptionMessage());
                        request.getRequestDispatcher("manual_sub.jsp").forward(request, response);
                    }
                }

            } else {

                //Send user a closely matched Keyword/Help message to subscribe
                String allKeywordsDictionary = " ";
                String returnMessage = " ";
                String systemSugestedKeyword = " ";
                String mostMatchedKeyword = "";

                try {

                    for (Service service : servicesCache) {
                        allKeywordsDictionary = allKeywordsDictionary + service.getServiceKeyword() + " " + service.getServiceAllowableKeyword().replace(",", " ") + " ";
                    }

                    returnMessage = new ApproxStringMatchingUsingLevenshteinDistance().
                            getCloseMatch(allKeywordsDictionary, subRequestText.replace(" ", ""));
                    systemSugestedKeyword = returnMessage;

                    logger.info("SYSTEM SUGGESTED KEYWORD: " + systemSugestedKeyword);
                    mostMatchedKeyword = systemSugestedKeyword.substring(0, systemSugestedKeyword.indexOf(" ")).toUpperCase();

                } catch (IndexOutOfBoundsException e) {
                    mostMatchedKeyword = systemSugestedKeyword.toUpperCase();
                } catch (Exception e) {
                }

                try {

                    prop = AppBroker.getPropertyFileHandle();

                    if (smsc.toLowerCase().contains("etisalat")) {
                        smsc = etiContentBind_PROP;
                    }

                    String notFoundNotification = "Did you mean: " + mostMatchedKeyword + "? If yes, text " + mostMatchedKeyword + " or HELP for help";
                    web2SMS.SMSBOX_NODLR(serviceMOShortCode,
                            subMSISDN, notFoundNotification, smsc);
                    logger.info(notFoundNotification);

                } catch (Exception E) {

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    // TODO: Single Point of Entry for User subscription
    // TODO: Create Necessary Filters/Rules before actual creation

    //Log every request.....
    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace"})
    public int subscribeUser(long id, long servID, String subMSISDN, long subServicePeriod,
                             String reqText, String resText, String subNetwork,
                             long subServiceOwner, String tob, String iob,
                             String subShortCode, String status, String subExpiryDate,
                             String subContentDeliveryMethod, String subscriptionSource) throws SQLException {

        pstmt = dbConnection.prepareStatement(
                "insert into subscription("
                        + "id,sub_service_id,sub_msisdn,sub_service_period,sub_request_text,"
                        + "sub_response_text,sub_network,sub_service_owner_id,"
                        + "sub_tob,sub_iob,sub_shortcode,sub_status,"
                        + "sub_expiry_date,sub_content_delivery_method,sub_source)"
                        + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        return subscribeUserAccess(id, servID, subMSISDN, subServicePeriod,
                reqText, resText, subNetwork, subServiceOwner, tob, iob, subShortCode, status, subExpiryDate,
                subContentDeliveryMethod, subscriptionSource, pstmt);

    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "null"})
    public String checkSubStatus(String subscriber, String serviceShortCode) {

        String status = "You are currently subscribed to\n ";
        String availableService = "";
        Integer index = 0;
        @SuppressWarnings("UnusedAssignment")
        Statement ps = null;
        @SuppressWarnings("UnusedAssignment")
        ResultSet rs = null;
        String msisdn = subscriber.substring(subscriber.length() - 9);

        try {

            if (dbConnection == null) {
                dbConnection = getDBConnection();
            }

            ps = dbConnection.createStatement();
            rs = ps.executeQuery(
                    "select a.service_name, b.sub_expiry_date "
                            + "from subscription_service a, subscription b "
                            + "where b.sub_msisdn ilike '%" + msisdn + "' "
                            + "and b.sub_status = 'active' "
                            + "and a.id = b.sub_service_id ");

            while (rs.next()) {
                index++;
                availableService += index + " " + rs.getString("service_name").toUpperCase() + ", ";
            }
            if (index > 0) {
                status += availableService + ". Text Stop to " + serviceShortCode + " to unsubscribe";
            } else {
                status = "You are currently not subscribed to any service on " + serviceShortCode + " Text HELP for help";
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return status;
    }

    public boolean isBlacklisted(String subscriber) {
        Statement ps;
        ResultSet rs;
        String msisdn = subscriber.substring(subscriber.length() - 9);

        try {
            if (dbConnection == null) {
                dbConnection = getDBConnection();
            }
            ps = dbConnection.createStatement();

            rs = ps.executeQuery("select blacklisted from subscription where sub_msisdn ilike '%" + msisdn + "' and blacklisted ilike '%blacklist%' ");

            while (rs.next()) {
                if (!rs.getString("blacklisted").isEmpty()) {
                    return true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "null"})
    public boolean isActiveSubscriber(String subscriber, Service service) {

        boolean isSubscribed = false;
        Statement ps;
        ResultSet rs;
        String msisdn = subscriber.substring(subscriber.length() - 9);

        try {

            if (dbConnection == null) {
                dbConnection = getDBConnection();
            }

            ps = dbConnection.createStatement();

            rs = ps.executeQuery(
                    "select a.service_name,a.service_period, b.sub_expiry_date, b.sub_status, b.blacklisted "
                            + "from subscription_service a, subscription b "
                            + "where b.sub_service_id = " + service.getServiceID() + "  "
                            + "and b.sub_msisdn ilike '%" + msisdn + "' "
                            + "and a.id = b.sub_service_id order by sub_request_time desc limit 1");

            while (rs.next()) {

                //Extend his subscription if it is MO based theParentService....
                if (service.getServiceSubType().equalsIgnoreCase("mo")) {

                    PreparedStatement ips = dbConnection.prepareStatement(
                            "update subscription set last_billed_date = CURRENT_TIMESTAMP, "
                                    + "sub_expiry_date = DATE '" + rs.getString("sub_expiry_date") + "' + " + rs.getInt("service_period") + ", blacklisted =  ''  "
                                    + "where sub_msisdn ilike '%" + msisdn + "' "
                                    + "and sub_service_id = " + service.getServiceID() + " ");

                    ips.executeUpdate();

                    //His service is MT based but his subscription is inactive...
                } else if (service.getServiceSubType().equalsIgnoreCase("mt")
                        && rs.getString("sub_status").equalsIgnoreCase("inactive")) {

                    PreparedStatement ips = dbConnection.prepareStatement(
                            "update subscription set sub_status = 'active', blacklisted = ' ' "
                                    + "where sub_msisdn ilike '%" + msisdn + "' "
                                    + "and sub_service_id = " + service.getServiceID() + " ");

                    ips.executeUpdate();

                }
                isSubscribed = true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return isSubscribed;
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "null"})
    public boolean processedConfirmationKeyword(
            String subscriber, String serviceMOShortCode,
            String subRequestText, Service service) {

        boolean isSubscribed = false;
        Statement ps;
        ResultSet rs;
        String msisdn = subscriber.substring(subscriber.length() - 10);

        try {

            if (dbConnection == null) {
                dbConnection = getDBConnection();
            }

            ps = dbConnection.createStatement();

            //We want to pick the most recent subscription for service that has not being activated.
            rs = ps.executeQuery(
                    "select sub_msisdn from subscription "
                            + "where sub_msisdn ilike '%" + msisdn + "%' "
                            + "and sub_service_id = " + service.getServiceID() + "  "
                            + "order by sub_request_time desc limit 1");

            //if exist, we will update his subscription
            while (rs.next()) {

                logger.info("THERE IS RECORD");
                isSubscribed = true;
                updateSubscription(subscriber, service);
                //isSubscribed = true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return isSubscribed;
    }

    public void routeToAnotherParty(Service service, String subMSISDN, String subRequestText) {

        logger.info("Attempting to Route to 3rd party ==> ");
        logger.info("ROUTE " + service.getRouteToExternalAPI());

        //informing third party if routing service...
        try {
            //Service if found...Check if we routing to third party..
            if (service.getRouteToExternalAPI().equalsIgnoreCase("yes")) {
                try {

                    HttpClient client;
                    GetMethod method;
                    byte[] responseBody;

                    String theURL = service.getExternalAPIURL();
                    String myParam = URIUtil.encodeQuery(service.getExternalAPIShortCodeParam() + "=" + service.getServiceShortCode()
                            + "&" + service.getExternalAPIMSISDNParam() + "=" + subMSISDN
                            + "&" + service.getExternalAPIMessageParam() + "=" + subRequestText.trim());

                    String url = theURL + myParam;

                    logger.info(url);
                    client = new HttpClient();
                    method = new GetMethod(url);

                    // Execute the method.
                    int statusCode = client.executeMethod(method);
                    if (statusCode != HttpStatus.SC_OK) {
                        logger.info("Method failed: " + method.getStatusLine());
                    }
                    responseBody = method.getResponseBody();
                    // Use caution: ensure correct character encoding and is not binary data
                    String theRes = new String(responseBody);
                    logger.info(theRes);
                    //}

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {

        }
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "null"})
    public boolean updateSubscription(String subscriber, Service service) {
        //String msisdn = subscriber.substring(subscriber.length() - 10);
        boolean updated = false;
        web2SMS = WEB2SMS.getInstance();

        try {
            //Check if we are running free promo or not...
            if (service.getServicePromo().equalsIgnoreCase("YES")) {

                // Give subscriber Promo using the preset Promo 
                updated = UpdateSubscription.doUpdate(getDBConnection(), service, subscriber, "promo");

                if (updated) {
                    web2SMS.SMSBOX(service.getServiceSubType(), service.getServiceShortCode(),
                            subscriber, formatText(service.getServiceDoubleConfirmationMessage()),
                            service.getServiceName(), service.getServiceID(),
                            "NewSubscription", etiContentBind_PROP,
                            getServletContext().getInitParameter("dlrMask"));
                }

            } else {

                //We are not running promo....
                String billingNotification;
                billingNotification = service.getServiceRenewalMessage();
                try {
                    billingNotification = prop.getProperty("chargeNotification_" + String.valueOf(service.getServiceID()));
                } catch (Exception e) {
                }

                logger.info("Billing Notification for new subscription - " + billingNotification);

                if (service.getServiceSubType().equalsIgnoreCase("EDB")) {

                    updated = true;
                    etisalatDirectBilling = EtisalatDirectBilling.getInstance();
                    etisalatDirectBilling.makeEtisalatDirectBillingBillingRequest(
                            service, "NewSubscriptionCharge", service.getServiceBillingShortCode(),
                            "NILL", billingNotification, subscriber);

                } else if (service.getServiceSubType().equalsIgnoreCase("UCIP")) {
                    updated = true;
                } else if (service.getServiceSubType().equalsIgnoreCase("PSA")) {
                    updated = true;
                    gloPSA = GloPSA.getInstance();
                    gloPSABilling(gloPSA, service, subscriber);

                } else {
                    // NO PROMO MT Billing
                    //Update User subscription and set promo period to 0
//                    updated = UpdateSubscription.doUpdate(getDBConnection(), service, subscriber, "premium");
//                    if (updated) {
                    //Try renew users subcription immediately
                    web2SMS.SMSBOX(service.getServiceSubType(), service.getServiceBillingShortCode(),
                            subscriber, billingNotification,
                            service.getServiceName(), service.getServiceID(),
                            "NewSubscriptionCharge", service.getServiceBillingNetwork(),
                            getServletContext().getInitParameter("dlrMask"));
                    //}
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return updated;
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "null"})
    public void sendFirstMOContent(String billingShortCode, String subscriber, long serviceID) {
        PreparedStatement st = null;
        try {

            if (dbConnection == null) {
                dbConnection = getDBConnection();
            }

            st = dbConnection.prepareStatement(
                    "select service_name, service_mt_shortcode, service_content_delivery_method, "
                            + "service_tob, welcome_shortcode, fallback_shortcode, "
                            + "service_period, service_fallback_period,service_sub_type "
                            + "from subscription_service where id = ?");
            st.setLong(1, serviceID);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                //If content has gone out before this update...
                //long servID, String serviceName, String mtShortCode, long timeOfBlast, String contentDelMethod
                if (getCurrentHour() > rs.getLong("service_tob")) {
                    getContent(serviceID, rs.getString("service_name"),
                            rs.getString("service_mt_shortcode"),
                            rs.getString("service_content_delivery_method"),
                            subscriber, rs.getString("service_sub_type"),
                            rs.getString("service_network"));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
            }

        }
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace"})
    public void getContent(long servID, String serviceName,
                           String mtShortCode, String contentDelMethod,
                           String subMSISDN, String netWork, String serviceSubType) {

        logger.info("Sending first content...");
        if (contentDelMethod.equalsIgnoreCase("sequential")) {
            int lastContentDay = 1;
            try {

                if (dbConnection == null) {
                    dbConnection = getDBConnection();
                }

                //insert new record in sequential content log table... pstmt =
                dbConnection.prepareStatement(
                        "insert into sequential_content_log "
                                + "(content_service_id,subscriber,last_content_date,last_content_day) "
                                + "values(?,?,?,?)");

                long epochTime = System.currentTimeMillis() / 1000L;
                pstmt.setLong(1, servID);
                pstmt.setString(2, subMSISDN);
                pstmt.setTimestamp(3, new Timestamp(epochTime));
                pstmt.setLong(4, lastContentDay);
                int theUpdate = pstmt.executeUpdate();

                //Query and send the actual content...
                if (theUpdate > 0) {

                    PreparedStatement selectPstmt = dbConnection.prepareStatement(
                            "select a.content from sequential_content a, subscription_service b "
                                    + "where content_blast_day = '" + lastContentDay + "' "
                                    + "and  a.content_service_id = " + servID + " "
                                    + "and a.content_service_id = b.id limit 1");

                    ResultSet rs = selectPstmt.executeQuery();
                    while (rs.next()) {
                        try {
                            web2SMS.SMSBOX(serviceSubType, mtShortCode,
                                    subMSISDN, formatText(rs.getString("content")),
                                    serviceName, servID, "DailyAlert", netWork,
                                    getServletContext().getInitParameter("dlrMask"));

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        } else if (contentDelMethod.equalsIgnoreCase("byDate")) {
            try {

                if (dbConnection == null) {
                    dbConnection = getDBConnection();
                }

                PreparedStatement selectPstmt = dbConnection.prepareStatement(
                        "select a.content from content a, subscription_service b where "
                                + "(trim(a.content_date_of_blast) = '" + cusFormatDateToString(new Date()).trim() + "' "
                                + "or trim(a.content_date_of_blast) = '" + formatDateToString(new Date()).trim() + "') "
                                + "and a.content_service_id = " + servID + " and a.content_service_id = b.id limit 1");

                ResultSet rs = selectPstmt.executeQuery();
                while (rs.next()) {
                    try {
                        web2SMS.SMSBOX(serviceSubType, mtShortCode,
                                subMSISDN, formatText(rs.getString("content")),
                                serviceName, servID, "DailyAlert", netWork,
                                getServletContext().getInitParameter("dlrMask"));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        } else {
            try {

                if (dbConnection == null) {
                    dbConnection = getDBConnection();
                }

                PreparedStatement selectPstmt = dbConnection.prepareStatement(
                        "select a.content from content a, "
                                + "subscription_service b "
                                + "where a.content_service_id = " + servID + " "
                                + "and a.content_service_id = b.id "
                                + "order by random() limit 1");

                ResultSet rs = selectPstmt.executeQuery();

                while (rs.next()) {
                    try {
                        web2SMS.SMSBOX(serviceSubType, mtShortCode,
                                subMSISDN, formatText(rs.getString("content")),
                                serviceName, servID, "DailyAlert", netWork,
                                getServletContext().getInitParameter("dlrMask"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
////

    boolean isKeywordExist(String allKeywords, String requestText) {
        boolean isExist = false;

        //If he send empty string...
        if (requestText.length() == 0 || requestText.equalsIgnoreCase(" ")) {
            isExist = true;
            return isExist;
        }

        String[] theServiceKeywords = allKeywords.split(" ");

        for (String theServiceKeyword : theServiceKeywords) {
            String[] theSubscriberKeywords = requestText.split(" ");
            for (String theSubKeyword : theSubscriberKeywords) {
                if (theServiceKeyword.equalsIgnoreCase(theSubKeyword)) {
                    isExist = true;
                    break;
                }
            }
        }

        return isExist;
    }

    //    private static MaspEventResponseData createMaspEventInboundSummary(
//            etisalat.unified.service.client.MaspEventSummaryData data) {
//        etisalat.unified.service.client.MaspServiceWS_Service service
//                = new etisalat.unified.service.client.MaspServiceWS_Service();
//        etisalat.unified.service.client.MaspServiceWS port = service.getMaspServiceWSPort();
//        return port.createMaspEventInboundSummary(data);
//    }
    @SuppressWarnings("CallToPrintStackTrace")
    public void gloPSABilling(GloPSA gloPSA, Service theService, String subMSISDN) {

        //wE WILL REMOVE HERE WHEN THE SORT THE BILLING ISSU end
        long amount = Long.parseLong(theService.getServicePrice());
        //Create an instance of the billing API
        //Attempt main code billing...
        try {
            gloPSA.makeGloChargingRquest(
                    theService, amount,
                    "NewSubscription", theService.getServiceBillingShortCode(), subMSISDN,
                    getDBConnection(), "");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void rabbitPublisher(
            String subRequestText, String subMSISDN,
            String serviceMOShortCode, String smsc, String srcModule,
            Service service,
            String rabbitQueueName_PROP) throws IOException, TimeoutException {

        RabbitMQPublisher publisher;
        String QUEUE_NAME;
        QUEUE_NAME = rabbitQueueName_PROP;
        publisher = RabbitMQPublisher.getInstance();
        String dlr = "8";

        try {
            publisher.publishDLRToRabbit(
                    service.getServiceID() + "*"
                            + subMSISDN + "*"
                            + serviceMOShortCode + "*"
                            + srcModule + "*"
                            + dlr + "*"
                            + smsc + "*"
                            + encodeParams(formatDateToString2(new Date())), QUEUE_NAME);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
