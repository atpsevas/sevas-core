package org.esme.controllers;

import com.csvreader.CsvReader;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.FetchBlacklistedNumbers;
import org.esme.controllers.helpers.MessageBroadCaster;
import org.esme.dao.models.SMSCampaign;
import org.esme.dao.models.Users;
import org.esme.dto.Service;
import org.esme.utils.datastructure.Modifier;
import org.esme.utils.pingenerator.RandomPinGenerator;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

@SuppressWarnings("serial")
public class SMSCampaignScheduler extends AppBroker {

    String senderID;
    String date;
    String schedule;
    long time;
    File uploadFile = null;
    BufferedReader reader;
    String message;
    String user;
    String userRole;
    long serviceID;
    long userID;
    ArrayList<Service> serviceCache;
    String serviceName, serviceType, serviceNetwork;
    ArrayList<Users> userCache;
    long campaignID;
    SMSCampaign campaign;
    ArrayList<SMSCampaign> campaignCache;
    PreparedStatement pst;

    double uploadedFileSize;
    double uploadedFileSizeInMB;
    double fileSizeAfterFilterInMB;

    long totalNumerbUploaded;
    long totalDuplicateNumber;
    long existingSubscribers;
    long totalUniqueNumber;
    long redundantNumber;

    Properties theProp;

    //Campaign Helper...
    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "UseSpecificCatch", "unchecked", "unchecked", "unchecked", "unchecked", "unchecked", "unchecked", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        prop = AppBroker.getPropertyFileHandle();


        try {

            response.setContentType("text/html;charset=UTF-8");
            ServletContext ctx = getServletContext();
            HttpSession session = request.getSession(true);

            user = (String) session.getAttribute("userName");
            userRole = (String) session.getAttribute("UserRole");
            campaignID = RandomPinGenerator.randomPinGenerator(9);

            //get the file using apache commons.
            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setRepository(new File(getServletContext().getRealPath("/")));
            ServletFileUpload upload = new ServletFileUpload(factory);
            @SuppressWarnings("UnusedAssignment")
            List<FileItem> items;

            try {
                items = upload.parseRequest(request);
            } catch (FileUploadException ex) {
                ex.printStackTrace();
                request.setAttribute("message", "ERROR:"
                        + "Unable to upload the file specified! Check and try again.");
                request.getRequestDispatcher("sms_campaign.jsp").forward(request, response);
                return;
            }
            for (FileItem item : items) {
                if (item.isFormField()) {
                    //processFormField(item);
                    if (item.getFieldName().equalsIgnoreCase("service")) {
                        //Acquire service ID
                        serviceID = Long.parseLong(item.getString());
                    } else if (item.getFieldName().equalsIgnoreCase("senderid")) {
                        try {
                            senderID = item.getString();
                        } catch (NullPointerException e) {
                            request.setAttribute("message", "Please enter a sender ID.");
                            request.getRequestDispatcher(
                                    "sms_campaign.jsp").forward(request, response);
                            return;
                        }
                    } else if (item.getFieldName().equalsIgnoreCase("message")) {
                        try {
                            message = item.getString();
                        } catch (NullPointerException e) {
                            request.setAttribute("message", "Please enter message to be sent.");
                            request.getRequestDispatcher(
                                    "sms_campaign.jsp").forward(request, response);
                            return;
                        }
                    } else if (item.getFieldName().equalsIgnoreCase("date")) {
                        try {
                            date = item.getString();
                        } catch (NullPointerException e) {
                            request.setAttribute("message", "Please select date.");
                            request.getRequestDispatcher(
                                    "sms_campaign.jsp").forward(request, response);
                            return;
                        }
                    } else if (item.getFieldName().equalsIgnoreCase("stob")) {
                        try {
                            time = Long.parseLong(item.getString());
                        } catch (NullPointerException e) {
                            request.setAttribute("message", "Please select time.");
                            request.getRequestDispatcher(
                                    "sms_campaign.jsp").forward(request, response);
                            return;
                        }
                    } else if (item.getFieldName().equalsIgnoreCase("scdm")) {
                        try {
                            schedule = item.getString();
                        } catch (NullPointerException e) {
                            request.setAttribute("message", "Please set scedule to YES or NO.");
                            request.getRequestDispatcher(
                                    "sms_campaign.jsp").forward(request, response);
                            return;
                        }
                    }

                } else {
                    //processUploadedFile(item);
                    String filename = item.getName();
                    File f = new File(filename);
                    filename = f.getName();
                    // get rid of spaces in filename.
                    filename = filename.replace(" ", "_");
                    //Validate file extension
                    if (!(filename.endsWith(".csv"))) {
                        request.setAttribute("message", "ERROR:"
                                + "You have selected an invalid file type!");
                        request.getRequestDispatcher("sms_campaign.jsp").forward(request, response);
                        return;
                    } else {

                        //Saving the File in the application root folder
                        uploadFile = new File(ctx.getInitParameter("SMSCapaignDir") + date + "_" + time + "_" + filename);

                        try {

                            item.write((uploadFile));
                            uploadedFileSize = uploadFile.length();
                            uploadedFileSizeInMB = uploadedFileSize / (float) (1024 * 1024);

                        } catch (Exception ex) {
                            request.setAttribute("message", ""
                                    + "SEVERE: Error while uploading your file. Please try again");
                            request.getRequestDispatcher("sms_campaign.jsp").forward(request, response);
                            ex.printStackTrace();
                            return;
                        }

                        try {

                            //Removing dubplicate from uloaded file..read to arraylist first then clean with a custom funtion..
                            ArrayList<String> campaignTarget;
                            HashMap existingSubscribersBase;
                            Modifier filterCampaign;
                            FileWriter writer;
                            HashMap blacklistedNumbers;

                            campaignTarget = new ArrayList<>();
                            CsvReader fileReader = new CsvReader(uploadFile.getAbsolutePath());
                            while (fileReader.readRecord()) {
                                String recipient = fileReader.get(0);
                                campaignTarget.add(recipient);
                            }

                            //Get total number uploaded
                            totalNumerbUploaded = campaignTarget.size();

                            org.esme.broker.AppBroker.sevasLogger.info("Campaign Target Before  Duplicates " + totalNumerbUploaded);
                            //Remove duplicate using set..
                            Set<String> hs = new HashSet<>();
                            hs.addAll(campaignTarget);
                            campaignTarget.clear();
                            campaignTarget.addAll(hs);

                            //Get total duplica numbers..
                            totalDuplicateNumber = totalNumerbUploaded - campaignTarget.size();
                            org.esme.broker.AppBroker.sevasLogger.info("Duplicates " + totalDuplicateNumber);

                            //Get existing subscriber count
                            existingSubscribersBase = (HashMap) getUniqueSubscriber(serviceID);
                            existingSubscribers = existingSubscribersBase.size();

                            org.esme.broker.AppBroker.sevasLogger.info("Existing active subscribers " + existingSubscribers);

                            try {

                                //Clean the base and also take out existing subscribers from it...
                                // filterCampaign = new FilterCampaignDB();
                                //uniqueCampaignTarget = filterCampaign.cleanCampaignDB(campaignTarget, existingSubscribersBase);
                                org.esme.broker.AppBroker.sevasLogger.info("Total MSISDN before blacklist filtered... " + campaignTarget.size());

                                //Remove blacklisted numbers....
                                //Get Blacklisted number into arraylist...
                                HashMap<Integer, String> uniqueCampaignTargetMap = new HashMap<>();

                                //Load uniqueCampaignTarget into map for faster filtering against blacklisted
                                int uniqueCampaignTargetMapIndex = 0;

                                org.esme.broker.AppBroker.sevasLogger.info("Converting campaign target from list to map...Starting at. " + new Date());
                                for (String blistMSISDN : campaignTarget) {
                                    uniqueCampaignTargetMap.put(uniqueCampaignTargetMapIndex, blistMSISDN);
                                    uniqueCampaignTargetMapIndex++;
                                }
                                org.esme.broker.AppBroker.sevasLogger.info("Converting campaign target from list to map...finished at. " + new Date());

                                org.esme.broker.AppBroker.sevasLogger.info("Fetching blacklisted MSISDN....  ");
                                blacklistedNumbers = (HashMap) getServletContext().getAttribute("blackListedNumbersCache");
                                if (blacklistedNumbers.size() == 0) {
                                    blacklistedNumbers = (HashMap) new FetchBlacklistedNumbers(theProp.getProperty("blacklistURL")).getBlacklistedNumbers();
                                }

                                org.esme.broker.AppBroker.sevasLogger.info("Total Fetched....  " + blacklistedNumbers.size());
                                org.esme.broker.AppBroker.sevasLogger.info("Total MSISDN in blacklist map before adding active sub filtered... " + blacklistedNumbers.size());

                                //All add existing subscribers..
                                blacklistedNumbers.putAll(existingSubscribersBase);
                                org.esme.broker.AppBroker.sevasLogger.info("Total MSISDN in blacklistmap after adding active sub filtered... " + blacklistedNumbers.size());

                                //Clear uniqueCampaignTarget first... then reload wiht clean DB
                                org.esme.broker.AppBroker.sevasLogger.info("Clearing campaign DB list....  ");
                                campaignTarget.clear();

                                //Now iterate through the uniqueCampaignTargetMap to see if any of its value exist in blacklisted DB
                                org.esme.broker.AppBroker.sevasLogger.info("Filtering campaign target against blacklisted...Starting at. " + new Date());
                                for (Entry<Integer, String> entry : uniqueCampaignTargetMap.entrySet()) {
                                    // Check if the current value is a key in the 2nd map
                                    if (!blacklistedNumbers.containsKey(entry.getValue())) {
                                        campaignTarget.add(entry.getValue());
                                        //org.esme.broker.AppBroker.sevasLogger.info(entry.getValue() + " added");
                                    }
                                }

                                //Get total number after filter..
                                totalUniqueNumber = campaignTarget.size();
                                org.esme.broker.AppBroker.sevasLogger.info("Campaign Target After + Duplicates " + totalUniqueNumber);

                                org.esme.broker.AppBroker.sevasLogger.info("Filtering campaign target against blacklisted...Finished at. " + new Date());

                                org.esme.broker.AppBroker.sevasLogger.info("Total MSISDN after blacklist filtered... " + campaignTarget.size());

                                //REALOD the uploaded file with filtered numbers....
                                writer = new FileWriter(uploadFile);
                                writer.write("");
                                writer.flush();

                                for (String filteredNumber : campaignTarget) {
                                    writer.write(filteredNumber + "\n");
                                }
                                writer.flush();

                            } catch (Exception E) {
                                E.printStackTrace();
                            }
                            //Gte file uploaded file size after filter
                            fileSizeAfterFilterInMB = uploadedFileSize / (float) (1024 * 1024);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            //Get service name
            serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            for (Service nextService : serviceCache) {
                if (nextService.getServiceID() == serviceID) {
                    serviceName = nextService.getServiceName();
                    serviceType = nextService.getServiceSubType();
                    serviceNetwork = nextService.getServiceNetwork();
                    break;
                }
            }

            //Get user ID
            userCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
            for (Users nextUser : userCache) {
                if (nextUser.getUserName().equalsIgnoreCase(user)) {
                    userID = nextUser.getId();
                    break;
                }
            }

            //if not schedule...
            if (schedule.equalsIgnoreCase("No")) {
                //Update the campaign table before start sending braodcast
                try {

                    PreparedStatement ps = getDBConnection().prepareStatement(
                            "insert into sms_campaign"
                                    + "(campaign_sender_id, campaign_content, "
                                    + "campaign_target_file_dir,"
                                    + "campaign_sent_date,campaign_sent_time,"
                                    + "campaign_owner,campaign_status,campaign_service_id,"
                                    + "campaign_id)"
                                    + "values(?,?,?,?,?,?,?,?,?)");
                    ps.setString(1, senderID);
                    ps.setString(2, message);
                    ps.setString(3, uploadFile.getAbsolutePath());
                    ps.setString(4, formatDateToString(new Date()));
                    ps.setLong(5, getCurrentHour());
                    ps.setLong(6, userID);
                    ps.setString(7, "Processing");
                    ps.setLong(8, serviceID);
                    ps.setLong(9, campaignID);
                    int update = ps.executeUpdate();
                    if (update > 0) {

                        campaign = new SMSCampaign();
                        campaign.setCampaignID(campaignID);
                        campaign.setCampaignTargetFileDir(uploadFile.getAbsolutePath());
                        campaign.setCampaignSenderID(senderID);
                        campaign.setCampaignContent(message);
                        campaign.setCampaignSentDate(formatDateToString(new Date()));
                        campaign.setCampaignSentTime(getCurrentHour());
                        campaign.setCampaignOnwerID(userID);
                        campaign.setCampaignActualStartTime(formatDateToString(new Date()));
                        campaign.setCampaignStatus("Processed");
                        campaign.setCampaignServiceID(serviceID);

                        //Send campaign for processing...
                        MessageBroadCaster campaignBroadCaster = new MessageBroadCaster(
                                campaign, serviceNetwork, getDBConnection());
                        campaignBroadCaster.doCampaign();

                        request.setAttribute("message",
                                "Great! Campaign successfully proccessed. "
                                        + "\nCAMPAIGN DETAILS:\n "
                                        + "Upload File Size [MB]: " + uploadedFileSizeInMB + "\n"
                                        + "Total Uploaded  Number: " + totalNumerbUploaded + "\n"
                                        + "Total Duplicate Number: " + totalDuplicateNumber + "\n"
                                        + "Total Existing  Number: " + existingSubscribers + "\n"
                                        + "Total Redundant Number: " + redundantNumber + "\n"
                                        + "Total Unique Num After Filter: " + totalUniqueNumber + "\n"
                                        + "Campaign File Size After Filter [MB]: " + fileSizeAfterFilterInMB + "\n");
                        request.getRequestDispatcher("sms_campaign.jsp").forward(request, response);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    request.setAttribute("message", "Sorry, the system could not process your campaign at the moment. Try again later");
                    request.getRequestDispatcher("sms_campaign.jsp").forward(request, response);
                }

            } else {
                //Check if present or future date is selected...
                if (substractDate(date) < 0) {
                    request.setAttribute("message", "Please select present or future date.");
                    request.getRequestDispatcher("sms_campaign.jsp").forward(request, response);
                    return;
                }
                //Check if present date and  future hour of the day is selected..
                if (substractDate(date) == 0 && getCurrentHour() >= time) {
                    request.setAttribute("message", "Please select future hour of the day.");
                    request.getRequestDispatcher("sms_campaign.jsp").forward(request, response);
                    return;
                }
                sheduleCampaign(senderID, formatText(message), uploadFile.getAbsolutePath(),
                        date, time, userID, "scheduled", serviceID);
                updateSMSCampaignCache();
                request.setAttribute("message",
                        "Great! Campaign successfully scheduled. "
                                + "\nCAMPAIGN DETAILS:\n "
                                + "Upload File Size [MB]: " + uploadedFileSizeInMB + "\n"
                                + "Total Uploaded  Number: " + totalNumerbUploaded + "\n"
                                + "Total Duplicate Number: " + totalDuplicateNumber + "\n"
                                + "Total Existing  Number: " + existingSubscribers + "\n"
                                + "Total Redundant Number: " + redundantNumber + "\n"
                                + "Total Unique Num After Filter: " + totalUniqueNumber + "\n"
                                + "Campaign File Size After Filter [MB]: " + fileSizeAfterFilterInMB + "\n");
                request.getRequestDispatcher("sms_campaign.jsp").forward(request, response);
            }
        } catch (Exception e) {
            try {
                pst.close();
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @SuppressWarnings("null")
    public int sheduleCampaign(
            String senderID, String message, String recipientFilePath,
            String dateToBeSent, long timeTobeSent, long scheduledBy,
            String status, long serviceID) {
        int result = 0;
        PreparedStatement ps = null;
        try {

            ps = getDBConnection().prepareStatement(
                    "insert into sms_campaign"
                            + "(campaign_sender_id, campaign_content, "
                            + "campaign_target_file_dir,"
                            + "campaign_sent_date,campaign_sent_time,"
                            + "campaign_owner,campaign_status,campaign_service_id,campaign_id)"
                            + "values(?,?,?,?,?,?,?,?,?)");

            ps.setString(1, senderID);
            ps.setString(2, message);
            ps.setString(3, recipientFilePath);
            ps.setString(4, dateToBeSent);
            ps.setLong(5, timeTobeSent);
            ps.setLong(6, scheduledBy);
            ps.setString(7, status);
            ps.setLong(8, serviceID);
            ps.setLong(9, campaignID);
            result = ps.executeUpdate();

        } catch (SQLException ex) {
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
            }

        }
        return result;
    }

    @SuppressWarnings({"null", "Convert2Diamond"})
    public void updateSMSCampaignCache() {
        org.esme.broker.AppBroker.sevasLogger.info("Updating Campaign...");
        campaignCache = new ArrayList<SMSCampaign>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(getServletContext().getInitParameter("smsCampaignReportQuery"));

            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                while (resultSet.next()) {
                    campaign = new SMSCampaign();
                    campaign.setCampaignID(resultSet.getLong("campaign_id"));
                    campaign.setCampaignUploadDate(resultSet.getString("campaign_upload_date"));
                    campaign.setCampaignSenderID(resultSet.getString("campaign_sender_id"));
                    campaign.setCampaignContent(resultSet.getString("campaign_content"));
                    campaign.setCampaignTargetFileDir(resultSet.getString("campaign_target_file_dir"));
                    campaign.setCampaignSentDate(resultSet.getString("campaign_sent_date"));
                    campaign.setCampaignSentTime(resultSet.getLong("campaign_sent_time"));
                    campaign.setCampaignOnwerID(resultSet.getLong("campaign_owner"));
                    campaign.setCampaignTotalTarget(resultSet.getLong("campaign_total_target_recipients"));
                    campaign.setCampaignTotalSent(resultSet.getLong("campaign_total_sent"));
                    campaign.setCampaignActualStartTime(resultSet.getString("campaign_actual_start_time"));
                    campaign.setCampaignActualEndTime(resultSet.getString("campaign_actual_end_time"));
                    campaign.setCampaignStatus(resultSet.getString("campaign_status"));
                    campaign.setCampaignServiceID(resultSet.getLong("campaign_service_id"));
                    campaignCache.add(campaign);
                }
                //Create cache of Campaign
                getServletContext().setAttribute("totalSMSCampaign", campaignCache.size());
                getServletContext().setAttribute("smsCampaignCache", campaignCache);
            }

        } catch (Exception ex) {
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }

    public int substractDate(String firstDate) {
        int remainingDate = 0;
        try {
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
            long d1 = formater.parse(firstDate).getTime();
            long d2 = formater.parse(formatDateToString(new Date())).getTime();
            remainingDate = Math.round(d1 - d2) / (1000 * 60 * 60 * 24);
        } catch (ParseException ex) {
        }
        return remainingDate;
    }

    public Map<String, Integer> getUniqueSubscriber(long serviceID) {
        Statement statement;
        ResultSet resultSet;
        Map<String, Integer> uniqueSubscriber = new HashMap<>();
        try {
            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery("select distinct(sub_msisdn) "
                    + "from subscription where sub_status = 'active' "
                    + "and sub_service_id = " + serviceID);
            while (resultSet.next()) {
                uniqueSubscriber.put(resultSet.getString("sub_msisdn"), 1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return uniqueSubscriber;
    }

}
