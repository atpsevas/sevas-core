
package org.esme.controllers;

import com.google.gson.Gson;
import org.esme.broker.AppBroker;
import org.esme.dao.models.DailySubscription;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SubscriptionReportChart extends AppBroker {

    private static final long serialVersionUID = 1L;

    PreparedStatement statement;
    ResultSet resultSet;
    List<String> dates;

    public SubscriptionReportChart() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {

        try {
            List<DailySubscription> dailySubscriptionActivities = getDailySubscription();

            Gson gson = new Gson();

            String jsonString = gson.toJson(dailySubscriptionActivities);

            response.setContentType("application/json");

            response.getWriter().write(jsonString);
            PrintWriter out = response.getWriter();
            out.println(jsonString);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    private List<DailySubscription> getDailySubscription() throws SQLException {

        @SuppressWarnings("Convert2Diamond")
        List<DailySubscription> dailySubscriptions = new ArrayList<DailySubscription>();
        DailySubscription newSub;
        statement = getDBConnection().prepareStatement(getServletContext().getInitParameter("dailySubReport"));
        resultSet = statement.executeQuery();

        // dates = new ArrayList<>();
        while (resultSet.next()) {
            newSub = new DailySubscription();
            newSub.setSubDate(resultSet.getString("theDate"));
            newSub.setNewSub(resultSet.getLong("newSub"));
            newSub.setNewActiveSub(resultSet.getLong("newActiveSub"));
            newSub.setNewInActiveSub(resultSet.getLong("newOptOut"));
            dailySubscriptions.add(newSub);
        }

        return dailySubscriptions;
    }

//    public String subDate(int theDate) {
//        String[] dayOfTheMonth =  {"0","1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
//        return dayOfTheMonth[theDate];
//    }
}
