package org.esme.controllers;

import org.esme.broker.AppBroker;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@SuppressWarnings("serial")
public class UpdadateServiceBilling extends AppBroker {

    PreparedStatement pstmt;

    long serviceID, billingRetryIntervals, billingTime,
            serviceReminderTime;

    String billingNetwork, billingType, billingPrice, servicePeriod,
            serviceRenewalMessage, billingShortCode,
            mainBillingnotifySMSSetting, serviceName;

    String fallbackShortCode1;
    String fallbackPrice1;
    long fallbackPeriod1;
    String serviceFallbackRenewalMessage1;
    String fallBack1billingnotifySMSSetting;

    String fallbackShortCode2;
    String fallbackPrice2;
    long fallbackPeriod2;
    String serviceFallbackRenewalMessage2;
    String fallBack2billingnotifySMSSetting;

    String fallbackShortCode3;
    String fallbackPrice3;
    long fallbackPeriod3;
    String serviceFallbackRenewalMessage3;
    String fallBack3billingnotifySMSSetting;

    String instantBillingRetry;

    String fallbackShortCode4;
    String fallbackPrice4;
    long fallbackPeriod4;
    String serviceFallbackRenewalMessage4;
    String fallBack4billingnotifySMSSetting;


    @SuppressWarnings({"CallToThreadDumpStack", "UseSpecificCatch", "CallToPrintStackTrace"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try {
            serviceID = Long.parseLong(removePoison(request.getParameter("id").trim()));
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Service ID cannot be empty.");
            request.getRequestDispatcher("manage_services_billing.jsp").forward(request, response);
            return;
        }

        try {
            serviceName = removePoison(request.getParameter("sn").trim());
        } catch (NullPointerException e) {

        }

        try {
            billingNetwork = removePoison(request.getParameter("bill_net").trim());
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Please select service network.");
            request.getRequestDispatcher("manage_services_billing.jsp?id=" + serviceID).forward(request, response);
            return;
        }

        try {
            billingType = removePoison(request.getParameter("sst").trim());
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Please select billing network.");
            request.getRequestDispatcher("manage_services_billing.jsp?id=" + serviceID).forward(request, response);
            return;
        }

        try {
            billingShortCode = removePoison(request.getParameter("wc_ssc"));
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Billing short code cannot be empty.");
            request.getRequestDispatcher("manage_services_billing.jsp?id=" + serviceID).forward(request, response);
            return;
        }

        try {
            billingTime = Long.parseLong(removePoison(request.getParameter("sbt")));
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Service billing retry intervals cannot be empty.");
            request.getRequestDispatcher("manage_services_billing.jsp?id=" + serviceID).forward(request, response);
            return;
        }

        try {
            billingPrice = removePoison(request.getParameter("price"));
        } catch (NullPointerException e) {
        }
        try {
            servicePeriod = removePoison(request.getParameter("sp"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            serviceRenewalMessage = removePoison(request.getParameter("rm").trim());
        } catch (NullPointerException e) {
        }

        try {
            mainBillingnotifySMSSetting = removePoison(request.getParameter("notify").trim());
        } catch (NullPointerException e) {
        }

        try {
            billingRetryIntervals = Long.parseLong(removePoison(request.getParameter("sbi")));
        } catch (NullPointerException e) {

        }

        try {
            instantBillingRetry = removePoison(request.getParameter("irb"));
        } catch (NullPointerException e) {
        }

        //Fallback 1.
        try {
            fallbackShortCode1 = removePoison(request.getParameter("fb_ssc"));
        } catch (NullPointerException e) {
        }

        try {
            fallbackPrice1 = removePoison(request.getParameter("fb_price"));
        } catch (NullPointerException e) {
        }

        try {
            fallbackPeriod1 = Long.parseLong(removePoison(request.getParameter("fsp")));
        } catch (NullPointerException e) {
        }

        try {
            serviceFallbackRenewalMessage1 = removePoison(request.getParameter("frm"));
        } catch (NullPointerException e) {
        }

        try {
            fallBack1billingnotifySMSSetting = removePoison(request.getParameter("fb_notify"));
        } catch (NullPointerException e) {
        }

        try {
            fallbackShortCode2 = removePoison(request.getParameter("fb_ssc2"));
        } catch (NullPointerException e) {
        }

        try {
            fallbackPrice2 = removePoison(request.getParameter("fb_price2"));
        } catch (NullPointerException e) {
        }

        try {
            fallbackPeriod2 = Long.parseLong(removePoison(request.getParameter("fsp2")));
        } catch (NullPointerException e) {
        }

        try {
            serviceFallbackRenewalMessage2 = removePoison(request.getParameter("frm2"));
        } catch (NullPointerException e) {
        }

        try {
            fallBack2billingnotifySMSSetting = removePoison(request.getParameter("fb2_notify"));
        } catch (NullPointerException e) {
        }

        try {
            fallbackShortCode3 = removePoison(request.getParameter("fb_ssc3"));
        } catch (NullPointerException e) {
        }

        try {
            fallbackPrice3 = removePoison(request.getParameter("fb_price3"));
        } catch (NullPointerException e) {
        }

        try {
            fallbackPeriod3 = Long.parseLong(removePoison(request.getParameter("fsp3")));
        } catch (NullPointerException e) {
        }

        try {
            serviceFallbackRenewalMessage3 = removePoison(request.getParameter("frm3"));
        } catch (NullPointerException e) {
        }

        try {
            fallBack3billingnotifySMSSetting = removePoison(request.getParameter("fb3_notify"));
        } catch (NullPointerException e) {
        }

        try {
            fallbackShortCode4 = removePoison(request.getParameter("fb_ssc4"));
        } catch (NullPointerException e) {
        }

        try {
            fallbackPrice4 = removePoison(request.getParameter("fb_price4"));
        } catch (NullPointerException e) {
        }

        try {
            fallbackPeriod4 = Long.parseLong(removePoison(request.getParameter("fsp4")));
        } catch (NullPointerException e) {
        }

        try {
            serviceFallbackRenewalMessage4 = removePoison(request.getParameter("frm4"));
        } catch (NullPointerException e) {
        }

        try {
            fallBack4billingnotifySMSSetting = removePoison(request.getParameter("fb4_notify"));
        } catch (NullPointerException e) {
        }

        try {

            pstmt = getDBConnection().prepareStatement(

                    "update subscription_service set "
                            + "billing_network = ?, service_sub_type = ?,"
                            + "welcome_shortcode=?, service_renewal_time_of_the_day =?,"
                            + "service_price=?, service_renewal_message=?,"
                            + "main_billing_notify_settings=?, instant_billing_retry=?,"
                            + "service_billing_retry_intervals=?,"
                            + "fallback_shortcode=?, fallback_price=?,"
                            + "service_fallback_period=?, service_fallback_renewal_message=?,"
                            + "fallback1_billing_notify_settings=?,"
                            + "fallback2_shortcode=?, fallback2_price=?,"
                            + "fallback2_period=?, fallback2_message=?,"
                            + "fallback2_billing_notify_settings=?,"
                            + "fallback3_shortcode=?,fallback3_price=?,"
                            + "fallback3_period=?,fallback3_message=?,"
                            + "fallback3_billing_notify_settings=?,"
                            + "fallback4_shortcode=?,fallback4_price=?,"
                            + "fallback4_period=?,fallback4_message=?,"
                            + "fallback4_billing_notify_settings=?,"
                            + "service_period=?"
                            + " where id = ?");

            pstmt.setString(1, billingNetwork);
            pstmt.setString(2, billingType);
            pstmt.setString(3, billingShortCode);
            pstmt.setLong(4, billingTime);
            pstmt.setLong(5, Long.parseLong(billingPrice));
            pstmt.setString(6, serviceRenewalMessage);
            pstmt.setString(7, mainBillingnotifySMSSetting);
            pstmt.setString(8, instantBillingRetry);
            pstmt.setLong(9, billingRetryIntervals);

            pstmt.setString(10, fallbackShortCode1);
            pstmt.setLong(11, Long.parseLong(fallbackPrice1));
            pstmt.setLong(12, fallbackPeriod1);
            pstmt.setString(13, serviceFallbackRenewalMessage1);
            pstmt.setString(14, fallBack1billingnotifySMSSetting);

            pstmt.setString(15, fallbackShortCode2);
            pstmt.setString(16, fallbackPrice2);
            pstmt.setLong(17, fallbackPeriod2);
            pstmt.setString(18, serviceFallbackRenewalMessage2);
            pstmt.setString(19, fallBack2billingnotifySMSSetting);

            pstmt.setString(20, fallbackShortCode3);
            pstmt.setString(21, fallbackPrice3);
            pstmt.setLong(22, fallbackPeriod3);
            pstmt.setString(23, serviceFallbackRenewalMessage3);
            pstmt.setString(24, fallBack3billingnotifySMSSetting);

            pstmt.setString(25, fallbackShortCode4);
            pstmt.setString(26, fallbackPrice4);
            pstmt.setLong(27, fallbackPeriod4);
            pstmt.setString(28, serviceFallbackRenewalMessage4);
            pstmt.setString(29, fallBack4billingnotifySMSSetting);

            pstmt.setLong(30, Long.parseLong(servicePeriod));

            pstmt.setLong(31, serviceID);
            int update = pstmt.executeUpdate();
            if (update > 0) {
                //Update Service Archive...
                updateServicesCache();
                request.setAttribute(
                        "message",
                        serviceName + " billing successfully updated.");
                request.getRequestDispatcher("manage_services_billing.jsp?id=" + serviceID).forward(request, response);
            }

        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute(
                    "message",
                    "Sorry, service cannot be updated at the momment. "
                            + "Please try again later or contact the administrator");
            request.getRequestDispatcher("manage_services_billing.jsp?id=" + serviceID).forward(request, response);
            e.printStackTrace();
        } finally {
            try {
                pstmt.close();
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
