package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.ShortCode;
import org.esme.utils.pingenerator.RandomPinGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CreateNewShortCode extends AppBroker {

    PreparedStatement pstmt;
    ArrayList<ShortCode> shortCodeCache;
    ShortCode shortCode;
    private String shortCodeID;

    @SuppressWarnings("CallToThreadDumpStack")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try {
            shortCodeID = removePoison(request.getParameter("shortcode"));
        } catch (NullPointerException e) {
            request.setAttribute("message",
                    "*Short Code is required parameters.");
            request.getRequestDispatcher("new_shortcode.jsp").forward(request, response);
            return;
        }

        //Check if the short code already exist
        shortCodeCache = (ArrayList<ShortCode>) getServletContext().getAttribute("shortCodesCache");
        for (ShortCode sc : shortCodeCache) {
            if (sc.getShortCodeID().equalsIgnoreCase(shortCodeID)) {
                request.setAttribute(
                        "message", shortCodeID + " has already been created");
                request.getRequestDispatcher("new_shortcode.jsp").forward(request, response);
                return;
            }
        }

        try {

            long shortCodeCustomID = RandomPinGenerator.randomPinGenerator(4);

            pstmt = getDBConnection().prepareStatement(
                    "insert into service_shortcode(id, shortcode_id) "
                            + "values(?,?)");
            pstmt.setLong(1, shortCodeCustomID);
            pstmt.setString(2, shortCodeID);
            int update = pstmt.executeUpdate();

            if (update > 0) {
                updateShortCodesCache();
                request.setAttribute(
                        "message",
                        shortCodeID + " successfully created.");
                request.getRequestDispatcher("new_shortcode.jsp").forward(request, response);
            } else {
                request.setAttribute(
                        "message",
                        "Sorry, an error occured while creating " + shortCodeID + ". Please try again.");
                request.getRequestDispatcher("new_shortcode.jsp").forward(request, response);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            request.setAttribute(
                    "message",
                    "*Sorry, an error occured while creating your short code. Try again.");
            request.getRequestDispatcher("new_shortcode.jsp").forward(request, response);
        } finally {
            try {
                pstmt.close();
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void updateShortCodesCache() {
        org.esme.broker.AppBroker.sevasLogger.info("Refreshing Short Codes Cache.....");
        ShortCode shortCodes;
        ArrayList<ShortCode> shortCodesCache = new ArrayList<ShortCode>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(getServletContext().getInitParameter("shortCodeQuery"));
            //We place a lock on the  cache untill it finishes updating....
            synchronized (this) {
                while (resultSet.next()) {
                    shortCodes = new ShortCode();
                    shortCodes.setID(resultSet.getLong("id"));
                    shortCodes.setShortCodeID(resultSet.getString("shortcode_id"));
                    shortCodes.setDateCreated(resultSet.getString("shortcode_datecreated"));
                    shortCodesCache.add(shortCodes);
                }
                //Create cache of services with all
                getServletContext().setAttribute("shortCodesCache", shortCodesCache);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }
}
