/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers;

import com.google.gson.Gson;
import org.esme.broker.AppBroker;
import org.esme.dao.models.MonthlyRevPerformance;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiceRevenueChart extends AppBroker {

    private static final long serialVersionUID = 1L;

    PreparedStatement statement;
    ResultSet resultSet;

    public ServiceRevenueChart() {
        super();
    }

    public static String theMonth(int month) {
        String[] monthNames = {"", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        return monthNames[month];
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {

        try {
            List<MonthlyRevPerformance> listOfStudent = getServiceRevenue();

            Gson gson = new Gson();

            String jsonString = gson.toJson(listOfStudent);

            response.setContentType("application/json");

            response.getWriter().write(jsonString);

            PrintWriter out = response.getWriter();
            out.println(jsonString);

        } catch (SQLException ex) {
            Logger.getLogger(ServiceRevenueChart.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private List<MonthlyRevPerformance> getServiceRevenue() throws SQLException {

        @SuppressWarnings("Convert2Diamond")
        List<MonthlyRevPerformance> services = new ArrayList<MonthlyRevPerformance>();
        MonthlyRevPerformance mothlyRev;
        statement = getDBConnection().prepareStatement(getServletContext().getInitParameter("monthlyRevenue"));
        resultSet = statement.executeQuery();

        while (resultSet.next()) {

            mothlyRev = new MonthlyRevPerformance();
            mothlyRev.setTheMonth(theMonth(resultSet.getInt("theMonth") + 1));
            mothlyRev.setMainCharged(resultSet.getLong("main_charge"));
            mothlyRev.setFallBackCharged(resultSet.getLong("fallback_charge"));
            mothlyRev.setTotalRevenue(resultSet.getLong("main_charge") + resultSet.getLong("fallback_charge") + resultSet.getLong("daily_paid_content"));
            services.add(mothlyRev);
        }

        return services;
    }
}
