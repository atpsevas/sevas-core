package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dto.Service;
import org.esme.gateway.kannel.WEB2SMS;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

@SuppressWarnings("serial")
public class Help extends AppBroker {

    String serviceShortCode, subMSISDN,
            subRequestText, subNetwork,
            subResponseText;
    ServletContext ctx;
    ArrayList<Service> serviceCache;
    Properties theProp;


    WEB2SMS web2SMS = WEB2SMS.getInstance();


    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "unchecked", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        org.esme.broker.AppBroker.sevasLogger.error("HELP CALLED");
        try {
            //Collect the parameters coming from the SMS Gateway
            serviceShortCode = removePoison(request.getParameter("sc").replace("+", ""));
            subMSISDN = request.getParameter("msisdn");
            subRequestText = removePoison(request.getParameter("req_text"));
            subNetwork = request.getParameter("smscID");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        @SuppressWarnings("Convert2Diamond")
        ArrayList<String> UserHelpMessages = new ArrayList<String>();

        theProp = AppBroker.getPropertyFileHandle();

        serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
        String serviceHelp = "";

        //Check services archive availabbility
        if (serviceCache.isEmpty()) {
            updateServicesCache();
            serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
        }


        //For those who have inititally sent HELP
        if (subRequestText.equalsIgnoreCase("more")) {

            UserHelpMessages = (ArrayList) getServletContext().getAttribute(subMSISDN.substring(subMSISDN.length() - 9));

            if (UserHelpMessages == null || UserHelpMessages.isEmpty()) {

                web2SMS.SMSBOX_NODLR(serviceShortCode,
                        subMSISDN, "Please text HELP for help.", subNetwork);

            } else {

                UserHelpMessages = (ArrayList) getServletContext().
                        getAttribute(subMSISDN.substring(subMSISDN.length() - 9));
                String nextMessage = UserHelpMessages.get(0);

                web2SMS.SMSBOX_NODLR(serviceShortCode,
                        subMSISDN, nextMessage, subNetwork);

                //Remove the element we just sent..
                UserHelpMessages.remove(0);
                //reload the archive...
                getServletContext().setAttribute(subMSISDN.substring(subMSISDN.length() - 9), UserHelpMessages);

            }
            return;
        }

        try {
            for (Service service : serviceCache) {

                org.esme.broker.AppBroker.sevasLogger.info("All " + service.getServiceName());
                if (
                        (serviceShortCode.equalsIgnoreCase(service.getServiceShortCode())
                                || serviceShortCode.equalsIgnoreCase(service.getServiceBillingShortCode())
                                || serviceShortCode.equalsIgnoreCase(service.getServiceMTShortCode())
                        )
                                && (subNetwork.equalsIgnoreCase(service.getServiceNetwork())
                                || subNetwork.equalsIgnoreCase(service.getServiceBillingNetwork())
                        )
                        ) {

                    //serviceHelp = serviceHelp + service.getServiceHelpMessage();
                    UserHelpMessages.add(service.getServiceHelpMessage());
                    //org.esme.broker.AppBroker.sevasLogger.info(service.getServiceHelpMessage());

                }
            }

//}
            if (subNetwork.toLowerCase().contains("etisalat")) {
                subNetwork = theProp.getProperty("etiContentBind");
            }

            //Save all the messages in memory DB....
            getServletContext().setAttribute(subMSISDN.substring(subMSISDN.length() - 9), UserHelpMessages);
            String nextMessage = UserHelpMessages.get(0);

            web2SMS.SMSBOX_NODLR(serviceShortCode,
                    subMSISDN, nextMessage, subNetwork);

            //Remove the element we just sent..
            UserHelpMessages.remove(0);
            //reload the archive...
            getServletContext().setAttribute(subMSISDN.substring(subMSISDN.length() - 9), UserHelpMessages);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public String getSubscribedToServicesHelp(String subscriber, String keyword) {
        String response = "";
        try {
            Statement st = getDBConnection().createStatement();
            ResultSet rs = st.executeQuery(
                    "select a.service_help_message from "
                            + "subscription_service a, "
                            + "subscription b where b.sub_msisdn "
                            + "ilike '%" + subscriber.substring(subscriber.length() - 9) + "%' "
                            + "and a.id = b.sub_service_id");
            while (rs.next()) {
                response = response + "" + rs.getString("service_help_message");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return response;

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
