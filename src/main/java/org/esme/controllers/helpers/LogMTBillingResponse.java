package org.esme.controllers.helpers;

import org.esme.broker.AppBroker;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class LogMTBillingResponse {

    Properties prop;
    private String delivertStatus;
    private String messageID;
    private String senderID;
    private String recipientMSISDN;
    private String smsc;
    private String message;
    private String servName;
    private String srcModule;
    private long serviceID;
    private String subscriptionBillingType;

    @SuppressWarnings("CallToPrintStackTrace")
    public LogMTBillingResponse() {
        prop = AppBroker.getPropertyFileHandle();
    }

    static java.sql.Timestamp getCurrentTimeStamp() {

        java.util.Date today = new java.util.Date();
        return new java.sql.Timestamp(today.getTime());

    }

    public String getDelivertStatus() {
        return delivertStatus;
    }

    public void setDelivertStatus(String delivertStatus) {
        this.delivertStatus = delivertStatus;
    }

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getRecipientMSISDN() {
        return recipientMSISDN;
    }

    public void setRecipientMSISDN(String recipientMSISDN) {
        this.recipientMSISDN = recipientMSISDN;
    }

    public String getSmsc() {
        return smsc;
    }

    public void setSmsc(String smsc) {
        this.smsc = smsc;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getServName() {
        return servName;
    }

    public void setServName(String servName) {
        this.servName = servName;
    }

    public String getSrcModule() {
        return srcModule;
    }

    public void setSrcModule(String srcModule) {
        this.srcModule = srcModule;
    }

    public long getServiceID() {
        return serviceID;
    }

    public void setServiceID(long serviceID) {
        this.serviceID = serviceID;
    }

    public String getSubscriptionBillingType() {
        return subscriptionBillingType;
    }

    public void setSubscriptionBillingType(String subscriptionBillingType) {
        this.subscriptionBillingType = subscriptionBillingType;
    }

    @SuppressWarnings("ConvertToTryWithResources")
    public long billing_outgoing_messages(LogMTBillingResponse mtResponse) {
        int update = 0;
        PrintWriter pw = null;

        try {

            synchronized (this) {

                File dataFile = new File("/home/sevas/billing_outgoing.sql");

                if (!dataFile.exists()) {
                    dataFile.createNewFile();
                }

                pw = new PrintWriter(new FileWriter(dataFile, true), true);
                pw.write("('" + mtResponse.getMessageID() + "',"
                        + "'" + mtResponse.getSenderID() + "',"
                        + "'" + mtResponse.getRecipientMSISDN() + "',"
                        + "'" + mtResponse.getSmsc() + "',"
                        + "'" + mtResponse.getMessage().replaceAll("'", "''") + "',"
                        + "'" + mtResponse.getDelivertStatus() + "',"
                        + "'" + mtResponse.getServName() + "',"
                        + "'" + mtResponse.getSrcModule() + "',"
                        + "'" + mtResponse.getSmsc() + "',"
                        + "" + mtResponse.getServiceID() + ","
                        + "'" + mtResponse.getSubscriptionBillingType() + "',"
                        + "'" + getCurrentTimeStamp() + "'"
                        + "),");

                pw.flush();
            }
        } catch (IOException ex1) {
            ex1.printStackTrace();
        } finally {
            pw.close();
        }
        return update;
    }

    @SuppressWarnings("ConvertToTryWithResources")
    public long content_outgoing_messages(LogMTBillingResponse mtResponse) {
        int update = 0;
        PrintWriter pw = null;

        try {

            synchronized (this) {

                File dataFile = new File("/home/sevas/content_outgoing.sql");
                if (!dataFile.exists()) {
                    dataFile.createNewFile();

                }

                pw = new PrintWriter(new FileWriter(dataFile, true), true);
                pw.write("('" + mtResponse.getMessageID() + "',"
                        + "'" + mtResponse.getSenderID() + "',"
                        + "'" + mtResponse.getRecipientMSISDN() + "',"
                        + "'" + mtResponse.getSmsc() + "',"
                        + "'" + mtResponse.getMessage().replaceAll("'", "''") + "',"
                        + "'" + mtResponse.getDelivertStatus() + "',"
                        + "'" + mtResponse.getServName() + "',"
                        + "'" + mtResponse.getSrcModule() + "',"
                        + "'" + mtResponse.getSmsc() + "',"
                        + "" + mtResponse.getServiceID() + ","
                        + "'" + mtResponse.getSubscriptionBillingType() + "',"
                        + "'" + getCurrentTimeStamp() + "'"
                        + "),");

                pw.flush();
            }
        } catch (IOException ex1) {
            ex1.printStackTrace();
        } finally {
            pw.close();
        }
        return update;
    }

    public String formatDateToString(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }
}
