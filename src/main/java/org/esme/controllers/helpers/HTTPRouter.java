/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers.helpers;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.http.HttpStatus;
import org.esme.dto.Service;

/**
 * @author saheedalbert
 */
public class HTTPRouter {

    HttpClient client = null;
    byte[] responseBody;
    private String routingURL;
    private String routingShortCode;
    private String routingMSISDN;
    private String routingText;

    public String getRoutingURL() {
        return routingURL;
    }

    public void setRoutingURL(String routingURL) {
        this.routingURL = routingURL;
    }

    public String getRoutingShortCode() {
        return routingShortCode;
    }

    public void setRoutingShortCode(String routingShortCode) {
        this.routingShortCode = routingShortCode;
    }

    public String getRoutingMSISDN() {
        return routingMSISDN;
    }

    public void setRoutingMSISDN(String routingMSISDN) {
        this.routingMSISDN = routingMSISDN;
    }

    public String getRoutingText() {
        return routingText;
    }

    public void setRoutingText(String routingText) {
        this.routingText = routingText;
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public String httpRoute(Service theService, HTTPRouter router) throws URIException {

        String theRes = "";

        String theURL = theService.getExternalAPIURL();
        String myParam = URIUtil.encodeQuery(
                theService.getExternalAPIShortCodeParam() + "=" + router.getRoutingShortCode()
                        + "&" + theService.getExternalAPIMSISDNParam() + "=" + router.getRoutingMSISDN()
                        + "&" + theService.getExternalAPIMessageParam() + "=" + router.getRoutingText());

        org.esme.broker.AppBroker.sevasLogger.error("THE URL " + theURL);
        org.esme.broker.AppBroker.sevasLogger.error("THE PARAM " + myParam);
        org.esme.broker.AppBroker.sevasLogger.error("THE TWO " + theURL + myParam);

        String url = theURL + myParam;

        org.esme.broker.AppBroker.sevasLogger.error(url);

        try {

            client = new HttpClient();
            GetMethod tmethod = new GetMethod(url);

            // Execute the method.
            int statusCode = client.executeMethod(tmethod);
            if (statusCode != HttpStatus.SC_OK) {
                org.esme.broker.AppBroker.sevasLogger.error("HTTP STATUS: " + tmethod.getStatusLine());
            }
            responseBody = tmethod.getResponseBody();
            // Use caution: ensure correct character encoding and is not binary data
            theRes = new String(responseBody);
            org.esme.broker.AppBroker.sevasLogger.info(theRes);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return theRes;
    }
}
