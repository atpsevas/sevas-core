package org.esme.controllers.helpers;

import org.esme.broker.AppBroker;
import org.esme.dto.Service;
import org.esme.gateway.kannel.WEB2SMS;
import org.esme.webservices.airtel.AirtelUCIP;
import org.esme.webservices.glo.GloPSA;

import java.sql.*;
import java.util.Properties;

public final class BillingBroadCaster extends HelperBroker {

    //static Connection connection;
    static BillingBroadCaster billingBroadCaster;
    static Properties prop;
    //Glo PSA
    private static GloPSA gloXMLSOAPFilterWebservice;
    private static String DLRURL;
    private static String DLR_MASK;
    private static Connection connection;
    long messageCounter;
    //Direct Billing
//    EtisalatDirectBilling etisalatDirectBilling;
//    MaspPaymentResponseData result = null;
//    MaspPaymentResponseData chargingResponse = null;
    String vendorUser;
    String vendorPassword;
    String vendorCode;
    String transactionID;
    AirtelUCIP airtelIBMUCIP;
    WEB2SMS web2SMS;

    //Instance of BillingBroadCaster  can only be created by call this method..
    @SuppressWarnings({"CallToPrintStackTrace", "SillyAssignment", "UnusedAssignment"})
    public static synchronized BillingBroadCaster getInstance(Connection conn) {

        try {
            if (billingBroadCaster == null) {
                billingBroadCaster = new BillingBroadCaster();
                // webSMS = WEB2SMS.getInstance();
                gloXMLSOAPFilterWebservice = GloPSA.getInstance();
                connection = conn;
            }
        } catch (Exception ex) {
        }

        prop = AppBroker.getPropertyFileHandle();

        return billingBroadCaster;
    }

    @SuppressWarnings({"CallToPrintStackTrace", "SleepWhileInLoop"})
    public void mtBilling(Service service, String mtBillingMethod, String billingQuery) {

        Connection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;
        ResultSet resultSet;
        String params = null;
        long pid = 64;

        web2SMS = WEB2SMS.getInstance();

        try {

            dbConnection = connection;

            DLRURL = prop.getProperty("dlr_url");
            DLR_MASK = prop.getProperty("dlrMask");
            String subMSISDN = null;

            String billingShortCode;
            String billingMessage;
            String billingModule = null;

            //String billingQueryHeader = "";
            long epochTime = (System.currentTimeMillis() / 1000);

            statement = dbConnection.createStatement();

            org.esme.broker.AppBroker.sevasLogger.info(billingQuery);

            resultSet = statement.executeQuery(billingQuery + " group by a.sub_msisdn");

            org.esme.broker.AppBroker.sevasLogger.info(billingQuery + " group by a.sub_msisdn");

            //Temporary billing options...main or fall back...
            billingShortCode = service.getServiceBillingShortCode();
            billingMessage = service.getServiceRenewalMessage();
            billingModule = "SubscriptionRenewal";

            //We want to try fall back only during the day...Fro freesia 1.
            try {
                if (prop.getProperty("tryFallbackOnly").equalsIgnoreCase("yes")) {
                    billingShortCode = service.getFallbackShortCode();
                    billingMessage = service.getServiceFallbackRenewalMessage();
                    billingModule = "SubscriptionRenewalRetry";
                }
            } catch (Exception E) {
                E.printStackTrace();
            }

            int MT = 2;
            PreparedStatement thePS;

            while (resultSet.next()) {

                try {

                    subMSISDN = resultSet.getString("sub_msisdn");

                    if (service.getMainbillingnotifySMSSetting().equalsIgnoreCase("yes")
                            | subMSISDN.startsWith("234809944")) {
                        pid = 127;

                        web2SMS.SMSBOX(service.getServiceSubType(), billingShortCode,
                                subMSISDN,
                                formatText(billingMessage.replace("'", "\'")),
                                service.getServiceName(), service.getServiceID(),
                                billingModule,
                                service.getServiceBillingNetwork(), DLR_MASK);

                    } else {

                        web2SMS.SILENCE_SMSBOX(service.getServiceSubType(), billingShortCode,
                                subMSISDN,
                                formatText(billingMessage.replace("'", "\'")),
                                service.getServiceName(), service.getServiceID(),
                                billingModule,
                                service.getServiceBillingNetwork(), DLR_MASK);
                    }
                } catch (SQLException | NumberFormatException ex1) {
                    ex1.printStackTrace();
                }

            }
        } catch (Exception E) {

        }
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public void retryFailedMTDailyPaidContent() {
        Connection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;
        String params = null;
        long pid = 64;

        try {

            dbConnection = connection;

            DLRURL = prop.getProperty("dlr_url");
            DLR_MASK = prop.getProperty("dlrMask");

            statement = dbConnection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "select network,sender,recipient,message,serv_name, "
                            + "src_module,serv_id, service_sub_type "
                            + "from outgoing_messages  where date(sent_time) = date(now()) "
                            + "and delivery_status ilike '%faile%' "
                            + "and src_module ilike '%dailypaidcontent%' ");

            String billingQueryHeader = "";
            long epochTime = System.currentTimeMillis() / 1000;
            pid = 64;

            billingQueryHeader
                    = "INSERT INTO send_sms (momt, sender, receiver, "
                    + "msgdata, smsc_id, sms_type, boxc_id,"
                    + "time,pid,service,dlr_mask,dlr_url) "
                    + "VALUES  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            ps = dbConnection.prepareStatement(billingQueryHeader);

            while (resultSet.next()) {

                if (resultSet.getString("recipient").startsWith("234809944")) {
                    pid = 127;
                }

                params = encodeParams(resultSet.getString("network"))
                        + "&dp_retry=yes"
                        + "&sub_type=" + encodeParams(resultSet.getString("service_sub_type"))
                        + "&msg_id=" + encodeParams(generateMessageID())
                        + "&sc=" + encodeParams(resultSet.getString("sender"))
                        + "&msisdn=" + encodeParams(resultSet.getString("recipient"))
                        + "&msg=" + encodeParams(resultSet.getString("message"))
                        + "&serv_id=" + resultSet.getString("serv_id")
                        + "&serv_name=" + encodeParams(resultSet.getString("serv_name"))
                        + "&src_module=" + encodeParams(resultSet.getString("src_module"))
                        + "&dlr_mask=" + encodeParams(DLR_MASK) + "&dlr=%d";

                if (resultSet.getString("recipient").startsWith("234809944")) {
                    pid = 127;
                }

                ps.setString(1, resultSet.getString("service_sub_type"));
                ps.setString(2, resultSet.getString("sender"));
                ps.setString(3, resultSet.getString("recipient"));
                ps.setString(4, formatText(resultSet.getString("message")));
                ps.setString(5, resultSet.getString("network"));
                ps.setLong(6, 2);
                ps.setString(7, prop.getProperty("billingSMSBoxID"));
                ps.setLong(8, epochTime);
                ps.setLong(9, pid);
                ps.setString(10, "'" + resultSet.getString("serv_id") + "'");
                ps.setLong(11, Long.parseLong(DLR_MASK));
                ps.setString(12, DLRURL + params);
                ps.executeUpdate();

            }

        } catch (SQLException | NumberFormatException ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public void doGLOPSABilling(Service service, String srcModule) {
        PreparedStatement statement = null;
        ResultSet resultSet;
        String sModule = srcModule;
        Service theService = service;

        try {

            org.esme.broker.AppBroker.sevasLogger.info("NAME OF SERVCE " + theService.getServiceName() + " ID OF SERVICE " + service.getServiceID());

            statement = connection.prepareStatement(prop.getProperty("expiredSubQuery"));
            statement.setLong(1, theService.getServiceID());
            resultSet = statement.executeQuery();

            while (resultSet.next()) {

                gloXMLSOAPFilterWebservice.makeGloChargingRquest(
                        theService, Long.parseLong(theService.getServicePrice()), sModule, theService.getServiceBillingShortCode(),
                        resultSet.getString("sub_msisdn"), connection, "");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("CallToPrintStackTrace")
    public void doAirtelIBMUCIPBilling(Service service, String srcModule) {
        PreparedStatement statement = null;
        ResultSet resultSet;
        String sModule = srcModule;
        Service theService = service;

        airtelIBMUCIP = AirtelUCIP.getInstance();

        try {

            org.esme.broker.AppBroker.sevasLogger.info("NAME OF SERVCE " + theService.getServiceName() + " ID OF SERVICE " + service.getServiceID());

            statement = connection.prepareStatement(prop.getProperty("expiredSubQuery"));
            statement.setLong(1, theService.getServiceID());
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                airtelIBMUCIP.makeAirtelChargingRequest(service, srcModule,
                        service.getServiceBillingShortCode(), "",
                        service.getServiceName(), resultSet.getString("sub_msisdn"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("CallToPrintStackTrace")
    public void sendBillingNotification(Service service, String msisdn, String srcModule) {

        DLRURL = prop.getProperty("dlr_url");
        DLR_MASK = prop.getProperty("dlrMask");

        String message;

        if (srcModule.equalsIgnoreCase("SubscriptionRenewal") || srcModule.equalsIgnoreCase("NewSubscription")) {
            message = service.getServiceRenewalMessage();
        } else {
            message = service.getServiceFallbackRenewalMessage();
        }

        try {

            long epochTime = System.currentTimeMillis() / 1000;
            long pid = 64;

            if (service.getMainbillingnotifySMSSetting().equalsIgnoreCase("yes") | msisdn.startsWith("234809944")) {
                pid = 127;
            }

            PreparedStatement ps = connection.prepareStatement(
                    "INSERT INTO send_sms (momt, sender, receiver, "
                            + "msgdata, smsc_id, sms_type, boxc_id,"
                            + "dlr_mask,dlr_url,time,pid) "
                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?)");

            org.esme.broker.AppBroker.sevasLogger.error("Seding..." + message + " to " + msisdn);
            String params = encodeParams(service.getServiceBillingNetwork())
                    + "&sub_type=" + encodeParams(service.getServiceSubType())
                    + "&msg_id=" + encodeParams(generateMessageID())
                    + "&sc=" + encodeParams(service.getServiceShortCode())
                    + "&msisdn=" + encodeParams(msisdn)
                    + "&msg=" + encodeParams(formatText(message))
                    + "&serv_id=" + service.getServiceID()
                    + "&serv_name=" + encodeParams(service.getServiceName())
                    + "&src_module=" + encodeParams(srcModule)
                    + "&dlr_mask=" + encodeParams(DLR_MASK) + "&dlr=%d";

            ps.setString(1, service.getServiceSubType());
            ps.setString(2, service.getServiceShortCode());
            ps.setString(3, msisdn);
            ps.setString(4, formatText(message));
            ps.setString(5, service.getServiceNetwork());
            ps.setLong(6, 2);
            ps.setString(7, prop.getProperty("billingSMSBoxID"));
            ps.setLong(8, Long.parseLong(DLR_MASK));
            ps.setString(9, DLRURL + params);
            ps.setLong(10, epochTime + 3600);
            ps.setLong(11, pid);
            ps.executeUpdate();

            org.esme.broker.AppBroker.sevasLogger.info(service.getServiceShortCode() + " "
                    + msisdn + " " + formatText(message) + " "
                    + service.getServiceNetwork());

        } catch (SQLException | NumberFormatException e) {
            e.printStackTrace();
        }
    }
}
