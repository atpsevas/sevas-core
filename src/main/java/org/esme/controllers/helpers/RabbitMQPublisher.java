package org.esme.controllers.helpers;

import com.rabbitmq.client.*;
import org.apache.log4j.Logger;
import org.esme.broker.AppBroker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

public class RabbitMQPublisher {

    private static final Logger logger = Logger.getLogger(RabbitMQPublisher.class);
    static ConnectionFactory factory;
    static Connection connection;
    static Channel channel;
    static int rabbitPort;
    static RabbitMQPublisher publisher;
    static Properties prop = AppBroker.getPropertyFileHandle();
    String DEFAULT_QUEUE_NAME = prop.getProperty("rabbitQueueName");

    //Instance of RabbitMQPublisher  can only be created by call this method...
    @SuppressWarnings("CallToPrintStackTrace")
    public static synchronized RabbitMQPublisher getInstance() throws IOException, TimeoutException {

        if (publisher == null) {

            //Logger
            //Create an instance of RabbitMQPublisher....
            publisher = new RabbitMQPublisher();


            String rabbitIP = prop.getProperty("rabbitIP");
            rabbitPort = Integer.parseInt(prop.getProperty("rabbitPort"));

            //We can use RabbitMQ or Lyra. We chose Lyra for performance sake. 
            factory = new ConnectionFactory();
            factory.setHost(rabbitIP);
            factory.setPort(rabbitPort);
            factory.setRequestedHeartbeat(1);
            factory.setConnectionTimeout(5000);
            factory.setAutomaticRecoveryEnabled(true);
            factory.setTopologyRecoveryEnabled(true);
            factory.setNetworkRecoveryInterval(580);

            connection = factory.newConnection();

            connection.addShutdownListener(new ShutdownListener() {
                public void shutdownCompleted(ShutdownSignalException cause) {
                    if (cause.isHardError()) {
                        Connection conn = (Connection) cause.getReference();
                        if (!cause.isInitiatedByApplication()) {
                            Method reason = cause.getReason();
                            logger.error(reason);
                        }

                    } else {
                        Channel ch = (Channel) cause.getReference();

                    }
                }
            });

            // Create Channel for Transaction
            channel = connection.createChannel();

        }
        return publisher;
    }

    public static void closeRabbitConnection() {

        try {
            channel.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        try {
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void publishDLRToRabbit(String message, String queue_name) throws Exception {

        // Use default queue name
        if (queue_name.isEmpty()) queue_name = DEFAULT_QUEUE_NAME;

        boolean durable = true;
        Map<String, Object> args = new HashMap<>();

        try {


            // args.put("x-dead-letter-exchange", "amq.direct");
            // channel.basicQos(0, 0, true);

            channel.queueDeclare(queue_name, durable, false, false, args);

            logger.debug(" [x] Queueing.. '" + message + "'");
            channel.basicPublish("", queue_name, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes("UTF-8"));
            logger.debug(" [x] Queued '" + message + "'");

            // Close Channel
//            channel.close();

        } catch (ShutdownSignalException sse) {
            // possibly check if channel was closed
            // by the time we started action and reasons for
            // closing it

            logger.error("Channel Closed Abruptly");
        } catch (IOException e) {
            // e.printStackTrace();

            // check why connection was closed
            logger.error("RabbitMQ Connection Closed Abruptly");

            // Reinitialize publisher for subsequent request
            // TODO: Never miss a message, Fix Compromise
            publisher = getInstance();
        }


    }

}
