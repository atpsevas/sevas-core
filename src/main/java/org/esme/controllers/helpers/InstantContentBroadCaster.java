package org.esme.controllers.helpers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.ActiveSubscription;
import org.esme.dao.models.SMSCampaign;
import org.esme.dto.Service;
import org.esme.gateway.ivr.WEB2IVR;
import org.esme.gateway.kannel.WEB2SMS;
import org.esme.utils.pingenerator.RandomPasswordGenerator;
import org.esme.webservices.airtel.AirtelUCIP;
import org.esme.webservices.glo.GloPSA;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;

public final class InstantContentBroadCaster {

    static String campaignMaxThread;
    static Properties prop;
    private static String DLRURL;
    private static String DLR_MASK;
    private static String params;
    SMSCampaign theCampaign;
    String theServiceNetwork;
    long messageCounter;
    Connection connection;
    Statement statment;
    ResultSet resultSet;
    Service service;
    String content;
    String serviceType;
    String billingShortCode;
    String msisdn;
    String billingText;
    String serviceName;
    long serviceID;
    String billingModule;
    String billingNetwork;
    String dlrMask;
    WEB2SMS webSMS;
    int counter;
    ArrayList categoryList;
    ArrayList<Service> serviceCache;
    ArrayList<ActiveSubscription> activeSubscription;
    Timer timer;
    //Direct Billing
    // EtisalatDirectBilling etisalatDirectBilling;
    //MaspPaymentResponseData chargingResponse = null;
    ArrayList<ActiveSubscription> activeSequentialSubscribers;
    ArrayList<ActiveSubscription> activeSsubscriptionCache;
    GloPSA gloPSA;
    AirtelUCIP airtelIBMUCIP;
    PreparedStatement ps;
    String transactionID;

    //get Send SENDSMS
    @SuppressWarnings("static-access")
    public InstantContentBroadCaster(Service service, String content, Connection conn) {
        this.service = service;
        this.connection = conn;
        this.content = content;
        this.webSMS = WEB2SMS.getInstance();
    }

    @SuppressWarnings({"UseSpecificCatch", "CallToPrintStackTrace"})
    public long sendContent() throws SQLException {

        if (this.service.getServiceSubType().equalsIgnoreCase("PSA")
                && Long.parseLong(this.service.getServicePeriod()) == 0) {
            try {
                gloPSA = GloPSA.getInstance();
                gloPSABillingInstantContent(gloPSA, service, serviceType);
            } catch (Exception ex) {
            }
        } else if (this.service.getServiceSubType().equalsIgnoreCase("EDB")
                && Long.parseLong(this.service.getServicePeriod()) == 0) {

        } else if (this.service.getServiceSubType().equalsIgnoreCase("UCIP")
                && Long.parseLong(this.service.getServicePeriod()) == 0) {
            try {
                airtelIBMUCIP = AirtelUCIP.getInstance();
                airtelUCIPBillingInstantContent(airtelIBMUCIP, service, serviceType);
            } catch (Exception ex) {
            }

        } else {

            statment = this.connection.createStatement();
            String instantContentQuery = "select substring(a.sub_msisdn,char_length(a.sub_msisdn)-9) as unique_msisdn from subscription a, subscription_service b "
                    + "where a.sub_status ilike 'active'  and "
                    + "((to_date(sub_expiry_date, 'YYYY-MM-DD') >= date(CURRENT_TIMESTAMP)) "
                    + "or (b.service_period = 0)) \n"
                    + "and a.sub_service_id = " + this.service.getServiceID() + " "
                    + "and a.sub_service_id = b.id group by unique_msisdn ";

            resultSet = statment.executeQuery(instantContentQuery);

            prop = AppBroker.getPropertyFileHandle();

            DLRURL = prop.getProperty("dlr_url");
            DLR_MASK = prop.getProperty("dlrMask");
            //Loop through guys that will receive content...
            while (resultSet.next()) {
                String prefix = AppBroker.CountryCodePrefix_PROP;
                String subMsisdn = prefix + resultSet.getString("unique_msisdn");

                // Initialize content Sender ID to MT shortcode
                String contentShortCode = this.service.getServiceMTShortCode();

                org.esme.broker.AppBroker.sevasLogger.error("sending....." + this.content + " to " + subMsisdn);
                try {

                    long servicePeriod = Long.parseLong(this.service.getServicePeriod());
                    String srcModule = "InstantContent";
                    long epochTime = System.currentTimeMillis() / 1000;
                    long pid = 127;
                    String contentNetwork = this.service.getServiceNetwork();

                    try {
                        contentShortCode = service.getServiceContentSenderID();

                        if (contentShortCode.isEmpty() || contentShortCode == null)
                            contentShortCode = this.service.getServiceMTShortCode();

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    // Daily Paid Content that bills
                    // TODO Abstract Billing and Content Delivery
                    if (servicePeriod == 0) {
                        srcModule = "DailyPaidContent";
                    }

                    if (contentNetwork.toLowerCase().contains("etisalat") && servicePeriod > 1) {
                        contentNetwork = prop.getProperty("etiContentBind");
                    }

                    //for MTN...We will use HTTP...
                    // TODO Confirm Content Delivery means for Integrat MTN
                    if (contentNetwork.trim().equalsIgnoreCase("integrat_mtn")) {

                        org.esme.broker.AppBroker.sevasLogger.info("Sending to SDP ");
                        webSMS = WEB2SMS.getInstance();
                        webSMS.SMSBOX_WITH_METADATA(service.getServiceSubType(),
                                " ", subMsisdn, formatText(this.content),
                                service.getServiceName(), service.getServiceID(),
                                srcModule, service.getServiceNetwork(),
                                "31", "?smpp?TLV_higate_service=" + service.getServiceMTShortCode());

                    }
                    // TODO Refactor WEB2SMS to single point of entry.

//                    else if (contentNetwork.trim().equalsIgnoreCase("mtn")) {
//
//                        try{
//                            contentShortCode = service.getServiceContentSenderID();
//                        } catch(Exception ex){
//                            ex.printStackTrace();
//                        }
//
//                        org.esme.broker.AppBroker.sevasLogger.info("Sending to SDP ");
//                        webSMS = WEB2SMS.getInstance();
//                        webSMS.SMSBOX(srcModule, contentShortCode,
//                                subMsisdn, formatText(this.content),
//                                service.getServiceName(), service.getServiceID(),
//                                srcModule, contentNetwork, "31");
//
//                    }

                    else {

                        if (service.getServiceContentDeliveryMethod().equalsIgnoreCase("byDate,voice")) {

                            //Define Voice Content Delivery Handler
                            WEB2IVR webIVR = WEB2IVR.getInstance();
                            webIVR.sendVoiceContent(msisdn, service);

                        } else {
                            webSMS = WEB2SMS.getInstance();
                            webSMS.SMSBOX(srcModule, contentShortCode, subMsisdn,
                                    formatText(this.content), service.getServiceName(), service.getServiceID(),
                                    srcModule, contentNetwork, "31");
                            //                        webSMS.SMSBOX_NODLR(contentShortCode, subMsisdn, formatText(this.content), contentNetwork);
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                messageCounter++;
            }

        }
        org.esme.broker.AppBroker.sevasLogger.info("\nFinished sending INSTANT Content");
        return messageCounter;

    }

    //For Single content....
    @SuppressWarnings("CallToPrintStackTrace")
    public void gloPSABillingInstantContent(GloPSA gloPSA, Service service, String serviceContentDeliveryMethod) {

        String srcModule = "DailyPaidContent";
        long amount = Long.parseLong(service.getServicePrice());

        org.esme.broker.AppBroker.sevasLogger.info("CONTENT DELIVERY METHOD " + serviceContentDeliveryMethod);
        try {

            try {

                Statement statement = this.connection.createStatement();
                ResultSet dataResultSet = statement.executeQuery("select distinct on(a.sub_msisdn) a.id, \n"
                        + "a.sub_msisdn from subscription a, subscription_service b \n"
                        + "where a.sub_status ilike 'active'  "
                        + " and b.service_period = 0 \n"
                        + "and a.sub_service_id = " + this.service.getServiceID() + " and a.sub_service_id = b.id");

                while (dataResultSet.next()) {
                    org.esme.broker.AppBroker.sevasLogger.info("WE ENTER SUB LOOP");
                    try {

                        gloPSA.makeGloChargingRquest(
                                service, amount,
                                srcModule, service.getServiceBillingShortCode(),
                                dataResultSet.getString("sub_msisdn"),
                                this.connection, this.content);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            org.esme.broker.AppBroker.sevasLogger.info("Error while processing daily alert content.");
            e.printStackTrace();
        }
    }

    @SuppressWarnings({"CallToPrintStackTrace", "UseSpecificCatch"})
    public void airtelUCIPBillingInstantContent(AirtelUCIP airtelIBMUCIP, Service service, String serviceContentDeliveryMethod) {

        String srcModule = "DailyPaidContent";
        ///END
        org.esme.broker.AppBroker.sevasLogger.info("CONTENT DELIVERY METHOD " + serviceContentDeliveryMethod);

        try {

            try {

                Statement statement = this.connection.createStatement();
                ResultSet dataResultSet = statement.executeQuery(
                        "select distinct on(a.sub_msisdn) a.id, \n"
                                + "a.sub_msisdn from subscription a, subscription_service b \n"
                                + "where a.sub_status ilike 'active' "
                                + " and b.service_period = 0 \n"
                                + "and a.sub_service_id = " + this.service.getServiceID()
                                + " and (lower(sub_source) not like 'bulk_sub' or lower(sub_source) != 'bulk_sub') and a.sub_service_id = b.id");

                while (dataResultSet.next()) {

                    //boolean subscriberBilled = false;
                    org.esme.broker.AppBroker.sevasLogger.info("WE ENTER SUB LOOP");

                    try {

                        airtelIBMUCIP.makeAirtelChargingRequest(
                                service, srcModule, service.getServiceBillingShortCode(),
                                "Content Purchase", this.content,
                                dataResultSet.getString("sub_msisdn"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            org.esme.broker.AppBroker.sevasLogger.info("Error while processing daily alert content.");
            e.printStackTrace();
        }
    }

    public void customBillingLogCondition(Service service, String subMSISDN,
                                          String message, String sourceModule, boolean isbILLED) {
        try {

            if (isbILLED) {
                //Log it as billed...
                org.esme.broker.AppBroker.sevasLogger.info("Logging ....");
                LogMTBillingResponse logMTBillingResponse = new LogMTBillingResponse();
                logMTBillingResponse.setMessageID(generateMessageID());
                logMTBillingResponse.setSenderID(service.getServiceMTShortCode());
                logMTBillingResponse.setRecipientMSISDN(subMSISDN);
                logMTBillingResponse.setSmsc(service.getServiceBillingNetwork());
                logMTBillingResponse.setMessage(formatText(message));
                logMTBillingResponse.setDelivertStatus("SUBMITTED");
                logMTBillingResponse.setServName(service.getServiceName());
                logMTBillingResponse.setSrcModule(sourceModule);
                logMTBillingResponse.setSmsc(service.getServiceBillingNetwork());
                logMTBillingResponse.setServiceID(service.getServiceID());
                logMTBillingResponse.setSubscriptionBillingType(service.getServiceSubType());
                logMTBillingResponse.billing_outgoing_messages(logMTBillingResponse);
            }
        } catch (Exception e) {

        }
    }

    public String formatText(String text) {
        String formattedContent;
        String myString = text.trim();
        byte[] ptext;
        try {
            ptext = myString.getBytes("ISO-8859-1");
            formattedContent = new String(ptext, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            return text;
        }
        return formattedContent;
    }

    public String generateMessageID() {
        return RandomPasswordGenerator.randomPasswordGenerator();
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace"})
    public String encodeParams(String param) {
        String encParam = "";
        try {
            encParam = URLEncoder.encode(param, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return encParam;
    }

    public String formatDateTillSecondToString(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd hh:mm:ss";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }
}
