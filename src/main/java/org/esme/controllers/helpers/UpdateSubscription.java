package org.esme.controllers.helpers;

import org.esme.broker.AppBroker;
import org.esme.dto.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UpdateSubscription extends HelperBroker {

    //WEB2SMS webSMS;
    static Properties prop;
    static PreparedStatement ps;

    //Instance of UpdateSubscription  can only be created by call this method...
    @SuppressWarnings("CallToPrintStackTrace")
    public UpdateSubscription() {
        prop = AppBroker.getPropertyFileHandle();

    }

    /*
      @desc Simply does subscription update considering the following parameters
      @params service
      @param  subscriber
      @param  promo
    */
    public static Boolean doUpdate(Connection connection, Service service, String subscriber, String billStatus) {

        Boolean updated = false;
        String subUpdateQuery = "UPDATE subscription AS t SET";

        String msisdn = subscriber.substring(subscriber.length() - 10);

        if (billStatus.equalsIgnoreCase("promo")) {
            // Give subscriber Promo using the preset Promo 
            Long servicePromoPeriod = Long.parseLong(service.getServicePromoPeriod());
            subUpdateQuery = subUpdateQuery + " sub_last_bill_status = '" + billStatus + "', "
                    + "last_billed_date = now(), sub_last_bill_attempt_time = now(), "
                    + "sub_status = 'active', sub_expiry_date = '" + addDate(servicePromoPeriod - 1) + "', "
                    + "sub_response_text = '"
                    + formatText(service.getServiceDoubleConfirmationMessage()) + "'";

        } else if (billStatus.equalsIgnoreCase("failed")) {
            subUpdateQuery = subUpdateQuery + " sub_last_bill_status = '" + billStatus + "', "
                    + "last_billed_date = now(), sub_status = 'active', "
                    + "sub_last_bill_attempt_time = now()";
        } else {
            Long servicePeriod = Long.parseLong(service.getServicePeriod());
            subUpdateQuery = subUpdateQuery + " sub_last_bill_status = '" + billStatus + "', "
                    + "last_billed_date = now(), sub_status = 'active', "
                    + "sub_expiry_date = '" + addDate(servicePeriod - 1) + "', "
                    + "sub_last_bill_attempt_time = now()";
        }

        subUpdateQuery = subUpdateQuery + " WHERE t.sub_service_id = " + service.getServiceID()
                + " AND t.sub_msisdn ilike '%" + msisdn + "'";

        org.esme.broker.AppBroker.sevasLogger.info(subUpdateQuery);

        try {
            ps = connection.prepareStatement(subUpdateQuery);
            int update = ps.executeUpdate();
            if (update > 0) {
                updated = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UpdateSubscription.class.getName()).log(Level.SEVERE, null, ex);
        }

        return updated;
    }


    @SuppressWarnings({"CallToPrintStackTrace", "null"})
    public void updateUserSubscription(Connection connection,
                                       Service service, String billingShortCode,
                                       String subscriber, String srcModule, String billingSatus) {

        org.esme.broker.AppBroker.sevasLogger.info("Updating user subscription...");

        PrintWriter pw = null;
        File dataFile;

        String theSubscriber = subscriber.substring(subscriber.length() - 10);

        long servicePeriod = Long.parseLong(service.getServicePeriod());

        if (srcModule.equalsIgnoreCase("NewSubscription")) {
            servicePeriod = Long.parseLong(service.getServicePeriod());
        }

        if (srcModule.equalsIgnoreCase("SubscriptionRenewal")) {
            servicePeriod = Long.parseLong(service.getServicePeriod());
        }

        if (srcModule.equalsIgnoreCase("SubscriptionRenewalRetry")) {
            servicePeriod = service.getFallbackPeriod();
        }

        if (srcModule.equalsIgnoreCase("SubscriptionRenewalRetry2")) {
            servicePeriod = service.getFallbackPeriod2();
        }

        try {
            String subUpdateQuery = "";
            if (billingSatus.equalsIgnoreCase("failed")) {
                subUpdateQuery
                        = "update subscription set last_billed_date = CURRENT_TIMESTAMP, "
                        + "last_billed_code = '" + billingShortCode + "', "
                        + "sub_last_bill_status = '" + billingSatus + "'      "
                        + "where sub_service_id = " + service.getServiceID() + " "
                        + "and sub_msisdn ilike '%" + theSubscriber + "';\n";

                ps = connection.prepareStatement(subUpdateQuery);
            } else {
                subUpdateQuery
                        = "update subscription set last_billed_date = CURRENT_TIMESTAMP, sub_expiry_date = '"
                        + addDate(Long.parseLong(service.getServicePeriod()) - 1) + "', "
                        + "last_billed_code = '" + billingShortCode + "', "
                        + "sub_last_bill_status = '" + billingSatus + "'     "
                        + "where sub_service_id = " + service.getServiceID() + " "
                        + "and sub_msisdn ilike '%" + theSubscriber + "';\n";

                ps = connection.prepareStatement(subUpdateQuery);
            }
            try {
                ps.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
                synchronized (this) {
                    dataFile = new File("/home/sevas/sqlFile.sql");
                    if (!dataFile.exists()) {
                        dataFile.createNewFile();
                    }
                    pw = new PrintWriter(new FileWriter(dataFile, true), true);
                    pw.write(subUpdateQuery);
                    pw.flush();
                }
            }

        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }

}
