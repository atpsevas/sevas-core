package org.esme.controllers.helpers;

import com.csvreader.CsvReader;
import org.esme.broker.AppBroker;
import org.esme.dao.models.SMSCampaign;
import org.esme.dto.Service;
import org.esme.gateway.kannel.WEB2SMS;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.esme.broker.AppBroker.sevasLogger;

public final class MessageBroadCaster extends HelperBroker {

    SMSCampaign theCampaign;
    String theServiceNetwork;
    long messageCounter;
    Connection connection;

    String serviceType;
    String billingShortCode;
    String msisdn;
    String billingText;
    String serviceName;
    long serviceID;
    String billingModule;
    String billingNetwork;
    String dlrMask;
    WEB2SMS webSMS;

    Properties prop = AppBroker.getPropertyFileHandle();

    //get Send SENDSMS
    @SuppressWarnings("static-access")
    public MessageBroadCaster(SMSCampaign smsCampaign, String serviceNetwork, Connection conn) {
        this.theCampaign = smsCampaign;
        this.theServiceNetwork = serviceNetwork;
        this.connection = conn;
    }

    @SuppressWarnings("static-access")
    public MessageBroadCaster(Connection conn) {
        this.connection = conn;
    }

    public static String convertStreamToStr(InputStream is) throws IOException {
        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        }
        return "";
    }

    @SuppressWarnings({"StaticNonFinalUsedInInitialization", "empty-statement", "CallToPrintStackTrace"})
    public void doSubscriptionReminder(Service service) {

        Connection dbConnection;
        Statement statement = null;
        ResultSet resultSet = null;
        PreparedStatement ps = null;
        String recipient = "";

        try {

            dbConnection = this.connection;
            //Reading the target number CSV file...
//            String reminderQuery = prop.getProperty("reminderQuery") + service.getServiceID() + " group by sub_msisdn";
            String reminderQuery = prop.getProperty("reminderQuery") + service.getServiceID() + " group by unique_msisdn";

            org.esme.broker.AppBroker.sevasLogger.info(reminderQuery);
            statement = dbConnection.createStatement();

            resultSet = statement.executeQuery(reminderQuery);

            while (resultSet.next()) {
                webSMS = WEB2SMS.getInstance();

                recipient = AppBroker.CountryCodePrefix_PROP + resultSet.getString("unique_msisdn");
                sevasLogger.info("Sending Reminder to " + recipient);
                webSMS.SMSBOX_NODLR(service.getServiceShortCode(), recipient, formatText(service.getServiceReminderMessage()), service.getServiceNetwork());
            }

            org.esme.broker.AppBroker.sevasLogger.info("Finished sending reminder");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings({"StaticNonFinalUsedInInitialization", "empty-statement", "CallToPrintStackTrace"})
    public void doSubscriptionActivationReminder(String reminderType) {

        Connection dbConnection;
        Statement statement = null;
        ResultSet resultSet = null;
        PreparedStatement ps = null;

        try {

            dbConnection = this.connection;
            //Reading the target number CSV file...

            if (reminderType.equalsIgnoreCase("weekly")) {
                //We want to attempt every body once per week until they confirm thier subscription
                org.esme.broker.AppBroker.sevasLogger.info(prop.getProperty("weeklySubscriptionActivationReminderQuery"));
                resultSet = statement.executeQuery(prop.getProperty("weeklySubscriptionActivationReminderQuery"));
            } else {
                //else.. we try those that subscribe on the hour....
                org.esme.broker.AppBroker.sevasLogger.info(prop.getProperty("subscriptionActivationReminderQuery"));
                resultSet = statement.executeQuery(prop.getProperty("subscriptionActivationReminderQuery"));
            }

            statement = dbConnection.createStatement();
            ps = dbConnection.prepareStatement(
                    "INSERT INTO send_sms (momt, sender, receiver, "
                            + "msgdata, smsc_id, sms_type, boxc_id,time) "
                            + "VALUES (?,?,?,?,?,?,?,?)");

            long epochTime = System.currentTimeMillis() / 1000;
            while (resultSet.next()) {
                ps.setString(1, "MT");
                ps.setString(2, resultSet.getString("service_shortcode"));
                ps.setString(3, resultSet.getString("sub_msisdn"));
                ps.setString(4, resultSet.getString("service_confirmation_message"));
                ps.setString(5, resultSet.getString("service_network"));
                ps.setLong(6, 2);
                ps.setString(7, prop.getProperty("contentSMSBoxID"));
                ps.setLong(8, epochTime + 3600);
                ps.executeUpdate();
            }
            org.esme.broker.AppBroker.sevasLogger.info("\nFinished sending doSubscriptionActivationReminder");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings({"StaticNonFinalUsedInInitialization", "empty-statement", "CallToPrintStackTrace"})
    public void doCampaign() {

        PreparedStatement ps = null;
        Connection dbConnection = null;
        CsvReader fileReader;

        long messageMclass = 0;

        try {
            messageMclass = Long.parseLong(prop.getProperty("messageMclass"));
        } catch (Exception e) {
            messageMclass = 3;
        }

        long batchSize = Long.parseLong(prop.getProperty("campaignBatchSize"));
        int count = 0;

        try {

            //Reading the target number CSV file...
            fileReader = new CsvReader(theCampaign.getCampaignTargetFileDir());
            dbConnection = localCampaignDBConnection();

            long epochTime = System.currentTimeMillis() / 1000;
            String recipient = null;
            //long count = 0;

            String batchQueryHeader
                    = "INSERT INTO send_sms (momt, sender, receiver, "
                    + "msgdata, smsc_id, sms_type, boxc_id,time,mclass) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?,?) ";

            //build the batch value...
            ps = dbConnection.prepareStatement(batchQueryHeader);
            org.esme.broker.AppBroker.sevasLogger.info(batchQueryHeader);

            while (fileReader.readRecord()) {

                recipient = fileReader.get(0);

                epochTime = System.currentTimeMillis() / 1000;

                ps.setString(1, "MT");
                ps.setString(2, theCampaign.getCampaignSenderID());
                ps.setString(3, recipient);
                ps.setString(4, formatText(theCampaign.getCampaignContent().replaceAll("'", "")));
                ps.setString(5, this.theServiceNetwork);
                ps.setLong(6, 2);
                ps.setString(7, prop.getProperty("contentSMSBoxID"));
                ps.setLong(8, epochTime);
                ps.setLong(9, messageMclass);

                try {
                    ps.addBatch();
                } catch (SQLException E) {
                }
                if (++count % batchSize == 0) {
                    try {
                        ps.executeBatch();
                    } catch (SQLException E) {
                    }
                }
            }

            try {
                //Insert the remaining data...
                ps.executeBatch(); // insert remaining records
                ps.close();
                org.esme.broker.AppBroker.sevasLogger.info("\nFinished sending reminder");

                //Update the campaign table after start sending braodcast
                ps = this.connection.prepareStatement(
                        "update sms_campaign set campaign_total_target_recipients = 0,"
                                + " campaign_total_sent = " + this.messageCounter + ", "
                                + " campaign_actual_end_time = now(), campaign_status = 'Sent' "
                                + " where campaign_id = " + this.theCampaign.getCampaignID() + " ");

                ps.executeUpdate();

            } catch (Exception E) {
                E.printStackTrace();
            }
            //connection.commit();;
            org.esme.broker.AppBroker.sevasLogger.info("\nFinished sending campaign");

        } catch (IOException | SQLException ex) {
            ex.printStackTrace();
        } finally {
            fileReader = null;
            try {
                dbConnection.close();
                dbConnection = null;
            } catch (SQLException ex) {
            }
        }
    }

    public String getDailyContent(Service service) {

        String content = "No Content";
        PreparedStatement ps = null;
        Statement statement = null;
        ResultSet resultSet = null;
        Connection dbConnection;

        dbConnection = this.connection;
        String params;

        try {
            //Pick content for the day...
            statement = dbConnection.createStatement();

            String contentQuery = "";

            String[] contentDelivery = service.getServiceContentDeliveryMethod().split(",");
            String contentDeliveryMethod = contentDelivery[0];

            if (contentDeliveryMethod.equalsIgnoreCase("byDate")) {
                contentQuery = "select a.content from content a, "
                        + "subscription_service b "
                        + "where a.content_service_id = " + service.getServiceID() + " "
                        + "and date(a.content_date_of_blast) = date(now()) "
                        + "and a.content_service_id = b.id order by a.content_upload_date desc limit 1";
            }

            org.esme.broker.AppBroker.sevasLogger.info(contentQuery);
            resultSet = statement.executeQuery(contentQuery);

            while (resultSet.next()) {
                org.esme.broker.AppBroker.sevasLogger.info("We have content");

                content = formatText(resultSet.getString("content"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(MessageBroadCaster.class.getName()).log(Level.SEVERE, null, ex);
        }

        return content;

    }

    public void doDailyAlert(Service service) {
        PreparedStatement ps = null;
        Statement statement = null;
        ResultSet resultSet = null;
        Connection dbConnection;

        dbConnection = this.connection;
        String subMSISDN;
        String params;
        String content;
        long lastContentDay = 0;
        long totalContentCount = 0;
        try {

            // TODO:  Create a simple classification algorithm to push content to users.
            // Write a module that will create an List Hash Mapping of content_id to subscribersQuery
            if (service.getServiceContentDeliveryMethod().equalsIgnoreCase("sequential")) {

                statement = dbConnection.createStatement();
                resultSet = statement.executeQuery(
                        "select distinct on (a.subscriber) a.content_service_id, "
                                + " a.subscriber, a.last_content_day "
                                + "from sequential_content_log a, subscription b "
                                + "where a.content_service_id = " + service.getServiceID() + " "
                                + " and b.sub_status = 'active' "
                                + "and to_date(b.sub_expiry_date, 'YYYY-MM-DD') > date(CURRENT_TIMESTAMP) "
                                + " and a.content_service_id = b.sub_service_id  ");

                //user has previusly recieved content
                while (resultSet.next()) {

                    lastContentDay = 0;
                    totalContentCount = 0;

                    try {

                        statement = dbConnection.createStatement();
                        ResultSet contentRowRS = statement.executeQuery(
                                "select count(*) as cnt from sequential_content "
                                        + "where content_service_id = " + service.getServiceID());
                        while (contentRowRS.next()) {
                            totalContentCount = contentRowRS.getLong("cnt");
                        }
                    } catch (Exception E) {

                    }
                    //Check if he has received the last content..If he has, we 'll start all over.
                    if (resultSet.getInt("last_content_day") == totalContentCount
                            || resultSet.getInt("last_content_day") == 0) {
                        lastContentDay = 1;
                    } else {
                        lastContentDay = resultSet.getInt("last_content_day") + 1;
                    }

                    //Query and send the actual content...
                    try {
                        ps = dbConnection.prepareStatement(
                                "select a.content from sequential_content a, "
                                        + "subscription_service b "
                                        + "where content_blast_day = ? "
                                        + "and a.content_service_id = ? "
                                        + "and a.content_service_id = b.id limit 1");
                        ps.setLong(1, lastContentDay);
                        ps.setLong(2, service.getServiceID());
                        resultSet = ps.executeQuery();
                    } catch (Exception e) {

                    }
                    //Determine src module...for daily paid content reporting...
                    String srcModule = "DailyAlert";
                    if (Long.parseLong(service.getServicePeriod()) == 0) {
                        srcModule = "DailyPaidContent";
                    }

                    while (resultSet.next()) {

                        //increment batch size..
                        subMSISDN = resultSet.getString("subscriber");
                        subMSISDN = AppBroker.CountryCodePrefix_PROP + subMSISDN.substring(subMSISDN.length() - 10);
                        content = resultSet.getString("content");

                        org.esme.broker.AppBroker.sevasLogger.info("Sending..." + content + " to " + subMSISDN);

                        //Voice based services....
                        if (service.getServiceContentDeliveryMethod().equalsIgnoreCase("byDate,voice")) {

                            sendVoiceContent(subMSISDN, service);

                        } else if (service.getServiceNetwork().trim().equalsIgnoreCase("integrat_mtn")) {

                            org.esme.broker.AppBroker.sevasLogger.info("Sending to SDP ");
                            webSMS = WEB2SMS.getInstance();
                            webSMS.SMSBOX_WITH_METADATA(service.getServiceSubType(),
                                    " ", subMSISDN, formatText(content),
                                    service.getServiceName(), service.getServiceID(),
                                    srcModule, service.getServiceNetwork(),
                                    "31", "?smpp?TLV_higate_service=" + service.getServiceMTShortCode());

                        } else {

                            webSMS = WEB2SMS.getInstance();
                            webSMS.SMSBOX(srcModule, service.getServiceMTShortCode(), subMSISDN,
                                    formatText(content),
                                    service.getServiceName(),
                                    service.getServiceID(),
                                    srcModule, service.getServiceNetwork(),
                                    "31");
                        }

                        org.esme.broker.AppBroker.sevasLogger.info(service.getServiceMTShortCode() + " "
                                + subMSISDN + " " + formatText(content) + " "
                                + service.getServiceNetwork());
                        try {

                            org.esme.broker.AppBroker.sevasLogger.info("UPDATING SEQ CONTENT LOG..");
                            String userNumbert = subMSISDN.substring(subMSISDN.length() - 9);
                            long lastCDay = resultSet.getLong("last_content_day") + 1;
                            ps = dbConnection.prepareStatement(
                                    "update sequential_content_log "
                                            + "set last_content_day = " + lastCDay + ", last_content_date = CURRENT_TIMESTAMP  "
                                            + "where subscriber ilike '%" + userNumbert + "' "
                                            + "and  content_service_id = " + service.getServiceID());
                            ps.executeUpdate();

                        } catch (Exception e) {
                        }
                    }
                }

            } else {

                //Pick content for the day...
                statement = dbConnection.createStatement();
                String contentQuery = "";

                String[] contentDelivery = service.getServiceContentDeliveryMethod().split(",");
                String contentDeliveryMethod = contentDelivery[0];

                if (contentDeliveryMethod.equalsIgnoreCase("byDate")) {
                    contentQuery = "select a.content from content a, "
                            + "subscription_service b "
                            + "where a.content_service_id = " + service.getServiceID() + " "
                            + "and date(a.content_date_of_blast) = date(now()) "
                            + "and a.content_service_id = b.id order by a.content_upload_date desc limit 1";
                }

                org.esme.broker.AppBroker.sevasLogger.info(contentQuery);
                resultSet = statement.executeQuery(contentQuery);

                while (resultSet.next()) {
                    org.esme.broker.AppBroker.sevasLogger.info("We have content");

                    content = formatText(resultSet.getString("content"));

                    String DLRURL = prop.getProperty("dlr_url");
                    String DLR_MASK = prop.getProperty("dlrMask");

                    try {
                        //Pick eligible subscribersQuery....
                        statement = dbConnection.createStatement();

                        String activeSubscriberQuery = "select substring(sub_msisdn,char_length(sub_msisdn)-9) as unique_msisdn from subscription "
                                + "where  sub_service_id = " + service.getServiceID() + " "
                                + "and sub_status = 'active' "
                                + "and (to_date(sub_expiry_date, 'YYYY-MM-DD') >= date(CURRENT_TIMESTAMP) "
                                + "or sub_service_period = 0) group by unique_msisdn";

                        ResultSet thRS = statement.executeQuery(activeSubscriberQuery);
                        org.esme.broker.AppBroker.sevasLogger.info(activeSubscriberQuery);

                        //Determine src module...for daily paid content reporting...
                        long servicePeriod = Long.parseLong(service.getServicePeriod());
                        String srcModule = "DailyAlert";
                        long epochTime = System.currentTimeMillis() / 1000;
                        long pid = 127;
                        String contentNetwork = service.getServiceNetwork();
                        String contentShortCode = service.getServiceShortCode();

                        if (servicePeriod == 0) {
                            srcModule = "DailyPaidContent";
                            contentNetwork = service.getServiceBillingNetwork();
                            contentShortCode = service.getServiceMTShortCode();
                        }

                        if (contentNetwork.toLowerCase().contains("etisalat") && servicePeriod > 1) {
                            contentNetwork = prop.getProperty("etiContentBind");
                        }

                        while (thRS.next()) {

                            org.esme.broker.AppBroker.sevasLogger.info("We have subscribers");
                            subMSISDN = thRS.getString("unique_msisdn");
                            subMSISDN = AppBroker.CountryCodePrefix_PROP + subMSISDN.substring(subMSISDN.length() - 10);

                            //Start from here...
                            //for MTN...We will use HTTP...
                            if (contentNetwork.equalsIgnoreCase("mtn")) {

                                webSMS = WEB2SMS.getInstance();
                                webSMS.SMSBOX(srcModule, contentShortCode, subMSISDN,
                                        formatText(content),
                                        service.getServiceName(),
                                        service.getServiceID(),
                                        srcModule, service.getServiceNetwork(),
                                        "31");
                            } else {

                                if (service.getServiceContentDeliveryMethod().equalsIgnoreCase("byDate,voice")) {
                                    //Define Voice Content Delivery Handler
                                    //WEB2IVR webIVR = WEB2IVR.getInstance();

                                    //
                                    //String callback_url = GatewayUtils.encodeCallbackURL(WEB2IVR.voice_callback_url_PROP, webIVR.generateParams(service, subMSISDN));
                                    //webIVR.pushIVRCall(contentShortCode, subMSISDN, callback_url);
                                    sendVoiceContent(subMSISDN, service);

                                } else {
                                    webSMS = WEB2SMS.getInstance();

                                    webSMS.SMSBOX(srcModule, service.getServiceMTShortCode(), subMSISDN,
                                            formatText(content), service.getServiceName(), service.getServiceID(),
                                            srcModule, service.getServiceNetwork(), "31");

                                    org.esme.broker.AppBroker.sevasLogger.info(service.getServiceMTShortCode() + " "
                                            + subMSISDN + " " + formatText(content) + " "
                                            + service.getServiceNetwork());
                                }

                            }

                        }

                    } catch (SQLException | NumberFormatException e) {
                    }
                }
                //ps.executeBatch();
            }
        } catch (SQLException | NumberFormatException e) {
            org.esme.broker.AppBroker.sevasLogger.info("Error while processing daily alert content.");
        }
        org.esme.broker.AppBroker.sevasLogger.info("Finished sending content for....");
    }

    public void sendVoiceContent(String msisdn, Service service) {
        try {

            //Long callFileRandomID = Long.valueOf(rand.nextLong());
            String fileName = prop.getProperty("VoiceContentCallFiles")
                    + formatDateToString(new Date()) + "_" + msisdn + ".call";

            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)));
            out.println("Channel: SIP/ETISALAT_OG/" + msisdn.substring(msisdn.length() - 10) + "");
            out.println("MaxRetries: 2");
            out.println("RetryTime: 900");
            out.println("WaitTime: 25");
            out.println("Application: Playback");
            out.println("Data: " + prop.getProperty("VoiceContentDir") + service.getServiceID() + "/" + formatDateToString(new Date()));
            out.println("Callerid: " + service.getServiceContentSenderID());
            out.println("Archive: yes");
            out.flush();

            copyFile(fileName);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean copyFile(String sourceFile) {
        boolean returnMessage = true;
        try {
            File src = new File(sourceFile);
            try {
                ProcessBuilder pb = new ProcessBuilder("bash", "-c", "chown  asterisk:asterisk " + src.getAbsolutePath());
                pb.redirectErrorStream(true);

                Process shell = pb.start();

                InputStream shellIn = shell.getInputStream();

                int shellExitStatus = shell.waitFor();

                String response = convertStreamToStr(shellIn);
                shellIn.close();
                System.err.append(response);
            } catch (Exception e) {
            }
            org.esme.broker.AppBroker.sevasLogger.info("Copying..." + src.getAbsolutePath());
            File dir = new File("/var/spool/asterisk/outgoing/");
            try {
                ProcessBuilder pb = new ProcessBuilder("bash", "-c", "mv " + src.getAbsolutePath() + " " + dir + "/");
                pb.redirectErrorStream(true);

                Process shell = pb.start();

                InputStream shellIn = shell.getInputStream();

                int shellExitStatus = shell.waitFor();

                String response = convertStreamToStr(shellIn);
                shellIn.close();
                System.err.append(response);
            } catch (Exception e) {
            }
        } catch (Exception ioe) {
            returnMessage = false;
            ioe.getMessage();
            ioe.getCause();
            org.esme.broker.AppBroker.sevasLogger.info("Problem copying file to dir.");
        }
        return returnMessage;
    }

    public void doDailyExtraPush(Service service) {
        PreparedStatement ps = null;
        Statement statement = null;
        ResultSet resultSet = null;
        Connection dbConnection;

        dbConnection = this.connection;
        String subMSISDN;
        String params;
        String content;
        try {

            statement = dbConnection.createStatement();
            //Pick content for the day...
            String contentQuery = "";
            if (service.getServiceContentDeliveryMethod().equalsIgnoreCase("byDate")) {

                contentQuery = "select a.content from content a, "
                        + "subscription_service b "
                        + "where (trim(a.content_date_of_blast) = '" + formatDateToString(new Date()).trim() + "' "
                        + "or trim(a.content_date_of_blast) = '" + cusFormatDateToString(new Date()).trim() + "') "
                        + "and a.content_service_id = " + service.getServiceID() + " "
                        + "and a.content_service_id = b.id order by a.content_upload_date asc limit 1";
            } else {
                //for random...
                contentQuery = "select a.content from content a, "
                        + "subscription_service b "
                        + "where a.content_service_id = " + service.getServiceID() + " "
                        + "and a.content_service_id = b.id order by random() limit 1";
            }

            org.esme.broker.AppBroker.sevasLogger.info(contentQuery);

            resultSet = statement.executeQuery(contentQuery);

            while (resultSet.next()) {

                org.esme.broker.AppBroker.sevasLogger.info("We have content");

                content = formatText(resultSet.getString("content"));

                String DLRURL = prop.getProperty("dlr_url");
                String DLR_MASK = prop.getProperty("dlrMask");

                try {

                    //Pick eligible subscribersQuery....
                    statement = dbConnection.createStatement();

                    ResultSet thRS = statement.executeQuery(
                            "select distinct(sub_msisdn) from subscription "
                                    + "where  sub_service_id = " + service.getServiceID() + " "
                                    + "and sub_status = 'active' "
                                    + "and (to_date(sub_expiry_date, 'YYYY-MM-DD') >= date(CURRENT_TIMESTAMP) "
                                    + "or sub_service_period = 0) ");

                    org.esme.broker.AppBroker.sevasLogger.info(
                            "select distinct(sub_msisdn) from subscription "
                                    + "where  sub_service_id = " + service.getServiceID() + " "
                                    + "and sub_status = 'active' \n"
                                    + "and (to_date(sub_expiry_date, 'YYYY-MM-DD') >= date(CURRENT_TIMESTAMP) or sub_service_period = 0) "
                                    + "group by sub_msisdn");

                    //Determine src module...for daily paid content reporting...
                    long servicePeriod = Long.parseLong(service.getServicePeriod());
                    String srcModule = "DailyAlert";
                    long epochTime = System.currentTimeMillis() / 1000;

                    long pid = 127;
                    if (servicePeriod == 0) {
                        srcModule = "DailyPaidContent";
                    }

                    String contentNetwork = service.getServiceNetwork();
                    String contentShortCode = service.getServiceShortCode();

                    if (contentNetwork.toLowerCase().contains("etisalat") && servicePeriod > 1) {
                        contentNetwork = prop.getProperty("etiContentBind");
                    }

                    if (servicePeriod == 0) {
                        contentNetwork = service.getServiceBillingNetwork();
                        contentShortCode = service.getServiceMTShortCode();
                    }

                    while (thRS.next()) {

                        org.esme.broker.AppBroker.sevasLogger.info("We have subscribers");
                        subMSISDN = thRS.getString("sub_msisdn");

                        //Start from here...
                        //for MTN...We will use HTTP...
                        if (contentNetwork.equalsIgnoreCase("mtn")) {

                            webSMS = WEB2SMS.getInstance();
                            webSMS.SMSBOX(srcModule, contentShortCode, subMSISDN,
                                    formatText(content),
                                    service.getServiceName(),
                                    service.getServiceID(),
                                    srcModule, service.getServiceNetwork(),
                                    "31");
                        } else {
                            ps = dbConnection.prepareStatement(
                                    "INSERT INTO send_sms (momt, sender, receiver, "
                                            + "msgdata, smsc_id, sms_type, boxc_id,"
                                            + "dlr_mask,dlr_url,time,pid) "
                                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?)");
//
                            org.esme.broker.AppBroker.sevasLogger.error("Sending..." + content + " to " + subMSISDN);
                            params = encodeParams(contentNetwork)
                                    + "&sub_type=" + encodeParams(service.getServiceSubType())
                                    + "&msg_id=" + encodeParams(generateMessageID())
                                    + "&sc=" + encodeParams(contentShortCode)
                                    + "&msisdn=" + encodeParams(subMSISDN)
                                    + "&msg=" + encodeParams(formatText(content))
                                    + "&serv_id=" + service.getServiceID()
                                    + "&serv_name=" + encodeParams(service.getServiceName())
                                    + "&src_module=" + encodeParams(srcModule)
                                    + "&dlr_mask=31&dlr=%d";

                            ps.setString(1, service.getServiceSubType());
                            ps.setString(2, contentShortCode);
                            ps.setString(3, subMSISDN);
                            ps.setString(4, formatText(content));
                            ps.setString(5, contentNetwork);
                            ps.setLong(6, 2);
                            ps.setString(7, prop.getProperty("billingSMSBoxID"));
                            ps.setLong(8, 31);
                            ps.setString(9, DLRURL + params);
                            ps.setLong(10, epochTime + 3600);
                            ps.setLong(11, pid);
                            ps.executeUpdate();
                        }

//                
                        org.esme.broker.AppBroker.sevasLogger.info(service.getServiceMTShortCode() + " "
                                + subMSISDN + " " + formatText(content) + " "
                                + service.getServiceNetwork());

                    }

                } catch (SQLException | NumberFormatException e) {
                }
            }
        } catch (SQLException | NumberFormatException e) {
            org.esme.broker.AppBroker.sevasLogger.info("Error while processing daily alert content.");
        }
        org.esme.broker.AppBroker.sevasLogger.info("\nFinished sending content for....");
    }

    public void doDailyFlashExtraPush(Service service) {
        PreparedStatement ps = null;
        Statement statement = null;
        ResultSet resultSet = null;
        Connection dbConnection;

        dbConnection = this.connection;
        String subMSISDN;
        String params;
        String content;
        try {

            statement = dbConnection.createStatement();
            //Pick content for the day...
            String contentQuery = "";
            if (service.getServiceContentDeliveryMethod().equalsIgnoreCase("byDate")) {

                contentQuery = "select a.content from content a, "
                        + "subscription_service b "
                        + "where (trim(a.content_date_of_blast) = '" + formatDateToString(new Date()).trim() + "' "
                        + "or trim(a.content_date_of_blast) = '" + cusFormatDateToString(new Date()).trim() + "') "
                        + "and a.content_service_id = " + service.getServiceID() + " "
                        + "and a.content_service_id = b.id order by a.content_upload_date asc limit 1";
            } else {
                //for random...
                contentQuery = "select a.content from content a, "
                        + "subscription_service b "
                        + "where a.content_service_id = " + service.getServiceID() + " "
                        + "and a.content_service_id = b.id order by random() limit 1";
            }

            org.esme.broker.AppBroker.sevasLogger.info(contentQuery);

            resultSet = statement.executeQuery(contentQuery);

            while (resultSet.next()) {

                org.esme.broker.AppBroker.sevasLogger.info("We have content");

                content = formatText(resultSet.getString("content"));

                String DLRURL = prop.getProperty("dlr_url");
                String DLR_MASK = prop.getProperty("dlrMask");

                try {

                    //Pick eligible subscribersQuery....
                    statement = dbConnection.createStatement();

                    String flashContentQuery;
                    flashContentQuery = "select distinct(sub_msisdn) from subscription "
                            + "where  sub_service_id = " + service.getServiceID() + " "
                            + "and sub_status = 'active' "
                            + "and (date(now()) - date(a.last_billed_date) = 7) "
                            + "and (to_date(sub_expiry_date, 'YYYY-MM-DD') >= date(CURRENT_TIMESTAMP) "
                            + "or sub_service_period = 0) ";
                    ResultSet thRS = statement.executeQuery(flashContentQuery);

                    //Determine src module...for daily paid content reporting...
                    long servicePeriod = Long.parseLong(service.getServicePeriod());
                    String srcModule = "SubscriptionRenewalRetry2";
                    long epochTime = System.currentTimeMillis() / 1000;

                    long pid = 127;

                    // Set MCLASS for Flash messgae
                    long mclass = 0;

                    String contentNetwork = service.getServiceNetwork();
                    String contentShortCode = service.getServiceShortCode();

                    if (contentNetwork.toLowerCase().contains("etisalat") && servicePeriod > 1) {
                        contentNetwork = prop.getProperty("fallbackBillingBind");
                    }

                    if (servicePeriod == 0) {
                        contentNetwork = service.getServiceBillingNetwork();
                        contentShortCode = service.getServiceMTShortCode();
                    }

                    while (thRS.next()) {

                        org.esme.broker.AppBroker.sevasLogger.info("We have subscribers");
                        subMSISDN = thRS.getString("sub_msisdn");

                        //Start from here...
                        //for MTN...We will use HTTP...
                        if (contentNetwork.equalsIgnoreCase("mtn")) {

                            webSMS = WEB2SMS.getInstance();
                            webSMS.SMSBOX(srcModule, contentShortCode, subMSISDN,
                                    formatText(content),
                                    service.getServiceName(),
                                    service.getServiceID(),
                                    srcModule, service.getServiceNetwork(),
                                    "31");
                        } else {
                            ps = dbConnection.prepareStatement(
                                    "INSERT INTO send_sms (momt, sender, receiver, "
                                            + "msgdata, smsc_id, sms_type, boxc_id,"
                                            + "dlr_mask,dlr_url,time,pid,mclass) "
                                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
//
                            org.esme.broker.AppBroker.sevasLogger.error("Sending..." + content + " to " + subMSISDN);
                            params = encodeParams(contentNetwork)
                                    + "&sub_type=" + encodeParams(service.getServiceSubType())
                                    + "&msg_id=" + encodeParams(generateMessageID())
                                    + "&sc=" + encodeParams(contentShortCode)
                                    + "&msisdn=" + encodeParams(subMSISDN)
                                    + "&msg=" + encodeParams(formatText(content))
                                    + "&serv_id=" + service.getServiceID()
                                    + "&serv_name=" + encodeParams(service.getServiceName())
                                    + "&src_module=" + encodeParams(srcModule)
                                    + "&dlr_mask=31&dlr=%d&queue=paid_content";

                            ps.setString(1, service.getServiceSubType());
                            ps.setString(2, contentShortCode);
                            ps.setString(3, subMSISDN);
                            ps.setString(4, formatText(content));
                            ps.setString(5, contentNetwork);
                            ps.setLong(6, 2);
                            ps.setString(7, prop.getProperty("billingSMSBoxID"));
                            ps.setLong(8, 31);
                            ps.setString(9, DLRURL + params);
                            ps.setLong(10, epochTime + 3600);
                            ps.setLong(11, pid);
                            ps.setLong(12, mclass);
                            ps.executeUpdate();
                        }

                        org.esme.broker.AppBroker.sevasLogger.info(service.getServiceMTShortCode() + " "
                                + subMSISDN + " " + formatText(content) + " "
                                + service.getServiceNetwork());

                    }

                } catch (SQLException | NumberFormatException e) {
                }
            }
        } catch (SQLException | NumberFormatException e) {
            org.esme.broker.AppBroker.sevasLogger.info("Error while processing daily extra push alert content.");
        }
        org.esme.broker.AppBroker.sevasLogger.info("\nFinished sending content for....");
    }

    public void doDailyHiddenContectPush(ArrayList<Service> serviceCache) {

        PreparedStatement ps = null;
        Statement contentStatement = null;
        Statement statement = null;
        ResultSet resultSet = null;
        Connection dbConnection;

        dbConnection = this.connection;
        String subMSISDN;
        String params;
        String content;
        try {

            for (Service service : serviceCache) {

                contentStatement = dbConnection.createStatement();
                //Pick content for the day...
                String contentQuery = "select a.content from content a, "
                        + "subscription_service b "
                        + "where a.content_service_id = " + service.getServiceID() + " "
                        + "and a.content_service_id = b.id order by random() limit 1";
                org.esme.broker.AppBroker.sevasLogger.info(contentQuery);

                resultSet = contentStatement.executeQuery(contentQuery);

                while (resultSet.next()) {

                    org.esme.broker.AppBroker.sevasLogger.info("We have content");

                    content = formatText(resultSet.getString("content"));

                    String DLRURL = prop.getProperty("dlr_url");

                    try {

                        //Pick eligible subscribersQuery....
                        statement = dbConnection.createStatement();

                        String subscribersQuery
                                = "select distinct(sub_msisdn) from subscription "
                                + "where sub_service_id = " + service.getServiceID() + " "
                                + "and sub_status = 'active' "
                                + "and (date(now()) - date(last_billed_date)) <= 60";

                        ResultSet thRS = statement.executeQuery(subscribersQuery);

                        //Determine src module...for daily paid content reporting...
                        long servicePeriod = Long.parseLong(service.getServicePeriod());
                        String srcModule = "SubscriptionRenewalRetry2";
                        long epochTime = System.currentTimeMillis() / 1000;

                        long pid = 64;

                        // Set MCLASS for Flash messgae
                        //long mclass = 0;
                        String contentNetwork = service.getServiceBillingNetwork();
                        String contentShortCode = service.getFallbackShortCode2();

                        if (contentNetwork.toLowerCase().contains("etisalat") && servicePeriod > 1) {
                            contentNetwork = prop.getProperty("fallbackBillingBind");
                        }

                        if (servicePeriod == 0) {
                            contentNetwork = service.getServiceBillingNetwork();
                            contentShortCode = service.getServiceMTShortCode();
                        }

                        final int batchSize = 1000;
                        int count = 0;

                        while (thRS.next()) {

                            org.esme.broker.AppBroker.sevasLogger.info("We have subscribers");
                            subMSISDN = thRS.getString("sub_msisdn");

                            //Start from here...
                            ps = dbConnection.prepareStatement(
                                    "INSERT INTO " + prop.getProperty(
                                            "billingSendSMSTable") + "(momt, sender, receiver, "
                                            + "msgdata, smsc_id, sms_type, boxc_id,"
                                            + "dlr_mask,dlr_url,time,pid) "
                                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?)");
//
                            org.esme.broker.AppBroker.sevasLogger.error("Sending..." + content + " to " + subMSISDN);
                            params = encodeParams(contentNetwork)
                                    + "&sub_type=" + encodeParams(service.getServiceSubType())
                                    + "&msg_id=" + encodeParams(generateMessageID())
                                    + "&sc=" + encodeParams(contentShortCode)
                                    + "&msisdn=" + encodeParams(subMSISDN)
                                    + "&msg=" + encodeParams(formatText(content))
                                    + "&serv_id=" + service.getServiceID()
                                    + "&serv_name=" + encodeParams(service.getServiceName())
                                    + "&src_module=" + encodeParams(srcModule)
                                    + "&dlr_mask=31&dlr=%d&queue=paid_content";

                            ps.setString(1, service.getServiceSubType());
                            ps.setString(2, contentShortCode);
                            ps.setString(3, subMSISDN);
                            ps.setString(4, formatText(content));
                            ps.setString(5, contentNetwork);
                            ps.setLong(6, 2);
                            ps.setString(7, prop.getProperty("billingSMSBoxID"));
                            ps.setLong(8, 31);
                            ps.setString(9, DLRURL + params);
                            ps.setLong(10, epochTime + 3600);
                            ps.setLong(11, pid);
                            ps.addBatch();

                            //Insert 1K records at a go...
                            if (++count % batchSize == 0) {
                                ps.executeBatch();
                            }
                            org.esme.broker.AppBroker.sevasLogger.info(service.getServiceMTShortCode() + " "
                                    + subMSISDN + " " + formatText(content) + " "
                                    + service.getServiceNetwork());

                        }
                        //Insert the last batch for the service..
                        ps.executeUpdate();

                    } catch (SQLException | NumberFormatException e) {
                    }
                }
            }
        } catch (SQLException | NumberFormatException e) {
            org.esme.broker.AppBroker.sevasLogger.info("Error while processing daily extra push alert content.");
        }
        org.esme.broker.AppBroker.sevasLogger.info("\nFinished sending content for....");
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public Connection localCampaignDBConnection() {
        Connection dbConnection = null;

        try {

            prop = AppBroker.getPropertyFileHandle();

            org.esme.broker.AppBroker.sevasLogger.info("-------- PostgreSQL "
                    + "JDBC Connection ------------");
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                org.esme.broker.AppBroker.sevasLogger.info("Where is your PostgreSQL JDBC Driver? "
                        + "Include in your library path!");
                e.printStackTrace();
            }

            org.esme.broker.AppBroker.sevasLogger.info("PostgreSQL JDBC Driver Registered!");
            dbConnection = DriverManager.getConnection(
                    prop.getProperty("campaignDBURL"), prop.getProperty("campaignDBUserName"),
                    prop.getProperty("campaignDBPassword"));

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dbConnection;
    }
}
