/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers.helpers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author saheedalbert
 */
public class FetchBlacklistedNumbers {

    String blacklistedURL;
    Scanner scanner;
    ArrayList<String> list;

    public FetchBlacklistedNumbers(String url) {
        this.blacklistedURL = url;
    }

    /**
     * @return
     */
    public Map<String, Integer> getBlacklistedNumbers() {

        URL url;

        //ArrayList<String> blacklistedNumbers = null;
        BufferedReader br;
        Map<String, Integer> blacklistedNumbers = null;

        try {
            // get URL content
            url = new URL(this.blacklistedURL);
            URLConnection conn = url.openConnection();

            // open the stream and put it into BufferedReader
            br = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));

            String inputLine;
            //blacklistedNumbers = new ArrayList<>();
            blacklistedNumbers = new HashMap<>();

            int index = 0;
            while ((inputLine = br.readLine()) != null) {
                //blacklistedNumbers.add(inputLine);
                blacklistedNumbers.put(inputLine, index);
                index++;
            }
            br.close();
            org.esme.broker.AppBroker.sevasLogger.info("Done");

        } catch (Exception e) {
        }

        return blacklistedNumbers;
    }

    public ArrayList<String> getDoNotChargeNumbers(String doNotChargeFilePath) {

        scanner = new Scanner(doNotChargeFilePath);
        list = new ArrayList<>();
        String nextLine = "";

        while (scanner.hasNextLine()) {
            nextLine = scanner.nextLine();
            list.add(nextLine.substring(nextLine.length() - 9));
        }

        return list;
    }

}
