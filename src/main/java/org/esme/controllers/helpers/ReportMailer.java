/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers.helpers;

/**
 * @author saheedalbert
 */

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class ReportMailer {

    String mailAddresses;
    String reportFilePath;
    String messageBody;
    String messageTitle;

    public ReportMailer(String addresses, String mBody, String title, String reportFile) {
        this.mailAddresses = addresses;
        this.messageTitle = title;
        this.messageBody = mBody;
        this.reportFilePath = reportFile;
    }

    public static void main(String arg[]) {
        new ReportMailer("saheedalbert@yahoo.com, saheedalbert@gmail.com", "Test", "test", "asda").sendReport();
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public void sendReport() {

        final String username = "moderateepheezy@gmail.com";
        final String password = "olatunji";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "false");
        //props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "localhost");
        props.put("mail.smtp.port", "25");
        //props.put("mail.user", username);
        //props.put("mail.password", password);

        //create new session with an autheticator
//        Authenticator auth = new Authenticator() {
//
//            @Override
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(username, password); //To change body of generated methods, choose Tools | Templates.
//            }
//        };

        Session session = Session.getInstance(props);

        try {

            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress("sevas@localhost.com"));

            //Set recipient Addresses
            if (customParser(this.mailAddresses, ",").size() < 1) {
                org.esme.broker.AppBroker.sevasLogger.info("No email adddress supply");
                return;
            }

            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("saheedalbert@yahoo.com"));

            message.addRecipients(Message.RecipientType.CC,
                    InternetAddress.parse(this.mailAddresses));

            message.setSubject(this.messageTitle);
            message.setText(this.messageBody);

            @SuppressWarnings("UnusedAssignment")
            MimeBodyPart messageBodyPart = new MimeBodyPart();

            Multipart multipart = new MimeMultipart();

            messageBodyPart = new MimeBodyPart();
            String file = this.reportFilePath;
            String fileName = this.reportFilePath;
            DataSource source = new FileDataSource(file);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(fileName);
            multipart.addBodyPart(messageBodyPart);

            message.setContent(multipart);

            org.esme.broker.AppBroker.sevasLogger.info("Sending");

            Transport.send(message);

            org.esme.broker.AppBroker.sevasLogger.info("Done");

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("ManualArrayToCollectionCopy")
    public List<String> customParser(String message, String delimiter) {
        @SuppressWarnings("unchecked")
        List<String> allTokens = new ArrayList();
        String delims = delimiter;
        String[] tokens = message.split(delims);
        for (String nextToken : tokens) {
            allTokens.add(nextToken);
        }
        return allTokens;
    }

    public String formatDateToString(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }
}
