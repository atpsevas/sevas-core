package org.esme.controllers.helpers;

import com.google.gson.Gson;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.esme.broker.AppBroker;
import org.esme.dao.models.MTBillingPayload;
import org.esme.dto.Service;
import org.esme.gateway.kannel.WEB2SMS;
import org.esme.utils.parser.StringParser;
import org.esme.webservices.airtel.AirtelUCIP;
import org.esme.webservices.etisalat.EtisalatDirectBilling;
import org.esme.xmlreader.SMSCStats;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeoutException;

@SuppressWarnings("serial")
public final class NewBillingBroadCaster extends AppBroker {

    private static final Logger logger = Logger.getLogger(NewBillingBroadCaster.class);

    public static ArrayList<Service> serviceCache;
    static PrintWriter updatePW = null;
    //Glo PSA
    //private static GloPSA gloXMLSOAPFilterWebservice;
    // Initialize Database connectin for Billing Module
    static java.sql.Connection dbConnection = null;
    private static Connection connection;
    //Direct Billing Details
    EtisalatDirectBilling etisalatDirectBilling = EtisalatDirectBilling.getInstance();
    //    MaspPaymentResponseData result = null;
//    MaspPaymentResponseData chargingResponse = null;
    String vendorUser;
    String vendorPassword;
    String vendorCode;
    String transactionID;
    AirtelUCIP airtelIBMUCIP;
    WEB2SMS web2SMS;

    // Billing Helper...
    // BillingBroadCaster billingBroadCaster;
    PreparedStatement statement = null;
    ResultSet resultSet;

    //    static List<String> cond = new ArrayList<>();
    String billingTracking;
//    private static Connection connection;

    SMSCStats theSMSCStats = SMSCStats.getInstance();

    public static int getHourOfTheDay() {
        // Check Time of the Day
        Calendar calendar = Calendar.getInstance();
        int hours = calendar.get(Calendar.HOUR_OF_DAY);

        return hours;
    }

    /**
     * @param serviceCache
     * @param dbConnection
     */
//    @SuppressWarnings("empty-statement")
    public void makeBillingRequest(ArrayList<Service> serviceCache, Connection dbConnection) {
        //Service theService = getServiceByID(serviceID, serviceCache);
        // Ensure service is not NULL

        //Check queue first...
        boolean mtQueue = false, edbQueue = false;
        try {
            mtQueue = isMTQueueEmpty(dbConnection);
            edbQueue = isEDBQueueEmpty();
        } catch (Exception e) {

        }

        for (Service service : serviceCache) {

            if (service.getServiceSubType().equalsIgnoreCase("mt") && mtQueue) {
                doMTBilling(service, dbConnection);
            } else if (service.getServiceSubType().equalsIgnoreCase("edb") && edbQueue) {
                doEDBBilling(service, dbConnection);
            }

        }
    }

// TODO: Implement Billing Factory for Billing Operation

//    public static void makeBillingRequest(Service theService, String senderID, String renewalMessage,
//                                          String srcModule, String recipientMSISDN) {
//
//        BillingProviderFactory billingFactory = new BillingProviderFactory();
//
//
//        billingFactory.setChannel(channel);
//
//        billingFactory.setRecipientMSISDN(recipientMSISDN);
//        billingFactory.setRenewalMessage(theService.getServiceRenewalMessage());
//        billingFactory.setSrcModule("SubscriptionRenewal");
//        billingFactory.setSenderID(senderID);
//        billingFactory.setTheService(theService);
//
//        // If EDB is set as fallback, do
//        if(senderID.contains("EDB")){
//            billingFactory.doBilling("edb");
//            return;
//        }
//
//        // Use MT Billing queue as default
//        billingFactory.doBilling(theService.getServiceSubType());
//
//
//    }

    /**
     * @param service
     * @param dbConnection
     * @desc Performs Actual MT Billing Operation per Service
     */
    public void doMTBilling(Service service, Connection dbConnection) {

        logger.info("=== Starting a New Billing Session ===");

        // Additional Query
        String expiredSubQuery = getPropertyFileHandle().getProperty("expiredSubQuery");

        logger.info("=== Billing for ===" + service.getServiceName());
        String billingCode;
        String renewalMessage;

        String sourceModule;
        String msisdn;
        String lastBilledStatus;
        long lastBilledInterval_PROP = Long.parseLong(getPropertyFileHandle().getProperty("lastBilledInterval"));
        long pid;
        Gson gson;
        MTBillingPayload mtBilling;
        String ampq_queue_name;
        RabbitMQPublisher publisher;

        try {

            // Expired subscription Datbaase PrepareStatement object
            Statement theStatement = dbConnection.createStatement();

            resultSet = theStatement.executeQuery(expiredSubQuery
                    + service.getServiceID());

            // Iterating throught expired subscription result
            while (resultSet.next()) {

                msisdn = resultSet.getString("unique_msisdn");
                long lastBilledInterval = resultSet.getLong("lastBilledInterval");
                lastBilledStatus = resultSet.getString("sub_last_bill_status");
                pid = 64;

                billingCode = service.getServiceBillingShortCode();
                renewalMessage = service.getServiceRenewalMessage();
                sourceModule = "SubscriptionRenewal";

                //If the subcribers hasn't been billed for a long time (Set in the property file)
                if (lastBilledInterval > lastBilledInterval_PROP) {

                    logger.info("Using Fallback Billing on " + msisdn + " after " + lastBilledInterval);
                    renewalMessage = service.getServiceFallbackRenewalMessage();
                    billingCode = service.getFallbackShortCode();
                    sourceModule = "SubscriptionRenewalRetry";

                }

                // if first billing service, send charge notification
                try {
                    logger.info("=== Checking if " + service.getServiceName() + " is On UAT ===");
                    String[] onUat = getPropertyFileHandle().getProperty("on_uat").split(",");
                    if (lastBilledStatus.equalsIgnoreCase("promo") && Arrays.asList(onUat).contains(String.valueOf(service.getServiceID()))) {
                        renewalMessage = getPropertyFileHandle().getProperty("chargeNotification_" + String.valueOf(service.getServiceID()));
                    }
                } catch (Exception ex) {
                    logger.error(ex);
                }

                // if subscriber is official MSISDN OR service notification settings is set to YES
                // Send notification to phone
                if (service.getMainbillingnotifySMSSetting().equalsIgnoreCase("yes")
                        || msisdn.matches("^23480994[4|5][\\d]{4}\\Z")
                        || msisdn.matches("^234908617392[1|2|8|4|6|3]$")) {
                    pid = 127;
                }

                List<String> uatServicesID = new StringParser().customParser(prop.getProperty("on_uat"), ",");

                mtBilling = new MTBillingPayload();
                mtBilling.setServiceSubType(service.getServiceSubType());
                mtBilling.setBillingCode(billingCode);
                mtBilling.setMsisdn(msisdn);
                mtBilling.setMessage(renewalMessage.replace("'", "\'"));
                mtBilling.setSmsc(service.getServiceBillingNetwork());
                mtBilling.setSmsType(2);
                mtBilling.setBillingSMSBoxID(billingSMSBoxID);
                mtBilling.setMilliTime(System.currentTimeMillis() / 1000);
                mtBilling.setPid(pid);
                mtBilling.setServiceId(service.getServiceID());
                mtBilling.setDlrMask(Long.parseLong(DLR_MASK));
                mtBilling.setSrcModule(sourceModule);

                if (uatServicesID.contains(String.valueOf(service.getServiceID()))) {
                    mtBilling.setIsOnUAT(true);
                } else {
                    mtBilling.setIsOnUAT(false);
                }

                //Convert to JSON...
                gson = new Gson();
                String mtBillingPayload = gson.toJson(mtBilling);
                logger.info(mtBillingPayload);

                publisher = RabbitMQPublisher.getInstance();

                try {
                    ampq_queue_name = prop.getProperty("MTBillingQueueName");
                    logger.info("MT QUEUE - " + ampq_queue_name);
                    publisher.publishDLRToRabbit(mtBillingPayload, ampq_queue_name);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("=== Unable to Start a new billing Session ===");
        }

    }

    public void doEDBBilling(Service service, Connection dbConnection) {

        etisalatDirectBilling = EtisalatDirectBilling.getInstance();

        logger.info("=== Starting a New Billing Session ===");

        // Additional Query
        String expiredSubQuery = "";

        expiredSubQuery = getPropertyFileHandle().getProperty("expiredSubQuery");

        logger.info("=== Billing for ===" + service.getServiceName());
        String billingCode = service.getServiceBillingShortCode();
        String renewalMessage = service.getServiceRenewalMessage();
        String sourceModule = "SubscriptionRenewal";
        String msisdn = "";
        String lastBilledStatus = "";

        try {

            PreparedStatement theStatement
                    = dbConnection.prepareStatement(expiredSubQuery
                    + service.getServiceID());

            ResultSet theResultSet = theStatement.executeQuery();

            while (theResultSet.next()) {

                msisdn = theResultSet.getString("unique_msisdn");
                lastBilledStatus = theResultSet.getString("sub_last_bill_status");
                // if first billing service, send charge notification
                try {
                    if (lastBilledStatus.equalsIgnoreCase("promo")) {
                        renewalMessage = getPropertyFileHandle().getProperty("chargeNotification_" + String.valueOf(service.getServiceID()));
                    }
                } catch (Exception ex) {
                    logger.error(ex);
                }
                logger.info("Making an ETISALAT Direct Billing request on " + msisdn);

                try {

                    etisalatDirectBilling.makeEtisalatDirectBillingBillingRequest(
                            service, sourceModule, billingCode,
                            "NILL", renewalMessage, msisdn);

                } catch (Exception ex) {
                    logger.info("Unable to Bill " + msisdn + " with ETISALAT DB");
                }

            }

        } catch (SQLException | NumberFormatException ex) {
            logger.info("=== Unable to Start a new billing Session ===");
        }
    }

    public long checkRabbitQueue(
            String rabbitQueueURL,
            String rabbitQueueUserName,
            String rabbitQueuePassword,
            String _queue) {
        JSONObject obj2 = null;

        JSONParser jSONParser = new JSONParser();
        try {

            String name = rabbitQueueUserName;
            String password = rabbitQueuePassword;

            String authString = name + ":" + password;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);

            URL url = new URL(rabbitQueueURL);
            URLConnection urlConnection = url.openConnection();
            urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
            InputStream is = urlConnection.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);

            int numCharsRead;
            char[] charArray = new char[1024];
            StringBuilder sb = new StringBuilder();
            while ((numCharsRead = isr.read(charArray)) > 0) {
                sb.append(charArray, 0, numCharsRead);
            }
            String theResult = sb.toString();

            JSONArray jSONArray = (JSONArray) jSONParser.parse(theResult);
            int size = jSONArray.size();
            for (int i = 0; i < size; i++) {
                obj2 = (JSONObject) jSONArray.get(i);
                String queue_name = obj2.get("name").toString();
                if (queue_name.equalsIgnoreCase(_queue)) {
                    return Long.parseLong(obj2.get("messages").toString());
                }
            }

        } catch (MalformedURLException e) {
        } catch (IOException | org.json.simple.parser.ParseException e) {
        }

        // If specified queue is not found, Return 0
        return 0;
    }

    /**
     * @return true or false
     */
    public boolean isEDBQueueEmpty() {

        EtisalatDirectBilling edb = new EtisalatDirectBilling();
        return edb.isQueueEmpty();

    }

    /**
     * Checks if MT Queue is Empty
     *
     * @param dbConnection
     * @return true or false
     */
    public boolean isMTQueueEmpty(Connection dbConnection) {
        boolean status = false;

        long receiverQueue, kannelQueue, publisherQueue, databaseQueue = 0;

        String smsc = "ETISALAT";

        String rabbitQueueName = prop.getProperty("rabbitQueueName");
        String rabbitPublisherQueueName = prop.getProperty("MTBillingQueueName");

        //Check Publisher Queue..
        try {
            publisherQueue = checkRabbitQueue(prop.getProperty("rabbitQueueURL"),
                    prop.getProperty("rabbitQueueUserName"), prop.getProperty("rabbitQueuePassword"), rabbitPublisherQueueName);
            logger.info("Rabbit PUblisher Queue Billing: " + publisherQueue);

            //Check Receiver Queue..
            receiverQueue = checkRabbitQueue(prop.getProperty("rabbitQueueURL"),
                    prop.getProperty("rabbitQueueUserName"), prop.getProperty("rabbitQueuePassword"), rabbitQueueName);
            logger.info("Rabbit Receiver Queue Billing: " + receiverQueue);

            //Check Kannel Queue..
            kannelQueue = theSMSCStats.readSMSCQueue(smsc);
            logger.info("Kannel Queue Billing: " + kannelQueue);

            // Check Database Queue Status      
            Statement st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery("select count(*) as theCount from "
                    + prop.getProperty("billingSendSMSTable") + " where smsc_id = '" + smsc + "' ");

            while (rs.next()) {
                databaseQueue = rs.getLong("theCount");
                logger.info("Database Queue - " + smsc + ":" + databaseQueue);
            }
            // Check Queue
            if (publisherQueue < 1 && kannelQueue < 1 && receiverQueue < 1 && databaseQueue < 1) {
                status = true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return status;
    }

}
