package org.esme.controllers.helpers;

import org.esme.dto.Service;
import org.esme.gateway.kannel.WEB2SMS;

import java.io.FileNotFoundException;

public class BillingRunable implements Runnable {

    WEB2SMS webSMS;
    String subcriber;
    Service service;
    String content;
    String mtBillingMethod;
    String DLR_MASK;
    String srcModule;
    String billingCode;
    String billingMessage;

    public BillingRunable(
            Service serivce, String rec, String mtBMethod,
            String dlrMask, String sourceModule) throws FileNotFoundException {

        service = serivce;
        subcriber = rec;
        mtBillingMethod = mtBMethod;
        DLR_MASK = dlrMask;
        srcModule = sourceModule;

        if (srcModule.equalsIgnoreCase("SubscriptionRenewal")) {
            billingCode = service.getServiceBillingShortCode();
            billingMessage = service.getServiceRenewalMessage();
        } else {
            billingCode = service.getFallbackShortCode();
            billingMessage = service.getServiceFallbackRenewalMessage();
        }

    }

    @Override
    @SuppressWarnings("CallToPrintStackTrace")
    public void run() {
        try {
            webSMS = WEB2SMS.getInstance();
            try {

//
                if (mtBillingMethod.equalsIgnoreCase("noDLR")) {
                    try {
                        if (service.getMainbillingnotifySMSSetting().equalsIgnoreCase("yes")
                                || subcriber.contains("9944")) {
                            webSMS.SMSBOX(
                                    service.getServiceSubType(),
                                    this.billingCode,
                                    subcriber,
                                    this.billingMessage,
                                    service.getServiceName(),
                                    service.getServiceID(),
                                    srcModule,
                                    service.getServiceBillingNetwork(),
                                    DLR_MASK);

                        } else {
                            webSMS.SILENCE_SMSBOX(
                                    service.getServiceSubType(),
                                    this.billingCode,
                                    subcriber,
                                    this.billingMessage,
                                    service.getServiceName(),
                                    service.getServiceID(),
                                    srcModule,
                                    service.getServiceBillingNetwork(),
                                    DLR_MASK);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        if (service.getMainbillingnotifySMSSetting().equalsIgnoreCase("yes")
                                || subcriber.contains("9944")) {
                            webSMS.SMSBOX_NODLR(this.billingCode,
                                    subcriber, this.billingMessage,
                                    service.getServiceBillingNetwork());

                        } else {
                            webSMS.SILENCE_SMSBOX_NODLR(
                                    this.billingCode,
                                    subcriber, this.billingMessage,
                                    service.getServiceBillingNetwork());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
