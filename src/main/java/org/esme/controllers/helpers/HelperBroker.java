/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers.helpers;

import org.esme.dto.Service;
import org.esme.utils.pingenerator.RandomPasswordGenerator;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

/**
 * @author saheedalbert
 */
public class HelperBroker {


    public static String addDate(long NoOfDays) {
        int castedNumber = (int) NoOfDays;
        Date date = new Date();
        String dt = formatDateToString(date);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(dt));
            c.add(Calendar.DATE, castedNumber);
            dt = sdf.format(c.getTime());
        } catch (ParseException ex) {
        }
        return dt;
    }

    public static String formatDateToString(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }

    public static String formatDateTillSecondToString(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd hh:mm:ss";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }

    public static String cusFormatDateToString(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-M-dd";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }

    public static int getCurrentHour() {
        Date date = new Date();   // given date
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(date);   // assigns calendar to given date
        calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
        calendar.get(Calendar.HOUR);        // gets hour in 12h format
        calendar.get(Calendar.MONTH);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static String formatText(String text) {
        String formattedContent;
        String myString = text.trim();
        byte[] ptext;
        try {
            ptext = myString.getBytes("ISO-8859-1");
            formattedContent = new String(ptext, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            return text;
        }
        return formattedContent;
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public static long dateDiff(String firstDate, String secondDate) {
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        long diff = 0;
        try {
            Date date1 = myFormat.parse(firstDate);
            Date date2 = myFormat.parse(secondDate);
            diff = date2.getTime() - date1.getTime();
            org.esme.broker.AppBroker.sevasLogger.info("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public static long dateDiffInHours(String firstDate, String secondDate) {
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        long diff = 0;
        try {
            Date date1 = myFormat.parse(firstDate);
            Date date2 = myFormat.parse(secondDate);
            diff = date2.getTime() - date1.getTime();
            org.esme.broker.AppBroker.sevasLogger.info("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public static void customBillingLogCondition(Service service, String subMSISDN,
                                                 String message, String sourceModule, boolean isbILLED) {
        try {

            if (isbILLED) {
                //Log it as billed...
                LogMTBillingResponse logMTBillingResponse = new LogMTBillingResponse();
                logMTBillingResponse.setMessageID(generateMessageID());
                logMTBillingResponse.setSenderID(service.getServiceMTShortCode());
                logMTBillingResponse.setRecipientMSISDN(subMSISDN);
                logMTBillingResponse.setSmsc(service.getServiceBillingNetwork());
                logMTBillingResponse.setMessage(formatText(message));
                logMTBillingResponse.setDelivertStatus("SUBMITTED");
                logMTBillingResponse.setServName(service.getServiceName());
                logMTBillingResponse.setSrcModule(sourceModule);
                logMTBillingResponse.setSmsc(service.getServiceBillingNetwork());
                logMTBillingResponse.setServiceID(service.getServiceID());
                logMTBillingResponse.setSubscriptionBillingType(service.getServiceSubType());
                logMTBillingResponse.billing_outgoing_messages(logMTBillingResponse);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String generateMessageID() {
        return RandomPasswordGenerator.randomPasswordGenerator();
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace"})
    public static String encodeParams(String param) {
        String encParam = "";
        try {
            encParam = URLEncoder.encode(param, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return encParam;
    }

    public static String formatDateToString2(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd hh:mm:ss";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }


}
