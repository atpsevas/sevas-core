package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.UserRole;
import org.esme.dao.models.Users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UpdateUsers extends AppBroker {

    PreparedStatement pstmt;

    @SuppressWarnings("CallToThreadDumpStack")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String action = removePoison(request.getParameter("action"));
            String username = request.getParameter("username");
            String role = request.getParameter("role");
            if (action.equalsIgnoreCase("delete")) {

                pstmt = getDBConnection().prepareStatement(
                        "delete from users where username = ? ");
                pstmt.setString(1, username);
                int update = pstmt.executeUpdate();
                if (update > 0) {
                    //update users cache..
                    updateUsers();
                    updateUserRole();
                    request.setAttribute(
                            "message",
                            "User successfully deleted.");
                    request.getRequestDispatcher("manage_users.jsp").forward(request, response);
                } else {

                    request.setAttribute(
                            "message",
                            "No user was deleted.");
                    request.getRequestDispatcher(
                            "manage_users.jsp").forward(request, response);
                }

            } else if (action.equalsIgnoreCase("update")) {
                String password = request.getParameter("password");
                try {

                    pstmt = getDBConnection().prepareStatement(
                            "update users set password = ? where username = ? ");
                    pstmt.setString(1, password);
                    pstmt.setString(2, username);
                    int update = pstmt.executeUpdate();
                    if (update > 0) {

                        pstmt = getDBConnection().prepareStatement(
                                "update roles set role = ? where username = ? ");
                        pstmt.setString(1, role);
                        pstmt.setString(2, username);
                        //Update users cache..
                        updateUsers();
                        updateUserRole();
                        request.setAttribute(
                                "message",
                                "User successfully updated.");
                        request.getRequestDispatcher(
                                "new_user.jsp?username=" + username + "&password=" + password + "").forward(request, response);
                    } else {

                        request.setAttribute("message", "No user was updated.");
                        request.getRequestDispatcher(
                                "new_user.jsp?username=" + username + "&password=" + password + "").forward(request, response);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    request.setAttribute(
                            "message", "SEVERE: DB eror occured while trying to update user account.");
                    request.getRequestDispatcher(
                            "new_user.jsp?username=" + username + "&password=" + password + "").forward(request, response);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute(
                    "message",
                    "An error occured while trying to update your account.");
            request.getRequestDispatcher("new_user.jsp").forward(request, response);
        } finally {
            try {
                pstmt.close();
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public void updateUsers() {
        ArrayList<Users> usersCache = new ArrayList<Users>();
        Users user;
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(getServletContext().getInitParameter("usersQuery"));
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                //update previously stored job contents from the container...
                while (resultSet.next()) {
                    user = new Users();
                    user.setId(resultSet.getInt("id"));
                    user.setUserName(resultSet.getString("username"));
                    user.setPassword(resultSet.getString("password"));
                    usersCache.add(user);
                }
                //Create cache of all users
                getServletContext().setAttribute("usersCache", usersCache);
            }

        } catch (Exception ex) {
            // ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }

    public void updateUserRole() {
        ArrayList<UserRole> roleCache = new ArrayList<UserRole>();
        UserRole userRole;
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(getServletContext().getInitParameter("userRoleQuery"));
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                //update previously stored job contents from the container...
                while (resultSet.next()) {
                    userRole = new UserRole();
                    userRole.setRoleID(resultSet.getInt("id"));
                    userRole.setUserName(resultSet.getString("username"));
                    userRole.setRoleName(resultSet.getString("role"));
                    roleCache.add(userRole);
                }
                //Create cache of all users
                getServletContext().setAttribute("userRoleCache", roleCache);
            }

        } catch (Exception ex) {
            // ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }
}
