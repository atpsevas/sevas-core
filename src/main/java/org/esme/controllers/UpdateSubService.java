package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.Content;
import org.esme.dao.models.Users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class UpdateSubService extends AppBroker {

    PreparedStatement pstmt;
    long serviceID, billingRetryIntervals, billingTime, serviceReminderTime,
            fallbackExpiryPeriod, serviceOwner, parentServiceID;
    String action;
    private String serviceMOShortCode, serviceMTShortCode,
            serviceSubKeyword, serviceUnSubKeyword, price, welcomeShortCode,
            serviceTimeOfBlast, serviceSubMsg, serviceUnsubMsg, serviceRemMsg,
            serviceRemPeriod, serviceAllowableKeywords, serviceCategory, serviceName,
            serviceCategoryKeyword, serviceContentDeliveryMethod, serviceDescription,
            serviceHelpMessage, serviceStatus, serviceNetwork, servicePromo, servicePromoPeriod, serviceCrossSell,
            serviceCrossNetwork, serviceCrossSellShortCode, serviceCrossMessage,
            autoSubToParentService, serviceConfirmation, serviceConfirmationKeyword,
            serviceConfirmationMessage, serviceContentSenderID;
    //routing...
    private String routeToExternalAPI;
    private String routeToExternalAPByShortCode;
    private String externalAPIURL;
    private String externalAPIShortCodeParam;
    private String externalAPIMSISDNParam;
    private String externalAPIMessageParam;

    @SuppressWarnings({"CallToThreadDumpStack", "UseSpecificCatch", "CallToPrintStackTrace"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try {
            serviceID = Long.parseLong(removePoison(request.getParameter("id").trim()));
            action = removePoison(request.getParameter("action"));
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Service ID cannot be empty.");
            request.getRequestDispatcher("manage_sub_services.jsp").forward(request, response);
            return;
        }

        try {
            action = removePoison(request.getParameter("action"));
        } catch (NullPointerException e) {
        }

        try {

            if (action.equalsIgnoreCase("delete")) {
                pstmt = getDBConnection().prepareStatement("delete from subscription_service where id = ? ");
                pstmt.setLong(1, serviceID);
                int update = pstmt.executeUpdate();
                if (update > 0) {
                    //update service archive...
                    updateServicesCache();
                    request.setAttribute(
                            "message",
                            "Service successfully deleted.");
                    request.getRequestDispatcher(
                            "manage_sub_services.jsp").forward(request, response);
                } else {
                    request.setAttribute(
                            "message",
                            "No service was deleted.");
                    request.getRequestDispatcher(
                            "manage_sub_services.jsp").forward(request, response);
                }
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            serviceName = removePoison(request.getParameter("sn").trim());
        } catch (NullPointerException e) {
        }

        try {
            serviceNetwork = removePoison(request.getParameter("net").trim());
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Please select service network.");
            request.getRequestDispatcher("manage_sub_services.jsp").forward(request, response);
            return;
        }

        try {
            serviceMOShortCode = removePoison(request.getParameter("ssc").trim());
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Service short code cannot be empty.");
            request.getRequestDispatcher("manage_sub_services.jsp").forward(request, response);
            return;
        }

        try {
            serviceMTShortCode = removePoison(request.getParameter("mt_ssc"));
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Service MT short code cannot be empty.");
            request.getRequestDispatcher("manage_sub_services.jsp").forward(request, response);
            return;
        }

        try {
            welcomeShortCode = removePoison(request.getParameter("wsc"));
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Service welcome message short code cannot be empty.");
            request.getRequestDispatcher("manage_sub_services.jsp").forward(request, response);
            return;
        }

        try {
            serviceReminderTime = Long.parseLong(removePoison(request.getParameter("srt")));
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Service subscription reminder time cannot be empty.");
            request.getRequestDispatcher("new_sub_service.jsp").forward(request, response);
            return;
        }

        try {
            serviceSubKeyword = removePoison(request.getParameter("skw"));
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Service keyword cannot be empty.");
            request.getRequestDispatcher("manage_sub_services.jsp").forward(request, response);
            return;
        }

        try {
            serviceSubMsg = removePoison(request.getParameter("ssm").trim());
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Service subscription message cannot be empty.");
            request.getRequestDispatcher("manage_sub_services.jsp").forward(request, response);
            return;
        }

        try {
            serviceUnsubMsg = removePoison(request.getParameter("sum").trim());
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Service unsubscription message cannot be empty.");
            request.getRequestDispatcher("manage_sub_services.jsp").forward(request, response);
            return;
        }

        try {
            serviceHelpMessage = removePoison(request.getParameter("shm").trim());
        } catch (NullPointerException e) {
            request.setAttribute("message", "*Service help message cannot be empty.");
            request.getRequestDispatcher("manage_sub_services.jsp").forward(request, response);
            return;
        }

        try {
            serviceContentDeliveryMethod = removePoison(request.getParameter("cdm").trim());

            // Adding content type functionality
            String content_type = removePoison(request.getParameter("content_type").trim());
            if (content_type.equalsIgnoreCase("voice")) {
                serviceContentDeliveryMethod = serviceContentDeliveryMethod + ",voice";
            } else {
                serviceContentDeliveryMethod = serviceContentDeliveryMethod + ",sms";
            }

        } catch (NullPointerException e) {
        }

        try {
            serviceDescription = removePoison(request.getParameter("sd"));
        } catch (NullPointerException e) {
        }
        try {
            serviceAllowableKeywords = removePoison(request.getParameter("sakw"));
        } catch (NullPointerException e) {
        }
        try {
            serviceUnSubKeyword = removePoison(request.getParameter("suk"));
        } catch (NullPointerException e) {
        }

        try {
            serviceCategory = removePoison(request.getParameter("sc"));
        } catch (Exception e) {
            serviceCategory = "1";
        }
        try {
            serviceCategoryKeyword = "Nill";
        } catch (NullPointerException e) {
        }

        try {
            serviceTimeOfBlast = removePoison(request.getParameter("stob"));
        } catch (NullPointerException e) {
        }
        try {
            serviceRemMsg = removePoison(request.getParameter("srm").trim());
        } catch (NullPointerException e) {
        }
        try {
            serviceRemPeriod = removePoison(request.getParameter("srp"));
        } catch (NullPointerException e) {
        }
        try {
            serviceOwner = Long.parseLong(request.getParameter("so"));
        } catch (NullPointerException e) {
        }

        try {
            serviceStatus = removePoison(request.getParameter("status"));
        } catch (NullPointerException e) {
        }

        try {
            servicePromo = removePoison(request.getParameter("promo"));
        } catch (NullPointerException e) {
        }

        try {
            servicePromoPeriod = removePoison(request.getParameter("promoPeriod"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            serviceCrossSell = removePoison(request.getParameter("cross_sell"));
        } catch (NullPointerException e) {
        }
        try {
            serviceCrossNetwork = removePoison(request.getParameter("cross_net"));
        } catch (NullPointerException e) {
        }
        try {
            serviceCrossSellShortCode = removePoison(request.getParameter("cross_sc"));
        } catch (NullPointerException e) {
        }
        try {
            serviceCrossMessage = removePoison(request.getParameter("cross_msg"));
        } catch (NullPointerException e) {
        }

        try {
            routeToExternalAPI = removePoison(request.getParameter("route"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            routeToExternalAPByShortCode = removePoison(request.getParameter("route_by_sc"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            externalAPIURL = removePoison(request.getParameter("routeurl"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            externalAPIShortCodeParam = removePoison(request.getParameter("routeshortcode"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            externalAPIMSISDNParam = removePoison(request.getParameter("routemsisdn"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            externalAPIMessageParam = removePoison(request.getParameter("routemessage"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            parentServiceID = Long.parseLong(removePoison(request.getParameter("parentServID")));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            autoSubToParentService = removePoison(request.getParameter("autoSubToParentServ"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            serviceConfirmation = removePoison(request.getParameter("dbCon"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            serviceConfirmationKeyword = removePoison(request.getParameter("dck"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            serviceConfirmationMessage = removePoison(request.getParameter("dcm"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            serviceContentSenderID = removePoison(request.getParameter("csID"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            pstmt = getDBConnection().prepareStatement(
                    "update subscription_service set service_owner = ?,  "
                            + "service_shortcode =?, "
                            + "service_mt_shortcode =?, service_keyword =?, "
                            + "service_allowable_keyword =?, "
                            + "service_unsubcribe_keyword =?, service_category =?,  "
                            + "service_tob =?, service_subscription_msg =?, "
                            + "service_unsubscription_msg =?, service_reminder_msg =?, "
                            + "service_reminder_period =?, service_description =?, "
                            + "service_category_keyword =?, service_content_delivery_method =?, "
                            + "service_help_message = ?, service_reminder_time = ?, "
                            + "service_network = ?, service_wc_msg_shortcode = ?, service_status = ?, "
                            + "service_promo = ?, service_cross_sell = ?,"
                            + "service_cross_sell_network = ?,"
                            + "service_cross_sell_shortcode = ?,"
                            + "service_cross_sell_message = ?, route_to_external_api = ?, "
                            + "route_by_shortcode = ?, external_api_url = ?, external_api_sc_param = ?, "
                            + "external_api_msisdn_param = ?, external_api_msg_param = ?,"
                            + "auto_sub_to_parent_serv = ?, service_parent_id = ?, "
                            + "service_name = ?, service_double_confirm = ?, "
                            + "service_double_confirm_keyword = ?, "
                            + "service_double_confirm_message = ?, service_promo_period = ?, "
                            + "service_content_sender_id = ?  where id = ?");

            pstmt.setLong(1, serviceOwner);
            pstmt.setString(2, serviceMOShortCode);
            pstmt.setString(3, serviceMTShortCode);
            pstmt.setString(4, serviceSubKeyword);
            pstmt.setString(5, serviceAllowableKeywords);
            pstmt.setString(6, serviceUnSubKeyword);
            pstmt.setLong(7, Long.parseLong(serviceCategory));
            pstmt.setLong(8, Long.parseLong(serviceTimeOfBlast));
            pstmt.setString(9, serviceSubMsg);
            pstmt.setString(10, serviceUnsubMsg);
            pstmt.setString(11, serviceRemMsg);
            pstmt.setLong(12, Long.parseLong(serviceRemPeriod));
            pstmt.setString(13, serviceDescription);
            pstmt.setString(14, serviceCategoryKeyword);

            // Update content type and delivery method as concatenation e.g. byDate,voice
            // Default is SMS 
            pstmt.setString(15, serviceContentDeliveryMethod);

            pstmt.setString(16, serviceHelpMessage);
            pstmt.setLong(17, serviceReminderTime);
            pstmt.setString(18, serviceNetwork);
            pstmt.setString(19, welcomeShortCode);
            pstmt.setString(20, serviceStatus);

            pstmt.setString(21, servicePromo);
            pstmt.setString(22, serviceCrossSell);
            pstmt.setString(23, serviceCrossNetwork);
            pstmt.setString(24, serviceCrossSellShortCode);
            pstmt.setString(25, serviceCrossMessage);

            //routing.
            pstmt.setString(26, routeToExternalAPI);
            pstmt.setString(27, routeToExternalAPByShortCode);
            pstmt.setString(28, externalAPIURL);
            pstmt.setString(29, externalAPIShortCodeParam);
            pstmt.setString(30, externalAPIMSISDNParam);
            pstmt.setString(31, externalAPIMessageParam);

            pstmt.setString(32, autoSubToParentService);
            pstmt.setLong(33, parentServiceID);

            pstmt.setString(34, serviceName);

            pstmt.setString(35, serviceConfirmation);
            pstmt.setString(36, serviceConfirmationKeyword);
            pstmt.setString(37, serviceConfirmationMessage);

            pstmt.setString(38, servicePromoPeriod);
            pstmt.setString(39, serviceContentSenderID);

            pstmt.setLong(40, serviceID);

            int update = pstmt.executeUpdate();
            if (update > 0) {

                //update user Archive
                updateUsers();

                //update service archive..
                updateServicesCache();

                //update content table....
                PreparedStatement ps = getDBConnection().prepareStatement(
                        "update content set content_provider_id = " + serviceOwner + " "
                                + "where content_service_id = " + serviceID + " ");
                ps.executeUpdate();

                updateContentCache();

                request.setAttribute(
                        "message",
                        "Service successfully updated.");
                request.getRequestDispatcher(
                        "manage_sub_services.jsp").forward(request, response);
            } else {

                request.setAttribute(
                        "message",
                        "No service was updated.");
                request.getRequestDispatcher(
                        "manage_sub_services.jsp").forward(request, response);
            }

        } catch (Exception e) {
            request.setAttribute(
                    "message",
                    "Sorry, service cannot be updated at the momment. "
                            + "Please try again later.");
            request.getRequestDispatcher("manage_sub_services.jsp").forward(request, response);
            e.printStackTrace();
        } finally {
            try {
                pstmt.close();
            } catch (SQLException ex) {
            }

        }
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "null"})
    public void updateUsers() {
        org.esme.broker.AppBroker.sevasLogger.info("Updating users...");
        @SuppressWarnings("Convert2Diamond")
        ArrayList<Users> usersCache = new ArrayList<Users>();
        Statement statement = null;
        ResultSet rs = null;
        Users users;

        try {

            statement = getDBConnection().createStatement();
            rs = statement.executeQuery(getServletContext().getInitParameter("usersQuery"));
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                //update previously stored job contents from the container...
                while (rs.next()) {
                    users = new Users();
                    users.setId(rs.getInt("id"));
                    users.setUserName(rs.getString("username"));
                    users.setPassword(rs.getString("password"));
                    usersCache.add(users);
                }
                //Create cache of all users
                org.esme.broker.AppBroker.sevasLogger.info("Total Users " + usersCache.size());
                getServletContext().setAttribute("usersCache", usersCache);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                rs.close();
            } catch (SQLException ex) {
            }
        }
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "null"})
    public void updateContentCache() {

        org.esme.broker.AppBroker.sevasLogger.info("Updating content archive...");
        @SuppressWarnings("Convert2Diamond")
        ArrayList<Content> contentCache = new ArrayList<Content>();
        Content content;
        Statement statement = null;
        ResultSet resultSet = null;

        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(
                    getServletContext().getInitParameter("contentQuery"));

            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                while (resultSet.next()) {
                    try {
                        content = new Content();
                        content.setContent_id(resultSet.getInt("content_id"));
                        content.setContentServiceName(resultSet.getString("service_name"));
                        content.setContent(resultSet.getString("content"));
                        content.setDateUploaded(resultSet.getString("content_upload_date"));
                        content.setContentOwner(resultSet.getInt("content_provider_id"));
                        content.setContentService(resultSet.getLong("content_service_id"));
                        content.setConentDOB(resultSet.getString("content_date_of_blast"));
                        contentCache.add(content);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //Create cache of content with all
                getServletContext().setAttribute("totalContents ", contentCache.size());
                getServletContext().setAttribute("contentCache", contentCache);
            }

        } catch (Exception ex) {
            org.esme.broker.AppBroker.sevasLogger.info("Error while updating active content cache.");
            ex.printStackTrace();

        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
