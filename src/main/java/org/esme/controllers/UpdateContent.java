package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.Content;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class UpdateContent extends AppBroker {

    PreparedStatement pstmt;
    String[] delete;

    @SuppressWarnings({"CallToThreadDumpStack", "UseSpecificCatch", "CallToPrintStackTrace"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        delete = request.getParameterValues("check");
        try {

            try {
                //For content deletion...
                if (delete.length > 0) {
                    int counter = 0;
                    for (int index = 0; index < delete.length; index++) {
                        //Update the database
                        try {

                            pstmt = getDBConnection().prepareStatement(
                                    "delete from content where content_id = ? ");
                            pstmt.setLong(1, Integer.parseInt(delete[index]));
                            int update = pstmt.executeUpdate();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        counter++;
                    }

                    //We refereh content cache here
                    updateContentCache();
                    request.setAttribute(
                            "message",
                            counter + " contents successfully deleted.");
                    request.getRequestDispatcher(
                            "content_mgr.jsp").forward(request, response);
                    return;
                }
            } catch (Exception e) {
            }

            //For content update
            long id = Long.parseLong(request.getParameter("id"));
            String content = removePoison(request.getParameter("con"));
            String dateOfBlast = removePoison(request.getParameter("date"));
            String service = request.getParameter("cs");

            try {

                pstmt = getDBConnection().prepareStatement(
                        "update content set content = ?, content_date_of_blast = ? where content_id = ? ");
                pstmt.setString(1, formatText(content));
                pstmt.setString(2, dateOfBlast);
                pstmt.setLong(3, id);
                int update = pstmt.executeUpdate();
                if (update > 0) {
                    request.setAttribute(
                            "message",
                            "Content successfully updated.");
                    request.getRequestDispatcher(
                            "content_mgr.jsp?id=" + id + "&cs=" + service + "&dob=" + dateOfBlast + ""
                                    + "&con=" + content + " ").forward(request, response);
                } else {

                    request.setAttribute("message", "No content was updated.");
                    request.getRequestDispatcher(
                            "content_mgr.jsp?id=" + id + "&cs=" + service + "&dob=" + dateOfBlast + ""
                                    + "&con=" + content + "").forward(request, response);
                }
                //Update the archive...
                updateContentCache();
            } catch (SQLException e) {
                e.printStackTrace();
                request.setAttribute(
                        "message", "SEVERE: DB eror occured while trying to update your content.");
                request.getRequestDispatcher(
                        "content_mgr.jsp?id=" + id + "&cs=" + service + "&dob=" + dateOfBlast + ""
                                + "&con=" + content + "").forward(request, response);
            }
        } catch (Exception e) {
        } finally {
            try {
                pstmt.close();
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "null"})
    public void updateContentCache() {

        org.esme.broker.AppBroker.sevasLogger.info("Updating content archive...");
        ArrayList<Content> contentCache = new ArrayList<Content>();
        Statement statement = null;
        ResultSet resultSet = null;
        Content content;
        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(
                    getServletContext().getInitParameter("contentQuery"));

            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                while (resultSet.next()) {
                    try {
                        content = new Content();
                        content.setContent_id(resultSet.getInt("content_id"));
                        content.setContentServiceName(resultSet.getString("service_name"));
                        content.setContent(resultSet.getString("content"));
                        content.setDateUploaded(resultSet.getString("content_upload_date"));
                        content.setContentOwner(resultSet.getInt("content_provider_id"));
                        content.setContentService(resultSet.getLong("content_service_id"));
                        content.setConentDOB(resultSet.getString("content_date_of_blast"));
                        contentCache.add(content);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //Create cache of content with all
                getServletContext().setAttribute("totalContents ", contentCache.size());
                getServletContext().setAttribute("contentCache", contentCache);
            }

        } catch (Exception ex) {
            org.esme.broker.AppBroker.sevasLogger.info("Error while updating active content cache.");
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }
}
