package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.InstantContentBroadCaster;
import org.esme.dao.models.Content;
import org.esme.dao.models.InstantContentDAO;
import org.esme.dao.models.Users;
import org.esme.dto.Service;
import org.esme.utils.pingenerator.RandomPinGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class InstantContent extends AppBroker {

    PreparedStatement pstmt;
    String serviceShortCode, serviceNetwork,
            serviceName, theContent, subType, timeToSend, serviceAmount;
    long serviceID, servicePeriod;
    String smscID;
    PreparedStatement ps = null;

    ArrayList<Service> serviceCache;
    ArrayList<InstantContentDAO> instantContentDAOList;
    InstantContentDAO instantContentDAO;
    InstantContentBroadCaster instantContentBroadCaster;

    @SuppressWarnings({"CallToThreadDumpStack", "empty-statement", "CallToPrintStackTrace", "UseSpecificCatch", "unchecked", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        HttpSession session = request.getSession(true);
        String user = (String) session.getAttribute("userName");
        try {
            serviceID = Long.parseLong(request.getParameter(removePoison("serv")));
            theContent = request.getParameter(removePoison("sd"));
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }

        try {
            //retrieve the service properties...
            if (serviceCache == null || serviceCache.size() < 1) {
                updateServicesCache();
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            }

            for (Service nextService : serviceCache) {

                if (nextService.getServiceID() == serviceID) {

                    serviceName = nextService.getServiceName();
                    subType = nextService.getServiceSubType();
                    serviceAmount = nextService.getServicePrice();
                    serviceShortCode = nextService.getServiceMTShortCode();
                    serviceNetwork = nextService.getServiceNetwork();
                    servicePeriod = Long.parseLong(nextService.getServicePeriod());
                    //   
                    long contentID = generateID();
                    try {
                        //Log the content
                        String date = formatDateToString(new java.util.Date());
                        ps = getDBConnection().prepareStatement(
                                "insert into content(content_id, content_provider_id,"
                                        + "content_service_id,content,content_date_of_blast)"
                                        + "values(?,?,?,?,?)");
                        ps.setLong(1, contentID);
                        ps.setLong(2, getUserID(user));
                        ps.setLong(3, serviceID);
                        ps.setString(4, formatText(theContent));
                        ps.setString(5, date);

                        int update = ps.executeUpdate();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //Update content catche...
                    try {
                        updateContentCache();
                    } catch (Exception e) {

                    }
                    instantContentBroadCaster = new InstantContentBroadCaster(nextService, theContent, getDBConnection());
                    instantContentBroadCaster.sendContent();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    //Get user ID usersCache
    private long getUserID(String userName) {
        long userID = 0;
        @SuppressWarnings("unchecked")
        ArrayList<Users> userCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
        for (Users user : userCache) {
            if (user.getUserName().equals(userName)) {
                userID = user.getId();
            }
        }
        return userID;
    }

    public long generateID() {
        long theID = RandomPinGenerator.randomPinGenerator(9);
        return theID;
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace"})
    public void updateContentCache() {

        org.esme.broker.AppBroker.sevasLogger.info("Updating content archive...");
        ArrayList<Content> contentCache = new ArrayList<Content>();
        Statement statement = null;
        ResultSet resultSet = null;
        Content content;
        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(
                    getServletContext().getInitParameter("contentQuery"));

            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                while (resultSet.next()) {
                    try {
                        content = new Content();
                        content.setContent_id(resultSet.getInt("content_id"));
                        content.setContentServiceName(resultSet.getString("service_name"));
                        content.setContent(resultSet.getString("content"));
                        content.setDateUploaded(resultSet.getString("content_upload_date"));
                        content.setContentOwner(resultSet.getInt("content_provider_id"));
                        content.setContentService(resultSet.getLong("content_service_id"));
                        content.setConentDOB(resultSet.getString("content_date_of_blast"));
                        contentCache.add(content);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //Create cache of content with all
                getServletContext().setAttribute("totalContents ", contentCache.size());
                getServletContext().setAttribute("contentCache", contentCache);
            }

        } catch (Exception ex) {
            org.esme.broker.AppBroker.sevasLogger.info("Error while updating active content cache.");
            ex.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException ex) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException ex) {
            }
        }
    }
}
