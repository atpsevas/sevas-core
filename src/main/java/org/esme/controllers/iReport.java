/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.controllers;

import org.apache.log4j.Logger;
import org.esme.broker.AppBroker;
import org.esme.dao.models.Users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

@SuppressWarnings("serial")
public class iReport extends AppBroker {

    private static final Logger logger = Logger.getLogger(iReport.class);
    Connection dbConnection = null;
    long monthTargetRev;
    PreparedStatement statement;
    ResultSet resultSet;
    private long inceptionTillDateRev;
    private long extraPolatedRev;
    private long todayRev;
    private long dailyAverageRev;
    private double dailyChurn;
    private long actualRev;
    private double revenueVariance;

    public iReport() {
        if (dbConnection == null) {
            try {
                dbConnection = getDBConnection(getPropertyFileHandle());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    static public String customFormat(long value) {
        DecimalFormat myFormatter = new DecimalFormat("###,###");
        return myFormatter.format(value);
    }

    @SuppressWarnings({"CallToPrintStackTrace", "UseSpecificCatch"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        Calendar cal = Calendar.getInstance();
        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        int totalDayofMonth = cal.getMaximum(Calendar.DAY_OF_MONTH);
        org.esme.broker.AppBroker.sevasLogger.info("DAYS IN MONTH" + totalDayofMonth);

        try {

            HttpSession sess = request.getSession(true);
            String user = (String) sess.getAttribute("userName");
            long userID = 0;
            String userRole = (String) sess.getAttribute("UserRole");

            @SuppressWarnings("unchecked")
            ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
            for (Users us : usersCache) {
                if (us.getUserName().equals(user)) {
                    userID = us.getId();
                    break;
                }
            }


            try {
                monthTargetRev = Long.parseLong(monthlyRevTarget_PROP);

//                if (userRole.equals("Admin")) {
//                    // monthTargetRev = (Long) getServletContext().getAttribute("monthlyRevTarget");
//                    monthTargetRev = Long.parseLong(monthlyRevTarget_PROP);
//                } else {
//                    monthTargetRev = getRevTarget(getServletContext().getInitParameter("monthlyRevTarget"));
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (userRole.equals("Admin")) {
//                    inceptionTillDateRev = (Long) getServletContext().getAttribute("incepTillDateRev");
                    inceptionTillDateRev = getRevenueData("incepTillDateRev");
                } else {
                    inceptionTillDateRev = getPeriodicRev(
                            "select sum((CASE WHEN ((b.sender = a.welcome_shortcode) "
                                    + "and ((b.src_module = 'NewSubscription' and a.service_promo = 'no') "
                                    + "or b.src_module = 'SubscriptionRenewal'  or "
                                    + "b.src_module ilike '%SubscriptionRenewalRetry%' )) THEN 1 ELSE 0 END) * a.service_price) AS main_charge,\n"
                                    + " sum((CASE WHEN (b.sender = a.fallback_shortcode) and ((b.src_module = 'NewSubscription' "
                                    + "and a.service_promo = 'no') or b.src_module = 'SubscriptionRenewal' or "
                                    + "b.src_module ilike '%SubscriptionRenewalRetry%' ) THEN 1 ELSE 0 END) * a.fallback_price) AS fallback_charge,\n "
                                    + "sum((CASE WHEN (b.sender = a.fallback2_shortcode) "
                                    + "and ((b.src_module = 'NewSubscription' and a.service_promo = 'no') or b.src_module = 'SubscriptionRenewal' or "
                                    + "b.src_module ilike '%SubscriptionRenewalRetry2%' ) THEN 1 ELSE 0 END) * a.fallback2_price::bigint) AS fallback_charge_2"
                                    + " FROM subscription_service a, delivered_outgoing_messages b "
                                    + " where a.service_owner = " + userID + " and a.id = b.serv_id");
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                if (userRole.equals("Admin")) {
//                    actualRev = (Long) getServletContext().getAttribute("monthTillDateRev");
                    actualRev = getRevenueData("monthTillDateRev");
                } else {
                    actualRev = getPeriodicRev(
                            "select sum((CASE WHEN (b.sender = a.welcome_shortcode and "
                                    + "(b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal'  "
                                    + "or b.src_module ilike '%SubscriptionRenewalRetry%' )) THEN 1 ELSE 0 END) * a.service_price) AS main_charge,\n "
                                    + "sum((CASE WHEN (b.sender = a.fallback_shortcode  and "
                                    + "(b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal' "
                                    + "or b.src_module ilike '%SubscriptionRenewalRetry%' )) THEN 1 ELSE 0 END) * a.fallback_price) AS fallback_charge,\n "
                                    + "sum((CASE WHEN (b.sender = a.fallback2_shortcode  and "
                                    + "(b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal' "
                                    + "or b.src_module ilike '%SubscriptionRenewalRetry2%' )) THEN 1 ELSE 0 END) * a.fallback2_price::bigint) AS fallback_charge_2\n "
                                    + " FROM subscription_service a, delivered_outgoing_messages b\n"
                                    + " where extract(month from b.sent_time) = extract (month from now()) \n"
                                    + " and extract(year from b.sent_time) = extract (year from now())\n"
                                    + " and a.service_owner = " + userID + " and a.id = b.serv_id");
                }
            } catch (Exception e) {

            }

            try {
                dailyAverageRev = actualRev / dayOfMonth;
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (userRole.equals("Admin")) {
                    todayRev = getRevenueData("todayRev");
                } else {
                    todayRev = getPeriodicRev("select sum((CASE WHEN ((b.sender = a.welcome_shortcode) and "
                            + "(b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal'  "
                            + "or b.src_module ilike '%SubscriptionRenewalRetry%' )) THEN 1 ELSE 0 END) * a.service_price) AS main_charge,\n"
                            + "sum((CASE WHEN (b.sender = a.fallback_shortcode) and "
                            + "(b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal' "
                            + "or b.src_module ilike '%SubscriptionRenewalRetry%' ) THEN 1 ELSE 0 END) * a.fallback_price) AS fallback_charge, \n"
                            + "sum((CASE WHEN (b.sender = a.fallback2_shortcode) and "
                            + "(b.src_module = 'NewSubscription' or b.src_module = 'SubscriptionRenewal' "
                            + "or b.src_module ilike '%SubscriptionRenewalRetry2%' ) THEN 1 ELSE 0 END) * a.fallback2_price::bigint) AS fallback_charge_2 \n"
                            + "FROM subscription_service a, delivered_outgoing_messages b \n"
                            + "where date(b.sent_time) = date(CURRENT_TIMESTAMP) \n"
                            + "and a.service_owner = " + userID + " and a.id = b.serv_id");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                extraPolatedRev = dailyAverageRev * totalDayofMonth;
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (userRole.equals("Admin")) {
                dailyChurn = (Double) getServletConfig().getServletContext().getAttribute("churnQuery");
            } else {
                dailyChurn = getChurnRate("select count(*) as total,\n"
                        + "            sum(CASE WHEN(sub_status = 'inactive') THEN 1 ELSE 0 END) as inactive\n"
                        + "            from subscription  \n"
                        + "            where extract(month from sub_request_time) = extract (month from now()) \n"
                        + "            and extract(year from sub_request_time) = extract (year from now()) "
                        + "and sub_service_owner_id = " + userID + " ");
            }

            //([‘actualrevenue'] - [‘forecastrevenue']) / [‘forecastrevenue'] * 100
            try {
                revenueVariance = 0; //((actualRev - monthTargetRev) * 100) / monthTargetRev;
            } catch (Exception e) {
                e.printStackTrace();
            }

            request.setAttribute("message", request.getAttribute("message"));
            request.getRequestDispatcher(
                    "report.jsp?"
                            + "inceptionTillDateRev=" + customFormat(inceptionTillDateRev) + ""
                            + "&extraPolatedRev=" + customFormat(extraPolatedRev) + ""
                            + "&dailyAverageRev=" + customFormat(dailyAverageRev) + ""
                            + "&todayRev=" + customFormat(todayRev) + ""
                            + "&dailyChurn=" + dailyChurn + ""
                            + "&monthTargetRev=" + customFormat(monthTargetRev) + ""
                            + "&actualRev=" + customFormat(actualRev) + ""
                            + "&revenueVariance=" + revenueVariance + " "
                            + "").forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    public long getRevenueData(String label) throws SQLException {
        Long revData = null;

        try {
            revData = (Long) getServletContext().getAttribute(label + "DATA");
            if (revData == null) {
                org.esme.broker.AppBroker.sevasLogger.info(label + "DATA not set in Context");

                // Get query from context
                String query = (String) getServletContext().getAttribute(label);
                long _revData = getPeriodicRev(query);
                revData = (Long) _revData;

                // After Retreiving save to Cache
                // NB: Primitive Data Type is saved
                getServletContext().setAttribute(label + "DATA", _revData);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        org.esme.broker.AppBroker.sevasLogger.info("Label: " + revData);
        return revData;
    }

    public long getRevTarget(String query) throws SQLException {

        statement = dbConnection.prepareStatement(query);
        resultSet = statement.executeQuery();
        long returnedRevenue = 0;
        while (resultSet.next()) {
            returnedRevenue = resultSet.getLong("theTarget");
        }
        return returnedRevenue;
    }

    public long getPeriodicRev(String query) throws SQLException {
        statement = dbConnection.prepareStatement(query);
        resultSet = statement.executeQuery();
        long returnedRevenue = 0;
        while (resultSet.next()) {
            returnedRevenue = resultSet.getLong("main_charge") + resultSet.getLong("fallback_charge") + resultSet.getLong("fallback_charge_2");
        }
        return returnedRevenue;
    }

    public double getChurnRate(String query) throws SQLException {

        statement = dbConnection.prepareStatement(query);
        resultSet = statement.executeQuery();
        double churn = 0;
        long totalNewSub = 0;
        long totalInactiveSub = 0;

        while (resultSet.next()) {
            totalNewSub = resultSet.getLong("total");
            totalInactiveSub = resultSet.getLong("inactive");
        }

        try {
            churn = (totalInactiveSub / totalNewSub) * totalNewSub;//(resultSet.getLong("total") - resultSet.getLong("active")) / resultSet.getLong("total");
        } catch (Exception e) {

        }
        DecimalFormat df = new DecimalFormat("#.#");
        df.format(churn);
        return Double.parseDouble(df.format(churn));
    }

}
