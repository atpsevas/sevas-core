package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.LogMTBillingResponse;
import org.esme.controllers.helpers.UpdateSubscription;
import org.esme.dto.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

@SuppressWarnings("serial")
public class NODLRMTBillingResponseCallback extends AppBroker {

    Connection dbConnection;

    @SuppressWarnings({"CallToPrintStackTrace", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        long serviceID = 0;
        Service service = null;
        long pid;
        long epochTime;
        Properties prop = null;

        //PreparedStatement pstmt;
        ArrayList<Service> serviceCache = null;
        LogMTBillingResponse logMTBillingResponse;
        String serviceBillingShortCode = null,
                serviceSubscriber = null,
                serviceMessageStatus = null,
                serviceMessageSMSC = null,
                serviceMessageModule = null,
                serviceMessage = null,
                serviceMessageID = null;

        try {

            try {

                serviceID = Long.parseLong(request.getParameter("servid").trim());
                serviceBillingShortCode = request.getParameter("shortcode");
                serviceSubscriber = request.getParameter("msisdn");
                serviceMessageStatus = request.getParameter("status");
                serviceMessageSMSC = request.getParameter("smsc");
                serviceMessage = request.getParameter("msg");
                serviceMessageID = generateMessageID();

                org.esme.broker.AppBroker.sevasLogger.info(serviceID);
                org.esme.broker.AppBroker.sevasLogger.info(serviceBillingShortCode);
                org.esme.broker.AppBroker.sevasLogger.info(serviceSubscriber);
                org.esme.broker.AppBroker.sevasLogger.info(serviceMessageStatus);
                org.esme.broker.AppBroker.sevasLogger.info(serviceMessageSMSC);
                org.esme.broker.AppBroker.sevasLogger.info(serviceMessage);
                org.esme.broker.AppBroker.sevasLogger.info(serviceMessageID);

                //Logger 
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                //Access cached services...
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            } catch (Exception e) {
                updateServicesCache();
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            }

            for (Service nextService : serviceCache) {
                service = nextService;
                if (service.getServiceID() == serviceID) {
                    if (serviceBillingShortCode.equalsIgnoreCase(nextService.getServiceBillingShortCode())) {
                        serviceMessageModule = "SubscriptionRenewal";
                    } else if (serviceBillingShortCode.equalsIgnoreCase(nextService.getFallbackShortCode())) {
                        serviceMessageModule = "SubscriptionRenewalRetry";
                    }
                    break;
                }
            }

            if ((serviceMessageStatus.trim().equalsIgnoreCase("success"))) {

                org.esme.broker.AppBroker.sevasLogger.info("DLR RECEIVED " + serviceMessageStatus);
                //Log it....DailyPaidContent
                org.esme.broker.AppBroker.sevasLogger.info("Logging to delivered table....");

                try {

                    logMTBillingResponse = new LogMTBillingResponse();
                    logMTBillingResponse.setMessageID(serviceMessageID);
                    logMTBillingResponse.setSenderID(serviceBillingShortCode);
                    logMTBillingResponse.setRecipientMSISDN(serviceSubscriber);
                    logMTBillingResponse.setSmsc(serviceMessageSMSC);
                    logMTBillingResponse.setMessage(serviceMessage);
                    logMTBillingResponse.setDelivertStatus(serviceMessageStatus);
                    logMTBillingResponse.setServName(service.getServiceName());
                    logMTBillingResponse.setSrcModule(serviceMessageModule);
                    logMTBillingResponse.setSmsc(serviceMessageSMSC);
                    logMTBillingResponse.setServiceID(serviceID);
                    logMTBillingResponse.setSubscriptionBillingType(service.getServiceSubType());
                    logMTBillingResponse.billing_outgoing_messages(logMTBillingResponse);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    //Send to help class for process...
                    new UpdateSubscription().
                            updateUserSubscription(getDBConnection(),
                                    service, serviceBillingShortCode, serviceSubscriber,
                                    serviceMessageModule, "success");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //If new subscription...Let him get content....
                if (serviceMessageModule.trim().equalsIgnoreCase("NewSubscription")) {
                    if (getCurrentHour() >= Long.parseLong(service.getTimeOfBlast())) {
                        //We dont want to send content for airtel new subscription until they send OK for confirmation
                        if (service.getServiceNetwork().equalsIgnoreCase("airtel")
                                && serviceMessageModule.trim().equalsIgnoreCase("NewSubscription")) {
                            return;
                        }
                        sendFirstMTContent(service, serviceSubscriber);
                    }
                }

                //We want to try fall backs immediately...
            } else if (service.getServiceSubType().equalsIgnoreCase("MT")
                    && serviceBillingShortCode.equalsIgnoreCase(service.getServiceBillingShortCode())) {

                try {
                    //Send to help class for process...
                    new UpdateSubscription().
                            updateUserSubscription(getDBConnection(),
                                    service, serviceBillingShortCode, serviceSubscriber,
                                    serviceMessageModule, "failed");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                org.esme.broker.AppBroker.sevasLogger.info("DLR RECEIVED " + serviceMessageStatus);

                if (Long.parseLong(service.getServicePeriod()) > 1
                        && service.getInstantBillingRetry().equalsIgnoreCase("yes")) {

                    if (Long.parseLong(service.getServicePeriod()) > 1
                            && service.getInstantBillingRetry().equalsIgnoreCase("yes")) {

                        prop = AppBroker.getPropertyFileHandle();

                        org.esme.broker.AppBroker.sevasLogger.info("Retrying fallbacks....");

                        try {

                            PreparedStatement ps = null;
                            dbConnection = getDBConnection();

                            ps = dbConnection.prepareStatement(
                                    "INSERT INTO send_sms (momt, sender, receiver, "
                                            + "msgdata, smsc_id, sms_type, boxc_id,time, pid,service) "
                                            + "VALUES (?,?,?,?,?,?,?,?,?,?)");

                            pid = 64;
                            //We want messages visible to telco staff...
                            if (service.getMainbillingnotifySMSSetting().equalsIgnoreCase("yes")
                                    || serviceSubscriber.contains("9944")) {
                                pid = 127;
                            }

                            epochTime = System.currentTimeMillis() / 1000;
                            ps.setString(1, service.getServiceSubType());
                            ps.setString(2, service.getFallbackShortCode());
                            ps.setString(3, serviceSubscriber);
                            ps.setString(4, formatText(service.getServiceFallbackRenewalMessage()));
                            ps.setString(5, service.getServiceBillingNetwork());
                            ps.setLong(6, 2);
                            ps.setString(7, prop.getProperty("billingSMSBoxID"));
                            ps.setLong(8, epochTime);
                            ps.setLong(9, pid);
                            // String servID = Long.toString(nextService.getServiceID());
                            ps.setLong(10, service.getServiceID());
                            ps.executeUpdate();

                        } catch (SQLException | NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else if (serviceBillingShortCode.equalsIgnoreCase(service.getServiceBillingShortCode())) {
                try {
                    //update failed fall back attempt...
                    new UpdateSubscription().
                            updateUserSubscription(getDBConnection(),
                                    service, serviceBillingShortCode, serviceSubscriber,
                                    serviceMessageModule, "failed");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

}
