package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.SubscriptionLogs;
import org.esme.dao.models.Users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;

@SuppressWarnings("serial")
public class LogsCSVDownload extends AppBroker {

    PrintWriter w;
    String startDate;
    String endDate;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/csv");
        PrintWriter out = response.getWriter();

        HttpSession sess = request.getSession(true);
        String user = (String) sess.getAttribute("userName");
        long userID = 0;
        String userRole = (String) sess.getAttribute("UserRole");

        @SuppressWarnings("unchecked")
        ArrayList<Users> usersCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
        for (Users us : usersCache) {
            if (us.getUserName().equals(user)) {
                userID = us.getId();
                break;
            }
        }

        try {
            w = response.getWriter();
            try {
                startDate = removePoison(request.getParameter("start"));
                endDate = removePoison(request.getParameter("end"));
            } catch (Exception e) {
            }
            ArrayList<SubscriptionLogs> logs;
            if (userRole.equals("Admin")) {
                if (startDate != null && endDate != null) {
                    logs = new SubscriptionLogs().customQueryLogs(getDBConnection(), startDate, endDate, 0);
                } else {
                    logs = new SubscriptionLogs().queryLogs(getDBConnection(), 0);
                }
            } else {
                if (startDate != null && endDate != null) {
                    logs = new SubscriptionLogs().customQueryLogs(getDBConnection(), startDate, endDate, userID);
                } else {
                    logs = new SubscriptionLogs().queryLogs(getDBConnection(), userID);
                }
            }
            generateCsvFile("SEVAS_" + formatDateToString(new Date()) + ".csv", logs);
            w.flush();
            w.close();
        } finally {
            out.close();
        }
    }

    public void generateCsvFile(String fileName, ArrayList<SubscriptionLogs> logs) {

        try {
            w.append("Request Time");
            w.append(',');
            w.append("Service Name");
            w.append(',');
            w.append("Network");
            w.append(',');
            w.append("Short Code");
            w.append(',');
            w.append("Subscriber");
            w.append(',');
            w.append("Keyword");
            w.append(',');
            w.append("Response");
            w.append('\n');

            for (SubscriptionLogs log : logs) {
                w.append("." + log.getRequestTime().substring(0, log.getRequestTime().lastIndexOf(".")));
                w.append(',');
                w.append(log.getService());
                w.append(',');
                w.append(log.getNetwork());
                w.append(',');
                w.append(log.getShortCode());
                w.append(',');
                w.append("_" + log.getMsisdn());
                w.append(',');
                w.append(log.getRequestText());
                w.append(',');
                w.append(log.getResponseText());
                w.append('\n');
            }

            w.flush();
            w.close();

        } catch (Exception e) {
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
