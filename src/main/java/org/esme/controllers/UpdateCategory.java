package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dao.models.Network;
import org.esme.dao.models.ServiceCategory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class UpdateCategory extends AppBroker {

    PreparedStatement pstmt;
    Network theNetwork;
    ArrayList<Network> networkCache;

    @SuppressWarnings({"CallToThreadDumpStack", "UseSpecificCatch", "CallToPrintStackTrace"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();


        String action = removePoison(request.getParameter("action"));
        String categoryID = removePoison(request.getParameter("categoryID"));
        String categoryName = removePoison(request.getParameter("catName"));

        if (action.equalsIgnoreCase("delete")) {
            try {

                pstmt = getDBConnection().prepareStatement(
                        "delete from service_category where id ilike ? ");
                pstmt.setString(1, categoryID);
                int update = pstmt.executeUpdate();
                if (update > 0) {
                    //update category cache
                    updateCategory();
                    request.setAttribute(
                            "message",
                            categoryID + " successfully deleted.");
                    request.getRequestDispatcher("new_serv_category.jsp?categoryID=" + categoryID).forward(request, response);
                } else {
                    request.setAttribute(
                            "message",
                            "No category was deleted.");
                    request.getRequestDispatcher(
                            "new_serv_category.jsp?categoryID=" + categoryID).forward(request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
                request.setAttribute(
                        "message",
                        "An error occured while trying to delete " + categoryID);
                request.getRequestDispatcher("new_serv_category.jsp?categoryID=" + categoryID).forward(request, response);
            } finally {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                }
            }

        } else if (action.equalsIgnoreCase("update")) {
            String categoryKeyword = request.getParameter("catKeyword");
            try {

                pstmt = getDBConnection().prepareStatement(
                        "update service_category set category_keyword = ? where id = ?");
                pstmt.setString(1, categoryKeyword);
                pstmt.setString(2, categoryID.toLowerCase());
                int update = pstmt.executeUpdate();
                if (update > 0) {
                    //Update network cache..
                    updateCategory();
                    request.setAttribute(
                            "message",
                            "Category successfully updated.");
                    request.getRequestDispatcher(
                            "new_serv_category.jsp?categoryID=" + categoryID).forward(request, response);
                } else {
                    request.setAttribute("message", "No category was updated.");
                    request.getRequestDispatcher(
                            "new_serv_category.jsp?categoryID=" + categoryID).forward(request, response);
                }
            } catch (SQLException e) {
                e.printStackTrace();
                request.setAttribute(
                        "message", "SEVERE: DB eror occured while trying to update " + categoryID);
                request.getRequestDispatcher(
                        "new_serv_category.jsp?categoryID=" + categoryID).forward(request, response);
            } finally {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                }
            }
        }

    }

    @SuppressWarnings({"CallToThreadDumpStack", "Convert2Diamond", "null"})
    public void updateCategory() {

        org.esme.broker.AppBroker.sevasLogger.info("Updating Category..");
        ArrayList<ServiceCategory> serviceCategoryCache = new ArrayList<ServiceCategory>();
        Statement statement = null;
        ResultSet resultSet = null;

        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(getServletContext().getInitParameter("categoryQuery"));
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {

                //update previously stored job contents from the container...
                while (resultSet.next()) {

                    ServiceCategory serviceCategory = new ServiceCategory();
                    serviceCategory.setCategoryID(resultSet.getLong("id"));
                    serviceCategory.setCategoryName(resultSet.getString("category_name"));
                    serviceCategory.setCategoryKeyword(resultSet.getString("category_keyword"));
                    serviceCategory.setCategoryDateCreated(resultSet.getString("date_created"));
                    serviceCategoryCache.add(serviceCategory);
                }
                //Create cache of all users
                org.esme.broker.AppBroker.sevasLogger.info("Total Category " + serviceCategoryCache.size());
                getServletContext().setAttribute("categoryCache", serviceCategoryCache);
            }

        } catch (Exception ex) {
            org.esme.broker.AppBroker.sevasLogger.info(ex.getMessage());
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
