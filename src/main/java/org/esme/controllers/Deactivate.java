package org.esme.controllers;

import org.esme.broker.AppBroker;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

@SuppressWarnings("serial")
public class Deactivate extends AppBroker {

    String msisdn, network;
    PreparedStatement pstmt, ps;

    @SuppressWarnings("UseSpecificCatch")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        msisdn = removePoison(request.getParameter("msisdn"));

        try {
            network = removePoison(request.getParameter("network"));
        } catch (Exception e) {

        }

        String theNumber = msisdn.substring(msisdn.length() - 9);

        org.esme.broker.AppBroker.sevasLogger.info("WE CALLED DEAC");

        try {

            ps = getDBConnection().prepareStatement(
                    "update subscription set blacklisted = 'blacklisted', "
                            + "blacklisted_date = CURRENT_TIMESTAMP, "
                            + "un_sub_date = CURRENT_TIMESTAMP, "
                            + "un_sub_source = 'WEB', sub_status = 'inactive' "
                            + "where sub_msisdn like '%" + theNumber + "' ");
            ps.executeUpdate();

            if (network.equalsIgnoreCase("mtn")) {

                BufferedReader in;
                URLConnection yc;
                URL sdpUnSubURL;
                Statement st = getDBConnection().createStatement();
                ResultSet rs = st.executeQuery(
                        "select a.service_keyword as service_keyword, "
                                + "b.sub_msisdn from subscription_service a, "
                                + "subscription b where sub_msisdn ilike '%" + theNumber + "' "
                                + "and b.sub_network ilike '%mtn%'  and b.sub_status = 'active'"
                                + "and a.id = b.sub_service_id");

                while (rs.next()) {

                    sdpUnSubURL = new URL(
                            "http://localhost:8585/sevas/SDPWebUnSubscription?productID="
                                    + rs.getString("service_keyword") + "&subscriber=" + theNumber);

                    org.esme.broker.AppBroker.sevasLogger.error(sdpUnSubURL);
                    yc = sdpUnSubURL.openConnection();
                    in = new BufferedReader(new InputStreamReader(
                            yc.getInputStream()));

                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        org.esme.broker.AppBroker.sevasLogger.info(inputLine);
                    }
                }

                request.setAttribute("message", theNumber + " has been successfully processed");
                request.getRequestDispatcher("deactivate_mtn.jsp?msisdn=" + msisdn).forward(request, response);
                return;
            }

            request.setAttribute("message", theNumber + " has been successfully processed");
            request.getRequestDispatcher("deactivate.jsp?msisdn=" + msisdn).forward(request, response);

            // }
        } catch (Exception e) {
            request.setAttribute("message", "Operation failed. Please try again later.");
            request.getRequestDispatcher("deactivate.jsp?msisdn=" + msisdn).forward(request, response);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
