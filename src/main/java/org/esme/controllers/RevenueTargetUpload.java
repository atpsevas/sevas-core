package org.esme.controllers;

import com.csvreader.CsvReader;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.esme.broker.AppBroker;
import org.esme.dao.models.Content;
import org.esme.dao.models.Users;
import org.esme.dto.Service;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class RevenueTargetUpload extends AppBroker {

    PreparedStatement pstmt;
    File uploadFile = null;
    long serviceID;
    ArrayList<Service> servicesCache;
    ArrayList<Content> contentCache;
    Content content;
    ArrayList<Users> userCache;
    String user;
    String userRole;

    @SuppressWarnings({"CallToThreadDumpStack", "unchecked", "CallToPrintStackTrace"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        ServletContext ctx = getServletContext();
        HttpSession session = request.getSession(true);

        user = (String) session.getAttribute("userName");
        userRole = (String) session.getAttribute("UserRole");

        //Get cached services, users, and contents
        servicesCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
        userCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
        contentCache = (ArrayList<Content>) getServletContext().getAttribute("contentCache");

        //get the file using apache commons.
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setRepository(new File(getServletContext().getRealPath("/")));
        ServletFileUpload upload = new ServletFileUpload(factory);
        List<FileItem> items;

        try {
            items = upload.parseRequest(request);
        } catch (FileUploadException ex) {
            ex.printStackTrace();
            request.setAttribute("message", "ERROR:"
                    + "Unable to upload the file specified! Check and try again.");
            request.getRequestDispatcher("./iReport.do").forward(request, response);
            return;
        }

        for (FileItem item : items) {
            if (item.isFormField()) {
                //processFormField(item);
                if (item.getFieldName().equalsIgnoreCase("service")) {
                    //Acquire service ID
                    serviceID = Long.parseLong(item.getString());
                }
            } else {
                //processUploadedFile(item);
                String filename = item.getName();
                File f = new File(filename);
                filename = f.getName();
                // get rid of spaces in filename.
                filename = filename.replace(" ", "_");

                if (!(filename.endsWith(".csv"))) {
                    request.setAttribute("message", "ERROR:"
                            + "You have selected an invalid file type!");
                    request.getRequestDispatcher("./iReport.do").forward(request, response);
                    return;
                } else {
                    //Saving the File in the application root folder
                    uploadFile = new File(ctx.getInitParameter("revenueFileDir") + filename);
                    try {
                        item.write((uploadFile));
                    } catch (Exception ex) {
                        request.setAttribute("message", "SEVERE: Error while uploading your file. Please try again");
                        request.getRequestDispatcher("./iReport.do").forward(request, response);
                        ex.printStackTrace();
                        return;
                    }
                }
            }
        }
        try {

            CsvReader reader = new CsvReader(uploadFile.getAbsolutePath());
            reader.readHeaders();
            while (reader.readRecord()) {

                try {
//                    //Check to see that the revenue target isn't already exist.
//                    Statement st = getDBConnection().createStatement();
//                    ResultSet rs = st.executeQuery(
//                            "select * from monthly_revenue_target  \n"
//                            + "where service_id = " + serviceID + " "
//                            + "and the_year = " + reader.get(0) + " "
//                            + "and the_month = " + reader.get(1) + " ");
//                    while (!rs.next()) {
                    try {
                        pstmt = getDBConnection().prepareStatement(
                                "insert into monthly_revenue_target"
                                        + " (service_id,the_year,the_month,"
                                        + "target_revenue)"
                                        + "values(?,?,?,?)");
                        pstmt.setLong(1, serviceID);
                        pstmt.setLong(2, Long.parseLong(reader.get(0)));
                        pstmt.setLong(3, Long.parseLong(reader.get(1)));
                        pstmt.setLong(4, Long.parseLong(reader.get(2)));
                        pstmt.executeUpdate();
                    } catch (SQLException | IOException e) {
                        e.printStackTrace();
                    }
                    //}
                } catch (Exception ex) {
                }

            }
            request.setAttribute("message", "Revenue target successfully uploaded. ");
            request.getRequestDispatcher("./iReport.do").forward(request, response);
        } catch (FileNotFoundException e) {
            request.setAttribute("message", "Oops!"
                    + "File specified could not be located. Please try again later.");
            request.getRequestDispatcher("./iReport.do").forward(request, response);
            e.printStackTrace();
        } finally {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }

}
