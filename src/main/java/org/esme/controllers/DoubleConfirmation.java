package org.esme.controllers;

import org.esme.broker.AppBroker;
import org.esme.dto.Service;
import org.esme.webservices.airtel.AirtelUCIP;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

@SuppressWarnings("serial")
public class DoubleConfirmation extends AppBroker {

    String requestShortCode,
            serviceMTShortCode,
            msisdn, req_Text,
            requestSource,
            availableServices, smsc;

    ArrayList<Service> serviceCache;
    Connection dbConnection;

    AirtelUCIP airtelIBMUCIP;

    @SuppressWarnings({"unchecked", "unchecked", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            req_Text = removePoison(request.getParameter("req_text").trim());
            requestShortCode = removePoison(request.getParameter("shortcode"));
            msisdn = removePoison(request.getParameter("msisdn").replace("+", ""));
            smsc = removePoison(request.getParameter("smscID").trim());
        } catch (NullPointerException e) {
        }

        int update = confirmSubscription(msisdn);

        if (update > 0) {

            getSendSMS().SMSBOX("Unsub", requestShortCode, msisdn,
                    formatText("Great! Your subscription has been confirmed. "
                            + "You will get Daily Alert at N10.For help Send HELP to " + requestShortCode + ". To unsubscribe, "
                            + "send  STOP to " + requestShortCode),
                    "Unsubscription", 0, "Unsubscribed", smsc,
                    getServletContext().getInitParameter("dlrMask"));

            try {

                if (dbConnection == null) {
                    dbConnection = getDBConnection();
                }

                //Get the service he subscribed to...
                String recipient = msisdn.substring(msisdn.length() - 9);
                Statement st = dbConnection.createStatement();
                ResultSet rs = st.executeQuery("select sub_service_id from subscription "
                        + "where sub_msisdn ilike '%" + recipient + "%' "
                        + "and date(sub_request_time) = date(now()) "
                        + "order by sub_request_time desc limit 1");
                long serviceID = 0;
                while (rs.next()) {
                    serviceID = rs.getLong("sub_service_id");
                }

                if (serviceID > 0) {
                    try {
                        //Access cached services...
                        serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                    } catch (Exception e) {
                        updateServicesCache();
                        serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                    }
                    for (Service nextService : serviceCache) {

                        if (nextService.getServiceID() == serviceID) {
                            //Delay content for a minute.
                            String theRec = msisdn;
                            try {
                                Thread.sleep(30000);
                            } catch (InterruptedException ex) {
                            }

                            PreparedStatement selectPstmt = getDBConnection().prepareStatement(
                                    "select a.content from content a, "
                                            + "subscription_service b "
                                            + "where (trim(a.content_date_of_blast) = '" + formatDateToString(new Date()).trim() + "' "
                                            + "or trim(a.content_date_of_blast) = '" + cusFormatDateToString(new Date()).trim() + "') "
                                            + "and a.content_service_id = " + nextService.getServiceID() + " "
                                            + "and a.content_service_id = b.id limit 1");

                            ResultSet crs = selectPstmt.executeQuery();

                            while (crs.next()) {
                                try {

                                    airtelIBMUCIP = new AirtelUCIP();
                                    airtelIBMUCIP.makeAirtelChargingRequest(
                                            nextService, "DailyPaidContent",
                                            nextService.getServiceBillingShortCode(),
                                            "Content Purchase", crs.getString("content"),
                                            theRec);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            break;
                        }
                    }

                } else {
                    getSendSMS().SMSBOX("Not Subscribe", requestShortCode, msisdn,
                            formatText("Dear subscriber, You have not subscribed to a service today. Text HELP for help."),
                            "Unsubscription", 0, "Unsubscribed", smsc,
                            getServletContext().getInitParameter("dlrMask"));
                }

            } catch (SQLException ex) {
            }

        } else {
            getSendSMS().SMSBOX("Unsub", requestShortCode, msisdn,
                    formatText("Dear subscriber, OK is a special keyword. Text HELP for help."),
                    "Unsubscription", 0, "Unsubscribed", smsc,
                    getServletContext().getInitParameter("dlrMask"));
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    //Deactivate Service
    @SuppressWarnings("CallToPrintStackTrace")
    public int confirmSubscription(String subscriber) {
        int update = 0;
        try {

            if (dbConnection == null) {
                dbConnection = getDBConnection();
            }

            String msisdn = subscriber.substring(subscriber.length() - 10);
            PreparedStatement p = dbConnection.prepareStatement(
                    "update subscription set sub_status = 'active' where sub_msisdn ilike '%" + msisdn + "' "
                            + "and date(sub_request_time) = date(now())");
            update = p.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return update;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
