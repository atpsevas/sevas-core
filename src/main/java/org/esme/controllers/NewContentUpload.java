package org.esme.controllers;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.esme.broker.AppBroker;
import org.esme.dao.models.Content;
import org.esme.dao.models.SequentialContent;
import org.esme.dao.models.Users;
import org.esme.dto.Service;
import org.esme.utils.pingenerator.RandomPinGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class NewContentUpload extends AppBroker {

    PreparedStatement pstmt;

    String smscID;
    ArrayList<Service> serviceCache;

    String date;
    String schedule;
    long time;

    String message;
    String user;
    String userRole;
    long userID;

    ArrayList<Users> userCache;
    PreparedStatement pst;

    double uploadedFileSize;
    double uploadedFileSizeInMB;
    double fileSizeAfterFilterInMB;

    long totalNumerbUploaded;
    long totalDuplicateNumber;
    long existingSubscribers;
    long totalUniqueNumber;
    long redundantNumber;

    @SuppressWarnings("CallToThreadDumpStack")
    public static String executeLinuxCommand(
            String filePath, String command, boolean waitForResponse, String format) throws IOException, InterruptedException {
        //Convert the file to SLN...
        String response = "";
        ProcessBuilder pb = new ProcessBuilder("bash", "-c", command);
        pb.redirectErrorStream(true);
        //org.esme.broker.AppBroker.sevasLogger.info("Linux command: " + command);
        Process shell = pb.start();
        if (waitForResponse) {
            // To capture output from the shell
            InputStream shellIn = shell.getInputStream();
            // Wait for the shell to finish and get the return code
            int shellExitStatus = shell.waitFor();
            //org.esme.broker.AppBroker.sevasLogger.info("Exit status" + shellExitStatus);
            response = convertStreamToStr(shellIn);
            shellIn.close();
            //We change the file permission here...
        }

        //if (!format.equalsIgnoreCase("mp3")) {
        //}
        return response;
    }

    /*
     * To convert the InputStream to String we use the Reader.read(char[]
     * buffer) method. We iterate until the Reader return -1 which means
     * there's no more data to read. We use the StringWriter class to
     * produce the string.
     */
    public static String convertStreamToStr(InputStream is) throws IOException {
        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is,
                        "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }

    @SuppressWarnings({"unchecked", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {

        String contentDeliveryMethod = null,
                serviceName, textContent = null, contentType = null, dateOfBlast = null;
        long serviceID = 0;

        ServletFileUpload upload;
        DiskFileItemFactory factory;
        List<FileItem> items = null;
        File uploadFile = null;

        try {

            response.setContentType("text/html;charset=UTF-8");
            HttpSession session = request.getSession(true);

            user = (String) session.getAttribute("userName");
            userRole = (String) session.getAttribute("UserRole");

            //get the file using apache commons.
            factory = new DiskFileItemFactory();
            factory.setRepository(new File(getServletContext().getRealPath("/")));
            upload = new ServletFileUpload(factory);

            try {
                items = upload.parseRequest(request);
            } catch (FileUploadException ex) {
                ex.printStackTrace();
                request.setAttribute("message", "ERROR:"
                        + "Unable to upload the file specified! Check and try again.");
                request.getRequestDispatcher("new_content.jsp").forward(request, response);
                return;
            }

            for (FileItem item : items) {

                org.esme.broker.AppBroker.sevasLogger.info(item.getString());

                if (item.isFormField()) {
                    //processFormField(item);
                    if (item.getFieldName().equalsIgnoreCase("serv")) {
                        //Acquire service ID
                        serviceID = Long.parseLong(item.getString());
                    } else if (item.getFieldName().equalsIgnoreCase("sd")) {
                        try {
                            textContent = item.getString();
                        } catch (Exception e) {
                            e.printStackTrace();
                            request.setAttribute("message", "Please select date.");
                            request.getRequestDispatcher(
                                    "new_content.jsp").forward(request, response);
                            return;
                        }
                    } else if (item.getFieldName().equalsIgnoreCase("date")) {

                        try {
                            dateOfBlast = item.getString();
                        } catch (Exception e) {
                            e.printStackTrace();
                            request.setAttribute("message", "Please select date.");
                            request.getRequestDispatcher(
                                    "new_content.jsp").forward(request, response);
                            return;
                        }
                    } else if (item.getFieldName().equalsIgnoreCase("contentType")) {
                        try {
                            contentType = item.getString();
                        } catch (Exception e) {
                            e.printStackTrace();
                            request.setAttribute("message", "Please select content type.");
                            request.getRequestDispatcher(
                                    "new_content.jsp").forward(request, response);
                            return;
                        }
                    }

                } else {

                    org.esme.broker.AppBroker.sevasLogger.error("We detect file request");
                    String filename = item.getName();

                    if (!(filename.endsWith(".wav") || filename.endsWith(".mp3"))) {
                        request.setAttribute("message", "ERROR:"
                                + "You have selected an invalid file type!");
                        request.getRequestDispatcher("new_content.jsp").forward(request, response);
                        return;

                    } else {

                        //Create service directory...if no already exist
                        File serviceDir = new File(
                                getPropertyFileHandle().getProperty("VoiceContentDir") + serviceID + "/");

                        if (!serviceDir.isDirectory()) {
                            serviceDir.mkdir();
                        }

                        try {

                            String theFileName = "";
                            if (filename.endsWith(".mp3")) {
                                //Saving the File in the appropriate directory....
                                theFileName = filename.replaceAll(filename, dateOfBlast + ".mp3");
                                uploadFile = new File(serviceDir.getAbsolutePath() + "/" + theFileName);
                            } else {
                                theFileName = filename.replaceAll(filename, dateOfBlast + ".wav");
                                uploadFile = new File(serviceDir.getAbsolutePath() + "/" + theFileName);
                            }
                            //write the file to a directory
                            item.write((uploadFile));

                            String theWavFile = uploadFile.getAbsolutePath().replace(".wav", ".sln");

                            //If it is mp3, we have to conver to .wav first... ffmpeg -i song.mp3 -acodec pcm_u8 -ar 22050 song.wav
                            if (uploadFile.getAbsolutePath().endsWith("mp3")) {
                                executeLinuxCommand(
                                        uploadFile.getAbsolutePath(),
                                        "ffmpeg -i " + uploadFile.getAbsolutePath()
                                                + " -acodec pcm_u8 -ar 22050 "
                                                + uploadFile.getAbsolutePath().replace(".mp3", ".wav") + " ", true, "mp3");

                                uploadFile.getAbsolutePath().replace(".mp3", ".sln");
                            }

//                            org.esme.broker.AppBroker.sevasLogger.info("ffmpeg -i " + uploadFile.getAbsolutePath()
//                                    + " -acodec pcm_u8 -ar 22050 "
//                                    + uploadFile.getAbsolutePath().replace(".mp3", ".wav") + " ");

//                            //Convert the file to sln for asterisk usage....
                            executeLinuxCommand(
                                    uploadFile.getAbsolutePath(),
                                    "sox " + uploadFile.getAbsolutePath().replace(".mp3", ".wav")
                                            + " -t raw -r 8000 -s  -c 1 "
                                            + theWavFile + " ", true, "wav");

                            Runtime runtime = Runtime.getRuntime();
                            //Change the file permission
                            Process processChangePermission = runtime.exec("chmod 7777 " + theWavFile);
                            //Chnage the owner to asterisk...
                            Process processChangeOwner = runtime.exec("chown asterisk " + theWavFile);
                            //Delete the .wav file
                            //Process processDeleteTheWAFFile = runtime.exec("rm -fr " + filePath);

//                            org.esme.broker.AppBroker.sevasLogger.info(
//                                    "sox " + uploadFile.getAbsolutePath().replace(".mp3", ".wav")
//                                    + " -t raw -r 8000 -s  -c 1 "
//                                    + theWavFile + " ");

                        } catch (Exception ex) {
                            ex.printStackTrace();
                            request.setAttribute("message", ""
                                    + "SEVERE: Error while uploading your file. Please try again");
                            request.getRequestDispatcher("new_content.jsp").forward(request, response);
                            return;
                        }

                    }
                }
            }

            try {

                //retrieve the service Name and content delivery method...
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                for (Service nextService : serviceCache) {
                    if (nextService.getServiceID() == serviceID) {
                        serviceName = nextService.getServiceName();
                        contentDeliveryMethod = nextService.getServiceContentDeliveryMethod();
                        org.esme.broker.AppBroker.sevasLogger.info("CON DEL METHOD " + contentDeliveryMethod);
                        break;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                request.setAttribute("message",
                        "An error occured while retrieving the content service ID. Please check and try again");
                request.getRequestDispatcher("new_content.jsp").forward(request, response);
                return;
            }

            PreparedStatement ps = null;
            long contentID = generateID();

            if (!contentType.equalsIgnoreCase("sms")) {
                textContent = uploadFile.getAbsolutePath();
            }

            //Handle sequential content here...
            if (contentDeliveryMethod.contains("sequential")) {
                //Query the DB to Check if content for the service has ever been uploaded...sequential
                long lastContentBlastDay = 0;
                try {

                    PreparedStatement inPstmt = getDBConnection().prepareStatement(
                            "select content_blast_day from sequential_content "
                                    + "where content_service_id = ? "
                                    + "order by content_blast_day desc limit 1");

                    inPstmt.setLong(1, serviceID);

                    ResultSet inRs = inPstmt.executeQuery();
                    //If there contet for the service already...
                    while (inRs.next()) {

                        lastContentBlastDay = inRs.getLong("content_blast_day");
                        ps = getDBConnection().prepareStatement(
                                "insert into sequential_content(content_id, content_provider_id,"
                                        + "content_service_id,content,content_blast_day)"
                                        + "values(?,?,?,?,?)");
                        ps.setLong(1, contentID);
                        ps.setLong(2, getUserID(user));
                        ps.setLong(3, serviceID);
                        ps.setString(4, formatText(textContent));
                        ps.setLong(5, lastContentBlastDay + 1);
                        int update = ps.executeUpdate();
                        ;
                        if (update > 0) {
                            //update Sequential Content cache...
                            updateSeqContentCache();
                            request.setAttribute("message", "Content successfully uploaded");
                            request.getRequestDispatcher("new_content.jsp").forward(request, response);
                            return;
                        }
                    }
                    //If it is the service first content
                    ps = getDBConnection().prepareStatement(
                            "insert into sequential_content(content_id, content_provider_id,"
                                    + "content_service_id,content,content_blast_day)"
                                    + "values(?,?,?,?,?)");

                    ps.setLong(1, contentID);
                    ps.setLong(2, getUserID(user));
                    ps.setLong(3, serviceID);
                    ps.setString(4, formatText(textContent));
                    ps.setLong(5, lastContentBlastDay + 1);
                    int update = ps.executeUpdate();
                    if (update > 0) {
                        //update Sequential Content cache...
                        updateSeqContentCache();
                        request.setAttribute("message", "Content successfully uploaded");
                        request.getRequestDispatcher("new_content.jsp").forward(request, response);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            } else {

                try {

                    ps = getDBConnection().prepareStatement(
                            "insert into content(content_id, content_provider_id,"
                                    + "content_service_id,content,content_date_of_blast)"
                                    + "values(?,?,?,?,?)");
                    ps.setLong(1, contentID);
                    ps.setLong(2, getUserID(user));
                    ps.setLong(3, serviceID);
                    ps.setString(4, formatText(textContent));
                    ps.setString(5, dateOfBlast);

                    int update = ps.executeUpdate();

                    if (update > 0) {
                        //update textContent cache...
                        updateContentCache();
                        request.setAttribute("message", "Content successfully uploaded");
                        request.getRequestDispatcher("new_content.jsp").forward(request, response);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                    request.setAttribute("message", "Content could not be uploaded. " + "Please try again later.");
                    request.getRequestDispatcher("new_content.jsp").forward(request, response);
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    //Get user ID
    private long getUserID(String userName) {
        long userID = 0;
        @SuppressWarnings("unchecked")
        ArrayList<Users> userCache = (ArrayList<Users>) getServletContext().getAttribute("usersCache");
        for (Users user : userCache) {
            if (user.getUserName().equals(userName)) {
                userID = user.getId();
            }
        }
        return userID;
    }

    public long generateID() {
        long theID = RandomPinGenerator.randomPinGenerator(9);
        return theID;
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "null"})
    public void updateContentCache() {

        org.esme.broker.AppBroker.sevasLogger.info("Updating content archive...");
        ArrayList<Content> contentCache = new ArrayList<Content>();
        Statement statement = null;
        ResultSet resultSet = null;
        Content content;
        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(
                    getServletContext().getInitParameter("contentQuery"));

            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                while (resultSet.next()) {
                    try {
                        content = new Content();
                        content.setContent_id(resultSet.getInt("content_id"));
                        content.setContentServiceName(resultSet.getString("service_name"));
                        content.setContent(resultSet.getString("content"));
                        content.setDateUploaded(resultSet.getString("content_upload_date"));
                        content.setContentOwner(resultSet.getInt("content_provider_id"));
                        content.setContentService(resultSet.getLong("content_service_id"));
                        content.setConentDOB(resultSet.getString("content_date_of_blast"));
                        contentCache.add(content);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //Create cache of content with all
                getServletContext().setAttribute("totalContents ", contentCache.size());
                getServletContext().setAttribute("contentCache", contentCache);
            }

        } catch (Exception ex) {
            org.esme.broker.AppBroker.sevasLogger.info("Error while updating active content cache.");
            ex.printStackTrace();

        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }

    @SuppressWarnings("null")
    public void updateSeqContentCache() {
        SequentialContent sequentialContent;
        ArrayList<SequentialContent> sequentialContentCache;
        sequentialContentCache = new ArrayList<SequentialContent>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(
                    getServletContext().getInitParameter("seqContentQuery"));
            //We place a lock on the sequentialContent cache untill it finishes updating....
            synchronized (this) {
                while (resultSet.next()) {
                    try {
                        sequentialContent = new SequentialContent();
                        sequentialContent.setContent_id(resultSet.getInt("content_id"));
                        sequentialContent.setContentServiceName(resultSet.getString("service_name"));
                        sequentialContent.setContent(resultSet.getString("content"));
                        sequentialContent.setDateUploaded(resultSet.getString("content_upload_date").substring(0, 19));
                        sequentialContent.setContentOwner(resultSet.getInt("content_provider_id"));
                        sequentialContent.setContentService(resultSet.getInt("content_service_id"));
                        sequentialContent.setContentBlastDay(resultSet.getLong("content_blast_day"));
                        sequentialContentCache.add(sequentialContent);
                    } catch (Exception e) {
                    }
                }
                //Create cache of sequentialContent with all
                getServletContext().setAttribute("totalSeqContents", sequentialContentCache.size());
                getServletContext().setAttribute("seqContentCache", sequentialContentCache);
            }
        } catch (Exception ex) {
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }
}
