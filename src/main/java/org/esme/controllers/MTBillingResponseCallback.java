package org.esme.controllers;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.RabbitMQPublisher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class MTBillingResponseCallback extends AppBroker {

    private static final Logger logger = Logger.getLogger(MTBillingResponseCallback.class);

    @SuppressWarnings({"CallToPrintStackTrace", "unchecked",
            "unchecked", "unchecked", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();

        String senderID, serviceMTShortCode, recipientMSISDN,
                message, servName, subscriptionBillingType, billingTime = dateFormat.format(date),
                messageID, delivertStatus, srcModule, smsc, dailyPaidRetry = "no";

        long serviceID;
        int dlr = 0;

        response.setContentType("text/html;charset=UTF-8");


        String QUEUE_NAME;

        try {
//            subscriptionBillingType = request.getParameter("sub_type");
//            messageID = request.getParameter("msg_id");
//            message = request.getParameter("msg").replaceAll("'", "");
//            servName = request.getParameter("serv_name");

//            try {
//                dailyPaidRetry = request.getParameter("dp_retry");
//            } catch (Exception E) {
//                E.printStackTrace();
//            }

            senderID = request.getParameter("sc");
            recipientMSISDN = request.getParameter("msisdn");
            serviceID = Long.parseLong(request.getParameter("serv_id").trim());
            srcModule = request.getParameter("src_module");
            dlr = Integer.parseInt(request.getParameter("dlr"));
            smsc = request.getParameter("smsc");


            try {
                billingTime = request.getParameter("billing_time");
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Send to AMPQ
            publishToAMPQ(recipientMSISDN, serviceID, senderID, smsc, srcModule, dlr, rabbitQueueName_PROP);


        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public void publishToAMPQ(String recipientMSISDN, long serviceID, String senderID, String smsc, String srcModule, int dlr, String queue) {

        RabbitMQPublisher publisher;

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();

        String billingTime = dateFormat.format(date);

        // Generate JSON equivalent for Payload
        MTBillingResponsePayload responsePayload = new MTBillingResponsePayload();
        responsePayload.setBillingTime(billingTime);
        responsePayload.setDlr(dlr);
        responsePayload.setRecipientMSISDN(recipientMSISDN);
        responsePayload.setSenderID(senderID);
        responsePayload.setSmsc(smsc);
        responsePayload.setSrcModule(srcModule);
        responsePayload.setServiceID(serviceID);

        //Convert to JSON...
        Gson gson = new Gson();
        String rabbitPayLoad = gson.toJson(responsePayload);

        logger.debug(rabbitPayLoad);

        // Attempt to publish to RabbitMQ Message Broker
        try {
            publisher = RabbitMQPublisher.getInstance();
            publisher.publishDLRToRabbit(rabbitPayLoad, queue);
        } catch (Exception e) {
            logger.error("Unable to publish " + rabbitPayLoad);
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    class MTBillingResponsePayload {
        long serviceID;
        int dlr;

        String recipientMSISDN;
        String senderID;
        String srcModule;
        String smsc;
        String billingTime;

        public long getServiceID() {
            return serviceID;
        }

        public void setServiceID(long serviceID) {
            this.serviceID = serviceID;
        }

        public String getRecipientMSISDN() {
            return recipientMSISDN;
        }

        public void setRecipientMSISDN(String recipientMSISDN) {
            this.recipientMSISDN = recipientMSISDN;
        }

        public String getSenderID() {
            return senderID;
        }

        public void setSenderID(String senderID) {
            this.senderID = senderID;
        }

        public String getSrcModule() {
            return srcModule;
        }

        public void setSrcModule(String srcModule) {
            this.srcModule = srcModule;
        }

        public int getDlr() {
            return dlr;
        }

        public void setDlr(int dlr) {
            this.dlr = dlr;
        }

        public String getSmsc() {
            return smsc;
        }

        public void setSmsc(String smsc) {
            this.smsc = smsc;
        }

        public String getBillingTime() {
            return billingTime;
        }

        public void setBillingTime(String billingTime) {
            this.billingTime = billingTime;
        }
    }

}
