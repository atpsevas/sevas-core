package org.esme.controllers.cronjobs;

import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.BillingBroadCaster;
import org.esme.dto.Service;
import org.esme.gateway.kannel.WEB2SMS;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class FailedDailyPaidContentRetry_CRON_TRIGGER extends AppBroker {

    int counter;
    ArrayList<Service> serviceCache;
    WEB2SMS web2SMS;
    BillingBroadCaster campaignBroadCaster;

    @SuppressWarnings("unchecked")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            org.esme.broker.AppBroker.sevasLogger.info("Retring Daily Paid Content");
            //iniitate 
            campaignBroadCaster = BillingBroadCaster.getInstance((Connection) getServletContext().getAttribute("database.connection"));
            campaignBroadCaster.retryFailedMTDailyPaidContent();

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
