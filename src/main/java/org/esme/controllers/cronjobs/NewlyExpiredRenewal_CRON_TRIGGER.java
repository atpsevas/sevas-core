package org.esme.controllers.cronjobs;

import org.apache.log4j.Logger;
import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.NewBillingBroadCaster;
import org.esme.dto.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class NewlyExpiredRenewal_CRON_TRIGGER extends AppBroker {

    private static final Logger logger = Logger.getLogger(NewlyExpiredRenewal_CRON_TRIGGER.class);

    ArrayList<Service> serviceCache;

    /**
     * Servlet to invoke billing operation. Specify action with
     * request.getParameter("action")
     *
     * @param request
     * @param response
     */
    @SuppressWarnings({"unchecked", "CallToPrintStackTrace", "unchecked", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {

        logger.info("Billing process callled....");
        // Retrieve Services from Cache
        try {
            if (serviceCache == null || serviceCache.size() < 1) {
                updateServicesCache();
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        response.setContentType("text/html;charset=UTF-8");
        NewBillingBroadCaster billingHandler = new NewBillingBroadCaster();

        billingHandler.makeBillingRequest(serviceCache, getDBConnection());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
