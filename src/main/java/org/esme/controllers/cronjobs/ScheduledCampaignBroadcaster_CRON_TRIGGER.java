package org.esme.controllers.cronjobs;

import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.MessageBroadCaster;
import org.esme.dao.models.SMSCampaign;
import org.esme.dto.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@SuppressWarnings("serial")
public class ScheduledCampaignBroadcaster_CRON_TRIGGER extends AppBroker {

    ArrayList<SMSCampaign> campaignCache;
    SMSCampaign campaign;
    PreparedStatement pst;
    long totalMessageProcessed;

    @SuppressWarnings({"unchecked", "unchecked"})
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {

            response.setContentType("text/html;charset=UTF-8");

            try {
                campaignCache = (ArrayList<SMSCampaign>) getServletContext().getAttribute("smsCampaignCache");
            } catch (Exception e) {
            }

            for (SMSCampaign theCampaign : campaignCache) {
                if (!theCampaign.getCampaignStatus().equalsIgnoreCase("Sent")) {
                    int remainingDays = substractDate(theCampaign.getCampaignSentDate());
                    if (remainingDays == 0 && theCampaign.getCampaignSentTime() == getCurrentHour()) {

                        String serviceNetwork = null;
                        @SuppressWarnings("unchecked")
                        ArrayList<Service> serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
                        for (Service nextService : serviceCache) {
                            if (nextService.getServiceID() == theCampaign.getCampaignServiceID()) {
                                serviceNetwork = nextService.getServiceNetwork();
                                //Update campaign table...
                                try {
                                    pst = getDBConnection().prepareStatement(
                                            "update sms_campaign set campaign_actual_start_time = now() "
                                                    + " where campaign_id = " + campaign.getCampaignID() + " ");
                                    pst.executeUpdate();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                //Send campaign for processing...
                                //iniitate campaign - multi threading...
                                MessageBroadCaster campaignBroadCaster = new MessageBroadCaster(
                                        theCampaign, serviceNetwork, getDBConnection());
                                campaignBroadCaster.doCampaign();
                                break;
                            }
                        }

                    }
                }
            }

        } catch (Exception ex) {
        }
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace"})
    public int substractDate(String firstDate) {
        int remainingDate = 0;
        try {
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
            long d1 = formater.parse(firstDate).getTime();
            long d2 = formater.parse(formatDateToString(new Date())).getTime();
            remainingDate = Math.round(d1 - d2) / (1000 * 60 * 60 * 24);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return remainingDate;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
