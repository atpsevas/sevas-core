package org.esme.controllers.cronjobs;

import org.esme.broker.AppBroker;
import org.esme.utils.email.Emailer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.Timer;

/**
 * @author saheedalbert
 */
public class ContentAdminNotifier_CRON_TRIGGER extends AppBroker {

    private static final long serialVersionUID = 1L;

    Timer timer;
    Properties prop;

    private String host;
    private String port;
    private String user;
    private String pass;

    private String contentAdminEmailAddresses;

    private String resultMessage;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            prop = AppBroker.getPropertyFileHandle();


            host = prop.getProperty("mailServerHost");
            port = prop.getProperty("mailServerPort");
            user = prop.getProperty("mailSenderAdd");
            pass = prop.getProperty("mailSenderPassword");
            contentAdminEmailAddresses = prop.getProperty("contentAdminEmailAdd");

            org.esme.broker.AppBroker.sevasLogger.info(host);
            org.esme.broker.AppBroker.sevasLogger.info(port);
            org.esme.broker.AppBroker.sevasLogger.info(user);
            org.esme.broker.AppBroker.sevasLogger.info(pass);
            org.esme.broker.AppBroker.sevasLogger.info(contentAdminEmailAddresses);
            sendReport();

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "null", "Convert2Diamond"})
    public void sendReport() {
        Statement statement = null;
        ResultSet resultSet = null;
        String service = "";
        StringBuilder email = new StringBuilder();
        int counter = 0;

        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(
                    "select id, service_name from subscription_service "
                            + "where id NOT IN (select serv_id "
                            + "from outgoing_messages where date(sent_time) = date (now()))");

            email.append("<html><body>"
                    + "<table style='font-family: verdana,arial,sans-serif;\n"
                    + "	font-size:11px;\n"
                    + "	color:#333333;\n"
                    + "	border-width: 1px;\n"
                    + "	border-color: #666666;\n"
                    + "	border-collapse: collapse;'>");
            email.append("<tr>");
            email.append("<th style='border-width: 1px;\n"
                    + "	padding: 8px;\n"
                    + "	border-style: solid;\n"
                    + "	border-color: #666666;\n"
                    + "	background-color: #dedede;'>");
            email.append("Service ID");
            email.append("</th>");
            email.append("<th style='border-width: 1px;\n"
                    + "	padding: 8px;\n"
                    + "	border-style: solid;\n"
                    + "	border-color: #666666;\n"
                    + "	background-color: #dedede;'>");
            email.append("Service Name");
            email.append("</th>");
            email.append("</tr>");
            while (resultSet.next()) {
                counter++;
                org.esme.broker.AppBroker.sevasLogger.info("in resultSet...");
                email.append("<tr>");
                email.append("<td style='border-width: 1px;\n"
                        + "	padding: 8px;\n"
                        + "	border-style: solid;\n"
                        + "	border-color: #666666;\n"
                        + "	background-color: #ffffff;'>");
                long id = resultSet.getLong("id");
                email.append(id);
                email.append("</td>");
                email.append("<td style='border-width: 1px;\n"
                        + "	padding: 8px;\n"
                        + "	border-style: solid;\n"
                        + "	border-color: #666666;\n"
                        + "	background-color: #ffffff;'>");
                email.append(resultSet.getString("service_name"));
                email.append("</td>");
                email.append("<tr>");
            }

            email.append("</table></body></html>");

            if (counter == 0) {
                service = "Content send for all services today.";
            } else {
                service = email.toString();
            }

            //Send the mail..
            try {

                Emailer.sendEmailWithAttachment(
                        host, port, user, pass,
                        contentAdminEmailAddresses,
                        "ALERT: Today's content has not been sent for services below.",
                        service, null);

                resultMessage = "The e-mail was sent successfully";
            } catch (Exception ex) {
                ex.printStackTrace();
                resultMessage = "There was an error: " + ex.getMessage();
            } finally {
                org.esme.broker.AppBroker.sevasLogger.info(resultMessage);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
