package org.esme.controllers.cronjobs;

import org.apache.commons.codec.binary.Base64;
import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.MessageBroadCaster;
import org.esme.dto.Service;
import org.esme.gateway.kannel.WEB2SMS;
import org.esme.xmlreader.SMSCStats;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class DailyContentExtraPush_CRON_TRIGGER extends AppBroker {

    int counter;
    ArrayList<Service> serviceCache;
    WEB2SMS web2SMS;
    MessageBroadCaster campaignBroadCaster;
    SMSCStats theSMSCStats = SMSCStats.getInstance();

    @SuppressWarnings("unchecked")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            org.esme.broker.AppBroker.sevasLogger.info("DAILY CONTENT EXTRA MODULE STARTED...");

            prop = AppBroker.getPropertyFileHandle();

            try {
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            } catch (Exception e) {
            }

            if (serviceCache == null || serviceCache.size() < 1) {
                updateServicesCache();
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            }

            campaignBroadCaster = new MessageBroadCaster(getDBConnection());

            //MT Based sevice...
            //org.esme.broker.AppBroker.sevasLogger.info("SENDING CONTENT FOR..." + nextService.getServiceName());
            campaignBroadCaster.doDailyHiddenContectPush(serviceCache);
            //}
        }
    }

    public long checkRabbitQueue(String rabbitQueueURL, String rabbitQueueUserName, String rabbitQueuePassword) {
        JSONObject obj2 = null;

        JSONParser jSONParser = new JSONParser();
        try {

            String name = rabbitQueueUserName;
            String password = rabbitQueuePassword;

            String authString = name + ":" + password;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);

            URL url = new URL(rabbitQueueURL);
            URLConnection urlConnection = url.openConnection();
            urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
            InputStream is = urlConnection.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);

            int numCharsRead;
            char[] charArray = new char[1024];
            StringBuffer sb = new StringBuffer();
            while ((numCharsRead = isr.read(charArray)) > 0) {
                sb.append(charArray, 0, numCharsRead);
            }
            String result = sb.toString();

            JSONArray jSONArray = (JSONArray) jSONParser.parse(result);
            obj2 = (JSONObject) jSONArray.get(0);

        } catch (MalformedURLException e) {
        } catch (IOException | org.json.simple.parser.ParseException e) {
        }

        return Long.parseLong(obj2.get("messages").toString());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
