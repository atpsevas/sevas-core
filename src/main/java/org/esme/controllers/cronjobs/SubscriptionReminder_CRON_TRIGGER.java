package org.esme.controllers.cronjobs;

import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.MessageBroadCaster;
import org.esme.dto.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class SubscriptionReminder_CRON_TRIGGER extends AppBroker {

    private static final long serialVersionUID = 1L;
    MessageBroadCaster campaignBroadCaster;
    ArrayList<Service> serviceCache;

    @SuppressWarnings("unchecked")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            campaignBroadCaster = new MessageBroadCaster(getDBConnection());

            org.esme.broker.AppBroker.sevasLogger.error("REMINDER CALLED");

            try {
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            } catch (Exception e) {
            }

            if (serviceCache == null || serviceCache.size() < 1) {
                updateServicesCache();
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            }

            for (Service nextService : serviceCache) {
                org.esme.broker.AppBroker.sevasLogger.info("SERVICE NAME..." + nextService.getServiceName());

                //MT Based sevice...
                if (nextService.getServiceReminderTime() == getCurrentHour()) {
                    org.esme.broker.AppBroker.sevasLogger.info("SENDING Reminder FOR..." + nextService.getServiceName());
                    //iniitate campaign - multi threading...
                    campaignBroadCaster.doSubscriptionReminder(nextService);
                }
            }

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
