package org.esme.controllers.cronjobs;

import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.MessageBroadCaster;
import org.esme.dto.Service;
import org.esme.gateway.kannel.WEB2SMS;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class DailyAlert_CRON_TRIGGER extends AppBroker {

    int counter;
    ArrayList<Service> serviceCache;
    WEB2SMS web2SMS;
    MessageBroadCaster campaignBroadCaster;

    @SuppressWarnings("unchecked")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            org.esme.broker.AppBroker.sevasLogger.info("DAILY CONTENT MODULE STARTED...");

            try {
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            } catch (Exception e) {
            }

            if (serviceCache == null || serviceCache.size() < 1) {
                updateServicesCache();
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            }
            try {
                for (Service nextService : serviceCache) {
                    org.esme.broker.AppBroker.sevasLogger.info("SERVICE NAME : " + nextService.getServiceName() + " SERVICE TOB : " + nextService.getTimeOfBlast() + " SERVER TIME : " + getCurrentHour());

                    //MT Based sevice...
                    if (Integer.parseInt(nextService.getTimeOfBlast()) == getCurrentHour()) {
                        try {
                            org.esme.broker.AppBroker.sevasLogger.info("SENDING CONTENT FOR..." + nextService.getServiceName());
                            //iniitate campaign - multi threading...
                            campaignBroadCaster = new MessageBroadCaster(getDBConnection());
                            campaignBroadCaster.doDailyAlert(nextService);
                        } catch (Exception e) {
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
