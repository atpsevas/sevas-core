package org.esme.controllers.cronjobs;

import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.MessageBroadCaster;
import org.esme.dto.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class NewSubscriptionActivationReminder extends AppBroker {

    private static final long serialVersionUID = 1L;
    MessageBroadCaster campaignBroadCaster;
    ArrayList<Service> serviceCache;
    String reminderType = "";

    @SuppressWarnings("unchecked")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            campaignBroadCaster = new MessageBroadCaster(getDBConnection());
            org.esme.broker.AppBroker.sevasLogger.error(" NewSubscriptionActivationReminder REMINDER CALLED");
            reminderType = removePoison(request.getParameter("reminderType"));
            //iniitate campaign - multi threading...
            campaignBroadCaster.doSubscriptionActivationReminder(reminderType);

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
