package org.esme.controllers.cronjobs;

import org.esme.broker.AppBroker;
import org.esme.dto.Service;
import org.esme.webservices.etisalat.EtisalatDirectBilling;
import org.esme.webservices.glo.GloPSA;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.*;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class DailyPaidServiceRenewal_CRON_TRIGGER extends AppBroker {

    ArrayList<Service> serviceCache;
    //Direct Billing
    //For etisalat unified portal..
//    MaspEventSummaryData etisalatNotify;
//    MaspEventResponseData etisalatReponse;

    //Direct Billing
    EtisalatDirectBilling etisalatDirectBilling;
//    MaspPaymentResponseData result = null;
//    MaspPaymentResponseData chargingResponse = null;

    //Glo PSA BILLING...
    GloPSA gloPSA;
    String gloPSAbillingResponse;

    String vendorUser;
    String vendorPassword;
    String vendorCode;
    String transactionID;

    @SuppressWarnings("unchecked")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            try {
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            } catch (Exception e) {
            }
            if (serviceCache == null || serviceCache.size() < 1) {
                updateServicesCache();
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            }

            for (Service nextService : serviceCache) {
                Service service = nextService;
                org.esme.broker.AppBroker.sevasLogger.info("Next service for billing " + service.getServiceName());
                //Campare the billing time of the service with current hour...
                //if (service.getBillingTime() == getCurrentHour()) {
                renewSubscription(service);
                // }
            }
        } catch (Exception e) {

        }
    }

    @SuppressWarnings({"CallToThreadDumpStack", "SleepWhileInLoop", "CallToPrintStackTrace", "null", "UseSpecificCatch"})
    public void renewSubscription(Service service) {
        try {

            //if (service.getBillingTime() == getCurrentHour()) {
            // totalDue++;
            prop = AppBroker.getPropertyFileHandle();

            try {
                if (service.getServiceSubType().equalsIgnoreCase("EDB")) {

                    org.esme.broker.AppBroker.sevasLogger.info("Direct BIlling Service..");
                    if (checkEtisalatDirectBillingQueue(prop.getProperty("etisalatDBQueueURL")) < 1) {

                        org.esme.broker.AppBroker.sevasLogger.info("No Direct BIlling Queue..");
                        etisalatDirectBilling = EtisalatDirectBilling.getInstance();
                        etisalatDirectBillingDailyPaidServiceRenewal(
                                service, getServletContext().getInitParameter("dailyExpiredSubscriptionQuery"));

                    }

                } else if (service.getServiceSubType().equalsIgnoreCase("PSA")) {
                    gloPSA = GloPSA.getInstance();
                    gloPSABilling(gloPSA, service, getServletContext().getInitParameter("dailyExpiredSubscriptionQuery"));
                } else if (service.getServiceSubType().equalsIgnoreCase("UCIP")
                        && Long.parseLong(service.getServicePeriod()) == 0) {
                } else {
                    //MT billing... 
                    mtBilling(getServletContext().getInitParameter("dailyExpiredSubscriptionQuery"), service);
                }
                // totalAttempted++;
            } catch (Exception e) {
                e.printStackTrace();
            }
            //}

        } catch (Exception e) {
            e.printStackTrace();
            org.esme.broker.AppBroker.sevasLogger.info("Error processing first time subscription renewal." + e.getMessage());
        }
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public int updateSubscription(String subscriber, Service service, long period, String lastBilledCode) {
        Statement ps;
        ResultSet rs;
        String msisdn = subscriber.substring(subscriber.length() - 9);
        int update = 0;
        try {

            ps = getDBConnection().createStatement();
            rs = ps.executeQuery(
                    "select sub_expiry_date "
                            + "from subscription "
                            + "where sub_service_id = " + service.getServiceID() + "  "
                            + "and sub_msisdn ilike '%" + msisdn + "' "
                            + "and sub_status = 'active' ");

            while (rs.next()) {
                PreparedStatement ips = getDBConnection().prepareStatement(
                        "update subscription set sub_expiry_date = "
                                + "DATE '" + rs.getString("sub_expiry_date") + "' + "
                                + period + ", last_billed_code = '" + lastBilledCode + "'   "
                                + "where sub_msisdn ilike '%" + msisdn + "' "
                                + "and sub_service_id = " + service.getServiceID());
                update = ips.executeUpdate();
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return update;
    }

    @SuppressWarnings("CallToPrintStackTrace")
    // Etisalat Direct Billing Daily renewal service
    public void etisalatDirectBillingDailyPaidServiceRenewal(Service theService, String query) {

        try {
            PreparedStatement statement;
            ResultSet dataResultSet;
            statement = getDBConnection().prepareStatement(query);
            statement.setLong(1, theService.getServiceID());
            dataResultSet = statement.executeQuery();
            while (dataResultSet.next()) {

                etisalatDirectBilling.makeEtisalatDirectBillingBillingRequest(
                        theService, "SubscriptionRenewal", theService.getServiceBillingShortCode(),
                        "NILL", theService.getServiceRenewalMessage(), dataResultSet.getString("sub_msisdn"));

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings({"CallToPrintStackTrace", "null"})
    public void gloPSABilling(GloPSA gloPSA, Service theService, String query) {

        vendorUser = getServletContext().getInitParameter("gloPSABillingUserName");
        vendorPassword = getServletContext().getInitParameter("gloPSABillingPassword");

        long amount = Long.parseLong(theService.getServicePrice());
        PreparedStatement statement;
        ResultSet dataResultSet;

        try {

            statement = getDBConnection().prepareStatement(query);
            dataResultSet = statement.executeQuery();
            String subMSISDN = "";

            //Create an instance of the billing API
            //Attempt main code billing...
            String thelastBilledMSISDN = "";
            while (dataResultSet.next()) {

                if (!subMSISDN.equalsIgnoreCase(thelastBilledMSISDN)) {
                    subMSISDN = dataResultSet.getString("sub_msisdn").
                            substring(dataResultSet.getString("sub_msisdn").length() - 9);
                    try {

                        gloPSA.makeGloChargingRquest(
                                theService, amount,
                                "SubscriptionRenewal", theService.getServiceBillingShortCode(), subMSISDN,
                                getDBConnection(), theService.getServiceRenewalMessage());

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }
                thelastBilledMSISDN = subMSISDN;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "UseSpecificCatch"})
    public void mtBilling(String query, Service service) {
        long batchSize = 0;
        long pid;
        String params;
        PreparedStatement ps;
        Connection dbConnection = getDBConnection();

        prop = AppBroker.getPropertyFileHandle();


        try {
            //Initiate billing help 
            try {
                try {

                    ps = dbConnection.prepareStatement(query);
                    ps.setLong(1, service.getServiceID());
                    ResultSet resultSet = ps.executeQuery();
                    String subMSISDN;
                    String theNextMSISDN = "";
                    String thelastBilledMSISDN;

                    while (resultSet.next()) {

                        batchSize++;

                        thelastBilledMSISDN = resultSet.getString("sub_msisdn").
                                substring(resultSet.getString("sub_msisdn").length() - 9);
                        if (!theNextMSISDN.equalsIgnoreCase(thelastBilledMSISDN)) {

                            //increment batch size..
                            subMSISDN = resultSet.getString("sub_msisdn");

                            params = encodeParams(service.getServiceBillingNetwork())
                                    + "&sub_type=" + encodeParams(service.getServiceSubType())
                                    + "&msg_id=" + encodeParams(generateMessageID())
                                    + "&sc=" + encodeParams(service.getServiceBillingShortCode())
                                    + "&msisdn=" + encodeParams(subMSISDN)
                                    + "&msg=" + encodeParams(service.getServiceRenewalMessage())
                                    + "&serv_id=" + service.getServiceID()
                                    + "&serv_name=" + encodeParams(service.getServiceName())
                                    + "&src_module=" + encodeParams("SubscriptionRenewal")
                                    + "&dlr_mask=" + encodeParams(DLRURL) + "&dlr=%d";

                            ps = getDBConnection().prepareStatement(
                                    "INSERT INTO send_sms (momt, sender, receiver, "
                                            + "msgdata, smsc_id, sms_type, boxc_id,"
                                            + "dlr_mask,dlr_url,time,pid) "
                                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?)");

                            pid = 64;
                            //We want messages visible to telco staff...
                            if (service.getMainbillingnotifySMSSetting().equalsIgnoreCase("yes")
                                    || resultSet.getString("sub_msisdn").contains("9944")) {
                                pid = 127;
                            }

                            long epochTime = System.currentTimeMillis() / 1000;
                            ps.setString(1, service.getServiceSubType());
                            ps.setString(2, service.getServiceBillingShortCode());
                            ps.setString(3, subMSISDN);
                            ps.setString(4, formatText(service.getServiceRenewalMessage()));
                            ps.setString(5, service.getServiceBillingNetwork());
                            ps.setLong(6, 2);
                            ps.setString(7, prop.getProperty("billingSMSBoxID"));
                            ps.setLong(8, Long.parseLong(DLR_MASK));
                            ps.setString(9, DLRURL + params);
                            ps.setLong(10, epochTime + 3600);
                            ps.setLong(11, pid);
                            ps.executeUpdate();

                            org.esme.broker.AppBroker.sevasLogger.info("Line..." + batchSize + ".."
                                    + service.getServiceBillingShortCode() + " "
                                    + resultSet.getString("sub_msisdn") + " "
                                    + formatText(service.getServiceRenewalMessage()) + " "
                                    + service.getServiceBillingNetwork());

                        }
                        theNextMSISDN = thelastBilledMSISDN;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public long checkEtisalatDirectBillingQueue(String etisalatDirectBillingQueueURL) throws MalformedURLException, IOException {

        long queue = 1;
        URL website = new URL(etisalatDirectBillingQueueURL);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        try {
            queue = Long.parseLong(response.toString().trim());
        } catch (Exception e) {

        }
        in.close();

        return queue;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
