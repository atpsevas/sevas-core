package org.esme.controllers.cronjobs;

import org.esme.broker.AppBroker;
import org.esme.gateway.kannel.Kannel;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author saheedalbert
 */
public class ShareSubscription extends AppBroker {

    private static final long serialVersionUID = 1L;
    Kannel kannelClass;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, InstantiationException, IllegalAccessException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            kannelClass = new Kannel(
                    "http://hscl.atp-sevas.com",
                    13031, "hscl", "hscl");
            Statement st = getDBConnection().createStatement();
            ResultSet rs = st.executeQuery(
                    "select sub_msisdn from subscription "
                            + "where date(sub_request_time) = '" + getCurrentDate("yyyy-MM-dd") + "' "
                            + "and extract(hour from sub_request_time) = " + getCurrentHour() + " ");

            while (rs.next()) {
                try {
                    kannelClass.sendText(
                            "47251", rs.getString("sub_msisdn"),
                            "Text CARE or FIT to 47251 to complete your FREE service.", "ETISALAT_BC");
                } catch (MalformedURLException ex) {
                    Logger.getLogger(ShareSubscription.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ShareSubscription.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } catch (SQLException ex) {
        }
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ShareSubscription.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ShareSubscription.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
