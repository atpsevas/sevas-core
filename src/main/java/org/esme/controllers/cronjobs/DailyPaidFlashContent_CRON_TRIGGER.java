package org.esme.controllers.cronjobs;

import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.MessageBroadCaster;
import org.esme.dto.Service;
import org.esme.gateway.kannel.WEB2SMS;
import org.esme.xmlreader.SMSCStats;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class DailyPaidFlashContent_CRON_TRIGGER extends AppBroker {

    int counter;
    ArrayList<Service> serviceCache;
    WEB2SMS web2SMS;
    MessageBroadCaster campaignBroadCaster;
    SMSCStats theSMSCStats = SMSCStats.getInstance();

    @SuppressWarnings("unchecked")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            org.esme.broker.AppBroker.sevasLogger.info("DAILY PAID FLASH CONTENT MODULE STARTED...");

            prop = AppBroker.getPropertyFileHandle();

            try {
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            } catch (Exception e) {
            }

            if (serviceCache == null || serviceCache.size() < 1) {
                updateServicesCache();
                serviceCache = (ArrayList<Service>) getServletContext().getAttribute("serviceCache");
            }

            //Check Rabbit Queue for Specific Queue through Hay HTTP API
            /*TODO*/
            String getQueueURL = prop.getProperty("rabbitQueueCheckUrl");

            Long currentQueue = this.checkQueue(getQueueURL);

            org.esme.broker.AppBroker.sevasLogger.info("Rabbit Queue: " + currentQueue);

            if (currentQueue <= 1) {
                campaignBroadCaster = new MessageBroadCaster(getDBConnection());
                for (Service nextService : serviceCache) {
                    org.esme.broker.AppBroker.sevasLogger.info("SERVICE NAME : " + nextService.getServiceName() + " SERVICE TOB : " + nextService.getTimeOfBlast() + " SERVER TIME : " + getCurrentHour());
                    //MT Based sevice...
                    org.esme.broker.AppBroker.sevasLogger.info("SENDING FLASH CONTENT FOR..." + nextService.getServiceName());
                    //iniitate campaign - multi threading...
                    campaignBroadCaster.doDailyFlashExtraPush(nextService);
                }
                org.esme.broker.AppBroker.sevasLogger.info("Starting new Daily Paid Flash Content ");
            }

        }
    }

    public long checkQueue(String queueUrl) {
        String result = null;
        try {
            URL url = new URL(queueUrl);
            URLConnection urlConnection = url.openConnection();
            InputStream is = urlConnection.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);

            int numCharsRead;
            char[] charArray = new char[1024];
            StringBuffer sb = new StringBuffer();
            while ((numCharsRead = isr.read(charArray)) > 0) {
                sb.append(charArray, 0, numCharsRead);
            }
            result = sb.toString();

        } catch (Exception e) {
        }

        return Long.parseLong(result);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
