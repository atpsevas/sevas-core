package org.esme.listeners;

import org.esme.dao.models.ServiceBundle;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ServiceBundleListener extends ListenersBroker implements ServletContextListener {

    ServiceBundle serviceBundle;
    ArrayList<ServiceBundle> serviceBundleCache;
    int counter;
    ArrayList categoryList;
    Timer timer;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        datasource = (DataSource) sce.getServletContext().getAttribute("dbPool");
        context = sce.getServletContext();
        scheduleTimer();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        timer.cancel();
    }

    public void scheduleTimer() {
        timer = new Timer();
        //Run every 24HRS!
        timer.scheduleAtFixedRate(new ServiceBundleListener.RemindTask(), 0, 86400000);
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {
            updateBundle(context);
        }

        @SuppressWarnings({"CallToThreadDumpStack", "Convert2Diamond", "null"})
        public void updateBundle(ServletContext sce) {

            org.esme.broker.AppBroker.getSevasLogger().info("Updating Bundle..");
            serviceBundleCache = new ArrayList<ServiceBundle>();
            Statement statement = null;
            ResultSet resultSet = null;

            try {

                statement = getListenerDBConnection().createStatement();
                resultSet = statement.executeQuery(context.getInitParameter("bundleQuery"));
                //We place a lock on the content cache untill it finishes updating....
                synchronized (this) {

                    //update previously stored job contents from the container...
                    while (resultSet.next()) {

                        serviceBundle = new ServiceBundle();
                        serviceBundle.setBundleID(resultSet.getLong("id"));
                        serviceBundle.setBundleName(resultSet.getString("bundle_name"));
                        serviceBundle.setBundleKeyword(resultSet.getString("bundle_keyword"));
                        serviceBundle.setBundlePrice(resultSet.getLong("bundle_price"));
                        serviceBundle.setBundleDateCreated(resultSet.getString("date_created"));
                        serviceBundle.setBundleSubscriptionMessage(resultSet.getString("bundle_sub_message"));
                        serviceBundle.setBundleUnsubscriptionMessage(resultSet.getString("bundle_unsub_message"));
                        serviceBundleCache.add(serviceBundle);
                    }
                    //Create cache of all users
                    org.esme.broker.AppBroker.getSevasLogger().info("Total Bundle " + serviceBundleCache.size());
                    context.setAttribute("totalBundle", serviceBundleCache.size());
                    context.setAttribute("bundleCache", serviceBundleCache);
                }

            } catch (Exception ex) {
                org.esme.broker.AppBroker.getSevasLogger().info(ex.getMessage());
            }
        }
    }

}
