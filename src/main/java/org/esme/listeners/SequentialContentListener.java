package org.esme.listeners;

import org.esme.dao.models.SequentialContent;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author salbert
 */
public class SequentialContentListener extends ListenersBroker implements ServletContextListener {

    SequentialContent sequentialContent;
    ArrayList<SequentialContent> sequentialContentCache;
    int counter;

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
//        datasource = (DataSource) sce.getServletContext().getAttribute("dbPool");
//        context = sce.getServletContext();
        updateSeqContentCache(context);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }


    @SuppressWarnings("CallToThreadDumpStack")
    public void updateSeqContentCache(ServletContext sce) {
        org.esme.broker.AppBroker.getSevasLogger().info("Updating sequential content archive...");
        sequentialContentCache = new ArrayList<SequentialContent>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getListenerDBConnection().createStatement();
            resultSet = statement.executeQuery(
                    context.getInitParameter("seqContentQuery"));
            //We place a lock on the sequentialContent cache untill it finishes updating....
            synchronized (this) {
                while (resultSet.next()) {
                    try {
                        sequentialContent = new SequentialContent();
                        sequentialContent.setContent_id(resultSet.getInt("content_id"));
                        sequentialContent.setContentServiceName(resultSet.getString("service_name"));
                        sequentialContent.setContent(resultSet.getString("content"));
                        sequentialContent.setDateUploaded(resultSet.getString("content_upload_date").substring(0, 19));
                        sequentialContent.setContentOwner(resultSet.getInt("content_provider_id"));
                        sequentialContent.setContentService(resultSet.getInt("content_service_id"));
                        sequentialContent.setContentBlastDay(resultSet.getLong("content_blast_day"));
                        sequentialContentCache.add(sequentialContent);
                    } catch (Exception e) {
                    }
                }
                //Create cache of sequentialContent with all
                context.setAttribute("totalSeqContents", sequentialContentCache.size());
                context.setAttribute("seqContentCache", sequentialContentCache);
            }

        } catch (Exception ex) {
            org.esme.broker.AppBroker.getSevasLogger().info(ex.getMessage());
        }
    }

}
