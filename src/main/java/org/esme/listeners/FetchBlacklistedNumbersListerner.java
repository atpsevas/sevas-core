package org.esme.listeners;

import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.FetchBlacklistedNumbers;

import javax.naming.Context;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.*;

public class FetchBlacklistedNumbersListerner extends ListenersBroker implements ServletContextListener {

    Context envCtx;
    Timer timer;

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        org.esme.broker.AppBroker.getSevasLogger().info("FetchBlacklistedNumbersListerner Started...");
//        datasource = (DataSource) sce.getServletContext().getAttribute("dbPool");
//        context = sce.getServletContext();
        scheduleTimer();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        timer.cancel();
    }

    public void scheduleTimer() {

        timer = new Timer();
        //Run every 5 minutes! with delay of 20 minute...
        timer.scheduleAtFixedRate(new FetchBlacklistedNumbersListerner.RemindTask(), 0, 1200000);
    }

    class RemindTask extends TimerTask {

        Calendar cal = Calendar.getInstance();
        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        int totalDayofMonth = cal.getMaximum(Calendar.DAY_OF_MONTH);

        FetchBlacklistedNumbers blockedNumbers;
        Properties theProp;
        HashMap blacklistedNumbers;
        ArrayList<String> doNotChargeNumbers;

        @Override
        @SuppressWarnings("CallToPrintStackTrace")
        public void run() {

            theProp = AppBroker.getPropertyFileHandle();

            try {
                org.esme.broker.AppBroker.getSevasLogger().info("Fetching Do not charge Numbers....");
                doNotChargeNumbers = blockedNumbers.getDoNotChargeNumbers(theProp.getProperty("doNotChargeFilePath"));
                context.setAttribute("doNotChargeNumbersCache", doNotChargeNumbers);
                org.esme.broker.AppBroker.getSevasLogger().info("Total Do not Charge Numbers Fetched.." + doNotChargeNumbers.size());
                org.esme.broker.AppBroker.getSevasLogger().info("Done Fechting.......");
            } catch (Exception e) {

            }

            org.esme.broker.AppBroker.getSevasLogger().info("Fetching Blacklisted Numbers....");
            blockedNumbers = new FetchBlacklistedNumbers(theProp.getProperty("blacklistURL"));
            try {
                blacklistedNumbers = (HashMap) blockedNumbers.getBlacklistedNumbers();
                context.setAttribute("blackListedNumbersCache", blacklistedNumbers);
                org.esme.broker.AppBroker.getSevasLogger().info("Total Blacklisted Number Fetched.." + blacklistedNumbers.size());
                org.esme.broker.AppBroker.getSevasLogger().info("Done Fechting.......");
            } catch (Exception e) {

            }

        }

    }
}
