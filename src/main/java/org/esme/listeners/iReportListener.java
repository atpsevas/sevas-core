package org.esme.listeners;

import helper.iReportHelper;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.esme.dao.models.ContentLogs;

import javax.naming.Context;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class iReportListener extends ListenersBroker implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(iReportListener.class);
    Context envCtx;
    Timer timer;

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.info("iReportListener Started...");
        datasource = (DataSource) sce.getServletContext().getAttribute("dbPool");
//        context = getServletContext();
        scheduleTimer();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        timer.cancel();
    }

    public void scheduleTimer() {
        timer = new Timer();
        //Run every 5 minutes! with delay of 0 minute...
        timer.scheduleAtFixedRate(new iReportListener.RemindTask(), 0, 300000);
    }

    class RemindTask extends TimerTask {

        Calendar cal = Calendar.getInstance();
        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        int totalDayofMonth = cal.getMaximum(Calendar.DAY_OF_MONTH);

        PreparedStatement statement;
        ResultSet dailyServiceRevenue_All,
                dailyServiceReport_All,
                dailyPaidContentRevenue, dailyPaidContent;

        ArrayList<ContentLogs> monthTillDateContentLogs;
        ArrayList<ContentLogs> todayContentLogs;

        Connection conn;

        iReportHelper irh;

        @Override
        @SuppressWarnings({"CallToPrintStackTrace", "UseSpecificCatch"})
        public void run() {

            logger.info("Generating DashBoard Report....");

            Long inceptionTillDateRev = null;
            Long todayRev = null;
            Long monthTargetRev = null;
            Double dailyChurn = null;
            Long actualRev = null;
            FileInputStream inputStream = null;
            String updateQuery;

//            conn = (Connection) context.getAttribute("database.connection");
            try {
                conn = getListenerDBConnection();
            } catch (Exception ex) {
                logger.error(ex);
            }

            synchronized (this) {

                logger.info("Processing billing logging query file..");
                try {

                    try {

                        File BILLING_OUTGOING_DATA_FILE = new File("/home/sevas/billing_outgoing.sql");
                        String billingQueryBegin = "INSERT INTO delivered_outgoing_messages "
                                + "(message_id,sender,recipient,network,message,delivery_status,"
                                + "serv_name,src_module,smsc,serv_id,service_sub_type, sent_time) VALUES";

                        //Log billing transaction.
                        inputStream = new FileInputStream(BILLING_OUTGOING_DATA_FILE);
                        updateQuery = IOUtils.toString(inputStream);
                        if (updateQuery.length() > 0 && !updateQuery.isEmpty()) {
                            statement = conn.prepareStatement(
                                    billingQueryBegin
                                            + updateQuery.substring(0, updateQuery.length() - 1));
                            int update = statement.executeUpdate();
                            if (update > 0) {
                                //Empty the file...
                                logger.info("Emptying billing query file..");
                                try (PrintWriter writer = new PrintWriter(BILLING_OUTGOING_DATA_FILE)) {
                                    writer.print("");
                                }
                            }
                        }

                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    } catch (FileNotFoundException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    try {

                        logger.info("Processing content logging query file..");
                        //Log content transaction.
                        File CONTENT_OUTGOING_DATA_FILE = new File("/home/sevas/content_outgoing.sql");
                        String contentLoginQueryBegin
                                = "INSERT INTO delivered_outgoing_messages ("
                                + "message_id,sender,recipient,network,message,"
                                + "delivery_status,serv_name,src_module,smsc,"
                                + "serv_id,service_sub_type,sent_time) VALUES";

                        inputStream = new FileInputStream(CONTENT_OUTGOING_DATA_FILE);

                        updateQuery = IOUtils.toString(inputStream);

                        if (updateQuery.length() > 0) {

                            statement = conn.prepareStatement(
                                    contentLoginQueryBegin + updateQuery.substring(0, updateQuery.length() - 1));
                            int update = statement.executeUpdate();
                            if (update > 0) {
                                //Empty the file...
                                logger.info("Emptying content query file..");
                                try (PrintWriter writer = new PrintWriter(CONTENT_OUTGOING_DATA_FILE)) {
                                    writer.print("");
                                }
                            }

                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                } finally {
                    try {
                        inputStream.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }

            }

            try {
                irh = new iReportHelper(getListenerDBConnection());
            } catch (Exception ex) {
                logger.error(ex);
            }

            //Revenue Report....
            //Monthly target revenue...
            try {
                monthTargetRev = irh.getRevTarget(context.getInitParameter("monthlyRevTarget"));
                context.setAttribute("monthlyRevTarget", monthTargetRev);
            } catch (Exception ex) {
            }

            //incaption till date...
            try {
                inceptionTillDateRev = irh.getPeriodicRev(context.getInitParameter("incepTillDateRev"));
                logger.info("Inception Till DAte Revenue JSON DATA" + inceptionTillDateRev);


                context.setAttribute("incepTillDateRevDATA", inceptionTillDateRev);

                logger.info("incepTillDateRevDATA JSON DATA from Context" + context.getAttribute("incepTillDateRevDATA"));

            } catch (Exception e) {
                e.printStackTrace();
            }

            //actual rev..
            try {
                actualRev = irh.getPeriodicRev(context.getInitParameter("monthTillDateRev"));

                logger.info("Actual Revenue JSON DATA" + actualRev);
                context.setAttribute("monthTillDateRevDATA", actualRev);

                logger.info("Actual Revenue JSON DATA from Context" + context.getAttribute("monthTillDateRevDATA"));

            } catch (Exception e) {

            }

            //today rev..
            try {
                todayRev = irh.getPeriodicRev(context.getInitParameter("todayRev"));

                logger.info("Today's Revenue JSON DATA" + todayRev);
                context.setAttribute("todayRevDATA", todayRev);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                //Daily Service Revenue Report...
                statement = conn.prepareStatement(context.getInitParameter("dailyServiceRevenue_All"));
                dailyServiceRevenue_All = statement.executeQuery();
                context.setAttribute("dailyServiceRevenue_All", dailyServiceRevenue_All);

                logger.info("dailyServiceRevenue_All JSON DATA from Context" + context.getAttribute("dailyServiceRevenue_All"));

            } catch (SQLException ex) {
                ex.printStackTrace();

            }

            try {
                //daily churn..
                dailyChurn = irh.getChurnRate(context.getInitParameter("churnQuery"));
                context.setAttribute("churnQuery", dailyChurn);

                logger.info("churnQuery from Context" + context.getAttribute("churnQuery"));

            } catch (SQLException ex) {
                ex.printStackTrace();
            }

            //dailyPaidContentRevenue
            logger.info(inceptionTillDateRev);
            logger.info(todayRev);
            logger.info(monthTargetRev);
            logger.info(actualRev);
            logger.info(dailyChurn);

            //Subscription Report...
            Long totalSubscribers;
            Long activeSubscribers;
            Long inActiveSubscribers;
            Long todayAquisition;
            Long monthTillDateAcquisition;
            Long lastMonthAquisition;
            Long todayOptOut;

            try {
                totalSubscribers = irh.getSubscription(
                        "select count(*) from subscription");
                context.setAttribute("totalSubscribers", totalSubscribers);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                activeSubscribers = irh.getSubscription(
                        "select count(*) from subscription where sub_status = 'active' and sub_Service_id > 0 ");
                context.setAttribute("activeSubscribers", activeSubscribers);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                inActiveSubscribers = irh.getSubscription(
                        "select count(*) from subscription where sub_status = 'inactive' ");
                context.setAttribute("inActiveSubscribers", inActiveSubscribers);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                todayAquisition = irh.getSubscription(
                        "select count(*) from subscription where date(sub_request_time) = date(now()) and sub_Service_id > 0 ");
                context.setAttribute("todayAquisition", todayAquisition);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                todayOptOut = irh.getSubscription(
                        "select count(*) from subscription where date(un_sub_date) = date(now()) ");
                context.setAttribute("todayOptOut", todayOptOut);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Long todayActivated;

            Long todayPedingConfirmation;

            try {
                todayActivated = irh.getSubscription(
                        "select count(*) from subscription where date(sub_request_time) = date(now()) "
                                + "and sub_status ='active' and sub_Service_id > 0 ");
                context.setAttribute("todayActivated", todayActivated);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                todayPedingConfirmation = irh.getSubscription(
                        "select count(*) from subscription where date(sub_request_time) = date(now()) and sub_status ='inactive'");
                context.setAttribute("todayPedingConfirmation", todayPedingConfirmation);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                monthTillDateAcquisition = irh.getSubscription(
                        "select count(*) from subscription where "
                                + "extract(month from sub_request_time) = extract(month from now()) "
                                + "and extract(year from sub_request_time) = extract(year from now()) "
                                + "and sub_Service_id > 0 ");
                context.setAttribute("monthTillDateAcquisition", monthTillDateAcquisition);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                lastMonthAquisition = irh.getSubscription(
                        "select count(*) from subscription "
                                + "where EXTRACT(month from sub_request_time) =  "
                                + "EXTRACT(month FROM CURRENT_DATE - '1 month'::interval) and sub_Service_id > 0 ");
                context.setAttribute("lastMonthAquisition", lastMonthAquisition);

            } catch (Exception e) {
                e.printStackTrace();
            }

            //Daily paid content...
            try {
                dailyPaidContent = irh.getDailyPaidContent(context.getInitParameter("dailyPaidContentRevenue"));
                context.setAttribute("dailyPaidContent", dailyPaidContent);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                monthTillDateContentLogs = irh.queryLogs(conn);
                context.setAttribute("monthTillDateContentLogs", monthTillDateContentLogs);
            } catch (Exception E) {

            }

            try {
                todayContentLogs = irh.todayContentReport(conn);
                context.setAttribute("todayContentLogs", todayContentLogs);
            } catch (Exception E) {

            }
        }

    }
}
