package org.esme.listeners;

import org.apache.log4j.Logger;
import org.esme.dao.models.Users;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class UsersListener extends ListenersBroker implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(UsersListener.class);
    Users users;
    ArrayList<Users> usersCache;
    int counter;
    ArrayList categoryList;

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.info("UsersListener started");
        //Create a datasource for pooled connections.
//        datasource = (DataSource) sce.getServletContext().getAttribute("dbPool");
//        context = sce.getServletContext();
        updateUsers(sce.getServletContext());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void updateUsers(ServletContext _context) {
        logger.info("Updating users...");
        usersCache = new ArrayList<Users>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getListenerDBConnection().createStatement();
            resultSet = statement.executeQuery(_context.getInitParameter("usersQuery"));
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                //update previously stored job contents from the container...
                while (resultSet.next()) {
                    users = new Users();
                    users.setId(resultSet.getInt("id"));
                    users.setUserName(resultSet.getString("username"));
                    users.setPassword(resultSet.getString("password"));
                    usersCache.add(users);
                }
                //Create cache of all users
                logger.info("Total Users " + usersCache.size());
                _context.setAttribute("usersCache", usersCache);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
