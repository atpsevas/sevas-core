package org.esme.listeners;

import org.apache.log4j.Logger;
import org.esme.dao.models.UserRole;
import org.esme.dao.models.Users;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class UserRoleListener extends ListenersBroker implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(UsersListener.class);
    UserRole userRole;
    Users users;
    ArrayList<UserRole> usersRoleCache;
    int counter;
    ArrayList categoryList;

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.info("UsersListener started");
        //Create a datasource for pooled connections.
        datasource = (DataSource) sce.getServletContext().getAttribute("dbPool");
        context = sce.getServletContext();
        updateUsers(context);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void updateUsers(ServletContext sce) {
        logger.info("Updating user role...");
        usersRoleCache = new ArrayList<UserRole>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getListenerDBConnection().createStatement();
            resultSet = statement.executeQuery(context.getInitParameter("userRoleQuery"));
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                //update previously stored job contents from the container...
                while (resultSet.next()) {
                    userRole = new UserRole();

                    userRole.setRoleID(resultSet.getInt("id"));
                    userRole.setRoleName(resultSet.getString("role"));
                    userRole.setUserName(resultSet.getString("username"));
                    usersRoleCache.add(userRole);
                }
                //Create cache of all users
                logger.info("Total Role " + usersRoleCache.size());
                context.setAttribute("userRoleCache", usersRoleCache);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
