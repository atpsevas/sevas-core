package org.esme.listeners;

import org.apache.log4j.Logger;
import org.esme.broker.AppBroker;
import org.esme.controllers.helpers.RabbitMQPublisher;
import org.esme.gateway.kannel.WEB2SMS;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ListenersBroker extends AppBroker implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(ListenersBroker.class);
    static ServletContext context;
    static DataSource datasource;
    String dbURL;
    String dbUserName;
    String dbPassword;
    Connection connection;
    WEB2SMS webSMS;
    String network;

    @Override
    @SuppressWarnings("CallToThreadDumpStack")
    public void contextInitialized(ServletContextEvent sce) {

        try {
            Context envCtx = (Context) new InitialContext().lookup("java:comp/env");
            datasource = (DataSource) envCtx.lookup("jdbc/sevas");
            context = getServletContext();
            context.setAttribute("dbPool", datasource);
            context.setAttribute("database.connection", getListenerDBConnection());
            logger.info("Listener Broker Stated");
        } catch (Exception e) {
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

        // Attempt to Close RabbitMQ Connection
        RabbitMQPublisher.closeRabbitConnection();
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace"})
    public synchronized Connection getListenerDBConnection() {
        try {
            if (connection == null) {
                connection = AppBroker.getDBConnection(getPropertyFileHandle());
                // Allocate and use a connection from the pool
//                connection = datasource.getConnection();
                logger.info("We were able to use the pool.");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            try {
                dbURL = context.getInitParameter("dbURL");
                dbUserName = context.getInitParameter("dbUserName");
                dbPassword = context.getInitParameter("dbPassword");
                connection = DriverManager.getConnection(dbURL, dbUserName, dbPassword);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    //get Send SENDSMS
    public WEB2SMS getSendSMS() {
        return webSMS = WEB2SMS.getInstance();
    }

    public String formatText(String text) {
        String formattedContent;
        String myString = text.trim();
        byte[] ptext;
        try {
            ptext = myString.getBytes("ISO-8859-1");
            formattedContent = new String(ptext, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            return text;
        }
        return formattedContent;
    }

    public String formatDateToString(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }

    public int getCurrentHour() {
        Date date = new Date();   // given date
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(date);   // assigns calendar to given date
        calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
        calendar.get(Calendar.HOUR);        // gets hour in 12h format
        calendar.get(Calendar.MONTH);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

}
