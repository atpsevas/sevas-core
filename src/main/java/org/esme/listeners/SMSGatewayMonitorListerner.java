package org.esme.listeners;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.*;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

public class SMSGatewayMonitorListerner extends ListenersBroker implements ServletContextListener {

    Timer timer;

    @SuppressWarnings({"CallToThreadDumpStack", "ConvertToTryWithResources"})
    public static String executeLinuxCommand(String command, boolean waitForResponse)
            throws IOException, InterruptedException {
        //Convert the file to SLN...
        String response = "";
        ProcessBuilder pb = new ProcessBuilder("bash", "-c", command);
        pb.redirectErrorStream(true);
        //org.esme.broker.AppBroker.getSevasLogger().info("Linux command: " + command);
        Process shell = pb.start();
        if (waitForResponse) {
            // To capture output from the shell
            InputStream shellIn = shell.getInputStream();
            // Wait for the shell to finish and get the return code
            shell.waitFor();
            //org.esme.broker.AppBroker.getSevasLogger().info("Exit runGatewayRestartCommands" + shellExitStatus);
            response = convertStreamToStr(shellIn);
            shellIn.close();
            //We change the file permission here...
        }
        return response;
    }

    public static String convertStreamToStr(InputStream is) throws IOException {
        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is,
                        "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        org.esme.broker.AppBroker.getSevasLogger().info("SMSGatewayMonitorListerner Started");
        context = sce.getServletContext();
        scheduleTimer();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        timer.cancel();
    }

    public void scheduleTimer() {
        timer = new Timer();
        //Run every 15 minutes...
        timer.scheduleAtFixedRate(new RemindTask(), 150000, 900000);
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public String sendGatewayStatusDetails(
            String sender, String recipient, String mailTitle,
            String mailDescription) throws MessagingException {
        String result = null;

        Properties props = new Properties();
        props.put("mail.smtp.user", "username");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "25");
        props.put("mail.debug", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.EnableSSL.enable", "true");

        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.port", "465");
        props.setProperty("mail.smtp.socketFactory.port", "465");
        Authenticator auth = new SMTPAuthenticator("saheedalbert@gmail.com", "wot15bir");
        Session session = Session.getInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);
        msg.setText(mailDescription);
        msg.setSubject(mailTitle);
        msg.setFrom(new InternetAddress(sender));
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        Transport.send(msg);
        return result;
    }

    @SuppressWarnings("UseSpecificCatch")
    public boolean checkGatewayStatus() {
        // Send a GET request to the subscription servlet
        boolean result = true;
        try {
            // Construct data
            @SuppressWarnings("StringBufferMayBeStringBuilder")
            // Send data
                    String urlStr = context.getInitParameter("smsGatewayIP") + ":" + context.getInitParameter("adminPort");
            URL url = new URL(urlStr);
            URI uri = new URI(
                    url.getProtocol(),
                    url.getUserInfo(),
                    url.getHost(),
                    url.getPort(),
                    url.getPath(),
                    url.getQuery(),
                    url.getRef());
            url = uri.toURL();
            URLConnection conn = url.openConnection();
            conn.connect();
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {
            //org.esme.broker.AppBroker.getSevasLogger().info( "Checking SMS Gateway status...");
//            if (!checkGatewayStatus()) {
//                runGatewayRestartCommands();
//            }
        }

        @SuppressWarnings({"CallToThreadDumpStack", "UseSpecificCatch"})
        public void runGatewayRestartCommands() {
            String response = "";
            try {
                response = executeLinuxCommand(
                        "/home/sevas/apps/sms_gateway_14/sbin/bearerbox /home/sevas/kannel_conf/kannel.conf &", true);
                Thread.sleep(5000);
                response = executeLinuxCommand(
                        "/home/sevas/apps/sms_gateway_14/sbin/smsbox /home/sevas/kannel_conf/kannel.conf &", true);
            } catch (Exception ex) {
                try {
                    sendGatewayStatusDetails(
                            context.getInitParameter("adminEmail"),
                            context.getInitParameter("adminEmail"),
                            "RESTART SMS GATEWAY FAILED!!!",
                            "SMS Gateway was down. An attemp to restart it FAILED!");
                } catch (MessagingException ex1) {
                    //We will use bulk sms API here...
                }
                return;
            }

            try {
                sendGatewayStatusDetails(
                        context.getInitParameter("adminEmail"),
                        context.getInitParameter("adminEmail"),
                        "RESTART SMS GATEWAY SUCCESSFUL!!!",
                        "SMS Gateway was down. An attemp to restart it was SUCCESSFUL!");
            } catch (MessagingException ex1) {
                //We will use bulk sms API here...
            }
        }
    }

    private class SMTPAuthenticator extends Authenticator {

        @SuppressWarnings("FieldMayBeFinal")
        private PasswordAuthentication authentication;

        public SMTPAuthenticator(String login, String password) {
            authentication = new PasswordAuthentication(login, password);
        }

        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            return authentication;
        }
    }
}
