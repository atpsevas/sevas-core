package org.esme.listeners;

import org.esme.dao.models.ServiceCategory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ServiceCategoryListener extends ListenersBroker implements ServletContextListener {

    ServiceCategory serviceCategory;
    ArrayList<ServiceCategory> serviceCategoryCache;
    int counter;
    ArrayList categoryList;
    Timer timer;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        datasource = (DataSource) sce.getServletContext().getAttribute("dbPool");
        context = sce.getServletContext();
        scheduleTimer();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        timer.cancel();
    }

    public void scheduleTimer() {
        timer = new Timer();
        //Run every 24HRS!
        timer.scheduleAtFixedRate(new ServiceCategoryListener.RemindTask(), 0, 86400000);
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {
            updateCategory(context);
        }

        @SuppressWarnings({"CallToThreadDumpStack", "Convert2Diamond", "null"})
        public void updateCategory(ServletContext sce) {

            org.esme.broker.AppBroker.getSevasLogger().info("Updating Category..");
            serviceCategoryCache = new ArrayList<ServiceCategory>();
            Statement statement = null;
            ResultSet resultSet = null;

            try {

                statement = getListenerDBConnection().createStatement();
                resultSet = statement.executeQuery(context.getInitParameter("categoryQuery"));
                //We place a lock on the content cache untill it finishes updating....
                synchronized (this) {

                    //update previously stored job contents from the container...
                    while (resultSet.next()) {

                        serviceCategory = new ServiceCategory();
                        serviceCategory.setCategoryID(resultSet.getLong("id"));
                        serviceCategory.setCategoryName(resultSet.getString("category_name"));
                        serviceCategory.setCategoryKeyword(resultSet.getString("category_keyword"));
                        serviceCategory.setCategoryDateCreated(resultSet.getString("date_created"));
                        serviceCategoryCache.add(serviceCategory);
                    }
                    //Create cache of all users
                    org.esme.broker.AppBroker.getSevasLogger().info("Total Category " + serviceCategoryCache.size());
                    context.setAttribute("totalCategory", serviceCategoryCache.size());
                    context.setAttribute("categoryCache", serviceCategoryCache);
                }

            } catch (Exception ex) {
                org.esme.broker.AppBroker.getSevasLogger().info(ex.getMessage());
            }
        }
    }

}
