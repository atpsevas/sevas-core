package org.esme.listeners;

import org.apache.log4j.Logger;
import org.esme.dao.models.ActiveSubscription;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Timer;

public class ActiveSubscriptionListener extends ListenersBroker implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(ActiveSubscriptionListener.class);
    ActiveSubscription activeSubscription;
    ArrayList<ActiveSubscription> activeSsubscriptionCache;
    int counter;
    ArrayList categoryList;
    Timer timer;

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
//        datasource = (DataSource) sce.getServletContext().getAttribute("dbPool");
//        context = sce.getServletContext();
        updateActiveSubscription();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "Convert2Diamond"})
    public void updateActiveSubscription() {

        logger.info("Updating Active Subscription cache...");

        try {
            if (activeSsubscriptionCache.size() > 0) {
                activeSsubscriptionCache.clear();
            }
        } catch (Exception e) {
        }
        activeSsubscriptionCache = new ArrayList<ActiveSubscription>();
        Statement statement;
        ResultSet dataResultSet;
        try {

            statement = getDBConnection().createStatement();
            dataResultSet = statement.executeQuery(
                    context.getInitParameter("activeSubscriptionQuery"));

            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                while (dataResultSet.next()) {
                    activeSubscription = new ActiveSubscription();
                    activeSubscription.setSubscriberMSISDN(dataResultSet.getString("sub_msisdn"));
                    activeSubscription.setSubscriberNetwork(dataResultSet.getString("sub_network"));
                    activeSubscription.setSubscriberServiceExpiryDate(dataResultSet.getString("sub_expiry_date"));
                    activeSubscription.setSubscriberServiceID(dataResultSet.getLong("id"));
                    activeSubscription.setSubscriberServiceMOShortCode(dataResultSet.getString("service_shortcode"));
                    activeSubscription.setSubscriberServiceMTShortCode(dataResultSet.getString("service_mt_shortcode"));
                    activeSubscription.setSubscriberServiceName(dataResultSet.getString("service_name"));
                    activeSubscription.setSubscriberServiceOwnerID(dataResultSet.getLong("service_owner"));
                    activeSubscription.setSubscriberServicePeriod(dataResultSet.getLong("sub_service_period"));
                    activeSubscription.setSubscriberServiceRequestTime(dataResultSet.getString("sub_request_time"));
                    activeSubscription.setSubscriberServiceTimeOfBlast(dataResultSet.getString("sub_tob"));
                    activeSubscription.setSubscriberWelcomeShortCode(dataResultSet.getString("welcome_shortcode"));
                    activeSubscription.setSubscriberServiceUnsubscribeKeyword(dataResultSet.getString("service_unsubcribe_keyword"));
                    activeSubscription.setSubscriberServiceCategoryKeyword(dataResultSet.getString("service_category_keyword"));
                    activeSubscription.setSubscriberUnsubMessage(dataResultSet.getString("service_unsubscription_msg"));
                    activeSubscription.setSubscriberReminderMessage(dataResultSet.getString("service_reminder_msg"));
                    activeSubscription.setSubscriberReminderPeriod(dataResultSet.getInt("service_reminder_period"));
                    activeSubscription.setSubcriberContentDeliveryMethod(dataResultSet.getString("sub_content_delivery_method"));
                    activeSubscription.setSubcriberServiceType(dataResultSet.getString("service_sub_type"));
                    activeSsubscriptionCache.add(activeSubscription);
                }
                context.setAttribute("activeSubscriptionCountCache", activeSsubscriptionCache.size());
                context.setAttribute("activeSubscriptionCache", activeSsubscriptionCache);
                logger.info("TOTAL ACTIVE SUB " + activeSsubscriptionCache.size());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
