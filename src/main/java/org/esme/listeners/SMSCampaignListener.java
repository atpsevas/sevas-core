package org.esme.listeners;

import org.esme.dao.models.SMSCampaign;

import javax.naming.Context;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class SMSCampaignListener extends ListenersBroker implements ServletContextListener {

    SMSCampaign campaign;
    ArrayList<SMSCampaign> campaignCache;
    int counter;
    ArrayList categoryList;
    Context envCtx;
    Timer timer;

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        org.esme.broker.AppBroker.getSevasLogger().info("SMSCampaignListener Started...");
//        datasource = (DataSource) sce.getServletContext().getAttribute("dbPool");
//        context = sce.getServletContext();
        scheduleTimer();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        timer.cancel();
    }

    public void scheduleTimer() {

        timer = new Timer();
        //Run every 15 minutes! with delay of 0 minute...
        timer.scheduleAtFixedRate(new RemindTask(), 0, 900000);
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {
            updateSMSCampaignCache(context);
        }

        @SuppressWarnings("CallToThreadDumpStack")
        public void updateSMSCampaignCache(ServletContext sce) {
            org.esme.broker.AppBroker.getSevasLogger().info("Updating Campaign...");
            campaignCache = new ArrayList<SMSCampaign>();
            Statement statement = null;
            ResultSet resultSet = null;
            try {

                statement = getListenerDBConnection().createStatement();
                resultSet = statement.executeQuery(context.getInitParameter("smsCampaignReportQuery"));

                //We place a lock on the content cache untill it finishes updating....
                synchronized (this) {
                    while (resultSet.next()) {

                        campaign = new SMSCampaign();
                        campaign.setCampaignID(resultSet.getLong("campaign_id"));
                        campaign.setCampaignUploadDate(resultSet.getString("campaign_upload_date"));
                        campaign.setCampaignSenderID(resultSet.getString("campaign_sender_id"));
                        campaign.setCampaignContent(resultSet.getString("campaign_content"));
                        campaign.setCampaignTargetFileDir(resultSet.getString("campaign_target_file_dir"));
                        campaign.setCampaignSentDate(resultSet.getString("campaign_sent_date"));
                        campaign.setCampaignSentTime(resultSet.getLong("campaign_sent_time"));
                        campaign.setCampaignOnwerID(resultSet.getLong("campaign_owner"));
                        campaign.setCampaignTotalTarget(resultSet.getLong("campaign_total_target_recipients"));
                        campaign.setCampaignTotalSent(resultSet.getLong("campaign_total_sent"));
                        campaign.setCampaignActualStartTime(resultSet.getString("campaign_actual_start_time"));
                        campaign.setCampaignActualEndTime(resultSet.getString("campaign_actual_end_time"));
                        campaign.setCampaignStatus(resultSet.getString("campaign_status"));
                        campaign.setCampaignServiceID(resultSet.getLong("campaign_service_id"));
                        campaignCache.add(campaign);
                    }
                    //Create cache of Campaign smsCampaignCache
                    org.esme.broker.AppBroker.getSevasLogger().info("TOTAL SMS CAMPAIGN " + campaignCache.size());
                    context.setAttribute("totalSMSCampaign", campaignCache.size());
                    context.setAttribute("smsCampaignCache", campaignCache);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
