package org.esme.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.*;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.Timer;
import java.util.TimerTask;

public class SMSGatewayStatusReportListener extends ListenersBroker implements ServletContextListener {

    Timer timer;

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        org.esme.broker.AppBroker.getSevasLogger().info("SMSGatewayStatusReportListener Started");
        context = sce.getServletContext();
        scheduleTimer();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        timer.cancel();
    }

    public void scheduleTimer() {
        timer = new Timer();
        //Run every hour...with delay of 2 minutes
        timer.scheduleAtFixedRate(new RemindTask(), 0, 3600000);
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void checkGatewayStatus() {
        // Send a GET request to the subscription servlet
        String result;
        File responseFile = new File("/home/sevas/SMSGateway_Report.xml");
        if (!responseFile.exists()) {
            try {
                responseFile.createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        BufferedWriter out;
        URL url;
        URI uri;
        URLConnection conn;
        BufferedReader rd;
        StringBuffer sb;
        try {

            // Construct data
            @SuppressWarnings("StringBufferMayBeStringBuilder")

            // Send data
                    String urlStr = "http://" + context.getInitParameter("smsGatewayIP")
                    + ":" + context.getInitParameter("adminPort") + "/status.xml";

            url = new URL(urlStr);
            uri = new URI(
                    url.getProtocol(),
                    url.getUserInfo(),
                    url.getHost(),
                    url.getPort(),
                    url.getPath(),
                    url.getQuery(),
                    url.getRef());

            url = uri.toURL();
            conn = url.openConnection();
            conn.connect();
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            sb = new StringBuffer();
            String line;

            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }

            result = sb.toString();
            //write the reponse to a file
            out = new BufferedWriter(new FileWriter(responseFile));
            org.esme.broker.AppBroker.getSevasLogger().info(responseFile.getAbsolutePath());
            out.write(result);
            out.flush();

        } catch (Exception e) {
            org.esme.broker.AppBroker.getSevasLogger().info(e.getMessage());
        }
    }

    class RemindTask extends TimerTask {
        @Override
        public void run() {
            org.esme.broker.AppBroker.getSevasLogger().info("Querying SMS Gateway report...");
            checkGatewayStatus();
        }
    }
}
