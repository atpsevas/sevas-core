package org.esme.listeners;

import org.esme.dao.models.Network;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class NetworksListener extends ListenersBroker implements ServletContextListener {

    Network theNetwork;
    ArrayList<Network> networkCache;
    int counter;
    ArrayList categoryList;
    Timer timer;
    Connection conn;

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        updateNetwork(context);
        // scheduleTimer();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        timer.cancel();
    }

    public void scheduleTimer() {
        timer = new Timer();
        //Run every 24HRS!
        timer.scheduleAtFixedRate(new NetworksListener.RemindTask(), 0, 86400000);
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void updateNetwork(ServletContext sce) {
        org.esme.broker.AppBroker.getSevasLogger().info("Updating networks..");
        networkCache = new ArrayList<Network>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getListenerDBConnection().createStatement();
            resultSet = statement.executeQuery(context.getInitParameter("networksQuery"));
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                //update previously stored job contents from the container...
                while (resultSet.next()) {
                    theNetwork = new Network();
                    theNetwork.setId(resultSet.getInt("id"));
                    theNetwork.setNetworkID(resultSet.getString("network_id"));
                    theNetwork.setPrefixes(resultSet.getString("network_prefixes"));
                    networkCache.add(theNetwork);
                }
                //Create cache of all users
                org.esme.broker.AppBroker.getSevasLogger().info("Total Network " + networkCache.size());
                context.setAttribute("networksCache", networkCache);
            }

        } catch (Exception ex) {
            org.esme.broker.AppBroker.getSevasLogger().info(ex.getMessage());
        }
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {
            updateNetwork(context);
        }

    }
}
