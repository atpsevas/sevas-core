package org.esme.listeners;

import org.apache.log4j.Logger;
import org.esme.dao.models.Content;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ContentListener extends ListenersBroker implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(ContentListener.class);
    Content content;
    ArrayList<Content> contentCache;
    int counter;
    Timer timer;

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.info("Starting Content Listener");
        updateContentCache(context);
//        scheduleTimer();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        timer.cancel();
    }

    public void scheduleTimer() {
        timer = new Timer();
        //Run every HOUR! With daily of 15 minutes...
        timer.scheduleAtFixedRate(new RemindTask(), 0, 3600000);

    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "null", "Convert2Diamond"})
    public void updateContentCache(ServletContext sce) {

        org.esme.broker.AppBroker.getSevasLogger().info("Updating content archive...");
        contentCache = new ArrayList<Content>();
        Statement statement = null;
        ResultSet resultSet = null;

        try {

            statement = getListenerDBConnection().createStatement();
            resultSet = statement.executeQuery(
                    context.getInitParameter("contentQuery"));

            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                while (resultSet.next()) {
                    try {
                        content = new Content();
                        content.setContent_id(resultSet.getInt("content_id"));
                        content.setContentServiceName(resultSet.getString("service_name"));
                        content.setContent(resultSet.getString("content"));
                        content.setDateUploaded(resultSet.getString("content_upload_date"));
                        content.setContentOwner(resultSet.getInt("content_provider_id"));
                        content.setContentService(resultSet.getLong("content_service_id"));
                        content.setConentDOB(resultSet.getString("content_date_of_blast"));
                        contentCache.add(content);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //Create cache of content with all
                context.setAttribute("totalContents ", contentCache.size());
                context.setAttribute("contentCache", contentCache);
            }

        } catch (Exception ex) {
            org.esme.broker.AppBroker.getSevasLogger().info("Error while updating active content cache.");
            ex.printStackTrace();
        }
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {
            updateContentCache(context);
        }

    }
}
