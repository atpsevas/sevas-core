package org.esme.listeners;

import org.apache.log4j.Logger;
import org.esme.dao.models.Blacklist;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class BlacklistListener extends ListenersBroker implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(BlacklistListener.class);
    Blacklist blacklist;
    ArrayList<Blacklist> activeBlacklistCache;
    Timer timer;

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        scheduleTimer();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        timer.cancel();
    }

    public void scheduleTimer() {
        Date d = new Date();
        d.setHours(0);
        d.setMinutes(0);
        d.setSeconds(0);
        timer = new Timer();
        timer.scheduleAtFixedRate(new BlacklistListener.RemindTask(), 0, 3600000);
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {
            updateBlacklist();
        }

        @SuppressWarnings({"CallToThreadDumpStack", "Convert2Diamond"})
        public void updateBlacklist() {
            logger.info("Updating Blacklist cache...");
            activeBlacklistCache = new ArrayList<Blacklist>();
            Statement statement = null;
            ResultSet dataResultSet = null;
            try {

                statement = getListenerDBConnection().createStatement();
                dataResultSet = statement.executeQuery(
                        context.getInitParameter("blacklist"));

                //We place a lock on the content cache untill it finishes updating....
                synchronized (this) {
                    while (dataResultSet.next()) {
                        blacklist = new Blacklist();
                        blacklist.setBlacklistedMSISDN(dataResultSet.getString("sub_msisdn"));
                        blacklist.setBlacklistDate(dataResultSet.getString("blacklisted_date"));
                        blacklist.setBlacklistedNetwork(dataResultSet.getString("sub_network"));
                        activeBlacklistCache.add(blacklist);
                    }
                    context.setAttribute("activeBlacklistCacheCount", activeBlacklistCache.size());
                    context.setAttribute("activeBlacklistCache", activeBlacklistCache);
                    logger.info("TOTAL Blacklist " + activeBlacklistCache.size());
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
