package org.esme.listeners;

import org.apache.log4j.Logger;
import org.esme.dto.Service;

import javax.naming.Context;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ServicesListener extends ListenersBroker implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(ServicesListener.class);
    Service service;
    int counter;
    ArrayList categoryList;
    Context envCtx;
    Timer timer;

    public static void updateServletServicesCache(ServletContextEvent sce) {


        ArrayList<Service> _servicesCache = Service.getAllServices();

        logger.info("Updating Servlet Enviroment Service Cache...");

        //Create cache of services with all
        sce.getServletContext().setAttribute("totalService", _servicesCache.size());
        sce.getServletContext().setAttribute("serviceCache", _servicesCache);
    }

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        updateServletServicesCache(sce);
//        scheduleTimer();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        timer.cancel();
    }

    public void scheduleTimer() {
        timer = new Timer();
        //Run every 24HRS! At Midnight
        timer.scheduleAtFixedRate(new RemindTask(), 0, 86400000);
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {
            logger.info("Updating Services...");

            updateServicesCache();
        }

    }
}
