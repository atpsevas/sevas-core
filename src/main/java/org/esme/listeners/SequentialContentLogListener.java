package org.esme.listeners;

import org.esme.dao.models.SequentialContentLog;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Timer;

public class SequentialContentLogListener extends ListenersBroker implements ServletContextListener {

    SequentialContentLog seqCotentLog;
    ArrayList<SequentialContentLog> seqContentLogCache;
    int counter;
    Timer timer;

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
//        datasource = (DataSource) sce.getServletContext().getAttribute("dbPool");
//        context = sce.getServletContext();
        updateSeqContentLogCache(context);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        timer.cancel();
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void updateSeqContentLogCache(ServletContext sce) {
        org.esme.broker.AppBroker.getSevasLogger().info("Updating SequentialContentLogCache...");
        seqContentLogCache = new ArrayList<SequentialContentLog>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getListenerDBConnection().createStatement();
            resultSet = statement.executeQuery(
                    context.getInitParameter("seqcontentLogQuery"));

            //We place a lock on the seqCotentLog cache untill it finishes updating....
            synchronized (this) {
                while (resultSet.next()) {
                    try {
                        seqCotentLog = new SequentialContentLog();
                        seqCotentLog.setSubscriber(resultSet.getString("subscriber"));
                        seqCotentLog.setContentServiceID(resultSet.getLong("content_service_id"));
                        seqCotentLog.setContentStartDate(resultSet.getString("content_start_date"));
                        seqCotentLog.setLastContentDate(resultSet.getString("last_content_date"));
                        seqCotentLog.setLastContentDay(resultSet.getLong("last_content_day"));
                        seqContentLogCache.add(seqCotentLog);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //Create cache of seqCotentLog with all
                context.setAttribute("totalSeqContentLogCacheSize", seqContentLogCache.size());
                context.setAttribute("totalSeqContentLogCache", seqContentLogCache);
            }

        } catch (Exception ex) {
            org.esme.broker.AppBroker.getSevasLogger().info(ex.getMessage());
        }
    }

}
