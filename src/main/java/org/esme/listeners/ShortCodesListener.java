package org.esme.listeners;

import org.esme.dao.models.ShortCode;

import javax.naming.Context;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ShortCodesListener extends ListenersBroker implements ServletContextListener {

    ShortCode shortCodes;
    ArrayList<ShortCode> shortCodesCache;
    int counter;
    ArrayList categoryList;
    Context envCtx;
    Timer timer;
    Connection conn;

    @SuppressWarnings("CallToThreadDumpStack")
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        updateShortCodesCache();
//        scheduleTimer();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        timer.cancel();
    }

    public void scheduleTimer() {
        timer = new Timer();
        //Run every 24HRS! 
        timer.scheduleAtFixedRate(new RemindTask(), 0, 86400000);
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void updateShortCodesCache() {
        org.esme.broker.AppBroker.getSevasLogger().info("Updating Short Codes...");
        shortCodesCache = new ArrayList<ShortCode>();
        Statement statement;
        ResultSet resultSet;
        try {

            statement = getListenerDBConnection().createStatement();
            resultSet = statement.executeQuery(context.getInitParameter("shortCodeQuery"));
            //We place a lock on the  cache untill it finishes updating....
            synchronized (this) {

                while (resultSet.next()) {
                    shortCodes = new ShortCode();
                    shortCodes.setID(resultSet.getLong("id"));
                    shortCodes.setShortCodeID(resultSet.getString("shortcode_id"));
                    shortCodes.setDateCreated(resultSet.getString("shortcode_datecreated"));
                    shortCodesCache.add(shortCodes);
                }
                //Create cache of services with all
                System.err.append("TOTAL SHORT CODE: " + shortCodesCache.size());
                context.setAttribute("shortCodesCache", shortCodesCache);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    class RemindTask extends TimerTask {
        @Override
        public void run() {
            updateShortCodesCache();
        }
    }
}
