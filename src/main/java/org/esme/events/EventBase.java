/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.events;

/**
 * @author tm30
 */

import org.esme.observers.ObserverInterface;

import java.util.ArrayList;
import java.util.List;

public class EventBase implements EventInterface {

    private final Object MUTEX = new Object();
    private List<ObserverInterface> observers;
    private String message;
    private boolean changed;

    public EventBase() {
        this.observers = new ArrayList<>();
    }

    public void register(ObserverInterface obj) {
        if (obj == null) throw new NullPointerException("Null Observer");
        synchronized (MUTEX) {
            if (!observers.contains(obj)) observers.add(obj);
        }
    }

    public void unregister(ObserverInterface obj) {
        synchronized (MUTEX) {
            observers.remove(obj);
        }
    }

    public void notifyObservers() {
        List<ObserverInterface> observersLocal = null;
        //synchronization is used to make sure any observer registered after message is received is not notified
        synchronized (MUTEX) {
            if (!changed)
                return;
            observersLocal = new ArrayList<>(this.observers);
            this.changed = false;
        }
        for (ObserverInterface obj : observersLocal) {
            obj.update();
        }

    }

    public Object getUpdate(ObserverInterface obj) {
        return this.message;
    }

    //method to post message to the topic
    public void postMessage(String msg) {
        System.out.println("Message Posted to Topic:" + msg);
        this.message = msg;
        this.changed = true;
        notifyObservers();
    }

}