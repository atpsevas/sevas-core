/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.events;

import org.esme.observers.ObserverInterface;

/**
 * @author tm30
 */
public interface EventInterface {

    //methods to register and unregister observers
    public void register(ObserverInterface obj);

    public void unregister(ObserverInterface obj);

    //method to notify observers of change
    public void notifyObservers();

    //method to get updates from subject
    public Object getUpdate(ObserverInterface obj);

}