/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.xmlreader;

public class SMSC {

    private String name;
    private String id;
    private String status;
    private String received;
    private String sent;
    private String failed;
    private String queued;
    private String sentSMSDLR;
    private String receivedSMSDLR;

    public String getFailed() {
        return failed;
    }

    public void setFailed(String failed) {
        this.failed = failed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQueued() {
        return queued;
    }

    public void setQueued(String queued) {
        this.queued = queued;
    }

    public String getReceived() {
        return received;
    }

    public void setReceived(String received) {
        this.received = received;
    }

    public String getSent() {
        return sent;
    }

    public void setSent(String sent) {
        this.sent = sent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReceivedSMSDLR() {
        return receivedSMSDLR;
    }

    public void setReceivedSMSDLR(String receivedSMSDLR) {
        this.receivedSMSDLR = receivedSMSDLR;
    }

    public String getSentSMSDLR() {
        return sentSMSDLR;
    }

    public void setSentSMSDLR(String sentSMSDLR) {
        this.sentSMSDLR = sentSMSDLR;
    }
}
