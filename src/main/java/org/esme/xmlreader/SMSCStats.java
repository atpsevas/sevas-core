package org.esme.xmlreader;

import org.apache.log4j.Logger;
import org.esme.broker.AppBroker;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class SMSCStats {

    private static final Logger logger = Logger.getLogger(SMSCStats.class);
    private static SMSCStats sMSCStats;
    private static Properties prop;
    String response;

    //Instance of WEB2SMS  can only be created by call this method...
    @SuppressWarnings("CallToPrintStackTrace")
    public static synchronized SMSCStats getInstance() {
        try {
            if (sMSCStats == null) {
                sMSCStats = new SMSCStats();
            }
            prop = AppBroker.getPropertyFileHandle();

        } catch (Exception ignored) {
        }
        return sMSCStats;
    }

    private static String getSentandReceivedSMSReport(String outterTagName, String innerTagName, Element eElement) {
        String theValue = "";
        NodeList nlList = eElement.getElementsByTagName(outterTagName);
        for (int nodeListIndex = 0; nodeListIndex < nlList.getLength(); nodeListIndex++) {
            Node nNode = nlList.item(nodeListIndex);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element theElement = (Element) nNode;
                theValue = getTagValue(innerTagName, theElement);
            }
        }
        return theValue;
    }

    private static String getTagValue(String sTag, Element element) {
        NodeList nodeList = element.getElementsByTagName(sTag).item(0).getChildNodes();
        Node nValue = nodeList.item(0);
        return nValue.getNodeValue();
    }

    public static void main(String arg[]) {
        org.esme.broker.AppBroker.sevasLogger.info(new SMSCStats().readSMSCQueueTest());
    }

    @SuppressWarnings({"CallToThreadDumpStack", "UseSpecificCatch", "CallToPrintStackTrace"})
    public ArrayList<SMSC> readSMSCXML() {
        @SuppressWarnings("Convert2Diamond")
        ArrayList<SMSC> smscs = new ArrayList<SMSC>();

        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(prop.getProperty("smsGatewayStatusURL"));
            doc.getDocumentElement().normalize();

            //org.esme.broker.AppBroker.sevasLogger.info("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList smscNideList = doc.getElementsByTagName("smsc");


            for (int nodeIndex = 0; nodeIndex < smscNideList.getLength(); nodeIndex++) {

                Node nNode = smscNideList.item(nodeIndex);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    SMSC smsc = new SMSC();
                    smsc.setName(getTagValue("name", eElement));
                    smsc.setId(getTagValue("id", eElement));
                    smsc.setStatus(getTagValue("status", eElement));
                    smsc.setReceived(getSentandReceivedSMSReport("sms", "received", eElement));
                    smsc.setSent(getSentandReceivedSMSReport("sms", "sent", eElement));
                    smsc.setSentSMSDLR(getSentandReceivedSMSReport("dlr", "received", eElement));
                    smsc.setFailed(getTagValue("failed", eElement));
                    smsc.setQueued(getTagValue("queued", eElement));
                    smscs.add(smsc);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return smscs;
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public long readSMSCQueue(String serviceBillingNetwork) throws ParserConfigurationException, SAXException, IOException {
        @SuppressWarnings("Convert2Diamond")
        long totalQueue = 0;

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(prop.getProperty("smsGatewayStatusURL"));
        doc.getDocumentElement().normalize();
        //org.esme.broker.AppBroker.sevasLogger.info("Root element :" + doc.getDocumentElement().getNodeName());
        NodeList smscNideList = doc.getElementsByTagName("smsc");
        for (int nodeIndex = 0; nodeIndex < smscNideList.getLength(); nodeIndex++) {
            Node nNode = smscNideList.item(nodeIndex);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if (getTagValue("id", eElement).equalsIgnoreCase(serviceBillingNetwork.trim())) {
                    totalQueue = totalQueue + Long.parseLong(getTagValue("queued", eElement));
                }
            }
        }

        org.esme.broker.AppBroker.sevasLogger.error("Total billing queue..... " + totalQueue);
        return totalQueue;
    }

    @SuppressWarnings({"UseSpecificCatch", "CallToPrintStackTrace"})
    public String getSMSGatewayStatus() {
        String value;

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(prop.getProperty("smsGatewayStatusURL"));
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("status").item(0).getChildNodes();
            Node nValue = nodeList.item(0);
            value = nValue.getNodeValue();

        } catch (Exception ex) {
            ex.printStackTrace();
            return "Report not available.";
        }
        return "SMS Gateway Status: " + value.substring(9);
    }

    private long readSMSCQueueTest() {
        @SuppressWarnings("Convert2Diamond")
        long totalQueue = 0;
        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse("http://astoria.atp-sevas.com:13013/status.xml");
            doc.getDocumentElement().normalize();
            //org.esme.broker.AppBroker.sevasLogger.info("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList smscNideList = doc.getElementsByTagName("smsc");
            for (int nodeIndex = 0; nodeIndex < smscNideList.getLength(); nodeIndex++) {
                Node nNode = smscNideList.item(nodeIndex);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    //if (getTagValue("id", eElement).equalsIgnoreCase(service.getServiceBillingNetwork())) {
                    totalQueue = totalQueue + Long.parseLong(getTagValue("queued", eElement));
                    //}
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException | NumberFormatException e) {
            e.printStackTrace();
        }
        org.esme.broker.AppBroker.sevasLogger.info("Total billing queue..... " + totalQueue);
        return totalQueue;
    }
}
