/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.utils.datastructure;

/**
 * @author saheedalbert
 */

import java.util.*;

/**
 * * Java program to perform binary search in Java collection e.g List, Set
 *
 * @author Javin Paul
 */
public class Modifier {

    public static void main(String args[]) {
    }


    public static Set<String> getArrayListSet(ArrayList<String> list) {
        Set<String> hs = new HashSet<>();
        hs.addAll(list);

        return hs;
    }


    public static <K, V> HashMap<V, K> reverse(Map<K, V> map) {
        HashMap<V, K> rev = new HashMap<V, K>();
        for (Map.Entry<K, V> entry : map.entrySet()) {
            rev.put(entry.getValue(), entry.getKey());
        }
        return rev;
    }

    public ArrayList<String> cleanCampaignDB(ArrayList<String> archiveTofilter, ArrayList<String> existignSubscribers) {
        // add elements to all, including duplicates....Set wil filter out duplicates...
        Set<String> hs = new HashSet<>();
        hs.addAll(archiveTofilter);
        archiveTofilter.clear();
        //filter duplicates..
        archiveTofilter.addAll(hs);
        archiveTofilter.removeAll(existignSubscribers);
        return archiveTofilter;
    }
}
