/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.utils.email;

/**
 * @author saheedalbert
 */

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Properties;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author simpumind
 */
public class Emailer {

    /**
     * Sends an e-mail message from a SMTP host with a list of attached files.
     *
     * @param host
     * @param port
     * @param userName
     * @param password
     * @param toAddresses
     * @param subject
     * @param message
     * @param attachedFiles
     */
    public static void sendEmailWithAttachment(String host, String port,
                                               final String userName, final String password, String toAddresses,
                                               String subject, String message, List<File> attachedFiles) {

        try {
            //set SMTP server properties
            Properties properties = new Properties();
//        properties.put("mail.smtp.host", host);
//        properties.put("mail.smtp.port", port);
//        properties.put("mail.smtp.auth", "true");
//        properties.put("mail.smtp.starttls.enable", "true");
//        properties.put("mail.user", userName);
//        properties.put("mail.password", password);
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.host", "smtp.gmail.com");
            properties.put("mail.smtp.port", "587");

            //create new session with an autheticator
//        Authenticator auth = new Authenticator() {
//
//            @Override
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(userName, password); //To change body of generated methods, choose Tools | Templates.
//            }
//        };
//
//        Session session = Session.getInstance(properties, auth);
            Session session = Session.getInstance(properties,
                    new javax.mail.Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(userName, password);
                        }
                    });
            //create a new email message
            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress(userName));

//            InternetAddress[] toAddresses = {new InternetAddress(toAddresses)};
//            msg.setRecipients(Message.RecipientType.TO, toAddresses);

            InternetAddress[] recipientAddresses = InternetAddress.parse(toAddresses);
            msg.addRecipients(Message.RecipientType.TO, recipientAddresses);

            msg.setSubject(subject);
            msg.setSentDate(new Date());

            // creates message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(message, "text/html");

            // creates multi-part
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            // adds attachments
            if (attachedFiles != null && attachedFiles.size() > 0) {
                for (File aFile : attachedFiles) {
                    MimeBodyPart attachPart = new MimeBodyPart();
/*

                    try {
                        attachPart.attachFile(aFile);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
*/

                    multipart.addBodyPart(attachPart);
                }
            }

            // sets the multi-part as e-mail's content
            msg.setContent(multipart);

            // sends the e-mail
            Transport.send(msg);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
