/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.utils.parser;

/**
 * @author salbert
 * <p>
 * This is String searching algorithm is implemented by Boyer-Moore (BM) and it
 * is called Boyer-Moore algorithm.
 */
public class StringSearcher {

    private final int BASE;
    private int[] occurrence;
    private String pattern;

    public StringSearcher(String pattern) {

        this.BASE = 256;
        this.pattern = pattern;

        occurrence = new int[BASE];
        for (int c = 0; c < BASE; c++) {
            occurrence[c] = -1;
        }

        for (int j = 0; j < pattern.length(); j++) {
            occurrence[pattern.charAt(j)] = j;
        }
    }

    public static void main(String[] args) {
        String text = "Lorem, ipsum, dolor, sit, amet";
        String pattern = "lorem";
        StringSearcher bm = new StringSearcher(pattern);
        org.esme.broker.AppBroker.sevasLogger.info("Lenght: " + text.length());
        int first_occur_position = bm.search(text);
        org.esme.broker.AppBroker.sevasLogger.info("The text '" + pattern + "' is first found after the "
                + first_occur_position + " position.");
    }

    public int search(String text) {
        int n = text.length();
        int m = pattern.length();
        int skip;
        for (int i = 0; i <= n - m; i += skip) {
            skip = 0;
            for (int j = m - 1; j >= 0; j--) {
                if (pattern.charAt(j) != text.charAt(i + j)) {
                    skip = Math.max(1, j - occurrence[text.charAt(i + j)]);
                    break;
                }
            }
            if (skip == 0) {
                return i;
            }
        }
        return n;
    }
}
