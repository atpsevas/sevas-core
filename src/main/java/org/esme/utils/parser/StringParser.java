/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.utils.parser;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 */
public class StringParser {

    public static void main(String args[]) {
        StringParser app = new StringParser();
        app.customParser("08164109622,08034473761,08105523249,08066525847,08028879344,08023872110,08037185416, 07035042301,08022501600,08036481000,07042676961,08178770914,08028296660,08038525253, 07065219575,08100869801,08027252902,07032728710,07036693968,08023703284,08034388434, 07086908626,08135145364,08023904853,08025760048,08026803533,08033920825,08088379458, 08067344466,08068882128,08132567879,08184445119,08029052753,08165731872,08023363255, 08056281143,08062431005,08086287836,08066495378,08032403462,08082947000,08023338340, 07065526628,07063190859,08052956815,08022501231,08033091992,08028953440,08035020742, 08033238188,08088778112,07030327683,07088409494,08025848029,08036099885,08134794406, 08162222105", ",");
    }

    //String str = "This is a sentence.  This is a question, right?  Yes!  It is.";
    public List<String> parseMessage(String message) {
        @SuppressWarnings("unchecked")
        ArrayList<String> allTokens = new ArrayList<String>();
        //All possible delimeters
        String delims = " ";
        String[] tokens = message.split(delims);
        for (String nextToken : tokens) {
            allTokens.add(nextToken);
        }
        return allTokens;
    }

    public List multipleParser(String message) {

        List<String> allTokens = new ArrayList();
        String delims = "[|/()+;_&.*,=?!-]+";
        String[] tokens = message.split(delims);
        for (String nextToken : tokens) {
            allTokens.add(nextToken);
            //org.esme.broker.AppBroker.sevasLogger.info(nextToken);
        }
        return allTokens;
    }

    public List<String> parseMultiple(String message) {

        List<String> allTokens = new ArrayList();
        //All possible delimeters
        String delims = "#*";
        String[] tokens = message.split(delims);
        for (String nextToken : tokens) {
            allTokens.add(nextToken);
        }
        return allTokens;
    }

    public List<String> parseBySlash(String message) {

        List<String> allTokens = new ArrayList();
        //All possible delimeters
        String delims = "/";
        String[] tokens = message.split(delims);
        for (String nextToken : tokens) {
            allTokens.add(nextToken);
        }
        return allTokens;
    }

    public List<String> parseByAsh(String message) {
        List<String> allTokens = new ArrayList();
        String delims = "#";
        String[] tokens = message.split(delims);
        for (String nextToken : tokens) {
            allTokens.add(nextToken);
        }
        return allTokens;
    }

    public List<String> parseByStar(String message) {
        List<String> allTokens = new ArrayList();
        String delims = "\\*";
        String[] tokens = message.split(delims);
        for (String nextToken : tokens) {
            allTokens.add(nextToken);
        }
        return allTokens;
    }

    public List<String> customParser(String message, String delimiter) {
        List<String> allTokens = new ArrayList();
        String delims = delimiter;
        String[] tokens = message.split(delims);
        for (String nextToken : tokens) {
            allTokens.add(nextToken);
        }
        return allTokens;
    }
}
