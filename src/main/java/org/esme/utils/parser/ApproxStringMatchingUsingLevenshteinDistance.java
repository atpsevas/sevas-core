/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.utils.parser;

/**
 * @author saheedalbert
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ApproxStringMatchingUsingLevenshteinDistance {

    public static int distance(String a, String b) {
        a = a.toLowerCase();
        b = b.toLowerCase();
        int[] costs = new int[b.length() + 1];
        for (int j = 0; j < costs.length; j++) {
            costs[j] = j;
        }
        for (int i = 1; i <= a.length(); i++) {
            costs[0] = i;
            int nw = i - 1;
            for (int j = 1; j <= b.length(); j++) {
                int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]),
                        a.charAt(i - 1) == b.charAt(j - 1) ? nw : nw + 1);
                nw = costs[j];
                costs[j] = cj;
            }
        }
        return costs[b.length()];
    }

    public static void main(String[] args) {
        String text = "SPANISH FRENCH GERMAN ADURA CONNECT ";
        org.esme.broker.AppBroker.sevasLogger.info(new ApproxStringMatchingUsingLevenshteinDistance().getCloseMatch(text, "instruction format is not correct!"));
    }

    public String getCloseMatch(String allKeywords, String userText) {
        Scanner sc = new Scanner(userText);
        String text = allKeywords;
        String keyword = sc.nextLine();
        String[] data = text.split(" ");
        List<Integer> dist = new ArrayList<Integer>();
        for (int i = 0; i < data.length; i++) {
            dist.add(distance(data[i], keyword));
        }

        Collections.sort(dist);
        String closestMatch = "";
        for (int i = 0; i < data.length; i++) {
            if (distance(data[i], keyword) == dist.get(0)) {
                closestMatch = closestMatch + data[i] + " ";
            }
        }
        sc.close();
        return closestMatch.trim();
    }
}
