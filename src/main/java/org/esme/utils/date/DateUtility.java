/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.utils.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author root
 */
public class DateUtility {

    public static int getCurrentHour() {
        Date date = new Date();   // given date
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(date);   // assigns calendar to given date
        calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
        calendar.get(Calendar.HOUR);        // gets hour in 12h format
        calendar.get(Calendar.MONTH);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static String cusFormatDateToString(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-M-dd";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }
}
