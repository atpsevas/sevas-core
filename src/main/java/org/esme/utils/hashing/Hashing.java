/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.utils.hashing;

/**
 * @author saheedalbert
 */

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

//Secure Hashing algorith types are SHA-1, SHA-256,SHA-384,SHA-512
public class Hashing {
    MessageDigest md;
    String hashedCode;

    public static void main(String[] args) throws NoSuchAlgorithmException {
        String passwordToHash = "password";
        String securePassword = new Hashing().doHashing(passwordToHash, "SHA-512");
        org.esme.broker.AppBroker.sevasLogger.info(securePassword);

    }

    //Add salt
    @SuppressWarnings("ImplicitArrayToString")
    public static String getSalt(String algorithmType) throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt.toString();
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public String doHashing(String passwordToHash, String hashType) {
        try {
            md = MessageDigest.getInstance(hashType);
            md.update(passwordToHash.getBytes());
            byte[] mb = md.digest();
            for (int i = 0; i < mb.length; i++) {
                byte temp = mb[i];
                String s = Integer.toHexString(new Byte(temp));
                while (s.length() < 2) {
                    s = "0" + s;
                }
                s = s.substring(s.length() - 2);
                hashedCode += s;
            }
        } catch (NoSuchAlgorithmException e) {
            org.esme.broker.AppBroker.sevasLogger.info("ERROR: " + e.getMessage());
        }
        return hashedCode;
    }
}
