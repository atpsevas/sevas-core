/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.utils.gateway;

/**
 * @author Tofunmi
 */
public interface GatewayInterface {
    public boolean isQueueEmpty(String queue);

    public long getQueueSize(String queue);

}
