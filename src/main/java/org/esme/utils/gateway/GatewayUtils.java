/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.utils.gateway;

import org.esme.gateway.kannel.WEB2SMS;
import org.esme.utils.pingenerator.RandomPasswordGenerator;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Tofunmi
 */
public class GatewayUtils {

    // Returns encoded callback URL
    public static String encodeCallbackURL(String BASE_URI, HashMap<String, String> params) {

        String callback_params = "";
        Set<Map.Entry<String, String>> entrySet = params.entrySet();
        for (Entry entry : entrySet) {
            callback_params += entry.getKey() + "=" + entry.getValue().toString() + "&";
        }

        String encoded_params = encodeParams(BASE_URI + callback_params);
        return encoded_params;
    }

    // Does the actual Encoding
    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace"})
    public static String encodeParams(String param) {
        String encParam = "";
        try {
            encParam = URLEncoder.encode(param, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return encParam;
    }

    public static String generateMessageID() {
        return RandomPasswordGenerator.randomPasswordGenerator();
    }

    public static String formatDateTillSecondToString(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd hh:mm:ss";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }

    public static long getCurrentTimeInMillis(Date theDate) {
        long currentTime = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
            String dateInString = formatDateToString2(theDate);
            Date date = sdf.parse(dateInString);
            currentTime = date.getTime();
        } catch (ParseException ex) {
            Logger.getLogger(WEB2SMS.class.getName()).log(Level.SEVERE, null, ex);
        }
        return currentTime;
    }

    public static String formatDateToString2(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd hh:mm:ss";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }
}
