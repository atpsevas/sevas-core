/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.utils.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Abiola Aluko
 */
public class Validator {

    public static boolean isThisAGMSNumber(String gsmNumber) {
        boolean isValid = false;

        String expression = "^(0|234|\\+234)[78][01]\\d{8}$";

        CharSequence inputStr = gsmNumber;
        //Make the comparison case-insensitive.
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    /**
     * isEmailValid: Validate email address using Java reg ex.
     * This method checks if the input string is a valid email address.
     *
     * @param email String. Email address to validate
     * @return boolean: true if email address is valid, false otherwise.
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        /*
        Email format: A valid email address will have following format:
        [\\w\\.-]+: Begins with word characters, (may include periods and hypens).
        @: It must have a '@' symbol after initial characters.
        ([\\w\\-]+\\.)+: '@' must follow by more alphanumeric characters (may include hypens.).
        This part must also have a "." to separate domain and subdomain names.
        [A-Z]{2,4}$ : Must end with two to four alaphabets.
        (This will allow domain names with 2, 3 and 4 characters e.g pa, com, net, wxyz)

        Examples: Following email addresses will pass validation
        abc@xyz.net; ab.c@tx.gov
         */
        //Initialize reg ex for email.
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        //Make the comparison case-insensitive.
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    /** isPhoneNumberValid: Validate phone number using Java reg ex.
     * This method checks if the input string is a valid phone number.
     * @param phoneNumber
     * @return boolean: true if phone number is valid, false otherwise.
     */


    /**
     * isNumeric: Validate a number using Java regex.
     * This method checks if the input string contains all numeric characters.
     *
     * @param number
     * @return boolean: true if the input is all numeric, false otherwise.
     */
    public static boolean isNumeric(String number) {
        boolean isValid = false;

        /*Number: A numeric value will have following format:
        ^[-+]?: Starts with an optional "+" or "-" sign.
        [0-9]*: May have one or more digits.
        \\.? : May contain an optional "." (decimal point) character.
        [0-9]+$ : ends with numeric digit.
         */
        //Initialize reg ex for numeric data.
        String expression = "^[-+]?[0-9]*\\.?[0-9]+$";
        CharSequence inputStr = number;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static boolean isNumericWithoutSign(String number) {
        boolean isValid = false;

        /*Number: A numeric value will have following format:
        [0-9]*: May have one or more digits.
        \\.? : May contain an optional "." (decimal point) character.
        [0-9]+$ : ends with numeric digit.
         */
        //Initialize reg ex for numeric data.
        String expression = "^[0-9]*\\.?[0-9]+$";
        CharSequence inputStr = number;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    /*A Java Metrhod to verify if a String variable is a number*/
    public static boolean IsStringANumber(String number) {
        boolean isValid = false;
        /*Explaination:
        [-+]?: Can have an optional - or + sign at the beginning.
        [0-9]*: Can have any numbers of digits between 0 and 9
        \\.? : the digits may have an optional decimal point.
        [0-9]+$: The string must have a digit at the end.
        If you want to consider x. as a valid number change
        the expression as follows. (but I treat this as an invalid number.).
        String expression = "[-+]?[0-9]*\\.?[0-9\\.]+$";
         */
        String expression = "[-+]?[0-9]*\\.?[0-9]+$";
        CharSequence inputStr = number;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static boolean isValidDate(String date) {
        boolean isValid = false;
        String expression = "^[0-1][1-9][- / ]?(0[1-9]|[12][0-9]|3[01])[- /]?(18|19|20|21)\\d{2}$";
        /*
         * ^[0-1][1-9] : The month starts with a 0 and a digit between 1 and 9
         * [- / ]?: Followed by  an optional "-" or "/".
         * (0[1-9]|[12][0-9]|3[01]) : The day part must be either between 01-09, or 10-29 or 30-31.
         * [- / ]?: Day part will be followed by  an optional "-" or "/".
         * (18|19|20|21)\\d{2}$ : Year begins with either 18, 19, 20 or 21 and ends with two digits.
         */
        CharSequence inputStr = date;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    /* A java method that validate any URL*/
    public static boolean isValidURL(String url) {
        boolean isValid = false;
        String expression = "^(https?|ftp|file)://.+$";
        CharSequence inputStr = url;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    ///    /*Here is a simple test method to test an input date using Java reg ex*/
    public static void main(String args[]) {
        boolean isValid = Validator.isThisAGMSNumber("+2348034678549");
        org.esme.broker.AppBroker.sevasLogger.info("Result: " + isValid);
    }
}
