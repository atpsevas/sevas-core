/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.utils.pingenerator;

import java.util.Random;

/**
 * @author S@heed
 */
public class RandomPinGenerator {

    final static int[] sizeTable = {0, 9, 99, 999, 9999, 99999, 999999, 9999999,
            99999999, 999999999, Integer.MAX_VALUE};
    private static Random random = new Random();

    public static int randomPinGenerator(int digit) {

        int highest = sizeTable[digit] + 1;
        int lowest = sizeTable[digit - 1] + 1;

        int generated = random.nextInt(highest);

        if (generated < lowest) {
            generated = randomPinGenerator(digit);
        }
        return generated;

    }


////     public static void main (String args []){
////        org.esme.broker.AppBroker.sevasLogger.info(RandomPinGenerator.randomPinGenerator(9));
////
////    }

}
