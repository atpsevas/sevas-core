/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.utils.http;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

/**
 * @author Abiola Aluko
 */
public class HttpClient {

    private String baseUrl;
    private ArrayList parameterList;

    /**
     * Creates a new instance of HttpPost
     */
    public HttpClient() {
        this.setBaseUrl("");
        this.setParameterList(new ArrayList());
    }

    public HttpClient(String baseUrl) {
        this.setBaseUrl(baseUrl);
        this.setParameterList(new ArrayList());
    }

    public HttpClient(String baseUrl, ArrayList parameterList) {
        this.setBaseUrl(baseUrl);
        this.setParameterList(parameterList);
    }

    // Getters and Setters
    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public ArrayList getParameterList() {
        return parameterList;
    }

    public void setParameterList(ArrayList parameterList) {
        this.parameterList = parameterList;
    }

    // Main Method for testing
//    public static void main(String [] args) {
//        String baseUrl = "http://72.3.232.103:13131/cgi-bin/sendsms";
//        //String baseUrl = "http://198.57.181.127:2528/bigfootProxy/Default.aspx";
//        //ArrayList parameters = new ArrayList();
//
//        HttpClient httpClient = new HttpClient();
//        httpClient.setBaseUrl(baseUrl);
//
//        //TODO: Look for a more dynamic way to achieve this.
//        try {
//
//            httpClient.addUrlParameter("user", "mtech");
//            httpClient.addUrlParameter("password", "mtech");
//            httpClient.addUrlParameter("from", "test");
//            httpClient.addUrlParameter("to", "2348034678549");
//            httpClient.addUrlParameter("text", "Hello World");
//
//            //org.esme.broker.AppBroker.sevasLogger.info(httpPost.post());
//            org.esme.broker.AppBroker.sevasLogger.info(httpClient.post());//.get());
//            org.esme.broker.AppBroker.sevasLogger.info("Returned Successfully from httpPost.post()...");
//
//        } catch (UnsupportedEncodingException ex) {
//            ex.printStackTrace();
//        } catch (MalformedURLException ex) {
//            ex.printStackTrace();
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }

    /**
     * @param key
     * @param value
     * @throws UnsupportedEncodingException
     */
    @SuppressWarnings("unchecked")
    public void addUrlParameter(String key, String value) throws UnsupportedEncodingException {
        String encodedData = URLEncoder.encode(key, "UTF-8") + "=" + URLEncoder.encode(value, "UTF-8");
        this.parameterList.add(encodedData);
    }

    @SuppressWarnings("unchecked")
    public void addUrlParameterDontEncode(String key, String value) throws UnsupportedEncodingException {
        //String encodedData = URLEncoder.encode(key, "UTF-8") + "=" + URLEncoder.encode(value, "UTF-8");
        String encodedData = key + "=" + value;
        this.parameterList.add(encodedData);
    }

    public String post() throws MalformedURLException, IOException {

        String response = "";
        String data = getQueryString();

        // Send data
        URL url = new URL(this.getBaseUrl() + data);
        URLConnection conn = url.openConnection();

        HttpURLConnection httpConn = (HttpURLConnection) conn;
        httpConn.setInstanceFollowRedirects(true);
        httpConn.setDoInput(true);
        conn.setDoOutput(true);

        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

        wr.write(data);
        wr.flush();

        // Get the response
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;

        while ((line = rd.readLine()) != null) {
            // Process line...
            response += line;
        }

        wr.close();
        rd.close();
        return response;
    }

    //Cusstom post....
    @SuppressWarnings("CallToPrintStackTrace")
    public String doPost(String theURL,
                         String SHORTCODE, String theShortCode,
                         String MSISDN, String theMSISDN,
                         String TEXT, String text) throws MalformedURLException, IOException {

        String theBaseURL = theURL;
        String response = "";

        HttpClient httpClient = new HttpClient();
        httpClient.setBaseUrl(theBaseURL);

        //TODO: Look for a more dynamic way to achieve this.
        try {

            httpClient.addUrlParameter(SHORTCODE, theShortCode);
            httpClient.addUrlParameter(MSISDN, theMSISDN);
            httpClient.addUrlParameter(TEXT, text);

            //org.esme.broker.AppBroker.sevasLogger.info(httpPost.post());
            response = httpClient.post();
            org.esme.broker.AppBroker.sevasLogger.info(response);//.get());
            org.esme.broker.AppBroker.sevasLogger.info("Returned Successfully from httpPost.post()...");

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return response;
    }

    private String getQueryString() {
        // Construct data
        String data = "";
        int size = this.getParameterList().size();

        for (int i = 0; i < size; i++) {
            if (i == 0) {
                data = "?" + (String) this.getParameterList().get(i);
            } else {
                data += "&" + (String) this.getParameterList().get(i);
            }
        }

        return data;
    }

    // For debug purposes only... we might get ride of this soon.
    public String queryString() {
        return getQueryString();
    }

    public String get() throws MalformedURLException, IOException, Exception {

        String theUrl = this.baseUrl + getQueryString();
        return doHttpGet(theUrl);
    }

    /**
     * Retrieve data specified by given URL as a String. This is bare-bones
     * HTTP/1.0 compliant solution, more demanding stuff needs to use the
     * jakarta-httpclient
     */
    private String doHttpGet(String RequestUrl) throws Exception {
        int timeout = 5000;

        Socket sock;
        String request;
        URL url = null;
        int port = 0;
        String response = null;

        url = new URL(RequestUrl);

        String host = url.getHost();

        port = url.getPort();
        if (port == -1) {
            port = 80;
        }
        if (url.getQuery() == null) {
            request = "GET " + url.getPath();
        } else {
            request = "GET " + url.getPath() + "?" + url.getQuery();
        }
        request += " HTTP/1.0\n\n";

        sock = new Socket(host, port);
        OutputStream os = sock.getOutputStream();
        for (int i = 0; i < request.length(); i++) {
            os.write(request.charAt(i));
        }
        os = null;

        InputStream is = sock.getInputStream();

        long start = System.currentTimeMillis();

        // wait for data...
        while ((is.available() == 0) && (System.currentTimeMillis() < (start + timeout))) {
            ; //TODO: Use something more descent.
        }

        while (is.available() != 0) {
            response += (char) is.read();
        }
        is = null;

        // finally, kill the connection
        sock.close();

        sock = null;
        url = null;

        return response;
    }
}
