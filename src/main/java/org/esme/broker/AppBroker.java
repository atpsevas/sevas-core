package org.esme.broker;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.esme.controllers.helpers.FetchBlacklistedNumbers;
import org.esme.controllers.helpers.LogMTBillingResponse;
import org.esme.dao.models.*;
import org.esme.dto.Service;
import org.esme.gateway.kannel.WEB2SMS;
import org.esme.logger.AppLogger;
import org.esme.utils.datastructure.Modifier;
import org.esme.utils.pingenerator.RandomPasswordGenerator;
import org.esme.webservices.airtel.AirtelUCIP;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author saheedalbert
 */
@SuppressWarnings({
        "StaticNonFinalUsedInInitialization",
        "ResultOfObjectAllocationIgnored",
        "ResultOfObjectAllocationIgnored",
        "CallToThreadDumpStack", "serial"})
public class AppBroker extends HttpServlet {

    private static final Logger logger = Logger.getLogger(AppBroker.class);
    public static Connection postgresDBCconnection, localPGSQLDBCconnection, mysqlDBConnection = null;
    public static ServletContext context;
    public static org.apache.log4j.Logger sevasLogger;
    //    public static ArrayList<Service> servicesCache;
    public static HashMap<Long, Service> serviceHashMap;
    public static Properties prop;
    public static String CountryCodePrefix_PROP, DLRURL, billingSendSMSTable,
            billingSMSBoxID, DLR_MASK, dbURL, dbUserName, dbPassword,
            expiredNumDayPriority, LowPriorityBillingNetwork_PROP,
            rabbitLowPriorityBillingQueueName_PROP, rabbitQueueName_PROP,
            monthlySubTarget_PROP, monthlyRevTarget_PROP, etiContentBind_PROP;
    static java.sql.Connection dbConnection = null;
    private final DataSource mysqlDatasource = null;
    Random generator = new Random();
    String network;
    ServletConfig config;
    WEB2SMS webSMS;
    Network theNetwork;
    UserRole userRole;
    ArrayList<Network> networkCache;
    ArrayList<UserRole> userRoleCache;
    ActiveSubscription activeSubscription;
    ArrayList<ActiveSubscription> activeSsubscriptionCache;
    Service service;
    private DataSource datasource = null;

    private static Date Date(String newDateString) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void initPropertyVariables() {
        logger.info("INFO: Loading app properties...");

        prop = getPropertyFileHandle();

        try {
            // Get Country Code from Property File
            CountryCodePrefix_PROP = prop.getProperty("CountryPrefix");

            // Use Nigeria if not set
            if (prop.getProperty("CountryPrefix") == null)
                CountryCodePrefix_PROP = "234";
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            DLRURL = prop.getProperty("dlr_url");
            DLR_MASK = prop.getProperty("dlrMask");
            billingSendSMSTable = prop.getProperty("billingSendSMSTable");
            billingSMSBoxID = prop.getProperty("billingSMSBoxID");
            dbURL = prop.getProperty("dbURL");
            dbUserName = prop.getProperty("dbUserName");
            dbPassword = prop.getProperty("dbPassword");
            expiredNumDayPriority = prop.getProperty("expiredNumDayPriority");
            LowPriorityBillingNetwork_PROP = prop.getProperty("LowPriorityBillingNetwork");
            rabbitLowPriorityBillingQueueName_PROP = prop.getProperty("rabbitLowPriorityBillingQueueName");
            rabbitQueueName_PROP = prop.getProperty("rabbitQueueName");
            monthlyRevTarget_PROP = prop.getProperty("monthlyRevTarget");
            monthlySubTarget_PROP = prop.getProperty("monthlySubTarget");
            etiContentBind_PROP = prop.getProperty("etiContentBind");
        } catch (Exception e) {
            logger.error("ERROR: Could not load app property file.");
            e.printStackTrace();
        }
    }

    public static Logger getSevasLogger() {
        Logger _logger = AppLogger.getInstance(getPropertyFileHandle().getProperty("log_file_path"));
        return _logger;
    }

    public static Properties getPropertyFileHandle() {

        Properties theProp = new Properties();

        // Log File Configuration
//        String sevasConfigFile = System.getProperty("user.dir")
//                + File.separator + "sevas.properties";
        try {
            theProp.load(new FileInputStream("/home/sevas/sevas.properties"));
        } catch (Exception e) {
            logger.error("ERROR: Could not load app property file.");
            e.printStackTrace();
        }

        return theProp;
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public static synchronized Connection getDBConnection() {
        try {
            if (postgresDBCconnection == null || postgresDBCconnection.isClosed()) {
                logger.info(context.getAttribute("dbPool"));

                DataSource dataSource = (DataSource) context.getAttribute("dbPool");
                // Allocate and use a postgresDBCconnection from the pool
                postgresDBCconnection = dataSource.getConnection();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return postgresDBCconnection;
    }

    public static java.sql.Connection getDBConnection(Properties prop) {


        try {
//            Class.forName("org.postgresql.Driver");
//            dbConnection = getDBConnection();

            dbConnection = DriverManager.getConnection(getPropertyFileHandle().getProperty("dbURL"), getPropertyFileHandle().getProperty("dbUserName"), getPropertyFileHandle().getProperty("dbPassword"));

        } catch (Exception e) {

        }

        return dbConnection;

    }

    /**
     * FormatDateToStringCustom Available format options - yyyyMMddhhmmss,
     * yyyy-MM-dd HH:mm:ss, yyyy-MM-dd
     *
     * @param date
     * @param formattedDate
     * @return
     */
    static public String formatDateToStringCustom(Date date, String formattedDate) {
        date.setTime(System.currentTimeMillis());
        String defaultFormat = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdFormat;

        if (formattedDate.isEmpty()) {
            formattedDate = defaultFormat;
        }
        // TODO Match valid date format, use default if invalide
        try {
            sdFormat = new SimpleDateFormat(formattedDate);
        } catch (Exception ex) {
            ex.printStackTrace();
            sdFormat = new SimpleDateFormat(defaultFormat);
        }
        return sdFormat.format(date);
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace"})
    public static String encodeParams(String param) {
        String encParam = "";
        try {
            encParam = URLEncoder.encode(param, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return encParam;
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "null"})
    public static void updateServicesCache() {

//        logger.info("Updating Runtime Service Cache...");
//        try {
//            logger.info("Clearing ArrayList for existing servicesCache");
//            servicesCache.clear();
//        } catch (Exception e) {
//            logger.error("Unable to clear..create new instance");
//            servicesCache = new ArrayList<Service>();
//        }

        ArrayList<Service> _servicesCache = Service.getAllServices();

        logger.info("Updating Servlet Enviroment Service Cache...");

        //Create cache of services with all
        context.setAttribute("totalService", _servicesCache.size());
        context.setAttribute("serviceCache", _servicesCache);
    }

    /**
     * Converts a String to Date Object
     *
     * @param date String Date
     * @return the new Date Object Equivalent of date
     */
    public static Date stringToDate(String date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = df.parse(date);
            String dateString = df.format(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    public static String getCurrentDate(String format) {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date dateobj = new Date();
        String currentDate = df.format(dateobj);
        return currentDate;

    }

    public static void LogMTResponse(String messageID, String senderID, String recipientMSISDN, String smsc,
                                     String message, String status, String srcModule, Service theService, String logType) {

        logger.info("Update " + logType + " outgoing message file...");

        LogMTBillingResponse response = new LogMTBillingResponse();

        response.setMessageID(messageID);
        response.setSenderID(senderID);
        response.setRecipientMSISDN(recipientMSISDN);
        response.setSmsc(smsc);
        response.setMessage(message);
        response.setDelivertStatus(status);
        response.setServName(theService.getServiceName());
        response.setSrcModule(srcModule.replace("2NDT", ""));
        response.setServiceID(theService.getServiceID());
        response.setSubscriptionBillingType(theService.getServiceSubType());

        switch (logType) {
            case "content":
                response.content_outgoing_messages(response);
                break;
            case "billing":
                response.billing_outgoing_messages(response);
                break;
            default:
                break;
        }
        logger.info("outgoing message file updated...");
    }

    @Override
    @SuppressWarnings("CallToPrintStackTrace")
    public void init(ServletConfig config) throws ServletException {

        this.config = config;
        super.init(config);

        initPropertyVariables();
        PropertyConfigurator.configure(getPropertyFileHandle().getProperty("log_file_path"));

        context = getServletContext();
        logger.info("Starting sevas...");

        try {
            synchronized (this) {
                logger.info("Configuring Postgres DB Connection Pooling");
                Context envCtx = (Context) new InitialContext().lookup("java:comp/env");
                this.datasource = (DataSource) envCtx.lookup("jdbc/sevas");

                logger.info(this.datasource);
                context.setAttribute("dbPool", this.datasource);

                logger.info("Postgres DB Connection Successfully Configured");
                context.setAttribute("database.connection", this.datasource.getConnection());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // TODO: Eliminate Global Logger Variable
        sevasLogger = AppLogger.getInstance(getPropertyFileHandle().getProperty("log_file_path"));

        logger.info("sevas Started");
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public synchronized Connection getMYSQLDBConnection() {
        try {
            if (mysqlDBConnection == null || mysqlDBConnection.isClosed()) {
                // Allocate and use a postgresDBCconnection from the pool
                mysqlDBConnection = mysqlDatasource.getConnection();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return mysqlDBConnection;
    }

    //get Send SENDSMS
    public WEB2SMS getSendSMS() {
        return webSMS = WEB2SMS.getInstance();
    }

    //Acces to the required application paramenters...configured in web descriptor of the application...
    public String getAppProperty(String paramName) {
        return context.getInitParameter(paramName);
    }

    public ServletContext getContext() {
        return context;
    }

    public String phoneFotmatter(String msisdn) {
        String customMSISDN = msisdn.replace("+", "");
        String gsmFormat;
        if (!customMSISDN.startsWith(AppBroker.CountryCodePrefix_PROP) || customMSISDN.length() != 13) {
            if (customMSISDN.startsWith("0")) {
                gsmFormat = customMSISDN.substring(1);
            } else {
                gsmFormat = customMSISDN.substring(3);
            }
            return AppBroker.CountryCodePrefix_PROP + gsmFormat;
        }
        return customMSISDN;
    }

    /*
     @desc Confirms the authenticity of msisdn
     Returns msisdn without country code or a trailing 0
     Returns False if msisdn not authentic
     */
    public String verifyMsisdn(String _msisdn) {
        String msisdn = null;

        if (_msisdn.matches("^\\+[1-9]{1}[0-9]{3,14}$")) {
            msisdn = _msisdn.substring(_msisdn.length() - 10);
        }

        return msisdn;
    }

    public String removePoison(String input) {
        String outputStr = null;

        try {
            if (input != null && !input.isEmpty()) {
                final String filterPattern = "[<>{}\\[\\];\\&]";
                String inputStr = input.replaceAll(filterPattern, "");
                outputStr = inputStr;
            }
        } catch (Exception ex) {
            // ex.printStackTrace();
        }

        return outputStr.trim();
    }

    public String formatText(String text) {
        String formattedContent;
        String myString = text.trim();
        byte[] ptext;
        try {
            ptext = myString.getBytes("ISO-8859-1");
            formattedContent = new String(ptext, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            return text;
        }
        return formattedContent;
    }

    public String formatDateToString(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }

    public String formatDateToString2(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }

    public String formatDateToString3(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyyMMddhhmmss";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public long dateDiff(String firstDate, String secondDate) {
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        long diff = 0;
        try {
            Date date1 = myFormat.parse(firstDate);
            Date date2 = myFormat.parse(secondDate);
            diff = date2.getTime() - date1.getTime();
            logger.info("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public long dateDiffInHours(String firstDate, String secondDate) {
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        long diff = 0;
        try {
            Date date1 = myFormat.parse(firstDate);
            Date date2 = myFormat.parse(secondDate);
            diff = date2.getTime() - date1.getTime();
            logger.info("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
    }

    @SuppressWarnings({"CallToThreadDumpStack", "null", "Convert2Diamond"})
    public void updateNetwork() {
        logger.info("Updating networks..");
        networkCache = new ArrayList<Network>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(context.getInitParameter("networksQuery"));
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                //update previously stored job contents from the container...
                while (resultSet.next()) {
                    theNetwork = new Network();
                    theNetwork.setId(resultSet.getInt("id"));
                    theNetwork.setNetworkID(resultSet.getString("network_id"));
                    theNetwork.setPrefixes(resultSet.getString("network_prefixes"));
                    networkCache.add(theNetwork);
                }
                //Create cache of all users
                logger.info("Total Network " + networkCache.size());
                getServletContext().setAttribute("networksCache", networkCache);
            }

        } catch (Exception ex) {
            logger.info(ex.getMessage());
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }

        }
    }

    public void updateUserRole() {
        logger.info("Updating User Roles..");
        userRoleCache = new ArrayList<UserRole>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery("SELECT * FROM roles");
            //We place a lock on the content cache untill it finishes updating....
            synchronized (this) {
                //update previously stored job contents from the container...
                while (resultSet.next()) {
                    userRole = new UserRole();

                    userRole.setRoleID(resultSet.getInt("id"));
                    userRole.setRoleName(resultSet.getString("role"));
                    userRole.setUserName(resultSet.getString("username"));
                    userRoleCache.add(userRole);
                }
                //Create cache of all users
                getServletContext().setAttribute("userRoleCache", networkCache);
            }

        } catch (Exception ex) {
            logger.info(ex.getMessage());
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
            try {
                resultSet.close();
            } catch (SQLException ex) {
            }

        }
    }

    public void setActiveSubscriptionCache() {

        logger.info("Updating Runtime Service Cache...");
        Set<String> activeSubscriptionCache;

        activeSubscriptionCache = Modifier.getArrayListSet(Subscription.getAllUniqueMsisdn());
        context.setAttribute("activeSubscriptionCache", activeSubscriptionCache);

    }

    public void updateBlacklistCache() {

        FetchBlacklistedNumbers blockedNumbers = null;
        Properties theProp;
        HashMap blacklistedNumbers;
        ArrayList<String> doNotChargeNumbers;

        theProp = AppBroker.getPropertyFileHandle();

        try {
            logger.info("Fetching Do not charge Numbers....");
            doNotChargeNumbers = blockedNumbers.getDoNotChargeNumbers(theProp.getProperty("doNotChargeFilePath"));
            context.setAttribute("doNotChargeNumbersCache", doNotChargeNumbers);
            logger.info("Total Do not Charge Numbers Fetched.." + doNotChargeNumbers.size());
            logger.info("Done Fechting.......");
        } catch (Exception e) {

        }

        logger.info("Fetching Blacklisted Numbers....");
        blockedNumbers = new FetchBlacklistedNumbers(theProp.getProperty("blacklistURL"));
        try {
            blacklistedNumbers = (HashMap) blockedNumbers.getBlacklistedNumbers();
            context.setAttribute("blackListedNumbersCache", blacklistedNumbers);
            logger.info("Total Blacklisted Number Fetched.." + blacklistedNumbers.size());
            logger.info("Done Fechting.......");
        } catch (Exception e) {

        }
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public Service getService(long serviceID) {
        Service _service = Service.getService(serviceID);

        return _service;
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public void customBillingLogCondition(Service service, String subMSISDN,
                                          String message, String sourceModule, boolean isbILLED) {
        try {

            //String tableName = "";
            if (isbILLED) {
                //Log it as billed...
                //logger.info("Logging to ...." + tableName);
                LogMTBillingResponse logMTBillingResponse = new LogMTBillingResponse();
                logMTBillingResponse.setMessageID(generateMessageID());
                logMTBillingResponse.setSenderID(service.getServiceMTShortCode());
                logMTBillingResponse.setRecipientMSISDN(subMSISDN);
                logMTBillingResponse.setSmsc(service.getServiceBillingNetwork());
                logMTBillingResponse.setMessage(formatText(message));
                logMTBillingResponse.setDelivertStatus("SUBMITTED");
                logMTBillingResponse.setServName(service.getServiceName());
                logMTBillingResponse.setSrcModule(sourceModule);
                logMTBillingResponse.setSmsc(service.getServiceBillingNetwork());
                logMTBillingResponse.setServiceID(service.getServiceID());
                logMTBillingResponse.setSubscriptionBillingType(service.getServiceSubType());
                logMTBillingResponse.billing_outgoing_messages(logMTBillingResponse);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String addDate(long NoOfDays) {
        int castedNumber = (int) NoOfDays;
        Date date = new Date();
        String dt = formatDateToString(date);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(dt));
            c.add(Calendar.DATE, castedNumber);
            dt = sdf.format(c.getTime());
        } catch (ParseException ex) {
        }
        return dt;
    }

    public int getCurrentHour() {
        Date date = new Date();   // given date
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(date);   // assigns calendar to given date
        calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
        calendar.get(Calendar.HOUR);        // gets hour in 12h format
        calendar.get(Calendar.MONTH);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public String generateMessageID() {
        return RandomPasswordGenerator.randomPasswordGenerator();
    }

    public String cusFormatDateToString(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-M-dd";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }

    @SuppressWarnings({"unchecked", "CallToPrintStackTrace", "unchecked", "unchecked", "UseSpecificCatch"})
    public void sendFirstMTContent(Service service, String recipientMSISDN) {

        //ArrayList<Content> content = null;
        ResultSet rs = null;
        PreparedStatement selectPstmt;

        logger.error("Sending first content...");

        try {

            if (service.getServiceContentDeliveryMethod().equalsIgnoreCase("sequential")) {

                long lastContentDay = 0;
                //Query and send the actual content...
                Statement statement = getDBConnection().createStatement();

                ResultSet resultSet = statement.executeQuery(
                        "select distinct on (a.subscriber) a.content_service_id, "
                                + " b.content_blast_day,  \n"
                                + " a.subscriber, a.last_content_day \n"
                                + " from sequential_content_log a, sequential_content b \n"
                                + " where a.content_service_id = " + service.getServiceID() + " "
                                + "and  a.content_service_id = b.content_service_id  and a.subscriber = '" + recipientMSISDN + "' limit 1");

                //If no row found for the number....he's a new sub or never receive content
                while (!resultSet.next()) {
                    try {

                        //insert new record in sequential content log table...
                        logger.info("INSERTING NEW SEQ SUB LOG..");
                        PreparedStatement thpstmt = getDBConnection().prepareStatement(
                                "insert into sequential_content_log"
                                        + "(content_service_id,subscriber,"
                                        + "last_content_day) "
                                        + "values(?,?,?)");

                        thpstmt.setLong(1, service.getServiceID());
                        thpstmt.setString(2, recipientMSISDN);
                        thpstmt.setLong(3, lastContentDay);
                        thpstmt.executeUpdate();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }

                long totalContentCount = 0;

                try {
                    Statement contentRows = getDBConnection().createStatement();
                    ResultSet contentRowRS = contentRows.executeQuery(
                            "select count(*) as cnt from equential_content "
                                    + "where content_service_id = " + service.getServiceID());
                    while (contentRowRS.next()) {
                        totalContentCount = contentRowRS.getLong("cnt");
                    }
                } catch (Exception E) {

                }
                //Query and send the actual content...
                statement = getDBConnection().createStatement();

                resultSet = statement.executeQuery(
                        "select distinct on (a.subscriber) a.content_service_id, "
                                + " b.content_blast_day,  \n"
                                + " a.subscriber, a.last_content_day \n"
                                + " from sequential_content_log a, sequential_content b \n"
                                + " where a.content_service_id = " + service.getServiceID() + " "
                                + "and  a.content_service_id = b.content_service_id "
                                + "and a.subscriber = '" + recipientMSISDN + "' limit 1");

                //user has previusly recieved content
                while (resultSet.next()) {

                    //Check if he has received the last content of if he has never received content..If he has, we 'll start all over.
                    if (resultSet.getInt("last_content_day") == totalContentCount
                            || resultSet.getInt("last_content_day") == 0) {
                        lastContentDay = 1;
                    } else {
                        lastContentDay = resultSet.getInt("last_content_day") + 1;
                    }
                    //Query and send the actual content...
                    try {

                        selectPstmt = getDBConnection().prepareStatement(
                                "select a.content from sequential_content a, "
                                        + "subscription_service b "
                                        + "where content_blast_day = ? "
                                        + "and a.content_service_id = ? "
                                        + "and a.content_service_id = b.id limit 1");

                        selectPstmt.setLong(1, lastContentDay);
                        selectPstmt.setLong(2, service.getServiceID());
                        rs = selectPstmt.executeQuery();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //Determine src module...for daily paid content reporting...
                    String srcModule = "DailyAlert";
                    if (Long.parseLong(service.getServicePeriod()) == 0) {
                        srcModule = "DailyPaidContent";
                    }

                    while (rs.next()) {

                        if (service.getServiceSubType().equalsIgnoreCase("PSA")
                                && Long.parseLong(this.service.getServicePeriod()) == 0) {

                        } else if (service.getServiceSubType().equalsIgnoreCase("EDB")
                                && Long.parseLong(service.getServicePeriod()) == 0) {

                        } else if (service.getServiceSubType().equalsIgnoreCase("UCIP")
                                && Long.parseLong(service.getServicePeriod()) == 0) {
                            logger.error("It's UCIP");
                            try {
                                AirtelUCIP airtelIBMUCIP = AirtelUCIP.getInstance();
                                airtelUCIPBillingInstantSingleContent(airtelIBMUCIP, service,
                                        recipientMSISDN, formatText(rs.getString("content")));
                            } catch (Exception ex) {
                            }

                        } else {

                            try {
                                getSendSMS().SMSBOX(service.getServiceSubType(),
                                        service.getServiceMTShortCode(),
                                        resultSet.getString("subscriber"),
                                        formatText(rs.getString("content")),
                                        service.getServiceName(),
                                        service.getServiceID(),
                                        srcModule,
                                        service.getServiceNetwork(),
                                        "24");
                            } catch (Exception e) {
                                logger.info("Error while sending sequential content.");
                                e.printStackTrace();
                            }

                            //Update sequential content log table..
                            try {
                                logger.info("UPDATING SEQ CONTENT LOG..");
                                String userNumbert = resultSet.getString("subscriber").substring(resultSet.getString("subscriber").length() - 9);
                                long lastCDay = resultSet.getLong("last_content_day") + 1;
                                PreparedStatement pstmt = getDBConnection().prepareStatement(
                                        "update sequential_content_log "
                                                + "set last_content_day = " + lastCDay + ", last_content_date = CURRENT_TIMESTAMP  "
                                                + "where subscriber ilike '%" + userNumbert + "' "
                                                + "and  content_service_id = " + service.getServiceID());
                                pstmt.executeUpdate();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
                //update the archive...
                updateSeqContentLogCache();

            } else if (service.getServiceContentDeliveryMethod().equalsIgnoreCase("byDate")) {
                logger.info("By date......");
                selectPstmt = getDBConnection().prepareStatement(
                        "select a.content from content a, "
                                + "subscription_service b "
                                + "where (trim(a.content_date_of_blast) = '" + formatDateToString(new Date()).trim() + "' "
                                + "or trim(a.content_date_of_blast) = '" + cusFormatDateToString(new Date()).trim() + "') "
                                + "and a.content_service_id = " + service.getServiceID() + " "
                                + "and a.content_service_id = b.id limit 1");

                ResultSet crs = selectPstmt.executeQuery();

                while (crs.next()) {
                    logger.info("There's conetnt......");
                    try {
                        logger.info("We reach content side...");

                        if (service.getServiceSubType().equalsIgnoreCase("PSA")
                                && Long.parseLong(this.service.getServicePeriod()) == 0) {

                        } else if (service.getServiceSubType().equalsIgnoreCase("EDB")
                                && Long.parseLong(service.getServicePeriod()) == 0) {

                        } else if (service.getServiceSubType().equalsIgnoreCase("UCIP")
                                && Long.parseLong(service.getServicePeriod()) == 0) {
                            try {

                                logger.info("NAA UCIP");
                                AirtelUCIP airtelIBMUCIP = AirtelUCIP.getInstance();
                                airtelUCIPBillingInstantSingleContent(airtelIBMUCIP, service,
                                        recipientMSISDN, formatText(crs.getString("content")));

                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }

                        } else {

                            //In integrat mtn...
                            if (service.getServiceNetwork().trim().equalsIgnoreCase("integrat_mtn")) {

                                logger.info("Sending to SDP ");
                                getSendSMS().SMSBOX_WITH_METADATA(service.getServiceSubType(),
                                        " ", recipientMSISDN, formatText(crs.getString("content")),
                                        service.getServiceName(), service.getServiceID(),
                                        "DailyAlert", service.getServiceNetwork(),
                                        "31", "?smpp?TLV_higate_service="
                                                + service.getServiceMTShortCode());
                            } else {
                                getSendSMS().SMSBOX(service.getServiceSubType(),
                                        service.getServiceMTShortCode(), recipientMSISDN, formatText(crs.getString("content")),
                                        service.getServiceName(), service.getServiceID(), "DailyAlert", service.getServiceNetwork(),
                                        "24");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //}

                }

            } else {

                logger.info("By Random......");
                selectPstmt = getDBConnection().prepareStatement(
                        "select a.content from content a, "
                                + "subscription_service b "
                                + "where and a.content_service_id = " + service.getServiceID() + " "
                                + "and a.content_service_id = b.id order by random() limit 1");
                ResultSet crs = selectPstmt.executeQuery();

                while (crs.next()) {
                    logger.info("There's conetnt......");
                    try {
                        logger.info("We reach content side...");
                        if (service.getServiceSubType().equalsIgnoreCase("PSA")
                                && Long.parseLong(this.service.getServicePeriod()) == 0) {

                        } else if (service.getServiceSubType().equalsIgnoreCase("EDB")
                                && Long.parseLong(service.getServicePeriod()) == 0) {

                        } else if (service.getServiceSubType().equalsIgnoreCase("UCIP")
                                && Long.parseLong(service.getServicePeriod()) == 0) {
                            logger.info("NAA UCIP");
                            try {
                                AirtelUCIP airtelIBMUCIP = AirtelUCIP.getInstance();

                                airtelUCIPBillingInstantSingleContent(airtelIBMUCIP, service,
                                        recipientMSISDN, formatText(rs.getString("content")));

                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }

                        } else {
                            getSendSMS().SMSBOX(service.getServiceSubType(),
                                    service.getServiceMTShortCode(), recipientMSISDN, formatText(crs.getString("content")),
                                    service.getServiceName(), service.getServiceID(), "DailyAlert", service.getServiceNetwork(),
                                    "24");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //}
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings({"CallToPrintStackTrace", "UseSpecificCatch"})
    public void airtelUCIPBillingInstantSingleContent(AirtelUCIP airtelIBMUCIP, Service service, String msisdn, String content) {

        String srcModule = "DailyPaidContent";

        if (Long.parseLong(service.getServicePeriod()) > 0) {
            srcModule = " DailyAlert";
        }
        ///END
        try {

            try {
                //boolean subscriberBilled = false;
                logger.info("WE ENTER SUB LOOP");

                try {
                    airtelIBMUCIP.makeAirtelChargingRequest(
                            service, srcModule, service.getServiceBillingShortCode(),
                            "Content Purchase", content, msisdn);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //}

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            logger.info("Error while processing daily alert content.");
            e.printStackTrace();
        }
    }

    @SuppressWarnings({"CallToThreadDumpStack", "Convert2Diamond", "CallToPrintStackTrace"})
    public void updateSeqContentLogCache() {
        logger.info("Updating SequentialContentLogCache...");
        SequentialContentLog seqCotentLog;
        ArrayList<SequentialContentLog> seqContentLogCache;
        seqContentLogCache = new ArrayList<SequentialContentLog>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = getDBConnection().createStatement();
            resultSet = statement.executeQuery(
                    getServletContext().getInitParameter("seqcontentLogQuery"));

            //We place a lock on the seqCotentLog cache untill it finishes updating....
            synchronized (this) {
                while (resultSet.next()) {
                    try {
                        seqCotentLog = new SequentialContentLog();
                        seqCotentLog.setSubscriber(resultSet.getString("subscriber"));
                        seqCotentLog.setContentServiceID(resultSet.getLong("content_service_id"));
                        seqCotentLog.setContentStartDate(resultSet.getString("content_start_date"));
                        seqCotentLog.setLastContentDate(resultSet.getString("last_content_date"));
                        seqCotentLog.setLastContentDay(resultSet.getLong("last_content_day"));
                        seqContentLogCache.add(seqCotentLog);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //Create cache of seqCotentLog with all
                getServletContext().setAttribute("totalSeqContentLogCacheSize", seqContentLogCache.size());
                getServletContext().setAttribute("totalSeqContentLogCache", seqContentLogCache);
            }

        } catch (Exception ex) {
            logger.info(ex.getMessage());
        }
    }

}
