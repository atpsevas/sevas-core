package org.esme.gateway.kannel;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.esme.broker.AppBroker;
import org.esme.utils.pingenerator.RandomPasswordGenerator;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WEB2SMS {

    private static WEB2SMS web2SMS = null;
    //private static  AppLogger appLogger = null;
    private static SendSMS sendSMS;
    private static String DLRURL;
    //
    private static Properties prop;
    Random generator = new Random();

    //Instance of WEB2SMS  can only be created by call this method...
    @SuppressWarnings("CallToPrintStackTrace")
    public static synchronized WEB2SMS getInstance() {
        try {
            if (web2SMS == null) {
                web2SMS = new WEB2SMS();
                sendSMS = SendSMS.getInstance();
            }

            prop = AppBroker.getPropertyFileHandle();

        } catch (Exception ex) {
        }
        return web2SMS;
    }

    //Send SMS using HTTP.
    @SuppressWarnings({"CallToThreadDumpStack", "UseSpecificCatch", "CallToPrintStackTrace"})
    public void SMSBOX(String serviceType, String send, String rec,
                       String message, String serviceName, long serviceID,
                       String srcMod, String smsc, String dlr_Mask) {

        org.esme.broker.AppBroker.sevasLogger.error("The SMSC is ..... " + smsc);
        //We use SDP Send SMS web service client to send messages to mtn subscribers.......
//        try {
//            if (smsc.equalsIgnoreCase("mtn")) {
//                org.esme.broker.AppBroker.sevasLogger.error("The number is MTN.....");
//                try {
//                    String status = callMTNSDPSendSMSWebServiceClient(
//                            send, rec, message, serviceID, srcMod, serviceType, serviceName);
//                    org.esme.broker.AppBroker.sevasLogger.info(status);
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//                //return;
//            }
//        } catch (Exception e) {
//        }

        String servType = serviceType;
        String recipient = rec;
        String sender = send;
        String text = message;
        String servName = serviceName;
        long servID = serviceID;
        String srcModule = srcMod;
        String msgID = generateMessageID();
        //We only want to take success or failed dlr..It reduces the 
        //number of times we hit the database and open HTTP conection
        String dlrMask = dlr_Mask;
        String theSMSC = smsc;

        @SuppressWarnings("LocalVariableHidesMemberVariable")
        String dlrURL = encodeDLRURL(servType, send, rec, message, servName, srcModule, smsc, servID, dlrMask);

        try {
            sendSMS.smsEncoder(theSMSC, sender,
                    recipient, formatText(text), msgID, dlrMask, dlrURL);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    //Send SMS using HTTP.
    @SuppressWarnings({"CallToThreadDumpStack", "UseSpecificCatch", "CallToPrintStackTrace"})
    public void SILENCE_SMSBOX(String serviceType, String send, String rec,
                               String message, String serviceName, long serviceID,
                               String srcMod, String smsc, String dlr_Mask) {
        //We use SDP Send SMS web service client to send messages to mtn subscribers.......
        if (smsc.equalsIgnoreCase("mtn")) {
            try {
                String status = callMTNSDPSendSMSWebServiceClient(
                        send, rec, message, serviceID, srcMod, serviceType, serviceName);
                org.esme.broker.AppBroker.sevasLogger.info(status);
            } catch (Exception ex) {
            }
            return;
        }

        String servType = serviceType;
        String recipient = rec;
        String sender = send;
        String text = message;
        String servName = serviceName;
        long servID = serviceID;
        String srcModule = srcMod;
        String msgID = generateMessageID();

        //We only want to take success or failed dlr..It reduces the 
        //number of times we hit the database and open HTTP conection
        String dlrMask = dlr_Mask;
        String theSMSC = smsc;

        @SuppressWarnings("LocalVariableHidesMemberVariable")
        String dlrURL = encodeDLRURL(servType, send, rec, message, servName, srcModule, smsc, servID, dlrMask);

        try {
            sendSMS.silenceSMSEncoder(theSMSC, sender,
                    recipient, formatText(text), msgID, dlrMask, dlrURL);
        } catch (Exception ex) {
            ex.getMessage();
        }

    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace", "UseSpecificCatch"})
    public void SMSBOX_NODLR(String send, String rec,
                             String message, String smsc) {
        String recipient = rec;
        String sender = send;
        String text = message;

        //We only want to take success or failed dlr..It reduces the 
        //number of times we hit the database and open HTTP conection
        String theSMSC = smsc;
        try {
            sendSMS.smsEncoderWithoutDLR(theSMSC, sender,
                    recipient, formatText(text));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void SILENCE_SMSBOX_NODLR(String send, String rec,
                                     String message, String smsc) {
        String recipient = rec;
        String sender = send;
        String text = message;
        //We only want to take success or failed dlr..It reduces the 
        //number of times we hit the database and open HTTP conection
        String theSMSC = smsc;
        try {
            sendSMS.silenceSMSEncoderNoDLR(theSMSC, sender,
                    recipient, formatText(text));
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public void SMSBOX_WITH_METADATA(String serviceType, String send, String rec,
                                     String message, String serviceName, long serviceID,
                                     String srcMod, String smsc, String dlr_Mask, String metadata) {

        SendSMS sendSMS = SendSMS.getInstance();
        String servType = serviceType;
        String recipient = rec;
        String sender = send;
        String text = message;
        String servName = serviceName;
        long servID = serviceID;
        String srcModule = srcMod;
        String msgID = generateMessageID();
        //We only want to take success or failed dlr..It reduces the 
        //number of times we hit the database and open HTTP conection
        String dlrMask = dlr_Mask;
        String theSMSC = smsc;

        @SuppressWarnings("LocalVariableHidesMemberVariable")
        String dlrURL = encodeDLRURL(servType, send, rec, message, servName, srcModule, smsc, servID, dlrMask);

        try {
            sendSMS.smsEncoderWithMetadata(theSMSC, sender,
                    recipient, formatText(text), msgID, dlrMask, dlrURL, metadata);

            org.esme.broker.AppBroker.sevasLogger.info("META VALUE " + metadata);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String encodeDLRURL(String serviceType,
                               String send, String rec,
                               String message, String serviceName,
                               String srcMod, String smsc, long serviceID, String dlr_mask) {

        String servType = serviceType;
        String recipient = rec;
        String sender = send;
        String text = message;
        String servName = serviceName;
        long servID = serviceID;
        String srcModule = srcMod;
        String msgID = generateMessageID();
        //Sucess or failure...
        String dlrMask = dlr_mask;
        String theSMSC = smsc;

        org.esme.broker.AppBroker.sevasLogger.error(DLRURL);

        DLRURL = sendSMS.getDlrURL();
        //org.esme.broker.AppBroker.sevasLogger.error(DLRURL);

        //We load the balance the app by having another instance for DLR...
        String availableURLs = DLRURL;

        String dlrURL = availableURLs + encodeParams(theSMSC)
                + "&sub_type=" + encodeParams(servType)
                + "&msg_id=" + encodeParams(msgID)
                + "&sc=" + encodeParams(sender)
                + "&msisdn=" + encodeParams(recipient)
                + "&msg=" + encodeParams(text)
                + "&serv_id=" + servID
                + "&serv_name=" + encodeParams(servName)
                + "&src_module=" + encodeParams(srcModule)
                + "&dlr_mask=" + encodeParams(dlrMask)
                + "&dlr=%d&billing_time=" + (encodeParams(formatDateTillSecondToString(new Date())));
        return dlrURL;
    }

    public String generateMessageID() {
        return RandomPasswordGenerator.randomPasswordGenerator();
    }

    public String formatText(String text) {
        String formattedContent;
        String myString = text.trim();
        byte[] ptext;
        try {
            ptext = myString.getBytes("ISO-8859-1");
            formattedContent = new String(ptext, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            return text;
        }
        return formattedContent;
    }

    public String formatDateTillSecondToString(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd hh:mm:ss";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }

    @SuppressWarnings({"CallToThreadDumpStack", "CallToPrintStackTrace"})
    public String encodeParams(String param) {
        String encParam = "";
        try {
            encParam = URLEncoder.encode(param, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return encParam;
    }

    @SuppressWarnings({"UseSpecificCatch", "null", "CallToPrintStackTrace"})
    public String callMTNSDPSendSMSWebServiceClient(String from, String to,
                                                    String text, long serviceID, String srcMod, String subType, String serviceName) {

        org.esme.broker.AppBroker.sevasLogger.info("Calling...SDP Send SMS Service....");
        String returnMsg = null;
        byte[] responseBody;
        URI uri;
        HttpGet httpget;
        HttpClient client;
        GetMethod method = null;
        @SuppressWarnings("Convert2Diamond")
        List<NameValuePair> sendSMSParams = new ArrayList<NameValuePair>();

        sendSMSParams.add(new BasicNameValuePair("to", to));
        sendSMSParams.add(new BasicNameValuePair("from", from));
        sendSMSParams.add(new BasicNameValuePair("text", text));
        sendSMSParams.add(new BasicNameValuePair("srcMod", srcMod));
        sendSMSParams.add(new BasicNameValuePair("subType", subType));
        sendSMSParams.add(new BasicNameValuePair("servName", serviceName));
        sendSMSParams.add(new BasicNameValuePair("servID", Long.toString(serviceID)));

        try {

            prop = AppBroker.getPropertyFileHandle();

//localhost
            //This is a send sms api from Hauweei....We wrap it in a local module to optimize thing...sdpSendSMSURL
            uri = URIUtils.createURI("http", "localhost", 8585, "/sevas/SDPSendSMS",
                    URLEncodedUtils.format(sendSMSParams, "UTF-8"), null);

            httpget = new HttpGet(uri);

            // Create an instance of HttpClient.
            client = new HttpClient();

            // Create a method instance and replace all possible poison.
            method = new GetMethod(uri.toString());

            // Provide custom retry handler is necessary
//            method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
//                    new DefaultHttpMethodRetryHandler(3, false));
            org.esme.broker.AppBroker.sevasLogger.error(uri);
            // Execute the method.
            int statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                org.esme.broker.AppBroker.sevasLogger.error("Method failed: " + method.getStatusLine());
            }
            responseBody = method.getResponseBody();
            // Deal with the response.
            returnMsg = new String(responseBody);
            org.esme.broker.AppBroker.sevasLogger.error(returnMsg);
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            method.releaseConnection();
        }
        return returnMsg;

    }

    public long getCurrentTimeInMillis(Date theDate) {
        long currentTime = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
            String dateInString = formatDateToString2(theDate);
            Date date = sdf.parse(dateInString);
            org.esme.broker.AppBroker.sevasLogger.info(dateInString);
            org.esme.broker.AppBroker.sevasLogger.info("Date - Time in milliseconds : " + date.getTime());
            currentTime = date.getTime();
//	Calendar calendar = Calendar.getInstance();
//	calendar.setTime(date);
//	org.esme.broker.AppBroker.sevasLogger.info("Calender - Time in milliseconds : " + calendar.getTimeInMillis());
        } catch (ParseException ex) {
            Logger.getLogger(WEB2SMS.class.getName()).log(Level.SEVERE, null, ex);
        }
        return currentTime;
    }

    public String formatDateToString2(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd hh:mm:ss";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }
}
