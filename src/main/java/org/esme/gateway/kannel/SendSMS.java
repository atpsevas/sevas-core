package org.esme.gateway.kannel;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.esme.broker.AppBroker;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class SendSMS {

    public static String dlrURL;
    private static SendSMS sendSMS;
    private static String gwPassword;
    private static String gwUserName;
    private static String gwIP;
    private static String gwPort;
    private static Properties prop;
    // private static AppLogger appLogger = null;

    //Instance of SendSMS  can only be created by call this method...
    @SuppressWarnings("CallToPrintStackTrace")
    public static synchronized SendSMS getInstance() {

        if (sendSMS == null) {

            prop = AppBroker.getPropertyFileHandle();
            //Logger 
            //appLogger = AppLogger.getInstance("/home/sevas/logs/send_sms_error.log");
            //Create an instance of Kannel send sms....
            sendSMS = new SendSMS();
            gwIP = prop.getProperty("sms_gateway_ip");
            gwPort = prop.getProperty("sms_gateway_port");
            gwPassword = prop.getProperty("sms_gateway_username");
            gwUserName = prop.getProperty("sms_gateway_password");
            dlrURL = prop.getProperty("dlr_url");
        }
        return sendSMS;
    }

    public String getDlrURL() {
        sendSMS = SendSMS.getInstance();
        return dlrURL;
    }

    public static void setDlrURL(String dlrURL) {
        SendSMS.dlrURL = dlrURL;
    }

    //&pid=64
    @SuppressWarnings({"null", "UseSpecificCatch"})
    public String smsEncoder(String smsc, String from, String to,
                             String text, String msg_id, String dlrmask, String dlrurl) {

        String returnMsg = null;
        URI uri;
        HttpGet httpget;
        HttpClient client;
        GetMethod method = null;
        //int sendSMSPort = 13031;
        @SuppressWarnings("UnusedAssignment")
        byte[] responseBody = null;
        @SuppressWarnings("Convert2Diamond")
        List<NameValuePair> sendSMSParams = new ArrayList<NameValuePair>();

        sendSMSParams.add(new BasicNameValuePair("smsc", smsc));
        sendSMSParams.add(new BasicNameValuePair("username", gwUserName));
        sendSMSParams.add(new BasicNameValuePair("password", gwPassword));
        sendSMSParams.add(new BasicNameValuePair("from", from));
        sendSMSParams.add(new BasicNameValuePair("to", to));
        sendSMSParams.add(new BasicNameValuePair("text", text));
        sendSMSParams.add(new BasicNameValuePair("msg_id", msg_id));
        sendSMSParams.add(new BasicNameValuePair("dlr-mask", dlrmask));
        sendSMSParams.add(new BasicNameValuePair("dlr-url", dlrurl));
        try {
            uri = URIUtils.createURI("http", gwIP, Integer.parseInt(gwPort), "/cgi-bin/sendsms",
                    URLEncodedUtils.format(sendSMSParams, "UTF-8"), null);

            httpget = new HttpGet(uri);

            // Create an instance of HttpClient.
            client = new HttpClient();

            // Create a method instance and replace all possible poison.
            method = new GetMethod(uri.toString());

            // Execute the method.
            int statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                org.esme.broker.AppBroker.sevasLogger.error("Method failed: " + method.getStatusLine());
            }
            responseBody = method.getResponseBody();
            returnMsg = new String(responseBody);
            // Deal with the response.
            // Use caution: ensure correct character encoding and is not binary data
            org.esme.broker.AppBroker.sevasLogger.info(new String(responseBody));
        } catch (Exception e) {
            //appLogger.log(e.getMessage());
        } finally {
            method.releaseConnection();
        }
        return returnMsg;
    }

    public String silenceSMSEncoder(String smsc, String from, String to,
                                    String text, String msg_id, String dlrmask, String dlrurl) {

        String returnMsg = null;
        URI uri;
        HttpGet httpget;
        HttpClient client;
        GetMethod method = null;
        //int sendSMSPort = 13031;
        @SuppressWarnings("UnusedAssignment")
        byte[] responseBody = null;
        @SuppressWarnings("Convert2Diamond")
        List<NameValuePair> sendSMSParams = new ArrayList<NameValuePair>();

        sendSMSParams.add(new BasicNameValuePair("smsc", smsc));
        sendSMSParams.add(new BasicNameValuePair("username", gwUserName));
        sendSMSParams.add(new BasicNameValuePair("password", gwPassword));
        sendSMSParams.add(new BasicNameValuePair("from", from));
        sendSMSParams.add(new BasicNameValuePair("to", to));
        sendSMSParams.add(new BasicNameValuePair("text", text));
        sendSMSParams.add(new BasicNameValuePair("msg_id", msg_id));
        sendSMSParams.add(new BasicNameValuePair("pid", "64"));
        sendSMSParams.add(new BasicNameValuePair("dlr-mask", dlrmask));
        sendSMSParams.add(new BasicNameValuePair("dlr-url", dlrurl));
        try {
            uri = URIUtils.createURI("http", gwIP, Integer.parseInt(gwPort), "/cgi-bin/sendsms",
                    URLEncodedUtils.format(sendSMSParams, "UTF-8"), null);

            httpget = new HttpGet(uri);

            // Create an instance of HttpClient.
            client = new HttpClient();

            // Create a method instance and replace all possible poison.
            method = new GetMethod(uri.toString());

            int statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                org.esme.broker.AppBroker.sevasLogger.error("Method failed: " + method.getStatusLine());
            }
            responseBody = method.getResponseBody();
            returnMsg = new String(responseBody);
            // Deal with the response.
            org.esme.broker.AppBroker.sevasLogger.info(new String(responseBody));
        } catch (Exception e) {
            //appLogger.log(e.getMessage());
        } finally {
            method.releaseConnection();
        }
        return returnMsg;
    }

    public String silenceSMSEncoderNoDLR(String smsc, String from, String to,
                                         String text) {

        String returnMsg = null;
        URI uri;
        HttpGet httpget;
        HttpClient client;
        GetMethod method = null;
        //int sendSMSPort = 13031;
        @SuppressWarnings("UnusedAssignment")
        byte[] responseBody = null;
        @SuppressWarnings("Convert2Diamond")
        List<NameValuePair> sendSMSParams = new ArrayList<NameValuePair>();

        sendSMSParams.add(new BasicNameValuePair("smsc", smsc));
        sendSMSParams.add(new BasicNameValuePair("username", gwUserName));
        sendSMSParams.add(new BasicNameValuePair("password", gwPassword));
        sendSMSParams.add(new BasicNameValuePair("from", from));
        sendSMSParams.add(new BasicNameValuePair("to", to));
        sendSMSParams.add(new BasicNameValuePair("text", text));
        sendSMSParams.add(new BasicNameValuePair("pid", "64"));
        try {
            uri = URIUtils.createURI("http", gwIP, Integer.parseInt(gwPort), "/cgi-bin/sendsms",
                    URLEncodedUtils.format(sendSMSParams, "UTF-8"), null);

            httpget = new HttpGet(uri);

            // Create an instance of HttpClient.
            client = new HttpClient();

            // Create a method instance and replace all possible poison.
            method = new GetMethod(uri.toString());

            int statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                org.esme.broker.AppBroker.sevasLogger.error("Method failed: " + method.getStatusLine());
            }
            responseBody = method.getResponseBody();
            returnMsg = new String(responseBody);
            // Deal with the response.
            org.esme.broker.AppBroker.sevasLogger.info(new String(responseBody));
        } catch (Exception e) {
            //appLogger.log(e.getMessage());
        } finally {
            method.releaseConnection();
        }
        return returnMsg;
    }

    @SuppressWarnings({"null", "UseSpecificCatch"})
    public String smsEncoderWithoutDLR(String smsc, String from, String to,
                                       String text) {

        String returnMsg = null;
        URI uri;
        HttpGet httpget;
        HttpClient client;
        GetMethod method = null;

        byte[] responseBody;

        @SuppressWarnings("Convert2Diamond")
        List<NameValuePair> sendSMSParams = new ArrayList<NameValuePair>();

        sendSMSParams.add(new BasicNameValuePair("smsc", smsc));
        sendSMSParams.add(new BasicNameValuePair("username", gwUserName));
        sendSMSParams.add(new BasicNameValuePair("password", gwPassword));
        sendSMSParams.add(new BasicNameValuePair("from", from));
        sendSMSParams.add(new BasicNameValuePair("to", to));
        sendSMSParams.add(new BasicNameValuePair("text", text));
        try {
            uri = URIUtils.createURI("http", gwIP, Integer.parseInt(gwPort), "/cgi-bin/sendsms",
                    URLEncodedUtils.format(sendSMSParams, "UTF-8"), null);
//       
            httpget = new HttpGet(uri);

            // Create an instance of HttpClient.
            client = new HttpClient();

            // Create a method instance and replace all possible poison.
            method = new GetMethod(uri.toString());

            int statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                org.esme.broker.AppBroker.sevasLogger.error("Method failed: " + method.getStatusLine());
            }

            responseBody = method.getResponseBody();
            returnMsg = new String(responseBody);

            // Deal with the response.
            // Use caution: ensure correct character encoding and is not binary data
            org.esme.broker.AppBroker.sevasLogger.info(new String(responseBody));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            method.releaseConnection();
        }
        return returnMsg;
    }

    @SuppressWarnings({"null", "UseSpecificCatch"})
    public String smsEncoderWithMetadata(String smsc, String from, String to,
                                         String text, String msg_id, String dlrmask, String dlrurl, String metadata) {

        String returnMsg = null;
        URI uri;
        HttpGet httpget;
        HttpClient client;
        GetMethod method = null;
        //int sendSMSPort = 13031;
        @SuppressWarnings("UnusedAssignment")
        byte[] responseBody = null;
        @SuppressWarnings("Convert2Diamond")
        List<NameValuePair> sendSMSParams = new ArrayList<NameValuePair>();

        sendSMSParams.add(new BasicNameValuePair("smsc", smsc));
        sendSMSParams.add(new BasicNameValuePair("username", gwUserName));
        sendSMSParams.add(new BasicNameValuePair("password", gwPassword));
        sendSMSParams.add(new BasicNameValuePair("from", from));
        sendSMSParams.add(new BasicNameValuePair("to", to));
        sendSMSParams.add(new BasicNameValuePair("text", text));
        sendSMSParams.add(new BasicNameValuePair("msg_id", msg_id));
        sendSMSParams.add(new BasicNameValuePair("dlr-mask", dlrmask));
        sendSMSParams.add(new BasicNameValuePair("dlr-url", dlrurl));
        sendSMSParams.add(new BasicNameValuePair("meta-data", metadata));

        try {

            uri = URIUtils.createURI("http", gwIP, Integer.parseInt(gwPort), "/cgi-bin/sendsms",
                    URLEncodedUtils.format(sendSMSParams, "UTF-8"), null);

            org.esme.broker.AppBroker.sevasLogger.error(uri);

            httpget = new HttpGet(uri);

            // Create an instance of HttpClient.
            client = new HttpClient();

            // Create a method instance and replace all possible poison.
            method = new GetMethod(uri.toString());

            // Execute the method.
            int statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                org.esme.broker.AppBroker.sevasLogger.error("Method failed: " + method.getStatusLine());
            }
            responseBody = method.getResponseBody();
            returnMsg = new String(responseBody);
            // Deal with the response.
            // Use caution: ensure correct character encoding and is not binary data
            org.esme.broker.AppBroker.sevasLogger.info(new String(responseBody));
        } catch (Exception e) {
            //appLogger.log(e.getMessage());
        } finally {
            method.releaseConnection();
        }
        return returnMsg;
    }

}
