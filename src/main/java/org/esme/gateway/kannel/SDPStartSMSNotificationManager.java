package org.esme.gateway.kannel;

import org.esme.broker.AppBroker;

import java.util.Properties;

public class SDPStartSMSNotificationManager {

    private static SDPStartSMSNotificationManager sDPStartSMSNotificationManager = null;

    //    
    private static Properties prop;

    //Instance of SDPStartSMSNotificationManager can only be created by call this method...
    @SuppressWarnings("CallToPrintStackTrace")
    public static synchronized SDPStartSMSNotificationManager getInstance() {
        try {
            if (sDPStartSMSNotificationManager == null) {
                sDPStartSMSNotificationManager = new SDPStartSMSNotificationManager();
                prop = AppBroker.getPropertyFileHandle();

            }
        } catch (Exception ex) {
        }
        return sDPStartSMSNotificationManager;
    }

}
