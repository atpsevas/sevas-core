 /*
  * To change this template, choose Tools | Templates
  * and open the template in the editor.
  */
 package org.esme.gateway.kannel;

 import org.esme.utils.http.HttpClient;

 import java.io.IOException;
 import java.io.UnsupportedEncodingException;
 import java.net.MalformedURLException;
 import java.util.ArrayList;

//import java.util.logging.Level;
//import java.util.logging.Logger;

 /**
  * @author abiola
  */
 public class Kannel {

     private HttpClient httpClient;
     private String httpHeader;
     private String baseURL;
     private String user;
     private String password;
     private String requestURL;
     private String response;

     /**
      * Constructures
      */
     public Kannel() {
         this.httpClient = new HttpClient();
         this.httpClient.setBaseUrl(this.baseURL);
     }

     public Kannel(String host, int port) {
         this();
         this.baseURL = "http://" + host + ":" + port + "/cgi-bin/";
     }

     public Kannel(String host, int port, String user, String password)
             throws InstantiationException, IllegalAccessException,
             UnsupportedEncodingException {
         this(host, port);
         this.user = user;
         this.password = password;
         // Initialize HttpClient class.
         initialize();
     }

     /**
      * To be implemented
      */
     private static String encodeNokiaHex(String hex) {
         ///TODO: Find out java's implementation of String.Insert()
         return null;
//        int hexLength = hex.Length;
//
//        while ((hexLength -= 2) > 0) {
//            hex = hex.Insert(toneLength, "%");
//        }
//
//        hex = hex.Insert(0, "%");
//        return hex;
     }

     public String getHttpHeader() {
         return httpHeader;
     }

     public void setHttpHeader(String httpHeader) {
         this.httpHeader = httpHeader;
     }

     public String getBaseURL() {
         return baseURL;
     }

     public void setBaseURL(String baseURL) {
         this.baseURL = baseURL;
     }

     public String getUser() {
         return user;
     }

     public void setUser(String user) {
         this.user = user;
     }

     public String getPassword() {
         return password;
     }

     public void setPassword(String password) {
         this.password = password;
     }

     public String getRequestURL() {
         return requestURL;
     }

     public void setRequestURL(String requestURL) {
         this.requestURL = requestURL;
     }

     public String getResponse() {
         return response;
     }

     public void setResponse(String response) {
         this.response = response;
     }

     //    public Kannel(String host, int port, String user, String password,
//            String shortMessage) {
//        this(host, port, user, password);
//        this.shortMessage = shortMessage;
//    }
     private void initialize() throws InstantiationException,
             IllegalAccessException, UnsupportedEncodingException {

         this.httpClient.setBaseUrl(this.requestURL);
         this.httpClient.setParameterList(ArrayList.class.newInstance());
         this.httpClient.addUrlParameter("user", this.user);
         this.httpClient.addUrlParameter("password", this.password);
     }

     public String sendText(String sourceMsisdn, String destMsisdn,
                            String shortMessage, String smsc) throws UnsupportedEncodingException,
             MalformedURLException, IOException, Exception {

         this.requestURL = baseURL + "sendsms";
         initialize();

         this.httpClient.addUrlParameter("from", sourceMsisdn.trim());
         this.httpClient.addUrlParameter("to", destMsisdn.trim());
         this.httpClient.addUrlParameter("text", shortMessage.trim());
         this.httpClient.addUrlParameterDontEncode("smsc", smsc);
         return this.httpClient.get();
     }

     public String sendWapPushSI(String sourceMsisdn, String destMsisdn,
                                 String description, String url, String smsc) throws UnsupportedEncodingException,
             MalformedURLException, IOException, Exception {

         this.requestURL = baseURL + "sendsms";
         initialize();
         org.esme.broker.AppBroker.sevasLogger.info(requestURL);

         String otaString = "%01%06%04%03%AE%81%EA%02%05%6A%00%45%C6%0b%03";
         otaString += java.net.URLEncoder.encode(url, "UTF-8");
         otaString += "%00%11%038@vodafone.pt%00%08%01%03";
         otaString += java.net.URLEncoder.encode(description, "UTF-8");
         otaString += "%00%01%01";

         this.httpClient.addUrlParameter("from", sourceMsisdn.trim());
         this.httpClient.addUrlParameter("to", destMsisdn.trim());
         this.httpClient.addUrlParameterDontEncode("text", otaString);
         this.httpClient.addUrlParameterDontEncode("smsc", smsc);
         this.httpClient.addUrlParameterDontEncode("udh", "%06%05%04%0B%84%23%F0");

         return this.httpClient.get();
     }

     public String sendOtaBookmark(String sourceMsisdn, String destMsisdn,
                                   String description, String url)
             throws InstantiationException, IllegalAccessException,
             MalformedURLException, UnsupportedEncodingException, IOException, Exception {

         this.requestURL = baseURL + "sendsms";
         initialize();

         String otaString = "%01%06%2D%1F%2B%61%70%70%6C%69%63%61%74%69%6F%6E%2F" +
                 "%78%2D%77%61%70%2D%70%72%6F%76%2E%62%72%6F%77%73%65%72%2D%62" +
                 "%6F%6F%6B%6D%61%72%6B%73%00%81%EA%00%01%00%45%C6%7F%01%87%15%11%03";
         otaString += java.net.URLEncoder.encode(description, "UTF-8");
         otaString += "%00%01%87%17%11%03";
         otaString += java.net.URLEncoder.encode(url, "UTF-8");
         otaString += "%00%01%01%01";

         this.httpClient.addUrlParameter("from", sourceMsisdn.trim());
         this.httpClient.addUrlParameter("to", destMsisdn.trim());
         this.httpClient.addUrlParameterDontEncode("text", otaString);
         this.httpClient.addUrlParameterDontEncode("udh", "%06%05%04%C3%4F%00%00");

         return this.httpClient.get();
     }

     /**
      * To be implemented
      */
     public String sendOmaOtaBrowserSetting(String sourceMsisdn, String destMsisdn,
                                            String otaId, String otaUser, String otaPassword) {
         return null;
     }

     public String sendEmsTone(String responseMsisdn, String requestMsisdn,
                               String tone) throws IllegalAccessException, InstantiationException,
             UnsupportedEncodingException, MalformedURLException, IOException,
             Exception {

         this.requestURL = baseURL + "sendsms";
         initialize();

         // Add needed tags for the tone
         String emsEncodedTone = "BEGIN:IMELODY\n" + "FORMAT:CLASS1.0" + "MELODY:" + tone + "\nEND:IMELODY";

         return sendText(responseMsisdn, requestMsisdn, emsEncodedTone, "THESMSC");
     }


     /// Kannel allows us to send OTA Browser Settings as a Url encoded XML
     /// this method lets us do just that by taking the xml document as a string
     /// argument and sends it via kannel
     public String sendSENOta(String sourceMsisdn, String destMsisdn,
                              String otaXmlDocument) throws InstantiationException,
             IllegalAccessException, UnsupportedEncodingException,
             MalformedURLException, IOException, Exception {

         this.requestURL = baseURL + "sendota";
         initialize();

         this.httpClient.addUrlParameter("from", sourceMsisdn);
         this.httpClient.addUrlParameter("to", destMsisdn);
         this.httpClient.addUrlParameter("type", "settings");
         this.httpClient.addUrlParameter("text", otaXmlDocument);

         return this.httpClient.get();
     }

 }
