package org.esme.gateway.kannel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayDeque;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author saheedalbert
 */
public class KannelAccessLogFileTailer {
    private static final Logger logger = Logger.getLogger("Tailer");
    ArrayDeque<String> lines = new ArrayDeque<>();
    private long tooLateTime = -1;
    private long maxMsToWait = 0;
    private File file = null;
    private long offset = 0;
    private int lineCount = 0;
    private boolean ended = false;
    private WatchService watchService = null;

    /**
     * Allows output of a file that is being updated by another process.
     *
     * @param file                   to watch and read
     * @param maxTimeToWaitInSeconds max timeout; after this long without changes,
     *                               watching will stop. If =0, watch will continue until <code>stop()</code>
     *                               is called.
     */
    public KannelAccessLogFileTailer(File file, long maxTimeToWaitInSeconds) {
        this.file = file;
        this.maxMsToWait = maxTimeToWaitInSeconds * 1000;
    }

    /**
     * Example main. Beware: the watch listens on a whole folder, not on a single
     * file. Any update on a file within the folder will trigger a read-update.
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        String fn = args.length == 0 ? "/tmp/test/log.txt" : args[0];
        KannelAccessLogFileTailer t = new KannelAccessLogFileTailer(new File(fn), 10);
        t.start();
        while (!t.hasEnded()) {
            while (t.linesAvailable()) {
                org.esme.broker.AppBroker.sevasLogger.info(t.getLineNumber() + ": " + t.getLine());
            }
            Thread.sleep(500);
        }
    }

    /**
     * Start watch.
     */
    public void start() {
        updateOffset();
        // listens for FS events
        new Thread(new FileWatcher()).start();
        if (maxMsToWait != 0) {
            // kills FS event listener after timeout
            new Thread(new WatchDog()).start();
        }
    }

    /**
     * Stop watch.
     */
    public void stop() {
        if (watchService != null) {
            try {
                watchService.close();
            } catch (IOException ex) {
                logger.info("Error closing watch service");
            }
            watchService = null;
        }
    }

    private synchronized void updateOffset() {
        tooLateTime = System.currentTimeMillis() + maxMsToWait;
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            br.skip(offset);
            while (true) {
                String line = br.readLine();
                if (line != null) {
                    lines.push(line);
                    // this may need tweaking if >1 line terminator char
                    offset += line.length() + 1;
                } else {
                    break;
                }
            }
            br.close();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Error reading", ex);
        }
    }

    /**
     * @return true if lines are available to read
     */
    public boolean linesAvailable() {
        return !lines.isEmpty();
    }

    /**
     * @return next unread line
     */
    public synchronized String getLine() {
        if (lines.isEmpty()) {
            return null;
        } else {
            lineCount++;
            return lines.removeLast();
        }
    }

    /**
     * @return true if no more lines will ever be available,
     * because stop() has been called or the timeout has expired
     */
    public boolean hasEnded() {
        return ended;
    }

    /**
     * @return next line that will be returned; zero-based
     */
    public int getLineNumber() {
        return lineCount;
    }

    private class WatchDog implements Runnable {
        @Override
        public void run() {
            while (System.currentTimeMillis() < tooLateTime) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    // do nothing
                }
            }
            stop();
        }
    }

    private class FileWatcher implements Runnable {
        private final Path path = file.toPath().getParent();

        @Override
        public void run() {
            try {
                watchService = path.getFileSystem().newWatchService();
                path.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
                while (true) {
                    WatchKey watchKey = watchService.take();
                    if (!watchKey.reset()) {
                        stop();
                        break;
                    } else if (!watchKey.pollEvents().isEmpty()) {
                        updateOffset();
                    }
                    Thread.sleep(500);
                }
            } catch (InterruptedException ex) {
                logger.info("Tail interrupted");
            } catch (IOException ex) {
                logger.log(Level.WARNING, "Tail failed", ex);
            } catch (ClosedWatchServiceException ex) {
                // no warning required - this was a call to stop()
            }
            ended = true;
        }
    }
}
