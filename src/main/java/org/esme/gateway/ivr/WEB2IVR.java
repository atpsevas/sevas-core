
package org.esme.gateway.ivr;


import org.esme.dto.Service;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;


public class WEB2IVR {

    public static String voice_gateway_ip_PROP, voice_gateway_port_PROP,
            voice_gateway_endpoint_PROP, voice_callback_url_PROP;
    private static WEB2IVR web2IVR = null;
    private static Properties prop;

    //Instance of WEB2IVR  can only be created by call this method...
    @SuppressWarnings("CallToPrintStackTrace")
    public static synchronized WEB2IVR getInstance() throws FileNotFoundException {

        try {
            prop = new Properties();
            prop.load(new FileInputStream("/home/sevas/sevas.properties"));
        } catch (Exception e) {
        }

        try {
            if (web2IVR == null) {
                web2IVR = new WEB2IVR();

                try {
                    voice_gateway_ip_PROP = prop.getProperty("voice_gateway_ip");
                    voice_gateway_port_PROP = prop.getProperty("voice_gateway_port");
                    voice_gateway_endpoint_PROP = prop.getProperty("voice_gateway_endpoint");
                    voice_callback_url_PROP = prop.getProperty("voice_callback_url");

                } catch (Exception e) {
                    org.esme.broker.AppBroker.sevasLogger.error("ERROR: Could not load app property file.");
                    e.printStackTrace();
                }
            }

        } catch (Exception ex) {
            org.esme.broker.AppBroker.sevasLogger.error("Failed to get WEB2IVR Instance ");
        }
        return web2IVR;
    }

    public static String formatDateToString(Date date) {
        date = new Date();
        date.setTime(System.currentTimeMillis());
        String formattedDate = "yyyy-MM-dd";
        SimpleDateFormat sdFormat = new SimpleDateFormat(formattedDate);
        return sdFormat.format(date);
    }

    public static String convertStreamToStr(InputStream is) throws IOException {
        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        }
        return "";
    }

    public HashMap<String, String> generateParams(Service service, String msisdn) {
        HashMap<String, String> params = new HashMap();

        return params;
    }

    public void sendVoiceContent(String msisdn, Service service) {
        try {

            //Long callFileRandomID = Long.valueOf(rand.nextLong());
            String fileName = prop.getProperty("VoiceContentCallFiles")
                    + formatDateToString(new Date()) + "_" + msisdn + ".call";

            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)));
            out.println("Channel: SIP/ETISALAT_OG/" + msisdn.substring(msisdn.length() - 10) + "");
            out.println("MaxRetries: 2");
            out.println("RetryTime: 900");
            out.println("WaitTime: 25");
            out.println("Application: Playback");
            out.println("Data: " + prop.getProperty("VoiceContentDir") + service.getServiceID() + "/" + formatDateToString(new Date()));
            out.println("Callerid: " + service.getServiceContentSenderID());
            out.println("Archive: yes");
            out.flush();

            copyFile(fileName);

        } catch (Exception e) {
        }
    }

    public boolean copyFile(String sourceFile) {
        boolean returnMessage = true;
        try {
            File src = new File(sourceFile);
            try {
                ProcessBuilder pb = new ProcessBuilder(new String[]{"bash", "-c", "chown  asterisk:asterisk " + src.getAbsolutePath()});
                pb.redirectErrorStream(true);

                Process shell = pb.start();

                InputStream shellIn = shell.getInputStream();

                int shellExitStatus = shell.waitFor();

                String response = convertStreamToStr(shellIn);
                shellIn.close();
                System.err.append(response);
            } catch (Exception e) {
            }
            org.esme.broker.AppBroker.sevasLogger.info("Copying..." + src.getAbsolutePath());
            File dir = new File("/var/spool/asterisk/outgoing/");
            try {
                ProcessBuilder pb = new ProcessBuilder(new String[]{"bash", "-c", "mv " + src.getAbsolutePath() + " " + dir + "/"});
                pb.redirectErrorStream(true);

                Process shell = pb.start();

                InputStream shellIn = shell.getInputStream();

                int shellExitStatus = shell.waitFor();

                String response = convertStreamToStr(shellIn);
                shellIn.close();
                System.err.append(response);
            } catch (Exception e) {
            }
        } catch (Exception ioe) {
            returnMessage = false;
            ioe.getMessage();
            ioe.getCause();
            org.esme.broker.AppBroker.sevasLogger.info("Problem copying file to dir.");
        }
        return returnMessage;
    }

}
