/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.observers;

import org.esme.events.EventBase;

/**
 * @author tm30
 */
public class ObserverPatternDemo {

    public static void main(String[] args) {
        //create subject
        EventBase topic = new EventBase();

        //create observers
        ObserverInterface obj1 = new EventSubscriber("Obj1");
        ObserverInterface obj2 = new EventSubscriber("Obj2");
        ObserverInterface obj3 = new EventSubscriber("Obj3");

        //register observers to the subject
        topic.register(obj1);
        topic.register(obj2);
        topic.register(obj3);

        //attach observer to subject
        obj1.setSubject(topic);
        obj2.setSubject(topic);
        obj3.setSubject(topic);

        //check if any update is available
        obj1.update();

        //now send message to subject
        topic.postMessage("New Message");
    }

}