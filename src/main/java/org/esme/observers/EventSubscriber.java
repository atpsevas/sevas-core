/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.esme.observers;

import org.esme.events.EventInterface;

/**
 * @author tm30
 */
public class EventSubscriber implements ObserverInterface {

    private String name;
    private EventInterface topic;

    public EventSubscriber(String nm) {
        this.name = nm;
    }

    @Override
    public void update() {
        String msg = (String) topic.getUpdate(this);
        if (msg == null) {
            System.out.println(name + ":: No new message");
        } else
            System.out.println(name + ":: Consuming message::" + msg);
    }

    @Override
    public void setSubject(EventInterface sub) {
        this.topic = sub;
    }

}