/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import org.esme.broker.AppBroker;
import org.esme.dao.models.ContentLogs;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * @author saheedalbert
 */
public class iReportHelper {

    Connection connection;
    PreparedStatement statement;
    ResultSet resultSet;
    ArrayList<ContentLogs> logs;
    ContentLogs log;
    private long totalSent;
    private long submitted;
    private long delivered;
    private long failed;
    private long buffered;
    private long rejected;
    private long unKnownStatus;

    public iReportHelper(Connection conn) {
        this.connection = conn;
    }

    public long getRevTarget(String query) throws SQLException {

        AppBroker.getSevasLogger().info("Get Rev Target - " + query);

        statement = this.connection.prepareStatement(query);
        resultSet = statement.executeQuery();
        long returnedRevenue = 0;
        while (resultSet.next()) {
            returnedRevenue = resultSet.getLong("theTarget");
        }
        return returnedRevenue;
    }

    public long getPeriodicRev(String query) throws SQLException {
        statement = this.connection.prepareStatement(query);

        AppBroker.getSevasLogger().info("Get Periodic Revenue Query - " + query);
        resultSet = statement.executeQuery();
        long returnedRevenue = 0;
        while (resultSet.next()) {
            returnedRevenue = resultSet.getLong("main_charge") + resultSet.getLong("fallback_charge") + resultSet.getLong("fallback_charge_2") + resultSet.getLong("daily_paid_content");
        }
        return returnedRevenue;
    }


    public double getChurnRate(String query) throws SQLException {

        AppBroker.getSevasLogger().info("Get Churn Rate - " + query);

        statement = this.connection.prepareStatement(query);
        resultSet = statement.executeQuery();
        double churn = 0;
        long totalNewSub = 0;
        long totalInactiveSub = 0;

        while (resultSet.next()) {
            totalNewSub = resultSet.getLong("total");
            totalInactiveSub = resultSet.getLong("inactive");
        }

        try {
            churn = (totalInactiveSub / totalNewSub) * totalNewSub;
        } catch (Exception e) {

        }
        DecimalFormat df = new DecimalFormat("#.#");
        df.format(churn);
        return java.lang.Double.parseDouble(df.format(churn));
    }

    public long getSubscription(String query) throws SQLException {
        AppBroker.getSevasLogger().info("Get Subscription - " + query);

        statement = this.connection.prepareStatement(query);
        resultSet = statement.executeQuery();
        long report = 0;
        while (resultSet.next()) {
            report = resultSet.getLong(1);
        }
        return report;
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public ResultSet getDailyPaidContent(String dailyPaidContentRevenue) {
        ResultSet theResultSet = null;
        try {
            Statement theStatement = this.connection.createStatement();
            theResultSet = theStatement.executeQuery(dailyPaidContentRevenue);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return theResultSet;
    }

    public ArrayList<ContentLogs> queryLogs(Connection DBConnection) {
        logs = new ArrayList<>();
        ResultSet rsPagination = null;

        try {

            String sqlPagination = " select a.service_name as theService, "
                    + "date(b.sent_time) as theDate,count(b.recipient) AS total_sent,"
                    + "sum((CASE WHEN (b.delivery_status =  'SUBMITTED') THEN 1 ELSE 0 END)) AS submitted,"
                    + "sum((CASE WHEN (b.delivery_status =  'DELIVERED') THEN 1 ELSE 0 END)) AS delivered,"
                    + "sum((CASE WHEN (b.delivery_status =  'FAILED') THEN 1 ELSE 0 END)) AS failed,"
                    + " sum((CASE WHEN (b.delivery_status =  'BUFFERED') THEN 1 ELSE 0 END)) AS buffered,"
                    + "sum((CASE WHEN (b.delivery_status =  'REJECTED') THEN 1 ELSE 0 END)) AS rejected,"
                    + " sum((CASE WHEN (b.delivery_status =  'NO DLR') THEN 1 ELSE 0 END)) AS no_dlr "
                    + "FROM subscription_service a, delivered_outgoing_messages b "
                    + "where extract(month from b.sent_time) = extract (month from now()) "
                    + "and extract(year from b.sent_time) = extract (year from now()) "
                    + "and (b.src_module = 'DailyAlert' or b.src_module = 'ContentUpload' "
                    + "or b.src_module = 'InstantContent' or b.src_module = 'DailyPaidContent') "
                    + "and a.service_name = b.serv_name group by theService,theDate order by theService,theDate Desc";

            PreparedStatement psPagination = DBConnection.prepareStatement(sqlPagination);
            rsPagination = psPagination.executeQuery();

            while (rsPagination.next()) {
                log = new ContentLogs();
                log.setService(rsPagination.getString("theService"));
                log.setTheDate(rsPagination.getString("theDate"));
                log.setTotalSent(rsPagination.getLong("total_sent"));
                log.setSubmitted(rsPagination.getLong("submitted"));
                log.setDelivered(rsPagination.getLong("delivered"));
                log.setFailed(rsPagination.getLong("failed"));
                log.setBuffered(rsPagination.getLong("buffered"));
                log.setRejected(rsPagination.getLong("rejected"));
                log.setUnKnownStatus(rsPagination.getLong("no_dlr"));
                logs.add(log);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return logs;
    }

    public ArrayList<ContentLogs> todayContentReport(Connection DBConnection) {
        logs = new ArrayList<ContentLogs>();
        ResultSet rsPagination = null;

        try {

            String sqlPagination = " select a.service_name as theService, "
                    + "date(b.sent_time) as theDate,count(b.recipient) AS total_sent,"
                    + "sum((CASE WHEN (b.delivery_status =  'SUBMITTED') THEN 1 ELSE 0 END)) AS submitted,"
                    + "sum((CASE WHEN (b.delivery_status =  'DELIVERED') THEN 1 ELSE 0 END)) AS delivered,"
                    + "sum((CASE WHEN (b.delivery_status =  'FAILED') THEN 1 ELSE 0 END)) AS failed,"
                    + " sum((CASE WHEN (b.delivery_status =  'BUFFERED') THEN 1 ELSE 0 END)) AS buffered,"
                    + "sum((CASE WHEN (b.delivery_status =  'REJECTED') THEN 1 ELSE 0 END)) AS rejected,"
                    + " sum((CASE WHEN (b.delivery_status =  'NO DLR') THEN 1 ELSE 0 END)) AS no_dlr "
                    + "FROM subscription_service a, delivered_outgoing_messages b "
                    + "where date(b.sent_time) = date(now()) "
                    + "and (b.src_module = 'DailyAlert' or b.src_module = 'DailyPaidContent' "
                    + "or b.src_module = 'InstantContent' or b.src_module = 'ContentUpload') "
                    + "and a.service_name = b.serv_name group by theService,theDate order by theService,theDate Desc";

            PreparedStatement psPagination = DBConnection.prepareStatement(sqlPagination);
            rsPagination = psPagination.executeQuery();

            while (rsPagination.next()) {
                log = new ContentLogs();
                log.setService(rsPagination.getString("theService"));
                log.setTheDate(rsPagination.getString("theDate"));
                log.setTotalSent(rsPagination.getLong("total_sent"));
                log.setSubmitted(rsPagination.getLong("submitted"));
                log.setDelivered(rsPagination.getLong("delivered"));
                log.setFailed(rsPagination.getLong("failed"));
                log.setBuffered(rsPagination.getLong("buffered"));
                log.setRejected(rsPagination.getLong("rejected"));
                log.setUnKnownStatus(rsPagination.getLong("no_dlr"));
                logs.add(log);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return logs;
    }

}
