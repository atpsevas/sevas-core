/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;
//
//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//import org.apache.commons.httpclient.Credentials;
//import org.apache.commons.httpclient.UsernamePasswordCredentials;
//import org.apache.commons.httpclient.HttpClient;
//import org.apache.commons.httpclient.HttpState;
//import org.apache.commons.httpclient.methods.PostMethod;
//import org.apache.commons.httpclient.methods.RequestEntity;
//import org.apache.commons.httpclient.methods.StringRequestEntity;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

/**
 * @author saheedalbert
 */
public class AirtelUCIPTesteer {

    @SuppressWarnings("deprecation")
    public static void main(String arg[]) {

        String sebSubscriptionRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:char=\"http://ChargingProcess/com/ibm/sdp/services/charging/abstraction/Charging\"> \n"
                + "   <soapenv:Header/> \n"
                + "   <soapenv:Body> \n"
                + "      <char:charge> \n"
                + "         <inputMsg> \n"
                + "            <operation>debit</operation> \n"
                + "            <userId>2348081067641</userId> \n"
                + "            <contentId>111</contentId> \n"
                + "            <itemName>sample</itemName> \n"
                + "            <contentDescription>Games Club - Subscription Service</contentDescription> \n"
                + "            <circleId/> \n"
                + "            <lineOfBusiness/> \n"
                + "            <customerSegment/> \n"
                + "            <contentMediaType>Inspiration</contentMediaType> \n"
                + "            <serviceId>1</serviceId> \n"
                + "            <parentId/> \n"
                + "            <actualPrice>5.0</actualPrice> \n"
                + "            <basePrice>50.00</basePrice> \n"
                + "            <discountApplied>0</discountApplied> \n"
                + "            <paymentMethod/> \n"
                + "            <revenuePercent/> \n"
                + "            <netShare>0</netShare> \n"
                + "            <cpId>GENIENG_NG</cpId> \n"
                + "            <customerClass/> \n"
                + "            <eventType>Subscription Purchase</eventType> \n"
                + "            <localTimeStamp/> \n"
                + "            <transactionId/> \n"
                + "            <subscriptionName>Games Club</subscriptionName> \n"
                + "            <parentType/> \n"
                + "            <deliveryChannel>WAP</deliveryChannel> \n"
                + "            <subscriptionTypeCode>abcd</subscriptionTypeCode> \n"
                + "            <subscriptionExternalId>2</subscriptionExternalId> \n"
                + "            <contentSize/> \n"
                + "            <currency>NGN</currency> \n"
                + "            <copyrightId>xxx</copyrightId> \n"
                + "            <cpTransactionId>201303094814_2348023029359</cpTransactionId> \n"
                + "            <copyrightDescription>copyright</copyrightDescription> \n"
                + "            <sMSkeyword>sms</sMSkeyword> \n"
                + "            <srcCode>abcd</srcCode> \n"
                + "            <contentUrl>www.ibm.com</contentUrl> \n"
                + "            <subscriptiondays>1</subscriptiondays> \n"
                + "         </inputMsg> \n"
                + "      </char:charge> \n"
                + "   </soapenv:Body> \n"
                + "</soapenv:Envelope>";

        try {
            disableSslVerification();
            URL url = new URL("https://196.46.244.21:8443/ChargingServiceFlowWeb/sca/ChargingExport1");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Authorization", "Basic YmR0ZWNoOmJkdGVjaA");
            InputStream content = (InputStream) connection.getInputStream();
            BufferedReader in
                    = new BufferedReader(new InputStreamReader(content));
            String line;
            while ((line = in.readLine()) != null) {
                org.esme.broker.AppBroker.sevasLogger.info(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //        HttpClient client = new HttpClient();
//        HttpState state = client.getState();
//
//        // Set credentials on the client
//        Credentials credentials
//                = new UsernamePasswordCredentials("1300520_XUME_NG", "A86xY5kSWOw");
//        client.getState().setCredentials(null, null, credentials);
//
//        // Prepare HTTP post
//        PostMethod method = new PostMethod("https://196.46.244.21:8443/ChargingServiceFlowWeb/sca/ChargingExport1");
//        method.setDoAuthentication(true);
//
//        // Request content will be retrieved directly
//        // from the input stream
//        RequestEntity entity = null;
//        try {
//            entity = new StringRequestEntity(sebSubscriptionRequest, "text/xml", "ISO-8859-1");
//        } catch (UnsupportedEncodingException ex) {
//        }
//        method.setRequestEntity(entity);
//        method.setRequestHeader("SOAPAction", "");
//
//        // Execute request
//        try {
//            int result = 0;
//            try {
//                result = client.executeMethod(method);
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            }
//            // Display status code
//            org.esme.broker.AppBroker.sevasLogger.info("Response status code: " + result);
//            // Display response
//            org.esme.broker.AppBroker.sevasLogger.info("Response body: ");
//            try {
//                org.esme.broker.AppBroker.sevasLogger.info(method.getResponseBodyAsString());
//            } catch (IOException ex) {
//ex.printStackTrace();
//            }
//        } finally {
//            // Release current connection to the connection pool once you are done
//            method.releaseConnection();
//        }
    private static void disableSslVerification() {
        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }

            }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }

            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }
}
