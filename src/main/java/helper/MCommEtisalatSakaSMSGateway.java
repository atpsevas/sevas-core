/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author saheedalbert
 */
public class MCommEtisalatSakaSMSGateway {

    public String forwardMessageToSaka(String smsc, String from, String to,
                                       String text) throws URISyntaxException,
            org.apache.commons.httpclient.HttpException, IOException {

        String returnMsg;
        byte[] responseBody;
        List<NameValuePair> sendSMSParams = new ArrayList<NameValuePair>();

        sendSMSParams.add(new BasicNameValuePair("smsc", smsc));
        sendSMSParams.add(new BasicNameValuePair("username", "mcomm"));
        sendSMSParams.add(new BasicNameValuePair("password", "mcomm"));
        sendSMSParams.add(new BasicNameValuePair("from", from));
        sendSMSParams.add(new BasicNameValuePair("to", to));
        sendSMSParams.add(new BasicNameValuePair("text", text));

//        URI uri = URIUtils.createURI("http", "localhost", 13031, "/cgi-bin/sendsms",
//                URLEncodedUtils.format(sendSMSParams, "UTF-8"), null);
//        
//        
        URI uri = URIUtils.createURI("http", "10.71.173.155", 15032, "/cgi-bin/sendsms",
                URLEncodedUtils.format(sendSMSParams, "UTF-8"), null);

        HttpGet httpget = new HttpGet(uri);

        // Create an instance of HttpClient.
        HttpClient client = new HttpClient();

        // Create a method instance and replace all possible poison.
        GetMethod method = new GetMethod(uri.toString());

        // Provide custom retry handler is necessary
        method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                new DefaultHttpMethodRetryHandler(3, false));
        // Execute the method.
        int statusCode = client.executeMethod(method);
        if (statusCode != HttpStatus.SC_OK) {
            org.esme.broker.AppBroker.sevasLogger.error("Method failed: " + method.getStatusLine());
        }
        responseBody = method.getResponseBody();
        // Deal with the response.
        // Use caution: ensure correct character encoding and is not binary data
        org.esme.broker.AppBroker.sevasLogger.info(new String(responseBody));
        returnMsg = new String(responseBody);
        return returnMsg;
    }
}
