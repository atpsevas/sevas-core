/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import java.io.IOException;

/**
 * @author saheedalbert
 */
public class TestSDPSendSMSDLR {

    ////////////////////////////////////////////////////////////////////////
    // Post message to URL
    ////////////////////////////////////////////////////////////////////////
    public static void main(String arg[])
            throws Exception {
        String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
                + "    <soapenv:Header>\n"
                + "        <ns1:NotifySOAPHeader xmlns:ns1=\"http://www.huawei.com.cn/schema/common/v2_1\">\n"
                + "            <ns1:spRevId>sdp</ns1:spRevId> \n"
                + "            <ns1:spRevpassword>206D88BB7F3D154B130DD6E1E0B8828B</ns1:spRevpassword> \n"
                + "            <ns1:spId>000201</ns1:spId> \n"
                + "            <ns1:serviceId>35000001000001</ns1:serviceId> \n"
                + "            <ns1:timeStamp>111029084631570</ns1:timeStamp> \n"
                + "            <ns1:traceUniqueID>100001200101110623021721000011</ns1:traceUniqueID>\n"
                + "        </ns1:NotifySOAPHeader>\n"
                + "    </soapenv:Header>\n"
                + "    <soapenv:Body>\n"
                + "        <ns2:notifySmsDeliveryReceipt xmlns:ns2=\"http://www.org.esme.csapi.org/schema/parlayx/sms/notification/v2_2/local\">\n"
                + "            <ns2:correlator>00001</ns2:correlator> \n"
                + "            <ns2:deliveryStatus>\n"
                + "                <address>tel:8612312345678</address>\n"
                + "                <deliveryStatus>DeliveredToTerminal</deliveryStatus> \n"
                + "            </ns2:deliveryStatus>\n"
                + "        </ns2:notifySmsDeliveryReceipt> \n"
                + "    </soapenv:Body>\n"
                + "</soapenv:Envelope>";

        PostMethod post = new PostMethod("http://localhost/SEVAS_v2_1/MTNSDPSendSMSDLRReceiver");
        try {
            StringRequestEntity requestEntity = new StringRequestEntity(xml);
            post.setRequestEntity(requestEntity);
            post.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
            HttpClient httpclient = new HttpClient();
            int result = httpclient.executeMethod(post);
            org.esme.broker.AppBroker.sevasLogger.info("Response status code: " + result);
            org.esme.broker.AppBroker.sevasLogger.info("Response body: ");
            org.esme.broker.AppBroker.sevasLogger.info(post.getResponseBodyAsString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            post.releaseConnection();
        }
    }

}
